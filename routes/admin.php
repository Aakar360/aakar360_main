<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Admin" middleware group. Enjoy building your Admin!
|
*/

Route::match(['get', 'post'],'/login', [ 'as' => 'login', 'uses' => 'Admin@login']);
Route::get('/', 'Admin@index')->middleware('Login');
Route::get('/logout', 'Admin@logout')->middleware('Login');
Route::get('/map', 'Admin@map')->middleware('Login');
Route::match(['get', 'post'],'/products', 'Admin@products')->middleware('Login');
Route::match(['get', 'post'],'/banners', 'Admin@banners')->middleware('Login');
Route::match(['get', 'post'],'/s-how-it-works', 'Admin@s_how_it_works')->middleware('Login');
Route::match(['get', 'post'],'/home-page-design', 'Admin@home_page_design')->middleware('Login');
Route::match(['get', 'post'],'/mega-menu-image', 'Admin@mega_menu_image')->middleware('Login');
Route::match(['get', 'post'],'/categories', 'Admin@categories')->middleware('Login');
Route::match(['get', 'post'],'/brands', 'Admin@brands')->middleware('Login');
Route::match(['get', 'post'],'/manufacturing-hubs', 'Admin@manufacturingHubs')->middleware('Login');
/*Route::match(['get', 'post'],'/ph-relations', 'Admin@phRelations')->middleware('Login');*/
Route::match(['get', 'post'],'/featured', 'Admin@featured')->middleware('Login');
Route::match(['get', 'post'],'/services_categories', 'Admin@services_categories')->middleware('Login');
Route::match(['get', 'post'],'/services', 'Admin@services')->middleware('Login');
Route::match(['get', 'post'],'/design_category', 'Admin@design_category')->middleware('Login');
Route::match(['get', 'post'],'/layout-plans', 'Admin@layout_plan')->middleware('Login');
Route::match(['get', 'post'],'/photos', 'Admin@photos')->middleware('Login');
Route::match(['get', 'post'],'/update-photos', 'Admin@updatePhotos')->middleware('Login');
Route::match(['get', 'post'],'/update-questions', 'Admin@updateQuestions')->middleware('Login');
Route::match(['get', 'post'],'/questions', 'Admin@questions')->middleware('Login');
Route::match(['get', 'post'],'/category-update-questions', 'Admin@CategoryupdateQuestions')->middleware('Login');
Route::match(['get', 'post'],'/services_answer', 'Admin@ServicesAnswer')->middleware('Login');
Route::match(['get', 'post'],'/modify_plan', 'Admin@modifyPlan')->middleware('Login');
Route::match(['get', 'post'],'/faq', 'Admin@Faq')->middleware('Login');
Route::match(['get', 'post'],'/layout_faq', 'Admin@layoutFaq')->middleware('Login');
Route::match(['get', 'post'],'/product_faq', 'Admin@productFaq')->middleware('Login');
Route::match(['get', 'post'],'/service_category_questions', 'Admin@Categoryquestions')->middleware('Login');
Route::match(['get', 'post'],'/service_category_image', 'Admin@Categoryimage')->middleware('Login');
Route::match(['get', 'post'],'/get_image', 'Admin@Getimage')->middleware('Login');
Route::match(['get', 'post'],'/pages', 'Admin@pages')->middleware('Login');
Route::match(['get', 'post'],'/blog', 'Admin@blog')->middleware('Login');
Route::get('/customers', 'Admin@customers')->middleware('Login');
Route::post('/change-rfr', 'Admin@changeRfr')->middleware('Login');
Route::match(['get', 'post'],'/coupons', 'Admin@coupons')->middleware('Login');
Route::match(['get', 'post'],'/postcodes', 'Admin@postcodes')->middleware('Login');
Route::match(['get', 'post'],'/shipping', 'Admin@shipping')->middleware('Login');
Route::match(['get', 'post'],'/flat', 'Admin@flat')->middleware('Login');
Route::match(['get', 'post'],'/units', 'Admin@units')->middleware('Login');
Route::match(['get', 'post'],'/reviews', 'Admin@reviews')->middleware('Login');
Route::match(['get', 'post'],'/photo-reviews', 'Admin@photoreviews')->middleware('Login');
Route::match(['get', 'post'],'/layout_reviews', 'Admin@layout_reviews')->middleware('Login');
Route::match(['get', 'post'],'/orders', 'Admin@orders')->middleware('Login');
Route::get('/stats', 'Admin@stats')->middleware('Login');
Route::match(['get', 'post'],'/tracking', 'Admin@tracking')->middleware('Login');
Route::match(['get', 'post'],'/newsletter', 'Admin@newsletter')->middleware('Login');
Route::get('/referrers', 'Admin@referrers')->middleware('Login');
Route::get('/os', 'Admin@os')->middleware('Login');
Route::get('/browsers', 'Admin@browsers')->middleware('Login');
Route::match(['get', 'post'],'/payment', 'Admin@payment')->middleware('Login');
Route::match(['get', 'post'],'/currency', 'Admin@currency')->middleware('Login');
Route::match(['get', 'post'],'/settings', 'Admin@settings')->middleware('Login');
Route::match(['get', 'post'],'/theme', 'Admin@theme')->middleware('Login');
Route::match(['get', 'post'],'/lang', 'Admin@lang')->middleware('Login');
Route::get('/tokens', 'Admin@tokens')->middleware('Login');
Route::get('/export', 'Admin@export')->middleware('Login');
Route::match(['get', 'post'],'/slider', 'Admin@slider')->middleware('Login');
Route::match(['get', 'post'],'/editor', 'Admin@editor')->middleware('Login');
Route::match(['get', 'post'],'/templates', 'Admin@templates')->middleware('Login');
Route::match(['get', 'post'],'/builder', 'Admin@builder')->middleware('Login');
Route::match(['get', 'post'],'/menu', 'Admin@menu')->middleware('Login');
Route::match(['get', 'post'],'/bottom', 'Admin@bottom')->middleware('Login');
Route::match(['get', 'post'],'/fields', 'Admin@fields')->middleware('Login');
Route::match(['get', 'post'],'/support', 'Admin@support')->middleware('Login');
Route::match(['get', 'post'],'/administrators', 'Admin@administrators')->middleware('Login');
Route::match(['get', 'post'],'/profile', 'Admin@profile')->middleware('Login');
Route::match(['get', 'post'],'/layout_addon', 'Admin@layout_addon')->middleware('Login');
Route::match(['get', 'post'],'/layout-plans/add-questions/{slug}', 'Admin@layout_plan_questions')->middleware('Login');
Route::match(['get', 'post'],'/layout-plans/layout_plan_addon/{slug}', 'Admin@layout_plan_addon')->middleware('Login');
Route::match(['get', 'post'],'/amminities', 'Admin@amminities')->middleware('Login');
Route::match(['get', 'post'],'/image_likes', 'Admin@image_likes')->middleware('Login');
Route::match(['get', 'post'],'/image_inspirations', 'Admin@image_inspirations')->middleware('Login');
Route::match(['get', 'post'],'/plan_orders', 'Admin@plan_orders')->middleware('Login');
Route::get('/layout-plans/delete/{slug}/{que_id}', 'Admin@layout_plan_delete_question')->middleware('Login');
Route::get('/layout-plans/delete/{slug}/{addon_id}', 'Admin@layout_plan_addoon_delete')->middleware('Login');
Route::post('/get-variant', 'Admin@getVariant')->middleware('Login');
Route::post('/get-variant-data', 'Admin@getVariantData')->middleware('Login');
Route::post('/get-discount', 'Admin@getDiscount')->middleware('Login');
Route::match(['get', 'post'],'/advices_faq', 'Admin@advicesFaq')->middleware('Login');
Route::match(['get', 'post'],'/advices_reviews', 'Admin@advices_reviews')->middleware('Login');
Route::match(['get', 'post'],'/advices_category', 'Admin@advices_category')->middleware('Login');
Route::match(['get', 'post'],'/best-for-cat', 'Admin@BestForCat')->middleware('Login');
Route::match(['get', 'post'],'/services_reviews', 'Admin@services_reviews')->middleware('Login');
Route::match(['get', 'post'],'/best-for-cat', 'Admin@BestForCat')->middleware('Login');
Route::match(['get', 'post'],'/product-type', 'Admin@productType')->middleware('Login');
Route::match(['get', 'post'],'/filters', 'Admin@filters')->middleware('Login');
Route::match(['get', 'post'],'/best-for-layout', 'Admin@BestForLayout')->middleware('Login');
Route::match(['get', 'post'],'/design-cat-banner', 'Admin@DesignCatBanner')->middleware('Login');
Route::match(['get', 'post'],'/best-for-photo', 'Admin@BestForPhoto')->middleware('Login');
Route::match(['get', 'post'],'/design-cat-banner', 'Admin@DesignCatBanner')->middleware('Login');
Route::match(['get', 'post'],'/length', 'Admin@Length')->middleware('Login');
Route::match(['get', 'post'],'/size', 'Admin@Size')->middleware('Login');
Route::match(['get', 'post'],'/sizes_group', 'Admin@SizesGroup')->middleware('Login');
Route::match(['get', 'post'],'/get_sizes_group', 'Admin@getSizesGroup')->middleware('Login');
Route::match(['get', 'post'],'/get_variant_group', 'Admin@getVariantGroup')->middleware('Login');

Route::match(['get', 'post'],'/sellers-payment', 'Admin@sellersPayment')->middleware('Login');
Route::match(['get', 'post'],'/get-amount', 'Admin@GetAmount');
Route::match(['get', 'post'],'/cr-note', 'Admin@CrNote');
Route::match(['get', 'post'],'/dr-note', 'Admin@DrNote');
Route::match(['get', 'post'],'/seller_po', 'Admin@SellerPo');
Route::match(['get', 'post'],'/default_image', 'Admin@DefaultImage');
Route::match(['get', 'post'],'/link', 'Admin@Link');
Route::match(['get', 'post'],'/catData', 'Admin@catData');
Route::match(['get', 'post'],'/product_sr', 'Admin@productSr');

Route::get('/seller', 'Admin@seller')->middleware('Login');
Route::match(['get', 'post'],'institutional', 'Admin@institutional')->middleware('Login');
Route::match(['get', 'post'],'bookings', 'Admin@bookings')->middleware('Login');
Route::match(['get', 'post'],'dispatch-programs', 'Admin@dispatchPrograms')->middleware('Login');
Route::match(['get', 'post'],'request-quotation', 'Admin@requestQuotation')->middleware('Login');
Route::match(['get', 'post'],'/sellerview', 'Admin@sellerView')->middleware('Login');
Route::get('/pendingproduct', 'Admin@pendingproduct')->middleware('Login');
Route::get('/deleteproduct', 'Admin@deleteproduct')->middleware('Login');
Route::get('/approvalproduct', 'Admin@approvalproduct')->middleware('Login');
Route::get('/rejectproduct', 'Admin@rejectproduct')->middleware('Login');
Route::match(['get', 'post'],'/match-sku', 'Admin@MatchSku');
Route::get('/myproduct', 'Admin@myProduct')->middleware('Login');
Route::get('/notice', 'Admin@notice')->middleware('Login');
Route::match(['get', 'post'],'/registerimage', 'Admin@Registerimage')->middleware('Login');
Route::match(['get', 'post'],'/tmtcalculator', 'Admin@Tmtcalculator')->middleware('Login');
Route::match(['get', 'post'],'/notification', 'Admin@Notification')->middleware('Login');
Route::match(['get', 'post'],'/retail_notification', 'Admin@retailNotification')->middleware('Login');
Route::match(['get', 'post'],'/request-for-call', 'Admin@requestForCall')->middleware('Login');
Route::match(['get', 'post'],'/quick-quote', 'Admin@quickQuote')->middleware('Login');
Route::match(['get', 'post'],'/mentor-program', 'Admin@mentorProgram')->middleware('Login');
Route::match(['get', 'post'],'/credit-charges', 'Admin@creditCharges')->middleware('Login');
Route::match(['get', 'post'],'/counter-offer', 'Admin@counterOffer')->middleware('Login');
Route::post('/get-customer-discount', 'Admin@getCustomerDiscount')->middleware('Login');
Route::match(['get', 'post'],'/cities', 'Admin@cities')->middleware('Login');
Route::match(['get', 'post'],'/get-cities-data', 'Admin@getCitiesData')->middleware('Login');

Route::post('/variantMultipleDelete', 'Admin@variantMultipleDelete')->middleware('Login');
Route::post('/get-multiple-data', 'Admin@getMultipleData')->middleware('Login');
Route::get('variantData', 'Admin@variantData')->middleware('Login');

Route::match(['get', 'post'],'/dealers', 'Admin@dealers')->middleware('Login');
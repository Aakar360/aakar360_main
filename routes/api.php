<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/register', 'Api@register');
Route::post('/login', 'Api@login');
Route::post('/logout', 'Api@logout');
Route::post('/billing', 'Api@billing');
Route::post('/servicebilling', 'Api@servicebilling');
Route::post('/plan-billing', 'Api@planbilling');
Route::post('/shipping', 'Api@shipping');
Route::post('/service-shipping', 'Api@serviceShipping');
Route::post('/plan-shipping', 'Api@planShipping');
Route::post('/shipping-method', 'Api@shippingMethod');
Route::post('/plan-shipping-method', 'Api@planshippingMethod');
Route::post('/payment-method', 'Api@paymentMethod');
Route::post('/plan-payment-method', 'Api@paymentMethod');
Route::match(['get', 'post'],'/order-confirm', 'Api@confirmOrder');
Route::post('/service-order-confirm', 'Api@ServiceconfirmOrder');
Route::post('/plan-order-confirm', 'Api@planconfirmOrder');
Route::post('/confirm', 'Api@confirm');
Route::post('/serviceconfirm', 'Api@serviceconfirm');
Route::post('/plan-confirm', 'Api@planconfirm');
Route::get('/products', 'Api@products');
Route::get('/products', 'Api@products');
Route::get('/posts', 'Api@posts');
Route::post('/add', 'Api@add');
Route::get('/book', 'Api@book');
Route::get('/cart', 'Api@cart');
Route::get('/remove', 'Api@remove');
Route::get('/checkout', 'Api@checkout');
Route::match(['get', 'post'],'/payment', 'Api@payment');
Route::match(['get', 'post'],'/pay', 'Api@pay');
Route::any('/paypal', 'Api@paypal');
Route::post('/review', 'Api@review');
Route::get('/coupon', 'Api@coupon');
Route::get('/subscribe', 'Api@subscribe');

Route::get('/orders', 'Api@orders')->middleware('token');
Route::get('/reviews', 'Api@reviews')->middleware('token');
Route::post('/get-states', 'Api@getStates');
Route::post('/get-cities', 'Api@getCities');


//Passport Routes
Route::post('/app/login', 'API\UserController@login');
Route::post('/app/register', 'API\UserController@register');
Route::post('/app/verifyOTP', 'API\UserController@verifyOTP');
Route::post('/app/retail_register', 'API\UserController@retail_register');
Route::post('/app/retail_login', 'API\UserController@retail_login');
Route::post('/app/call_log', 'API\UserController@call_log');
Route::get('/app/user', 'API\UserController@user');
Route::post('/app/check_login', 'API\UserController@check_login');
Route::post('/app/check_customer', 'API\UserController@check_customer');
Route::post('/app/quickentry', 'API\UserController@quickentry');
Route::post('/app/retail_nav', 'API\UserController@retail_nav');

Route::post('/app/update_profile', 'API\UserController@updateProfile');         //Added by BK
Route::post('/app/get_profile', 'API\UserController@getProfile');
Route::get('/app/country_list', 'API\UserController@countryList');              //Added by BK
Route::get('/app/state_list/{country_id}', 'API\UserController@stateList');     //Added by BK
Route::get('/app/city_list/{state_id}', 'API\UserController@cityList');         //Added by BK
Route::get('/app/customers', 'API\UserController@customers');
Route::get('/app/searchapi', 'API\UserController@searchApi');

//Institutional App APIs
Route::get('/app/institutional/index', 'API\InstitutionalController@index');
Route::get('/app/institutional/category', 'API\InstitutionalController@getCategory');
Route::get('/app/institutional/category1', 'API\InstitutionalController@getCategory1');
Route::post('/app/institutional/products', 'API\InstitutionalController@getProductsByCat');
Route::get('/app/institutional/nav', 'API\InstitutionalController@nav');
Route::get('/app/institutional/product', 'API\InstitutionalController@product');
Route::post('/app/institutional/review', 'API\InstitutionalController@review');
Route::post('/app/institutional/faq', 'API\InstitutionalController@faq');
Route::post('/app/institutional/quick_quote', 'API\InstitutionalController@quick_quote');
Route::post('/app/institutional/enquiry', 'API\InstitutionalController@enquiry');
Route::post('/app/institutional/book', 'API\InstitutionalController@book');
Route::get('/app/institutional/pending-bookings', 'API\InstitutionalController@myBookingsPending');
Route::get('/app/institutional/approved-bookings', 'API\InstitutionalController@myBookingsApproved');
Route::get('/app/institutional/dispatched-bookings', 'API\InstitutionalController@myBookingsDispatched');
Route::get('/app/institutional/hubs', 'API\InstitutionalController@hubs');
Route::get('/app/institutional/get-products', 'API\InstitutionalController@getProductsByIds');
Route::get ('/app/institutional/brands', 'API\InstitutionalController@getBrand');
Route::post ('/app/institutional/mentor', 'API\InstitutionalController@mentor');
Route::post ('/app/institutional/request_quotation', 'API\InstitutionalController@requestQuotation');
Route::get ('/app/institutional/get-live-quotation', 'API\InstitutionalController@getLiveQuotation');
Route::get ('/app/institutional/get-expired-quotation', 'API\InstitutionalController@getExpiredQuotation');
Route::get ('/app/institutional/quote_detail', 'API\InstitutionalController@getQuotationDetail');
Route::get ('/app/institutional/get-notification', 'API\InstitutionalController@getNotification');
Route::post ('/app/institutional/requestForCall', 'API\InstitutionalController@requestForCall');
Route::get ('/app/institutional/getBrands', 'API\InstitutionalController@getBrands');
Route::get ('/app/institutional/getProductByBrand', 'API\InstitutionalController@getProductByBrand');
Route::post ('/app/institutional/rfqBooking', 'API\InstitutionalController@rfqBooking');
Route::post('/app/institutional/forgotPassword', 'API\InstitutionalController@forgotPassword');
Route::post('/app/institutional/resendOTP', 'API\InstitutionalController@resendOTP');
Route::post('/app/institutional/resetPassword', 'API\InstitutionalController@resetPassword');
Route::post('/app/institutional/verifyOTP', 'API\InstitutionalController@verifyOTP');
Route::post('/app/institutional/getCity', 'API\InstitutionalController@getCity');
Route::post('/app/institutional/getState', 'API\InstitutionalController@getState');
Route::get('/app/institutional/getCountry', 'API\InstitutionalController@getCountry');
Route::get('/app/institutional/counter-offer', 'API\InstitutionalController@counterOffer');
Route::post('/app/institutional/counter-offer-delete', 'API\InstitutionalController@counterOfferDelete');
//Retail App APIs
Route::post('/app/retail/index', 'API\UserController@index');
Route::post('/app/retail/category', 'API\UserController@getretailCategory');
Route::post('/app/retail/product', 'API\UserController@getretailProduct');
Route::post ('/app/retail/products', 'API\UserController@getretailProductsByCat');
Route::get('/app/retailsearchapi', 'API\UserController@retailSearchApi');
Route::post('/app/retail/add_cart', 'API\UserController@addCart');
Route::post('/app/retail/remove', 'API\UserController@remove');
Route::post('/app/retail/cart', 'API\UserController@cart');
Route::post('/app/retail/checkout', 'API\UserController@checkout');
Route::post('/app/retail/checkout_data', 'API\UserController@checkoutData');
Route::post('/app/retail/update_cart', 'API\UserController@updateCart');
Route::post('/app/retail/billing', 'API\UserController@billing');
Route::post('/app/retail/shipping', 'API\UserController@shipping');
Route::post('/app/retail/shippingmethod', 'API\UserController@shippingmethod');
Route::post('/app/retail/paymentmethod', 'API\UserController@paymentmethod');
Route::post('/app/retail/order_review', 'API\UserController@confirmOrder');
Route::post('/app/retail/place_order', 'API\UserController@PlaceOrder');
Route::post('/app/retail/my_orders', 'API\UserController@myOrders');
Route::post('/app/retail/check_avail', 'API\UserController@checkAvail');
Route::post('/app/retail/advices_cat', 'API\UserController@advicesCategory');
Route::post('/app/retail/blogs', 'API\UserController@blogs');
Route::post('/app/retail/layout_cat', 'API\UserController@layoutCategory');
Route::post('/app/retail/photo_cat', 'API\UserController@photoCategory');
Route::post('/app/retail/photos', 'API\UserController@photos');
Route::post('/app/retail/layout_plan', 'API\UserController@layoutPlans');
Route::post('/app/retail/layout_plan_detail', 'API\UserController@layoutPlanDetail');
Route::post('/app/retail/service_cat', 'API\UserController@servicesCategory');
Route::post('/app/retail/sub_service', 'API\UserController@subService');
Route::post('/app/retail/advices', 'API\UserController@advices');
Route::post('/app/retail/photo_gallery', 'API\UserController@photosGallery');
Route::post('/app/customers', 'API\UserController@customerData');
Route::post('/app/retail/advice', 'API\UserController@advice');
Route::post('/app/retail/layout-reviews', 'API\UserController@layoutReview');
Route::post('/app/retail/photo-reviews', 'API\UserController@photoReview');
Route::post('/app/retail/advice-reviews', 'API\UserController@adviceReview');
Route::post('/app/retail/product-reviews', 'API\UserController@productReview');
Route::post('/app/retail/layout-faq/', 'API\UserController@layoutFaq');
Route::post('/app/retail/product-faq/', 'API\UserController@productFaq');
Route::post('/app/retail/advices-faq/', 'API\UserController@advicesFaq');
Route::post('/app/retail/services-faq/', 'API\UserController@servicesFaq');

Route::post('/app/retail/advice-like', 'API\UserController@advicelike');
Route::post('/app/retail/advice-save', 'API\UserController@advicesave');
Route::post('/app/retail/like-plan', 'API\UserController@LikePlan');
Route::post('/app/retail/like-image', 'API\UserController@likeImage');
Route::post('/app/retail/add-to-project', 'API\UserController@addToProject');
Route::post('/app/retail/useradvices', 'API\UserController@userAdvices');
Route::post('/app/retail/userDesignes', 'API\UserController@userDesignes');
Route::post('/app/retail/forgotPassword', 'API\UserController@forgotPassword');
Route::post('/app/retail/getRecentViewedProduct', 'API\UserController@getRecentViewedProduct');
Route::post('/app/retail/getRecentViewedAdvice', 'API\UserController@getRecentViewedAdvice');
Route::post('/app/retail/getRecentViewedLayout', 'API\UserController@getRecentViewedLayout');
Route::post('/app/retail/getRecentViewedGallery', 'API\UserController@getRecentViewedGallery');

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Admin" middleware group. Enjoy building your Admin!
|
*/

Route::match(['get', 'post'],'/login', 'Crm@login');
Route::match(['get', 'post'],'/', 'Crm@login');
Route::get('/index', 'Crm@index')->middleware('CrmLogin');
Route::get('/logout', 'Crm@logout')->middleware('CrmLogin');
Route::match(['get', 'post'],'/sms', 'Crm@Sms')->middleware('CrmLogin');
Route::match(['get', 'post'],'/variant-compare', 'Crm@Variantcompare')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-report-option', 'Crm@Getreportoption')->middleware('CrmLogin');
Route::match(['get', 'post'],'/add-default-sales-pipeline', 'Crm@addDefaultPipeline')->middleware('CrmLogin');
Route::match(['get', 'post'],'/insert-pipeline', 'Crm@insertPipeline')->middleware('CrmLogin');
Route::match(['get', 'post'],'/update-pipeline', 'Crm@updatePipeline')->middleware('CrmLogin');
Route::match(['get', 'post'],'/update-delete-pipeline', 'Crm@updateDeletePipeline')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customers', 'Crm@Customers')->middleware('CrmLogin');
Route::get('/verify-customer', 'Crm@verifyCustomer')->middleware('CrmLogin');
Route::get('/verify-tronsportor', 'Crm@verifyTransportor')->middleware('CrmLogin');
Route::get('/get-issue-data', 'Crm@getIssueData')->middleware('CrmLogin');
Route::get('/get-customers', 'Crm@getCustomers')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-refered-by', 'Crm@getReferedCustomer')->middleware('CrmLogin');
Route::get('/get-customers-data', 'Crm@getCustomersData')->middleware('CrmLogin');
Route::get('/get-unqualified-data', 'Crm@getUnqualifiedData')->middleware('CrmLogin');
Route::get('/get-unreachable-data', 'Crm@getUnreachableData')->middleware('CrmLogin');
Route::get('/get-leads_order-data', 'Crm@getLeadsData')->middleware('CrmLogin');
Route::get('/get-transportor-data', 'Crm@getTransportorData')->middleware('CrmLogin');
Route::get('/get-notinterested-data', 'Crm@getNotinterestedData')->middleware('CrmLogin');
Route::get('/get-converted-customers', 'Crm@getConvertedCustomers')->middleware('CrmLogin');
Route::get('/get-vists-data', 'Crm@getVisitsData')->middleware('CrmLogin');
Route::get('/get-calls-data', 'Crm@getCallsData')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-bpr-data', 'Crm@getBPRData')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-bpr-data-sw', 'Crm@getBPRDataSW')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-scr-data', 'Crm@getSCRData')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-scr-data-sw', 'Crm@getSCRDataSW')->middleware('CrmLogin');
Route::post('/get-visits-chart', 'Crm@getVisitsChartData')->middleware('CrmLogin');
Route::post('/get-visits-map', 'Crm@getVisitsMapData')->middleware('CrmLogin');
Route::post('/get-calls-chart', 'Crm@getCallsChartData')->middleware('CrmLogin');
Route::post('/get-conversions-chart', 'Crm@getConversionChartData')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-district', 'Crm@Getdistrict')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer_delete', 'Crm@Customerdelete')->middleware('CrmLogin');
Route::delete('/locality_delete', 'Crm@Localitydelete')->middleware('CrmLogin');
Route::delete('/block_delete', 'Crm@blockDelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/product-price-list', 'Crm@ProductPriceList')->middleware('CrmLogin');
Route::match(['get', 'post'],'/add-product-price', 'Crm@AddProductPrice')->middleware('CrmLogin');
Route::match(['get', 'post'],'/pdf', 'Crm@PDF')->middleware('CrmLogin');
Route::match(['get', 'post'],'/pdf-preview', 'Crm@PdfPreview')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-cust-data', 'Crm@getCustData')->middleware('CrmLogin');
Route::match(['get', 'post'],'/marketing-data', 'Crm@MarketingData')->middleware('CrmLogin');
Route::match(['get', 'post'],'/district-master', 'Crm@Districtmaster')->middleware('CrmLogin');
Route::match(['get', 'post'],'/no-order-reason', 'Crm@NoOrderReason')->middleware('CrmLogin');
Route::match(['get', 'post'],'/block-master', 'Crm@blockmaster')->middleware('CrmLogin');
Route::match(['get', 'post'],'/brand-penetration-report/{slug?}', 'Crm@brandPenetrationReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/supplier-competition-report/{slug?}', 'Crm@supplierCompetitionReport')->middleware('CrmLogin');
Route::get('/crm-ticket', 'Crm@crm_ticket_report')->middleware('CrmLogin');
Route::get('/crm-ticket-data', 'Crm@crm_ticket_report_data')->middleware('CrmLogin');
Route::get('/cv-frequency', 'Crm@cvFrequency')->middleware('CrmLogin');
//Route::get('/cv-frequency-data', 'Crm@cvFrequencyData')->middleware('CrmLogin');
Route::get('/cv-frequency-data', 'Crm@cvFrequencyDataNew')->middleware('CrmLogin');
Route::get('/cv-frequency-visit', 'Crm@cvFrequencyVisit')->middleware('CrmLogin');
Route::get('/cv-frequency-visit-data', 'Crm@cvFrequencyVisitData')->middleware('CrmLogin');
Route::match(['get', 'post'],'/crm-quick-entry-report', 'Crm@crm_quick_entry_report')->middleware('CrmLogin');
Route::get('/get-quick_report-data', 'Crm@crm_quick_report_data')->middleware('CrmLogin');
Route::match(['get', 'post'],'/quick-report-delete', 'Crm@quickreportdelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/locality-master', 'Crm@LocalityMaster')->middleware('CrmLogin');
Route::get('/get-districts', 'Crm@getDistricts')->middleware('CrmLogin');
Route::get('/get-blocks', 'Crm@getBlocks')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-block', 'Crm@GetBlockAjax')->middleware('CrmLogin');
Route::get('/get-locality', 'Crm@getLocality')->middleware('CrmLogin');
Route::post('/get-locality', 'Crm@GetLocalityAjax')->middleware('CrmLogin');
Route::post('/get-price-groups-ajax', 'Crm@GetPriceGroupsAjax')->middleware('CrmLogin');
Route::post('/save-locality', 'Crm@saveLocalityAjax')->middleware('CrmLogin');
Route::post('/save-manageBySave', 'Crm@manageBySaveAjax')->middleware('CrmLogin');
Route::match(['get', 'post'],'/category-master', 'Crm@Categorymaster')->middleware('CrmLogin');
Route::match(['get', 'post'],'/designation-master', 'Crm@Designationmaster')->middleware('CrmLogin');
Route::match(['get', 'post'],'/contact-designation', 'Crm@Contactdesignation')->middleware('CrmLogin');
Route::match(['get', 'post'],'/company', 'Crm@Companymaster')->middleware('CrmLogin');
Route::match(['get', 'post'],'/association-master', 'Crm@assocMaster')->middleware('CrmLogin');
Route::match(['get', 'post'],'/price-groups-master', 'Crm@CategoryGroupsMaster')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-price', 'Crm@Getprice')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-category_all_price', 'Crm@Getcategoryallprice')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-category_all_price_cust', 'Crm@Getcategoryallpricecust')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-data', 'Crm@CustomerData')->middleware('CrmLogin');
Route::match(['get', 'post'],'/converted-customers', 'Crm@ConvertedCustomers')->middleware('CrmLogin');
Route::match(['get', 'post'],'/sales-pipeline', 'Crm@SalesPipeline')->middleware('CrmLogin');
Route::match(['get', 'post'],'/score-card', 'Crm@ScoreCard')->middleware('CrmLogin');
Route::match(['get', 'post'],'/score-card-search', 'Crm@ScoreCardSearch')->middleware('CrmLogin');
Route::match(['get', 'post'],'/visit-report', 'Crm@visitReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/call-report', 'Crm@callReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/transportor-report', 'Crm@transportorReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/unqualified-report', 'Crm@unqualiReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/unreachable-report', 'Crm@unreachableReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/lead-order-report', 'Crm@leadOrderReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/notinterested-report', 'Crm@notinterestReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customers-map', 'Crm@CustomersMapReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/visit-map', 'Crm@visitMapReport')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-profile', 'Crm@CustomerProfile')->middleware('CrmLogin');
Route::match(['get', 'post'],'/ticket-edit ', 'Crm@Ticketedit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/project-edit ', 'Crm@Projectedit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/product-edit ', 'Crm@Productedit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/promotinal-edit ', 'Crm@Promoedit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-class-update', 'Crm@Customerclassupdate')->middleware('CrmLogin');
Route::match(['get', 'post'],'/project-subtype-master', 'Crm@Projectsubtypemaster')->middleware('CrmLogin');
Route::get('/get-projects-subtype', 'Crm@getProjectsSubtype')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-subtype', 'Crm@Getsubtype')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer_category_delete', 'Crm@Customercategorydelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/cust_designation_delete', 'Crm@CustomerDesignationdelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/cust_transportor_delete', 'Crm@CustomerTranportordelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/cust_contax_delete', 'Crm@CustomerContaxdelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/association_delete', 'Crm@AssociationDelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/company_delete', 'Crm@Companydelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/project_subtype_delete', 'Crm@Projectsubtypedelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-project-delete', 'Crm@Customerprojectdelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-ticket-delete', 'Crm@Customerticketdelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-note-delete', 'Crm@Customernotedelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/trans-issue-delete', 'Crm@TransIssuedelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-visit-delete', 'Crm@Customervisitdelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-promotional-delete', 'Crm@Customerpromotionaldelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-product-delete', 'Crm@Customerproductdelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/note-edit ', 'Crm@Noteedit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/issue-edit ', 'Crm@Issueedit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/visit-edit ', 'Crm@Visitedit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/promotional-edit', 'Crm@Promotionaledit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-district-ajax', 'Crm@GetDistrictAjax')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-stages-ajax', 'Crm@GetStagesAjax')->middleware('CrmLogin');
Route::match(['get', 'post'],'/update-call_log-ajax', 'Crm@UpdateCallLogAjax')->middleware('CrmLogin');
Route::match(['get', 'post'],'/delete-call_log-ajax', 'Crm@DeleteCallLogAjax')->middleware('CrmLogin');
Route::match(['get', 'post'],'/delete-pipeline-ajax', 'Crm@DeletePipelineAjax')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-brands ', 'Crm@GetBrands')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-supplier ', 'Crm@GetSupplier')->middleware('CrmLogin');
Route::match(['get', 'post'],'/suppliers', 'Crm@Suppliers')->middleware('CrmLogin');
Route::get('/get-suppliers', 'Crm@getSuppliers')->middleware('CrmLogin');
Route::match(['get', 'post'],'/supplier_delete', 'Crm@Supplierdelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/call-edit ', 'Crm@Calledit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/lead_order-edit ', 'Crm@LeadOrderEdit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/transportor_order-edit', 'Crm@TransportorOrderEdit')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-call-delete', 'Crm@Customercalldelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer-lead-delete', 'Crm@Customerleaddelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/user-manager', 'Crm@CrmUserManager')->middleware('CrmLogin');
Route::match(['get', 'post'],'/company-master', 'Crm@Companymaster')->middleware('CrmLogin');
Route::match(['get', 'post'],'/transportor-master', 'Crm@transportorMaster')->middleware('CrmLogin');
Route::get('/get-users', 'Crm@getUsers')->middleware('CrmLogin');
Route::get('/get-company', 'Crm@getCompany')->middleware('CrmLogin');
Route::match(['get', 'post'],'/customer_user_delete', 'Crm@Customeruserdelete')->middleware('CrmLogin');
Route::match(['get', 'post'],'/user-role', 'Crm@CrmUserRole')->middleware('CrmLogin');
Route::match(['get', 'post'],'/msg_save', 'Crm@CrmMsgSave')->middleware('CrmLogin');

Route::get('/login-with-token', 'Crm@loginUsingToken');
Route::post('/login-app', 'Crm@postLogin');

//Customer Profile Datatables
Route::get('/get-tickets-data', 'Crm@getTicketsData')->middleware('CrmLogin');
Route::get('/get-notes-data', 'Crm@getNotesData')->middleware('CrmLogin');
Route::get('/get-projects-data', 'Crm@getProjectsData')->middleware('CrmLogin');
Route::get('/get-visits-data', 'Crm@getVisitsDataP')->middleware('CrmLogin');
Route::get('/get-products-data', 'Crm@getProductsData')->middleware('CrmLogin');
Route::get('/get-promotion-data', 'Crm@getPromoData')->middleware('CrmLogin');
Route::match(['get', 'post'],'/get-message-data', 'Crm@getMessageData')->middleware('CrmLogin');
Route::get('/get-call-data', 'Crm@getCallData')->middleware('CrmLogin');
Route::get('/get-leave_order-data', 'Crm@getLeaveOrderData')->middleware('CrmLogin');
Route::get('/get-month_order-data', 'Crm@getMonthOrderData')->middleware('CrmLogin');
Route::get('/update-medians', 'Crm@crmUpdateMedian');
Route::get('/notification', 'Crm@notification')->middleware('CrmLogin');
Route::get('/update-month-order-records', 'Crm@updateMonthOrderRecords')->middleware('CrmLogin');
Route::get('/update-call-records', 'Crm@updateCallRecords')->middleware('CrmLogin');
Route::get('/update-visit-records', 'Crm@updateVisitRecords')->middleware('CrmLogin');
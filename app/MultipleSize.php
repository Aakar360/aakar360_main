<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class MultipleSize extends Model
{

    protected $table = 'multiple_size';

}

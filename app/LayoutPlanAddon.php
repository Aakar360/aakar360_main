<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class LayoutPlanAddon extends Model
{

    protected $table = 'layout_plan_addon';

}

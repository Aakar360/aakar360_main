<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class CrmSupplier extends Model
{

    protected $table = 'crm_supplier';

}

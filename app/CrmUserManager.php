<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class CrmUserManager extends Model
{

    protected $table = 'crm_user_manager';

}

<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class HomePageDesign extends Model
{

    protected $table = 'home_page_design';

}

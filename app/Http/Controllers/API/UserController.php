<?php
namespace App\Http\Controllers\API;


use App\CrmCustomer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\DB;
use Image;
use Carbon\Carbon;

class UserController extends Controller
{
    public $successStatus = 200;
    public $cfg;
    public $cart;
    public $cart_options;
    public $cart_variants;

    public function __construct()
    {   $this->cfg = DB::select('SELECT * FROM config WHERE id = 1')[0];
        $this->cart = isset($_COOKIE['cart']) ? json_decode(stripslashes($_COOKIE['cart']),true) : array();
        $this->cart_options = isset($_COOKIE['cart_options']) ? json_decode($_COOKIE['cart_options'],true) : array();        $this->cart_variants = isset($_COOKIE['cart_variants']) ? json_decode($_COOKIE['cart_variants'],true) : array();
    }
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    protected function validateAPI($key){
        $result = DB::table('oauth_clients')->where('secret', $key)->where('personal_access_client', 1)->count();
        if($result){
            return true;
        }
        return false;
    }
    public function index(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
            $i_count =0;
            if(isset($request->token)) {
                if (!empty($request->token)) {
                    $user = Customer::where('appid', $request->token)->first();
					if($user !== null){
						$notification_read = DB::table('notification_read')->where('customer_id','=',$user->id)->first();
						if($notification_read !== null){
							if($notification_read->note_read > 0){
								$i_count = $notification_read->note_read;
							}
							else{
								$i_count =0;
							}

						}
					}
                }
                else{
                    $i_count =0;
                }
            }
            else{
                $i_count =0;
            }
            $response = array();
//            $response['noti_count'] = $i_count;
            $banners = DB::table('banner')->where('section', 'home')->orderBy('priority', 'ASC')->get();
            foreach ($banners as $banner) {
                $response['banners'][] = array(
                    'title' => $banner->title,
                    'description' => $banner->short_description,
                    'image' => url('assets/banners/' . $banner->image)
                );
            }
            $pcs = getPCategories();
//            dd($pcs);
            foreach ($pcs as $pc) {
                $response['pcs'][] = array(

                    'id' => $pc->id,
                    'title' => $pc->name,
                    'image' => url('assets/products/' . $pc->image)
                );
            }
            $pcss = DB::table('category')->where('popular', 1)->orderBy('popular_priority', 'ASC')->skip(0)->take(5)->get();

            $i = 0;
            foreach ($pcss as $pcsss) {
                $pcp = DB::table('products')->where('category', $pcsss->id)->orderBy('id', 'DESC')->skip(0)->take(6)->get();
                $cps = array();
                foreach ($pcp as $cp) {
                    $cps[] = array(
                        'id' => $cp->id,
                        'title' => translate($cp->title),
                        'image' => url('/assets/products/' . image_order($cp->images)),
                        'price' => $cp->price
                    );
                }
                $response['pcss'][] = array(
                    'id' => $pcsss->id,
                    'title' => $pcsss->name,
                    'products' => $cps
                );
                $i++;
            }
            $response['skip'] = $i;
            $top_brands = DB::table('brand')->orderBy('name', 'ASC')->get();
            foreach ($top_brands as $tb) {
                $response['tb'][] = array(
                    'id' => $tb->id,
                    'image' => url('assets/brand/' . $tb->image)
                );
            }

            $chb = DB::select("SELECT * FROM category WHERE popular_in_brands=1 ORDER BY popular_brands_priority ASC LIMIT 1");
            if(!empty($chb)){
                $response['chb'] = $chb;
            }else{
                $response['chb'] = [];
            }
            $sc = DB::select("select * from services_category where parent = 0 order by name ASC");
            if(!empty($sc)){
				foreach($sc as $s){
					$s->image = url('assets/services/' . $s->image);
					$response['sc'][] = $s;
				}
            }else{
                $response['sc'] = [];
            }
            $dc = DB::select("select * from design_category where parent = 0 order by name ASC");
            if (!empty($dc)){
				foreach($dc as $d){
					$d->image = url('assets/homepagedesign/' . $d->image);
					$response['dc'][] = $d;
				}
            }else{
                $response['dc'] = [];
            }
            $pro_s = DB::select("select * from services_category where display = 1 order by name ASC");
            if(empty($pro_s)){
                $response['prof_services'] = [];
            }
            foreach ($pro_s as $ps) {
                $response['prof_services'][] = array(
                    'id' => $ps->id,
                    'name' => $ps->name,
                    'image' => url('assets/services/'.$ps->image)
                );
            }
            $top_design = DB::select('select * from home_page_design');
            if(empty($top_design)){
                $response['top_design'] = [];
            }
            foreach ($top_design as $td) {
                $response['top_design'][] = array(
                    'id' => $td->id,
                    'name' => $td->title,
                    'image' => url('assets/homepagedesign/'.$td->image),
                    'link'  => $td->link
                );
            }

            $posts =  DB::select("select * from blog where section = 'retail' order by time DESC");

            if(empty($posts)){
                $response['top_advice'] = [];
            }
            foreach ($posts as $post) {
                $response['top_advices'][] = array(
                    'id' => $post->id,
                    'name' => $post->title,
                    'short_description' => $post->short_des,
                    'content' => $post->content,
                    'time' => $post->time,
                    'visits' => $post->visits,
                    'image' => url('assets/blog/'.$post->images)
                );
            }
            return response()->json($response, 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
    public function login(Request $request){
        if(isset($request->API_KEY)){
            if(!$this->validateAPI($request->API_KEY)){
                return response()->json(['message'=>'unauthorised'], 401);
            }
            $email = escape($request->email);
            $pass = md5($request->password);
            $gsm_id = $request->gcmid;
            if(empty($email) or empty($pass) or empty($gsm_id)){
                return response()->json(['message'=>'required'], 200);
            } else {
                if(Customer::where('email', $email)->orWhere('mobile', $email)->where('password', $pass)->count() > 0) {
                    $c = Customer::where('email', $email)->orWhere('mobile', $email)->where('password', $pass)->first();
					if($c){
						$cust = $c;
						if($cust->user_type == 'institutional'){
							if($cust->status == 0){
								$error = 'wait';
								return response()->json(['message' => $error, 'user_type' => $cust->user_type], $this->successStatus);
							}
							else{
								$secure_id = md5(microtime());
								$cust->appid = $secure_id;
								$cust->gsm_id = $gsm_id;
								$cust->save();
								return response()->json(['message' => $secure_id, 'user_type' => $cust->user_type,'company'=>$cust->company, 'email'=>$cust->email], $this->successStatus);
							}
						}
						$secure_id = md5(microtime());
						$cust->appid = $secure_id;
						$cust->gsm_id = $gsm_id;
						$cust->save();
						return response()->json(['message' => $secure_id, 'user_type' => $cust->user_type], $this-> successStatus);
					}else{
						$cust = Customer::where('mobile', $email)->orWhere('mobile', $email)->where('password', $pass)->first();
						if($cust === null){
							return response()->json(['message'=>'email or password is incorrect'], 401);
						}
						if($cust->user_type == 'institutional'){
							if($cust->status == 0){
								$error = 'wait';
								return response()->json(['message' => $error, 'user_type' => $cust->user_type], $this->successStatus);
							}
							else{
								$secure_id = md5(microtime());
								$cust->appid = $secure_id;
								$cust->gsm_id = $gsm_id;
								$cust->save();
								return response()->json(['message' => $secure_id, 'user_type' => $cust->user_type,'company'=>$cust->company, 'email'=>$cust->email], $this->successStatus);
							}
						}
						$secure_id = md5(microtime());
						$cust->appid = $secure_id;
						$cust->gsm_id = $gsm_id;
						$cust->save();
						return response()->json(['message' => $secure_id, 'user_type' => $cust->user_type], $this-> successStatus);
					}
                } else {
                    return response()->json(['message'=>'email or password is incorrect'], 401);
                }

            }
        }else{
            return response()->json(['message'=>'Unauthorised'], 401);
        }

        /*if(Auth::guard('customer')->attempt(['email' => request('email'), 'password' => bcrypt(request('password'))])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['success' => $success], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }*/
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function retail_login(Request $request){
        if(isset($request->API_KEY)){
            // Check email and password and redirect to the account

            if(!$this->validateAPI($request->API_KEY)){
                return response()->json(['message'=>'unauthorised API_KEY','code' => 401], 401);
            }
            $email = escape($request->email);
            $pass = md5($request->password);
            $gsm_id = $request->gcmid;
            if(empty($email) or empty($pass) or empty($gsm_id)){
                return response()->json(['message'=>'fill required fields','code' => 401], 200);
            } else {
                if(DB::table('customers')->where('email', $email)->orWhere('mobile', $email)->where('password', $pass)->count() > 0) {
                    $cust = Customer::where('email', $email)->orWhere('mobile', $email)->where('password', $pass)->first();

                    if($cust->user_type == 'customer'){

                        $secure_id = md5(microtime());
                        $name = $cust->name;
                        $cust->gsm_id = $gsm_id;
                        $cust->appid = $secure_id;
                        $cust->save();
                        return response()->json(['message' => 'you are successfully logged in', 'token' => $secure_id,'name'=>$name,'code' => 300], $this->successStatus);
                    }


                } else {
                    return response()->json(['message'=>'unauthorised user','code'=>401], 401);
                }

            }
        }else{
            return response()->json(['message'=>'API_KEY is required','code'=>401], 401);
        }

        /*if(Auth::guard('customer')->attempt(['email' => request('email'), 'password' => bcrypt(request('password'))])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['success' => $success], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }*/
    }
    public function register(Request $request)
    {
        if(isset($request->API_KEY)) {

            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'unauthorised'], 401);
            }

            if (!empty($request->name) && !empty($request->email) && !empty($request->password) && !empty($request->confirm_password) && !empty($request->cust_type) && !empty($request->mobile)) {
                // Escape the user entries and insert them to database
                $name = escape(htmlspecialchars($request->name));
                $company = escape(htmlspecialchars($request->company));
                $email = escape(htmlspecialchars($request->email));
                $user_type = escape(htmlspecialchars($request->cust_type));
                $country = '';
                $state = '';
                $city = '';
                if(isset($request->country)){
                    $country = escape(htmlspecialchars($request->country));
                }
                if(isset($request->state)){
                    $state = escape(htmlspecialchars($request->state));
                }
                if(isset($request->city)){
                    $city = escape(htmlspecialchars($request->city));
                }
                $password = md5($request->password);
                //$mobile = escape(htmlspecialchars($request->mobile));
                $id= false;
                if ($request->password != $request->confirm_password) {
                    return json_encode(["message" => "mismatch","code"=>200]);
                }
                if (DB::table('customers')->where('email', $email)->count()) {
                    return json_encode(["message" => "already registered","code"=>200]);
                } else {
                    if($request->cust_type=='institutional'){
                        $mobile = escape(htmlspecialchars($request->mobile));
                        $alt_mobileno = escape(htmlspecialchars($request->alt_mobileno));
                        if (DB::table('customers')->where('mobile', $mobile)->count()) {
                            return json_encode(["message" => "Mobile no already registered","code"=>200]);
                        }
                        $address_line_1 = escape(htmlspecialchars($request->address_line_1));
                        $gst = escape(htmlspecialchars($request->gst));
                        $cont_person_name = escape(htmlspecialchars($request->cont_person_name));
                        $cont_person_phone = escape(htmlspecialchars($request->cont_person_phone));

                        $id = DB::insert("INSERT INTO customers (name,email,password,user_type,sid,mobile,alternate_mobile,address_line_1,gst,cont_person_name,cont_person_phone,company,country,city,state) VALUE ('".$name."','".$email."','".$password."','".$user_type."','".md5(microtime())."','$mobile','$alt_mobileno','$address_line_1','$gst','$cont_person_name','$cont_person_phone','$company','".$country."','".$city."','".$state."')");
                    }
                    else{
                        $id = DB::insert("INSERT INTO customers (name,email,password,user_type,sid) VALUE ('".$name."','".$email."','".$password."','".$user_type."','".md5(microtime())."')");
                    }
                    if($id) {
                        $user = Customer::where('email', $email)->where('password', $password)->first();
                        $secure_id = md5(microtime());
                        $user->appid = $secure_id;
                        $user->save();
                        $success['code'] = 300;
                        $success['token'] = $secure_id;
                        $success['message'] = 'you are successfully registered';
                        $success['name'] = $user->name;
                        return response()->json($success, 200);
                    }else{
                        return response()->json(['message' => 'wrong','code'=>200]);
                    }
                }
            } else {
                return json_encode(["message" => "required","code"=>200]);
            }
        }else{
            return response()->json(['message'=>'unauthorised',"code"=>200], 401);
        }
    }
    /*public function register(Request $request)
    {
        if(isset($request->API_KEY)) {

            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'unauthorised'], 401);
            }

            if (!empty($request->name) && !empty($request->email) && !empty($request->password) && !empty($request->confirm_password) && !empty($request->cust_type) && !empty($request->mobile)) {
                // Escape the user entries and insert them to database
                $name = escape(htmlspecialchars($request->name));
                $company = escape(htmlspecialchars($request->company));
                $email = escape(htmlspecialchars($request->email));
                $user_type = escape(htmlspecialchars($request->cust_type));
				$country = '';
				$state = '';
				$city = '';
				if(isset($request->country)){
					$country = escape(htmlspecialchars($request->country));
				}
				if(isset($request->state)){
					$state = escape(htmlspecialchars($request->state));
				}
				if(isset($request->city)){
					$city = escape(htmlspecialchars($request->city));
				}
                $password = md5($request->password);
				//$mobile = escape(htmlspecialchars($request->mobile));
                $id= false;
                if ($request->password != $request->confirm_password) {
                    return json_encode(["message" => "mismatch","code"=>200]);
                }
                if (DB::table('customers')->where('email', $email)->count()) {
                    return json_encode(["message" => "already registered","code"=>200]);
                } else {
                    if($request->cust_type=='institutional'){
                        $mobile = escape(htmlspecialchars($request->mobile));
                        $alt_mobileno = escape(htmlspecialchars($request->alt_mobileno));
						if (DB::table('customers')->where('mobile', $mobile)->count()) {
							return json_encode(["message" => "Mobile no already registered","code"=>200]);
						}
                        $address_line_1 = escape(htmlspecialchars($request->address_line_1));
                        $gst = escape(htmlspecialchars($request->gst));
                        $cont_person_name = escape(htmlspecialchars($request->cont_person_name));
                        $cont_person_phone = escape(htmlspecialchars($request->cont_person_phone));

                        $id = DB::insert("INSERT INTO customers (name,email,password,user_type,sid,mobile,alternate_mobile,address_line_1,gst,cont_person_name,cont_person_phone,company,country,city,state) VALUE ('".$name."','".$email."','".$password."','".$user_type."','".md5(microtime())."','$mobile','$alt_mobileno','$address_line_1','$gst','$cont_person_name','$cont_person_phone','$company','".$country."','".$city."','".$state."')");
                    }
                    else{
                        $id = DB::insert("INSERT INTO customers (name,email,password,user_type,sid) VALUE ('".$name."','".$email."','".$password."','".$user_type."','".md5(microtime())."')");
                    }
                    if($id) {
                        $key = mt_rand(100000,999999);
                        $msg = "Your OTP for register ".$key;
                        $sms = urlencode($msg);
                        $no = urlencode($request->mobile);
                        $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=".$sms."&numbers=".$no."&unicode=0";
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FAILONERROR, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        $output = curl_exec($ch);
                        $error_msg = '';
                        if (curl_errno($ch)) {
                            $error_msg = curl_error($ch);
                        }
                        curl_close($ch);
                        $response['output'] = $error_msg;
                        $response['code'] = 300;
//                        $response['token'] = $secure_id;
                        //$success['message'] = 'you are successfully registered';
//                        $response['name'] = $user->name;
                        if($output) {
                            $user = Customer::where('email', $email)->where('password', $password)->first();
//                            $secure_id = md5(microtime());
                            $user->appid = $key;
                            $user->save();
                            $response['message'] = 'OTP sent successfully';
                            return response()->json($response, 200);
                        }else {
                            $response['message'] = 'Failed';
                            return response()->json($response, 200);
                        }
                    }else{
                        return response()->json(['message' => 'wrong','code'=>200]);
                    }
                }
            } else {
                return json_encode(["message" => "required","code"=>200]);
            }
        }else{
            return response()->json(['message'=>'unauthorised',"code"=>200], 401);
        }
    }*/

    public function verifyOTP(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $email = $request->mobile;
        $otp = $request->otp;
        if(empty($email)){
            $response['message'] = 'Mobile required';
            return response()->json($response, 200);
        } else {
            $c = DB::select("SELECT * FROM customers WHERE mobile = '".$email."'");
            if(count($c)){
                $customer = $c[0];
            }else{
                $c = DB::select("SELECT * FROM customers WHERE email = '".$email."'");
                if(count($c)){
                    $customer = $c[0];
                }else{
                    $response['message'] = 'This account does not exist';
                    $error = 'This account does not exist';
                    return response()->json($response, 200);
                }
            }
            if($customer->otp == $otp){
                $secure_id = md5(microtime());
                $customer->appid = $secure_id;
                $customer->save();
                $response['message'] = 'success';
                return response()->json($response, 200);
            }else{
                $response['message'] = 'failed';
                return response()->json($response, 200);
            }
        }
    }

    public function retail_register(Request $request)
    {
        if(isset($request->API_KEY)) {

            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message'=>'unauthorised API_KEY','code'=>401], 401);
            }

            if (!empty($request->name) && !empty($request->email) && !empty($request->password) && !empty($request->confirm_password) && !empty($request->cust_type) && !empty($request->mobile)) {
                // Escape the user entries and insert them to database
                $name = escape(htmlspecialchars($request->name));
                $email = escape(htmlspecialchars($request->email));
                $user_type = escape(htmlspecialchars($request->cust_type));
                $mobile = escape(htmlspecialchars($request->mobile));
                $password = md5($request->password);
                $id= false;
                if ($request->password != $request->confirm_password) {
                    return json_encode(["message" => "mismatch password",'code'=>401]);
                }
                if (DB::table('customers')->where('email', $email)->count()) {
                    return json_encode(["message" => "this emailid is already registered",'code'=>401]);
                } else {
                    if($request->cust_type=='customer'){

                        $id = DB::insert("INSERT INTO customers (name,email,password,user_type,sid,mobile) VALUE ('".$name."','".$email."','".$password."','".$user_type."','".md5(microtime())."','$mobile')");

                    }
                    else{
                        return response()->json(['message'=>'User type should be customer','code'=>401], 401);
                    }

                    if($id) {
                        $user = Customer::where('email', $email)->where('password', $password)->first();
                        $secure_id = md5(microtime());
                        $user->appid = $secure_id;
                        $user->save();
                        $success['code'] = 300;
                        $success['token'] = $secure_id;
                        $success['message'] = 'you are successfully registered';
                        $success['name'] = $user->name;
                        return response()->json($success , 200);
                    }else{
                        $wrong['code'] = 401;
                        $wrong['message']='wrong insert in database';
                        return response()->json($wrong, 401);
                    }
                }
            } else {
                return json_encode(['code'=>401,"message" => "fill all the required fields"]);
            }
        }else{
            return response()->json(['message'=>'API_KEY is required','code'=>401], 401);
        }
    }


    public function quickentry(Request $request)
    {
        if (!empty($request->name) && empty($request->id)) {
            // Escape the user entries and insert them to database
            $name = escape(htmlspecialchars($request->name));
            $company_name = escape(htmlspecialchars($request->company_name));
            $contact = escape(htmlspecialchars($request->mobile_no));
            $discription = escape(htmlspecialchars($request->discription));
            $location = escape(htmlspecialchars($request->location));
            $lat = escape(htmlspecialchars($request->lat));
            $lng = escape(htmlspecialchars($request->lng));
            $filex = '';
            $files = array();
            if($request->hasFile('image')) {
                $image = $request->image;
                /*if ($image != '') {
                    $image = str_replace(' ', '+', $image);
                    $imgdata = base64_decode($image);
                    $fname = $request->filename;
                    $ext = explode('.', $fname);
                    /*$f = finfo_open();
                    $ext = finfo_buffer($f, $imgdata, FILEINFO_EXTENSION);
                    finfo_close($f);

                    $filex = md5(time()) . '.' . $ext[1];

                    $path = base_path() . '/assets/crm/images/user/';
                    Image::make($image)->save($path . $filex);
                }*/
                foreach ($request->file('image') as $file){
                    $files[] = $filex = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().'/assets/crm/images/user/';
                    $img = Image::make($path.$filex)->save($path.$filex);
                    //$files[] = base64_encode(file_get_contents($file->path()));
                }
            }
            $filex = implode('<**>', $files);
            $created_by = escape(htmlspecialchars($request->token));
            $user = DB::table('user')->where('secure', $created_by)->first();
            if ($user !== null) {
                DB::insert("INSERT INTO crm_master_quick_entry (`name`,`company_name`,`contact`,`discription`,`location`,`image`,`created_by`, `lat`, `lng`) VALUE ('" . $name . "','" . $company_name . "', '" . $contact . "','" . $discription . "','" . $location . "','" . $filex . "','" . $user->u_id . "', '".$lat."', '".$lng."')");

                return response()->json(['response' => "Data Submitted"], 200);
            }else{
                return response()->json(['response' => 'Invalid Login!']);
            }
        }
        else if(!empty($request->name) && !empty($request->id)) {
            $customer = CrmCustomer::where('id', $request->id)->first();
            if($customer === null){
                // Escape the user entries and insert them to database
                $name = escape(htmlspecialchars($request->name));
                $company_name = escape(htmlspecialchars($request->company_name));
                $contact = escape(htmlspecialchars($request->mobile_no));
                $discription = escape(htmlspecialchars($request->discription));
                $location = escape(htmlspecialchars($request->location));
                $lat = escape(htmlspecialchars($request->lat));
                $lng = escape(htmlspecialchars($request->lng));
                $files = array();
                if($request->hasFile('image')) {
                    $image = $request->image;
                    /*if ($image != '') {
                        $image = str_replace(' ', '+', $image);
                        $imgdata = base64_decode($image);
                        $fname = $request->filename;
                        $ext = explode('.', $fname);
                        /*$f = finfo_open();
                        $ext = finfo_buffer($f, $imgdata, FILEINFO_EXTENSION);
                        finfo_close($f);

                        $filex = md5(time()) . '.' . $ext[1];

                        $path = base_path() . '/assets/crm/images/user/';
                        Image::make($image)->save($path . $filex);
                    }*/
                    foreach ($request->file('image') as $file){
                        $files[] = $filex = md5(time()).'.'.$file->getClientOriginalExtension();
                        $path = base_path().'/assets/crm/images/user/';
                        $img = Image::make($path.$filex)->save($path.$filex);
                        //$files[] = base64_encode(file_get_contents($file->path()));
                    }
                }
                $filex = implode('<**>', $files);
                $created_by = escape(htmlspecialchars($request->token));
                $user = DB::table('user')->where('secure', $created_by)->first();
                if ($user !== null) {
                    DB::insert("INSERT INTO crm_master_quick_entry (`name`,`company_name`,`contact`,`discription`,`location`,`image`,`created_by`, `lat`, `lng`) VALUE ('" . $name . "','" . $company_name . "', '" . $contact . "','" . $discription . "','" . $location . "','" . $filex . "','" . $user->u_id . "', '".$lat."', '".$lng."')");

                    return response()->json(['response' => "Data Submitted"], 200);
                }else{
                    return response()->json(['response' => 'Invalid Login!']);
                }
            }else{
                $name = escape(htmlspecialchars($request->name));
                $company_name = escape(htmlspecialchars($request->company_name));
                $contact = escape(htmlspecialchars($request->mobile_no));
                $discription = escape(htmlspecialchars($request->discription));
                $lat = escape(htmlspecialchars($request->lat));
                $lng = escape(htmlspecialchars($request->lng));
                $customer->name = $name;
                if($contact != '')
                    $customer->contact_no = $contact;
                if($company_name != '')
                    $customer->proprieter_name = $company_name;
                if($lat != '')
                    $customer->lat = $lat;
                if($lng != '')
                    $customer->lng = $lng;

                $customer->save();

                $created_by = escape(htmlspecialchars($request->token));
                $user = DB::table('user')->where('secure', $created_by)->first();

                $files = array();
                if($request->hasFile('image')) {
                    foreach ($request->file('image') as $file){
                        $files[] = $filex = md5(time()).'.'.$file->getClientOriginalExtension();
                        $path = base_path().'/assets/crm/images/visits/';
                        $img = Image::make($path.$filex)->save($path.$filex);
                    }
                }
                $filex = implode('<**>', $files);
                DB::insert("INSERT INTO crm_visits(`crm_customer_id`, `name`, `phone_no`, `address`, `select_date`, `description`, `user_id`, `image`, `customer_status`) VALUE ('".$customer->id."', '".$customer->name."', '".$customer->contact_no."', '".$customer->postal_address."', '".date('Y-m-d')."', '".$discription."', '".$user->u_id."', '".$filex."', '".$customer->class."')");
                return response()->json(['response' => "Data Submitted"], 200);
            }
        }else{
            return response()->json(['response' => 'Something went wrong. Try again!']);
        }
    }



    public function check_login(Request $request)
    {

        if (isset($request->token)) {
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('user')->where('secure',$token)->first();
            $version_code = 1.0;
//            return $user->id;
//            return response()->json(['message' => $user], 200);
            if ($user === null) {
                return response()->json(['message' => 'please login' ,'version_code'=>$version_code,'code'=>401], 200);
            } else {


                return response()->json(['message' => 'you are already logged in','version_code'=>$version_code, 'code' =>300], 200);
            }
        }
    }
    public function check_customer(Request $request)
    {
        $version_code = 1.0;
        if (isset($request->API_KEY)) {

            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'unauthorised API_KEY', 'code' => 401], 401);
            }

            if (isset($request->token)) {
                $user = Customer::where('appid', $request->token)->first();

//            return $user->id;
//            return response()->json(['message' => $user], 200);
                if ($user === null) {
                    return response()->json(['message' => 'please login', 'version_code' => $version_code, 'company' => '', 'code' => 401], 200);
                } else {


                    return response()->json(['message' => 'you are already logged in', 'version_code' => $version_code, 'company' => $user->company, 'code' => 300], 200);
                }
            } else {


                return response()->json(['message' => 'token is reqiured', 'version_code' => $version_code, 'code' => 300], 200);
            }
        }
    }




    public function customers(Request $request){
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('user')->where('secure', $token)->first();
            if($user === null){
                return response()->json(['response' => 'Unauthorised'], 401);
            }else{
                $cdata = array();
                if(isset($request->search)) {
                    $cdata = CrmCustomer::select(['id', 'name', 'proprieter_name as company', 'contact_no as contact'])->where('name', 'LIKE', '%'.$request->search.'%')->orWhere('proprieter_name','LIKE', '%'.$request->search.'%')->orWhere('contact_no', 'LIKE', '%'.$request->search.'%')->get()->toArray();
                }
                return response()->json(['response' => $cdata], 200);
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
    }

    public function call_log_search(Request $request){
        if(isset($request->token)){
            $user = Customer::where('appid', $request->token)->first();
            if($user === null){
                return response()->json(['response' => 'Unauthorised'], 401);
            }else{
                $cdata = array();
                if(isset($request->search)) {
                    $cdata = CrmCustomer::select(['id', 'name', 'proprieter_name as company', 'contact_no as contact'])->where('name', 'LIKE', '%'.$request->search.'%')->orWhere('proprieter_name','LIKE', '%'.$request->search.'%')->orWhere('contact_no', 'LIKE', '%'.$request->search.'%')->get()->toArray();
                }
                return response()->json(['response' => $cdata], 200);
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function user(Request $request)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised'], 401);
            }
            if (isset($request->token)) {
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login'], 200);
                }
                $ret = array(
                    'name' => $user->name,
                    'email' => $user->email,
                    'mobile' => $user->mobile,
                    'image' => url('assets/user_image/'.$user->image),
                    'alt_mobile' => $user->alternate_mobile,
                    'company' => $user->company,
                    'address_line_1' => $user->address_line_1,
                    'address_line_2' => $user->address_line_2,
                    'city' => getCityName($user->city),
                    'state' => getStateName($user->state),
                    'country' => getCountryName($user->country),
                    'country_id' => $user->country,
                    'state_id' => $user->state,
                    'city_id' => $user->city,
                    'postcode' => $user->postcode,
                    'contact_person' => $user->cont_person_name,
                    'contact_email' => $user->cont_person_email,
                    'contact_phone' => $user->cont_person_phone,
                    'contact_designation' => $user->cont_person_designation,
                    'pan' => $user->pan,
                    'gst' => $user->gst
                );
                return response()->json(['message' => $ret], 200);
            }else{
                return response()->json(['message' => 'login'], 200);
            }
        }
        return response()->json(['message' => 'Unauthorised'], 401);
    }

    public function call_log(Request $request)
    {
        date_default_timezone_set('Asia/Kolkata');
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
            }
            if (isset($request->token)) {
                $id = false;
                $token = escape(htmlspecialchars($request->token));
                $user = DB::table('user')->where('secure', $token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login', 'code' => 401], 200);
                }
                $res = json_decode(str_replace("\\", "", $request->response));

                $current_date = date('Y-m-d H:i:s');
                $crm = array();
                $cids = array();
                if(!empty($res)){

                    foreach($res as $value){
                        if(isset($value->contact_no) && isset($value->date) && isset($value->time) && isset($value->callType)){

                            $customers = DB::table('crm_customers')->whereRaw('FIND_IN_SET(?, contact_no)', $value->contact_no)->first();
                            if($customers!==null){

                                $customer_call = DB::table('crm_call')->where('crm_customer_id',$customers->id)->where('select_date', $value->date)->where('time', $value->time)->first();

                                if($customers !== null && $customer_call === null && $value->callType!=='3') {
                                    $cids[] = $customers->id;
                                    $crm[] = array(
                                        'crm_customer_id' => $customers->id,
                                        'customer_number' => $value->contact_no,
                                        'select_date' => $value->date,
                                        'time' => $value->time,
                                        'message' => 'call log',
                                        'user_id' => $user->u_id,
                                        'duration' => $value->duration,
                                        'call_type' => $value->callType,
                                        'created_date' => $current_date
                                    );
                                }
                            }

                        }
                    }
                }
                DB::table('crm_call')->insert($crm);

                return response()->json(['message' => 'entry successfully in call log', 'code' => 200], 200);
            }
            else {
                return response()->json(['message' => 'login', 'code' => 401], 200);
            }
        }

        return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
    }

    public function retail_nav(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'Unauthorised','code' => 401], 401);
            }
            $notifications = 0;
            $cart_count = 0;
            $orders_count = 0;
            if(isset($request->token)){
                $user = Customer::where('appid', $request->token)->first();
                $notification_read = DB::table('notification_read')->where('customer_id','=',$user->id)->first();
                $cart = DB::table('cart')->where('user_id','=',$user->id)->where('status',0)->get();
                $my_orders = DB::table('orders')->where('customer','=',$user->id)->get();
                if($notification_read!==null){
                    $notifications = $notification_read->note_read;
                }
                if(count($cart)!==0){
                    $cart_count = count($cart);
                }
                if(count($my_orders)!==0){
                    $orders_count = count($my_orders);
                }
            }
            $response = array();
            $pcats = DB::table('category')->where('parent', 0)->orderBy('name', 'ASC')->get();
            $dcats = DB::table('design_category')->where('parent', 0)->orderBy('name', 'ASC')->get();
            $acats = DB::table('advices_category')->where('parent', 0)->orderBy('name', 'ASC')->get();
            $scats = DB::table('services_category')->where('parent', 0)->orderBy('name', 'ASC')->get();
            if ($pcats !== null) {
                foreach ($pcats as $pcat) {
                    $pscats = getProductCategories($pcat->id);
                    $sub = array();
//                    $sub[] = array();
                    if ($pscats) {
                        foreach ($pscats as $pscat) {
                            $sub[] = array(
                                'id' => $pscat->id,
                                'name' => $pscat->name
                            );
                        }
                    }
                    $response['products'][] = array(
                        'id' => $pcat->id,
                        'name' => $pcat->name,
                        'sub' => $sub

                    );
                }
            }
            if ($dcats !== null) {
                foreach ($dcats as $dcat) {
                    $dscats = DB::table('design_category')->where('parent', $dcat->id)->orderBy('name', 'ASC')->get();
                    $sub_design = array();
//                    $sub[] = array();
                    if ($dscats) {
                        foreach ($dscats as $dscat) {
                            $sub_design[] = array(
                                'id' => $dscat->id,
                                'name' => $dscat->name
                            );
                        }
                    }
                    $response['designs'][] = array(
                        'id' => $dcat->id,
                        'name' => $dcat->name,
                        'sub' => $sub_design

                    );
                }
            }
            if ($acats !== null) {
                foreach ($acats as $acat) {
                    $ascats = DB::table('advices_category')->where('parent', $acat->id)->orderBy('name', 'ASC')->get();
                    $sub_advices = array();
//                    $sub[] = array();
                    if ($ascats) {
                        foreach ($ascats as $ascat) {
                            $sub_advices[] = array(
                                'id' => $ascat->id,
                                'name' => $ascat->name
                            );
                        }
                    }
                    $response['advices'][] = array(
                        'id' => $acat->id,
                        'name' => $acat->name,
                        'sub' => $sub_advices

                    );
                }
            }
            if ($scats !== null) {
                foreach ($scats as $scat) {
                    $sscats = DB::table('services_category')->where('parent', $scat->id)->orderBy('name', 'ASC')->get();
                    $sub_advices = array();
//                    $sub[] = array();
                    if ($sscats) {
                        foreach ($sscats as $sscat) {
                            $sub_services[] = array(
                                'id' => $sscat->id,
                                'name' => $sscat->name
                            );
                        }
                    }
                    $response['services'][] = array(
                        'id' => $scat->id,
                        'name' => $scat->name,
                        'sub' => $sub_services

                    );
                }
            }

            return response()->json(['code' => "300",'message' => 'success','notifications'=>$notifications,'cart'=>$cart_count,'my_orders'=>$orders_count,'nav'=>[$response]]);
        } else {
            return response()->json(['error' => 'Unauthorised','code' => 401], 401);
        }
    }

    //Retail API Category
    public function getretailCategory(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
            $customer_id = '';
            $user_type = '';
            if (isset($request->token)) {
                $customer_id = $request->token;

            }
            if (isset($request->cid)) {
                $cat = $request->cid;
                $category = DB::table('category')->where('id', $cat)->first();
                if ($category === null) {
                    return response()->json(['response' => 'Category Not Found'], 404);
                }
                $response['show_filter'] = false;
                $response['show_products'] = false;
                $response['suggested'] = [];

                $response['title'] = translate($category->name);
                if ($category->banner !== '') {
                    $response['banner'] = url('assets/products/' . $category->banner);
                } else {
                    $response['banner'] = url('assets/images/background/2.jpg');
                }
                $pdata = DB::table('category')->where('parent', $category->id)->count();
                if ($pdata == 0) {
                    $response['show_products'] = true;
                }
                $top_brands = array();
                if ($category->brands !== '') {
                    $tpbrands = explode(',', $category->brands);
                    $top_brands = DB::table('brand')->whereIn('id', $tpbrands)->orderBy('name', 'ASC')->get();
                }

                foreach ($top_brands as $tb) {
                    $response['top_brands'][] = array(
                        'id' => $tb->id,
                        'image' => url('assets/brand/' . $tb->image)
                    );
                }
                $p_categories = DB::table('category')->where('parent', $category->id)->get();
                $cats = DB::select('SELECT * FROM category WHERE parent = 0 ORDER BY name ASC');
                $response['navigation'] = array();
                foreach ($cats as $p_cat) {
                    $response['navigation'][] = array(
                        'id' => $p_cat->id,
                        'title' => $p_cat->name,
                    );
                }
                $response['p_categories'] = array();
                foreach ($p_categories as $p_category) {
                    $response['p_categories'][] = array(
                        'id' => $p_category->id,
                        'title' => $p_category->name,
                        'image' => url('/assets/products/' . image_order($p_category->image))
                    );
                }
                $bests = DB::table('best_for_category')->where('category', $category->id)->orderBy('id', 'DESC')->get();
                $response['bests'] = [];
                foreach ($bests as $best) {
                    $response['bests'][] = array(
                        'id' => $best->id,
                        'image' => url('/assets/products/' . image_order($best->image)),
                        'content' => $best->content
                    );
                }
                $faqs = DB::select("SELECT * FROM product_faq WHERE section='retail' AND FIND_IN_SET (" . $category->id . ", category) ORDER BY id DESC LIMIT 5");
                foreach ($faqs as $faq) {
                    $response['faqs'][] = array(
                        'id' => $faq->id,
                        'question' => $faq->question,
                        'date' => date('d M, Y', strtotime($faq->time)),
                        'answer' => $faq->answer
                    );
                }
                $filters = false;
                $cfc = $category->filter;
                if ($cfc !== '') {
                    $ccx = explode(',', $cfc);
                    foreach ($ccx as $cx) {
                        $filters = DB::select("SELECT * FROM filter WHERE id = '$cx'");
                    }
                    //dd($filters);
                } else {
                    $filters = false;
                }
                if ($filters) {
                    foreach ($filters as $filter) {
                        $fos = array();
                        $fil_options = DB::table('filter_options')->where('filter_id', $filter->id)->get();
                        foreach ($fil_options as $fo) {
                            $fos[] = array(
                                'id' => $fo->id,
                                'name' => $fo->name
                            );
                        }
                        $response['filters'][] = array(
                            'id' => $filter->id,
                            'name' => $filter->name,
                            'options' => $fos
                        );
                    }
                }
                $cid[] = $category->id;
                //Kd codes
                $c = array();
                $cont = true;
                $temp = $cid;
                while ($cont) {
                    $c = array_pluck(DB::table('category')->whereIn('parent', $temp)->get(), 'id');
                    if (count($c)) {
                        $cid = array_merge($cid, $c);
                        $temp = $c;
                    } else {
                        $cont = false;
                    }
                }
                $products = DB::table('products')->whereIn('category', $cid);
                $price['set_min'] = 0;
                $price['set_max'] = 0;

                $p_min = DB::select("SELECT MIN(price) as min_price, MAX(price) as max_price FROM products where category IN (" . implode(',', $cid) . ")");
                if (count($p_min) > 0) {
                    $price['min'] = $p_min[0]->min_price;
                    $price['set_min'] = $p_min[0]->min_price;
                    $price['max'] = $p_min[0]->max_price;
                    $price['set_max'] = $p_min[0]->max_price;
                } else {
                    $price['min'] = 0;
                    $price['set_min'] = 0;
                    $price['max'] = 0;
                }
                $bids = array();
                if (isset($request->brands)) {
                    $bids = explode(',', $request->brands);
                }
                if (isset($request->min)) {
                    $price['set_min'] = $request->min;
                    $products = $products->where('price', '>=', $request->min);
                }
                if (isset($request->max)) {
                    $price['set_max'] = $request->max;
                    $products = $products->where('price', '<=', $request->max);
                }
                if (count($bids) > 0) {
                    $products = $products->whereIn('brand_id', $bids);
                }
                $products = $products->get();
                $response['products'] = array();
                $pvalidity = 0;
                $hub='';
                foreach ($products as $product) {
                    $pr = getIndividual($product->id);

                    if($product->validity!==null){
                        date_default_timezone_set("Asia/Kolkata");

                        $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));


                        if($pvalidity <= 0){
                            $pvalidity = 0;
                        }
                    }

                    if(is_array($pr)) {
                        $response['products'][] = array(
                            'id' => $product->id,
                            'title' => $product->title,
                            'image' => url('/assets/products/' . image_order($product->images)),
                            'price' => round($pr['price']),
                            'loading' => round($pr['loading']),
                            'tax_per' => $product->tax,
                            'tax' => $pr['tax'],
                            'amount' => round($pr['total']),
                            'rates' => getRatings($product->id),
                            'validity' => $pvalidity
                        );
                    }
                }
                $ladvices = DB::select("SELECT * FROM blog WHERE FIND_IN_SET (".$category->id.", pro_cat) AND section='retail' ORDER BY id DESC LIMIT 4");
                foreach($ladvices as $ladvice){
                    $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$ladvice->id)[0]->count;
                    $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$ladvice->id)[0]->count;
                    $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$ladvice->id)[0]->count;
                    $rating = 0;
                    if ($total_ratings > 0){
                        $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$ladvice->id)[0]->sum;
                        $rating = $rating_summ / $total_ratings;
                    }
                    $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$ladvice->id." AND active = 1 ORDER BY time DESC");
                    $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$ladvice->id."'")[0];
                    $total_rating =  $rating->total_rating;
                    $total_user = $rating->total_user;
                    if($total_rating==0){
                        $avg_rating = 0;
                    }
                    else{
                        $avg_rating = round($total_rating/$total_user,1);
                    }
                    $response['like'] = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$ladvice->id)[0]->count;
                    $response['rating'] = 0;
                    if ($total_ratings > 0){
                        $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$ladvice->id)[0]->sum;
                        $response['rating'] = $rating_summ / $total_ratings;
                    }
                    $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$ladvice->id."'")[0];
                    $total_rating =  $rating->total_rating;
                    $total_user = $rating->total_user;
                    if($total_rating==0){
                        $avg_rating = 0;
                    }
                    else{
                        $avg_rating = round($total_rating/$total_user,1);
                    }
                    $rating1= DB::select("SELECT count(id) as rating1_user FROM advices_reviews WHERE rating = '1' AND advice_id = '".$ladvice->id."' AND active = '1'")[0];
                    $rating2 = DB::select("SELECT count(id) as rating2_user FROM advices_reviews WHERE rating = '2' AND advice_id = '".$ladvice->id."' AND active = '1'")[0];
                    $rating3 = DB::select("SELECT count(id) as rating3_user FROM advices_reviews WHERE rating = '3' AND advice_id = '".$ladvice->id."' AND active = '1'")[0];
                    $rating4 = DB::select("SELECT count(id) as rating4_user FROM advices_reviews WHERE rating = '4' AND advice_id = '".$ladvice->id."' AND active = '1'")[0];
                    $rating5 = DB::select("SELECT count(id) as rating5_user FROM advices_reviews WHERE rating = '5' AND advice_id = '".$ladvice->id."' AND active = '1'")[0];
                    $response['ladvices'][] = array(
                        'id' => $ladvice->id,
                        'title' => $ladvice->title,
                        'image' => url('/assets/blog/'.$ladvice->images),
                        'date' => date('d M, Y', strtotime($ladvice->time)),
                        'answer' => $ladvice->short_des,
                        'visits' => $ladvice->visits,
                        'total_reviews' => $total_reviews,
                        'total_rating' => $avg_rating,
                        'likes' => $like,
                        'rating1' => $rating1,
                        'rating2' => $rating2,
                        'rating3' => $rating3,
                        'rating4' => $rating4,
                        'rating5' => $rating5,
                    );
                }
                $tadvices = DB::select("SELECT * FROM blog WHERE FIND_IN_SET (".$category->id.", pro_cat) AND section='retail' ORDER BY visits DESC LIMIT 4");
                foreach($tadvices as $tadvice){
                    $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$tadvice->id)[0]->count;
                    $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$tadvice->id)[0]->count;
                    $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$tadvice->id)[0]->count;
                    $rating = 0;
                    if ($total_ratings > 0){
                        $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$tadvice->id)[0]->sum;
                        $rating = $rating_summ / $total_ratings;
                    }
                    $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$tadvice->id." AND active = 1 ORDER BY time DESC");
                    $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$tadvice->id."'")[0];
                    $total_rating =  $rating->total_rating;
                    $total_user = $rating->total_user;
                    if($total_rating==0){
                        $avg_rating = 0;
                    }
                    else{
                        $avg_rating = round($total_rating/$total_user,1);
                    }
                    $response['like'] = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$tadvice->id)[0]->count;
                    $response['rating'] = 0;
                    if ($total_ratings > 0){
                        $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$tadvice->id)[0]->sum;
                        $response['rating'] = $rating_summ / $total_ratings;
                    }
                    $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$tadvice->id."'")[0];
                    $total_rating =  $rating->total_rating;
                    $total_user = $rating->total_user;
                    if($total_rating==0){
                        $avg_rating = 0;
                    }
                    else{
                        $avg_rating = round($total_rating/$total_user,1);
                    }
                    $rating1= DB::select("SELECT count(id) as rating1_user FROM advices_reviews WHERE rating = '1' AND advice_id = '".$tadvice->id."' AND active = '1'")[0];
                    $rating2 = DB::select("SELECT count(id) as rating2_user FROM advices_reviews WHERE rating = '2' AND advice_id = '".$tadvice->id."' AND active = '1'")[0];
                    $rating3 = DB::select("SELECT count(id) as rating3_user FROM advices_reviews WHERE rating = '3' AND advice_id = '".$tadvice->id."' AND active = '1'")[0];
                    $rating4 = DB::select("SELECT count(id) as rating4_user FROM advices_reviews WHERE rating = '4' AND advice_id = '".$tadvice->id."' AND active = '1'")[0];
                    $rating5 = DB::select("SELECT count(id) as rating5_user FROM advices_reviews WHERE rating = '5' AND advice_id = '".$tadvice->id."' AND active = '1'")[0];
                    $response['tadvices'][] = array(
                        'id' => $tadvice->id,
                        'title' => $tadvice->title,
                        'image' => url('/assets/blog/'.$tadvice->images),
                        'date' => date('d M, Y', strtotime($tadvice->time)),
                        'description' => $tadvice->short_des,
                        'visits' => $tadvice->visits,
                        'total_reviews' => $total_reviews,
                        'total_rating' => $avg_rating,
                        'likes' => $like,
                        'rating1' => $rating1,
                        'rating2' => $rating2,
                        'rating3' => $rating3,
                        'rating4' => $rating4,
                        'rating5' => $rating5,
                    );
                }
                $sg = DB::select("SELECT product_id from featured_products where category_id = ".$category->id);
                //dd($sg);
                $response['suggested'] = array();
                if (count($sg) > 0) {
                    $sug_prods = $sg[0]->product_id;
                    $suggested = DB::select("SELECT * FROM products WHERE id in (".$sug_prods.")");
                    foreach ($suggested as $sg) {
                        $prsg = getIndividual($sg->id);
                        if(is_array($prsg)) {
                            $response['suggested'][] = array(
                                'id' => $sg->id,
                                'title' => $sg->title,
                                'image' => url('/assets/products/' . image_order($sg->images)),
                                'price' => round($prsg['price']),
                                'loading' => round($prsg['loading']),
                                'tax_per' => $sg->tax,
                                'tax' => $prsg['tax'],
                                'amount' => round($prsg['total']),
                                'rates' => getRatings($sg->id)
                            );
                        }
                    }
                }
                $brands = DB::select("SELECT * FROM brand where id In (" . $category->brands . ") ORDER BY id DESC ");
                foreach ($brands as $brand) {
                    $response['f_brands'][] = array(
                        'id' => $brand->id,
                        'name' => $brand->name
                    );
                }

                return response()->json($response, 200);
            } else {
                return response()->json(['error' => 'Category Not Found'], 404);
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }



    //Retail Api Products
    public function getretailProduct(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
            $customer_id = '';
            $hub = '';
            if (isset($request->token)) {
                $user = Customer::where('appid', $request->token)->first();
                if($user!==null){
                    $customer_id = $user->id;
                    $rcheck = DB::select("SELECT COUNT(*) as count FROM recently_viewed_product WHERE user_id = ".$customer_id." AND product_id = ".$request->pid)[0]->count;
                    if($rcheck == 0) {
                        $rvp = DB::insert("INSERT INTO recently_viewed_product (user_id, product_id) VALUES ('" . $customer_id . "','" . $request->pid . "')");
                    }
                }
            }
            if (isset($request->pid)) {

                $product = DB::table('products')->where('id', $request->pid)->first();
                $cat = DB::table('category')->where('id', '=', $product->category)->first();
                $wunits = DB::table('units')->whereIn('id', explode(',', $cat->weight_unit))->get();
                if ($product === null) {
                    return response()->json(['error' => 'Invalid product id'], 404);
                } else {
                    $category = DB::table('category')->where('id', $product->category)->first();
                    $variants = DB::table("product_variants")->where('display', '1')->where('product_id', $product->id)->orderBy('variant_title', 'ASC')->get();
                    $bulk_discounts = DB::table("product_discount")->where('product_id', $product->id)->orderBy('quantity', 'ASC')->get();

                    $fb = array();
                    if($product->fb_cat !== '') {
                        $fb_cats = explode(',', $product->fb_cat);
                        foreach($fb_cats as $fb_cat) {
                            $pro = null;
                            $pro = getProduct($fb_cat, $product->id);
                            if ($pro !== null) {
								//Rating Calculations
								$tr = DB::table("reviews")->where('active', 1)->where('product', $pro->id)->count();
								$r = 0;
								if ($tr > 0) {
									$r_summ = DB::table("reviews")->where('active', 1)->where('product', $pro->id)->sum('rating');
									$r = $r_summ / $tr;
								}

								$t_reviews = DB::table("reviews")->where('active', 1)->where('review', '<>', '')->where('product', $pro->id)->count();
								$ratingax = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM reviews WHERE active = '1' AND product = '" . $pro->id . "'")[0];

								$t_rating = $ratingax->total_rating;
								$t_user = $ratingax->total_user;
								$avg = 0;
								if($t_rating === null){
									$t_rating = 0;
								}
								if ($t_rating == 0) {
									$avg = 0;
								} else {
									$avg = round($t_rating / $t_user, 1);
								}
								//END Rating Calculations
								
                                $fb[] = array(
                                    'id' => $pro->id,
                                    'sku' => $pro->sku,
                                    'title' => $pro->title,
                                    'category' => $pro->category,
                                    'retail_price' => $pro->price,
                                    'images' => url('/assets/products/' . image_order($pro->images)),
                                    'text' => $pro->text,
                                    'status' => $pro->status,
									'rating' => $r,
									'total_rating' => intval($t_rating),
									'average_rating' => $avg
                                );
                            }
                        }
                    }

                    $total_ratings = DB::table("reviews")->where('active', 1)->where('product', $product->id)->count();
                    $rating = 0;
                    if ($total_ratings > 0) {
                        $rating_summ = DB::table("reviews")->where('active', 1)->where('product', $product->id)->sum('rating');
                        $rating = $rating_summ / $total_ratings;
                    }

                    $total_reviews = DB::table("reviews")->where('active', 1)->where('review', '<>', '')->where('product', $product->id)->count();
                    $ratinga = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM reviews WHERE active = '1' AND product = '" . $product->id . "'")[0];

                    $total_rating = $ratinga->total_rating;
                    $total_user = $ratinga->total_user;
                    if ($total_rating == 0) {
                        $avg_rating = 0;
                    } else {
                        $avg_rating = round($total_rating / $total_user, 1);
                    }
                    $rating1 = DB::select("SELECT count(id) as rating1_user FROM reviews WHERE rating = '1' AND product = '" . $product->id . "' AND active = '1'")[0];
                    $rating2 = DB::select("SELECT count(id) as rating2_user FROM reviews WHERE rating = '2' AND product = '" . $product->id . "' AND active = '1'")[0];
                    $rating3 = DB::select("SELECT count(id) as rating3_user FROM reviews WHERE rating = '3' AND product = '" . $product->id . "' AND active = '1'")[0];
                    $rating4 = DB::select("SELECT count(id) as rating4_user FROM reviews WHERE rating = '4' AND product = '" . $product->id . "' AND active = '1'")[0];
                    $rating5 = DB::select("SELECT count(id) as rating5_user FROM reviews WHERE rating = '5' AND product = '" . $product->id . "' AND active = '1'")[0];
                    $imagesx = explode(',', $product->images);
                    $images = array();
                    foreach ($imagesx as $imagex) {
                        $images[] = array(
                            'image' => url('/assets/products/' . $imagex),
                            'thumb' => url('/assets/products/thumbs/' . $imagex)
                        );
                    }

                    //$reviews = DB::table("reviews")->where('product', $product->id)->where('active', 1)->orderBy('time', 'DESC')->get();
                    $reviews = DB::select("SELECT * FROM reviews where product='" . $product->id . "' AND  active=1 order by time DESC");
                    $related_products = getRelatedProducts($product->category, $product->id);
                    $rp = array();
                    foreach ($related_products as $related_product) {
                        $rp_price = getIndividual($related_product->id);
                        $total_related = round($rp_price['total']);
						
						//Rating Calculations
						$tr = DB::table("reviews")->where('active', 1)->where('product', $related_product->id)->count();
						$r = 0;
						if ($tr > 0) {
							$r_summ = DB::table("reviews")->where('active', 1)->where('product', $related_product->id)->sum('rating');
							$r = $r_summ / $tr;
						}

						$t_reviews = DB::table("reviews")->where('active', 1)->where('review', '<>', '')->where('product', $related_product->id)->count();
						$ratingax = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM reviews WHERE active = '1' AND product = '" . $related_product->id . "'")[0];

						$t_rating = $ratingax->total_rating;
						$t_user = $ratingax->total_user;
						$avg = 0;
						if($t_rating === null){
							$t_rating = 0;
						}
						if ($t_rating == 0) {
							$avg = 0;
						} else {
							$avg = round($t_rating / $t_user, 1);
						}
						//END Rating Calculations
                        if($rp_price != 'NA') {
                            $rp[] = array(
                                'id' => $related_product->id,
                                'sku' => $related_product->sku,
                                'title' => $related_product->title,
                                'category' => $related_product->category,
                                'retail_price' => $total_related,
                                'images' => url('/assets/products/' . image_order($related_product->images)),
                                'text' => $related_product->text,
                                'status' => $related_product->status,
                                'rating' => $r,
                                'total_rating' => intval($t_rating),
                                'average_rating' => $avg
                            );
                        }
                    }
                    $sellers = DB::select("SELECT c.*,p.* FROM `products` as p INNER JOIN customers as c on p.added_by = c.id WHERE p.sku = '$product->sku' limit 2");
                    $faqs = DB::select("SELECT * FROM product_faq WHERE section='institutional' AND status = 1 ORDER BY id DESC");
                    $link = DB::table('link')->where('page', 'Single Product')->first();
                    if ($link !== null) {
                        $link->image = url('assets/products/' . $link->image);
                    }
                    $pr_individual = getIndividual($product->id);
                    $product->retail_price = round($pr_individual['total']);
                    $product->sale_price = round($pr_individual['total_sale']);
                    $product->price_detail = getIndividual($product->id);
                    $cart = DB::table('cart')->where('user_id', $customer_id)->where('product_id',$request->pid)->where('status', 0)->first();
                    $added_to_cart = 'No';
                    if($cart!==null){
                        $added_to_cart = 'Yes';
                    }
                    $vr = [];
                    $final_quantity =0;
                    foreach ($variants as $variant){
                        $varianttitle = DB::table('size')->where('id',$variant->variant_title)->first();
                        if($cart){
                            $cart_variants = (array)json_decode($cart->variants);
                            foreach($cart_variants as $key=>$value){
                                $val =(array)json_decode($value);
                                foreach($val as $key=>$va){
//                                   dd($variant->id);
                                    if($key==$variant->id){
                                        $final_quantity = $va->quantity;
                                        continue;
                                    }
                                }
                            }
                        }
                        $var_price = getIndividual($product->id,$variant->id);
                        $variant->variant_price = round($var_price['var_price']);
                        $variant->variant_title = $varianttitle->name;
                        $variant->price = round($var_price['total']);
                        $variant->sale_price = round($var_price['total_sale']);
                        $variant->price_detail = getIndividual($product->id,$variant->id);
                        $variant->final_quantity = $final_quantity;
                        if($cart !==null){
                            $cart_units = (array)json_decode($cart->unit);
                            if($variant->final_quantity!==''){
                                foreach($cart_units as $k => $unit){
                                    if($k == $variant->id){
                                        $final_unit = $unit;
                                    }

                                }
                            }
                            else{
                                $final_unit = '';
                            }
                        }
                        else{
                            $final_unit = '';
                        }
                        $variant->final_unit = $final_unit;
                        $vr[] = (array)$variant;
                    }
                    $unit_cat =array();
                    foreach ($wunits as $unit){
                        $unit_cat[] = array(
                            'id' => $unit->id,
                            'symbol' => $unit->symbol
                        );

                    }
                    $sp = 0;
                    $pvalidity = 0;
                    if(isset($request->token)){
                        $user = Customer::where('appid', $request->token)->first();
                        if ($user === null) {
                            return response()->json(['message' => 'login'], 401);
                        }
                        if($user->show_price==1 && $product->product_showprice==1){
                            $sp =1;
                        }
                        else{
                            $sp = 0;
                        }
                        if($user->user_type!== "institutional" && $product->validity!==null){
                            date_default_timezone_set("Asia/Kolkata");

                            $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));


                            if($pvalidity <= 0){
                                $pvalidity = 0;
                            }
                        }
                    }

                    $response = array(
                        'product' => (array)$product,
                        'category' => (array)$category,
                        'variants' => (array)$vr,
                        'rating' => $rating,
                        'total_rating' => $total_ratings,
                        'average_rating' => $avg_rating,
                        'rating_list' => array('rate1' => (array)$rating1, 'rate2' => (array)$rating2, 'rate3' => (array)$rating3, 'rate4' => (array)$rating4, 'rate5' => (array)$rating5),
                        'images' => (array)$images,
                        'reviews' => (array)$reviews,
                        'related_products' => (array)$rp,
                        'frequently_bought' => (array)$fb,
                        'sellers' => (array)$sellers,
                        'faqs' => (array)$faqs,
                        'link' => (array)$link,
                        'show_price' => $sp,
                        'unit' => $unit_cat,
                        'validity'=>$pvalidity,
                        'added_to_cart'=>$added_to_cart
                    );
                    return response()->json($response, 200);
                }
            } else {
                return response()->json(['error' => 'Check api data'], 500);
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    //Retail Products By Catid

    public function getretailProductsByCat(Request $request)
    {
//        dd($request->API_KEY);
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'unautorised'], 401);
            }
            $ret = array();

//    dd($request->token);
            $cid  = explode(',', $request->cat_id);

            $brands = DB::table('products')->whereIn('category',$cid)->get();


            foreach ($brands as $brand) {


                $ret[] = array(

                    'id' => $brand->id,
                    'name' => $brand->title
                );
            }
            return response()->json(['message' => $ret], 200);

        } else {
            return response()->json(['error' => 'unautorised'], 401);
        }

    }


    /**
     * Update UserProfile api
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised','code'=>200], 401);
            }
            if (isset($request->token)) {
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login','code'=>200], 200);
                }
                $files = '';
                if($request->hasFile('image')) {
                    $image = $request->image;
                    $files = md5(time()).'.'.$request->file('image')->getClientOriginalExtension();
                    $path = base_path().'/assets/crm/images/user/';
                    $request->file('image')->move($path,$files);
                    $img = Image::make($path.$files)->save($path.$files);
                }
                $user->image = $files;
                $user->name = escape(htmlspecialchars($request->name));
                $user->mobile = escape(htmlspecialchars($request->mobile));
                $user->alternate_mobile = escape(htmlspecialchars($request->alternate_mobile));
                $user->company = escape(htmlspecialchars($request->company));
                $user->address_line_1 = escape(htmlspecialchars($request->address_line_1));
                $user->city = escape(htmlspecialchars($request->city));
                $user->state = escape(htmlspecialchars($request->state));
                $user->country = escape(htmlspecialchars($request->country));
                $user->postcode = escape(htmlspecialchars($request->postcode));
                $user->cont_person_name = escape(htmlspecialchars($request->cont_person_name));
                $user->cont_person_email = escape(htmlspecialchars($request->cont_person_email));
                $user->cont_person_phone = escape(htmlspecialchars($request->cont_person_phone));
                $user->cont_person_designation = escape(htmlspecialchars($request->cont_person_designation));
                $user->pan = escape(htmlspecialchars($request->pan));
                $user->gst = escape(htmlspecialchars($request->gst));
                $user->save();

                return response()->json(['message' => 'success','code'=>200], 200);
            }else{
                return response()->json(['message' => 'login','code'=>200], 200);
            }
        }
        return response()->json(['message' => 'Unauthorised','code'=>200], 401);
    }


    //Get profile Api

    public function getProfile(Request $request)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised','code'=>200], 401);
            }
            if (isset($request->token)) {
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login','code'=>200], 200);
                }
                $profile_array = array();
                $country_name = '';
                $state_name = '';
                $city_name = '';
                if($user->country!== null){
                    $countries = DB::table('countries')->where('id',$user->country)->first();
                    $country_name = $countries->name;
                }
                if($user->state!== null){
                    $states = DB::table('states')->where('id',$user->state)->first();
                    $state_name = $states->name;
                }
                if($user->city!== null){
                    $cities = DB::table('cities')->where('id',$user->city)->first();
                    $city_name = $cities->name;
                }
                $profile_array = array(
                    "message" =>'ok',
                    "code"=>300,
                    "id" => $user->id,
                    "name" => $user->name,
                    "email" => $user->email,
                    "mobile" => $user->mobile,
                    "image" => url('assets/crm/images/user/' . $user->image),
                    "alternate_mobile" => $user->alternate_mobile,
                    "company" => $user->company,
                    "address_line_1" => $user->address_line_1,
                    "address_line_2" => $user->address_line_2,
                    "city" => $city_name,
                    "state" => $state_name,
                    "country" => $country_name,
                    "postcode" => $user->postcode,
                    "cont_person_name" => $user->cont_person_name,
                    "cont_person_email" => $user->cont_person_email,
                    "cont_person_phone" => $user->cont_person_phone,
                    "cont_person_designation" => $user->cont_person_designation,
                    "pan" => $user->pan,
                    "gst" => $user->gst
                );

                return response()->json($profile_array, 200);
            }else{
                return response()->json(['message' => 'login again','code'=>200], 200);
            }
        }
        return response()->json(['message' => 'Unauthorised','code'=>200], 401);
    }


    /**
     * Country List api
     *
     * @return \Illuminate\Http\Response
     */
    public function countryList(Request $request)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised','code'=>200], 401);
            }
            $country_list =array();
            $countries = DB::table('countries')->select(['id','name'])->get();
            foreach($countries as $country){
                $country_list[] =array(
                    'id'=>$country->id,
                    'name'=>$country->name
                );
            }

            return response()->json(['code'=>300,'message'=>'ok','countries' => $country_list], 200);
        }
        return response()->json(['message' => 'Unauthorised','code'=>200], 401);
    }

    /**
     * State List api
     *
     * @return \Illuminate\Http\Response     *
     *
     */
    public function stateList(Request $request, $country_id)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised','code'=>200], 401);
            }
            $state_list =array();

            if(isset($country_id)){
                if(is_numeric($country_id)){
                    $states = DB::table('states')->where('country_id', '=', $country_id)->get();
                    foreach($states as $state){
                        $state_list[] = array(
                            'id' =>$state->id,
                            'name'=>$state->name
                        );
                    }
                    return response()->json(['code'=>300,'message'=>'ok','states' => $state_list], 200);
                }

                return response()->json(['error' => 'Invalid Country Id','code'=>200], 200);
            }

            return response()->json(['error' => 'Country Id Not Found','code'=>200], 200);

        }
        return response()->json(['message' => 'Unauthorised','code'=>200], 401);
    }

    /**
     * City List api
     * @param  Request $request //For getting the values
     * @param  int $state_id //For getting the state id
     *
     * @return \Illuminate\Http\Response
     */

    public function cityList(Request $request, $state_id)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised','code'=>200], 401);
            }
            $city_list =array();
            if(isset($state_id)){
                if(is_numeric($state_id)){

                    $cities = DB::table('cities')->where('state_id', '=', $state_id)->get();
                    foreach($cities as $city){
                        $city_list[] = array(
                            'id' =>$city->id,
                            'name'=>$city->name
                        );
                    }
                    return response()->json(['code'=>300,'message'=>'ok','cities' => $city_list], 200);
                }

                return response()->json(['error' => 'Invalid State Id','code'=>200], 200);
            }

            return response()->json(['error' => 'State Id Not Found','code'=>200], 200);

        }
        return response()->json(['message' => 'Unauthorised','code'=>200], 401);
    }
    // Retail Search Api
    public function retailSearchApi(Request $request){
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'wrong api', 'code' => 401], 401);
            }
            $utype = '';
            $customer_id = '';
            if (isset($request->token)) {
                if($request->token == ''){

                }else {
                    $utype = '';
                    $customer_id = $request->token;
                    $user = Customer::where('appid', $request->token)->first();
                    if ($user === null) {
                        $utype = '';
                    } else {
                        if ($user->user_type) {
                            $utype = 'individual';
                        }
                    }
                }
            }

            // Apply the product filters
            $where = array();
            if (!empty($request->min) && !empty($request->max)) {
                $where['price'] = "price BETWEEN '" . escape($request->min) . "' AND '" . escape($request->max) . "'";
            }
            if (!empty($request->search)) {
                $where['search'] = "(title LIKE '%" . escape($request->search) . "%' OR text LIKE '%" . escape($request->search) . "%')";
            }
            $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
            $products = DB::select("SELECT * FROM products $where ORDER BY title DESC LIMIT 5");
            $response = array();
            //$response['products'] = array();
            // fetch products and return them in json format
            foreach ($products as $row) {
                $data['id'] = $row->id;
                $data['images'] = url('/assets/products/' . image_order($row->images));
                $data['title'] = $row->title;
                $data['category'] = $row->category;
                $data['text'] = mb_substr(translate($row->text), 0, 200);
                $data['path'] = 'product/' . path($row->title, $row->id);
                if($utype == 'individual'){
                    $data['price'] = c($row->price);
                }else{
                    $data['price'] = 'login to view price';
                }
                $data['in'] = "Products";
                array_push($response, $data);
            }
            return response()->json([ 'message' => $response], 200);
        }
        return response()->json(['message' => 'Unauthorised'], 401);
    }

    // Retail Search Api
    public function searchApi(Request $request){
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'wrong api', 'code' => 401], 401);
            }
            $utype = '';
            $customer_id = '';
            if (isset($request->token)) {
                if($request->token == ''){

                }else {
                    $utype = '';
                    $customer_id = $request->token;
                    $user = Customer::where('appid', $request->token)->first();
                    if ($user === null) {
                        $utype = '';
                    } else {
                        if ($user->user_type) {
                            $utype = 'institutional';
                        } else {
                            $utype = 'individual';
                        }
                    }
                }
            }

            // Apply the product filters
            $where = array();
            if (!empty($request->min) && !empty($request->max)) {
                $where['price'] = "price BETWEEN '" . escape($request->min) . "' AND '" . escape($request->max) . "'";
            }
            if (!empty($request->search)) {
                $where['search'] = "(title LIKE '%" . escape($request->search) . "%' OR text LIKE '%" . escape($request->search) . "%')";
            }
            if (!empty($request->hub)) {
                $ph_products = array();
                $ph_relations = DB::table('ph_relations')->get();
                foreach($ph_relations as $ph){
                    $hub_ids = json_decode($ph->hubs);
                    foreach($hub_ids as $hub_id){
                        if($hub_id->hub == $request->hub){
                            $ph_products[] = $ph->products;
                        }
                    }

                }
//                $where['hub'] = "id IN (" .$ph_products. ")";
                $where['hub'] = "id IN (" . implode(',', $ph_products) . ")";
            }
            $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
            $products = DB::select("SELECT * FROM products $where ORDER BY title DESC LIMIT 5");
            $response = array();
            //$response['products'] = array();
            // fetch products and return them in json format
            foreach ($products as $row) {
                $data['id'] = $row->id;
                $data['images'] = url('/assets/products/' . image_order($row->images));
                $data['title'] = $row->title;
                $data['category'] = $row->category;
                $data['text'] = mb_substr(translate($row->text), 0, 200);
                $data['path'] = 'product/' . path($row->title, $row->id);
                if($utype == 'institutional') {
                    $pr = getInstitutionalPrice($row->id,$customer_id,$request->hub,false,false,false);
                    $data['price'] = $pr['total'];
                }elseif($utype == 'individual'){
                    $data['price'] = c($row->price);
                }else{
                    $data['price'] = 'login to view price';
                }
                $data['in'] = "Products";
                array_push($response, $data);
            }
            return response()->json([ 'message' => $response], 200);
        }
        return response()->json(['message' => 'Unauthorised'], 401);
    }

    //Retail Add to cart Api




    public function addCart(Request $request)
    {
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }

            if (!isset($request->id)) {
                exit;
            }
            $id = $request->id;
            $type = $request->type;
            $product = DB::select("SELECT * FROM products WHERE id = " . $id)[0];
            $q = isset($request->q) ? (int)$request->q : "1";
            $min = isset($request->min) ? (int)$request->min : 0;


            $variants = array();
            if ($q == 0 && $type == 'variants') {
                $variants = (array)json_decode(str_replace("\\", "", $request->quantity));

                foreach ($variants as $var) {
                    $q = $q + $var;

                }

            }
            if ($min == 0) {
                if ($q <= $min) {
                    return response()->json(['message' => 'invalid_data'], 200);
                }
            } else {
                if ($q < $min) {
                    return response()->json(['message' => 'minimum_quantity'], 200);
                }
            }
            $cart_variants[$id] = $variants;
            $cart_items = array();
            $cart_items[$id] = $q;
            $options = json_decode(DB::select("SELECT options FROM products WHERE id = " . $id)[0]->options, true);
            $option_list = array();
            if (!empty($options)) {
                foreach ($options as $option) {
                    $name = $option['name'];
                    $title = $option['title'];
                    $option_list[$name] = array('title' => $title, 'value' => ($request->input($name) != '' ? ($option['type'] == 'multi_select' ? implode(' , ', $request->input($name)) : $request->input($name)) : ''));
                }
            }
            $cart_options[$id] = json_encode($option_list);
            $bulk_discounts = DB::select("SELECT * FROM product_discount where quantity<='" . $q . "' AND product_id = '" . $id . "' ORDER BY quantity DESC LIMIT 1");
            $variantsx = DB::select("SELECT * FROM product_variants WHERE display = '1' AND product_id = " . $id);

            $variant_list = array();

            if (count($bulk_discounts) > 0) {
                $discount = $bulk_discounts[0];
                if (!empty($variantsx) && $type == 'variants') {
                    foreach ($variantsx as $variant) {
                        $title = $variant->variant_title;
                        $price = $product->institutional_price;

                        if ($discount->discount_type == 'Flat') {
                            $price = ($variant->price + $price) - $discount->discount;
                        } else {
                            $dis = number_format((($variant->price + $price) * $discount->discount) / 100, 2, '.', '');
                            $price = ($variant->price + $price) - $dis;
                        }
                        $quantity = "";
                        foreach($variants as $key=>$value){
                            if($key == $variant->id){
                                $quantity = $value;
                            }
                        }
                        $variant_list[$variant->id] = array('title' => $title, 'price' => $price, 'quantity' => $quantity);

                    }
                }
            } else {
                if (!empty($variantsx) && $type == 'variants') {

                    foreach ($variantsx as $variant) {
                        $title = $variant->variant_title;
                        $price = $product->institutional_price;

                        $price = $price + $variant->price;
//                        dd($variants);
                        $quantity = "";
                        foreach($variants as $key=>$value){
                            if($key == $variant->id){
                                $quantity = $value;
                            }
                        }

//                        }
                        $variant_list[$variant->id] = array('title' => $title, 'price' => $price, 'quantity' => $quantity);

                    }
                }
            }



            $cart_variants[$id] = json_encode($variant_list);

            $unit = '';
            if (DB::select("SELECT quantity FROM products WHERE id = " . $id)[0]->quantity < $q) {
                // Throw stock unavailable error
                return response()->json(['message' => 'unavailable'], 200);
            }
            $data['items'] = json_encode($cart_items, true);
            // Update cart cookies


            $data['options'] = json_encode($cart_options, true);


            $data['variants'] = json_encode($cart_variants, true);


            $data['product_id'] = $id;
            $data['user_id'] = $user->id;
            $data['price'] = $product->price;
            if(isset($request->unit)){
                $unit = $request->unit;
            }
            $data['unit'] = $unit;
            $cart = DB::table('cart')->where('user_id', $user->id)->where('product_id',$id)->where('status', 0)->first();
            if($cart!== null){
                return response()->json(['message' => ' already added to cart ','code'=>300], 200);
            }

            DB::table('cart')->insert($data);
            return response()->json(['message' => 'added to cart successfully','code'=>300], 200);
        }

        return response()->json(['message' => 'login'], 401);
    }
    //Retail update cart
    //retail update cart
    public function updateCart(Request $request)
    {
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }

            if (!isset($request->id)) {
                exit;
            }
            $id = $request->id;
            $type = $request->type;
            $product = DB::select("SELECT * FROM products WHERE id = " . $id)[0];
            $q = isset($request->q) ? (int)$request->q : "1";
            $min = isset($request->min) ? (int)$request->min : 0;


            $variants = array();
            if ($q == 0 && $type == 'variants') {
                $variants = (array)json_decode(str_replace("\\", "", $request->quantity));

                foreach ($variants as $var) {
                    $q = $q + $var;

                }

            }
            if ($min == 0) {
                if ($q <= $min) {
                    return response()->json(['message' => 'invalid_data'], 200);
                }
            } else {
                if ($q < $min) {
                    return response()->json(['message' => 'minimum_quantity'], 200);
                }
            }
            $cart_variants[$id] = $variants;
            $cart_items = array();
            $cart_items[$id] = $q;
            $options = json_decode(DB::select("SELECT options FROM products WHERE id = " . $id)[0]->options, true);
            $option_list = array();
            if (!empty($options)) {
                foreach ($options as $option) {
                    $name = $option['name'];
                    $title = $option['title'];
                    $option_list[$name] = array('title' => $title, 'value' => ($request->input($name) != '' ? ($option['type'] == 'multi_select' ? implode(' , ', $request->input($name)) : $request->input($name)) : ''));
                }
            }
            $cart_options[$id] = json_encode($option_list);
            $bulk_discounts = DB::select("SELECT * FROM product_discount where quantity<='" . $q . "' AND product_id = '" . $id . "' ORDER BY quantity DESC LIMIT 1");
            $variantsx = DB::select("SELECT * FROM product_variants WHERE display = '1' AND product_id = " . $id);

            $variant_list = array();

            if (count($bulk_discounts) > 0) {
                $discount = $bulk_discounts[0];
                if (!empty($variantsx) && $type == 'variants') {
                    foreach ($variantsx as $variant) {
                        $title = $variant->variant_title;
                        $price = $product->institutional_price;

                        if ($discount->discount_type == 'Flat') {
                            $price = ($variant->price + $price) - $discount->discount;
                        } else {
                            $dis = number_format((($variant->price + $price) * $discount->discount) / 100, 2, '.', '');
                            $price = ($variant->price + $price) - $dis;
                        }
                        $quantity = "";
                        foreach($variants as $key=>$value){
                            if($key == $variant->id){
                                $quantity = $value;
                            }
                        }
                        $variant_list[$variant->id] = array('title' => $title, 'price' => $price, 'quantity' => $quantity);

                    }
                }
            } else {
                if (!empty($variantsx) && $type == 'variants') {

                    foreach ($variantsx as $variant) {
                        $title = $variant->variant_title;
                        $price = $product->institutional_price;

                        $price = $price + $variant->price;
//                        dd($variants);
                        $quantity = "";
                        foreach($variants as $key=>$value){
                            if($key == $variant->id){
                                $quantity = $value;
                            }
                        }

//                        }
                        $variant_list[$variant->id] = array('title' => $title, 'price' => $price, 'quantity' => $quantity);

                    }
                }
            }



            $cart_variants[$id] = json_encode($variant_list);

            $unit = '';
            if (DB::select("SELECT quantity FROM products WHERE id = " . $id)[0]->quantity < $q) {
                // Throw stock unavailable error
                return response()->json(['message' => 'unavailable'], 200);
            }
            $cart = DB::table('cart')->where('user_id', $user->id)->where('product_id',$id)->where('status', 0)->first();

            if($cart !==null){
                $items = json_encode($cart_items, true);
                $options = json_encode($cart_options, true);
                $variants = json_encode($cart_variants, true);
                if(isset($request->unit)){
                    $unit = $request->unit;
                }
                $units = $unit;
                DB::table('cart')
                    ->where('product_id', $id)
                    ->where('user_id', $user->id)
                    ->where('status', 0)
                    ->update(['items' => $items,'options'=>$options,'variants'=>$variants,'unit'=>$units]);
                return response()->json(['message' => 'cart updated successfully','code'=>300], 200);
            }
            else{
                return response()->json(['message' => 'it is new product','code'=>300], 200);
            }

        }

        return response()->json(['message' => 'login'], 401);
    }

    //Remove Cart items    //Remove Cart items
    public function remove(Request $request)
    {
        // Remove product from cart

        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }

            if (!isset($request->id)) {
                exit;
            }
            $id = $request->id;

            DB::table('cart')->where('product_id',$id)->where('status',0)->where('user_id',$user->id)->delete();

            return response()->json(['message' => 'removed from cart','code'=>300], 200);
        }
        return response()->json(['message' => 'login'], 401);

    }
    //customer data

    public function customerData(Request $request)
    {
        // Remove product from cart

        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $response=array();
        $contact_no =array();
        $type = '';
        $crm_customers = DB::table('crm_customers')->get();
        foreach ($crm_customers as $cust) {
            if($cust->customer_category==1){
                $type= 'C';
            }
            elseif ($cust->customer_category==2){
                $type= 'R';
            }
            elseif ($cust->customer_category==3){
                $type= 'T';
            }
            elseif ($cust->customer_category==4){
                $type= 'B';
            }
            elseif ($cust->customer_category==7){
                $type= 'M';
            }
            // $con = DB::table('crm_customers')->where('id',$cust->id)->get();
            $district = DB::table('district')->where('id',$cust->district)->first();
            // foreach($con as $c){
            //     $contact_no[] = array($c->contact_no);
            // }

            $response['contacts'][] = array(
                'id' => $cust->id,
                'name' => $type.'.'.$cust->name.'.'.$district->name,
                'numbers'=>$cust->contact_no
            );
        }



        return response()->json($response, 200);

    }
// Retail cart
    public function cart(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $ret = array();
        $variant = array();


        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $bookings = DB::table('cart')->where('user_id', $user->id)->where('status', 0)->get();
            foreach ($bookings as $booking){
                $product = DB::table('products')->where('id', $booking->product_id)->first();
                $product_variants = DB::table('product_variants')->where('product_id', $booking->product_id)->first();
//                $variants = DB::table('product_variants')->where('id', $booking->product_id)->first();
                if($product !== null){
                    $items = json_decode($booking->items);
                    $count_items = count($items);
                    //$jdata = str_replace("\\", "", $booking->variants);
                    $variant_array = (array)json_decode($booking->variants);

                    $units = (array)json_decode($booking->unit);


                    foreach($variant_array as $key => $v){
                        if($key == $product->id){
                            $variant = (array)json_decode($v);
                        }
                    }
                    /*if(isset($variant_array[$product->id])){
                        $variant = (array)json_decode($variant_array[$product->id]);
                    }*/
                    $retVar = array();
                    $unit_array = array();
                    $uni = '';
                    foreach($variant as $key => $value){
                        $varianttitle = DB::table('size')->where('id',$value->title)->first();
                        $price_detail = getIndividual($product->id,$key);
                        $total_price = $price_detail['total'];
                        $total_sale_price = $price_detail['total_sale'];
                        $variant_price = $price_detail['var_price'];
                        foreach($units as $k=> $unit){
                            if($key == $k ){
                                $var_title = DB::table('product_variants')
                                    ->where('id',$k)
                                    ->first();
                                $unit_symbol = DB::table('units')
                                    ->where('id',$unit)
                                    ->first();
                                $uni = $unit;

                                $unit_id = '';
                                $unit_symbol_symbol = '';
                                if($unit_symbol){
                                    $unit_id = $unit_symbol->id;
                                    $unit_symbol_symbol = $unit_symbol->symbol;
                                }
                                $unit_array[] = array(
                                    "unit_id"=>$unit_id,
                                    "unit" => $unit_symbol_symbol
                                );
                            }
                        }
                        if($value->quantity == ''){
                            continue;
                        }
                        $retVar[] = array(
                            "id" => $key,
                            "title" => $varianttitle->name,
                            "variant_price" => $variant_price,
                            "price" => $total_price,
                            "sale_price" =>$total_sale_price,
                            "quantity" => $value->quantity,
                            "unit" => $uni,
                            "info" => $price_detail
                        );
                    }



                    $booked = 0;
                    foreach ($items as $key => $val){
                        $booked = $booked + $val;
                    }

                    DB::table('booking_count')
                        ->where('customer_id',$user->id)
                        ->update(['booking_counter' => 0]);
                    $ret[] = array(
                        'id' => $booking->id,
                        'p_id' => $booking->product_id,
                        'c_id' => $product->category,
                        'image' => url('assets/products/'.image_order($product->images)),
                        'status' => 'Pending',
                        'title' => $product->title,
                        'price' => $booking->price,
                        'quantity' => $booked,
                        'unit' => $unit_array,
                        'variant' => $retVar,
                        'booking_date' => date('d-m-y h:i A',strtotime($booking->created_at))
                    );
                }
            }
            return response()->json(['message' => $ret], 200);
        }else{
            return response()->json(['message' => 'login'], 401);
        }
    }

    //Retail cart ends

    //Retail checkout

    public function checkout(Request $request)
    {
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $response = array();
            // Get custom fields from database
            $fields = DB::select("SELECT * FROM fields ORDER BY id ASC ");
            foreach ($fields as $field) {
                if ($field->code == 'country') {
                    // Return country selector if code of field is country
                    $options = array();
                    $countries = DB::select("SELECT * FROM country ORDER BY nicename ASC");
                    foreach ($countries as $country) {
                        $options[] = array('iso' =>$country->iso,
                            'phonecode' => $country->phonecode,
                            'nicename' => $country->nicename
                        );
                    }

                    $response[] =  array('name' =>$field->name,
                        'code' => $field->code,
                        'options' => $options
                    );
                } else {
                    $response[] =  array('name' =>$field->name,
                        'code' => $field->code,
                        'type' => $field->code == 'mobile' ? 'number' : 'text',
                    );
                }
            }
            return response()->json(['message' => $response,'code'=>300], 200);
        }
        return response()->json(['message' => 'login','code'=>200], 401);
    }
    //Retail billing
    public function billing(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        if (isset($request->token)) {
            $response = array();
            $state_list = array();
            $city_list = array();
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $same = '';
            $response['fullname']= array('fullname' =>$user->name== '' ? 'text' : $user->name,
                'type' => 'text',
            );
            $response['company']= array('company' =>$user->company== '' ? 'text' : $user->company,
                'type' => 'text',
            );
            $response['email']= array('email' =>$user->email== '' ? 'text' : $user->email,
                'type' => 'text',
            );
            $countries = DB::table('countries')->where('id', 101)->first();
            $response['country']= array(
                'id'=> $countries->id,
                'sortname' =>$countries->sortname,
                'name' => $countries->name,
            );

            $states = getStates($countries->id);
            $state_ids = array_pluck(DB::table('states')->where('country_id', $countries->id)->get(), 'id');
            $user_state = '';
            foreach($states as $state){
                if($state->id == $user->state){
                    $user_state = 'yes';

                }
                else{
                    $user_state = '';
                }
                $state_list[]=array(
                    'id' =>$state->id,
                    'name'=>$state->name,
                    'selected'=>$user_state
                );
            }
            $response['state']= $state_list;
            $cities = DB::table('cities')->whereIn('state_id',$state_ids)->get();
            $user_city = '';
            foreach($cities as $city){
                if($city->id == $user->city){
                    $user_city = 'yes';

                }
                else{
                    $user_city = '';
                }
                $city_list[]=array(
                    'id' =>$city->id,
                    'name'=>$city->name,
                    'state'=>$city->state_id,
                    'selected'=>$user_city
                );
            }
            $response['city']= $city_list;
            $response['Zip/postal code']= array('postcode' =>$user->postcode== '' ? 'text' : $user->postcode,
                'type' => 'text',
            );
            $response['mobile']= array('mobile' =>$user->mobile== '' ? 'text' : $user->mobile,
                'type' => 'text',
            );
            $response['alternate_no']= array('alternate_mobile' =>$user->alternate_mobile== '' ? 'text' : $user->alternate_mobile,
                'type' => 'text',
            );
            $response['address_line_1']= array('address_line_1' =>$user->address_line_1== '' ? 'text' : $user->address_line_1,
                'type' => 'text',
            );
            $response['address_line_2']= array('address_line_2' =>$user->address_line_2== '' ? 'text' : $user->address_line_2,
                'type' => 'text',
            );
            if($request->shipping=="same"){
                $same ='shipping address will be same';
            }
            else{
                $same ='shipping address will be different';
            }
            $response['shipping']= $same;
            return response()->json(['message'=>$response,'code'=>300], 200);
        }


        return response()->json(['message' => 'login'], 401);
    }


    //retail checkout ends
    public function checkAvail(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        $cat = '';
        $product_id = $request->product_id;
        $products = DB::table('products')->where('id','=',$product_id)->first();
        if($products!==null){
            $cat = $products->category;
        }
        else{
            return response()->json(['message' => 'invalid product id'], 401);
        }
        $pincode = $request->pincode;
        //dd($cat);
        $record = DB::table('shipping')->whereRaw('FIND_IN_SET(?,postcodes)',$pincode)->whereRaw('FIND_IN_SET(?,category)',$cat)->get();

//        ->join('shipping','pincode.id', '=', 'shipping.id')
//            DB::select("select * from shipping where postcodes LIKE '%".$pincode."%' AND FIND_IN_SET('".$cat."',category)");

        //dd($record);


        if(count($record)){
            return response()->json(['message' => 'Yes','code'=>300], 200);
        }
        else{
            return response()->json(['message' => 'No','code'=>200], 200);
        }
    }


    public function shipping(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        if (isset($request->token)) {
            $response = array();
            $state_list = array();
            $city_list = array();
            $type = $request->type;
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            if ($type == 'same') {
                $response['fullname'] = array('fullname' => $user->name,
                    'type' => 'text',
                );
                $response['company'] = array('company' => $user->company,
                    'type' => 'text',
                );
                $response['email'] = array('email' => $user->email,
                    'type' => 'text',
                );
                $countries = DB::table('countries')->where('id', 101)->first();
                $response['country'] = array(
                    'id' => $countries->id,
                    'sortname' => $countries->sortname,
                    'name' => $countries->name,
                );

                $states = getStates($countries->id);
                $state_ids = array_pluck(DB::table('states')->where('country_id', $countries->id)->get(), 'id');
                $user_state = '';
                foreach ($states as $state) {
                    if ($state->id == $user->state) {
                        $user_state = 'yes';

                    } else {
                        $user_state = '';
                    }
                    $state_list[] = array(
                        'id' => $state->id,
                        'name' => $state->name,
                        'selected' => $user_state
                    );
                }
                $response['state'] = $state_list;
                $cities = DB::table('cities')->whereIn('state_id', $state_ids)->get();
                $user_city = '';
                foreach ($cities as $city) {
                    if ($city->id == $user->city) {
                        $user_city = 'yes';

                    } else {
                        $user_city = '';
                    }
                    $city_list[] = array(
                        'id' => $city->id,
                        'name' => $city->name,
                        'state' => $city->state_id,
                        'selected' => $user_city
                    );
                }
                $response['city'] = $city_list;
                $response['Zip/postal code'] = array('postcode' => $user->postcode,
                    'type' => 'text',
                );
                $response['mobile'] = array('mobile' => $user->mobile,
                    'type' => 'text',
                );
                $response['alternate_no'] = array('alternate_mobile' => $user->alternate_mobile,
                    'type' => 'text',
                );
                $response['address_line_1'] = array('address_line_1' => $user->address_line_1,
                    'type' => 'text',
                );
                $response['address_line_2'] = array('address_line_2' => $user->address_line_2,
                    'type' => 'text',
                );

                return response()->json($response, 200);
            }
            else{
                $response['fullname'] = array('fullname' => 'text',
                    'type' => 'text',
                );
                $response['company'] = array('company' => 'text',
                    'type' => 'text',
                );
                $response['email'] = array('email' => 'text',
                    'type' => 'text',
                );
                $countries = DB::table('countries')->where('id', 101)->first();
                $response['country'] = array(
                    'id' => $countries->id,
                    'sortname' => $countries->sortname,
                    'name' => $countries->name,
                );

                $states = getStates($countries->id);
                $state_ids = array_pluck(DB::table('states')->where('country_id', $countries->id)->get(), 'id');
                foreach ($states as $state) {
                    $state_list[] = array(
                        'id' => $state->id,
                        'name' => $state->name,
                    );
                }
                $response['state'] = $state_list;
                $cities = DB::table('cities')->whereIn('state_id', $state_ids)->get();
                foreach ($cities as $city) {
                    $city_list[] = array(
                        'id' => $city->id,
                        'name' => $city->name,
                        'state' => $city->state_id,
                    );
                }
                $response['city'] = $city_list;
                $response['Zip/postal code'] = array('postcode' => 'text',
                    'type' => 'text',
                );
                $response['mobile'] = array('mobile' => 'text',
                    'type' => 'text',
                );
                $response['alternate_no'] = array('alternate_mobile' => 'text',
                    'type' => 'text',
                );
                $response['address_line_1'] = array('address_line_1' => 'text',
                    'type' => 'text',
                );
                $response['address_line_2'] = array('address_line_2' => 'text',
                    'type' => 'text',
                );

                return response()->json(['message'=>$response,'code'=>300], 200);
            }

        }


        return response()->json(['message' => 'login'], 401);
    }





    public function shippingMethod(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $shipping_add = json_encode($request->shippingaddress);
            $cart = DB::table('cart')->where('user_id', $user->id)->where('status',0)->get();
            if(count($cart)!==0){
                DB::table('cart')
                    ->where('user_id', $user->id)
                    ->where('status', 0)
                    ->update(['shipping_address' => $shipping_add]);
            }
            $records = DB::select("select * from shipping where postcodes LIKE '%".$request->pincode."%'");
            $response = array();
            foreach($records as $record){
                $response['shipping_method'][] = array('id' => $record->id,
                    'name' => $record->name,
                    'cost' => $record->cost,
                );
            }

            return response()->json($response, 200);

        }
        return response()->json(['message' => 'login'], 401);

    }
    public function paymentMethod(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $shipping_method = json_encode($request->all());
            $cart = DB::table('cart')->where('user_id', $user->id)->where('status',0)->get();
            if(count($cart)!==0){
                DB::table('cart')
                    ->where('user_id', $user->id)
                    ->where('status', 0)
                    ->update(['shipping_method' => $shipping_method]);
            }
            $records = DB::select("select * from payments where active = 1");
            $response = array();
            $option =array();
            foreach($records as $record){
                if ($record->options != '{}') {
                    $data = json_decode($record->options);
                    foreach ($data as $key => $value) {
                        if ($value != '')
                            $option[]= array('key'=>$key,
                                'value'=>$value

                            );
                    }

                }
                $response['payment_method'][] = array('id' => $record->id,
                    'title' => $record->title,
                    'options' => $option,
                );

            }

            return response()->json([$response], 200);
        }
        return response()->json(['message' => 'login'], 401);

    }

    //Retail confirm order api
    public function confirmOrder(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $payment_method = json_encode($request->all());
            $cart = DB::table('cart')->where('user_id', $user->id)->where('status', 0)->get();
            if (count($cart)!==0) {
                DB::table('cart')
                    ->where('user_id', $user->id)
                    ->where('status', 0)
                    ->update(['payment_method' => $payment_method]);
                $ids = array_pluck(DB::table('cart')->where('user_id', $user->id)->where('status', 0)->get(), 'product_id');
                $cart_products = DB::table('products')->whereIn('id', $ids)->get();
                $total_price = 0;
                $total_p = 0;
                $quant = 0;
                $grand_total = 0;
                $response["products"] = array();

                foreach($cart as $car){
                    $product = DB::table('products')->where('id', $car->product_id)->first();
                    $items = (array)json_decode($car->items);
                    foreach($items as $key=>$value){
                        $qaunt = $value;
                    }

                    /*$vart = json_decode($car->variants);
                    $tp = 0;
                    foreach($vart as $key=>$varj){
                        $vjason = json_decode($varj);
                        foreach($vjason as $key=>$vs){
                            $price_detail = getIndividual($product->id,$key);
                            $tp += $price_detail['total'];
                        }
                    }*/


                    $units = (array)json_decode($car->unit);
                    $options = (array)json_decode($car->options);
                    $option_array = array();

                    foreach ($options as $option) {
//                        $option_array[] = '<i>'.$option['title'].'</i> : '.$option['value'];
                    }
                    $unit_array = array();
                    $uni = '';
                    $ret = array();
                    foreach ($cart_products as $row){
                        /*$data['id'] = $product->id;
                        $data['images'] = url('assets/products/' . image_order($row->images));
                        $data['title'] = $product->title;
                        //$data['price'] = $tp;
                        $data['quantity'] = $qaunt;
                        $data['options'] = implode('<br/>',$option_array);
                        $data['total'] = 0.00;
                        $data['variants'] = '';*/
                        $variants = (array)json_decode($car->variants,true);
                        $variant_array = array();
                        $p_var = DB::select("SELECT * FROM product_variants WHERE product_id = '".$row->id."' ORDER BY id ASC ");
                        $price_total = 0;
                        $price = 0;
                        if(count($variants) > 0){
                            foreach ($variants as $key=>$var) {
                                $var_name=(array)json_decode($var);
                                foreach($var_name as $vid=>$v){
                                    if($v->quantity !== ''){
                                        $pp = 0;
                                        $varianttitle = DB::table('size')->where('id',$v->title)->first();
                                        $price_detail = getIndividual($product->id,$vid);
                                        $total_price = $price_detail['total'];
                                        $total_sale_price = $price_detail['total_sale'];
                                        $variant_price = $price_detail['var_price'];
                                        foreach($units as $k=> $unit){
                                            if($vid == $k ){
                                                $var_title = DB::table('product_variants')
                                                    ->where('id',$k)
                                                    ->first();
                                                $unit_symbol = DB::table('units')
                                                    ->where('id',$unit)
                                                    ->first();
                                                $uni = $unit;

                                                $unit_id = '';
                                                $unit_symbol_symbol = '';
                                                if($unit_symbol){
                                                    $unit_id = $unit_symbol->id;
                                                    $unit_symbol_symbol = $unit_symbol->symbol;
                                                }
                                                $unit_array[] = array(
                                                    "unit_id"=>$unit_id,
                                                    "unit" => $unit_symbol_symbol
                                                );
                                            }
                                        }
                                        $variant_array[] = array(
                                            'id'=>$vid,
                                            'title'=>$varianttitle->name,
                                            'variant_price'=>$variant_price,
                                            'quantity'=>$v->quantity,
                                            'price'=>$total_price,
                                            'sale_price'=>$total_sale_price,
                                            'total_price'=>$total_price*$v->quantity,
                                            'unit'=>$unit_array,
                                            'info'=>$price_detail
                                        );
                                        $price_total += $total_price*$v->quantity;
                                    }
                                }
                            }

                            //$data['variants'] = $variant_array;
                        }
                        $ret[] = array(
                            'id' => $car->id,
                            'p_id' => $car->product_id,
                            'c_id' => $product->category,
                            'image' => url('assets/products/'.image_order($product->images)),
                            'title' => $product->title,
                            'price' => $price_total,
                            'quantity' => $qaunt,
                            'variant' => $variant_array
                        );
                        $price = $row->price;
                        if($row->price > $row->sale){
                            $price = $row->sale;
                        }
                        $data['total'] = $price*$quant;
                        $s = $price*$quant;
                        if($price_total){
                            $price = $price_total;
                            $data['total'] = $price;
                            $total_price += $price;
                            $s = $price;
                        }else{
                            $total_price += $price*$quant;
                        }
                        $data['price'] = $price_total;
                        $total_p+= $quant;
                        $t = 100;
                        $p = $s/$t;
                        $tax = $s - $p;
                    }
                    $dis = 0;
                    $sp = 0.00;
                    if(isset($car->shipping_method)){
                        $sm = json_decode($car->shipping_method);
                        $meth = $sm->shippingmethod;
                        if (DB::select("SELECT COUNT(*) as count FROM shipping WHERE id = " . $meth)[0]->count > 0) {
                            $shipping = DB::select("SELECT * FROM shipping WHERE id = " . $meth)[0];
                            $sp = $shipping->cost;
                        }
                    }
                    $paid = '';
                    if(isset($car->payment_method)){
                        $pm = json_decode($car->payment_method);
                        //dd($pm->token);
                        if(isset($pm->paymentmethod)) {
                            $pmId = $pm->paymentmethod;

                            if (DB::select("SELECT COUNT(*) as count FROM payments WHERE id = " . $pmId)[0]->count > 0) {
                                $payment = DB::select("SELECT * FROM payments WHERE id = " . $pmId)[0];
                                $paid = $payment->title;
                            }
                        }
                    }
                    $response['products'] = $ret;
                }
                $response['shipping_price'] = $sp;
                $response['payment_method'] = $paid;
                return response()->json($response, 200);
            }
        }
    }
    //Retail place order api
    public function PlaceOrder(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $datax = array();
            $cart = DB::table('cart')->where('user_id', $user->id)->where('status', 0)->get();
            if (count($cart)!==0) {

                $ids = array_pluck(DB::table('cart')->where('user_id', $user->id)->where('status', 0)->get(), 'product_id');

                $cart_products = DB::table('products')->whereIn('id', $ids)->get();


                $total_price = 0;
                $total_p = 0;
                $quant = 0;
                $grand_total = 0;
                $response["products"] = array();
                foreach($cart as $car){
                    $product = DB::table('products')->where('id', $car->product_id)->first();
                    $items = (array)json_decode($car->items);
                    foreach($items as $key=>$value){
                        $qaunt = $value;
                    }
                    $options = (array)json_decode($car->options);
                    $option_array = array();

                    foreach ($options as $option) {
//                        $option_array[] = '<i>'.$option['title'].'</i> : '.$option['value'];

                    }
                    foreach ($cart_products as $row){

                        $data['id'] = $product->id;

                        $data['images'] = image_order($row->images);
                        $data['title'] = $product->title;
                        $data['price'] = $product->price;
                        $data['quantity'] = $qaunt;
                        $data['options'] = implode('<br/>',$option_array);
                        $data['total'] = 0.00;
                        $data['variants'] = '';
                        $variants = (array)json_decode($car->variants,true);
                        $variant_array = array();
                        $p_var = DB::select("SELECT * FROM product_variants WHERE product_id = '".$row->id."' ORDER BY id ASC ");
                        $price_total = 0;
                        $price = 0;

                        if(count($variants) > 0){
                            foreach ($variants as $key=>$var) {
                                $var_name=(array)json_decode($var);
                                foreach($var_name as $v){
                                    if($v->quantity !== ''){
                                        $variant_array[] = array(
                                            'title'=>$v->title,
                                            'quantity'=>$v->quantity,
                                            'price'=>$v->price,
                                            'total_price'=>$v->price*$v->quantity
                                        );
                                        $price_total += $v->price*$v->quantity;

                                    }
                                }

                            }
//                            dd($price_total);
                            $data['variants'] = $variant_array;
                        }
                        $price = $row->price;

                        if($row->price > $row->sale){
                            $price = $row->sale;
                        }
                        $data['total'] = $price*$quant;
                        $s = $price*$quant;

                        if($price_total){
                            $price = $price_total;
                            $data['total'] = $price;
                            $total_price += $price;
                            $s = $price;

                        }else{
                            $total_price += $price*$quant;

                        }
                        $data['price'] = $price;

                        $total_p+= $quant;

                        $t = 100;

                        $p = $s/$t;

                        $tax = $s - $p;

                    }

                    $dis = 0;

                    $sp = 0.00;
                    if(isset($car->shipping_method)){
                        $sm = json_decode($car->shipping_method);
                        $meth = $sm->shippingmethod;
                        if (DB::select("SELECT COUNT(*) as count FROM shipping WHERE id = " . $meth)[0]->count > 0) {
                            $shipping = DB::select("SELECT * FROM shipping WHERE id = " . $meth)[0];
                            $sp = $shipping->cost;
                        }

                    }
                    $paid = array();
                    if(isset($car->payment_method)){
                        $pm = json_decode($car->payment_method);
                        $pmId = $pm->paymentmethod;
                        if (DB::select("SELECT COUNT(*) as count FROM payments WHERE id = " . $pmId)[0]->count > 0) {
                            $payment = DB::select("SELECT * FROM payments WHERE id = " . $pmId)[0];
//                            $paid = $payment->title;
                            $paid['method'] = $payment->code;
                            if($payment->code == 'cash') {
                                $paid['payment_status'] = 'paid';
                            }else{
                                $paid['payment_status'] = 'unpaid';
                            }
                        }

                    }


                    array_push($response["products"], $data);
                }
                $cart_address = DB::table('cart')->where('user_id', $user->id)->where('status', 0)->first();
                $sum = ($total_price-$dis)+$sp;
                $datax['products'] = json_encode($response["products"]);
                $datax['customer'] = $user->id;
                $datax['name'] = $user->name;
                $datax['email'] = $user->email;
                $datax['city'] = $user->city;
                $datax['address'] = $cart_address->shipping_address;
                $datax['mobile'] = $user->mobile;
                $datax['date'] = date('y-m-d');
                $datax['shipping'] = $sp;
                $datax['payment'] = json_encode($paid);
                $datax['country'] = 'IN';
                $datax['stat'] = 1;
                $datax['summ'] = $sum;
                $id= DB::table('orders')->insert($datax);
                if($id){
                    DB::table('cart')
                        ->where('user_id', $user->id)
                        ->where('status', 0)
                        ->update(['status' => 1]);
                    return response()->json(['message'=>'order placed successfully','code'=>300], 200);
                }
                else{
                    return response()->json(['message'=>'order has not been placed successfully','code'=>200], 200);
                }


            }
            return response()->json(['message'=>'no products in cart','code'=>200], 200);

        }
        return response()->json(['message'=>'please login','code'=>200], 200);

    }


    //Retail checkout data
    public function checkoutData(Request $request)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
            }
            if (isset($request->token)) {
                $id = false;
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login', 'code' => 401], 200);
                }


                $ment = array();


                $ment = array(
                    'customer_id' => $user->id,
                    'billing_info' => $request->billing_info,
                    'shipping_info' => $request->shipping_info,
                    'shipping_method' => $request->shipping_method,
                    'payment_method'=>$request->payment_method,
                    'payment_status'=>$request->payment_status,
                    'order_status'=>1,
                    'date'=>date('y-m-d')
                );

                DB::table('checkout_data')->insert($ment);

                return response()->json(['message' => 'entry successfully in checkout data ', 'code' => 200], 200);
            }
            else {
                return response()->json(['message' => 'login', 'code' => 401], 200);
            }


        }

        return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
    }

    // Retail my orders
    public function myOrders(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $ret = array();
        $product = array();


        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $orders = DB::table('orders')->where('customer', $user->id)->get();
            if (count($orders) !== 0) {
                $total_quantity = 0;
                foreach ($orders as $booking) {

//                $product = DB::table('products')->where('id', $booking->product_id)->first();
//                $product_variants = DB::table('product_variants')->where('product_id', $booking->product_id)->first();
//                $variants = DB::table('product_variants')->where('id', $booking->product_id)->first();
                    $items = json_decode($booking->products);

                    foreach ($items as $item) {
                        $total_quantity+= $item->quantity;

                        $product[] = array(
                            'id' => $item->id,
                            'images' => url('assets/products/' . image_order($item->images)),
                            'title' => $item->title,
                            'price' => $item->price,
                            'quantity' => $item->quantity,
                            'total' => $item->total,
                            'variants' => $item->variants
                        );

                    }
                    $payment_detail = json_decode($booking->payment);
                    $pay_method = $payment_detail->method;
                    $pay_status = $payment_detail->payment_status;
                    $shippingadd = json_decode($booking->address);
//                    dd($shippingadd);
                    $add1 = '';
                    $city = '';
                    $state = '';
                    $country = '';
                    $postcode = '';
//                    foreach($shippingadd as $ship) {
//                        $add1 = $ship->addressline1;
//                        $city = $ship->city;
//                        $state = $ship->state;
//                        $country = $ship->country;
//                        $postcode = $ship->postalcode;
//                    }
                    $ret[] = array(
                        'code' => 300,
                        'order_id' => $booking->id,
                        'name' => $booking->name,
                        'email' => $booking->email,
                        'country' => 'India',
                        'city' => $booking->city,
                        'mobile' => $booking->mobile,
                        'shipping_address' => $add1.','.$city.','.$state.','.$country.','.$postcode,
                        'billing_address' => $add1.','.$city.','.$state.','.$country.','.$postcode,
                        'total_quantity'=> $total_quantity,
                        'products' => $product,
                        'order_date' => date('d-m-y h:i A', strtotime($booking->date)),
                        'shipping_charge' => $booking->shipping,
                        'total'=>$booking->summ-$booking->shipping,
                        'grand_total' => $booking->summ,
                        'payment_method'=> $pay_method,
                        'payment_status'=>$pay_status

                    );

                }
                return response()->json(['message' => $ret], 200);
            }
            else{
                return response()->json(['message' => 'no orders','code'=>200], 401);
            }
        }
        else{
            return response()->json(['message' => 'login','code'=>200], 401);
        }
    }

    //advices,design and services api
    public function advicesCategory(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $response= array();
        $sub = array();
        $totli=0;
        $totrat=0;
        $totvisit=0;
        if($request->cat) {
            $postscat = DB::select("SELECT * FROM advices_category WHERE parent = '".$request->cat."' ORDER BY id DESC");

            foreach ($postscat as $pos) {
                $response['advices_by_category'] = array(
                    'id' => $pos->id,
                    'name' => $pos->name,
                    'image'=>url('assets/blog/' . $pos->banner_image),
                );
            }
        }else {
            $posts = DB::select("SELECT * FROM blog ORDER BY time DESC");
            foreach ($posts as $pos) {
                $response['advices_by_category'] = array(
                    'id' => $pos->id,
                    'name' => $pos->title,
                    'image'=>url('assets/blog/' . $pos->banner_image),
                );
            }
        }

        $dimg = DB::table('advices_category')->where('id', $request->cat)->first();
//dd($dimg);
        if($dimg !== null) {
            $response['banner_image'] = url('/assets/blog/' . $dimg->banner_image);
            $response['title'] = $dimg->name;
            $response['short_description'] = $dimg->short_description;
        }
        $advices_cat = DB::select("SELECT * FROM advices_category WHERE parent = '0'");

        foreach ($advices_cat as $cats) {
            $childs = DB::select("SELECT * FROM advices_category WHERE parent = ".$cats->id." ORDER BY id DESC");
            if($childs){
                foreach ($childs as $pscat) {
                    $sub[] = array(
                        'id' => $pscat->id,
                        'name' => $pscat->name
                    );
                }
            }

            $response['advice_category'][] = array(
                'id' => $cats->id,
                'name' => $cats->name,
                'sub_category'=>$sub,
                'link' => url('advices/' . $cats->id.'/'.$cats->name)
            );
        }



//        $reponse['post_title']=$dimg->name;
//        $reponse['post_subhead']=$dimg->short_description;
        $btotal = DB::select("SELECT * FROM blog WHERE category = '".$request->cat."'");
        foreach($btotal as $btv){
            $totvisit = $totvisit + $btv->visits;
        }
        $response['total_visits']=$totvisit;
        foreach ($btotal as $btr){
            $total_revi = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$btr->id)[0]->count;
            $totrat = $totrat + $total_revi;

        }
        $response['total_reviews']=$totrat;
        foreach ($btotal as $btl){
            $li = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$btl->id)[0]->count;
            $totli = $totli + $li;

        }
        $response['total_likes']=$totli;
        $featured = DB::select("SELECT * FROM blog Where featured = '1' LIMIT 12");
        // dd($featured);
        $best = DB::select("SELECT * FROM best_for_advices ORDER BY id DESC LIMIT 2");
        $link = DB::table('link')->where('page', 'Advices')->first();

        foreach ($featured as $post) {
            $response['featured_advices'][] = array(
                'id' => $post->id,
                'title' => $post->title,
                'link'=>url('blog/' . path($post->title,$post->id)),
                'image'=>url('/assets/blog/' . $post->images)
            );
        }

        foreach ($best as $b) {
            $response['best_for_you'][] = array(
                'id' => $b->id,
                'title' => $b->title,
                'content' => $b->content,
                'image'=>url('assets/products/' . $b->image),
                'link'=>$b->link
            );
        }



        if(!empty($link)){
            $response['link'][] = array(
                'link' => $link->link,
                'content' => $link->content,
                'image'=>url('/assets/products/'.image_order($link->image)),
            );

        }
        return response()->json($response, 200);


    }


    public function layoutCategory(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $cat = [];
        $plan_cat = false;
        $response= array();
        $sub = array();
        $totli=0;
        $totrat=0;
        $totvisits=0;
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$request->slug."'");
        if(count($plan_cat) <= 0){
            return response()->json(['message' => 'page not found'], 401);
        }else{
            $cat = $plan_cat[0];
        }

        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 AND layout = "layout_plans" ORDER BY name ASC');
        if($design_categorycon !== '') {
            $advices_cat = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $advices_cat = 'Data Not Available';
        }
        $banner = DB::select("SELECT * FROM design_cat_banner");
        $layout_plans = DB::select('SELECT * FROM design_category Where parent = '.$cat->id.' ORDER BY `name` ASC');
        $featured = DB::select("SELECT * FROM layout_plans Where featured = '1' LIMIT 12");
        $best = DB::select("SELECT * FROM best_for_layout ORDER BY id DESC LIMIT 2");

        $link = DB::table('link')->where('page', 'Layout Plan Category')->first();

        $response['post_title']=$cat->name;
        $response['post_subhead']=$cat->description;
        $response['banner_image']=url('assets/design/'.$cat->image);
        foreach ($advices_cat as $cats) {
            $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$cats->id." AND layout = 'layout_plans' ORDER BY id DESC");
            if($childs){
                foreach ($childs as $pscat) {
                    $sub[] = array(
                        'layout' => $pscat->layout,
                        'name' => $pscat->name,
                        'link'=>$pscat->slug
                    );
                }
            }

            $response['design_category'][] = array(
                'layout' => $cats->layout,
                'name' => $cats->name,
                'sub_category'=>$sub,
                'link' => $cats->slug
            );
        }

        $btotal = DB::select("SELECT * FROM design_category WHERE parent = '".$cat->id."'");
        foreach($btotal as $btvs){
            $plas = DB::select("SELECT * FROM layout_plans WHERE category = '".$btvs->id."'");
            foreach ($plas as $pls) {
                $totvisits = $totvisits + $pls->visits;
            }
        }
        $response['total_visits']=$totvisits;
        foreach($btotal as $btv){
            $pla = DB::select("SELECT * FROM layout_plans WHERE category = '".$btv->id."'");
            foreach ($pla as $pl) {
                $pla = DB::select("SELECT COUNT(*) as count FROM layout_reviews WHERE active = 1 AND review <> '' AND layout = '".$pl->id."'")[0]->count;
                $totrat = $totrat + $pla;
            }
        }
        $response['total_reviews']=$totrat;
        foreach ($btotal as $btl){
            $plal = DB::select("SELECT * FROM layout_plans WHERE category = '".$btl->id."'");
            foreach ($plal as $pll) {
                $li = DB::select("SELECT COUNT(*) as count FROM plan_like WHERE plan_id = " . $pll->id)[0]->count;
                $totli = $totli + $li;
            }
        }
        $response['total_likes']=$totli;

        foreach ($featured as $post) {
            $response['featured_advices'][] = array(
                'slug' => $post->slug,
                'title' => $post->title,
                'link'=>$post->slug,
                'image'=>url('/assets/layout_plans/'.image_order($post->images))
            );
        }

        foreach ($best as $b) {
            $response['best_for_you'][] = array(
                'id' => $b->id,
                'title' => $b->title,
                'content' => $b->content,
                'image'=>url('assets/products/' . $b->image),
                'link'=>$b->link
            );
        }

        foreach ($layout_plans as $pos) {
            $response['layout_by_category'][] = array(
                'slug' => $pos->slug,
                'layout' => $pos->layout,
                'name' => $pos->name,
                'image'=>url('assets/design/' . $pos->image),
                'link'=>$pos->slug,
            );
        }
        return response()->json($response, 200);


    }

    public function layoutPlans(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $cat = [];
        $plan_cat = false;
        $response= array();
        $sub = array();
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$request->slug."'");
        if(count($plan_cat) <= 0){
            return response()->json(['message' => 'page not found'], 401);
        }else{
            $cat = $plan_cat[0];
        }

        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $advices_cat = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $advices_cat = 'Data Not Available';
        }
        $banner = DB::select("SELECT * FROM design_cat_banner");
        $layout_plans = DB::select('SELECT * FROM layout_plans Where category = '.$cat->id.' ORDER BY `created_at` DESC');
        $featured = DB::select("SELECT * FROM layout_plans Where featured = '1' LIMIT 12");
        $best = DB::select("SELECT * FROM best_for_layout ORDER BY id DESC LIMIT 2");

        $link = DB::table('link')->where('page', 'Layout Plan Category')->first();
        $response['post_title']=$cat->name;
        $response['post_subhead']=$cat->description;
        $response['banner_image']=url('assets/design/'.$cat->image);
        $btotal = DB::select("SELECT * FROM design_category WHERE parent = '".$cat->id."'");
        $totvisit = 0;
        $btotal = DB::select("SELECT * FROM design_category WHERE id = '".$cat->id."'");
        foreach($btotal as $btv){
            $pla = DB::select("SELECT * FROM layout_plans WHERE category = '".$btv->id."'");
            foreach ($pla as $pl) {
                $pla = DB::select("SELECT COUNT(*) as count FROM layout_reviews WHERE active = 1 AND review <> '' AND layout = '".$pl->id."'")[0]->count;
                $totvisit = $totvisit + $pla;
            }
        }
        $response['total_reviews']=$totvisit;
        $totvisits = 0;
        foreach($btotal as $btvs){
            $plas = DB::select("SELECT * FROM layout_plans WHERE category = '".$btvs->id."'");
            foreach ($plas as $pls) {
                $totvisits = $totvisits + $pls->visits;
            }
        }
        $response['total_visits']=$totvisits;
        $totli = 0;
        foreach ($btotal as $btl){
            $plal = DB::select("SELECT * FROM layout_plans WHERE category = '".$btl->id."'");
            foreach ($plal as $pll) {
                $li = DB::select("SELECT COUNT(*) as count FROM plan_like WHERE plan_id = " . $pll->id)[0]->count;
                $totli = $totli + $li;
            }
        }
        $response['total_likes']=$totli;

        foreach ($advices_cat as $cats) {
            $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$cats->id." ORDER BY id DESC");
            if($childs){
                foreach ($childs as $pscat) {
                    $sub[] = array(
                        'layout' => $pscat->layout,
                        'name' => $pscat->name,
                        'link'=>url('/' . $pscat->layout.'/'.$pscat->slug)
                    );
                }
            }

            $response['design_category'][] = array(
                'layout' => $cats->layout,
                'name' => $cats->name,
                'sub_category'=>$sub,
                'link' => url('/' . $cats->layout.'/'.$cats->slug)
            );
        }




        foreach ($layout_plans as $pos) {
            $amm = json_decode($pos->interior_ammenities);

            //dd($amm);
            $ammenties = array();
            foreach($amm as $key=>$value) {
                $a = DB::select("SELECT * FROM amminities WHERE id = " . $key)[0];
                // dd($a->name);

                $ammenties[]=array(
                    'name'=>$a->name,
                    'value'=>$value
                );



            }

            $response['layout_plans'][] = array(
                'slug' => $pos->slug,
                'ammenties' => $ammenties,
                'image'=>url('/assets/layout_plans/' . $pos->images),
                'link'=>url('/layout_plan/' .$pos->slug),
            );
        }
        return response()->json($response, 200);


    }


    public function photoCategory(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $cat = [];
        $plan_cat = false;
        $response= array();
        $sub = array();
        $totli=0;
        $totrat=0;
        $totvisits=0;
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$request->slug."'");
        if(count($plan_cat) <= 0){
            return response()->json(['message' => 'page not found'], 401);
        }else{
            $cat = $plan_cat[0];
        }

        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $advices_cat = DB::select('SELECT * FROM design_category WHERE parent = 0 AND layout = "photos" ORDER BY name ASC');
        }else {
            $advices_cat = 'Data Not Available';
        }
        $banner = DB::select("SELECT * FROM design_cat_banner");
        $layout_plans = DB::select('SELECT * FROM design_category Where parent = '.$cat->id.' ORDER BY `name` ASC');
        $featured = DB::select("SELECT * FROM photo_gallery where featured = '1' ORDER BY id ASC LIMIT 12");
        $best = DB::select("SELECT * FROM best_for_photo ORDER BY id ASC LIMIT 2");
        $link = DB::table('link')->where('page', 'Photos Category')->first();

        $response['post_title']=$cat->name;
        $response['post_subhead']=$cat->description;
        $response['banner_image']=url('assets/design/'.$cat->image);

        foreach ($advices_cat as $cats) {
            $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$cats->id." AND layout = 'photos' ORDER BY id DESC");
            if($childs){
                foreach ($childs as $pscat) {
                    $sub[] = array(
                        'layout' => $pscat->layout,
                        'name' => $pscat->name,
                        'link'=>$pscat->slug,
                        'image'=>url('assets/products/' . $pscat->image),
                    );
                }
            }

            $response['design_category'][] = array(
                'layout' => $cats->layout,
                'name' => $cats->name,
                'sub_category'=>$sub,
                'link' => $cats->slug,
                'image'=>url('assets/products/' . $cats->image),
            );
        }
        $reponse['post_title']=$cat->name;
        $reponse['post_subhead']=$cat->description;
        $btotal = DB::select("SELECT * FROM design_category WHERE parent = '".$cat->id."'");
        foreach($btotal as $btvs){
            $plas = DB::select("SELECT * FROM photo_gallery WHERE category = '".$btvs->id."'");
            foreach ($plas as $pls) {
                $totvisits = $totvisits + $pls->visits;
            }
        }
        $reponse['total_visits']=$totvisits;
        foreach($btotal as $btv){
            $pla = DB::select("SELECT * FROM photo_gallery WHERE category = '".$btv->id."'");
            foreach ($pla as $pl) {
                $pla = DB::select("SELECT COUNT(*) as count FROM photo_reviews WHERE active = 1 AND review <> '' AND photo = '".$pl->id."'")[0]->count;
                $totrat = $totrat + $pla;
            }
        }
        $reponse['total_reviews']=$totrat;
        foreach ($btotal as $btl){
            $plal = DB::select("SELECT * FROM photo_gallery WHERE category = '".$btl->id."'");
            foreach ($plal as $pll) {
                $li = DB::select("SELECT COUNT(*) as count FROM image_like WHERE image_id = " . $pll->id)[0]->count;
                $totli = $totli + $li;
            }
        }
        $reponse['total_likes']=$totli;

        foreach ($featured as $post) {
            $response['featured_gallery'][] = array(
                'slug' => $post->slug,
                'title' => $post->title,
                'link'=>$post->slug,
                'image'=>url('assets/products/' . $post->image),
            );
        }

        foreach ($best as $b) {
            $response['best_for_you'][] = array(
                'id' => $b->id,
                'title' => $b->title,
                'content' => $b->content,
                'image'=>url('assets/products/' . $b->image),
                'link'=>$b->link
            );
        }

        foreach ($layout_plans as $pos) {
            $response['layout_by_category'][] = array(
                'slug' => $pos->slug,
                'layout' => $pos->layout,
                'name' => $pos->name,
                'image'=>url('assets/design/' . $pos->image),
                'link'=>url('/' . $pos->layout.'/'.$pos->slug),
            );
        }
        return response()->json($response, 200);


    }


    public function photos(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $cat = [];
        $plan_cat = false;
        $response= array();
        $sub = array();
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$request->slug."'");
        if(count($plan_cat) <= 0){
            return response()->json(['message' => 'page not found'], 401);
        }else{
            $cat = $plan_cat[0];
        }

        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $advices_cat = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $advices_cat = 'Data Not Available';
        }

        $galleries = DB::select("SELECT * FROM photo_gallery where category = ".$cat->id." ORDER BY created_at DESC");


        foreach ($advices_cat as $cats) {
            $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$cats->id." ORDER BY id DESC");
            if($childs){
                foreach ($childs as $pscat) {
                    $sub[] = array(
                        'layout' => $pscat->layout,
                        'name' => $pscat->name,
                        'link'=>$pscat->slug
                    );
                }
            }

            $response['photos_cat'][] = array(
                'layout' => $cats->layout,
                'name' => $cats->name,
                'sub_category'=>$sub,
                'link' => $cats->slug,
                'image'=>url('assets/products/' . $cats->image),
            );
        }


        foreach ($galleries as $pos) {
            $response['gallary'][] = array(
                'slug' => $pos->slug,
                'title' => $pos->title,
                'image'=>url('assets/images/gallery/' . $pos->image),
                'link'=>$pos->slug,
                'count'=>getPlanPhotsCount($pos->id)
            );
        }
        return response()->json($response, 200);


    }

    public function servicesCategory(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $response= array();
        $sub = array();
        $totli=0;
        $totrat=0;
        $totvisit=0;
        $categoriescon = DB::select('SELECT COUNT(*) as count FROM category WHERE parent = 0 ORDER BY name ASC');
        if($categoriescon !== '') {
            $cats = DB::select('SELECT * FROM category WHERE parent = 0 ORDER BY name ASC');
        }else{
            $cats = 'Data Not Available';
        }
        $check = DB::select("SELECT COUNT(*) as count FROM services_category WHERE parent = '".explode('-',$request->services_category)[0]."'")[0];

        if ($check->count == 0){
            return response()->json(['message' => 'page not found'], 401);
        }
        if (!empty($_GET['search']))
        {
            $where['search'] = "(name LIKE '%".escape($_GET['search'])."%' AND parent = '".explode('-',$request->services_category)[0]."')";
        }
        if (empty($_GET['search']))
        {
            $where['search'] = "(parent = '".explode('-',$request->services_category)[0]."')";
        }

        $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
        $services_categories = DB::select("SELECT * FROM services_category $where");
        $serv = DB::select("SELECT * FROM services_category WHERE parent = '".explode('-',$request->services_category)[0]."'");
        $how = DB::select("SELECT * FROM s_how_it_works WHERE FIND_IN_SET('".explode('-',$request->services_category)[0]."', cat)");
        $icat = DB::select("SELECT * FROM services_category_image WHERE service_category_id = '".explode('-',$request->services_category)[0]."'");
        $link = DB::table('link')->where('page', 'Services Category')->first();
        if($request->services_category!==''){
            $service_title =  DB::table('services_category')->where('id','=',$request->services_category)->first();
            if($service_title){
                $response['cat_title']=$service_title->name;
            }
            $response['banner_image']=url('/assets/images/background/2.jpg');
            $response['heading']='Get a designer space you\'ll love';
            $response['post']='Let our accomplished team of experts submit concepts for your space';
        }
        foreach ($services_categories as $cats) {
            $response['sub_category'][] = array(
                'id' => $cats->id,
                'name' => $cats->name,
                'description' => $cats->description,
                'link' => $cats->id.'-'.$cats->name,
                'image' => url('/assets/services/' .$cats->image)
            );
        }

        if($how){
            foreach ($how as $h) {
                $response['how_it_works'][] = array(
                    'id' => $h->id,
                    'tab_title' => $h->tab_title,
                    'tab_icon' => $h->tab_icon,
                    'content' => $h->content,
                    'content_title' => $h->content_title,
                    'image'=>url('/assets/how_it_works/' .$h->image)
                );
            }
        }
        if($icat){
            $i = 0;
            $default_cat = 0;
            $incat = '';
            $dcat = '';
            $ncat = null;
            $gcid = '';
            $scid = '';
            $dcn = '';
            $image_1 = '';
            $image_2 = '';
            $image_3 = '';
            $image_4 = '';
            $image_5 = '';

            foreach ($icat as $ic) {
                if($i == 0) {
                    $dcat = DB::select("SELECT * FROM design_category WHERE id = '" . $ic->gallery_category_id . "'")[0];
                    $incat = $ic->gallery_category_id;
                    $ncat = DB::select("SELECT * FROM services_category_image WHERE gallery_category_id = '" . $incat . "'")[0];
                    $default_cat = $ic->gallery_category_id;
//                    $response['gallery_category_id'][] = $ic->gallery_category_id.',';
//                    $response['service_category_id'][] = $ic->service_category_id;
//                    $response['gallery_category_name'][] = $dcat->name.',';
                    $image_1 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_1."'")[0];
                    $image_2 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_2."'")[0];
                    $image_3 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_3."'")[0];
                    $image_4 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_4."'")[0];
                    $image_5 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_5."'")[0];
                    $response['images'][] = array(
                        'gallery_category_id' => $ic->gallery_category_id,
                        'service_category_id' => $ic->service_category_id,
                        'gallery_category_name' => $dcat->name,
                        'image1_link' => $image_1->slug,
                        'image1'=>url('assets/images/gallery/' . $image_1->image),
                        'image2_link' => $image_2->slug,
                        'image2'=>url('assets/images/gallery/' . $image_2->image),
                        'image3_link' => $image_3->slug,
                        'image3'=>url('assets/images/gallery/' . $image_3->image),
                        'image4_link' => $image_4->slug,
                        'image4'=>url('assets/images/gallery/' . $image_4->image),
                        'image5_link' => $image_5->slug,
                        'image5'=>url('assets/images/gallery/' . $image_5->image),
                    );
                }
                else {
                    $ncat = DB::select("SELECT * FROM services_category_image WHERE gallery_category_id = '" . $incat . "'")[0];
                    $dcat = DB::select("SELECT * FROM design_category WHERE id = '" . $ic->gallery_category_id . "'")[0];
//                    $response['gallery_category_id'][] = $ic->gallery_category_id.',';
//                    $response['service_category_id'][] = $ic->service_category_id;
//                    $response['gallery_category_name'][] = $dcat->name.',';
                    $image_1 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_1."'")[0];
                    $image_2 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_2."'")[0];
                    $image_3 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_3."'")[0];
                    $image_4 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_4."'")[0];
                    $image_5 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_5."'")[0];
                    $response['images'][] = array(
                        'gallery_category_id' => $ic->gallery_category_id,
                        'service_category_id' => $ic->service_category_id,
                        'gallery_category_name' => $dcat->name,
                        'image1_link' => $image_1->slug,
                        'image1'=>url('assets/images/gallery/' . $image_1->image),
                        'image2_link' => $image_2->slug,
                        'image2'=>url('assets/images/gallery/' . $image_2->image),
                        'image3_link' => $image_3->slug,
                        'image3'=>url('assets/images/gallery/' . $image_3->image),
                        'image4_link' => $image_4->slug,
                        'image4'=>url('assets/images/gallery/' . $image_4->image),
                        'image5_link' => $image_5->slug,
                        'image5'=>url('assets/images/gallery/' . $image_5->image),
                    );
                }
                $i++;
            }
        }
        if($link){
            $response['link'][] = array(
                'image' => url('/assets/products/'.image_order($link->image)),
                'content' => $link->content,
                'link' => $link->link
            );
        }
        return response()->json($response, 200);
    }

    public function subService(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $customer_id='';
        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if($user!==null){
                $customer_id = $user->id;
            }
        }

        $response= array();
        $sub = array();
        $totli=0;
        $totrat=0;
        $totvisit=0;
        if ($request->category_id == ''){
            return response()->json(['message' => 'page not found'], 401);
        }
        // Apply the product filters

        $categoriescon = DB::select('SELECT COUNT(*) as count FROM category WHERE parent = 0 ORDER BY name ASC');
        if($categoriescon !== '') {
            $cats = DB::select('SELECT * FROM category WHERE parent = 0 ORDER BY name ASC');
        }else{
            $cats = 'Data Not Available';
        }
        $check = DB::select("SELECT COUNT(*) as count FROM services WHERE category = '".explode('-',$request->category_id)[0]."'")[0];
        if ($check->count == 0){
            return response()->json(['message' => 'page not found'], 401);
        }
        if (!empty($_GET['search']))
        {
            $where['search'] = "(name LIKE '%".escape($_GET['search'])."%' AND category = '".explode('-',$request->category_id)[0]."')";
        }
        if (empty($_GET['search']))
        {
            $where['search'] = "(category = '".explode('-',$request->category_id)[0]."')";
        }
        $questions = DB::select("SELECT * FROM service_category_questions WHERE service_id = '".explode('-',$request->category_id)[0]."' Order By priority ASC");
        foreach ($questions as $que){
            $ques = DB::select("SELECT * FROM questions WHERE id = '".$que->question_id."'");
            $response['questions'][] = array(
                'question' => $ques[0]->question,
                'option' => $ques[0]->options,
                'type' => $ques[0]->type
            );
        }
        $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
        $sub_service = DB::select("SELECT * FROM services $where");
        $how = DB::select("SELECT * FROM s_how_it_works WHERE FIND_IN_SET('".explode('-',$request->category_id)[0]."', cat)");
        $serv = DB::select("SELECT * FROM services_category");

        $faqs = DB::select("SELECT * FROM faq WHERE service_id = '".explode('-',$request->category_id)[0]."' AND status = 'Approved' ORDER BY id DESC");
        $link = DB::table('link')->where('page', 'Sub Services')->first();
        if($request->category_id!==''){
            $service_title =  DB::table('services_category')->where('id','=',$request->category_id)->first();
            if($service_title){
                $response['cat_title']=$service_title->name;

            }
            $response['banner_image']=url('/assets/images/background/2.jpg');
            $response['heading']='Get a designer space you\'ll love';
            $response['post']='Let our accomplished team of experts submit concepts for your space';
        }

        if($how){

            foreach ($how as $h) {
                $response['how_it_works'][] = array(
                    'id' => $h->id,
                    'tab_title' => $h->tab_title,
                    'tab_icon' => $h->tab_icon,
                    'content' => $h->content,
                    'content_title' => $h->content_title,
                    'image'=>url('/assets/how_it_works/' .$h->image)
                );
            }
        }else{
            $response['how_it_works'] = array();
        }

        foreach ($sub_service as $service) {
            $get_started='';
            $quest ='';
            if($customer_id!==''){
                $question = DB::table("questions")->where('id', $service->id)->first();
                if($question !== null){
                    $quest= $question->question;
                }
                $get_started = $quest;
            }
            else{
                $get_started ='login please';
            }
            $service_id = $service->id;
            $price = is_numeric($service->price)? $service->price : $service->price;
            $response['services_offered_by_experts'][] = array(
                'id' => $service->id,
                'title' => $service->title,
                'description' => $service->description,
                'price'=>$price,
                'link' => url('service/' . $service->id),
                'image' => url('/assets/services/'.image_order($service->images)),
                'get_started'=>$get_started
            );
        }


        foreach ($faqs as $faq) {
            if($customer_id!==''){
                $u = $faq->user_id;
                $user = DB::select("SELECT * FROM customers WHERE id = '$u'")[0];
                $response['faqs'][] = array(
                    'question' => $faq->question,
                    'user_name' => $user->name,
                    'posted_date' => date('d M, Y', strtotime($faq->created)),
                    'answer' => $faq->answer
                );
            }
        }
        if($customer_id==''){
            $response['faqs'] = 'login please';
        }




        $response['why_choose_us_image1'] = url('assets/images/ico1.png') ;
        $response['why_choose_us_image2'] = url('assets/images/ico2.png') ;
        $response['why_choose_us_image3'] = url('assets/images/ico3.png') ;
        $response['why_choose_us_image4'] = url('assets/images/ico21.png') ;

        if($link){
            $response['link'][] = array(
                'image' => url('/assets/products/'.image_order($link->image)),
                'content' => $link->content,
                'link' => $link->link
            );
        }



        return response()->json($response, 200);


    }

    public function advices(Request $request)
    {
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $response= array();
        $sub = array();
        $totli=0;
        $totrat=0;
        $totvisit=0;
        if($request->cat) {
            $posts = DB::select("SELECT * FROM blog WHERE category = '".$request->cat."' ORDER BY time DESC");
        }else {
            $posts = DB::select("SELECT * FROM blog ORDER BY time DESC");
        }


        $advices_cat = DB::select("SELECT * FROM advices_category WHERE parent = '0'");
        $dimg = DB::table('advices_category')->where('id', $request->cat)->first();
        $response['banner_image']=url('/assets/blog/'.$dimg->banner_image);
        $response['title']=$dimg->name;
        $response['short_description']='Take a look at these 19 websites made in Webflow and handpicked from our internal Slack';
        $btotal = DB::select("SELECT * FROM blog WHERE category = '".$request->cat."'");
        foreach ($advices_cat as $cats) {
            $childs = DB::select("SELECT * FROM advices_category WHERE parent = ".$cats->id." ORDER BY id DESC");
            if($childs){
                foreach ($childs as $pscat) {
                    $sub[] = array(
                        'id' => $pscat->id,
                        'name' => $pscat->name
                    );
                }
            }

            $response['advice_category'][] = array(
                'id' => $cats->id,
                'name' => $cats->name,
                'sub_category'=>$sub,
                'link' => url('advices/' . $cats->id.'/'.$cats->name)
            );
        }



//        $reponse['post_title']=$dimg->name;
//        $reponse['post_subhead']=$dimg->short_description;
        $btotal = DB::select("SELECT * FROM blog WHERE category = '".$request->cat."'");
        foreach($btotal as $btv){
            $totvisit = $totvisit + $btv->visits;
        }
        $response['total_visits']=$totvisit;
        foreach ($btotal as $btr){
            $total_revi = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$btr->id)[0]->count;
            $totrat = $totrat + $total_revi;

        }
        $response['total_reviews']=$totrat;
        foreach ($btotal as $btl){
            $li = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$btl->id)[0]->count;
            $totli = $totli + $li;

        }
        $response['total_likes']=$totli;
        foreach ($posts as $post) {
            $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->count;
            $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$post->id)[0]->count;
            $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$post->id)[0]->count;
            $rating = 0;
            if ($total_ratings > 0){
                $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->sum;
                $rating = $rating_summ / $total_ratings;
            }
            $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$post->id." AND active = 1 ORDER BY time DESC");
            $faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$post->id." AND status = 'Approved' ORDER BY id DESC");
            $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$post->id."'")[0];
            $total_rating =  $rating->total_rating;
            $total_user = $rating->total_user;
            if($total_rating==0){
                $avg_rating = 0;
            }
            else{
                $avg_rating = round($total_rating/$total_user,1);
            }
            $response['posts'][] = array(
                'id' => $post->id,
                'title' => $post->title,
                'date' => $post->date,
                'category' => $post->category,
                'short_desc'=>$post->short_des,
                'visits' => $post->visits,
                'reviews' => $total_reviews,
                'rating' => $avg_rating,
                'likes'=>$like,
                'link' =>url('advice/'.path($post->title,$post->id)),
                'image'=>url('/assets/blog/'.$post->images),
            );
        }
        $link = DB::table('link')->where('page', 'Advices')->first();
        if($link){
            $response['link'][] = array(
                'image' => url('/assets/products/'.image_order($link->image)),
                'content' => $link->content,
                'link' => $link->link
            );
        }

        return response()->json($response, 200);
    }


    public function photosGallery(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }

        $cat = [];
        $plan_cat = false;
        $response= array();
        $sub = array();
        $plan_cat = DB::select("SELECT * FROM photo_gallery WHERE slug = '".$request->slug."'");

        if(count($plan_cat) <= 0){
            return response()->json(['message' => 'page not found'], 401);
        }else{
            $cat = $plan_cat[0];
        }

        $customer_id='';
        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if($user!==null){
                $customer_id = $user->id;
                $rcheck = DB::select("SELECT COUNT(*) as count FROM recently_viewed_photo_gallery WHERE user_id = ".$customer_id." AND gallery_id = ".$cat->id)[0]->count;
                if($rcheck == 0) {
                    $rvp = DB::insert("INSERT INTO recently_viewed_photo_gallery (user_id, gallery_id) VALUES ('" . $customer_id . "','" . $cat->id . "')");
                }
            }
        }

        // Apply the product filters
        $images = DB::table('photos')->where('gallery_id', '=', $cat->id)->get();
        foreach($images as $im){
            $response['slider_image'][]= url('/assets/images/gallery/photos/'.$im->image);
        }
        $related = DB::select("SELECT * FROM photo_gallery WHERE id <> ".$cat->id." and category=".$cat->category." ORDER BY Rand() LIMIT 18");
        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $cats = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $cats = 'Data Not Available';
        }
        $response['id']= $cat->id;
        $response['title']= $cat->title;
        $response['image_1']= url('/assets/images/gallery/'.$cat->image);
        if($customer_id!==''){
            $user_id = $customer_id;
            $image_id = $cat->id;
            $img = DB::select("SELECT * FROM image_like WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
            $ins = DB::select("SELECT * FROM image_inspiration WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
            if($img){
                $response['like']= 'Liked';
            }
            else{
                $response['like']= 'Like';
                $id_image = DB::insert("INSERT INTO image_like (user_id, image_id) VALUES ('".$user_id."','".$image_id."')");
            }
            if($ins){
                $response['add_to_project']= 'Added to project';
            }
            else{
//                $response['add_to_project']= 'Added to project';
                $pro = DB::select("SELECT * FROM user_project WHERE user_id = '".$user_id."'");
                if($pro !== ''){
                    foreach($pro as $p){
                        $response['add_to_project'][] = array(
                            'p_title' => $p->title,
                            'p_id' => $p->id,
                            'p_image' => url('assets/projects/'.$p->image)
                        );
                    }
                }


            }

        }
        else{
            $response['like']= 'Login please';
            $response['add_to_project']= 'Login please';
        }

        $i = 0;
        foreach($images as $image){
            if($i==0){
                $response['image']=url('/assets/images/gallery/photos/'.$image->image);
                if($customer_id!==''){
                    $user_id = $customer_id;
                    $image_id = $cat->id;
                    $img = DB::select("SELECT * FROM image_like WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
                    $ins = DB::select("SELECT * FROM image_inspiration WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
                    if(!empty($img)){
                        $response['like']= 'Liked';
                    }
                    else{
                        $response['like']= 'Like';
                        $images = DB::insert("INSERT INTO image_like (user_id, image_id) VALUES ('".$user_id."','".$image_id."')");
                    }
                    if(!empty($ins)){
                        $response['add_to_project']= 'Added to project';
                    }
                    else{
//                $response['add_to_project']= 'Added to project';
                        $pro = DB::select("SELECT * FROM user_project WHERE user_id = '".$user_id."'");
                        if($pro !== ''){
                            foreach($pro as $p){
                                $response['add_to_project'][] = array(
                                    'p_title' => $p->title,
                                    'p_id' => $p->id,
                                    'p_image' => url('assets/projects/'.$p->image)
                                );
                            }
                        }


                    }

                }
                else{
                    $response['like']= 'Login please';
                    $response['add_to_project']= 'Login please';
                }

            }
            else{
                $response['image']=url('/assets/images/gallery/photos/'.$image->image);
                if($customer_id!==''){
                    $user_id = $customer_id;
                    $image_id = $cat->id;
                    $img = DB::select("SELECT * FROM image_like WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
                    $ins = DB::select("SELECT * FROM image_inspiration WHERE user_id = '".$user_id."' AND image_id = '".$image_id."'");
                    if(!empty($img)){
                        $response['like']= 'Liked';
                    }
                    else{
                        $response['like']= 'Like';
                        $images = DB::insert("INSERT INTO image_like (user_id, image_id) VALUES ('".$user_id."','".$image_id."')");
                    }
                    if(!empty($ins)){
                        $response['add_to_project']= 'Added to project';
                    }
                    else{
//                $response['add_to_project']= 'Added to project';
                        $pro = DB::select("SELECT * FROM user_project WHERE user_id = '".$user_id."'");
                        if($pro !== ''){
                            foreach($pro as $p){
                                $response['add_to_project'][] = array(
                                    'p_title' => $p->title,
                                    'p_id' => $p->id,
                                    'p_image' => url('assets/projects/'.$p->image)
                                );
                            }
                        }


                    }

                }
                else{
                    $response['like']= 'Login please';
                    $response['add_to_project']= 'Login please';
                }

            }
            $i++;
        }
        $i==0;
        if(count($images)){
            $response['data_to_slide']=$i;
            $response['thumbnail_image']=url('/assets/images/gallery/thumbs/'.$cat->image);
            $i++;

        }

        foreach($images as $image){
            if($i==0){
                $response['data_to_slide']=$i;
                $response['thumbnail_image']=url('/assets/images/gallery/thumbs/'.$image->image);
            }
            else{
                $response['data_to_slide']=$i;
                $response['thumbnail_image']=url('/assets/images/gallery/thumbs/'.$image->image);
            }
            $i++;
        }
        $likes = DB::select("SELECT COUNT(*) as count FROM image_like WHERE image_id = '".$cat->id."'")[0]->count;
        $inspi = DB::select("SELECT COUNT(*) as count FROM image_inspiration WHERE image_id = '".$cat->id."'")[0]->count;

        $total_ratings = DB::select("SELECT COUNT(*) as count FROM photo_reviews WHERE active = 1 AND photo = ".$cat->id)[0]->count;
        $total_reviews = DB::select("SELECT COUNT(*) as count FROM photo_reviews WHERE active = 1 AND review <> '' AND photo = ".$cat->id)[0]->count;
        $response['likes']=$likes;
        $response['views']=48;
        $response['reviews']=$total_reviews;
        $response['collections']=$inspi;
        $response['description']=$cat->description;
        $rating = 0;
        if ($total_ratings > 0){
            $rating_summ = DB::select("SELECT SUM(rating) as sum FROM photo_reviews WHERE active = 1 AND photo = ".$cat->id)[0]->sum;
            $rating = $rating_summ / $total_ratings;
        }
        $reviews = DB::select("SELECT * FROM photo_reviews WHERE photo = ".$cat->id." AND active = 1 ORDER BY time DESC");
        $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM photo_reviews WHERE active = '1' AND photo = '".$cat->id."'")[0];
        $total_rating =  $rating->total_rating;
        $total_user = $rating->total_user;
        if($total_rating==0){
            $avg_rating = 0;
        }
        else{
            $avg_rating = round($total_rating/$total_user,1);
        }
//        $response['ratings_reviews'][]=array()
        $response['avg_rating']=$avg_rating;
        $response['total_rating']=$total_user;
        $response['total_comment']=$total_reviews;

        $rating1= DB::select("SELECT count(id) as rating1_user FROM photo_reviews WHERE rating = '1' AND photo = '".$cat->id."' AND active = '1'")[0];
        $rating2= DB::select("SELECT count(id) as rating2_user FROM photo_reviews WHERE rating = '2' AND photo = '".$cat->id."' AND active = '1'")[0];
        $rating3= DB::select("SELECT count(id) as rating3_user FROM photo_reviews WHERE rating = '3' AND photo = '".$cat->id."' AND active = '1'")[0];
        $rating4= DB::select("SELECT count(id) as rating4_user FROM photo_reviews WHERE rating = '4' AND photo = '".$cat->id."' AND active = '1'")[0];
        $rating5= DB::select("SELECT count(id) as rating5_user FROM photo_reviews WHERE rating = '5' AND photo = '".$cat->id."' AND active = '1'")[0];
        if($rating5!==''){
            $a = $rating5->rating5_user;
            $response['rating5']=$rating5->rating5_user;

        }
        if($rating4!==''){
            $a = $rating4->rating4_user;
            $response['rating4']=$rating4->rating4_user;

        }
        if($rating3!==''){
            $a = $rating5->rating5_user;
            $response['rating3']=$rating3->rating3_user;

        }
        if($rating2!==''){
            $a = $rating2->rating2_user;
            $response['rating2']=$rating2->rating2_user;

        }
        if($rating1!==''){
            $a = $rating1->rating1_user;
            $response['rating1']=$rating1->rating1_user;

        }
        if($customer_id!==''){
            $response['add_a_review']='Add a Review';
        }
        else{
            $response['add_a_review']='Login';
        }
        $count=0;
        foreach ($reviews as $review) {
            $imgg ='';
            $usid = $review->name;
            $uimg = DB::select("SELECT * FROM customers WHERE id = '".$usid."'")[0];
            if($uimg->image != ''){
                $imgg=url('assets/user_image/' . $uimg->image);
            }
            else{
                $imgg=url('assets/user_image/user.png');
            }
            $rr = $review->rating; $i = 0; while($i<5){ $i++;
//                ($i<=$review->rating)

            }
            $response['review_photos'][] = array(
                'name' => $uimg->name,
                'review_time' => date('M d, Y',$review->time),
                'review_rating'=>$review->rating,
                'review'=>$review->review,
            );
            $count++;
        }

        foreach ($related as $rel) {
            $title='';
            substr(translate($rel->title), 0, 20);
            if(strlen($rel->title)>20){
                $title ='...';
            }
            $photos = array();
            $photos = DB::select('SELECT * FROM photos where gallery_id = '.$rel->id);
            $response['related_gallary'][] = array(
                'id'=>$rel->id,
                'slug' => $rel->slug,
                'title' => $rel->title,
                'image'=>url('/assets/images/gallery/'.$rel->image),
                'link'=>url('/photo-gallery/'.$rel->slug),
                'count'=>count($photos)
            );
        }
        $link = DB::table('link')->where('page', 'Photo Gallery')->first();
        DB::update("UPDATE photo_gallery SET visits = visits+1 WHERE id = '".$cat->id."'");

        return response()->json($response, 200);


    }


    public function blogs(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $response =array();
        $posts = DB::select("SELECT * FROM blog ORDER BY time DESC");
        foreach ($posts as $pos) {
            $response['blogs'][] = array(
                'id' => $pos->id,
                'title' => $pos->title,
                'time' => 'posted:'.timegap($pos->time).'ago',
                'visits' => $pos->visits,
                'link' =>url('blog/'.path($pos->title,$pos->id)),
                'image'=>url('assets/blog/' . $pos->images),
            );
        }
        return response()->json($response, 200);

    }

    public function layoutPlanDetail(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $response =array();
        if(!isset($request->slug)){
            return response()->json(['message' => 'slug_not_sent'], 404);
        }
        $slug = $request->slug;
        if($slug == ''){
            return response()->json(['message' => 'slug_empty'], 404);
        }
        $layout = false;
        $layout = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if(!$layout){
            return response()->json(['message' => 'plan_not_found'], 404);
        }
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
                $rcheck = DB::select("SELECT COUNT(*) as count FROM recently_viewed_layout WHERE user_id = ".$user_id." AND layout_id = ".$layout->id)[0]->count;
                if($rcheck == 0) {
                    $rvp = DB::insert("INSERT INTO recently_viewed_layout (user_id, layout_id) VALUES ('" . $user_id . "','" . $layout->id . "')");
                }
            }
        }
        $response['plan_details'] = array(
            'id' => $layout->id,
            'slug' => $layout->slug,
            'title' => $layout->title,
            'price' => $layout->price,
            'description' => $layout->description,
            'specification' => $layout->specification,
            'image' => url('/assets/layout_plans/'.$layout->images[0]),
        );
        $questions = DB::select("SELECT * FROM layout_plan_questions WHERE plan_id = '".$layout->id."' Order By priority ASC");
        $retQues = array();
        foreach($questions as $question){
            $retQues[] = array(
                'id' => $question->question_id,
                'question' => getQuestion($question->question_id),
                'type' => getQuestionType($question->question_id),
                'options' => json_decode(getQuestionOptions($question->question_id))
            );
        }
        $response['questions'] = $retQues;

        $addons = DB::select("SELECT * FROM layout_plan_addon WHERE layout_plan_id = '".$layout->id."'");
        $retAddons = array();
        foreach($addons as $addon){
            $add = DB::select("SELECT * FROM layout_addon WHERE id = '".$addon->layout_addon_id."'")[0];
            $retAddons[] = array(
                'id' => $addon->layout_addon_id,
                'name' => $add->name,
                'price' => $add->price
            );
        }
        $response['addons'] = $retAddons;

        $images = explode(',',$layout->images);
        $retImages = array();
        foreach ($images as $image){
            $retImages[] = url('/assets/layout_plans/'.$image);
        }
        $response['images'] = $retImages;

        $plan_liked = 0;
        if(customer('id') !== null) {
            $user_id = customer('id');
            $image_id = $layout->id;
            $img = DB::select("SELECT * FROM plan_like WHERE user_id = '" . $user_id . "' AND plan_id = '" . $image_id . "'");
            if(!empty($img)){
                $plan_liked = 1;
            }
        }
        $response['plan_liked'] = $plan_liked;

        $planAddedToProject = 0;
        if(customer('id') !== ''){
            $user_id = customer('id');
            $layout_id = $layout->id;

            $img = DB::select("SELECT * FROM layout_wishlist WHERE user_id = '".$user_id."' AND layout_id = '".$layout_id."'");

            if(!empty($img)){
                $planAddedToProject =1;
            }
        }
        $response['plan_added_to_project'] = $planAddedToProject;

        $amm = json_decode($layout->interior_ammenities);
        $retAmmenities = array();
        foreach($amm as $key=>$value) {
            $a = DB::select("SELECT * FROM amminities WHERE id = " . $key)[0];
            $retAmmenities[] = array(
                'id' => $key,
                'name' => $a->name,
                'value' => $value
            );
        }
        $response['ammenities'] = $retAmmenities;

        $plansets = array();
        if($layout->p1_name != ''){
            $plansets[] = array(
                'id' => 1,
                'name' => $layout->p1_name,
                'description' => $layout->p1_description,
                'duration' => $layout->p1_duration,
                'price' => $layout->p1_price,
            );
        }
        if($layout->p2_name != ''){
            $plansets[] = array(
                'id' => 2,
                'name' => $layout->p2_name,
                'description' => $layout->p2_description,
                'duration' => $layout->p2_duration,
                'price' => $layout->p2_price,
            );
        }
        if($layout->p3_name != ''){
            $plansets[] = array(
                'id' => 3,
                'name' => $layout->p3_name,
                'description' => $layout->p3_description,
                'duration' => $layout->p3_duration,
                'price' => $layout->p3_price,
            );
        }
        $response['plan_sets'] = $plansets;

        $retDrawings = array();
        $dv = json_decode($layout->drawing_views);
        if(count($dv) > 0 ){
            $fl = explode(',',$layout->file);
            foreach($dv as $key=>$value) {
                $retDrawings[] = array(
                    'title' => $value,
                    'file' => url('assets/layout_plans/'.$fl[$key])
                );
            }
        }
        $response['drawings'] = $retDrawings;


        $total_ratings = DB::select("SELECT COUNT(*) as count FROM layout_reviews WHERE active = 1 AND layout = ".$layout->id)[0]->count;
        $total_reviews = DB::select("SELECT COUNT(*) as count FROM layout_reviews WHERE active = 1 AND review <> '' AND layout = ".$layout->id)[0]->count;
        $rating = 0;
        if ($total_ratings > 0){
            $rating_summ = DB::select("SELECT SUM(rating) as sum FROM layout_reviews WHERE active = 1 AND layout = ".$layout->id)[0]->sum;
            $rating = $rating_summ / $total_ratings;
        }
        $reviews = DB::select("SELECT * FROM layout_reviews WHERE layout = ".$layout->id." AND active = 1 ORDER BY time DESC");
        $retReviews = array();
        foreach($reviews as $review){
            $usid = $review->name;
            $uimg = DB::select("SELECT * FROM customers WHERE id = '".$usid."'")[0];
            $retReviews[] = array(
                'id' => $review->id,
                'user_id' => $usid,
                'name' => $uimg->name,
                'image' => url('assets/user_image/'.$uimg->image),
                'review_date' => date('M d, Y',$review->time),
                'rating' => $review->rating,
                'review' => nl2br($review->review)
            );
        }
        $response['reviews'] = $retReviews;


        $faqs = DB::select("SELECT * FROM layout_faq WHERE layout_id = ".$layout->id." AND status = 'Approved' ORDER BY id DESC");
        $retFaq = array();
        foreach($faqs as $faq){
            $u = $faq->user_id;
            $user = DB::select("SELECT * FROM customers WHERE id = '$u'")[0];
            $retFaq[] = array(
                'id' => $faq->id,
                'user_id' => $u,
                'user_name' => $user->name,
                'date' => date('d M, Y', strtotime($faq->time)),
                'answer' => $faq->answer
            );
        }
        $response['faqs'] = $retFaq;

        $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM layout_reviews WHERE active = '1' AND layout = '".$layout->id."'")[0];
        $total_rating =  $rating->total_rating;
        $total_user = $rating->total_user;
        if($total_rating==0){
            $avg_rating = 0;
        }
        else{
            $avg_rating = round($total_rating/$total_user,1);
        }
        $rating1= DB::select("SELECT count(id) as rating1_user FROM layout_reviews WHERE rating = '1' AND layout = '".$layout->id."' AND active = '1'")[0]->rating1_user;
        $rating2= DB::select("SELECT count(id) as rating2_user FROM layout_reviews WHERE rating = '2' AND layout = '".$layout->id."' AND active = '1'")[0]->rating2_user;
        $rating3= DB::select("SELECT count(id) as rating3_user FROM layout_reviews WHERE rating = '3' AND layout = '".$layout->id."' AND active = '1'")[0]->rating3_user;
        $rating4= DB::select("SELECT count(id) as rating4_user FROM layout_reviews WHERE rating = '4' AND layout = '".$layout->id."' AND active = '1'")[0]->rating4_user;
        $rating5= DB::select("SELECT count(id) as rating5_user FROM layout_reviews WHERE rating = '5' AND layout = '".$layout->id."' AND active = '1'")[0]->rating5_user;

        $response['avg_rating'] = $avg_rating;
        $response['total_user'] = $total_user;
        $response['total_reviews'] = $total_reviews;
        $response['rating1'] = $rating1;
        $response['rating2'] = $rating2;
        $response['rating3'] = $rating3;
        $response['rating4'] = $rating4;
        $response['rating5'] = $rating5;

        $link = DB::table('link')->where('page', 'Single Plan')->first();
        $response['link'] = array();
        if(!empty($link)){
            $response['link'] = array(
                'image' => url('/assets/products/'.image_order($link->image)),
                'content' => $link->content,
                'link' => $link->link
            );
        }

        $related = DB::select("SELECT * FROM layout_plans WHERE id <> ".$layout->id." and category=".$layout->category." ORDER BY Rand() LIMIT 18");
        $retRelated = array();
        foreach($related as $rel){
            $amm = json_decode($rel->interior_ammenities);
            $ammenities = array();
            foreach($amm as $key=>$value) {
                $a = DB::select("SELECT * FROM amminities WHERE id = " . $key)[0];
                $ammenities[] = array(
                    'name' => $a->name,
                    'value' => $value
                );
            }
            $retRelated[] = array(
                'id' => $rel->id,
                'slug' => $rel->slug,
                'image' => url('/assets/layout_plans/'.image_order($rel->images)),
                'ammenities' => $ammenities
            );
        }
        $response['related'] = $retRelated;

        return response()->json(['message' => $response], 200);

    }


    public function advice(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $response =array();
        $check = DB::select("SELECT COUNT(*) as count FROM blog WHERE id = '".$request->cat."'")[0];
        if ($check->count == 0){
            abort(404);
        }
        $post = DB::select("SELECT * FROM blog WHERE id = '".$request->cat."'")[0];
        DB::update("UPDATE blog SET visits = visits+1 WHERE id = '".$post->id."'");
        $response['id'] = $post->id;
        $response['title'] = $post->title;
        $response['banner_image'] = url('assets/blog/'.$post->banner_image);
        $response['short_des'] = $post->short_des;
        $response['content'] = $post->content;
        $response['link_to_share'] = url('advice/'.path($post->title,$post->id));
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
//                dd($user_id);
                $pos = DB::select("SELECT * FROM advices_like WHERE user_id = '".$user_id."' AND advice_id = '".$post->id."'");
                if($pos) {
                    $response['like_value'] = 'Yes';
                }else {
                    $response['like_value'] = 'No';
                }
                $rcheck = DB::select("SELECT COUNT(*) as count FROM recently_viewed_advice WHERE user_id = ".$user_id." AND advice_id = ".$post->id)[0]->count;
                if($rcheck == 0) {
                    $rvp = DB::insert("INSERT INTO recently_viewed_advice (user_id, advice_id) VALUES ('" . $user_id . "','" . $post->id . "')");
                }
            }
        }
//        $response['header'] = $this->header(translate($post->title),$response['desc'],false,true,url('/assets/blog/'.$post->images));
        // Get blocs of the page builder
        $response['blocs'] = DB::select("SELECT * FROM blocs WHERE area = 'post' ORDER BY o ASC");
        $auditorials = DB::select("SELECT * FROM blog WHERE auditorial_pics = 'Yes' ORDER BY id DESC");
        foreach($auditorials as $audi){
            $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$audi->id)[0]->count;
            $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$audi->id)[0]->count;
            $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$audi->id)[0]->count;
            $rating = 0;
            if ($total_ratings > 0){
                $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$audi->id)[0]->sum;
                $rating = $rating_summ / $total_ratings;
            }
            $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$audi->id." AND active = 1 ORDER BY time DESC");
            $faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$audi->id." AND status = 'Approved' ORDER BY id DESC");
            $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$audi->id."'")[0];
            $total_rating =  $rating->total_rating;
            $total_user = $rating->total_user;
            if($total_rating==0){
                $avg_rating = 0;
            }
            else{
                $avg_rating = round($total_rating/$total_user,1);
            }
            $auditorial[] = array(
                'id' => $audi->id,
                'title' => $audi->title,
                'short_des' => $audi->short_des,
                'content' => $audi->content,
                'time' => $audi->time,
                'images' => url('assets/blog/'.$audi->images),
                'banner_image' => url('assets/blog/'.$audi->banner_image),
                'visits' => $audi->visits,
                'auditorial_pics' => $audi->auditorial_pics,
                'category' => $audi->category,
                'pro_cat' => $audi->pro_cat,
                'services' => $audi->services,
                'featured' => $audi->featured,
                'section' => $audi->section,
                'date' => $audi->date,
                'total_reviews' => $total_reviews,
                'total_rating' => $avg_rating,
                'likes' => $like,
            );
        }

        $response['auditorials'] = $auditorial;
        $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->count;
        $response['total_reviews'] = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$post->id)[0]->count;
        $response['like'] = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$post->id)[0]->count;
        $response['rating'] = 0;
        if ($total_ratings > 0){
            $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->sum;
            $response['rating'] = $rating_summ / $total_ratings;
        }
        $response['reviews'] = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$post->id." AND active = 1 ORDER BY time DESC");
        $response['faqs'] = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$post->id." AND status = 'Approved' ORDER BY id DESC");
        $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$post->id."'")[0];
        $total_rating =  $rating->total_rating;
        $total_user = $rating->total_user;
        if($total_rating==0){
            $response['avg_rating'] = 0;
        }
        else{
            $response['avg_rating'] = round($total_rating/$total_user,1);
        }
        $response['rating1']= DB::select("SELECT count(id) as rating1_user FROM advices_reviews WHERE rating = '1' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $response['rating2']= DB::select("SELECT count(id) as rating2_user FROM advices_reviews WHERE rating = '2' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $response['rating3']= DB::select("SELECT count(id) as rating3_user FROM advices_reviews WHERE rating = '3' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $response['rating4']= DB::select("SELECT count(id) as rating4_user FROM advices_reviews WHERE rating = '4' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $response['rating5']= DB::select("SELECT count(id) as rating5_user FROM advices_reviews WHERE rating = '5' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $link = DB::table('link')->where('page', 'Post')->first();
        $response['link'] = array();
        if(!empty($link)){
            $response['link'] = array(
                'image' => url('/assets/products/'.image_order($link->image)),
                'content' => $link->content,
                'link' => $link->link
            );
        }
        $related_advice = getRelatedAdvices($post->category, $post->id);
        foreach($related_advice as $rel){
            $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$rel->id)[0]->count;
            $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$rel->id)[0]->count;
            $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$rel->id)[0]->count;
            $rating = 0;
            if ($total_ratings > 0){
                $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$rel->id)[0]->sum;
                $rating = $rating_summ / $total_ratings;
            }
            $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$rel->id." AND active = 1 ORDER BY time DESC");
            $faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$rel->id." AND status = 'Approved' ORDER BY id DESC");
            $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$rel->id."'")[0];
            $total_rating =  $rating->total_rating;
            $total_user = $rating->total_user;
            if($total_rating==0){
                $avg_rating = 0;
            }
            else{
                $avg_rating = round($total_rating/$total_user,1);
            }
            $related_advices[] = array(
                'id' => $rel->id,
                'title' => $rel->title,
                'short_des' => $rel->short_des,
                'content' => $rel->content,
                'time' => $rel->time,
                'images' => url('assets/blog/'.$rel->images),
                'banner_image' => url('assets/blog/'.$rel->banner_image),
                'visits' => $rel->visits,
                'auditorial_pics' => $rel->auditorial_pics,
                'category' => $rel->category,
                'pro_cat' => $rel->pro_cat,
                'services' => $rel->services,
                'featured' => $rel->featured,
                'section' => $rel->section,
                'date' => $rel->date,
                'total_reviews' => $total_reviews,
                'total_rating' => $avg_rating,
                'likes' => $like,
            );
        }

        $response['related_advices'] = $related_advices;
        $response['cats'] = DB::select("SELECT * FROM advices_category WHERE parent = '0'");
        return response()->json($response, 200);

    }

    public function layoutReview(Request $request){
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        if (isset($request->review)){

            //dd('hello');
            // escape review details and insert them into the database
            //$name = escape(htmlspecialchars($_POST['name']));
            if(isset($request->rating)) {
                $rating = (int)$request->rating;
            }else {
                $rating = 0;
            }
            $review = escape(htmlspecialchars($request->review));
            $layout = (int)$request->layout_id;
//            $name = Customer('id');
            $ins = DB::insert("INSERT INTO layout_reviews (name,rating,review,layout,time,active) VALUE ('$user_id','$rating','$review','$layout','".time()."','0')");
            if($ins) {
                $response['message'] = 'Success';
                return response()->json($response, 200);
            }else {
                $response['message'] = 'Failed';
                return response()->json($response, 200);
            }
        }
    }

    public function photoReview(Request $request){

        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        if (isset($request->review)){

            //dd('hello');
            // escape review details and insert them into the database
            //$name = escape(htmlspecialchars($_POST['name']));
            if(isset($request->rating)) {
                $rating = (int)$request->rating;
            }else {
                $rating = 0;
            }
            $review = escape(htmlspecialchars($request->review));
            $photo_id = (int)$request->photo_id;
            $name = Customer('id');
            $ins = DB::insert("INSERT INTO photo_reviews (name,rating,photo,review,time,active) VALUE ('$user_id','$rating','$photo_id','$review','".time()."','0')");
            if($ins) {
                $response['message'] = 'Success';
                return response()->json($response, 200);
            }else {
                $response['message'] = 'Failed';
                return response()->json($response, 200);
            }
        }
    }

    public function adviceReview(Request $request){
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        if (isset($request->review)){

            //dd('hello');
            // escape review details and insert them into the database
            //$name = escape(htmlspecialchars($_POST['name']));
            if(isset($request->rating)) {
                $rating = (int)$request->rating;
            }else {
                $rating = 0;
            }
            $review = escape(htmlspecialchars($request->review));
            $advice_id = (int)$request->advice_id;
            $name = Customer('id');
            $ins = DB::insert("INSERT INTO advices_reviews (name,advice_id,rating,review,time,active) VALUE ('$user_id','$advice_id','$rating','$review','".time()."','0')");
            if($ins) {
                $response['message'] = 'Success';
                return response()->json($response, 200);
            }else {
                $response['message'] = 'Failed';
                return response()->json($response, 200);
            }
        }
    }

    public function productReview(Request $request){

        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        if (isset($request->review)){
            // escape review details and insert them into the database
//            $name = (int)$_POST['name'];
            $rating = (int)$request->rating;
            $review = escape(htmlspecialchars($request->review));
            $product = (int)$request->product_id;
            $ins = DB::insert("INSERT INTO reviews (name,rating,review,product,time,active) VALUE ('$user_id','$rating','$review','$product','".time()."','0')");
            if($ins) {
                $response['message'] = 'Success';
                return response()->json($response, 200);
            }else {
                $response['message'] = 'Failed';
                return response()->json($response, 200);
            }
        }
    }

    public function layoutFaq(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $layout_id = $request->layout_id;
        $question = $request->question;
//        $user_id = Customer('id');
        $status = 'Pending';

        $ins = DB::insert("INSERT INTO layout_faq (layout_id,user_id,question,status) VALUES ('$layout_id','$user_id','$question','$status')");
        if($ins) {
            $response['message'] = 'Success';
            return response()->json($response, 200);
        }else {
            $response['message'] = 'Failed';
            return response()->json($response, 200);
        }
    }

    public function advicesFaq(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $advice_id = $request->advice_id;
        $question = $request->question;
        $status = '0';

        $ins = DB::insert("INSERT INTO advices_faq (advice_id,user_id,question,status) VALUES ('$advice_id','$user_id','$question','$status')");
        if($ins) {
            $response['message'] = 'Success';
            return response()->json($response, 200);
        }else {
            $response['message'] = 'Failed';
            return response()->json($response, 200);
        }
    }

    public function productFaq(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $product_id = $request->product_id;
        $question = $request->question;
//        $user_id = Customer('id');
        $status = 'Pending';

        $ins = DB::insert("INSERT INTO product_faq (category,user_id,question,status) VALUES ('$product_id','$user_id','$question','$status')");
        if($ins) {
            $response['message'] = 'Success';
            return response()->json($response, 200);
        }else {
            $response['message'] = 'Failed';
            return response()->json($response, 200);
        }
    }

    public function servicesFaq(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $advice_id = $request->service_id;
        $question = $request->question;
        $status = '0';
        $ins = DB::insert("INSERT INTO faq (service_id,user_id,question,status) VALUES ('$advice_id','$user_id','$question','$status')");
        //$ins = DB::insert("INSERT INTO advices_faq (advice_id,user_id,question,status) VALUES ('$advice_id','$user_id','$question','$status')");
        if($ins) {
            $response['message'] = 'Success';
            return response()->json($response, 200);
        }else {
            $response['message'] = 'Failed';
            return response()->json($response, 200);
        }
    }

    public function advicelike(Request $request){
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $image_id = $request->advice_id;
        $advices = DB::insert("INSERT INTO advices_like (user_id, advice_id) VALUES ('".$user_id."','".$image_id."')");
        if($advices) {
            $response['message'] = 'Success';
            return response()->json($response, 200);
        }else {
            $response['message'] = 'Failed';
            return response()->json($response, 200);
        }
    }

    public function LikePlan(Request $request){
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $image_id = $request->plan_id;
        $images = DB::insert("INSERT INTO plan_like (user_id, plan_id) VALUES ('".$user_id."','".$image_id."')");
        if($images) {
            $response['message'] = 'Success';
            return response()->json($response, 200);
        }else {
            $response['message'] = 'Failed';
            return response()->json($response, 200);
        }
    }

    public function likeImage(Request $request){
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $image_id = $request->image_id;
        $images = DB::insert("INSERT INTO image_like (user_id, image_id) VALUES ('".$user_id."','".$image_id."')");
        if($images) {
            $response['message'] = 'Success';
            return response()->json($response, 200);
        }else {
            $response['message'] = 'Failed';
            return response()->json($response, 200);
        }
    }

    public function advicesave(Request $request){
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $image_id = $request->advice_id;
        $advices = DB::insert("INSERT INTO advices_save (user_id, advice_id) VALUES ('".$user_id."','".$image_id."')");
        if($advices) {
            $response['message'] = 'Success';
            return response()->json($response, 200);
        }else {
            $response['message'] = 'Failed';
            return response()->json($response, 200);
        }
    }

    public function addToProject(Request $request){
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $image_id = $request->image_id;
        $project_id = $request->project_id;
        $images = DB::insert("INSERT INTO image_inspiration (user_id, image_id,project_id) VALUES ('".$user_id."','".$image_id."','".$project_id."')");
        if($images) {
            $response['message'] = 'Success';
            return response()->json($response, 200);
        }else {
            $response['message'] = 'Failed';
            return response()->json($response, 200);
        }
    }

    public function userAdvices(Request $request)
    {
		
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
        $saved = DB::select("SELECT * FROM advices_save WHERE user_id = ".$user_id." ORDER BY id DESC ");
        foreach($saved as $save) {
            $posts = DB::select("select * from blog where id = " . $save->advice_id . " order by time DESC");

            if (empty($saved)) {
                $response['saved'] = [];
            }
            foreach ($posts as $post) {
                $response['saved'][] = array(
                    'id' => $post->id,
                    'name' => $post->title,
                    'short_description' => $post->short_des,
                    'content' => $post->content,
                    'time' => $post->time,
                    'visits' => $post->visits,
                    'image' => url('assets/blog/' . $post->images)
                );
            }
        }
        $liked = DB::select("SELECT * FROM advices_like WHERE user_id = ".$user_id." ORDER BY id DESC ");

        foreach($liked as $like) {
            $postsl = DB::select("select * from blog where id = " . $like->advice_id . " order by time DESC");
            if (empty($saved)) {
                $response['liked'] = [];
            }
            foreach ($postsl as $postl) {
                $response['liked'][] = array(
                    'id' => $postl->id,
                    'name' => $postl->title,
                    'short_description' => $postl->short_des,
                    'content' => $postl->content,
                    'time' => $postl->time,
                    'visits' => $postl->visits,
                    'image' => url('assets/blog/' . $postl->images)
                );
            }
        }
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        return response()->json($response, 200);
    }

    public function forgotPassword(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $email = escape($request->email);
        if(empty($email)){
            $response['message'] = 'Email required';
            return response()->json($response, 200);
        } else {
            if(DB::select("SELECT COUNT(*) as count FROM customers WHERE email = '".$email."' ")[0]->count > 0) {
                $customer = DB::select("SELECT id,name FROM customers WHERE email = '".$email."'")[0];
                $key = md5(uniqid());
                DB::insert("INSERT INTO password_resets (customer,`key`) VALUE ('".$customer->id."','".$key."')");
                $mail = mailing('reset-password',array('link'=>url('reset-password/'.$key)),'Reset your password',$email);
                if($mail) {
                    $response['message'] = 'Mail sent successfully';
                    return response()->json($response, 200);
                }else {
                    $response['message'] = 'Failed';
                    return response()->json($response, 200);
                }
            } else {
                $response['message'] = 'This account does not exist';
                $error = 'This account does not exist';
                return response()->json($response, 200);
            }

        }
    }

    public function getRecentViewedProduct(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
                $date = new Carbon; //  DateTime string will be 2014-04-03 13:57:34

                $date->subWeek();
                $rvp = DB::table('recently_viewed_product')->where('user_id', $user_id)
                    ->skip(0)->take(20)
                    ->orderBy('id', 'DESC')
                    ->get();
                //dd($rvp);

                $cps = array();
                foreach ($rvp as $cp) {
                    if($cp->product_id !== 0) {
                        $pcp = DB::table('products')->where('id', $cp->product_id)->first();
                        $cps[] = array(
                            'id' => $pcp->id,
                            'title' => translate($pcp->title),
                            'image' => url('/assets/products/' . image_order($pcp->images)),
                            'price' => $pcp->price
                        );
                    }
                }
                $response['product'] = $cps;
                $response['message'] = 'Data sent';
                return response()->json($response, 200);
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
    }

    public function getRecentViewedAdvice(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
                $date = new Carbon; //  DateTime string will be 2014-04-03 13:57:34

                $date->subWeek();
                $rvp = DB::table('recently_viewed_advice')->where('user_id', $user_id)
                    ->skip(0)->take(20)
                    ->orderBy('id', 'DESC')
                    ->get();
                //dd($rvp);

                $cps = array();
                foreach ($rvp as $cp) {
                    if($cp->advice_id !== 0) {
                        $pcp = DB::table('blog')->where('id', $cp->advice_id)->first();
                        $cps[] = array(
                            'id' => $pcp->id,
                            'title' => $pcp->title,
                            'short_des' => $pcp->short_des,
                            'content' => $pcp->content,
                            'time' => $pcp->time,
                            'images' => url('assets/blog/'.$pcp->images),
                            'banner_image' => url('assets/blog/'.$pcp->banner_image),
                            'visits' => $pcp->visits,
                            'auditorial_pics' => $pcp->auditorial_pics,
                            'category' => $pcp->category,
                            'pro_cat' => $pcp->pro_cat,
                            'services' => $pcp->services,
                            'featured' => $pcp->featured,
                            'section' => $pcp->section,
                            'date' => $pcp->date,
                        );
                    }
                }
                $response['advice'] = $cps;
                $response['message'] = 'Data sent';
                return response()->json($response, 200);
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
    }

    public function getRecentViewedLayout(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
                $date = new Carbon; //  DateTime string will be 2014-04-03 13:57:34

                $date->subWeek();
                $rvp = DB::table('recently_viewed_layout')->where('user_id', $user_id)
                    ->skip(0)->take(20)
                    ->orderBy('id', 'DESC')
                    ->get();
                //dd($rvp);

                $cps = array();
                foreach ($rvp as $cp) {
                    if($cp->layout_id !== 0) {
                        $pcp = DB::table('layout_plans')->where('id', $cp->layout_id)->first();
                        $img = explode(',',$pcp->images);
                        $cps[] = array(
                            'id' => $pcp->id,
                            'slug' => $pcp->slug,
                            'title' => $pcp->title,
                            'price' => $pcp->price,
                            'description' => $pcp->description,
                            'specification' => $pcp->specification,
                            'image' => url('/assets/layout_plans/'.$img[0]),
                        );
                    }
                }
                $response['advice'] = $cps;
                $response['message'] = 'Data sent';
                return response()->json($response, 200);
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
    }

    public function getRecentViewedGallery(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
                $date = new Carbon; //  DateTime string will be 2014-04-03 13:57:34

                $date->subWeek();
                $rvp = DB::table('recently_viewed_photo_gallery')->where('user_id', $user_id)
                    ->skip(0)->take(20)
                    ->orderBy('id', 'DESC')
                    ->get();
                //dd($rvp);

                $cps = array();
                foreach ($rvp as $cp) {
                    if($cp->gallery_id !== 0) {
                        $pcp = DB::table('photo_gallery')->where('id', $cp->gallery_id)->first();
                        $response['gallary'][] = array(
                            'slug' => $pcp->slug,
                            'title' => $pcp->title,
                            'image'=>url('assets/images/gallery/' . $pcp->image),
                            'link'=>$pcp->slug,
                            'count'=>getPlanPhotsCount($pcp->id)
                        );
                    }
                }
                $response['advice'] = $cps;
                $response['message'] = 'Data sent';
                return response()->json($response, 200);
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
    }

    public function userDesignes(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $user_id = '';
        if(isset($request->token)){
            $token = escape(htmlspecialchars($request->token));
            $user = DB::table('customers')->where('appid', $token)->first();
            if($user === null){
                return response()->json(['response' => 'User not authorize'], 401);
            }else{
                $user_id = $user->id;
            }
        }else{
            return response()->json(['response' => 'Access Denied'], 401);
        }
//        $saved = DB::select("SELECT * FROM advices_save WHERE user_id = ".$user_id." ORDER BY id DESC ");
//        foreach($saved as $save) {
//            $posts = DB::select("select * from blog where id = " . $save->advice_id . " order by time DESC");
//
//            if (empty($saved)) {
//                $response['saved'] = [];
//            }
//            foreach ($posts as $post) {
//                $response['saved'][] = array(
//                    'id' => $post->id,
//                    'name' => $post->title,
//                    'short_description' => $post->short_des,
//                    'content' => $post->content,
//                    'time' => $post->time,
//                    'visits' => $post->visits,
//                    'image' => url('assets/blog/' . $post->images)
//                );
//            }
//        }
        $liked = DB::select("SELECT * FROM plan_like WHERE user_id = ".$user_id." ORDER BY id DESC ");

        foreach($liked as $like) {
            $postsl = DB::select("select * from layout_plans where id = " . $like->plan_id);
            if (empty($saved)) {
                $response['liked'] = [];
            }
            foreach ($postsl as $postl) {
                //dd($postl);
                $ij = array();
                $img = explode(',',$postl->images);
                //dd($ij);
                $response['liked'][] = array(
                    'id' => $postl->id,
                    'slug' => $postl->slug,
                    'title' => $postl->title,
                    'price' => $postl->price,
                    'description' => $postl->description,
                    'specification' => $postl->specification,
                    'image' => url('/assets/layout_plans/'.$img[0]),
                );
            }
        }
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        return response()->json($response, 200);
    }

}
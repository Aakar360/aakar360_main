<?php
namespace App\Http\Controllers\API;
use App\ManufacturingHubs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Bookings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\DB;
use DateTime;
use Image;

class InstitutionalController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    public function nav(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
            $response = array();
            $pcats = DB::table('category')->where('parent', 0)->where('is_view', 0)->orderBy('name', 'ASC')->get();
            if ($pcats !== null) {
                foreach ($pcats as $pcat) {
                    $pscats = getProductCategories($pcat->id);
                    $sub = array();
                    $sub['sub_nav'] = array();
                    if ($pscats) {
                        foreach ($pscats as $pscat) {
                            $sub['sub_nav'][] = array(
                                'id' => $pscat->id,
                                'name' => $pscat->name
                            );
                        }
                    }
                    $response['nav'][] = array(
                        'id' => $pcat->id,
                        'name' => $pcat->name,
                        'sub' => $sub,
                        'image' => url('assets/products/' . $pcat->image)
                    );
                }
            }
            return response()->json($response, 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function index(Request $request)
    {
        if (isset($request->API_KEY) || isset($request->token)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
            $id = '';
            $i_count =0;
            $b_count =2;
            $r_count =3;
            $version_code =1.0;
          if(isset($request->token)){
              if(!empty($request->token)){
                  $user = Customer::where('appid', $request->token)->first();
                  $notification_read = DB::table('notification_read')->where('customer_id','=',$user->id)->first();
                  $booking = DB::table('booking_count')->where('customer_id','=',$user->id)->first();
                  $rfq = DB::table('rfq_count')->where('customer_id','=',$user->id)->first();
				  if($notification_read !== null){
					  if($notification_read->note_read > 0){
						  $i_count = $notification_read->note_read;
					  }
				  }
                  else{
                      $i_count =0;
                  }
                  if($booking){
                      if($booking->booking_counter > 0){
                          $b_count = $booking->booking_counter;
                      }
                      else{
                          $b_count =0;
                      }
                  }
                  else{
                      $b_count =0;
                  }
                  if($rfq){
                      if($rfq->rfq_counter > 0){
                          $r_count = $rfq->rfq_counter;
                      }
                      else{
                          $r_count =0;
                      }
                  }
                  else{
                      $r_count =0;
                  }
                  $version_code =1.0;
              }
              else{
                  $i_count =0;
                  $b_count =0;
                  $r_count =0;
                  $version_code =1.0;
              }
          }
          else{
              $i_count =0;
              $b_count =0;
              $r_count =0;
              $version_code =1.0;
          }
            $response = array();
            $response['noti_count'] = $i_count;
            $response['booking_count'] = $b_count;
            $response['rfq_count'] = $r_count;
            $response['version_code'] = $version_code;
            $banners = DB::table('banner')->where('section', 'institutional_home')->orderBy('priority', 'ASC')->get();
            foreach ($banners as $banner) {
                $response['banners'][] = array(
                    'title' => $banner->title,
                    'description' => $banner->short_description,
                    'image' => url('assets/banners/' . $banner->image)
                );
            }
            $pcs = getPCategories();
            foreach ($pcs as $pc) {
                $response['pcs'][] = array(
                    'id' => $pc->id,
                    'title' => $pc->name,
                    'image' => url('assets/products/' . $pc->image)
                );
            }
            $pcss = DB::table('category')->where('popular', 1)->where('is_view', 0)->orderBy('popular_priority', 'ASC')->skip(0)->take(5)->get();
            $i = 0;
            foreach ($pcss as $pcsss) {
                $pcp = DB::table('products')->where('category', $pcsss->id)->orderBy('id', 'DESC')->skip(0)->take(6)->get();
                $cps = array();
                foreach ($pcp as $cp) {
                    $cps[] = array(
                        'id' => $cp->id,
                        'title' => translate($cp->title),
                        'image' => url('/assets/products/' . image_order($cp->images)),
                        'price' => $cp->price
                    );
                }
                $response['pcss'][] = array(
                    'id' => $pcsss->id,
                    'title' => $pcsss->name,
                    'products' => $cps
                );
                $i++;
            }
            $response['skip'] = $i;
            $top_brands = DB::table('brand')->orderBy('name', 'ASC')->get();
			$tpb = array();
			$category = DB::table('category')->get();
            if($category) {
				foreach($category as $catBrand){
					if($catBrand->brands !== '') {
						$tpb[] =  $catBrand->brands;
					}
				}
				$tpbi =  implode(',',$tpb);
				$tpbrands =  '('.$tpbi.')';
                $top_brands = DB::select("SELECT * FROM brand WHERE id IN ".$tpbrands." ORDER BY name ASC");
            }
            foreach ($top_brands as $tb) {
                $response['tb'][] = array(
                    'id' => $tb->id,
                    'image' => url('assets/brand/' . $tb->image)
                );
            }
            return response()->json($response, 200);
        }
    }

    public function getCategory(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
            $customer_id = '';
            $user_type = '';
            if (isset($request->token)) {
                $customer_id = $request->token;
				$user = Customer::where('appid', $request->token)->first();
				if ($user===null) {
					return response()->json(['message' => 'login'], 200);
				}
                $user_type = $user->user_type;
            }
            if (isset($request->cid)) {
                $cat = $request->cid;
                $category = DB::table('category')->where('id', $cat)->first();
                if ($category === null) {
                    return response()->json(['response' => 'Category Not Found'], 404);
                }
                $response['show_filter'] = false;
                $response['show_products'] = false;
                $response['suggested'] = [];
                $response['title'] = translate($category->name);
                if ($category->banner !== '') {
                    $response['banner'] = url('assets/products/' . $category->banner);
                } else {
                    $response['banner'] = url('assets/images/background/2.jpg');
                }
                $pdata = DB::table('category')->where('parent', $category->id)->count();
                if ($pdata == 0) {
                    $response['show_products'] = true;
                }
                $top_brands = array();
                if ($category->brands !== '') {
                    $tpbrands = explode(',', $category->brands);
                    $top_brands = DB::table('brand')->whereIn('id', $tpbrands)->orderBy('name', 'ASC')->get();
                }
                foreach ($top_brands as $tb) {
                    $response['top_brands'][] = array(
                        'id' => $tb->id,
                        'image' => url('assets/brand/' . $tb->image)
                    );
                }
                $p_categories = DB::table('category')->where('parent', $category->id)->get();
                $response['p_categories'] = array();
                foreach ($p_categories as $p_category) {
                    $response['p_categories'][] = array(
                        'id' => $p_category->id,
                        'title' => $p_category->name,
                        'image' => url('/assets/products/' . image_order($p_category->image))
                    );
                }
                $bests = DB::table('best_for_category')->where('category', $category->id)->orderBy('id', 'DESC')->get();
                $response['bests'] = [];
                foreach ($bests as $best) {
                    $response['bests'][] = array(
                        'id' => $best->id,
                        'image' => url('/assets/products/' . image_order($best->image)),
                        'content' => $best->content
                    );
                }
                $faqs = DB::select("SELECT * FROM product_faq WHERE section='institutional' AND FIND_IN_SET (" . $category->id . ", category) ORDER BY id DESC LIMIT 5");
                foreach ($faqs as $faq) {
                    $response['faqs'][] = array(
                        'id' => $faq->id,
                        'question' => $faq->question,
                        'date' => date('d M, Y', strtotime($faq->time)),
                        'answer' => $faq->answer
                    );
                }
                $filters = false;
                $cfc = $category->filter;
                if ($cfc !== '') {
                    $ccx = explode(',', $cfc);
                    foreach ($ccx as $cx) {
                        $filters = DB::select("SELECT * FROM filter WHERE id = '$cx'");
                    }
                } else {
                    $filters = false;
                }
                if ($filters) {
                    foreach ($filters as $filter) {
                        $fos = array();
                        $fil_options = DB::table('filter_options')->where('filter_id', $filter->id)->get();
                        foreach ($fil_options as $fo) {
                            $fos[] = array(
                                'id' => $fo->id,
                                'name' => $fo->name
                            );
                        }
                        $response['filters'][] = array(
                            'id' => $filter->id,
                            'name' => $filter->name,
                            'options' => $fos
                        );
                    }
                }
                $cid[] = $category->id;
                $c = array();
                $cont = true;
                $temp = $cid;
                while ($cont) {
                    $c = array_pluck(DB::table('category')->whereIn('parent', $temp)->get(), 'id');
                    if (count($c)) {
                        $cid = array_merge($cid, $c);
                        $temp = $c;
                    } else {
                        $cont = false;
                    }
                }
                $products = DB::table('products')->whereIn('category', $cid);
                $price['set_min'] = 0;
                $price['set_max'] = 0;
                $p_min = DB::select("SELECT MIN(price) as min_price, MAX(price) as max_price FROM products where category IN (" . implode(',', $cid) . ")");
                if (count($p_min) > 0) {
                    $price['min'] = $p_min[0]->min_price;
                    $price['set_min'] = $p_min[0]->min_price;
                    $price['max'] = $p_min[0]->max_price;
                    $price['set_max'] = $p_min[0]->max_price;
                } else {
                    $price['min'] = 0;
                    $price['set_min'] = 0;
                    $price['max'] = 0;
                }
                $bids = array();
                if (isset($request->brands)) {
                    $bids = explode(',', $request->brands);
                }
                if (isset($request->min)) {
                    $price['set_min'] = $request->min;
                    $products = $products->where('price', '>=', $request->min);
                }
                if (isset($request->max)) {
                    $price['set_max'] = $request->max;
                    $products = $products->where('price', '<=', $request->max);
                }
                if (count($bids) > 0) {
                    $products = $products->whereIn('brand_id', $bids);
                }
                if (isset($request->hub)) {
                    $ph_products = array();
                    $ph_relations = DB::table('ph_relations')->get();
                    foreach($ph_relations as $ph){
                        $hub_ids = json_decode($ph->hubs);
                        foreach($hub_ids as $hub_id){
                            if($hub_id->hub == $request->hub){
                                $ph_products[] = $ph->products;
                            }
                        }
                    }
                    $products = $products->whereIn('id', $ph_products);
                }
                $products = $products->get();
                $response['products'] = array();
                $pvalidity = 0;
                foreach ($products as $product) {
					$pr = getInstitutionalPrice($product->id,$customer_id,$request->hub, false, false);
                    if($user_type== "institutional"){
                        $pvalidity = $pr['timer'];
                    }
                    elseif($user_type!== "institutional" && $product->validity!==null){
                        date_default_timezone_set("Asia/Kolkata");
                        $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
                        if($pvalidity <= 0){
                            $pvalidity = 0;
                        }
                    }
					$user = Customer::where('appid', $request->token)->first();
                    if(is_array($pr)) {
                        $response['products'][] = array(
                            'id' => $product->id,
                            'title' => $product->title,
                            'image' => url('/assets/products/' . image_order($product->images)),
                            'price' => $pr['price'],
                            'loading' => $pr['loading'],
                            'tax_per' => $product->tax,
                            'tax' => $pr['tax'],
                            'amount' => $pr['total'],
                            'rates' => getRatings($product->id),
                            'validity' => $pvalidity
                        );
                    }
                }
                $sg = DB::select("SELECT product_id from featured_products where category_id = " . $category->id);
                $response['suggested'] = array();
                if (count($sg) > 0) {
                    $sug_prods = $sg[0]->product_id;
                    $suggested = DB::select("SELECT * FROM products WHERE id in (" . $sug_prods . ")");
                    foreach ($suggested as $sg) {
                        $prsg = getInstitutionalPrice($sg->id,$customer_id,$request->hub, false, false);
                        if(is_array($prsg)) {
                            $response['suggested'][] = array(
                                'id' => $sg->id,
                                'title' => $sg->title,
                                'image' => url('/assets/products/' . image_order($sg->images)),
                                'price' => $prsg['pprice'],
                                'loading' => $prsg['loading'],
                                'tax_per' => $sg->tax,
                                'tax' => $prsg['tax'],
                                'amount' => $prsg['total'],
                                'rates' => getRatings($sg->id)
                            );
                        }
                    }
                }
				if($category->brands !== ''){
					$brands = DB::select("SELECT * FROM brand where id In (" . $category->brands . ") ORDER BY id DESC ");
					foreach ($brands as $brand) {
						$response['f_brands'][] = array(
							'id' => $brand->id,
							'name' => $brand->name
						);
					}
				}
                return response()->json($response, 200);
            } else {
                return response()->json(['error' => 'Category Not Found'], 404);
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function getCategory1(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'llll'], 401);
            }
            $ret = array();
            $unit_cat =array();
            if (isset($request->token)) {
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login'], 401);
                }
                $cats = DB::table('category')->select('id','name','weight_unit')->orderBy('name')->get();
                foreach ($cats as $cat) {
                    $wunits = DB::table('units')->whereIn('id', explode(',', $cat->weight_unit))->get();
					$unit_cat = array();
					foreach ($wunits as $unit){
                        $unit_cat[] = array(
                            'id' => $unit->id,
                            'symbol' => $unit->symbol
                        );
                    }
                    $ret[] = array(
                        'id' => $cat->id,
                        'name' => $cat->name,
                        'unit' => $unit_cat
                    );
                }
                return response()->json(['message' => $ret], 200);
            } else {
                return response()->json(['error' => 'Category Not Found'], 404);
            }
        } else {
            return response()->json(['error' => 'll'], 401);
        }
    }

    public function getProductsByCat(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'unautorised'], 401);
            }
            $ret = array();
            if (isset($request->token)) {
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login'], 401);
                }
                $cid  = explode(',', $request->cat_id);
                $brands = DB::table('products')->whereIn('category',$cid)->get();
                foreach ($brands as $brand) {
                    $ret[][] = array(
                        $request->cat_id  =>array(
                            'id' => $brand->id,
                            'name' => $brand->title
                        ),
                    );
                }
                return response()->json(['message' => $ret], 200);
            } else {
                return response()->json(['error' => 'Brand Not Found'], 404);
            }
        } else {
            return response()->json(['error' => 'unautorised'], 401);
        }
    }

    public function getBrand(Request $request)
    {
        if (isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'unautorised'], 401);
            }
            $ret = array();
            if (isset($request->token)) {
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login'], 401);
                }
                $cid  = explode(',', $request->cat_id);
                $brands = DB::table('products')->where('category',$cid)->get();
                foreach ($brands as $brand) {
                    $ret[] = array(
                        'id' => $brand->brand_id,
                        'name' => $brand->title
                    );
                }
                return response()->json(['message' => $ret], 200);
            } else {
                return response()->json(['error' => 'Brand Not Found'], 404);
            }
        } else {
            return response()->json(['error' => 'unautorised'], 401);
        }
    }

    public function product(Request $request)
    {
        if (isset($request->API_KEY)) {
            $customer_id = '';
            $user_type = '';
            $user_id = '';
            $credit_charges = '';
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
            if (isset($request->token)){
                $customer_id = $request->token;
				$user = Customer::where('appid', $request->token)->first();
				if ($user===null) {
					return response()->json(['message' => 'login'], 200);
				}
                $user_type = $user->user_type;
                $user_id = $user->id;
                $credit_charges = $user->credit_charges;
            }
            if (isset($request->pid)) {
                $product = DB::table('products')->where('id', $request->pid)->first();
                $cat = DB::table('category')->where('id', '=', $product->category)->first();
                $wunits = DB::table('units')->where('id', $product->weight_unit)->first();
                if ($product === null) {
                    return response()->json(['error' => 'Invalid product id'], 404);
                } else {
                    $category = DB::table('category')->where('id', $product->category)->first();
                    $variants = DB::table("product_variants")
						->where('display', '1')
						->where('product_id', $product->id)
						->join('size','size.id','=','product_variants.variant_title')
						->select(['product_variants.*'])
						->orderBy('size.priority')
						->get();
                    $bulk_discounts = DB::table("product_discount")->where('product_id', $product->id)->orderBy('quantity', 'ASC')->get();
                    $total_ratings = DB::table("reviews")->where('active', 1)->where('product', $product->id)->count();
                    $rating = 0;
                    if ($total_ratings > 0) {
                        $rating_summ = DB::table("reviews")->where('active', 1)->where('product', $product->id)->sum('rating');
                        $rating = $rating_summ / $total_ratings;
                    }
                    $total_reviews = DB::table("reviews")->where('active', 1)->where('review', '<>', '')->where('product', $product->id)->count();
                    $ratinga = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM reviews WHERE active = '1' AND product = '" . $product->id . "'")[0];
                    $total_rating = $ratinga->total_rating;
                    $total_user = $ratinga->total_user;
                    if ($total_rating == 0) {
                        $avg_rating = 0;
                    } else {
                        $avg_rating = round($total_rating / $total_user, 1);
                    }
                    $rating1 = DB::select("SELECT count(id) as rating1_user FROM reviews WHERE rating = '1' AND product = '".$product->id."' AND active = '1'")[0];
                    $rating2 = DB::select("SELECT count(id) as rating2_user FROM reviews WHERE rating = '2' AND product = '".$product->id . "' AND active = '1'")[0];
                    $rating3 = DB::select("SELECT count(id) as rating3_user FROM reviews WHERE rating = '3' AND product = '" . $product->id . "' AND active = '1'")[0];
                    $rating4 = DB::select("SELECT count(id) as rating4_user FROM reviews WHERE rating = '4' AND product = '" . $product->id . "' AND active = '1'")[0];
                    $rating5 = DB::select("SELECT count(id) as rating5_user FROM reviews WHERE rating = '5' AND product = '" . $product->id . "' AND active = '1'")[0];
                    $imagesx = explode(',', $product->images);
                    $images = array();
                    foreach ($imagesx as $imagex) {
                        $images[] = array(
                            'image' => url('/assets/products/' . $imagex),
                            'thumb' => url('/assets/products/thumbs/' . $imagex)
                        );
                    }
                    $reviews = DB::select("SELECT * FROM reviews where product='" . $product->id . "' AND  active=1 order by time DESC");
                    $related_products = getRelatedProductsWithHub($product->category, $product->id,$request->hub);
                    $p_id = $product->id;
                    $rp = array();
					$rp_price = null;
					$pvalidity = 0;
                    foreach ($related_products as $related_product) {
						if($user_type!== "institutional" && $product->validity!==null){
							date_default_timezone_set("Asia/Kolkata");
							$pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
							if($pvalidity <= 0){
								$pvalidity = 0;
							}
						}
						$productPrice = '';
						$user = Customer::where('appid', $request->token)->first();
						if ($user===null) {
							$productPrice = 'Login to view price';
						}elseif($pvalidity <= 0){
							$productPrice = 'Waiting for update';
						}else{
							$productPrice = $product->purchase_price;
						}
						$rp[] = array(
							'id' => $related_product->id,
							'sku' => $related_product->sku,
							'title' => $related_product->title,
							'category' => $related_product->category,
							'institutional_price' => $productPrice,
							'images' => url('/assets/products/'.image_order($related_product->images)),
							'text' => $related_product->text,
							'status' => $related_product->status
						);
                    }
                    $sellers = DB::select("SELECT c.*,p.* FROM `products` as p INNER JOIN customers as c on p.added_by = c.id WHERE p.sku = '$product->sku' limit 2");
                    $faqs = DB::select("SELECT * FROM product_faq WHERE section='institutional' AND status = 1 ORDER BY id DESC");
                    $link = DB::table('link')->where('page', 'Single Product')->first();
                    $total = '';
                    if ($link !== null) {
                        $link->image = url('assets/products/' . $link->image);
                    }
                        $pri = getInstitutionalPrice($product->id,$customer_id,$request->hub,false,false);
                    if(is_numeric($pri['total'])){
                        $total = round($pri['total']);
                    }
                    else{
                        $total = $pri['total'];
                    }
                    $product->institutional_price = $total;
                    $product->price_detail = getInstitutionalPrice($product->id,$customer_id,$request->hub,false,false);
                    $vr = [];
                    foreach ($variants as $variant){
                        $title = DB::table('size')->where('id',$variant->variant_title)->first();
						//DB::select("SELECT * FROM size WHERE id = $variant->variant_title");
                        $ms = DB::table('multiple_size')->where('id',$variant->price)->first();
                        $tn = '';
                        if($title){
                            $tn = $title->name;
							$variant->variant_title_id = $title->id;
							$variant->variant_title = $title->name;
                        }
                        $var_price = getInstitutionalPriceFancy($product->id,$customer_id,$request->hub, $variant->id,false,false);
                        if($request->hub === null) {
                            $variant->variant_price = round($var_price['var_price']);
							$variant->price = round($var_price['total']);
							$variant->sale_price = round($var_price['total_sale']);
                        }else{
							if($ms !== null){
								if($ms->price === null){
									$variant->price = 0;
								}else{
									$variant->price = $ms->price;
								}
							}
						}
                        $variant->price_detail = getInstitutionalPrice($product->id,$customer_id,$request->hub, $variant->id,false);
						if($product->show_weight_length == 1){
							$sw = DB::table('size_weight')->where('size_id',$title->id)->first();
							if($sw){
								$variant->variant_length = $sw->length;
								$variant->variant_length_unit = getUnitSymbol($sw->lunit);
								$variant->variant_weight = $sw->weight;
								$variant->variant_weight_unit = getUnitSymbol($sw->wunit);
							}
						}
                        $vr[] = (array)$variant;
                    }
                    $unit_cat =array();
					if($wunits !== null){
						$unit_cat[] = array(
							'id' => $wunits->id,
							'symbol' => $wunits->symbol
						);
					}
                    $sp = 0;
                    $rfq =0;
                    $pvalidity = 0;
                    if (isset($request->token)) {
                        $user = Customer::where('appid', $request->token)->first();
                        if ($user === null) {
                            return response()->json(['message' => 'login'], 401);
                        }
                        if($user->show_price==1 && $product->product_showprice==1){
                            $sp =1;
                        }
                        else{
                            $sp = 0;
                        }
                        if($product->rfq==1 && $user->user_type == "institutional"){
                            $rfq =1;
                        }
                        else{
                            $rfq =0;
                        }
                        if($user->user_type== "institutional"){
                            foreach ($variants as $variant) {
                                $institute_time = getInstitutionalPrice($product->id, $customer_id, $request->hub, $variant->id,false,false);
                                $pvalidity = $institute_time['timer'];
                            }
                        }
                        elseif($user->user_type!== "institutional" && $product->validity!==null){
                            date_default_timezone_set("Asia/Kolkata");
                            $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
                            if($pvalidity <= 0){
                                $pvalidity = 0;
                            }
                        }
                    }
                    $booked = 0;
                    $latest = DB::table('bookings')->where('product_id','=',$request->pid)->orderBy('created_at', 'desc')->first();
                    if($latest!==null){
                        $items = json_decode($latest->items);
                        foreach ($items as $key => $val){
                            $booked = $booked + $val;
                        }
                    }
                    $product_quantity = $product->quantity;
                    $total_quantity = 0;
					$credit_charges_list = array();
					if($credit_charges == 1){
						$creditCharges = DB::table('credit_charges')->get();
						foreach ($creditCharges as $cc) {
							$credit_charges_list[] = array(
								'id' => $cc->id,
								'days' => $cc->days,
								'amount' => $cc->amount
							);
						}
					}
					$counter_status = 0;
					$counter_id = 0;
					$counter_price = 0;
					$counter_quantity = 0;
					$counter_unit = '';
					$cs = DB::table('counter_offer')->where('user_id',$user_id)->where('product_id','=',$request->pid)->where('hub_id','=',$request->hub)->orderBy('id', 'desc')->first();
					if($cs !== null){
						$counter_id = $cs->id;
						$counter_status = $cs->status;
						$counter_price = $cs->price;
						$counter_quantity = $cs->quantity;
						$counter_unit = $cs->unit;
					}
                    $response = array(
                        'product' => (array)$product,
                        'category' => (array)$category,
                        'variants' => (array)$vr,
                        'rating' => $rating,
                        'total_rating' => $total_ratings,
                        'average_rating' => $avg_rating,
                        'rating_list' => array('rate1' => (array)$rating1, 'rate2' => (array)$rating2, 'rate3' => (array)$rating3, 'rate4' => (array)$rating4, 'rate5' => (array)$rating5),
                        'images' => (array)$images,
                        'reviews' => (array)$reviews,
                        'related_products' => (array)$rp,
                        'sellers' => (array)$sellers,
                        'faqs' => (array)$faqs,
                        'link' => (array)$link,
                        'show_price' => $sp,
                        'unit' => $unit_cat,
                        'stock'=> $product_quantity-$booked,
                        'rfq' => $rfq,
                        'validity'=>$pvalidity,
						'description_view'=>$product->description_view,
						'specification_view'=>$product->specification_view,
						'rating_view'=>$product->rating_view,
						'faq_view'=>$product->faq_view,
						'credit_charges'=>$credit_charges_list,
						'counter_status'=>$counter_status,
						'counter_price'=>$counter_price,
						'counter_quantity'=>$counter_quantity,
						'counter_unit'=>$counter_unit,
						'counter_id' => $counter_id
                    );
                    return response()->json($response, 200);
                }
            } else {
                return response()->json(['error' => 'Check api data'], 500);
            }
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function review(Request $request)
    {
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        if (isset($request->review)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user===null) {
                return response()->json(['message' => 'login'], 200);
            } else {
                $rating = (int)$request->rating;
                $review = escape(htmlspecialchars($request->review));
                $product = (int)$request->product;
                DB::insert("INSERT INTO reviews (name,rating,review,product,time,active) VALUE ('$user->id  ','$rating','$review','$product','".time()."','0')");
                return response()->json(['message' => 'success'], 200);
            }
        }
        return response()->json(['message' => 'failed'], 200);
    }

    public function faq(Request $request)
    {
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        if (isset($request->question)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 200);
            } else {
                $question = $request->question;
                $category = (int)$request->category;
                DB::insert("INSERT INTO product_faq (question,category,time,status) VALUE ('$question','$category','" . time() . "','0')");
                return response()->json(['message' => 'success'], 200);
            }
        }
    }

    public function quick_quote(Request $request)
    {
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        if (isset($request->quote)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user===null) {
                return response()->json(['message' => 'login'], 200);
            } else {
                $name = $request->name;
                $email = $request->email;
                $contact = $request->contact;
                $quote = $request->quote;
                $files = '';
                if($request->hasFile('image')) {
                    $image = $request->image;
                    $files = md5(time()).'.'.$request->file('image')->getClientOriginalExtension();
                    $path = base_path().'/assets/crm/images/user/';
                    $request->file('image')->move($path,$files);
                    $img = Image::make($path.$files)->save($path.$files);
                }
				date_default_timezone_set('Asia/Kolkata');
				$date = date('Y-m-d H:i:s');
                DB::insert("INSERT INTO quick_quote (name,email,contact,quote,image,time,status) VALUE ('$name','$email','$contact','$quote','$files','".$date."','0')");
                return response()->json(['message' => 'success'], 200);
            }
        }
        return response()->json(['message' => 'failed'], 200);
    }

    public function enquiry(Request $request)
    {
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        if (isset($request->enquiry)) {
            $filex = '';
            $files = '';
            $img = '';
            $user = Customer::where('appid', $request->token)->first();
            if ($user===null) {
                return response()->json(['message' => 'login'], 200);
            } else {
                $enquiry = $request->enquiry;
                if($request->hasFile('image')) {
                    $image = $request->image;
                    $files = md5(time()).'.'.$request->file('image')->getClientOriginalExtension();
                    $path = base_path().'/assets/crm/images/user/';
                    $request->file('image')->move($path,$files);
                    $img = Image::make($path.$files)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$files);
                }
                DB::insert("INSERT INTO enquiry (enquiry,image,time,status) VALUE ('$enquiry','$files','".time()."','0')");
                return response()->json(['message' => 'success'], 200);
            }
        }
        return response()->json(['message' => 'failed'], 200);
    }

    public function book(Request $request)
    {
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            if (!isset($request->id)) {
                exit;
            }
            $id = $request->id;
            $type = $request->type;
            $product = DB::select("SELECT * FROM products WHERE id = " . $id)[0];
			$basic = '';
			if($product !== null) {
				$basic = $product->purchase_price;
			}
			$tax = '';
			if($product !== null) {
				if ($product->tax !== '') {
					$tax = $product->tax;
				}
			}
            $q = isset($request->q) ? (int)$request->q : "1";
            $min = isset($request->min) ? (int)$request->min : 0;
            $variants = array();
            if ($q == 0 && $type == 'bysize') {
                $variants = (array)json_decode(stripcslashes($request->quantity));
                foreach ($variants as $var) {
                    $q = $q + $var;
                }
            }
            if ($min == 0) {
                if ($q <= $min) {
                    return response()->json(['message' => 'invalid_data'], 200);
                }
            } else {
                if ($q < $min) {
                    return response()->json(['message' => 'minimum_quantity'], 200);
                }
            }
            if (DB::select("SELECT quantity FROM products WHERE id = " . $id)[0]->quantity < $q) {
                return response()->json(['message' => 'unavailable'], 200);
            }
            $data['variants'] = stripcslashes($request->variants);
            $data['product_id'] = $id;
            $data['brand_id'] = $id;
            $data['credit_charge_id'] = $request->credit_charge_id;
            $data['user_id'] = $user->id;
            $data['direct_booking'] = '1';
            $data['hub_id'] = $request->hub_id;
            $data['method'] = $request->type;
			if(isset($request->without_variants)){
				$data['without_variants'] = $request->without_variants;
			}
            $data['unit'] = $request->unit;
            DB::table('rfq_booking')->insert($data);
            
            return response()->json(['message' => 'success'], 200);
        }
        return response()->json(['message' => 'login'], 401);
    }

    public function myBookingsPending(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $ret = array();
        $variant = array();
        if (isset($request->token)) {
			$customer_id = $request->token;
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $bookings = DB::table('rfq_booking')->where('user_id', $user->id)->where(function($q){
				$q->where('status', 0)->orWhere('status', 1);
			})->orderBy('id', 'DESC')->get();
            foreach ($bookings as $booking){
                $product = DB::table('products')->where('id', $booking->product_id)->first();
                $pname = DB::table('products')->where('id', $booking->brand_id)->first();
				$proName = '';
				if($pname !== null){
					$proName = $pname->title;
				}else{
					$proName = $product->title;
				}
                $product_variants = DB::table('product_variants')->where('product_id', $booking->product_id)->first();
				$ph = \App\PhRelations::where('products', $booking->product_id)->first();
				$data = json_decode($ph->hubs);
				$pids = array_pluck($data, 'hub');
				$key = array_search($booking->hub_id, $pids);
				$loading = 0;
				$pdata = $data[$key];
                if($product !== null){
                    $variant_array = (array)json_decode($booking->variants);
                    foreach($variant_array as $key => $v){
						$variant[] = (array)$v;
                    }
                    $retVar = array();
                    $unit_array = array();
                    $total_basic_price = array();
                    $uni = '';
					$total_basic = 0;
					$total_size_diff = 0;
					$total_loading = 0;
					$total_tax_value = 0;
					$total_pro_quantity = 0;
					if($booking->direct_booking == 1){
						if($booking->without_variants == 1){
							$priceData = array();
							foreach($variant_array as $key => $value){
								$productin = DB::table('products')->where('id', $booking->brand_id)->first();
								$tax_value = 0;
								$basic = $pdata->price;
								if($productin){
									if($productin->tax!==''){
										$tax = $productin->tax;
										$tax_value = round(($basic)*(int)$tax/100);
									}
								}
								$total_amount = $basic+$tax_value;
								$total_basic = $basic;
								$total_tax_value = $tax_value;
								$total_pro_quantity = $value->quantity;
								$ppv = '0';
								$info = array();
								$info[] = array(
									"basic" => $basic,
									"tax" => $tax_value,
									"total_amount" => $total_amount,
								);
								if($productin){
									$priceData[] = getInstitutionalPrice($productin->id,$customer_id,$booking->hub_id,$value->id, false,false);
								}else{
									$priceData[] = getInstitutionalPrice(false,$customer_id,$booking->hub_id,$value->id, false,false);
								}
								$pt = '';
								if($productin){
									$pt = $productin->title;
								}
								$retVar[] = array(
									"id" => $value->id,
									"title" => $pt,
									"price" => $basic,
									"quantity" => $value->quantity,
									"unit" => $value->unit,
									"price_detail" => $priceData
								);
							}
						}else{
							foreach($variant_array as $key => $value){
								if(isset($value->id)){
									$info = array();
									$priceData = array();
									$var_price=0;
									$price = 0;
									$sizeName = '';
									$productin = DB::table('products')->where('id', $booking->product_id)->first();
									if($booking->method == 'bysize'){
										$pvar = DB::table('product_variants')->where('id', $value->id)->first();
										if($pvar !== null){
											$size_name = DB::table('size')->where('id', $pvar->variant_title)->first();
											$sizeName = $size_name->name;
											$prices = DB::table('multiple_size')->where('size_id', $size_name->price)->first();
											if($prices) {
												$var_price = $prices->price;
												$price = $prices->price;
											}
										}
									}else{
										$sizeName = $productin->title;
									}
									$total_amount ='';
									$total_quantity = 0;
									$product_quantity = $productin->quantity;
									$tax_value='';
									$quantity = '';
									$action = 0;
									$basic = $pdata->price;
									$loading =$pdata->loading;;
									if($productin->tax!==''){
										$tax = $productin->tax;
										$tax_value = round(($basic+$var_price)*(int)$tax/100);
									}
									$total_amount = round($basic+$var_price+$tax_value+$loading);
									if($value->quantity !== ''){
										$total_basic = round(($total_basic + $basic) * $value->quantity);
										$total_size_diff = round(($total_size_diff + $var_price) * $value->quantity);
										$total_loading = round(($total_loading + $loading) * $value->quantity);
										$total_tax_value = round(($total_tax_value + $tax_value) * $value->quantity);
									}else{
										$total_basic = round($total_basic + $basic);
										$total_size_diff = round($total_size_diff + $var_price);
										$total_loading = round($total_loading + $loading);
										$total_tax_value = round($total_tax_value + $tax_value);
									}
									if($value->quantity !== ''){
										$total_pro_quantity = $total_pro_quantity + $value->quantity;
									}
									$priceData[] = getInstitutionalPrice($productin->id,$customer_id,$booking->hub_id,$value->id, false);
									$variant_length = '';
									$variant_length_unit = '';
									$variant_weight = '';
									$variant_weight_unit = '';
									if($productin->show_weight_length == 1){
                                        $pvar = DB::table('product_variants')->where('id', $value->id)->first();
                                        if($pvar !== null) {
                                            $size = DB::table('size')->where('id', $pvar->variant_title)->first();
                                            $sw = DB::table('size_weight')->where('size_id', $size->id)->first();
                                            if ($sw) {
                                                $variant_length = $sw->length;
                                                $variant_length_unit = getUnitSymbol($sw->lunit);
                                                $variant_weight = $sw->weight;
                                                $variant_weight_unit = getUnitSymbol($sw->wunit);
                                            }
                                        }
									}
									$retVar[] = array(
										"id" => $value->id,
										"title" => $sizeName,
										"price" => $basic,
										"quantity" => $value->quantity,
										"unit" => $value->unit,
										"price_detail" => $priceData,
										"variant_length" => $variant_length,
										"variant_length_unit" => $variant_length_unit,
										"variant_weight" => $variant_weight,
										"variant_weight_unit" => $variant_weight_unit
									);
								}
							}
						}
					}
					else{
						$rfqData = $booking;
						if(isset($rfqData->variants)){
							$var = json_decode($rfqData->variants);
							$pid = $rfqData->product_id;
							foreach($var as $key => $value){
								if(isset($value->id)){
									$info = array();
									$price = 0;
									$var_price=0;
									$size_name = '';
									$productin = DB::table('products')->where('id', $booking->product_id)->first();
									$total_amount ='';
									$total_quantity = 0;
									$product_quantity = $productin->quantity;
									$tax_value='';
									$quantity = '';
									$uni = $value->unit;
									$basic = floatval($value->price);
									if(isset($value->var_price)){
										$var_price = floatval($value->var_price);
									}
									$loading =$pdata->loading;
									if($productin->tax!==''){
										$tax = $productin->tax;
										$tax_value = round(($basic+$var_price+$loading)*(int)$tax/100);
									}
									$total_amount = ($basic+$var_price+$loading)+$tax_value;
									if($value->quantity == ''){
										continue;
									}
									if($value->quantity !== ''){
										$total_basic = round($total_basic + ($basic * $value->quantity));
										$total_loading = round($total_loading + ($loading * $value->quantity));
										$total_tax_value = round($total_tax_value + ($tax_value * $value->quantity));
									}else{
										$total_basic = round($total_basic + $basic);
										$total_loading = round($total_loading + $loading);
										$total_tax_value = round($total_tax_value + $tax_value);
									}
									if($value->quantity !== ''){
										$total_pro_quantity = $total_pro_quantity + $value->quantity;
									}
									//dd($value);
									$variant_length = '';
									$variant_length_unit = '';
									$variant_weight = '';
									$variant_weight_unit = '';
                                    if($productin->show_weight_length == 1){
                                        $pvar = DB::table('product_variants')->where('id', $value->id)->first();
                                        if($pvar !== null) {
                                            $size = DB::table('size')->where('id', $pvar->variant_title)->first();
                                            $sw = DB::table('size_weight')->where('size_id',$size->id)->first();
                                            if($sw){
                                                $variant_length = $sw->length;
                                                $variant_length_unit = getUnitSymbol($sw->lunit);
                                                $variant_weight = $sw->weight;
                                                $variant_weight_unit = getUnitSymbol($sw->wunit);
                                            }
                                        }
                                    }
									$info[] = array(
										"timer" => 0,
										"price" => "".$basic,
										"loading" => $loading,
										"tax" => "".$tax_value,
										"total" => "".$total_amount,
										"var" => "".$var_price,
									);
									$retVar[] = array(
										"id" => $value->id,
										"title" => $value->title,
										"price" => $basic,
										"quantity" => $value->quantity,
										"unit" => $value->unit,
										"price_detail" => $info,
										"variant_length" => $variant_length,
										"variant_length_unit" => $variant_length_unit,
										"variant_weight" => $variant_weight,
										"variant_weight_unit" => $variant_weight_unit
									);
								}
							}
						}
					}
					$booked = 0;
					DB::table('booking_count')
						->where('customer_id',$user->id)
						->update(['booking_counter' => 0]);
						$pp = '0';
					$status = '';
					if($booking->status == 0){
						$status = 'Unapproved';						
					}elseif($booking->status == 1){
						$status = 'Approved';						
					}
					$credit_charge = array();
					if($booking->credit_charge_id !== 0 || $booking->credit_charge_id !== '' || $booking->credit_charge_id !== null){
						$credit = DB::table('credit_charges')->where('id', $booking->credit_charge_id)->first();
						if($credit !== null){
							$credit_charge[] = array(
								'id'=>$credit->id,
								'days'=>$credit->days,
								'amount'=>$credit->amount,
							);
						}
					}
					$ret[] = array(
						'id' => $booking->id,
						'p_id' => $booking->product_id,
						'c_id' => $product->category,
						'location' => $booking->location,
						'payment_option' => $booking->payment_option,
						'image' => url('assets/products/'.image_order($product->images)),
						'status' => $status,
						'title' => $proName,
						'unit' => $uni,
						'method' => $booking->method,
						'total_basic' => $total_basic,
						'total_loading' => $total_loading,
						'total_tax_value' => $total_tax_value,
						'total_quantity' => $total_pro_quantity,
						'variant' => $retVar,
						'booking_date' => date("d-m-Y", strtotime($booking->created_at)),
						'credit_charge' => $credit_charge
					);
				}
            }
            return response()->json(['message' => $ret], 200);
        }else{
            return response()->json(['message' => 'login'], 401);
        }
    }

    public function getLiveQuotation(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $ret = array();
        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
			
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
			$customer_id = $request->token;
            $quotation = DB::table('request_quotation')->where('user_id', $user->id)->where(function($q){
                $q->where('validity', '>=', date('Y-m-d H:i:s'));
                $q->orWhere('validity', null);
            })->orderBy('id', 'DESC')->get();
            DB::table('rfq_count')
                ->where('customer_id',$user->id)
                ->update(['rfq_counter' => 0]);
            foreach ($quotation as $quote){
                $cat_name = DB::table('category')->where('id', $quote->cat_id)->first();
                $products = DB::table('products')->where('id', $quote->brand_id)->first();
                $product_quantity = 0;
                if($products !== null) {
                    $product_quantity = $products->quantity;
                }
				$product_id = '';
                if($products !== null) {
                    $product_id = $products->id;
                }
                $total_quantity = 0;
                $retVar = array();
                $info = array();
                $jdata = json_decode(str_replace("\\", "", $quote->variants));
                $variant = (array)json_decode($quote->variants);
				$brand_ids = str_replace(' ', '', $quote->brand_id);
                $bids = explode(',', $brand_ids);
                $brand_names = array_pluck(DB::table('brand')->whereIn('id', $bids)->get(), 'name');
                if($quote->method == 'bysize'){
					$variant = json_decode($quote->variants);
					foreach($bids as $bi){
						foreach($variant->$bi as $vr){
							$total_quantity = $total_quantity+$vr->quantity;
							$var_p=0;
							$basic = 0;
							if($products !== null) {
								$basic = $products->purchase_price;
							}
							$loading =0;
							$tax_value = '';
							$total_amount = '';
							$tax = '';
							if($products !== null) {
								if ($products->tax !== '') {
									$tax = $products->tax;
								}
							}
							$title = '';
							$var_p = '';
							$tax_value = '';
							$total_amount = '';
							if($quote->method=='bybasic'){
								$pr = DB::table('products')->where('id', $vr->id)->first();
								if($pr){
									$title = $pr->title;
								}
								$var_p=0;
								$tax_value = round(($basic+$var_p)*(int)$tax/100);
								$total_amount = $basic+$var_p+$tax_value+$loading;
							}
							else{
								$pvar = DB::table('product_variants')->where('id', $vr->id)->first();
								if($pvar !== null) {
									$title = $pvar->variant_title;
									$prices = DB::table('multiple_size')->where('size_id', $pvar->price)->first();
									$var_p = 0;
									if($prices){
										$var_p = $prices->price;
									}
									$tax_value = round((floatval($basic) + floatval($var_p)) * floatval($tax) / 100);
									$total_amount = floatval($basic) + floatval($var_p) + floatval($tax_value) + floatval($loading);
								}
							}
							$price_detail = '';
							if($quote->method=='bybasic'){
								$info = array(
									'price'=> $vr->basic_price,
									'loading'=> "",
									'tax'=> "",
									'var'=> $vr->var_price,
									'total'=> ""
								);
								$price_detail = $info;
							}else{
								$info = array(
									'price'=> isset($vr->price_detail->price) ? $vr->price_detail->price : 'Waiting for update',
									'loading'=> isset($vr->price_detail->loading) ? $vr->price_detail->loading : 0,
									'tax'=> isset($vr->price_detail->tax) ? $vr->price_detail->tax : 0,
									'var'=> isset($vr->price_detail->var) ? $vr->price_detail->var : 0,
									'total'=> isset($vr->price_detail->total) ? $vr->price_detail->total : 0,
								);
								$price_detail = $info;
							}
							$var_price = '';
							if(isset($vr->var_price)){
								$var_price = $vr->var_price;
							}
							$retVar[] =  array(
								"id" => $vr->id,
								"title" => $vr->title,
								"quantity" => $vr->quantity,
								"price" => $var_price,
								"price_detail" => $price_detail,
							);
						}
					}
				}else{
					foreach($variant as $vr){
						$total_quantity = $total_quantity+$vr->quantity;
						$var_p=0;
						$basic = '';
						if($products !== null) {
							$basic = $products->purchase_price;
						}
						$loading =0;
						$tax_value = '';
						$total_amount = '';
						$tax = '';
						if($products !== null) {
							if ($products->tax !== '') {
								$tax = $products->tax;
							}
						}
						$title = '';
						$var_p = '';
						$tax_value = '';
						$total_amount = '';
						if($quote->method=='bybasic'){
							$pr = DB::table('products')->where('id', $vr->id)->first();
							if($pr){
								$title = $pr->title;
							}
							$var_p=0;
							$tax_value = round(($basic+$var_p)*(int)$tax/100);
							$total_amount = $basic+$var_p+$tax_value+$loading;
						}
						else{
							$pvar = DB::table('product_variants')->where('id', $vr->id)->first();
							if($pvar !== null) {
								$title = $pvar->variant_title;
								$var_p = $pvar->price;
								$tax_value = round(($basic + $var_p) * (int)$tax / 100);
								$total_amount = $basic + $var_p + $tax_value + $loading;
							}
						}
						$price_detail = getInstitutionalPrice($products->id,$customer_id,$quote->hub_id,$vr->id, false);
						if($quote->method=='bybasic'){
							$info = array(
								'price'=> $vr->basic_price,
								'loading'=> "",
								'tax'=> "",
								'var'=> $vr->var_price,
								'total'=> ""
							);
							$price_detail = $info;
						}
						$var_price = '';
						if(isset($vr->var_price)){
							$var_price = $vr->var_price;
						}
						$retVar[] =  array(
							"id" => $vr->id,
							"title" => $title,
							"quantity" => $vr->quantity,
							"price" => $var_price,
							"price_detail" => $price_detail,
						);
					}
				}
                $st = 'Pending';
                $pvalidity= 0;
                if($quote->status > intval(0)){
					$st = 'Approved';
				}
				if($quote->validity !== null){
					date_default_timezone_set('Asia/Kolkata');
					$pvalidity = strtotime($quote->validity) - strtotime(date('Y-m-d H:i:s'));
					if(new DateTime() > new DateTime($quote->validity)){
						$st = 'Expired';
					}
				}
                $section = 'Live';
                if($st == 'Expired'){
                    $section = 'Expired';
                }
                $ret[] = array(
                    'id' => $quote->id,
					'product_name' => isset($products->title) ? $products->title : '',
					'product_id' => $product_id,
                    'cat_name' => $cat_name->name,
                    'cat_id' => $cat_name->id,
                    'brand_id' => implode(', ', $brand_names),
                    'status' => $st,
                    'section' => $section,
                    'payment_option' => $quote->payment_option,
                    'other_option' => $quote->other_option,
                    'location' => $quote->location,
                    'method' => $quote->method,
                    'variant' => $retVar,
                    'booking_date' => date("d, M Y H:i", strtotime($quote->created_at)),
                    'timer' => $pvalidity,
                    'stock' => $product_quantity-$total_quantity
                );
            }
            return response()->json(['message' => $ret], 200);
        }else{
            return response()->json(['message' => 'login'], 401);
        }
    }
	
    public function getExpiredQuotation(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $ret = array();
        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $quotation = DB::table('request_quotation')->where('user_id', $user->id)->where('validity', '<', date('Y-m-d H:i:s'))->orderBy('id', 'DESC')->get();
            foreach ($quotation as $quote){
                $cat_name = DB::table('category')->where('id', $quote->cat_id)->first();
                $products = DB::table('products')->where('id', $quote->product_id)->first();
                $retVar = array();
                $jdata = json_decode(str_replace("\\", "", $quote->variants));
                $variant = (array)json_decode($quote->variants);
				$brand_ids = str_replace(' ', '', $quote->brand_id);
                $bids = explode(',', $brand_ids);
				if($quote->method=='bybasic'){
					foreach($variant as $vr){
						$pr = DB::table('products')->where('id', $vr->id)->first();
						if($pr !== null){
							$title = $pr->title;
						}
						$retVar[] =  array(
							"id" => $vr->id,
							"title" => $title,
							"quantity" => $vr->quantity,
							"price" =>$vr->var_price
						);
					}
				}
				else{
					$variant = json_decode($quote->variants);
					foreach($bids as $bi){
						foreach($variant->$bi as $vr){
							$pvar = DB::table('product_variants')->where('id', $vr->id)->first();
							if($pvar) {
								$title = $pvar->variant_title;
							}
							$retVar[] =  array(
								"id" => $vr->id,
								"title" => $title,
								"quantity" => $vr->quantity,
								"price" =>$vr->var_price
							);
						}
					}
				}
                $brand_names = array_pluck(DB::table('brand')->whereIn('id', $bids)->get(), 'name');
                $st = 'Pending';
                if($quote->status > intval(0)){
					$st = 'Approved';
				}
				if($quote->validity !== null){
					if(new DateTime() > new DateTime($quote->validity)){
						$st = 'Expired';
					}
				}
                $section = 'Live';
                if($st == 'Expired'){
                    $section = 'Expired';
                }
                $ret[] = array(
                    'id' => $quote->id,
                    'product_id'=>$quote->product_id,
                    'cat_id'=>$quote->cat_id,
                    'cat_name' => $cat_name->name,
                    'brand_id' => implode(', ', $brand_names),
                    'status' => $st,
                    'section' => $section,
                    'payment_option' => $quote->payment_option,
                    'other_option' => $quote->other_option,
                    'location' => $quote->location,
                    'method' => $quote->method,
                    'variant' => $retVar,
                    'booking_date' => date("d, M Y", strtotime($quote->created_at)),
                    'stock' => $products->quantity
                );
            }
            return response()->json(['message' => $ret], 200);
        }else{
            return response()->json(['message' => 'login'], 401);
        }
    }

    public function getQuotationDetail(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $ret = array();
        if (isset($request->token)) {
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
			$customer_id = $request->token;
            $quote_id = $request->id;
            $met = $request->type;
            if($quote_id!==null){
                $quotation = DB::table('request_quotation')->where('id', $quote_id)->first();
                if($quotation===null){
                    return response()->json(['message' => 'id not found'], 401);
                }
                $brand_ids = str_replace(' ', '', $quotation->brand_id);
                $bids = explode(',', $brand_ids);
                $brand_names = array_pluck(DB::table('brand')->whereIn('id', $bids)->get(), 'name');
                $cat = DB::table('category')->where('id', '=', $quotation->cat_id)->first();
                $wunits = DB::table('units')->whereIn('id', explode(',', $cat->weight_unit))->get();
                $var_ids = array_pluck(DB::table('product_variants')
				->where('product_id', $quotation->product_id)
				->join('size','size.id','=','product_variants.variant_title')
				->select(['product_variants.*'])
				->orderBy('size.priority')
				->get(), 'id');
				$cat_name = DB::table('category')->where('id', $quotation->cat_id)->first();
				$quantity = 0;
				$ii = 0;
				$ret = array();
                foreach($bids as $key=>$bid){
                    $brand_name = DB::table('products')->where('id', $bid)->first();
					$ph = \App\PhRelations::where('products', $bid)->first();
					$data = json_decode($ph->hubs);
					$pids = array_pluck($data, 'hub');
					$keyx = array_search($quotation->hub_id, $pids);
					$pdata = $data[$keyx];
                    $retVar = array();
					$quantity = 0;
                    if($met =='bysize'){
						$retVar = array();
                        $variant = json_decode($quotation->variants);
                        $product_quantity = 0;
                        $total_quantity = 0;
						foreach($var_ids as $var){
                            $info = array();
							$var_price=0;
                            $basic_price=0;
							$vp = 0;
							$sizename = '';
							$sizeid = '';
                            $pvar = DB::table('product_variants')->where('id', $var)->first();
							if($pvar !== null){
								$size_name = DB::table('size')->where('id', $pvar->variant_title)->first();
								$prices = DB::table('multiple_size')->where('id', $pvar->price)->first();
								if($prices !== null) {
									$vp = $var_price = is_numeric($prices->price) ? $prices->price : 0;
								}
								$sizename = $size_name->name;
								$sizeid = $size_name->id;
							}
                            $product = DB::table('products')->where('id', $quotation->product_id)->first();
                            $total_amount =0;
                            $total_quantity = 0;
                            $product_quantity = $product->quantity;
                            $tax_value=0;
                            $quantity = 0;
                            $action = 0;
                            if($user->institutional_price !== null) {
                                $basic = $pdata->price+$user->institutional_price;
                            }else {
                                $basic = $pdata->price;
                            }
                            $loading =$pdata->loading;
							$tax = 0;
                            if($product->tax!==''){
                                $tax = $product->tax;
                                $tax_value = round(($basic+$var_price+$loading)*(int)$tax/100);
                            }
                            $total_amount = $basic+$var_price+$loading;
							$tamtdata = round((($total_amount)*(int)$tax/100));
							$tamt = 0;
							$new_price = 0;
							$unit = '';
							$bprice = round($total_amount+$tamtdata);
                            $variant_length = '';
                            $variant_length_unit = '';
                            $variant_weight = '';
                            $variant_weight_unit = '';
                            foreach($variant->$bid as $var_id){
								$size_diff = $vp;
								if($quotation->action){
									$var_price = $basic + $quotation->price_diff;
									$tamtdata = round(($var_price + $size_diff + $loading)*(int)$tax/100);
									$bprice = round(($var_price + $size_diff + $loading)+$tamtdata);
								}else{
									$var_price = $basic - $quotation->price_diff;
									$tamtdata = round(($var_price + $size_diff + $loading)*(int)$tax/100);
									$bprice = round(($var_price + $size_diff + $loading)+$tamtdata);
								}
                                if($var == $var_id->id){
                                    $quantity = $var_id->quantity;
                                    $var_price = is_numeric($var_id->var_price) ? $var_id->var_price : 0;;
                                    $size_diff = $var_id->price_detail->var;
									if($quantity > 0){
										$action = 1;
									}
                                    $total_quantity = $total_quantity+$var_id->quantity;
									$new_price = $var_price;
									if($product->tax!==''){
										$tax = $product->tax;
										$tamtdata = round(((floatval($var_price)+floatval($size_diff))+floatval($pdata->loading))*(int)$tax/100);
										if($quantity > 0){
											$tamt = round((((floatval($var_price)+floatval($size_diff))+floatval($pdata->loading)+$tamtdata)*$var_id->quantity));
										}else{
											$tamt = round((((floatval($var_price)+floatval($size_diff))+floatval($pdata->loading)+$tamtdata)));
										}
										$bprice = round(((floatval($var_price)+floatval($size_diff))+floatval($pdata->loading)+$tamtdata));
									}
									$unit = $var_id->unit;
									continue;
                                }

								if($product->show_weight_length == 1){
									$sw = DB::table('size_weight')->where('size_id',$size_name->id)->first();
									if($sw){
										$variant_length = $sw->length;
										$variant_length_unit = getUnitSymbol($sw->lunit);
										$variant_weight = $sw->weight;
										$variant_weight_unit = getUnitSymbol($sw->wunit);
									}
								}
                            }
                            $info = array(
                                "basic" => $var_price,
                                "size_diff" => $size_diff,
                                "loading" => $loading,
                                "tax" => $tamtdata,
                                "total_amount" => $tamt,
                                "bprice" => $bprice,
								"new_price"=>($new_price !== 0) ? $new_price : $var_price,
                            );
							$retVar[] =  array(
								"id" => $var,
								"title" => $sizename,
								"variant_title_id" => $sizeid,
								"quantity" => $quantity,
								"unit" => $unit,
								'cat_id' => $product->category,
								"var_price" => $var_price,
								"basic_price" => $basic_price,
								"total_price" => $total_amount,
								"action" => $action,
								"basic" => $basic,
								"price_detail" => $info,
								"variant_length" => $variant_length,
								"variant_length_unit" => $variant_length_unit,
								"variant_weight" => $variant_weight,
								"variant_weight_unit" => $variant_weight_unit
							);
                        }
                        $unit_cat =array();
                        foreach ($wunits as $unit){
                            $unit_cat[] = array(
                                'id' => $unit->id,
                                'symbol' => $unit->symbol
                            );
                        }
						$st = 'Pending';
						$pvalidity= 0;
						if($quotation->status == 1){
							$st = 'Approved';
							if($quotation->validity !== null){
								date_default_timezone_set('Asia/Kolkata');
								$pvalidity = strtotime($quotation->validity) - strtotime(date('Y-m-d H:i:s'));
								if(new DateTime() > new DateTime($quotation->validity)){
									$st = 'Expired';
								}
							}
						}
						$bt = 'N/A';
						if($brand_name){ $bt = $brand_name->title; }
						$section = 'Live';
						if($st == 'Expired'){
							$section = 'Expired';
						}
						$credit_charge = array();
						if($quotation->credit_charge_id !== 0 || $quotation->credit_charge_id !== '' || $quotation->credit_charge_id !== null){
							$credit = DB::table('credit_charges')->where('id', $quotation->credit_charge_id)->first();
							if($credit !== null){
								$credit_charge[] = array(
									'id'=>$credit->id,
									'days'=>$credit->days,
									'amount'=>$credit->amount,
								);
							}
						}
                        $ret[] = array(
                            'id' => $quotation->id,
                            'product_id' => $quotation->product_id,
                            'product_name' => $brand_name->title,
                            'brand_id' => $bid,
							'cat_name' => $cat_name->name,
							'cat_id' => $cat_name->id,
							'brand_ids' => implode(', ', $brand_names),
							'status' => $st,
							'section' => $section,
							'payment_option' => $quotation->payment_option,
							'credit_charge_id' => $quotation->credit_charge_id,
							'other_option' => $quotation->other_option,
							'location' => $quotation->location,
							'method' => $quotation->method,
                            'brand_name' => $bt,
                            'variant' => $retVar,
                            'stock'=> $product_quantity,
                            'unit'=>$unit_cat,
                            'booking_date' => date("d, M Y", strtotime($quotation->created_at)),
							'timer' => $pvalidity,
							'hub_id' => $quotation->hub_id,
							'credit_charge' => $credit_charge
                        );	
                    }else{
						$product = DB::table('products')->where('id', $bid)->first();
						$base_variant = json_decode($quotation->variants);
						$quantity = 0;
						$total_quantity = 0;
						$product_quantity = $product->quantity;
						$tamt = 0;
						$bprice = 0;
						$tamtdata = 0;
						$new_price = 0;
						$base = $base_variant[$key];
						$quantity = $base->quantity;
						$total_quantity = $total_quantity+$base->quantity;
						if($base->action == 0){
							if(is_numeric($base->basic_price) && is_numeric($base->var_price)){
								$new_price = $base->basic_price-$base->var_price;
							}
							if($product->tax!==''){
								$tax = $product->tax;
								if(is_numeric($base->basic_price) && is_numeric($base->var_price)){
									$tamtdata = round((($base->basic_price-$base->var_price)+$pdata->loading)*(int)$tax/100);
									$tamt = round(((($base->basic_price-$base->var_price)+$pdata->loading+$tamtdata)*$base->quantity));
									$bprice = round((($base->basic_price-$base->var_price)+$pdata->loading+$tamtdata));
								}
							}
						}elseif($base->action == 1){
							if(is_numeric($base->basic_price) && is_numeric($base->var_price)){
								$new_price = $base->basic_price+$base->var_price;
							}
							if($product->tax!==''){
								$tax = $product->tax;
								if(is_numeric($base->basic_price) && is_numeric($base->var_price)){
									$tamtdata = round((($base->basic_price+$base->var_price)+$pdata->loading)*(int)$tax/100);
									$tamt = round((($base->basic_price+$base->var_price+$pdata->loading+$tamtdata)*$base->quantity));
									$bprice = round(($base->basic_price+$base->var_price+$pdata->loading+$tamtdata));
								}
							}
						}
						$tax_base ='';
						$total_base = '';
						$basic = $pdata->price;
						$loading =$pdata->loading;
						if($product->tax!==''){
							$tax = $product->tax;
						}
						$unit_cat =array();
						foreach ($wunits as $unit){
							$unit_cat[] = array(
								'id' => $unit->id,
								'symbol' => $unit->symbol
							);
						}
						$st = 'Pending';
						$pvalidity= 0;
						if($quotation->status == 1){
							$st = 'Approved';
							if($quotation->validity !== null){
								date_default_timezone_set('Asia/Kolkata');
								$pvalidity = strtotime($quotation->validity) - strtotime(date('Y-m-d H:i:s'));
								if(new DateTime() > new DateTime($quotation->validity)){
									$st = 'Expired';
								}
							}
						}
						$bt = 'N/A';
						if($brand_name !== null){ 
							$bt = $brand_name->title; 
						}
						$section = 'Live';
						if($st == 'Expired'){
							$section = 'Expired';
						}
						$credit_charge = array();
						if($quotation->credit_charge_id !== 0 || $quotation->credit_charge_id !== '' || $quotation->credit_charge_id !== null){
							$credit = DB::table('credit_charges')->where('id', $quotation->credit_charge_id)->first();
							if($credit !== null){
								$credit_charge[] = array(
									'id'=>$credit->id,
									'days'=>$credit->days,
									'amount'=>$credit->amount,
								);
							}
						}
						$ret[] = array(
							'id' => $quotation->id,
							'product_id' => $quotation->product_id,
							'product_name' => $bt,
							'cat_id' => $product->category,
							'brand_id' => $bid,
							'cat_name' => $cat_name->name,
							'cat_id' => $cat_name->id,
							'brand_ids' => implode(', ', $brand_names),
							'status' => $st,
							'section' => $section,
							'payment_option' => $quotation->payment_option,
							'other_option' => $quotation->other_option,
							'location' => $quotation->location,
							'method' => $quotation->method,
							'brand_name' => $bt,
							'quantity' => $quantity,
							'booking_date' => date("d, M Y", strtotime($quotation->created_at)),
							"basic" => $basic,
							"loading" => $loading,
							"tax" => $tamtdata,
							'stock'=> $product_quantity,
							'unit'=>$unit_cat,
							'bprice'=>$bprice,
							'new_price'=>$new_price,
							"total_amount" => $tamt,
							'timer' => $pvalidity,
							'hub_id' => $quotation->hub_id,
							'credit_charge' => $credit_charge
						);
					}
                }
                return response()->json(['message' => $ret], 200);
            }
            else{
                return response()->json(['message' => 'please insert id'], 401);
            }
        }else{
            return response()->json(['message' => 'login'], 401);
        }
    }

    public function myBookingsApproved(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $ret = array();
        $variant = array();
        if (isset($request->token)) {
			$customer_id = $request->token;
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $bookings = DB::table('rfq_booking')->where('user_id', $user->id)->where('status', 3)->orderBy('id', 'DESC')->get();
            foreach ($bookings as $booking){
                $product = DB::table('products')->where('id', $booking->product_id)->first();
                $product_variants = DB::table('product_variants')->where('product_id', $booking->product_id)->first();
				$ph = \App\PhRelations::where('products', $booking->product_id)->first();
				$data = json_decode($ph->hubs);
				$pids = array_pluck($data, 'hub');
				$key = array_search($booking->hub_id, $pids);
				$pdata = $data[$key];
                if($product !== null){
                    $variant_array = (array)json_decode($booking->variants);
                    foreach($variant_array as $key => $v){
                            $variant[] = (array)$v;
                    }
                    $retVar = array();
                    $unit_array = array();
                    $total_basic_price = array();
                    $uni = '';
					$total_basic = 0;
					$total_size_diff = 0;
					$total_loading = 0;
					$total_tax_value = 0;
					$total_pro_quantity = 0;
					if($booking->direct_booking == 1){
						if($booking->without_variants == 1){
							$priceData = array();
							foreach($variant_array as $key => $value){
								$product = DB::table('products')->where('id', $booking->product_id)->first();
								$basic = $pdata->price;
								if($product->tax!==''){
									$tax = $product->tax;
									$tax_value = round(($basic)*(int)$tax/100);
								}
								$total_amount = $basic+$tax_value;
								$total_basic = $basic;
								$total_tax_value = $tax_value;
								$total_pro_quantity = $value->quantity;
								$ppv = '0';
								$info = array();
								$info[] = array(
									"basic" => $basic,
									"tax" => $tax_value,
									"total_amount" => $total_amount,
								);
								$priceData[] = getInstitutionalPrice($product->id,$customer_id,$booking->hub_id,$value->id, false);
								$variant_length = '';
								$variant_length_unit = '';
								$variant_weight = '';
								$variant_weight_unit = '';
								if($product->show_weight_length == 1){
								    if(isset($value->variant_title_id)) {
                                        $size = DB::table('size')->where('id', $value->variant_title_id)->first();
                                        $sw = DB::table('size_weight')->where('size_id', $size->id)->first();
                                        if ($sw) {
                                            $variant_length = $sw->length;
                                            $variant_length_unit = getUnitSymbol($sw->lunit);
                                            $variant_weight = $sw->weight;
                                            $variant_weight_unit = getUnitSymbol($sw->wunit);
                                        }
                                    }
								}
								$retVar[] = array(
									"id" => $value->id,
									"title" => $product->title,
									"price" => $basic,
									"quantity" => $value->quantity,
									"unit" => $value->unit,
									"price_detail" => $priceData,
									"variant_length" => $variant_length,
									"variant_length_unit" => $variant_length_unit,
									"variant_weight" => $variant_weight,
									"variant_weight_unit" => $variant_weight_unit
								);
							}
						}else{
							foreach($variant_array as $key => $value){
								if(isset($value->id)){
									$info = array();
									$priceData = array();
									$pvar = DB::table('product_variants')->where('id', $value->id)->first();
									$size_name = DB::table('size')->where('id', $pvar->variant_title)->first();
									$prices = DB::table('multiple_size')->where('size_id', $size_name->price)->first();
									$product = DB::table('products')->where('id', $booking->product_id)->first();
									$total_amount ='';
									$total_quantity = 0;
									$product_quantity = $product->quantity;
									$tax_value='';
									$quantity = '';
									$var_price=0;
									$price = 0;
									if($prices) {
										$var_price = $prices->price;
										$price = $prices->price;
									}
									$action = 0;
									$basic = $pdata->price;
									$loading =$pdata->loading;;
									if($product->tax!==''){
										$tax = $product->tax;
										$tax_value = round(($basic+$var_price)*(int)$tax/100);
									}
									$total_amount = round($basic+$var_price+$tax_value+$loading);
									if($value->quantity !== ''){
										$total_basic = round(($total_basic + $basic) * $value->quantity);
										$total_size_diff = round(($total_size_diff + $var_price) * $value->quantity);
										$total_loading = round(($total_loading + $loading) * $value->quantity);
										$total_tax_value = round(($total_tax_value + $tax_value) * $value->quantity);
									}else{
										$total_basic = round($total_basic + $basic);
										$total_size_diff = round($total_size_diff + $var_price);
										$total_loading = round($total_loading + $loading);
										$total_tax_value = round($total_tax_value + $tax_value);
									}
									if($value->quantity !== ''){
										$total_pro_quantity = $total_pro_quantity + $value->quantity;
									}
									$priceData[] = getInstitutionalPrice($product->id,$customer_id,$booking->hub_id,$value->id, false);
									$variant_length = '';
									$variant_length_unit = '';
									$variant_weight = '';
									$variant_weight_unit = '';
									if($product->show_weight_length == 1){
										$size = DB::table('size')->where('id', $value->variant_title_id)->first();
										$sw = DB::table('size_weight')->where('size_id',$size->id)->first();
										if($sw){
											$variant_length = $sw->length;
											$variant_length_unit = getUnitSymbol($sw->lunit);
											$variant_weight = $sw->weight;
											$variant_weight_unit = getUnitSymbol($sw->wunit);
										}
									}
									$retVar[] = array(
										"id" => $value->id,
										"title" => $size_name->name,
										"price" => $basic,
										"quantity" => $value->quantity,
										"unit" => $value->unit,
										"price_detail" => $priceData,
										"variant_length" => $variant_length,
										"variant_length_unit" => $variant_length_unit,
										"variant_weight" => $variant_weight,
										"variant_weight_unit" => $variant_weight_unit
									);
								}
							}
						}
					}
					else{
						$rfqData = $booking;
						if(isset($rfqData->variants)){
							$var = json_decode($rfqData->variants);
							$pid = $rfqData->product_id;
							foreach($var as $key => $value){
								if(isset($value->id)){
									$info = array();
									$price = 0;
									$var_price=0;
									$size_name = '';
									$product = DB::table('products')->where('id', $booking->product_id)->first();
									$total_amount ='';
									$total_quantity = 0;
									$product_quantity = $product->quantity;
									$tax_value='';
									$quantity = '';
									$uni = $value->unit;
									$basic = floatval($value->price);
									if(isset($value->var_price)){
										$var_price = floatval($value->var_price);
									}
									$loading =$pdata->loading;
									if($product->tax!==''){
										$tax = $product->tax;
										$tax_value = round(($basic+$var_price+$loading)*(int)$tax/100);
									}
									$total_amount = ($basic+$var_price+$loading)+$tax_value;
									if($value->quantity == ''){
										continue;
									}
									if($value->quantity !== ''){
										$total_basic = round($total_basic + ($basic * $value->quantity));
										$total_loading = round($total_loading + ($loading * $value->quantity));
										$total_tax_value = round($total_tax_value + ($tax_value * $value->quantity));
									}else{
										$total_basic = round($total_basic + $basic);
										$total_loading = round($total_loading + $loading);
										$total_tax_value = round($total_tax_value + $tax_value);
									}
									if($value->quantity !== ''){
										$total_pro_quantity = $total_pro_quantity + $value->quantity;
									}
									$info[] = array(
										"timer" => 0,
										"price" => "".$basic,
										"loading" => $loading,
										"tax" => "".$tax_value,
										"total" => "".$total_amount,
										"var" => "".$var_price,
									);
									$retVar[] = array(
										"id" => $value->id,
										"title" => $value->title,
										"price" => $basic,
										"quantity" => $value->quantity,
										"unit" => $value->unit,
										"price_detail" => $info
									);
								}
							}
						}
					}
					$booked = 0;
					DB::table('booking_count')
						->where('customer_id',$user->id)
						->update(['booking_counter' => 0]);
						$pp = '0';
					$status = '';
					if($booking->status == 0){
						$status = 'Unapproved';						
					}elseif($booking->status == 1){
						$status = 'Approved';						
					}
					$credit_charge = array();
					if($booking->credit_charge_id !== 0 || $booking->credit_charge_id !== '' || $booking->credit_charge_id !== null){
						$credit = DB::table('credit_charges')->where('id', $booking->credit_charge_id)->first();
						if($credit !== null){
							$credit_charge[] = array(
								'id'=>$credit->id,
								'days'=>$credit->days,
								'amount'=>$credit->amount,
							);
						}
					}
					$ret[] = array(
						'id' => $booking->id,
						'p_id' => $booking->product_id,
						'c_id' => $product->category,
						'location' => $booking->location,
						'payment_option' => $booking->payment_option,
						'image' => url('assets/products/'.image_order($product->images)),
						'status' => 'Completed',
						'title' => $product->title,
						'unit' => $uni,
						'method' => $booking->method,
						'total_basic' => $total_basic,
						'total_loading' => $total_loading,
						'total_tax_value' => $total_tax_value,
						'total_quantity' => $total_pro_quantity,
						'variant' => $retVar,
						'booking_date' => date("d-m-Y", strtotime($booking->created_at)),
						'credit_charge' => $credit_charge
					);
				}
            }
            return response()->json(['message' => $ret], 200);
        }else{
            return response()->json(['message' => 'login'], 401);
        }
    }

    public function myBookingsDispatched(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $ret = array();
        $variant = array();
        if (isset($request->token)) {
			$customer_id = $request->token;
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login'], 401);
            }
            $bookings = DB::table('rfq_booking')->where('user_id', $user->id)->where('status', 2)->orderBy('id', 'DESC')->get();
            foreach ($bookings as $booking){
                $product = DB::table('products')->where('id', $booking->product_id)->first();
                $product_variants = DB::table('product_variants')->where('product_id', $booking->product_id)->first();
				$ph = \App\PhRelations::where('products', $booking->product_id)->first();
				$data = json_decode($ph->hubs);
				$pids = array_pluck($data, 'hub');
				$key = array_search($booking->hub_id, $pids);
				$pdata = $data[$key];
                if($product !== null){
                    $variant_array = (array)json_decode($booking->variants);
                    foreach($variant_array as $key => $v){
						$variant[] = (array)$v;
                    }
                    $retVar = array();
                    $unit_array = array();
                    $total_basic_price = array();
                    $uni = '';
					$total_basic = 0;
					$total_size_diff = 0;
					$total_loading = 0;
					$total_tax_value = 0;
					$total_pro_quantity = 0;
					if($booking->direct_booking == 1){
						if($booking->without_variants == 1){
							$priceData = array();
							foreach($variant_array as $key => $value){
								$product = DB::table('products')->where('id', $booking->product_id)->first();
								$basic = $pdata->price;
								if($product->tax!==''){
									$tax = $product->tax;
									$tax_value = round(($basic)*(int)$tax/100);
								}
								$total_amount = $basic+$tax_value;
								$total_basic = $basic;
								$total_tax_value = $tax_value;
								$total_pro_quantity = $value->quantity;
								$ppv = '0';
								$info = array();
								$info[] = array(
									"basic" => $basic,
									"tax" => $tax_value,
									"total_amount" => $total_amount,
								);
								$priceData[] = getInstitutionalPrice($product->id,$customer_id,$booking->hub_id,$value->id, false);
								$variant_length = '';
								$variant_length_unit = '';
								$variant_weight = '';
								$variant_weight_unit = '';
								if($product->show_weight_length == 1){
                                    if(isset($value->variant_title_id)) {
                                        $size = DB::table('size')->where('id', $value->variant_title_id)->first();
                                        $sw = DB::table('size_weight')->where('size_id', $size->id)->first();
                                        if ($sw) {
                                            $variant_length = $sw->length;
                                            $variant_length_unit = getUnitSymbol($sw->lunit);
                                            $variant_weight = $sw->weight;
                                            $variant_weight_unit = getUnitSymbol($sw->wunit);
                                        }
                                    }
								}
								$retVar[] = array(
								"id" => $value->id,
									"id" => $value->id,
									"title" => $product->title,
									"price" => $basic,
									"quantity" => $value->quantity,
									"unit" => $value->unit,
									"price_detail" => $priceData,
									"variant_length" => $variant_length,
									"variant_length_unit" => $variant_length_unit,
									"variant_weight" => $variant_weight,
									"variant_weight_unit" => $variant_weight_unit
								);
							}
						}else{
							foreach($variant_array as $key => $value){
								if(isset($value->id)){
									$info = array();
									$priceData = array();
									$pvar = DB::table('product_variants')->where('id', $value->id)->first();
									$size_name = DB::table('size')->where('id', $pvar->variant_title)->first();
									$prices = DB::table('multiple_size')->where('size_id', $size_name->price)->first();
									$product = DB::table('products')->where('id', $booking->product_id)->first();
									$total_amount ='';
									$total_quantity = 0;
									$product_quantity = $product->quantity;
									$tax_value='';
									$quantity = '';
									$var_price=0;
									$price = 0;
									if($prices) {
										$var_price = $prices->price;
										$price = $prices->price;
									}
									$action = 0;
									$basic = $pdata->price;
									$loading =$pdata->loading;;
									if($product->tax!==''){
										$tax = $product->tax;
										$tax_value = round(($basic+$var_price)*(int)$tax/100);
									}
									$total_amount = round($basic+$var_price+$tax_value+$loading);
									if($value->quantity !== ''){
										$total_basic = round(($total_basic + $basic) * $value->quantity);
										$total_size_diff = round(($total_size_diff + $var_price) * $value->quantity);
										$total_loading = round(($total_loading + $loading) * $value->quantity);
										$total_tax_value = round(($total_tax_value + $tax_value) * $value->quantity);
									}else{
										$total_basic = round($total_basic + $basic);
										$total_size_diff = round($total_size_diff + $var_price);
										$total_loading = round($total_loading + $loading);
										$total_tax_value = round($total_tax_value + $tax_value);
									}
									if($value->quantity !== ''){
										$total_pro_quantity = $total_pro_quantity + $value->quantity;
									}
									$priceData[] = getInstitutionalPrice($product->id,$customer_id,$booking->hub_id,$value->id, false);
									$variant_length = '';
									$variant_length_unit = '';
									$variant_weight = '';
									$variant_weight_unit = '';
									if($product->show_weight_length == 1){
										$size = DB::table('size')->where('id', $value->variant_title_id)->first();
										$sw = DB::table('size_weight')->where('size_id',$size->id)->first();
										if($sw){
											$variant_length = $sw->length;
											$variant_length_unit = getUnitSymbol($sw->lunit);
											$variant_weight = $sw->weight;
											$variant_weight_unit = getUnitSymbol($sw->wunit);
										}
									}
									$retVar[] = array(
										"id" => $value->id,
										"title" => $size_name->name,
										"price" => $basic,
										"quantity" => $value->quantity,
										"unit" => $value->unit,
										"price_detail" => $priceData,
										"variant_length" => $variant_length,
										"variant_length_unit" => $variant_length_unit,
										"variant_weight" => $variant_weight,
										"variant_weight_unit" => $variant_weight_unit
									);
								}
							}
						}
					}
					else{
						$rfqData = $booking;
						if(isset($rfqData->variants)){
							$var = json_decode($rfqData->variants);
							$pid = $rfqData->product_id;
							foreach($var as $key => $value){
								if(isset($value->id)){
									$info = array();
									$price = 0;
									$var_price=0;
									$size_name = '';
									$product = DB::table('products')->where('id', $booking->product_id)->first();
									$total_amount ='';
									$total_quantity = 0;
									$product_quantity = $product->quantity;
									$tax_value='';
									$quantity = '';
									$uni = $value->unit;
									$basic = floatval($value->price);
									if(isset($value->var_price)){
										$var_price = floatval($value->var_price);
									}
									$loading =$pdata->loading;
									if($product->tax!==''){
										$tax = $product->tax;
										$tax_value = round(($basic+$var_price+$loading)*(int)$tax/100);
									}
									$total_amount = ($basic+$var_price+$loading)+$tax_value;
									if($value->quantity == ''){
										continue;
									}
									if($value->quantity !== ''){
										$total_basic = round($total_basic + ($basic * $value->quantity));
										$total_loading = round($total_loading + ($loading * $value->quantity));
										$total_tax_value = round($total_tax_value + ($tax_value * $value->quantity));
									}else{
										$total_basic = round($total_basic + $basic);
										$total_loading = round($total_loading + $loading);
										$total_tax_value = round($total_tax_value + $tax_value);
									}
									if($value->quantity !== ''){
										$total_pro_quantity = $total_pro_quantity + $value->quantity;
									}
									$info[] = array(
										"timer" => 0,
										"price" => "".$basic,
										"loading" => $loading,
										"tax" => "".$tax_value,
										"total" => "".$total_amount,
										"var" => "".$var_price,
									);
									$retVar[] = array(
										"id" => $value->id,
										"title" => $value->title,
										"price" => $basic,
										"quantity" => $value->quantity,
										"unit" => $value->unit,
										"price_detail" => $info
									);
								}
							}
						}
					}
					$booked = 0;
					DB::table('booking_count')
						->where('customer_id',$user->id)
						->update(['booking_counter' => 0]);
						$pp = '0';
					$status = '';
					if($booking->status == 0){
						$status = 'Unapproved';						
					}elseif($booking->status == 1){
						$status = 'Approved';						
					}
					$credit_charge = array();
					if($booking->credit_charge_id !== 0 || $booking->credit_charge_id !== '' || $booking->credit_charge_id !== null){
						$credit = DB::table('credit_charges')->where('id', $booking->credit_charge_id)->first();
						if($credit !== null){
							$credit_charge[] = array(
								'id'=>$credit->id,
								'days'=>$credit->days,
								'amount'=>$credit->amount,
							);
						}
					}
					$ret[] = array(
						'id' => $booking->id,
						'p_id' => $booking->product_id,
						'c_id' => $product->category,
						'location' => $booking->location,
						'payment_option' => $booking->payment_option,
						'image' => url('assets/products/'.image_order($product->images)),
						'status' => 'Failed',
						'title' => $product->title,
						'unit' => $uni,
						'method' => $booking->method,
						'total_basic' => $total_basic,
						'total_loading' => $total_loading,
						'total_tax_value' => $total_tax_value,
						'total_quantity' => $total_pro_quantity,
						'variant' => $retVar,
						'booking_date' => date("d-m-Y", strtotime($booking->created_at)),
						'credit_charge' => $credit_charge
					);
				}
            }
            return response()->json(['message' => $ret], 200);
        }else{
            return response()->json(['message' => 'login'], 401);
        }
    }

    public function mentor(Request $request)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
            }
            if (isset($request->token)) {
                $id = false;
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login', 'code' => 401], 200);
                }
                $res = (array)json_decode(str_replace("\\", "", $request->mentor));
                $ment = array();
                foreach($res as $key => $value){
                    $ment = array(
                        'user_id' => $user->id,
                        'cat_id' => $value->category_id,
                        'time' => $value->time,
                        'quantity' => $value->qty,
                        'unit' => $value->unit
                    );
                    DB::table('mentor')->insert($ment);
                }
                return response()->json(['message' => 'entry successfully in mentor ', 'code' => 200], 200);
            }
            else {
                return response()->json(['message' => 'login', 'code' => 401], 200);
            }
        }
        return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
    }

    public function RequestQuotation(Request $request)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
            }
            if (isset($request->token)) {
                $id = false;
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login', 'code' => 401], 200);
                }
                $req = json_decode($request->variant);
                $other='';
                $location ='';
                $quotation = array();
                if($request->other_option){
                    if($request->other_option == ''){
                        $other = '';
                    }
                    else{
                        $other = $request->other_option;
                    }
                }
                if($request->location){
                    if($request->location == ''){
                        $location = '';
                    }
                    else{
                        $location = $request->location;
                    }
                }
				$vars = stripslashes($request->variants);
				$xx = 0;
				if($request->met == 'bybasic'){
					$bids = explode(',', $request->b_id);
					$var_data = json_decode(stripcslashes($request->variants));
					foreach($bids as $bid){
						$varsx = array();
						$br = DB::table('products')->where('id', $bid)->first();
						if($br !== null){
							$pricing = getInstitutionalPrice($br->id, $request->token, $var_data[0]->hub_id, false, false);
							foreach($var_data as $vd){
								$varsx[] = array(
									'id' => $bid,
									'title' => $br->title,
									'quantity' => $vd->quantity,
									'unit' => $vd->unit,
									'hub_id' => $vd->hub_id,
									'basic_price' => (is_numeric($pricing['price'])) ? round($pricing['price']) : $pricing['price'],
									'action' => 0,
									'var_price' => 0,
									'new_price' => 0,
								);
							}
						}
						$quotation[] = array(
							'cat_id' => $request->c_id,
							'hub_id' => $request->hub_id,
							'product_id' => $request->p_id,
							'brand_id' => $bid,
                            'payment_option' => ($request->payment_option !== null) ? $request->payment_option : 'not selected',
							'other_option' => $other,
							'method' => $request->met,
							'location' => $location,
							'variants' => json_encode($varsx),
							'user_id' => $user->id,
							'credit_charge_id' => $request->credit_charge_id
						);
						$xx++;
					}
				}else{
					$bids = explode(',', $request->b_id);
					$var_data = (array)json_decode(stripcslashes($request->variants));
					//dd($var_data);
					foreach($bids as $key=>$bid){
						$varsx = array();
						$br = DB::table('products')->where('id', $bid)->first();
						$brId = false;
						if($br !== null){
							if($br->id){
								$brId = $br->id;
							}
						}
						foreach($var_data as $vd){
							$varId = false;
							if($vd->id){
								$varId = $vd->id;
							}
							$varsx[$bid][] = array(
								"id"=>$vd->id,
								"title"=>$vd->title,
								"quantity"=>$vd->quantity,
								"unit"=>$vd->unit,
								"action"=>$vd->action,
								"var_price"=>'',
								"price_detail"=>getInstitutionalPrice($brId, $request->token, $request->hub_id, $varId, false, false),
							);
						}
						//dd($varsx);
						$quotation[] = array(
							'cat_id' => $request->c_id,
							'hub_id' => $request->hub_id,
							'product_id' => $request->p_id,
							'brand_id' => $bid,
                            'payment_option' => ($request->payment_option !== null) ? $request->payment_option : 'not selected',
							'other_option' => $other,
							'method' => $request->met,
							'location' => $location,
							'variants' => json_encode($varsx),
							'user_id' => $user->id,
							'credit_charge_id' => $request->credit_charge_id
						);
						$xx++;
					}
				}
                DB::table('request_quotation')->insert($quotation);
                
                return response()->json(['message' => 'entry successfully in request quotation ', 'code' => 200], 200);
            }
            else {
                return response()->json(['message' => 'login', 'code' => 401], 200);
            }
        }
        return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
    }

    public function hubs(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $hubs = ManufacturingHubs::orderBy('title', 'ASC')->get()->toArray();
        return response()->json(['message' => $hubs], 200);
    }
	
    public function getProductsByIds(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $customer_id ='';
        if (isset($request->token)) {
            $customer_id =$request->token;
        }
        $products = getProducts($request->pids);
        $rp = array();
		$rp_price = null;
        foreach ($products as $product) {
			if($customer_id && $customer_id != ''){
				$rp_price = getInstitutionalPrice($product->id,$customer_id,$request->hub,false,false,false);
			}else{
				$rp_price['price'] = 'Login to view price';
			}
            $rp[] = array(
				'id' => $product->id,
				'sku' => $product->sku,
				'title' => $product->title,
				'category' => $product->category,
				'institutional_price' => $rp_price['price'],
				'price_detail' => getInstitutionalPrice($product->id,$customer_id,$request->hub,false,false,false),
				'images' => url('/assets/products/' . image_order($product->images)),
				'text' => $product->text,
				'status' => $product->status
			);       
        }
        return response()->json(['products' => $rp], 200);
    }

    public function getNotification(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        if (isset($request->token)) {
            $notification = array();
            $id = false;
            $user = Customer::where('appid', $request->token)->first();
            if ($user === null) {
                return response()->json(['message' => 'login', 'code' => 401], 200);
            }
            $note_send = DB::table('notification')->whereRaw('FIND_IN_SET(?, cust_id)', $user->id)->orderBy('created_at','DESC')->get();
            foreach($note_send as $note){
                $notification[] = array(
                    'id' => $note->id,
                    'title' => $note->title,
                    'message' => $note->notification,
                    'datetime' => date('d-m-y h:i A',strtotime($note->created_at))
                );
                DB::table('notification_read')->where('customer_id',$user->id)->update(['note_read' => 0]);
            }
            return response()->json(['message' => $notification, 'code' => 200], 200);
        }
        else {
            return response()->json(['message' => 'login', 'code' => 401], 200);
        }
    }
	
	public function requestForCall(Request $request)
    {
        if(isset($request->API_KEY)) {
            if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
            }
            if ($request->mobile !== '') {
                $ment = array(
                    'name' => $request->id,
                    'company_name' => $request->company_name,
                    'mobile' => $request->mobile,
                );
                DB::table('request_for_call')->insert($ment);
                return response()->json(['message' => 'Request submitted successfully', 'code' => 200], 200);
            } else {
                return response()->json(['message' => 'Please enter your mobile number', 'code' => 401], 200);
            }
        }else {
            return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
        }
    }
	
	public function getBrands(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        else {
            $notification = array();
            $id = false;
            $note_send = DB::table('brand')->orderBy('name', 'ASC')->get();
            foreach($note_send as $note){
                $notification[] = array(
                    'id' => $note->id,
                    'name' => $note->name,
                    'image' => url('assets/brand/' . $note->image)
                );
            }
            return response()->json(['message' => $notification, 'code' => 200], 200);
        }
    }

    public function getProductByBrand(Request $request){
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        else {
            $customer_id ='';
            if (isset($request->token)) {
                $customer_id =$request->token;
                $user = Customer::where('appid', $request->token)->first();
                $user_type = $user->user_type;
            }
            $brand = DB::table('brand')->where('id', $request->id)->first();
            $products = DB::table('products')->where('brand_id', $request->id);
            $products = $products->get();
            $response['products'] = array();
            $pvalidity = 0;
			$pro = array();
            foreach ($products as $product) {
                if($user_type== "institutional"){
                    $institute_time = getInstitutionalPrice($product->id,$customer_id,$request->hub);
                    $pvalidity = $institute_time['timer'];

                }
                elseif($user_type!== "institutional" && $product->validity!==null){
                    date_default_timezone_set("Asia/Kolkata");
                    $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
                    if($pvalidity <= 0){
                        $pvalidity = 0;
                    }
                }
                $pr = getInstitutionalPrice($product->id,$customer_id,$request->hub);
                if(is_array($pr)) {
                    $pro[] = array(
                        'id' => $product->id,
                        'title' => $product->title,
                        'image' => url('/assets/products/' . image_order($product->images)),
                        'price' => round($pr['price']),
                        'loading' => round($pr['loading']),
                        'tax_per' => round($product->tax),
                        'tax' => round($pr['tax']),
                        'amount' => round($pr['total']),
                        'rates' => getRatings($product->id),
                        'validity' => $pvalidity
                    );
                }
            }
            $response = array(
                'id' => $brand->id,
                'name' => $brand->name,
                'banner_image' => url('assets/brand/' . $brand->banner_image),
                'image' => url('assets/brand/' . $brand->image),
                'products'=>$pro
            );
            return response()->json(['message' => $response, 'code' => 200], 200);
        }
    }
	
	public function rfqBooking(Request $request)
    {
		if(isset($request->API_KEY)) {
			if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
            }
            if (isset($request->token)) {
                $id = false;
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login', 'code' => 401], 200);
                }
                $req = json_decode($request->variant);
                $other='';
                $location ='';
                $quotation = array();
                if(isset($request->other_option)){
                    if($request->other_option == ''){
                        $other == '';
                    }
                    else{
                        $other == $request->other_option;
                    }
                }
                if($request->location){
                    if($request->location == ''){
                        $location == '';
                    }
                    else{
                        $location == $request->location;
                    }
                }
                $quotation = array(
                    'rfq_id' => $request->rfq_id,
                    'cat_id' => $request->cat_id,
                    'product_id' => $request->product_id,
                    'brand_id' => $request->brand_id,
                    'credit_charge_id' => $request->credit_charge_id,
                    'hub_id' => $request->hub_id,
                    'payment_option' => $request->payment_option,
                    'other_option' => $other,
                    'method' => $request->method,
                    'location' => $request->location,
                    'variants' => $request->variants,
                    'user_id' => $user->id
                );
                $items = DB::table('rfq_booking')->insert($quotation);
                if($items){
                    $status = '2';
                    DB::update("UPDATE request_quotation SET status = '$status' WHERE id = '".$request->rfq_id."'");
                }
                return response()->json(['message' => 'booked successfully ', 'code' => 200], 200);
            }
            else {
                return response()->json(['message' => 'login', 'code' => 401], 200);
            }
        }
        return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
    }
	
	public function forgotPassword(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $email = $request->mobile;
        if(empty($email)){
            $response['message'] = 'Mobile required';
            return response()->json($response, 200);
        } else {
			$c = DB::select("SELECT * FROM customers WHERE mobile = '".$email."'");
			if(count($c)){
				$customer = $c[0];
			}else{
				$c = DB::select("SELECT * FROM customers WHERE email = '".$email."'");
				if(count($c)){
					$customer = $c[0];
				}else{
					$response['message'] = 'This account does not exist';
					$error = 'This account does not exist';
					return response()->json($response, 200);
				}
			}
			$key = mt_rand(100000,999999);
			$mail = DB::update("UPDATE customers SET otp = '$key' WHERE id = '".$customer->id."'");
			$msg = "Your OTP to reset password ".$key;
			$sms = urlencode($msg);
			$no = urlencode($customer->mobile);
            $url = "https://merasandesh.com/api/sendsms?username=Aakar_360&password=Ak@12345&senderid=AKAARR&message=".$sms."&numbers=".$no."&unicode=0";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FAILONERROR, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$output = curl_exec($ch);
			$error_msg = '';
			if (curl_errno($ch)) {
				$error_msg = curl_error($ch);
			}
			curl_close($ch);
			$response['output'] = $error_msg;
			if($output) {
				$response['message'] = 'OTP sent successfully';
				return response()->json($response, 200);
			}else {
				$response['message'] = 'Failed';
				return response()->json($response, 200);
			}
        }
    }
	
	public function resendOTP(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $email = $request->mobile;
        if(empty($email)){
            $response['message'] = 'Mobile required';
            return response()->json($response, 200);
        } else {
			$c = DB::select("SELECT * FROM customers WHERE mobile = '".$email."'");
			if(count($c)){
				$customer = $c[0];
			}else{
				$c = DB::select("SELECT * FROM customers WHERE email = '".$email."'");
				if(count($c)){
					$customer = $c[0];
				}else{
					$response['message'] = 'This account does not exist';
					$error = 'This account does not exist';
					return response()->json($response, 200);
				}
			}
			$key = mt_rand(100000,999999);
			$mail = DB::update("UPDATE customers SET otp = '$key' WHERE id = '".$customer->id."'");
			$msg = "Your OTP to reset password ".$key;
			$sms = urlencode($msg);
			$no = urlencode($customer->mobile);
            $url = "https://merasandesh.com/api/sendsms?username=Aakar_360&password=Ak@12345&senderid=AKAARR&message=".$sms."&numbers=".$no."&unicode=0";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FAILONERROR, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$output = curl_exec($ch);
			$error_msg = '';
			if (curl_errno($ch)) {
				$error_msg = curl_error($ch);
			}
			curl_close($ch);
			$response['output'] = $error_msg;
			if($output) {
				$response['message'] = 'OTP sent successfully';
				return response()->json($response, 200);
			}else {
				$response['message'] = 'Failed';
				return response()->json($response, 200);
			}
        }
    }
	
	public function verifyOTP(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $email = $request->mobile;
        $otp = $request->otp;
        if(empty($email)){
            $response['message'] = 'Mobile required';
            return response()->json($response, 200);
        } else {
			$c = DB::select("SELECT * FROM customers WHERE mobile = '".$email."'");
			if(count($c)){
				$customer = $c[0];
			}else{
				$c = DB::select("SELECT * FROM customers WHERE email = '".$email."'");
				if(count($c)){
					$customer = $c[0];
				}else{
					$response['message'] = 'This account does not exist';
					$error = 'This account does not exist';
					return response()->json($response, 200);
				}
			}
			if($customer->otp == $otp){
				$response['message'] = 'success';
				return response()->json($response, 200);
			}else{
				$response['message'] = 'failed';
				return response()->json($response, 200);
			}
        }
    }
	
	public function resetPassword(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
        $password = $request->password;
        $confirm_password = $request->confirm_password;
        $email = $request->mobile;
        if(empty($password) && empty($confirm_password)){
            $response['message'] = 'Both field required';
            return response()->json($response, 200);
        }elseif($password !== $confirm_password){
			$response['message'] = 'Password not matched';
            return response()->json($response, 200);
		} else {
			if(empty($email)){
				$response['message'] = 'Mobile required';
				return response()->json($response, 200);
			} else {
				$c = DB::select("SELECT * FROM customers WHERE mobile = '".$email."'");
				if(count($c)){
					$customer = $c[0];
					$password = md5($password);
					if($customer->password == $password){
						$response['message'] = 'Old password and new password can not be same';
						return response()->json($response, 200);
					}
					$mail = DB::update("UPDATE customers SET password = '$password' WHERE id = '".$customer->id."'");
					if($mail == 1) {
						$response['message'] = 'Password changed successfully';
						return response()->json($response, 200);
					}else {
						$response['message'] = 'Failed';
						return response()->json($response, 200);
					}
				}else{
					$c = DB::select("SELECT * FROM customers WHERE email = '".$email."'");
					if(count($c)){
						$customer = $c[0];
					}else{
						$response['message'] = 'This account does not exist';
						$error = 'This account does not exist';
						return response()->json($response, 200);
					}
					$password = md5($password);
					if($customer->password == $password){
						$response['message'] = 'Old password and new password can not be same';
						return response()->json($response, 200);
					}
					$mail = DB::update("UPDATE customers SET password = '$password' WHERE id = '".$customer->id."'");
					if($mail == 1) {
						$response['message'] = 'Password changed successfully';
						return response()->json($response, 200);
					}else {
						$response['message'] = 'Failed';
						return response()->json($response, 200);
					}
				} 
			}
		}
	}
	
	public function getCountry(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
		$c = DB::select("SELECT * FROM countries");
		if($c){
			$response['countries'] = $c;
			$response['message'] = 'success';
			return response()->json($response, 200);
		}else{
			$response['message'] = 'failed';
			return response()->json($response, 200);
		}
    }
	public function getState(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
		$cid = $request->country_id;
		$c = DB::select("SELECT * FROM states WHERE country_id=".$cid);
		if($c){
			$response['states'] = $c;
			$response['message'] = 'success';
			return response()->json($response, 200);
		}else{
			$response['message'] = 'failed';
			return response()->json($response, 200);
		}
    }
	
	public function getCity(Request $request)
    {
        $response =array();
        if (!$this->validateAPI($request->API_KEY)) {
            return response()->json(['message' => 'Unauthorised'], 401);
        }
		$cid = $request->state_id;
		$c = DB::select("SELECT * FROM cities WHERE state_id=".$cid);
		if($c){
			$response['cities'] = $c;
			$response['message'] = 'success';
			return response()->json($response, 200);
		}else{
			$response['message'] = 'failed';
			return response()->json($response, 200);
		}
    }
	
	public function counterOffer(Request $request)
    {
		if(isset($request->API_KEY)) {
			if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
            }
            if (isset($request->token)) {
                $id = false;
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login', 'code' => 401], 200);
                }else{
					$quotation = array(
						'user_id' => $user->id,
						'product_id' => $request->product_id,
						'credit_charge_id' => $request->credit_charge_id,
						'hub_id' => $request->hub_id,
						'price' => $request->price,
						'quantity' => $request->quantity,
						'unit' => $request->unit,
						'status' => 1,
					);
					$items = DB::table('counter_offer')->insert($quotation);
					$response['status'] = 0;
					$response['message'] = 'success';
					$response['post_value'] = $quotation;
					return response()->json($response, 200);
				}
            }
            else {
                return response()->json(['message' => 'login', 'code' => 401], 200);
            }
        }
        return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
    }

	public function counterOfferDelete(Request $request)
    {
		if(isset($request->API_KEY)) {
			if (!$this->validateAPI($request->API_KEY)) {
                return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
            }
            if (isset($request->token)) {
                $id = false;
                $user = Customer::where('appid', $request->token)->first();
                if ($user === null) {
                    return response()->json(['message' => 'login', 'code' => 401], 200);
                }else{
			DB::table('counter_offer')->where('id',$request->counter_id)->delete();
			$response['message'] = 'success';
			return response()->json($response, 200);
		}
            }
            else {
                return response()->json(['message' => 'login', 'code' => 401], 200);
            }
        }
        return response()->json(['message' => 'Unauthorised', 'code' => 401], 401);
    }
}
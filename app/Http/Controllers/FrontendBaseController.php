<?php

namespace App\Http\Controllers;

use App\AdviceCategory;
use App\Browser;
use App\Category;
use App\Config;
use App\Country;
use App\Currency;
use App\Customer;
use App\CustomerCache;
use App\DefaultImage;
use App\DesignCategory;
use App\Footer;
use App\Lang;
use App\ManufacturingHubs;
use App\MegaMenuImage;
use App\Menu;
use App\Os;
use App\Payments;
use App\RateRequests;
use App\Referrer;
use App\RegisterImage;
use App\ServiceCategory;
use App\SpecialRequirements;
use App\Style;
use App\Tracking;
use App\Visitor;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Node\Directory;
use Image;
use Illuminate\Support\Facades\Session;

//use Auth;

class FrontendBaseController extends Controller
{
    public $cfg;
    public $style;
    public $menu;
    public $footer;
    public $languages;
    public $categories;
    public $services_categories;
    public $design_category;
    public $layout_plans;
    public $mega_design;
    public $mega_product;
    public $mega_service;
    public $mega_advice;
    public $advices_category;
    public $p_cat;
    public $s_cat;
    public $a_cat;
    public $d_cat;
    public $hub;

    public function __construct()
    {
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            return redirect('install')->send();
        }
        $configcon = Config::where('id', 1)->first();

        if($configcon !== null) {
            $this->cfg = $configcon;
        } else {
            $this->cfg = 'Data Not Available';
        }
        $stylecon = Style::first();
        if($stylecon !== null){
            $this->style = $stylecon;
        }else {
            $this->style = 'Data Not Available';
        }
        $menucon = Menu::where('parent', 0)->orderBy('o', 'ASC')->get();
        if(count($menucon)) {
            $this->menu = $menucon;
        }else {
            $this->menu = 'Data Not Available';
        }
        $footercon = Footer::orderBy('o', 'ASC')->get();
        if(count($footercon)) {
            $this->footer = $footercon;
        }else {
            $this->footer = 'Data Not Available';
        }
        $languagescon = Lang::orderBy('id', 'ASC')->get();
        if(count($languagescon)) {
            $this->languages = $languagescon;
        }else {
            $this->languages = 'Data Not Available';
        }
        $currenciescon = Currency::orderBy('default', 'ASC')->get();
        if(count($currenciescon)) {
            $this->currencies = $currenciescon;
        }else{
            $this->currencies = 'Data Not Available';
        }
        $mega_designcon = MegaMenuImage::where('id', 1)->first();
        if($mega_designcon !== null) {
            $this->mega_design = $mega_designcon;
        }else {
            $this->mega_design = 'Data Not Available';
        }
        $mega_productcon = MegaMenuImage::where('id', 2)->first();
        if($mega_productcon !== null) {
            $this->mega_product = $mega_productcon;
        }else {
            $this->mega_product = 'Data Not Available';
        }
        $mega_servicecon = MegaMenuImage::where('id', 3)->first();
        if($mega_servicecon !== null) {
            $this->mega_service = $mega_servicecon;
        }else{
            $this->mega_service = 'Data Not Available';
        }
        $mega_advicecon = MegaMenuImage::where('id', 4)->first();
        if($mega_advicecon !== null) {
            $this->mega_advice = $mega_advicecon;
        }else {
            $this->mega_advice = 'Data Not Available';
        }
        if(isset($_COOKIE['aakar_hub'])) {
            $this->hub = ManufacturingHubs::find($_COOKIE['aakar_hub']);
        }


    }

    public function welcome(){
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/themes/default/");
        Session::forget('aakar360.visitor_type');
        Session::save();
        return view('welcome')->with(compact('data'));
    }
    public function change_type($slug){
        Session::put('aakar360.visitor_type', $slug);
        if($slug == 'individual'){
            return redirect()->to('');
        }elseif($slug == 'institutional'){
            return redirect()->to('institutional-index');
        }
    }

    public function retailHeader($title = false,$desc = false,$page = false,$landing = false,$bg = false)
    {
        $data['vtype'] ='';
        $data['cust_type'] ='';

        if(Session::get('aakar360.visitor_type')){
            $data['cust_type'] = Session::get('aakar360.visitor_type');
        }

        if(customer('id') !== ''){
            $data['cust_type'] = customer('user_type');
        }
        // Update visitors count
        DB::update("UPDATE config SET views = views + 1");
        $visit = Visitor::where('date',date('Y-m-d'))->first();
        // Update today visitors
        if ($visit !== null)
        {
            DB::update("UPDATE visitors SET visits = $visit->visits+1 WHERE date = '".date('Y-m-d')."'");
        } else {
            $visitor = new Visitor();
            $visitor->date = date('Y-m-d');
            $visitor->visits = '1';
            $visitor->save();
        }
        // Get user operating system and update the database
        $useros = getOS();
        $os = Os::where('os',$useros)->first();
        if ($os !== null)
        {
            DB::update("UPDATE os SET visits = $os->visits+1 WHERE os = '".$useros."'");
        } else {
            $visitor = new Os();
            $visitor->os = $useros;
            $visitor->visits = '1';
            $visitor->save();
        }
        // Get visitor browser
        $userbrowser = getBrowser();
        $browser = Browser::where('browser',$userbrowser)->first();
        if ($browser !== null)
        {
            DB::update("UPDATE browsers SET visits = $browser->visits+1 WHERE browser = '".$userbrowser."'");
        } else {
            $visitor = new Browser();
            $visitor->browser = $userbrowser;
            $visitor->visits = '1';
            $visitor->save();
        }
        // Get the website where the user came from
        $userreferrer = getReferrer();
        if(empty($userreferrer)){$userreferrer = 'Direct';}
        $referrer = Referrer::where('referrer',$userreferrer)->first();
        if ($referrer !== null)
        {
            DB::update("UPDATE referrer SET visits = $referrer->visits+1 WHERE referrer = '".$userreferrer."'");
        } else {
            $visitor = new Referrer();
            $visitor->referrer = $userreferrer;
            $visitor->visits = '1';
            $visitor->save();
        }
        // Get visitors country
        $usercountry = getCountry();
        if($usercountry){
            DB::update("UPDATE country SET visitors = $usercountry->visitors+1 WHERE iso = '".$usercountry."'");
        }
        // Visitors tracking tool
        if (isset($_GET['tracking'])){
            $trackingcode = $_GET['tracking'];
            $tracking = Tracking::where('code',$trackingcode)->first();
            if ($tracking !== null)
            {
                DB::update("UPDATE tracking SET clicks = $tracking->clicks+1 WHERE code = '".$trackingcode."'");
            }
        }
        $data['mega_design'] = $this->mega_design;
        $data['mega_product'] = $this->mega_product;
        $data['mega_service'] = $this->mega_service;
        $data['mega_advice'] = $this->mega_advice;
        $data['stripe'] = Payments::where('code', 'stripe')->select('active','options')->first();
        $data['title'] = ($title) ? translate($title).' | '.translate($this->cfg->name) : translate($this->cfg->name);
        $data['desc'] = ($desc) ? $desc : $this->cfg->desc;
        $data['keywords'] = $this->cfg->key;
        $data['style'] = $this->style;
        $data['color'] = explode(',',$this->style->background);
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['page'] = $page;
        $data['landing'] = $landing;
        $data['bg'] = $bg;
        $data['modal_hub'] = false;
        $data['vtype']=Session::get('aakar360.visitor_type');
        $categoriescon = Category::where('parent',0)->orderBy('name','ASC')->get();
        if(count($categoriescon) > 0) {
            $data['p_cat'] = Category::where('parent',0)->orderBy('name','ASC')->get();
        }else{
            $data['p_cat'] = 'Data Not Available';
        }
        $services_categoriescon = ServiceCategory::where('parent',0)->orderBy('name','ASC')->get();
        if(count($services_categoriescon) > 0) {
            $data['s_cat'] = ServiceCategory::where('parent',0)->orderBy('name','ASC')->get();
        }else {
            $data['s_cat'] = 'Data Not Available';
        }
        $design_categorycon = DesignCategory::where('parent',0)->orderBy('name','ASC')->get();
        if($design_categorycon !== '') {
            $data['d_cat'] = DesignCategory::where('parent',0)->orderBy('name','ASC')->get();
        }else {
            $dta['d_cat'] = 'Data Not Available';
        }
        $advices_categorycon = AdviceCategory::where('parent',0)->orderBy('name','ASC')->get();
        if(!empty($advices_categorycon)) {

            $data['a_cat'] = AdviceCategory::where('parent',0)->orderBy('name','ASC')->get();
        }else{
            $data['a_cat'] = 'Data Not Available';
        }
        //dd($this->advices_category);
        $data['languages'] = $this->languages;
        $data['currencies'] = $this->currencies;
        $data['hub'] = $this->hub;
        return view('header')->with('data',$data)->render();
    }
    public function footer()
    {

        $vtype ='';
        $cust_type ='';

        if(Session::get('aakar360.visitor_type')){
            $cust_type = Session::get('aakar360.visitor_type');
        }

        if(customer('id') !== ''){
            $cust_type = customer('user_type');
        }
        // An array of social links to use them in the footer
        $social = array();
        if(!empty($this->cfg->facebook)){$social['facebook'] = $this->cfg->facebook;}
        if(!empty($this->cfg->instagram)){$social['instagram'] = $this->cfg->instagram;}
        if(!empty($this->cfg->youtube)){$social['youtube'] = $this->cfg->youtube;}
        if(!empty($this->cfg->twitter)){$social['twitter'] = $this->cfg->twitter;}
        if(!empty($this->cfg->tumblr)){$social['tumblr'] = $this->cfg->tumblr;}
        $links = $this->footer;
        $data['cfg'] = $this->cfg;
        $categoriescon = Category::where('parent',0)->orderBy('name','ASC')->get();
        if(count($categoriescon) > 0) {
            $data['p_cat'] = Category::where('parent',0)->orderBy('name','ASC')->get();
        }else{
            $data['p_cat'] = 'Data Not Available';
        }
        $services_categoriescon = ServiceCategory::where('parent',0)->orderBy('name','ASC')->get();
        if(count($services_categoriescon) > 0) {
            $data['s_cat'] = ServiceCategory::where('parent',0)->orderBy('name','ASC')->get();
        }else {
            $data['s_cat'] = 'Data Not Available';
        }
        $hubs = ManufacturingHubs::orderBy('title')->get();
        return view('footer')->with(compact('social','links','data','cust_type', 'hubs'))->render();
    }
    public function language($language_code)
    {
        // Check if the language exists and redirect the user
        $check = Lang::where('code',escape($language_code))->orderBy('id', 'ASC')->get();
        if (count($check) > 0)
        {
            setcookie('lang', $language_code,time()+31556926,'/');
        } else {
            return 'Language not found';
        }
        return redirect()->to('/');
    }
    public function currency($currency_code)
    {
        // Check if the currency exists and redirect the user
        $check = Lang::where('code',escape($currency_code))->orderBy('id', 'ASC')->get();
        if (count($check) > 0)
        {
            setcookie('currency', $currency_code,time()+31556926,'/');
        } else {
            return 'currency not found';
        }
        return redirect()->to('/');
    }

    public function institutionalHeader($title = false,$desc = false,$page = false,$landing = false,$bg = false)
    {
        $vtype ='';
        $cust_type ='';

        if(Session::get('aakar360.visitor_type')){
            $cust_type = Session::get('aakar360.visitor_type');
        }
        if(customer('id') !== ''){
            $cust_type = customer('user_type');
        }
        $data['cust_type'] = $cust_type;
        // Update visitors count
        DB::update("UPDATE config SET views = views + 1");

        $visit =  Visitor::where('date' , date('Y-m-d'))->count();
        // Update today visitors
        if ($visit > 0)
        {
            DB::update("UPDATE visitors SET visits = visits+1 WHERE date = '".date('Y-m-d')."'");
        } else {
            $visiters = new Visitor();
            $visiters->date = date('Y-m-d');
            $visiters->visits = '1';
            $visiters->save();
        }
        // Get user operating system and update the database
        $useros = getOS();
        $os = Os::where('os',$useros)->count();
        if ($os > 0)
        {
            DB::update("UPDATE os SET visits = visits+1 WHERE os = '".$useros."'");
        } else {
            DB::insert("INSERT INTO os (os,visits) VALUE ('".$useros."','1')");
        }
        // Get visitor browser
        $userbrowser = getBrowser();
        $browser = Browser::where('browser',$userbrowser)->first();
        if ($browser->count > 0)
        {
            DB::update("UPDATE browsers SET visits = visits+1 WHERE browser = '".$userbrowser."'");
        } else {
            $insertbrowers = new Browser();
            $insertbrowers->browser     =   $userbrowser;
            $insertbrowers->visits      =   '1';
            $insertbrowers->save();
        }
        // Get the website where the user came from
        $userreferrer = getReferrer();
        if(empty($userreferrer)){$userreferrer = 'Direct';}
        $referrer =  Referrer::where('referrer',$userreferrer)->count();
        if ($referrer > 0)
        {
            DB::update("UPDATE referrer SET visits = visits+1 WHERE referrer = '".$userreferrer."'");
        } else {
            $insertreferrer = new Referrer();
            $insertreferrer->referrer = $userreferrer;
            $insertreferrer->visits = '1';
            $insertreferrer->save();

        }
        // Get visitors country
        $usercountry = getCountry();
        if($usercountry){
            DB::update("UPDATE country SET visitors = visitors+1 WHERE iso = '".$usercountry."'");
        }
        // Visitors tracking tool
        if (isset($_GET['tracking'])){
            $trackingcode = $_GET['tracking'];
            $tracking = Tracking::where('code',$trackingcode)->count();
            if ($tracking->count > 0)
            {
                DB::update("UPDATE tracking SET clicks = clicks+1 WHERE code = '".$trackingcode."'");
            }
        }
        $data['mega_design'] = $this->mega_design;
        $data['mega_product'] = $this->mega_product;
        $data['mega_service'] = $this->mega_service;
        $data['mega_advice'] = $this->mega_advice;
        $data['stripe'] = Payments::where('code','=','stripe')->select('active','options')->first();
        $data['title'] = ($title) ? translate($title).' | '.translate($this->cfg->name) : translate($this->cfg->name);
        $data['desc'] = ($desc) ? $desc : $this->cfg->desc;
        $data['keywords'] = $this->cfg->key;
        $data['style'] = $this->style;
        $data['color'] = explode(',',$this->style->background);
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['page'] = $page;
        $data['landing'] = $landing;
        $data['bg'] = $bg;
        $data['vtype']=Session::get('aakar360.visitor_type');
        $categoriescon = Category::where('parent','=', 0)->orderBy('name','ASC')->count();
        if($categoriescon !== '') {
            $data['p_cat'] = Category::where('parent','=', 0)->orderBy('name','ASC')->get();
        }else{
            $data['p_cat'] = 'Data Not Available';
        }
        $services_categoriescon = ServiceCategory::where('parent','=', 0)->orderBy('name','ASC')->count();
        if($services_categoriescon !== '') {
            $data['s_cat'] = ServiceCategory::where('parent','=', 0)->orderBy('name','ASC')->get();
        }else {
            $data['s_cat'] = 'Data Not Available';
        }
        $design_categorycon = DesignCategory::where('parent','=', 0)->orderBy('name','ASC')->count();
        if($design_categorycon !== '') {
            $data['d_cat'] = DesignCategory::where('parent','=', 0)->orderBy('name','ASC')->get();
        }else {
            $dta['d_cat'] = 'Data Not Available';
        }
        $advices_categorycon = AdviceCategory::where('parent','=', 0)->orderBy('name','ASC')->count();
        if(!empty($advices_categorycon)) {

            $data['a_cat'] = AdviceCategory::where('parent','=', 0)->orderBy('name','ASC')->get();
        }else{
            $data['a_cat'] = 'Data Not Available';
        }
        //dd($this->advices_category);
        $data['languages'] = $this->languages;
        $data['currencies'] = $this->currencies;
        return view('frontInstitutional/header')->with(compact('data','cust_type'))->render();
    }
}
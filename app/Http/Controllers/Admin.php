<?php

namespace App\Http\Controllers;

use App\AdviceCategory;
use App\AdviceSave;
use App\Blog;
use App\Bookings;
use App\Category;
use App\City;
use App\CityWise;
use App\CityWiseShippingCharges;
use App\Customer;
use App\DispatchPrograms;
use App\ManufacturingHubs;
use App\PhRelations;
use App\Products;
use App\SpecialRequirements;
use App\State;
use App\Unit;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use ZipArchive;
use Illuminate\Support\Facades\Auth;
use Image;
use Yajra\Datatables\Datatables;


class Admin extends Controller
{
    public $cfg;
    public $style;
    public $buttons;
    public function __construct()
    {
        $this->cfg = DB::select('SELECT * FROM config WHERE id = 1')[0];
        $this->style = DB::select('SELECT * FROM style')[0];
        $this->buttons = "['copy', 'excel', 'pdf']";
    }

    public function header($title = 'Admin',$area = false)
    {
        $title = $title.' | '.$this->cfg->name;
        $cfg = $this->cfg;
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/header')->with(compact('title','cfg','tp','area'))->render();
    }

    public function footer()
    {
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/footer')->with(compact('tp'))->render();
    }

    public function login()
    {
        if(isset($_POST['login'])){
            $email = escape($_POST['email']);
            $pass = md5($_POST['pass']);
            if(empty($email) or empty($pass)){
                $error = '<div class="alert alert-dismissible alert-danger"> All fields are required </div>';
            } else {
                if(DB::select("SELECT COUNT(*) as count FROM user WHERE u_email = '".$email."' AND u_pass = '".$pass."' ")[0]->count > 0) {
                    $secure_id = md5(microtime());
                    DB::update("UPDATE user SET secure = '$secure_id' WHERE u_email = '".$email."'");
                    session(['admin' => $secure_id]);
                    return redirect('admin');
                } else {
                    $error = '<div class="alert alert-dismissible alert-danger"> Wrong email or password </div>';
                }
            }
        }
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/login')->with('data',$data)->render();
    }
    public function logout()
    {
        session(['admin' => '']);
        return redirect('admin/login');
    }

    public function index()
    {
        for ($day = 6; $day >= 0; $day--) {
            $d[7 - $day] = "'".date('Y-m-d', strtotime("-" . $day . " day"))."'";
            $i[date('Y-m-d', strtotime("-" . $day . " day"))] = 0;
            $o[date('Y-m-d', strtotime("-" . $day . " day"))] = 0;
            $s[date('Y-m-d', strtotime("-" . $day . " day"))] = 0;
            $c[date('Y-m-d', strtotime("-" . $day . " day"))] = 0;
        }
        $fs = DB::table("visitors")->where('date', '>', $d[1])->orderBy('date','ASC')->get();
        foreach($fs as $visits){
            $i[$visits->date] = $visits->visits;
        }
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') .' -1 day'));
        $yvisits = (!in_array($yesterday,$i)) ? $i[$yesterday] : 0;
        $tvisits = (!in_array(date('Y-m-d'),$i)) ? $i[date('Y-m-d')] : 0;
        $order_query = DB::select("SELECT * FROM orders WHERE date > ".$d[1]." ORDER BY date ASC");
        foreach ($order_query as $order){
            $o[$order->date] = $o[$order->date]+1;
            $s[$order->date] = $s[$order->date]+$order->summ;
            $c[$order->date] = $o[$order->date] / $i[$order->date]*100;
        }
        if ($yorders = DB::table("orders")->where('date', '=', $yesterday)->count() > 0){
            $ysales = DB::table("orders")->select(DB::raw('SUM(summ) as sum'))->where('date', '=', $yesterday)->sum('summ');
        } else {
            $ysales = 0;
        }
        $torders = DB::table("visitors")->select(DB::raw('COUNT(*) as count'))->where('date', '=', date('Y-m-d'))->count();
        if ($torders > 0){
            $tsales = DB::table("orders")->select(DB::raw('SUM(summ) as sum'))->where('date', '=', date('Y-m-d'))->sum('summ');
        } else {
            $tsales = 0;
        }
        $yconversion = $c[$yesterday];
        $tconversion = $c[date('Y-m-d')];
        $mvisits = max($i)+max($i)*2/6+1;
        $morders = max($o)+max($o)*2/6+1;
        $msales = max($s)+max($s)*2/6+1;
        $mconversion = max($c)+max($c)*2/6+1;
        $porders = p($yorders,$torders);
        $pvisits = p($yvisits,$tvisits);
        $psales = p($ysales,$tsales);
        $pconversion = p($yconversion,$tconversion);
        $stat['0'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))->count();
        $stat['1'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))->where('stat', '=', 1)->count();
        $stat['2'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))->where('stat', '=', 2)->count();
        $stat['3'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))->where('stat', '=', 3)->count();
        $stat['4'] = DB::table("orders")->select(DB::raw('COUNT(*) as count'))->where('stat', '=', 4)->count();
        $emails = array();
        $emails['orders'] = DB::table("orders")->select(DB::raw('COUNT(email) as count'))->count('email');
        $emails['support'] = DB::table("tickets")->select(DB::raw('COUNT(email) as count'))->count('email');
        $emails['newsletter'] = DB::table("subscribers")->select(DB::raw('COUNT(email) as count'))->count('email');
        $chart = array();
        $chart['days'] = implode(', ',$d);
        $i = implode(', ',$i);
        $o = implode(', ',$o);
        $s = implode(', ',$s);
        $c = implode(', ',$c);
        $ssales = DB::table("orders")->select(DB::raw('SUM(summ) as sum'))->sum('summ');
        $orders = DB::table("orders")->orderBy('id','DESC')->limit(3)->get();
        $reviews = DB::table("reviews")->orderBy('id','DESC')->limit(3)->get();
        $tickets = DB::table("tickets")->orderBy('id','DESC')->limit(3)->get();
        $referrers = DB::table("referrer")->orderBy('visits','DESC')->limit(3)->get();
        $oss = DB::table("os")->orderBy('visits','DESC')->limit(3)->get();
        $browsers = DB::table("browsers")->orderBy('visits','DESC')->limit(3)->get();
        $subscribers = DB::table("subscribers")->limit(6)->get();
        $countries = DB::table("country")->orderBy('visitors','DESC')->orderBy('orders','DESC')->limit(10)->get();
        $cfg = $this->cfg;
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->header('Admin','index');
        $footer = $this->footer();
        return view('admin/index')->with(compact('header','cfg','tp','d','i','o','s','c','stat','porders','pvisits','psales','pconversion','ssales','orders','reviews','tickets','referrers','oss','browsers','emails','subscribers','countries','chart','morders','mvisits','mconversion','msales','footer'));
    }

    public function map(){
        $json = array();
        $countries = DB::table("country")->orderBy('id','ASC')->get();
        foreach ($countries as $country) {
            $json[strtolower($country->iso)] = array(
                'total'  => $country->orders,
                'visitors'  => $country->visitors,
            );
        }
        return json_encode($json);
    }

    public function manufacturingHubs(){
        $notices = '';
        if(isset($_POST['add'])){
            $data['title'] = $_POST['name'];
            ManufacturingHubs::insert($data);
            $notices .= "<div class='alert mini alert-success'>Hub has been added successfully!</div>";
        }
        if(isset($_POST['edit'])){
            $h = ManufacturingHubs::find($_GET['edit']);
            if($h === null){
                return back();
            }
            $h->title = $_POST['name'];
            $h->save();
            $notices .= "<div class='alert mini alert-success'>Hub edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            if(checkHubLinks($_GET['delete'])){
                $notices .= "<div class='alert alert-danger'>Hub is linked with one or more products!</div>";
            }else {
                DB::table("manufacturing_hubs")->where('id', '=', $_GET['delete'])->delete();
                $notices .= "<div class='alert alert-success'>Hub has been deleted successfully !</div>";
            }
        }
        $header = $this->header('Manufacturing Hubs','mhubs');
        $hubs = ManufacturingHubs::orderBy('id', 'DESC')->get();
        $hub = null;
        if(isset($_GET['edit'])) {
            $hub = ManufacturingHubs::find($_GET['edit']);
        }
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/manufacturing-hubs')->with(compact('header','notices','tp','hubs','hub','footer'));
    }

    public function productSr(){
        $notices = '';
        $data['title'] = 'Product Special Requirements';
        $header = $this->header($data['title'],'product_sr');
        $srs = SpecialRequirements::orderBy('id', 'DESC')->get();
        $footer = $this->footer();
        return view('admin/products-sr')->with(compact('header','notices','srs','footer', 'data'));
    }

    public function phRelations(){
        $notices = '';
        if(isset($_POST['add'])){
            $data['hubs'] = implode(',', $_POST['hubs']);
            $prices = $_POST['price'];
            $lprices = $_POST['lprice'];
            $pros = array();
            foreach($_POST['products'] as $key=>$p){
                $pros[] = array(
                    'product' => $p,
                    'price' => $prices[$key],
                    'loading' => $lprices[$key]
                );
            }
            $data['products'] = json_encode($pros);
            PhRelations::insert($data);
            $notices .= "<div class='alert mini alert-success'>PH Relation has been added successfully!</div>";
        }
        if(isset($_POST['edit'])){
            $ph = PhRelations::find($_GET['edit']);
            if($ph === null){
                return back();
            }
            $ph->hubs = implode(',', $_POST['hubs']);
            $prices = $_POST['price'];
            $lprices = $_POST['lprice'];
            $pros = array();
            foreach($_POST['products'] as $key=>$p){
                $pros[] = array(
                    'product' => $p,
                    'price' => $prices[$key],
                    'loading' => $lprices[$key]
                );
            }
            $ph->products = json_encode($pros);
            $ph->save();
            $notices .= "<div class='alert mini alert-success'>PH Relation edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('ph_relations')->where('id', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>PH Relations has been deleted successfully !</div>";
        }
        $header = $this->header('Products-Hubs Relations','phrelations');
        $hubs = ManufacturingHubs::orderBy('title', 'ASC')->get();
        $products = Products::orderBy('title', 'ASC')->get();
        $phs = PhRelations::orderBy('id', 'DESC')->get();
        $ph = null;
        if(isset($_GET['edit'])) {
            $ph = PhRelations::find($_GET['edit']);
        }
        $footer = $this->footer();
        return view('admin/ph-relations')->with(compact('header','notices','hubs', 'phs', 'ph', 'products','footer'));
    }

    public function products()
    {
        $data['notices'] = '';
        $data['variants'] = [];
        $cat_name ='';
        $data['ph'] = null;
        if(isset($_POST['add'])){
            $udata['title'] = $_POST['title'];
            $udata['text'] = $_POST['text'];
            $udata['specification'] = $_POST['specification'];
            $udata['purchase_price'] = $_POST['purchase_price'];
            $udata['price'] = $_POST['price'];
            $udata['tax'] = $_POST['tax'];
            $udata['asr'] = $_POST['asr'];
            $udata['product_showprice'] = $_POST['sp'];
            $udata['category'] = (int)$_POST['category'];
            $udata['description_view'] = (int)$_POST['description_view'];
            $udata['specification_view'] = (int)$_POST['specification_view'];
            $udata['rating_view'] = (int)$_POST['rating_view'];
            $udata['faq_view'] = (int)$_POST['faq_view'];
            $udata['show_weight_length'] = (int)$_POST['show_weight_length'];
            $category_id = (int)$_POST['category'];
            $category_detail = Category::select(['parent','name'])->where('id','=',$category_id)->first();
            $product_max_id = DB::select("SELECT max(id) as maxid FROM products")[0];
            $new_pid = $product_max_id->maxid +1;
            $pid=$category_detail->parent;
            $sku_code ='';
            $cat_name =  $category_detail->name;
            $sku_code = strtolower(substr($cat_name, 0, 3));
            if($pid==0){
                $sku_code = $sku_code;
            }else{
                while($pid !=0){
                    $category_detail = Category::where('id','=',$pid)->get('parent','name');
                    $sku_code = strtolower(substr($category_detail->name, 0, 3)).$sku_code;
                    $pid = $category_detail->parent;
                }
            }
            $sku_code = $sku_code.'-'.$new_pid;
            $udata['sku'] = $sku_code;
            $udata['quantity'] = (int)$_POST['quantity'];
            $udata['images'] = '';
            $udata['download'] = '';
            $udata['fb_cat'] = '';
            $udata['hsn'] = $_POST['hsn'];
            $udata['min_qty'] = $_POST['min'];
            $udata['max_qty'] = $_POST['max'];
            $udata['institutional_min_quantity'] = $_POST['min_ins'];
            $udata['sale'] = $_POST['sale'];
            $udata['weight'] = $_POST['weight'];
            $udata['weight_unit'] = $_POST['weight_unit'];
            $udata['brand_id'] = $_POST['brand_id'];
            if(isset($_POST['fb_cat'])){
                $udata['fb_cat'] = implode(',', $_POST['fb_cat']);
            }
            if(isset($_POST['is_variable'])){
                $udata['is_variable'] = $_POST['is_variable'];
            }
            if(isset($_POST['validity'])){
                $udata['validity'] = date('Y-m-d H:i:s',strtotime($_POST['validity']));
            }
            if(isset($_POST['substract_stock'])){
                $udata['sub_stock'] = $_POST['substract_stock'];
            }
            if(isset($_POST['grade'])){
                $udata['grade'] = implode(',', $_POST['grade']);
            }
            $options = array();
            if (isset($_POST['option_title'])){
                $choice_titles = $_POST['option_title'];
                $choice_types = $_POST['option_type'];
                $choice_no = $_POST['option_no'];
                if(count($choice_titles ) > 0){
                    foreach ($choice_titles as $i => $row) {
                        $choice_options = $_POST['option_set'.$choice_no[$i]];
                        $options[] = array(
                            'no' => $choice_no[$i],
                            'title' => $choice_titles[$i],
                            'name' => 'choice_'.$choice_no[$i],
                            'type' => $choice_types[$i],
                            'option' => $choice_options
                        );
                    }
                }
            }
            $udata['options'] = json_encode($options);
            $product = DB::table('products')->insertGetId($udata);
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'products/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path . $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path . $image);
                            $thumb = Image::make($path . $image)->resize(165, 110)->save($path .'thumbs/'. $image);
                            $order++;
                        }
                    } else {
                        $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE products SET images = '".implode(',',$images)."' WHERE id = '".$product."'");
            }
            if (request()->file('download')) {
                $name = request()->file('download')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('download')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'downloads';
                request()->file('download')->move($path,$file);
                DB::update("UPDATE products SET download = '".$file."' WHERE id = '".$product."'");
            }
            if(isset($_POST['hubs']) && isset($_POST['price']) && isset($_POST['lprice'])) {
                $data1['products'] = $product;
                $pprices = $_POST['pprice'];
                $prices = $_POST['iprice'];
                $lprices = $_POST['lprice'];
                $ins_quantity = $_POST['ins_quantity'];
                $ins_validity = $_POST['ins_validity'];
                $hubs = array();
                foreach ($_POST['hubs'] as $key => $h) {
                    $hubs[] = array(
                        'hub' => $h,
                        'pprice' => $pprices[$key],
                        'price' => $prices[$key],
                        'loading' => $lprices[$key],
                        'quantity' => $ins_quantity[$key],
                        'validity' => $ins_validity[$key]
                    );
                }
                $data1['hubs'] = json_encode($hubs);
                PhRelations::insert($data1);
            }else{
                PhRelations::where('products', $product)->delete();
            }
            if(isset($_POST['cities']) && isset($_POST['cwprice']) && isset($_POST['cwlprice'])) {
                $cwpprices = $_POST['cwpprice'];
                $cwprices = $_POST['cwprice'];
                $cwlprices = $_POST['cwlprice'];
                $cwins_quantity = $_POST['cwins_quantity'];
                $cwins_validity = $_POST['cwins_validity'];
                $cities = array();
                $prodata = array();
                $data1['product_id'] = $product;
                foreach ($_POST['cities'] as $key => $h) {
                    $data1['city'] = $h;
                    $prodata[] = array(
                        'product' => $product,
                        'pprice' => $cwpprices[$key],
                        'price' => $cwprices[$key],
                        'loading' => $cwlprices[$key],
                        'quantity' => $cwins_quantity[$key],
                        'validity' => $cwins_validity[$key]
                    );
                }
                $data1['prices'] = json_encode($prodata);
                CityWise::insert($data1);
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Product has been added successfully !</div>";
        }
        if(isset($_POST['edit'])){
            $product = Products::find($_GET['edit']);
            $product->title = escape($_POST["title"]);
            $product->text = escape($_POST["text"]);
            $product->specification = escape($_POST["specification"]);
            $product->purchase_price = $_POST["purchase_price"];
            $product->price = $_POST["price"];
            $product->tax = $_POST["tax"];
            $product->asr = $_POST["asr"];
            $product->product_showprice = $_POST["sp"];
            $product->hsn = $_POST["hsn"];
            $product->min_qty = $_POST["min"];
            $product->max_qty = isset($_POST["max"])?$_POST["max"]:'';
            $product->institutional_min_quantity = $_POST["min_ins"];
            $product->sale = $_POST["sale"];
            $product->weight = $_POST["weight"];
            $product->weight_unit = $_POST["weight_unit"];
            $product->quantity = (int)$_POST["quantity"];
            $product->category = (int)$_POST["category"];
            $product->brand_id = (int)$_POST["brand_id"];
            $product->description_view = (int)$_POST['description_view'];
            $product->specification_view = (int)$_POST['specification_view'];
            $product->rating_view = (int)$_POST['rating_view'];
            $product->faq_view = (int)$_POST['faq_view'];
            $product->show_weight_length = (int)$_POST['show_weight_length'];
            $product->is_variable = 0;
            $product->validity = null;
            $product->sub_stock = 0;
            $fb_cat = '';
            if(isset($_POST['fb_cat'])){
                $product->fb_cat = implode(',', $_POST['fb_cat']);
            }
            if(isset($_POST['is_variable'])){
                $product->is_variable = $_POST['is_variable'];
            }
            if(isset($_POST['validity'])){
                $product->validity = date('Y-m-d H:i:s',strtotime($_POST['validity']));
            }
            if(isset($_POST['substract_stock'])){
                $product->sub_stock = $_POST['substract_stock'];
            }
            $grade = '';
            if(isset($_POST['grade'])){
                $product->grade = implode(',', $_POST['grade']);
            }
            $options = array();
            if (isset($_POST['option_title'])){
                $choice_titles = $_POST['option_title'];
                $choice_types = $_POST['option_type'];
                $choice_no = $_POST['option_no'];
                if(count($choice_titles ) > 0){
                    foreach ($choice_titles as $i => $row) {
                        $choice_options = $_POST['option_set'.$choice_no[$i]];
                        $options[] = array(
                            'no' => $choice_no[$i],
                            'title' => $choice_titles[$i],
                            'name' => 'choice_'.$choice_no[$i],
                            'type' => $choice_types[$i],
                            'option' => $choice_options
                        );
                    }
                }
            }
            $product->options = json_encode($options);
            $product->save();
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'products/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path . $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path . $image);
                            $thumb = Image::make($path . $image)->resize(165, 110)->save($path .'thumbs/'. $image);
                            $order++;
                        }
                    } else {
                        $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE products SET images = '".implode(',',$images)."' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('download')) {
                $name = request()->file('download')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('download')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'downloads';
                request()->file('download')->move($path,$file);
                DB::update("UPDATE products SET download = '".$file."' WHERE id = '".$_GET['edit']."'");
            }
            if(isset($_POST['hubs']) && isset($_POST['price']) && isset($_POST['lprice'])) {
                $data1['products'] = $_GET['edit'];
                $prices = $_POST['iprice'];
                $lprices = $_POST['lprice'];
                $ins_quantity = $_POST['ins_quantity'];
                $ins_validity = $_POST['ins_validity'];
                $hubs = array();
                foreach ($_POST['hubs'] as $key => $h) {
                    $qty = '';
                    if($key<1){
                        $qty = $ins_quantity[$key];
                    }
                    $hubs[] = array(
                        'hub' => $h,
                        'price' => $prices[$key],
                        'loading' => $lprices[$key],
                        'quantity' => $qty,
                        'validity' => $ins_validity[$key]
                    );
                }
                $ph = PhRelations::where('products', $_GET['edit'])->first();
                if($ph === null) {
                    $data1['hubs'] = json_encode($hubs);
                    PhRelations::insert($data1);
                }else{
                    $ph->products = $_GET['edit'];
                    $ph->hubs = json_encode($hubs);
                    $ph->save();
                }
            }else{
                PhRelations::where('products', $_GET['edit'])->delete();
            }
            if(isset($_POST['cities']) && isset($_POST['cwprice']) && isset($_POST['cwlprice'])) {
                $citiesData = CityWise::where('product_id', $_GET['edit'])->delete();
                $cwpprices = $_POST['cwpprice'];
                $cwprices = $_POST['cwprice'];
                $cwlprices = $_POST['cwlprice'];
                $cwins_quantity = $_POST['cwins_quantity'];
                $cwins_validity = $_POST['cwins_validity'];
                $cities = array();
                $prodata = array();
                $data12['product_id'] = $_GET['edit'];
                foreach ($_POST['cities'] as $key => $h) {
                    if(!empty($cwpprices[$key]) && !empty($cwprices[$key]) && !empty($cwlprices[$key]) && !empty($cwins_quantity[$key]) && !empty($cwins_validity[$key])) {
                        $data12['city'] = $h;
                        $prodata[] = array(
                            'product' => $_GET['edit'],
                            'pprice' => $cwpprices[$key],
                            'price' => $cwprices[$key],
                            'loading' => $cwlprices[$key],
                            'quantity' => $cwins_quantity[$key],
                            'validity' => $cwins_validity[$key]
                        );
                    }
                }
                $data12['prices'] = json_encode($prodata);
                CityWise::insert($data12);
            }
            $data['notices'] .= '<div class="alert mini alert-success">Product updated successfully !</div>';
        }
        if(isset($_POST['add_variant'])){
            $pid = $_GET['add_variants'];
            $title = $_POST['variant_title'];
            $price = $_POST['price'];
            $display = $_POST['display'];
            $manufacture = $_POST['manufacture'];
            $length = $_POST['length'];
            $s_available = '';
            DB::insert("INSERT INTO product_variants (product_id, variant_title, price,display,length,manufacture)
            VALUE ('$pid','$title','$price', '$display','$length', '$manufacture')");
        }
        if(isset($_POST['add_group_variant'])){
            $pid = $_GET['add_variants'];
            $i=0;
            DB::table('product_variants')->where('product_id','=',$pid)->delete();
            foreach (  $_POST['variant_title'] as $key=>$vt){
                $title = $_POST['variant_title'][$i];
                $check = DB::table('product_variants')->where('variant_title', '=', $title)->where('product_id', '=', $pid)->first();
                $price = '';
                if(isset($_POST['price'][$vt])){
                    $price = ($_POST['price'][$vt] == '' ? 0 : $_POST['price'][$vt]);
                }
                $display = $_POST['display'.$i];
                $manufacture = $_POST['manufacture'.$i];
                $length = '';
                if(isset($_POST['length'.$i])){
                    $length = implode(',', $_POST['length'.$i]);
                }
                $si = '';
                DB::insert("INSERT INTO product_variants (product_id, variant_title, price,display,length,manufacture)
                    VALUE ('$pid','$title','$price', '$display','$length','$manufacture')");
                $i++;
            }
        }
        if(isset($_POST['edit_variant'])){
            $var_id = $_GET['edit_variants'];
            $ret_id = $_GET['ret_id'];
            $title = $_POST['variant_title'];
            $price = $_POST['price'];
            $display = $_POST['display'];
            $manufacture = $_POST['manufacture'];
            $length = $_POST['length'];
            DB::update("UPDATE product_variants SET variant_title='$title', price='$price', display='$display', length='$length', manufacture='$manufacture' WHERE id=$var_id");
            if($ret_id){
                return redirect()->to('admin/products?add_variants='.$ret_id);
            }
        }
        if(isset($_POST['add_discount'])){
            $pid = $_GET['add_discount'];
            $quantity = $_POST['quantity'];
            $discount = $_POST['discount'];
            $discount_type = $_POST['discount_type'];
            $user_type = $_POST['user_type'];
            DB::insert("INSERT INTO product_discount (product_id, quantity, discount,discount_type,user_type) VALUE ('$pid','$quantity','$discount', '$discount_type','$user_type')");
        }
        if(isset($_POST['edit_discount'])){
            $id = $_POST['dis_id'];
            $pid = $_GET['add_discount'];
            $quantity = $_POST['quantity'];
            $discount = $_POST['discount'];
            $discount_type = $_POST['discount_type'];
            DB::update("UPDATE product_discount SET quantity='$quantity', discount='$discount', discount_type='$discount_type' WHERE id=$id");
        }
        if(isset($_GET['delete']) && isset($_GET['add_variants'])){
            DB::table('product_variants')->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-dismissible alert-success'> Variant has been deleted </div>";
        }
        if(isset($_GET['delete']) && isset($_GET['add_discount'])){
            DB::delete("DELETE FROM product_discount WHERE id = '".$_GET['delete']."' ");
            $data['notices'] .= "<div class='alert alert-dismissible alert-success'> Discount has been deleted </div>";
        }
        if(isset($_GET['delete']) && !isset($_GET['add_variants']) && !isset($_GET['add_discount'])){
            $db = DB::table("ph_relations")->where('products', '=', $_GET['delete'])->delete();
            DB::table("products")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-dismissible alert-success'> Product has been deleted </div>";
        }
        if(isset($_GET['edit'])){
            $product = DB::table("products")->where('id', '=', $_GET['edit'])->first();
            $data['product'] = DB::table("products")->where('id', '=', $_GET['edit'])->first();
            $data['cat'] = DB::table('category')->where('id', '=', $product->category)->first();
            $cat = DB::table('category')->where('id', '=', $product->category)->first();
            $data['wunits'] = DB::table('units')->whereIn('id', explode(',', $cat->weight_unit))->get();
            $data['ph'] = PhRelations::where('products', $_GET['edit'])->first();
            $data['cityWise'] = CityWise::where('product_id', $_GET['edit'])->get();
        }
        if(isset($_GET['add_variants'])){
            $data['variants'] = DB::table('product_variants')->where('product_id', '=', $_GET['add_variants'])->get();
            $data['product'] = DB::table('products')->where('id', '=', $_GET['add_variants'])->first();
        }
        if(isset($_GET['add_discount'])){
            $data['discounts_individual'] =  DB::table('product_discount')->where('user_type', '=', 'individual')->where('product_id', '=', $_GET['add_discount'])->orderBy('quantity','ASC')->get();
            $data['discounts_institutional'] = DB::table('product_discount')->where('user_type','=','institutional')->where('product_id','=',$_GET['add_discount'])->orderBy('quantity','ASC')->get();
            $data['product'] = DB::table("products")->where('id', '=', $_GET['add_discount'])->first();
        }
        $data['header'] = $this->header('Products','products');
        $data['products'] = DB::table('products')->orderBy('id','DESC')->get();
        $data['brands'] = DB::table('brand')->orderBy('id','DESC')->get();
        $data['grades'] = DB::table('product_type')->orderBy('id','DESC')->get();
        $data['categories'] = DB::table('category')->where('parent','=',0)->orderBy('id','DESC')->get();
        $data['units'] = DB::table('units')->orderBy('name','ASC')->get();
        $data['lengths'] = DB::table('length')->orderBy('length','ASC')->get();
        $data['sizes'] = DB::table('size')->orderBy('id','ASC')->get();
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['groups'] = DB::table('sizes_group')->orderBy('id','ASC')->get();
        $data['hubs'] = ManufacturingHubs::orderBy('title', 'ASC')->get();
        $data['buttons'] = $this->buttons;
        $data['cities'] = City::where('is_available','1')->get();
        return view('admin/products')->with('data', $data);
    }

    public function categories(Request $request){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cat = new Category();
            $cat->name = $_POST['name'];
            $cat->path = $_POST['path'];
            $cat->parent = $_POST['parent'];
            $cat->popular = $_POST['popular'];
            $cat->popular_priority = $_POST['popular_priority'];
            $cat->popular_brands_priority = $_POST['popular_brands_priority'];
            $cat->popular_in_brands = $_POST['popular_in_brands'];
            $cat->consumption_ratio = $_POST['consumption_ratio'];
            $cat->is_view = $_POST['is_view'];
            $cat->brands = implode(',', $_POST['brands']);
            $cat->weight_units = implode(',', $_POST['weight_unit']);
            if(isset($_POST['filter'])) {
                $cat->filter = implode(',', $_POST['filter']);
            }

            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'products/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            if (request()->file('banner')) {
                $bname = request()->file('banner')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('banner')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('banner')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
            }
            if (request()->file('brand_banner')) {
                $bbname = request()->file('banner')->getClientOriginalName();
                $bbfile = md5(time()).'.'.request()->file('brand_banner')->getClientOriginalExtension();
                $bbpath = base_path().'/assets/'.'products/';
                request()->file('brand_banner')->move($bbpath,$bbfile);
                $img = Image::make($bbpath.$bbfile)->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bbpath.$bbfile);
            }
            $cat->image = (isset($file)) ? $file : '';
            $cat->banner = (isset($bfile)) ? $bfile : '';
            $cat->brand_banner = (isset($bbfile)) ? $bbfile : '';
            $cat->save();
//            DB::insert("INSERT INTO category (image,banner,brand_banner) VALUE ('$file','$bfile','$bbfile')");
            $data['notices'] .= "<div class='alert mini alert-success'> Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $cat = Category::where('id', $_GET['edit'])->first();
            $cat->name = $_POST['name'];
            $cat->path = $_POST['path'];
            $cat->parent = $_POST['parent'];
            $cat->popular = $_POST['popular'];
            $cat->popular_priority = $_POST['popular_priority'];
            $cat->popular_brands_priority = $_POST['popular_brands_priority'];
            $cat->popular_in_brands = $_POST['popular_in_brands'];
            $cat->consumption_ratio = $_POST['consumption_ratio'];
            $cat->is_view = $_POST['is_view'];

            if(isset($_POST['filter'])) {
                $cat->filter = implode(',', $_POST['filter']);
            }
            if(isset($_POST['brands'])){
                $cat->brands = implode(',', $_POST['brands']);
            }
            $cat->weight_units = implode(',', $_POST['weight_unit']);
            $cat->save();
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'products/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE category SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('banner')) {
                $bname = request()->file('banner')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('banner')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('banner')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE category SET banner = '$bfile' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('brand_banner')) {
                $bbname = request()->file('brand_banner')->getClientOriginalName();
                $bbfile = md5(time()).'.'.request()->file('brand_banner')->getClientOriginalExtension();
                $bbpath = base_path().'/assets/'.'products/';
                request()->file('brand_banner')->move($bbpath,$bbfile);
                $img = Image::make($bbpath.$bbfile)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bbpath.$bbfile);
                DB::update("UPDATE category SET brand_banner = '$bbfile' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Categories','categories');
        $data['categories'] = DB::table('category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['category'] = DB::table('category')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['parents'] = DB::table('category')->where('parent','=',0)->orderBy('id','DESC')->get();

        $data['brands'] = DB::table('brand')->orderBy('name','ASC')->get();
        $data['filters'] = DB::table('filter')->orderBy('name','ASC')->get();
        $data['units'] = DB::table('units')->orderBy('id','ASC')->get();
        return view('admin/categories')->with('data',$data);
    }

    public function featured(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cid = $_POST['category'];
            $pids = implode(',', $_POST['products']);
            DB::insert("INSERT INTO featured_products (product_id, category_id) VALUE ('$pids','$cid')");
            $data['notices'] .= "<div class='alert mini alert-success'> Data saved successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $cid = $_POST['category'];
            $pids = implode(',', $_POST['products']);
            DB::update("UPDATE featured_products SET category_id = '$cid',product_id = '$pids' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Featured Products','featured-products');
        $data['featured_products'] = DB::table('featured_products')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['featured'] =  DB::table('featured_products')->where('id','=',$_GET['edit'])->first();

        }
        $data['footer'] = $this->footer();
        $data['products'] = DB::table('products')->orderBy('title','ASC')->get();
        $data['categories'] = DB::table('category')->orderBy('name','ASC')->get();
        return view('admin/featured')->with('data',$data);
    }

    public function brands(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $bname = $_POST['name'];
            $path = $_POST['path'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'brand/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            if (request()->file('banner_image')) {
                $name1 = request()->file('banner_image')->getClientOriginalName();
                $file1 = md5(time()).'.'.request()->file('banner_image')->getClientOriginalExtension();
                $path1 = base_path().'/assets/'.'brand/';
                request()->file('banner_image')->move($path1,$file1);
                $img = Image::make($path1.$file1)->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path1.$file1);
            }
            DB::insert("INSERT INTO brand (name,path,image,banner_image) VALUE ('$bname','$path','$file','$file1')");
            $data['notices'] .= "<div class='alert mini alert-success'> Brand has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $bname = $_POST["name"];
            $path = $_POST["path"];
            DB::update("UPDATE brand SET name = '$bname',path = '$path' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/brand/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path,$file);
                DB::update("UPDATE brand SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('banner_image')) {
                $name1 = request()->file('banner_image')->getClientOriginalName();
                $file1 = md5(time()).'.'.request()->file('banner_image')->getClientOriginalExtension();
                $path1 = base_path().'/assets/brand/';
                request()->file('banner_image')->move($path1,$file1);
                $img = Image::make($path1,$file1)->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path1.$file1);
                DB::update("UPDATE brand SET banner_image = '$file1' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'>Brand edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("brand")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Brand has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Brands','brands');
        $data['brands'] = DB::table('brand')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['brand'] = DB::table('brand')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/brands')->with('data',$data);
    }

    public function layout_addon(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $aname = $_POST['name'];
            $price = $_POST['price'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'addon/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO layout_addon (`name`,`price`,`image`) VALUE ('$aname','$price','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'> Addon has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST["name"];
            $price = $_POST["price"];
            DB::update("UPDATE layout_addon SET name = '$name',price = '$price' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'addon/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE layout_addon SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'>Addon edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("layout_addon")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Addon has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Layout Addons','layout addons');
        $data['addons'] = DB::table('layout_addon')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['addon'] = DB::table('layout_addon')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/layout_addon')->with('data',$data);
    }

    public function services_categories(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name1 = $_POST['name'];
            $description = $_POST['description'];
            $path1 = $_POST['path'];
            $parent = $_POST['parent'];
            $display = $_POST['display'];
            $list = $_POST['list'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'services/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO services_category (name,description,display, list, path,parent,image) VALUE ('$name1','$description','$display', '$list','$path1','$parent','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'>Service Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name1 = $_POST["name"];
            $description = $_POST['description'];
            $path1 = $_POST["path"];
            $parent = $_POST["parent"];
            $display = $_POST["display"];
            $list = $_POST["list"];
            DB::update("UPDATE services_category SET name = '$name1',description='$description', display = '$display', list = '$list', path = '$path1',parent = '$parent' WHERE id = '" . $_GET['edit'] . "'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'services/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE services_category SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('services_category')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Service Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Service Categories','services_categories');
        $data['services_categories'] = DB::table('services_category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['services_categories'] = DB::table('services_category')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['parents'] = DB::table('services_category')->where('parent',0)->orderBy('id','DESC')->get();
        return view('admin/services_categories')->with('data',$data);
    }

    public function services(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $price = $_POST['price'];
            $category = $_POST['category'];
            $display = $_POST['display'];
            $payment_mode = $_POST['payment_mode'];
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().'/assets/'.'services/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path . $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path.$image);
                            $order++;
                        }
                    } else {
                        $data['notices'] .= "<div class='alert mini alert-warning'> ".$name." is not a valid format</div>";
                    }
                }
            }
            DB::insert("INSERT INTO services (title,description,price,category,images,display,payment_mode) VALUE ('$title','$description','$price','$category','".implode(',',$images)."','$display','$payment_mode')");
            $data['notices'] .= "<div class='alert mini alert-success'>Service has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $price = $_POST['price'];
            $category = $_POST['category'];
            $display = $_POST['display'];
            $payment_mode = $_POST['payment_mode'];
            DB::update("UPDATE services SET title = '$title',description = '$description',price = '$price',category = '$category',display = '$display',payment_mode = '$payment_mode' WHERE id = '" . $_GET['edit'] . "'");
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'.'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().'/assets/'.'services/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path . $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path . $image);
                            $order++;
                        }
                    } else {
                        $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE services SET images = '".implode(',',$images)."' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('services')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Service has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Services','services');
        $data['services'] = DB::table('services')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['services'] = DB::table('services')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['parents'] = DB::table('services_category')->where('parent','<>',0)->orderBy('id','DESC')->get();
        return view('admin/services')->with('data',$data);
    }

    public function updateQuestions(){
        $notices = '';
        $header = $this->header('Question Master','questions');
        if(isset($_POST['update'])){
            DB::table("services_questions")->where('service_id', '=', $_GET['edit'])->where('question_id', '=', $_POST['question'])->delete();
            DB::insert("INSERT INTO services_questions (service_id,question_id,priority) VALUE ('".$_GET['edit']."','".$_POST['question']."','".$_POST['priority']."')");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if(isset($_GET['delete'])){
            DB::table("services_questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        }
        $footer = $this->footer();
        $questions = DB::table('questions')->orderBy('question','ASC')->get();
        $service_questions = DB::table('services_questions')->where('service_id','=',$_GET['edit'])->orderBy('priority','ASC')->get();
        return view('admin/update-questions')->with(compact('questions', 'header', 'footer', 'notices', 'service_questions'));
    }

    public function ServicesAnswer(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['answer'] = DB::table('services_answer')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            DB::update("UPDATE services_answer SET status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Status successfully updated !</div>";
        }
        $data['header'] = $this->header('Answer Master','answers');
        $data['footer'] = $this->footer();
        $data['answers'] = DB::table('services_answer')->orderBy('id','DESC')->get();
        return view('admin/services_answer')->with('data',$data);
    }

    public function modifyPlan(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['plan'] = DB::table('modify_plan')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            DB::update("UPDATE modify_plan SET status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Status successfully updated !</div>";
        }
        $data['header'] = $this->header('Modify Plan','modify plan');
        $data['footer'] = $this->footer();
        $data['plans'] = DB::table('modify_plan')->orderBy('id','DESC')->get();
        return view('admin/modify_plan')->with('data',$data);
    }

    public function plan_orders(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['plan'] = DB::table('layout_order')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_GET['details'])){
            $data['pland'] = DB::table('layout_order')->where('id','=',$_GET['details'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            DB::update("UPDATE layout_order SET status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Status successfully updated !</div>";
        }
        $data['header'] = $this->header('Plan Orders','plan orders');
        $data['footer'] = $this->footer();
        $data['plans'] = DB::table('layout_order')->orderBy('id','DESC')->get();
        return view('admin/plan_orders')->with('data',$data);
    }

    public function Faq(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['faq'] = DB::table('faq')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            $answer = $_POST['answer'];
            DB::update("UPDATE faq SET answer = '".$answer."', status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> FAQ successfully updated !</div>";
        }
        $data['header'] = $this->header('FAQ(s) Master','faq');
        $data['footer'] = $this->footer();
        $data['faqs'] = DB::table('faq')->orderBy('id','DESC')->get();
        $data['categories'] = DB::table('category')->orderBy('name','ASC')->get();
        return view('admin/faq')->with('data',$data);
    }

    public function layoutFaq(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['faq'] = DB::table('layout_faq')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            $answer = $_POST['answer'];
            DB::update("UPDATE layout_faq SET answer = '".$answer."', status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> FAQ successfully updated !</div>";
        }
        $data['header'] = $this->header('FAQ(s) Master','faq');
        $data['footer'] = $this->footer();
        $data['faqs'] = DB::table('layout_faq')->orderBy('id','DESC')->get();
        return view('admin/layout_faq')->with('data',$data);
    }

    public function productFaq(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['faq'] = DB::table('product_faq')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            $answer = $_POST['answer'];
            $section = $_POST['section'];
            DB::update("UPDATE product_faq SET answer = '".$answer."', status = '".$status."', section = '".$section."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> FAQ successfully updated !</div>";
        }
        $data['header'] = $this->header('FAQ(s) Master','faq');
        $data['footer'] = $this->footer();
        $data['faqs'] = DB::table('product_faq')->orderBy('id','DESC')->get();
        $data['categories'] = DB::table('category')->orderBy('name','ASC')->get();
        return view('admin/product_faq')->with('data',$data);
    }

    public function questions(){
        $notices = '';
        if(isset($_POST['add'])){
            $question = $_POST['question'];
            $options = json_encode($_POST['option']);
            $weightage = $_POST['weightage'];
            $type = $_POST['type'];
            DB::insert("INSERT INTO questions (question,options,weightage,type) VALUE ('$question','$options','$weightage','$type')");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $question = $_POST['question'];
            $options = json_encode($_POST['option']);
            $weightage = $_POST['weightage'];
            $type = $_POST['type'];
            DB::update("UPDATE questions SET question = '$question',options = '$options',weightage = '$weightage',type = '$type' WHERE id = '".$_GET['edit']."'");
            $notices .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Question has been deleted successfully !</div>";
        }
        $header = $this->header('Question Master','questions');
        $services = DB::table('services')->orderBy('id','DESC')->get();
        $que = DB::table('questions')->orderBy('name','ASC')->get();
        if(isset($_GET['edit'])) {
            $ques = DB::table('questions')->where('id','=',$_GET['edit'])->first();
        }
        $footer = $this->footer();
        return view('admin/questions')->with(compact('header','notices','services','ques','que','question','option_1','option_2','option_3','option_4','weightage','type','footer'));
    }

    public function Link(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['link'] = $_POST['link'];
            $datas['page'] = $_POST['page'];
            $datas['content'] = $_POST['content'];
            $post = DB::table('link')->insertGetId($datas);
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE link SET image = '".$bfile."' WHERE id = '".$post."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Link has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $link = $_POST['link'];
            $page = $_POST['page'];
            $content = $_POST['content'];
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE link SET image = '".$bfile."' WHERE id = '".$_GET['edit']."'");
            }
            DB::update("UPDATE link SET link = '$link', page = '$page', content = '$content' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("link")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Link Master','link');
        $data['links'] = DB::table('link')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['link'] = DB::table('link')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['categories'] = DB::table('category')->where('parent',0)->orderBy('id','DESC')->get();
        return view('admin/link')->with('data',$data);
    }

    public function BestForCat(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['link'] = $_POST['link'];
            $datas['category'] = $_POST['category'];
            $datas['content'] = $_POST['content'];
            $post = DB::table('best_for_category')->insertGetId($datas);
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_category SET image = '".$bfile."' WHERE id = '".$post."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Best For Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $link = $_POST['link'];
            $category = $_POST['category'];
            $content = $_POST['content'];
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_category SET image = '".$bfile."' WHERE id = '".$_GET['edit']."'");
            }
            DB::update("UPDATE best_for_category SET link = '$link', category = '$category', content = '$content' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("best_for_category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Best For Category Master','best-for-cat');
        $data['bestforcats'] = DB::table('best_for_category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['bestforcat'] = DB::table('best_for_category')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['categories'] = DB::table('category')->where('parent',0)->orderBy('id','DESC')->get();
        return view('admin/best-for-cat')->with('data',$data);
    }

    public function catData(){
        $html = '';
        $page = $_POST['page'];
        if($page == 'Products' || $page == 'Single Product' || $page == 'Brand'){
            $categories = DB::table('category')->where('parent',0)->orderBy('id','DESC')->get();
            $html = '
                <label class="control-label">Product category</label>
                <select name="category" class="form-control">';
            foreach ($categories as $category){
                $html .= '<option value="'.$category->id.'">'.$category->name.'</option>';
                $childs = DB::table("category")->where('parent', '=', $category->id)->orderBy('id','DESC')->get();
                foreach ($childs as $child){
                    $html .= '<option value="'.$child->id.'">- '.$child->name.'</option>';
                    $subchilds = DB::table("category")->where('parent', '=', $child->id)->orderBy('id','DESC')->get();
                    foreach ($subchilds as $subchild){
                        $html .= '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                    }
                }
            }
            $html .= '</select>';
            return $html;
        }
        else {
            return $html;
        }
    }

    public function pages(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            DB::insert("INSERT INTO pages (title,content,path) VALUE ('".escape($_POST['title'])."','".escape($_POST['content'])."','".$_POST['path']."')");
            $data['notices'] .= "<div class='alert mini alert-success'> Page has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            DB::update("UPDATE pages SET title = '".escape($_POST['title'])."',content = '".escape($_POST['content'])."',path = '".$_POST['path']."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("pages")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Page has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Pages','pages');
        $data['pages'] = DB::table('pages')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['page'] = DB::table('pages')->where('id','=',$_GET['edit'])->first();
        }
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        return view('admin/pages')->with('data',$data);
    }

    public function blog(Request $request){
        $data['notices'] = '';
        $image = '';
        $bimage = '';
        $pro_cat = '';
        if(isset($_POST['add'])){

            $blog = new Blog();
            $blog->title = $_POST['title'];
            $blog->slug = makeSlugOf($_POST['title'], 'design_category');
            $blog->content = $_POST['content'];
            $blog->short_des = str_limit(escape($_POST['content']),150);
            $blog->category = $_POST['category'];
            $blog->section = $_POST['section'];
            $blog->date = date('Y-m-d');
            if(isset($_POST['pro_cat'])){
                $blog->pro_cat = implode(',', $_POST['pro_cat']);
            }
            $blog->time = time();
            $blog->images = '';
            $blog->save();
            $post = $blog->id;
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                DB::update("UPDATE blog SET images = '".$image."' WHERE id = '".$post."'");
                $blogimage = Blog::find($post);
                $blogimage->images = $image;
                $blogimage->save();
            }
            if (request()->file('banner_image')) {
                $bfile = request()->file('banner_image');
                $bname = $bfile->getClientOriginalName();
                if (in_array($bfile->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $bimage = md5(time()).'.'.$bfile->getClientOriginalExtension();
                    $bpath = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $bfile->move($bpath,$bimage);
                    $img = Image::make($bpath.$bimage)->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($bpath.$bimage);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $bname is not a valid format</div>";
                }
                $bannerimage = Blog::find($post);
                $bannerimage->banner_image = $bimage;
                $bannerimage->save();

            }
            $data['notices'] .= "<div class='alert alert-success'> Post has been published successfully !</div>";
        }
        if(isset($_POST['edit'])){
            if(isset($_POST['pro_cat'])){
                $pro_cat = implode(',', $_POST['pro_cat']);
            }
            $updateblog = Blog::find($_GET['edit']);
            $updateblog->title = escape($_POST['title']);
            $updateblog->content = escape($_POST['content']);
            $updateblog->short_des = str_limit(escape($_POST['content']),150);
            $updateblog->category = $_POST['category'];
            $updateblog->pro_cat = $pro_cat;
            $updateblog->section = $_POST['section'];
            $updateblog->save();
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                $updateimage = Blog::find($_GET['edit']);
                $updateimage->images = $image;
                $updateimage->save();
            }
            if (request()->file('banner_image')) {
                $bfile = request()->file('banner_image');
                $bname = $bfile->getClientOriginalName();
                if (in_array($bfile->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $bimage = md5(time()).'.'.$bfile->getClientOriginalExtension();
                    $bpath = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $bfile->move($bpath,$bimage);
                    $img = Image::make($bpath.$bimage)->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($bpath.$bimage);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $bname is not a valid format</div>";
                }
                $updatebanner = Blog::find($_GET['edit']);
                $updatebanner->banner_image = $bimage;
                $updatebanner->save();
            }
            $data['notices'] .= '<div class="alert mini alert-success">Post has been updated successfully !</div>';
        }
        if(isset($_GET['delete']))
        {
            $blogdelete = Blog::find($_GET['delete']);
            $blogdelete->delete();
            $data['notices'] .= "<div class='alert alert-success'> Post has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Blog','blog');
        $data['posts'] = Blog::orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['post'] = Blog::where('id','=',$_GET['edit'])->first();
        }
        $data['categories'] = AdviceCategory::all();
        $data['procat'] = Category::where('parent',0)->orderBy('id','DESC')->get();
        $data['footer'] = $this->footer();
        return view('admin/blog')->with('data',$data);
    }

    public function customers(){
        $notices = '';
        if(isset($_GET['delete']))
        {
            DB::table("customers")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Customer deleted successfully !</div>";
        }

        $header = $this->header('Customers','customers');
        $customers = DB::table('customers')->get();
        $footer = $this->footer();
        return view('admin/customers')->with(compact('header','notices','customers','footer'));
    }

    public function coupons(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $code = $_POST['code'];
            $discount = (int)$_POST['discount'];
            $type = $_POST['type'];
            DB::insert("INSERT INTO coupons (code,discount,type) VALUE ('$code','$discount','$type')");
            $data['notices'] .= "<div class='alert mini alert-success'> Coupon has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $code = $_POST['code'];
            $discount = (int)$_POST['discount'];
            $type = $_POST['type'];
            DB::update("UPDATE coupons SET code = '$code',discount = '$discount',type = '$type' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Coupon edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("coupons")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Coupon has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Coupons','coupons');
        $data['coupons'] = DB::table('coupons')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['coupon'] = DB::table('coupons')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/coupons')->with('data',$data);
    }

    public function shipping(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $country = $_POST['country'];
            $name = $_POST['name'];
            $postcodes = $_POST['postcodes'];
            $category = implode(',', $_POST['category']);
            $cost = $_POST['cost'];
            DB::insert("INSERT INTO shipping (name,category,postcodes,country,cost) VALUE ('$name','$category','$postcodes','$country','$cost')");
            $data['notices'] .= "<div class='alert mini alert-success'> Shipping cost has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $country = $_POST['country'];
            $name = $_POST['name'];
            $postcodes = $_POST['postcodes'];
            $category = implode(',', $_POST['category']);
            $cost = $_POST['cost'];
            DB::update("UPDATE shipping SET name = '$name', category='$category', postcodes = '$postcodes', country = '$country',cost = '$cost' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Shipping cost edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("shipping")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Shipping cost has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Shipping cost','shipping');
        $data['costs'] = DB::table('shipping')->orderBy('id','DESC')->get();
        $data['countries'] = DB::table('country')->orderBy('nicename','ASC')->get();
        $data['postcodes'] = DB::table('pincode')->orderBy('pincode','ASC')->get();
        $data['category'] = DB::table('category')->get();
        if(isset($_GET['edit'])) {
            $data['cost'] = DB::table('shipping')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/shipping')->with('data',$data);
    }

    public function flat(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $country = $_POST['country'];
            $name = $_POST['name'];
            $postcodes = $_POST['postcodes'];
            $cost = $_POST['cost'];
            DB::insert("INSERT INTO shipping (name,postcodes,country,cost) VALUE ('$name','$postcodes','$country','$cost')");
            $data['notices'] .= "<div class='alert mini alert-success'> Shipping cost has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $country = $_POST['country'];
            $name = $_POST['name'];
            $postcodes = $_POST['postcodes'];
            $cost = $_POST['cost'];
            DB::update("UPDATE shipping SET name = '$name', postcodes = '$postcodes', country = '$country',cost = '$cost' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Shipping cost edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("shipping")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Shipping cost has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Shipping cost','shipping');
        $data['costs'] = DB::table('shipping')->orderBy('id','DESC')->get();
        $data['countries'] = DB::table('country')->orderBy('nicename','ASC')->get();
        $data['postcodes'] = DB::table('pincode')->orderBy('pincode','ASC')->get();
        if(isset($_GET['edit'])) {
            $data['cost'] = DB::table('shipping')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/flat')->with('data',$data);
    }

    public function postcodes(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $postcode = $_POST['postcode'];
            $city = $_POST['city'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $delivery = $_POST['delivery'];
            $info = $_POST['info'];
            $pin = DB::table('pincode')->where('id','=',$_GET['postcode'])->get();
            if(count($pin)>0){
                $data['notices'] .= "<div class='alert mini alert-danger'> Postcode already exists !</div>";
            }else{
                DB::insert("INSERT INTO pincode (pincode, city, district, state, delivery, info) VALUE ('$postcode', '$city', '$district', '$state', '$delivery', '$info')");
                $data['notices'] .= "<div class='alert mini alert-success'> Postcode has been successfully added !</div>";
            }
        }
        if(isset($_POST['edit'])){
            $postcode = $_POST['postcode'];
            $city = $_POST['city'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $delivery = $_POST['delivery'];
            $info = $_POST['info'];
            DB::update("UPDATE pincode SET pincode = '$postcode',city = '$city',district = '$district',state = '$state',delivery = '$delivery',info = '$info' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Postcode edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("pincode")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Postcode has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Postcodes','postcodes');
        $data['postcodes'] = DB::table('pincode')->orderBy('pincode','ASC')->get();
        if(isset($_GET['edit'])) {
            $data['postcode'] = DB::table('pincode')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/postcodes')->with('data',$data);
    }

    public function units(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $symbol = $_POST['symbol'];
            $unit = $_POST['unit'];
            $default = $_POST['default'];
            if($default){
                DB::update("UPDATE units SET def=0");
            }
            DB::insert("INSERT INTO units (name, symbol, unit, def) VALUE ('$name', '$symbol', '$unit', $default)");
            $data['notices'] .= "<div class='alert mini alert-success'> Weight Unit has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST['name'];
            $symbol = $_POST['symbol'];
            $unit = $_POST['unit'];
            $default = $_POST['default'];
            if($default){
                DB::update("UPDATE units SET def=0");
            }
            DB::update("UPDATE units SET name = '$name',symbol = '$symbol',unit = '$unit',def = '$default' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Weight Unit edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("units")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Weight Unit has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Weight Units','units');
        $data['units'] = DB::table('units')->orderBy('name','ASC')->get();
        if(isset($_GET['edit'])) {
            $data['unit'] = DB::table('units')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/units')->with('data',$data);
    }

    public function reviews(){
        $notices = '';
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE reviews SET active = '1' WHERE id = ".$_GET['approve']);
            $notices = "<div class='alert alert-success'> Review has been approved !</div>";
        }
        $header = $this->header('Admin','reviews');
        $reviews = DB::table('reviews')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/reviews')->with(compact('header','notices','reviews','footer'));
    }

    public function photoreviews(){
        $notices = '';
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE photo_reviews SET active = '1' WHERE id = ".$_GET['approve']);
            $notices = "<div class='alert alert-success'> Review has been approved !</div>";
        }
        $header = $this->header('Admin','Photo reviews');
        $reviews = DB::table('photo_reviews')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/photo-reviews')->with(compact('header','notices','reviews','footer'));
    }

    public function image_likes(){
        $notices = '';
        $header = $this->header('Admin','reviews');
        $reviews = DB::table('image_like')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/image_likes')->with(compact('header','notices','reviews','footer'));
    }

    public function image_inspirations(){
        $notices = '';
        $header = $this->header('Admin','reviews');
        $reviews = DB::table('image_inspiration')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/image_inspirations')->with(compact('header','notices','reviews','footer'));
    }

    public function layout_reviews(){
        $notices = '';
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE layout_reviews SET active = '1' WHERE id = ".$_GET['approve']);
            $notices = "<div class='alert alert-success'> Review has been approved !</div>";
        }
        $header = $this->header('Admin','layout reviews');
        $reviews = DB::table('layout_reviews')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/layout_reviews')->with(compact('header','notices','reviews','footer'));
    }

    public function advices_reviews(){
        $notices = '';
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE advices_reviews SET active = '1' WHERE id = ".$_GET['approve']);
            $notices = "<div class='alert alert-success'> Review has been approved !</div>";
        }
        $header = $this->header('Admin','Advices reviews');
        $reviews = DB::table('advices_reviews')->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/advices_reviews')->with(compact('header','notices','reviews','footer'));
    }

    public function advicesFaq(){
        $data['notices'] = '';
        if(isset($_GET['edit'])){
            $data['faq'] = DB::table('advices_faq')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_POST['edit'])){
            $status = $_POST['status'];
            $answer = $_POST['answer'];
            DB::update("UPDATE advices_faq SET answer = '".$answer."', status = '".$status."' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> FAQ successfully updated !</div>";
        }
        $data['header'] = $this->header('FAQ(s) Master','faq');
        $data['footer'] = $this->footer();
        $data['faqs'] = DB::table('advices_faq')->orderBy('id','DESC')->get();
        return view('admin/advices_faq')->with('data',$data);
    }

    public function advices_category(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $advice         =   new AdviceCategory();
            $advice->name   =   $_POST['name'];
            $advice->parent = $_POST['parent'];
            $advice->slug = makeSlugOf($_POST['name'], 'advices_category');;
            $advice->short_description = $_POST['short_description'];
            $datas['banner_image'] = '';
            $advice->save();
            $catdata = $advice->id;
            if (request()->file('banner_image')) {
                $file = request()->file('banner_image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                $banner_image   =   AdviceCategory::find($catdata);
                $banner_image->banner_image = $image;
                $banner_image->save();
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $updatecategory     =   AdviceCategory::find($_GET['edit']);
            $updatecategory->name = escape($_POST['name']);
            $updatecategory->parent = $_POST['parent'];
            $updatecategory->short_description = $_POST['short_description'];
            $updatecategory->slug = makeSlugOf(escape($_POST['name']), 'advices_category');
            $updatecategory->save();
            if (request()->file('banner_image')) {
                $file = request()->file('banner_image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'blog/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                $updatebanner    =   AdviceCategory::find($_GET['edit']);
                $updatebanner->banner_image =   $image;
                $updatebanner->save();
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Category edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("advices_category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Pages','pages');
        $data['categories'] = DB::table('advices_category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['category'] = DB::table('advices_category')->where('id','=',$_GET['edit'])->first();
        }
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['parents'] = DB::table('advices_category')->where('parent','=',0)->orderBy('id','DESC')->get();
        $data['footer'] = $this->footer();
        return view('admin/advices_category')->with('data',$data);
    }

    public function orders(){
        $data['notices'] = '';
        if(isset($_GET['delete']))
        {
            DB::table("orders")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Order has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Orders','orders');
        $data['fields'] = DB::table('fields')->select('name','code')->orderBy('id','ASC')->get();
        $data['orders'] = DB::table('orders')->orderBy('id','DESC')->get();
        if(isset($_GET['details'])) {
            if(isset($_POST['save'])){
                DB::update("Update orders SET stat = ".$_POST['stat']." WHERE id = '".$_GET['details']."' ");
                $data['notices'] .= "<div class='alert alert-success'> Order status has been changed ! </div>";
            }
            $data['order'] = DB::table('orders')->where('id','=',$_GET['details'])->limit(1)->first();
        }
        $data['foot'] = $this->footer();
        return view('admin/orders')->with('data',$data);
    }

    public function stats(){
        if (isset($_GET['year'])){
            $data['term'] = 'year';
            for ($iDay = 365; $iDay >= 0; $iDay--) {
                $d[366 - $iDay] = "'".date('Y-m-d', strtotime("-" . $iDay . " day"))."'";
                $i[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $o[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $s[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $c[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
            }
        } elseif (isset($_GET['month'])){
            $data['term'] = 'month';
            for ($iDay = 30; $iDay >= 0; $iDay--) {
                $d[31 - $iDay] = "'".date('Y-m-d', strtotime("-" . $iDay . " day"))."'";
                $i[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $o[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $s[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $c[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
            }
        } else {
            $data['term'] = 'week';
            for ($iDay = 6; $iDay >= 0; $iDay--) {
                $d[7 - $iDay] = "'".date('Y-m-d', strtotime("-" . $iDay . " day"))."'";
                $i[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $o[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $s[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
                $c[date('Y-m-d', strtotime("-" . $iDay . " day"))] = 0;
            }
        }
        $fs = DB::table('visitors')->where('date','=',$d[1])->orderBy('date','ASC')->get();
        $visit = array();
        foreach ($fs as $visits){
            $i[$visits->date] = $visits->visits;
        }
        $yesterday = date('Y-m-d', strtotime(date('Y-m-d') .' -1 day'));
        $yvisits = (!in_array($yesterday,$i)) ? $i[$yesterday] : 0;
        $tvisits = (!in_array(date('Y-m-d'),$i)) ? $i[date('Y-m-d')] : 0;
        $order_query = DB::table('orders')->where('date','=',$d[1])->orderBy('date','ASC')->get();
        foreach ($order_query as $order){
            $o[$order->date] = $o[$order->date]+1;
            $s[$order->date] = $s[$order->date]+$order->summ;
            $c[$order->date] = $o[$order->date] / $i[$order->date]*100;
        }
        if ($yorders = DB::select("SELECT COUNT(*) as count FROM orders WHERE date ='".$yesterday."'")[0]->count > 0){
            $ysales = DB::select("SELECT SUM(summ) as sum FROM orders WHERE date ='".$yesterday."'")[0]->sum;
        } else {
            $ysales = 0;
        }
        $torders = DB::select("SELECT COUNT(*) as count FROM orders WHERE date ='".date('Y-m-d')."'")[0]->count;
        if ($torders > 0){
            $tsales = DB::select("SELECT SUM(summ) as sum FROM orders WHERE date ='".date('Y-m-d')."'")[0]->sum;
        } else {
            $tsales = 0;
        }
        $yconversion = $c[$yesterday];
        $tconversion = $c[date('Y-m-d')];
        $data['morders'] = max($o)+max($o)*2/6+1;
        $data['msales'] = max($s)+max($s)*2/6+1;
        $data['mconversion'] = max($c)+max($c)*2/6+1;
        $data['mvisits'] = max($i)+max($i)*2/6+1;
        $data['porders'] = p($yorders,$torders);
        $data['pvisits'] = p($yvisits,$tvisits);
        $data['psales'] = p($ysales,$tsales);
        $data['pconversion'] = p($yconversion,$tconversion);
        $data['orders'] = DB::select("SELECT COUNT(*) as count FROM orders")[0]->count;
        $data['ssales'] = DB::select("SELECT SUM(summ) as sum FROM orders ")[0]->sum;
        $data['chart'] = array();
        $data['chart']['days'] = implode(', ',$d);
        $data['i'] = implode(', ',$i);
        $data['o'] = implode(', ',$o);
        $data['s'] = implode(', ',$s);
        $data['c'] = implode(', ',$c);
        $data['header'] = $this->header('Statistics','stats');
        $data['cfg'] = $this->cfg;
        $data['footer'] = $this->footer();
        return view('admin/stats')->with('data',$data);
    }

    public function tracking(){
        $notices = '';
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $code = $_POST['code'];
            DB::insert("INSERT INTO tracking (name,code) VALUE ('$name','$code')");
            $notices .= "<div class='alert mini alert-success'> Tracking code has been successfully added !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("tracking")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> The tracking code has been deleted successfully !</div>";
        }
        $header = $this->header('Tracking','tracking');
        $codes = DB::table("tracking")->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/tracking')->with(compact('header','notices','codes','footer'));
    }

    public function newsletter(){
        $notices = '';
        if(isset($_POST['send'])){
            $emails['orders'] = array();
            $emails['support'] = array();
            $emails['newsletter'] = array();
            $orders = DB::table('orders')->select('email')->get();
            foreach ($orders as $order){
                $emails['orders'][$order->email] = $order->email;
            }
            $tickets = DB::table('tickets')->select('email')->get();
            foreach ($tickets as $ticket){
                $emails['support'][$ticket->email] = $ticket->email;
            }
            $subscribers = DB::table('subscribers')->select('email')->get();
            foreach ($subscribers as $subscriber){
                $emails['newsletter'][$subscriber->email] = $subscriber->email;
            }
            if ($_POST['group'] == 'orders') {
                $tos = $emails['orders'];
            } elseif ($_POST['group'] == 'newsletter') {
                $tos = $emails['newsletter'];
            } elseif ($_POST['group'] == 'support') {
                $tos = $emails['support'];
            } else {
                $tos = array_merge($emails['support'],$emails['newsletter'],$emails['orders']);
            }
            foreach ($tos as $to){
                mailing('newsletter',array('title'=>escape($_POST['title']),'email'=>$to,'content'=>nl2br(escape($_POST['content']))),escape($_POST['title']),$to);
            }
            $notices = "<div class='alert mini alert-success'> Newsletter has been successfully sent !</div>";
        }
        $header = $this->header('Newsletter','newsletter');
        $footer = $this->footer();
        return view('admin/newsletter')->with(compact('header','notices','footer'));
    }

    public function referrers(){
        $header = $this->header('Referrers','referrers');
        $referrers = DB::table('referrer')->orderBy('visits','DESC')->get();
        $footer = $this->footer();
        return view('admin/referrers')->with(compact('header','referrers','footer'));
    }

    public function os(){
        $header = $this->header('Operating systems','os');
        $OSs = DB::table('os')->orderBy('visits','DESC')->get();
        $footer = $this->footer();
        return view('admin/os')->with(compact('header','OSs','footer'));
    }

    public function browsers(){
        $header = $this->header('Browsers','browsers');
        $browsers = DB::table('browsers')->orderBy('visits','DESC')->get();
        $footer = $this->footer();
        return view('admin/browsers')->with(compact('header','browsers','footer'));
    }

    public function payment(){
        $data['notices'] = '';
        if(isset($_POST['edit'])){
            $method = DB::table('payments')->where('id','=',$_GET['edit'])->first();
            $method_options = json_decode($method->options,true);
            $options = array();
            foreach ($method_options as $key => $value){
                $options[$key] = $_POST[$key];
            }
            $data_options = json_encode($options);
            DB::update("UPDATE payments SET title = '".escape($_POST['title'])."',options = '".$data_options."',active = '".$_POST['active']."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> Payment method edited successfully !</div>";
        }
        $data['header'] = $this->header('Payment methods','payment');
        $data['methods'] = DB::table('payments')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['method'] = DB::table('payments')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/payment')->with('data',$data);
    }

    public function currency(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = escape($_POST['name']);
            $code = escape($_POST['code']);
            $rate = $_POST['rate'];
            DB::insert("INSERT INTO currency (name,code,rate) VALUE ('$name','$code','$rate')");
            $data['notices'] .= "<div class='alert mini alert-success'> Currency has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = escape($_POST['name']);
            $code = escape($_POST['code']);
            $rate = $_POST['rate'];
            DB::update("UPDATE currency SET name = '$name',code = '$code',rate = '$rate' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Currency edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("currency")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Currency has been deleted successfully !</div>";
        }
        if(isset($_GET['default']))
        {
            DB::update("UPDATE currency SET `default` = '0' WHERE 1");
            DB::update("UPDATE currency SET `default` = '1' WHERE id = '".$_GET['default']."'");
            $data['notices'] .= "<div class='alert alert-success'> Currency has been set as default !</div>";
        }
        $data['header'] = $this->header('Currency','currency');
        $data['currencies'] = DB::table('currency')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['currency'] = DB::table('currency')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/currency')->with('data',$data);
    }

    public function settings(){
        $notices = '';
        if(isset($_POST['save'])){
            unset($_POST['save'],$_POST['_token']);
            DB::table('config')->update($_POST);
            $notices .= '<div class="alert alert-success mini">Settings updated successfully !</div>';
        }
        $header = $this->header('Settings','settings');
        $languages = DB::table('langs')->orderBy('id','DESC')->get();
        $cfg = $this->cfg;
        $footer = $this->footer();
        return view('admin/settings')->with(compact('header','notices','languages','cfg','footer'));
    }

    public function theme(){
        $notices = '';
        if(isset($_POST['save'])){
            unset($_POST['save'],$_POST['_token']);
            $_POST['background'] = $_POST['color1'].','.$_POST['color2'];
            $_POST['button'] = $_POST['button_text'].','.$_POST['button_link'];
            unset($_POST['color1'],$_POST['color2'],$_POST['button_text'],$_POST['button_link']);
            DB::table('style')->update($_POST);
            $notices .= '<div class="alert alert-success mini">Settings updated successfully !</div>';
        }
        $header = $this->header('Theme settings','theme');
        $style = $this->style;
        $footer = $this->footer();
        return view('admin/theme')->with(compact('header','notices','style','footer'));
    }

    public function lang(){
        $data['notices'] = '';
        if (isset($_GET['lang'])){
            $data['l'] = $_GET['lang'];
        } else {
            $data['l'] = $this->cfg->lang;
        }
        if(isset($_GET['save'])) {
            DB::update("UPDATE translate SET translation = '".$_POST['translation']."' WHERE id = ".$_GET['save']);
            return "success";
        }
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $code = $_POST['code'];
            DB::insert("INSERT INTO langs (name,code) VALUE ('$name','$code')");
            $data['notices'] .= "<div class='alert mini alert-success'> Language has been added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST["name"];
            $code = $_POST["code"];
            DB::update("UPDATE langs SET name = '$name',code = '$code' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Language has been updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("translate")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Translation deleted successfully !</div>";
        }
        if(isset($_GET['delete_language'])) {
            DB::table("langs")->where('id', '=', $_GET['delete_language'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Language deleted successfully !</div>";
        }
        if(isset($_GET['edit'])){
            $data['lang'] = DB::table('langs')->where('id','=',$_GET['edit'])->first();
        }
        $data['header'] = $this->header('Language','lang');
        $data['langs'] = DB::table('langs')->get();
        $data['translations'] = DB::table('translate')->where('lang','=',$data['l'])->orderBy('id','DESC')->get();
        $data['footer'] = $this->footer();
        return view('admin/lang')->with('data',$data);
    }

    public function tokens(){
        $notices = '';
        if(isset($_GET['add'])){
            DB::insert("INSERT INTO tokens (token,requests) VALUE ('".md5(time())."',0)");
            $notices .= "<div class='alert mini alert-success'> An Api token has been generated !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("tokens")->where('token', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Token deleted successfully !</div>";
        }
        $header = $this->header('API Tokens','tokens');
        $langs =  DB::table('tokens')->get();
        $tokens =  DB::table('tokens')->get();
        $footer = $this->footer();
        return view('admin/tokens')->with(compact('header','notices','tokens','footer'));
    }

    public function export(){
        $notices = '';
        if(isset($_GET['database']))
        {
            exec('mysqldump -u '.env('DB_USERNAME').' -p '.env('DB_PASSWORD').' -h '.env('DB_HOST', '127.0.0.1').' '.env('DB_DATABASE').' > '.base_path('file.sql'));
            header( "Pragma: public" );
            header( "Expires: 0" );
            header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
            header( "Cache-Control: public" );
            header( "Content-Description: File Transfer" );
            header( "Content-type: application/zip" );
            header( "Content-Disposition: attachment; filename=\"file.sql\"" );
            header( "Content-Transfer-Encoding: binary" );
            readfile( base_path('file.sql') );
            unlink( base_path('file.sql') );
            return;
        }
        elseif (isset($_GET['files'])) {
            $rootPath = base_path();
            $backup = md5(time()).'.zip';
            $zip = new ZipArchive();
            $zip->open($backup, ZipArchive::CREATE | ZipArchive::OVERWRITE);
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath),RecursiveIteratorIterator::LEAVES_ONLY);
            foreach ($files as $name => $file)
            {
                if (!$file->isDir())
                {
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);
                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();
            header( "Pragma: public" );
            header( "Expires: 0" );
            header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
            header( "Cache-Control: public" );
            header( "Content-Description: File Transfer" );
            header( "Content-type: application/zip" );
            header( "Content-Disposition: attachment; filename=\"" . $backup . "\"" );
            header( "Content-Transfer-Encoding: binary" );
            header( "Content-Length: " . filesize( $backup ) );
            ob_get_clean();
            readfile( $backup );
            ob_get_clean();
            unlink($backup);
            return;
        }
        $header = $this->header('Export','export');
        $footer = $this->footer();
        return view('admin/export')->with(compact('header','footer'));
    }

    public function slider(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['title'] = $_POST['title'];
            $datas['link'] = $_POST['link'];
            $datas['image'] = '';
            $slide = DB::table('slider')->insertGetId($datas);
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'slider/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                DB::update("UPDATE slider SET image = '".$image."' WHERE id = '".$slide."'");
            }
            $data['notices'] .= "<div class='alert alert-success'> The slide has been published successfully !</div>";
        }
        if(isset($_POST['edit'])){
            DB::update("UPDATE slider SET title = '".escape($_POST['title'])."',link = '".escape($_POST['link'])."' WHERE id = ".$_GET['edit']);
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'slider/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                DB::update("UPDATE slider SET image = '".$image."' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= '<div class="alert mini alert-success">The slide has been updated successfully !</div>';
        }
        if(isset($_GET['delete']))
        {
            DB::table("slider")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> The slide has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Slider','slider');
        $data['slides'] = DB::table('slider')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['slide'] = DB::table('slider')->orderBy('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/slider')->with('data',$data);
    }
    /*Abhijeet code start*/

    public function Registerimage(){
        $data['notices'] = '';
        if(isset($_POST['edit'])){
            if (request()->file('image')) {
                $file = request()->file('image');
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $image = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'register/';
                    $file->move($path,$image);
                    $img = Image::make($path.$image)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$image);
                } else {
                    $data['notices'] .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
                DB::update("UPDATE register_image SET image = '".$image."' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= '<div class="alert mini alert-success">Image has been updated successfully !</div>';
        }
        if(isset($_GET['delete']))
        {
            DB::update("UPDATE register_image SET image = '' WHERE id = '".$_GET['delete']."'");
            $data['notices'] .= "<div class='alert alert-success'>Image has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Slider','slider');
        $data['register'] = DB::table('register_image')->get();
        if(isset($_GET['edit'])) {
            $data['register'] = DB::table('register_image')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/registerimage')->with('data',$data);
    }

    public function Tmtcalculator(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $data['brand_id'] = $_POST['brand_id'];
            $data['brand_name'] = $_POST['brand_name'];
            $data['variant_name'] = $_POST['variant_name'];
            $data['kg'] = $_POST['kg'];
            $data['rod'] = $_POST['rod'];
            $data['bundle'] = $_POST['bundle'];
            $tmt = DB::table('tmt_calculator')->insertGetId($data);
            $data['notices'] .= "<div class='alert alert-success'> TMT conversion created successfully !</div>";
        }
        if(isset($_POST['edit'])){

            $variant_name  = $_POST['variant_name'];
            $kg = $_POST['kg'];
            $rod = $_POST['rod'];
            $bundle = $_POST['bundle'];

            DB::update("UPDATE tmt_calculator SET variant_name = '$variant_name', kg = '$kg' , rod = '$rod' , bundle = '$bundle' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= '<div class="alert mini alert-success">Conversion has been updated successfully !</div>';
        }
        if(isset($_GET['delete']))
        {
            DB::table("tmt_calculator")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Conversion has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('TMT Calculator','tmtcalculator');
        $data['tmtbars'] = DB::table('tmt_calculator')->get();
        $data['brand'] = DB::table('brand')->get();
        if(isset($_GET['edit'])) {
            $data['tmtconversion'] = DB::table('tmt_calculator')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/tmtcalculator')->with('data',$data);
    }
    /*Abhijeet code end*/
    public function editor(){
        $notices = '';
        $cfg = $this->cfg;
        if (isset($_GET['file'])){
            $file = resource_path('views/'.$_GET['file']);
            if (!file_exists($file)){
                $file = resource_path('views/index.php');
                echo '<div class="alert  alert-warning"> File not found , edititng home.php </div>';
            }
        } else {
            $file = resource_path('views/index.php');
        }
        if (isset($_POST['text']))
        {
            file_put_contents($file, escape($_POST['text']));
            $notices = '<div class="alert  alert-success"> File has been saved </div>';
        }
        $text = file_get_contents($file);
        $header = $this->header('Editor','editor');
        $files = glob(resource_path('views/*.php'), GLOB_BRACE);
        $footer = $this->footer();
        return view('admin/editor')->with(compact('header','notices','cfg','files','text','footer'));
    }

    public function templates(){
        $data['notices'] = '';
        if(isset($_POST['edit'])){
            DB::update("UPDATE templates SET title = '".escape($_POST['title'])."',template = '".escape($_POST['template'])."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> Template edited successfully !</div>";
        }
        if(isset($_GET['edit']))
        {
            $data['template'] = DB::table("templates")->where('id', '=', $_GET['edit'])->first();
        }
        $data['header'] = $this->header('Templates','templates');
        $data['templates'] = DB::table('templates')->get();
        $data['footer'] = $this->footer();
        return view('admin/templates')->with('data',$data);
    }

    public function builder(){
        $data['notices'] = '';
        if (isset($_GET['page'])) {
            $area = 'page';
        } elseif (isset($_GET['post'])) {
            $area = 'post';
        } else {
            $area = 'home';
        }
        if(isset($_GET['save'])){
            $datas = $_POST['data'];
            parse_str($datas,$str);
            $builder = $str['item'];
            foreach($builder as $key => $value){
                $key=$key+1;
                DB::update("UPDATE blocs SET o=$key where id=$value");
            }
            return "Succesfully updated";
        }
        if(isset($_POST['add'])){
            $area = $_POST['area'];
            $content = escape($_POST['content']);
            $title = escape($_POST['title']);
            DB::insert("INSERT INTO blocs (area,content,title) VALUE ('$area','$content','$title')");
            $data['notices'] .= "<div class='alert mini alert-success'> bloc has been added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST["title"]);
            $content = escape($_POST["content"]);
            DB::update("UPDATE blocs SET title = '$title',content = '".$content."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> bloc has been updated successfully !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['bloc'] = DB::select("SELECT * FROM blocs WHERE id = ".$_GET['edit'])[0];
        }
        if(isset($_GET['delete']))
        {
            DB::table("blocs")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> The bloc has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Page builder','builder');
        $data['blocs'] = DB::table('blocs')->where('area','=',$area)->orderBy('o','ASC');
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        return view('admin/builder')->with('data',$data);
    }

    public function menu(){
        $data['notices'] = '';
        if(isset($_GET['save'])){
            $data = $_POST['data'];
            parse_str($data,$str);
            $builder = $str['item'];
            foreach($builder as $key => $value){
                $key=$key+1;
                DB::update("UPDATE menu SET o=$key where id=$value");
            }
            return "Succesfully updated";
        }
        if(isset($_POST['add'])){
            $link = $_POST['link'];
            $title = escape($_POST['title']);
            $parent = $_POST['parent'];
            DB::insert("INSERT INTO menu (link,title,parent) VALUE ('$link','$title','$parent')");
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> menu item has been added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST["title"]);
            $link = $_POST["link"];
            $parent = $_POST["parent"];
            DB::update("UPDATE menu SET title = '$title',link = '$link',parent = '$parent' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> menu item has been updated successfully !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['item'] = DB::table('menu')->where('id','=',$_GET['edit'])->get();
        }
        if(isset($_GET['delete']))
        {
            DB::table("menu")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> The menu item has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Menu','menu');
        $data['items'] = DB::table('menu')->orderBy('o','ASC')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        $data['parents'] = DB::table('menu')->where('parent','=',0)->orderBy('id','DESC');

        return view('admin/menu')->with('data',$data);
    }

    public function bottom(){
        $data['notices'] = '';
        if(isset($_GET['save'])){
            $datas = $_POST['data'];
            parse_str($datas,$str);
            $builder = $str['item'];
            foreach($builder as $key => $value){
                $key=$key+1;
                DB::update("UPDATE footer SET o=$key where id=$value");
            }
            return "Succesfully updated";
        }
        if(isset($_POST['add'])){
            if (escape($_POST['title']) != "" && $_POST['link'] != ""){
                $link = $_POST['link'];
                $title = escape($_POST['title']);
                DB::insert("INSERT INTO footer (link,title) VALUE ('$link','$title')");
                $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> footer item has been added !</div>";
            }
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST["title"]);
            $link = $_POST["link"];
            DB::update("UPDATE footer SET title = '$title',link = '$link' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> footer item has been updated successfully !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['item'] = DB::table('footer')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_GET['delete']))
        {
            DB::table("footer")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> The footer item has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Footer menu','bottom');
        $data['items'] =  DB::table('footer')->orderBy('o','ASC')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        return view('admin/bottom')->with('data',$data);
    }

    public function fields(){
        $notices = '';
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $code = $_POST['code'];
            DB::statement(DB::raw("ALTER TABLE `orders` ADD `".$code."` VARCHAR(255) NOT NULL"));
            DB::insert("INSERT INTO fields (name,code) VALUE ('$name','$code')");
            $notices .= "<div class='alert mini alert-success'> field has been added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST["name"];
            $code = $_POST["code"];
            $field = DB::table('fields')->where('id','=',$_GET['edit'])->first();
            DB::statement(DB::raw("ALTER TABLE `orders` CHANGE `".$field->code."` `".$code."` VARCHAR(255) NOT NULL"));
            DB::update("UPDATE fields SET name = '$name',code = '$code' WHERE id = '".$_GET['edit']."'");
            $notices .= "<div class='alert mini alert-success'> Field has been updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            $field = DB::table('fields')->where('id','=',$_GET['delete'])->first();
            DB::statement(DB::raw("ALTER TABLE `orders` DROP `".$field->code."`"));
            DB::table("fields")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> The field has been deleted successfully !</div>";
        }
        if(isset($_GET['edit']))
        {
            $field = DB::table('fields')->where('id','=',$_GET['edit'])->first();
        }
        $header = $this->header('Extrafields','fields');
        $fields = DB::table('fields')->get();
        $footer = $this->footer();
        return view('admin/fields')->with(compact('header','notices','field','fields','footer'));
    }

    public function support(){
        $data['notices'] = '';
        if(isset($_POST['send'])){
            $ticket = DB::table('tickets')->where('id','=',$_GET['reply'])->first();
            mailing('reply',array('title'=>escape($_POST['title']),'email'=>$ticket->email,'reply'=>nl2br($_POST['reply'])),escape($_POST['title']),$ticket->email);
            $data['notices'] = "<div class='alert mini alert-success'> E-mail has been successfully sent !</div>";
        }
        if(isset($_GET['reply'])){
            $data['ticket'] = DB::table('tickets')->where('id','=',$_GET['reply'])->first();
        }
        $data['header'] = $this->header('Support','support');
        $data['tickets'] = DB::table('tickets')->get();
        $data['footer'] = $this->footer();
        return view('admin/support')->with('data',$data);
    }

    public function administrators(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $pass = md5($_POST['pass']);
            $secure = md5(time());
            DB::insert("INSERT INTO user (u_name,u_email,u_pass,secure) VALUE ('$name','$email','$pass','$secure')");
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> Admin has been added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST["name"];
            $email = $_POST['email'];
            $pass = md5($_POST['pass']);
            $admin = DB::select("SELECT * FROM user WHERE u_id = ".$_GET['edit']);
            DB::update("UPDATE user SET u_name = '$name',u_email = '$email',u_pass = '$pass' WHERE u_id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-dismissible alert-success'> Admin informations has been updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("user")->where('u_id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-dismissible alert-success'> The admin has been deleted successfully !</div>";
        }
        if(isset($_GET['edit'])){
            $data['admin'] = DB::table('user')->where('u_id','=',$_GET['edit'])->first();
        }
        $data['header'] = $this->header('Administrators','administrators');
        $ata['admins'] = DB::table('user')->get();
        $data['footer'] = $this->footer();
        return view('admin/administrators')->with('data',$data);
    }

    public function profile(){
        $notices = '';
        if(isset($_POST['update'])){
            $user = DB::table('user')->where('secure','=',session('admin'))->first();
            $user_name = $_POST['name'];
            $user_email = $_POST['email'];
            if ($_POST['pass'] != ""){
                $user_pass = md5($_POST['pass']);
            } else {
                $user_pass = $user->u_pass;
            }
            DB::update("UPDATE user SET u_name = '$user_name',u_email = '$user_email',u_pass = '$user_pass' WHERE secure = '".session('admin')."'");
            $notices = "<div class='alert alert-success mini'> Profile updated successfully </div>";
        }
        $header = $this->header('Profile','profile');
        $user = DB::table('user')->where('secure','=',session('admin'))->first();
        $footer = $this->footer();
        return view('admin/profile')->with(compact('header','notices','user','footer'));
    }

    public function design_category(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name1 = $_POST['name'];
            $slug = makeSlugOf($name1, 'design_category');
            $description = $_POST['description'];
            $path1 = $_POST['path'];
            $parent = $_POST['parent'];
            $layout = $_POST['layout'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'design/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                $thumb = Image::make($path.$file)->resize(165,110)->save($path.'thumbs/'.$file);
            }
            DB::insert("INSERT INTO design_category (name,slug,description,path,parent,layout,image) VALUE ('$name1','$slug','$description','$path1','$parent','$layout','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'>Design Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name1 = $_POST["name"];
            $description = $_POST['description'];
            $path1 = $_POST["path"];
            $parent = $_POST["parent"];
            $layout = $_POST['layout'];
            DB::update("UPDATE design_category SET name = '$name1',description='$description',path = '$path1',parent = '$parent', layout = '$layout' WHERE id = '" . $_GET['edit'] . "'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'design/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                $thumb = Image::make($path.$file)->resize(165,110)->save($path.'thumbs/'.$file);
                DB::update("UPDATE design_category SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("design_category")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Design Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Categories','design_category');
        $data['design_category'] = DB::table('design_category')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['design_category'] = DB::table('design_category')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['parents'] =DB::table('design_category')->where('parent',0)->orderBy('id','DESC')->get();
        return view('admin/design_category')->with('data',$data);
    }

    public function photos(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $title = $_POST['title'];
            $slug = makeSlugOf($title, 'photo_gallery');
            $description = $_POST['description'];
            $category = $_POST['category'];
            $featured = $_POST['featured'];
            $file = '';
            if (request()->file('banner_image')) {
                $name = request()->file('banner_image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('banner_image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'gallery/';
                request()->file('banner_image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                $thumb = Image::make($path.$file)->resize(165,110)->save($path.'thumbs/'.$file);
            }
            DB::insert("INSERT INTO photo_gallery (title,slug,image,description,category,featured) VALUE ('$title','$slug','$file','$description','$category','$featured')");
            $data['notices'] .= "<div class='alert mini alert-success'>Photo Gallery has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $category = $_POST['category'];
            $featured = $_POST['featured'];
            $file = '';
            if (request()->file('banner_image')) {
                $name = request()->file('banner_image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('banner_image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'gallery/';
                request()->file('banner_image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                $thumb = Image::make($path.$file)->resize(165,110)->save($path.'thumbs/'.$file);
            }
            DB::update("UPDATE photo_gallery SET title = '$title',image = '$file', description = '$description',category = '$category',featured = '$featured' WHERE id = '" . $_GET['edit'] . "'");
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("photo_gallery")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'>Photo Gallery has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Photo Gallery','photos');
        $data['photo'] = DB::table('photo_gallery')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['photo'] = DB::table('photo_gallery')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['categories'] = DB::table('design_category')->where('parent','=',0)->orderBy('id','DESC')->get();
        return view('admin/photo_gallery')->with('data',$data);
    }

    public function updatePhotos(){
        $image = '';
        $notices = '';
        $header = $this->header('Add Photos','add-photos-to-gallery');
        if(!isset($_GET['gallery'])){
            return redirect('/admin/photos');
        }elseif($_GET['gallery'] == ''){
            return redirect('/admin/photos');
        }
        $photo = DB::table('photo_gallery')->where('id', '=', $_GET['gallery'])->first();
        if(isset($_POST['update'])){
            if(!$photo){
                abort(404);
            }
            if(request()->file('image')){
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'gallery'.DIRECTORY_SEPARATOR.'photos/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                $thumb = Image::make($path.$file)->resize(165,110)->save($path.'thumbs/'.$file);
                DB::insert("INSERT INTO photos (gallery_id,image,priority) VALUE (".$photo->id.",'".$file."',".$_POST['priority'].")");
            }
            $notices .= "<div class='alert mini alert-success'> Photo has been successfully added !</div>";
        }
        if(isset($_GET['delete'])){
            DB::table("photos")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Photo has been deleted successfully !</div>";
            return redirect('admin/update-photos?gallery='.$_GET['gallery']);
        }
        $footer = $this->footer();
        $photo_gallery = DB::table('photos')->orderBy('id','ASC')->get();
        $photos = DB::table('photos')->where('gallery_id','=',$photo->id)->orderBy('priority')->get();
        return view('admin/update-photos')->with(compact('photo_gallery', 'header', 'footer', 'notices', 'photo', 'photos'));
    }

    public function layout_plan(){
        $notices = '';
        if(isset($_POST['add'])){
            $data['title'] = $_POST['title'];
            $data['slug'] = makeSlugOf($data['title'], 'layout_plans');
            $data['plan_code'] = $_POST['plan_code'];
            $data['description'] = $_POST['description'];
            $data['specification'] = $_POST['specification'];
            $data['price'] = $_POST['price'];
            $data['p1_name'] = $_POST['p1_name'];
            $data['p1_price'] = $_POST['p1_price'];
            $data['p1_duration'] = $_POST['p1_duration'];
            $data['p1_description'] = $_POST['p1_description'];
            $data['p2_name'] = $_POST['p2_name'];
            $data['p2_price'] = $_POST['p2_price'];
            $data['p2_duration'] = $_POST['p2_duration'];
            $data['p2_description'] = $_POST['p2_description'];
            $data['p3_name'] = $_POST['p3_name'];
            $data['p3_price'] = $_POST['p3_price'];
            $data['p3_duration'] = $_POST['p3_duration'];
            $data['p3_description'] = $_POST['p3_description'];
            $data['featured'] = $_POST['featured'];
            $ammi = array();
            $x =0;
            foreach ($_POST['ammenities'] as $ammen){
                $ammi[$ammen] = $_POST['interior_ammenities'][$x];
                $x++;
            }
            $data['interior_ammenities'] = json_encode($ammi);
            $data['category'] = (int)$_POST['category'];
            $data['drawing_views'] = json_encode($_POST['drawing_views']);
            $data['file'] = '';
            $data['images'] = '';
            $layout_plan = DB::table('layout_plans')->insertGetId($data);
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path. $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path. $image);
                            $thumb = Image::make($path. $image)->resize(165, 110)->save($path.'thumbs/'.$image);
                            $order++;
                        }
                    } else {
                        $notices .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE layout_plans SET images = '".implode(',',$images)."' WHERE id = '".$layout_plan."'");
            }
            if (request()->file('file') || request()->file('file1')) {
                $name = request()->file('file')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('file')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                request()->file('file')->move($path,$file);
                $img = Image::make($path. $file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path. $file);
                $thumb = Image::make($path. $file)->resize(165, 110)->save($path.'thumbs/'.$file);
                $name = request()->file('file1')->getClientOriginalName();
                $file1 = md5(time()).'.'.request()->file('file1')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                request()->file('file1')->move($path,$file1);
                $img = Image::make($path. $file1)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path. $file1);
                $thumb = Image::make($path. $file1)->resize(165, 110)->save($path.'thumbs/'.$file1);

                $nfile = $file.','.$file1;
                DB::update("UPDATE layout_plans SET file = '".$nfile."' WHERE id = '".$layout_plan."'");
            }
            $notices .= "<div class='alert mini alert-success'> Plan has been added successfully !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST["title"]);
            $plan_code = $_POST['plan_code'];
            $description = escape($_POST['description']);
            $specification = escape($_POST['specification']);
            $price = $_POST['price'];
            $tax = $_POST['tax'];
            $p1_name = $_POST['p1_name'];
            $p1_price = $_POST['p1_price'];
            $p1_duration = $_POST['p1_duration'];
            $p1_description = $_POST['p1_description'];
            $p2_name = $_POST['p2_name'];
            $p2_price = $_POST['p2_price'];
            $p2_duration = $_POST['p2_duration'];
            $p2_description = $_POST['p2_description'];
            $p3_name = $_POST['p3_name'];
            $p3_price = $_POST['p3_price'];
            $p3_duration = $_POST['p3_duration'];
            $p3_description = $_POST['p3_description'];
            $featured = $_POST['featured'];
            $ammi = array();
            $x =0;
            foreach ($_POST['ammenities'] as $ammen){
                $ammi[$ammen] = $_POST['interior_ammenities'][$x];
                $x++;
            }
            $interior_ammenities = json_encode($ammi);
            $category = (int)$_POST['category'];
            $drawing_views = json_encode($_POST['drawing_views']);
            $file = '';
            $images = '';
            DB::update("UPDATE layout_plans SET plan_code='$plan_code', title = '$title', price = '$price', 
			tax = '$tax', description = '$description', category = '$category', specification = '$specification', 
			interior_ammenities = '$interior_ammenities', drawing_views = '$drawing_views', p1_name = '$p1_name', p1_price = '$p1_price',
			 p1_duration = '$p1_duration', p1_description = '$p1_description', p2_name = '$p2_name', p2_price = '$p2_price',
			 p2_duration = '$p2_duration', p2_description = '$p2_description', p3_name = '$p3_name', p3_price = '$p3_price',
			 p3_duration = '$p3_duration', p3_description = '$p3_description', featured = '$featured'  WHERE id = '".$_GET['edit']."'");
            if (request()->file('images')) {
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path. $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path. $image);
                            $thumb = Image::make($path. $image)->resize(165, 110)->save($path.'thumbs/'.$image);
                            $order++;
                        }
                    } else {
                        $notices .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
                DB::update("UPDATE layout_plans SET images = '".implode(',',$images)."' WHERE id = '".$_GET['edit']."'");
            }
            if (request()->file('file') || request()->file('file1')) {
                $name = request()->file('file')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('file')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                request()->file('file')->move($path,$file);
                $img = Image::make($path. $file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path. $file);
                $thumb = Image::make($path. $file)->resize(165, 110)->save($path.'thumbs/'.$file);

                $name = request()->file('file1')->getClientOriginalName();
                $file1 = md5(time()).'.'.request()->file('file1')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                request()->file('file1')->move($path,$file1);
                $img = Image::make($path. $file1)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path. $file1);
                $thumb = Image::make($path. $file1)->resize(165, 110)->save($path.'thumbs/'.$file1);

                $nfile = $file.','.$file1;
                DB::update("UPDATE layout_plans SET file = '".$nfile."' WHERE id = '".$_GET['edit']."'");
            }
            $notices .= '<div class="alert mini alert-success">Plan updated successfully !</div>';
        }
        if(isset($_GET['delete'])){
            DB::table("layout_plans")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-dismissible alert-success'> Plan has been deleted </div>";
        }
        $plan = [];
        if(isset($_GET['edit'])){
            $plan = DB::table('layout_plans')->where('id','=',$_GET['edit'])->first();
        }
        $header = $this->header('Layout Plans','layout_plans');
        $plans = DB::table('layout_plans')->orderBy('id','DESC')->get();
        $amminities = DB::table('amminities')->orderBy('id','DESC')->get();
        $categories = DB::table('design_category')->where('parent','=',0)->orderBy('id','DESC')->get();
        $footer = $this->footer();
        return view('admin/layout_plans')->with(compact('header','notices','plans','plan','categories','footer', 'amminities'));
    }

    public function CategoryupdateQuestions(){
        $notices = '';
        $header = $this->header('Update Category Questions','category-question-entry');
        if(isset($_POST['update'])){
            DB::table("service_category_questions")->where('service_id', '=', $_GET['edit'])->where('question_id', '=', $_POST['question'])->delete();
            DB::insert("INSERT INTO service_category_questions (service_id,question_id,priority) VALUE ('".$_GET['edit']."','".$_POST['question']."','".$_POST['priority']."')");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if(isset($_GET['delete'])){
            DB::table("service_category_questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        }
        $footer = $this->footer();
        $questions = DB::table('questions')->orderBy('question','ASC')->get();
        $service_questions = DB::table('service_category_questions')->where('service_id','=',$_GET['edit'])->orderBy('priority')->get();
        return view('admin/category-update-questions')->with(compact('questions', 'header', 'footer', 'notices', 'service_questions'));
    }

    public function layout_plan_questions($slug = ''){
        if($slug == ''){
            abort(404);
        }
        $plan = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if(!$plan){
            abort(404);
        }
        $notices = '';
        $header = $this->header('Update Layout Plan Questions','layout-plan-questions-entry');
        if(isset($_POST['update'])){
            DB::table("layout_plan_questions")->where('plan_id', '=', $plan->id)->where('question_id', '=', $_POST['question'])->delete();
            DB::insert("INSERT INTO layout_plan_questions (plan_id,question_id,priority) VALUE (".$plan->id.",".$_POST['question'].",".$_POST['priority'].")");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if(isset($_GET['delete'])){
            DB::table("layout_plan_questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        }
        $footer = $this->footer();
        $questions = DB::table('questions')->orderBy('question','ASC')->get();
        $service_questions = DB::table('layout_plan_questions')->where('plan_id','=',$plan->id)->orderBy('priority')->get();
        return view('admin/layout-update-questions')->with(compact('questions', 'header', 'footer', 'notices', 'plan', 'service_questions'));
    }

    public function layout_plan_addon($slug = ''){
        if($slug == ''){
            abort(404);
        }
        $plan = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if(!$plan){
            abort(404);
        }
        $notices = '';
        $header = $this->header('Add Layout Plan Addon','layout-plan-addon-entry');
        if(isset($_POST['update'])){
            DB::table("layout_plan_addon")->where('layout_plan_id', '=', $plan->id)->where('layout_addon_id', '=', $_POST['layout_addon_id'])->delete();
            DB::insert("INSERT INTO layout_plan_addon (layout_plan_id,layout_addon_id) VALUE (".$plan->id.",".$_POST['layout_addon_id'].")");
            $notices .= "<div class='alert mini alert-success'> Addon has been successfully added !</div>";
        }
        if(isset($_GET['delete'])){
            DB::table("layout_plan_addon")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Addon has been deleted successfully !</div>";
        }
        $footer = $this->footer();
        $addons = DB::table('layout_addon')->get();
        $plan_addons = DB::table('layout_plan_addon')->where('plan_id','=',$plan->id)->get();
        return view('admin/layout_plan_addon')->with(compact('addons', 'header', 'footer', 'notices', 'plan', 'plan_addons'));
    }

    public function layout_plan_addon_delete($slug, $addon_id){
        $notices = '';
        if($slug == ''){
            abort(404);
        }
        $plan = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if(!$plan){
            abort(404);
        }
        DB::table("layout_plan_addon")->where('id', '=', $addon_id)->delete();
        $notices .= "<div class='alert alert-success'>Addon has been deleted successfully !</div>";
        $header = $this->header('Update Layout Plan Addons','layout-plan-addons-entry');
        $footer = $this->footer();
        $addons = DB::table('layout_addon')->get();
        $service_questions = DB::table('layout_plan_addon')->where('layout_plan_id','=',$plan->id)->orderBy('priority')->get();
        return back();
    }

    public function layout_plan_delete_question($slug, $que_id){
        $notices = '';
        if($slug == ''){
            abort(404);
        }
        $plan = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if(!$plan){
            abort(404);
        }
        DB::table("layout_plan_questions")->where('id', '=', $que_id)->delete();
        $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        $header = $this->header('Update Layout Plan Questions','layout-plan-questions-entry');
        $footer = $this->footer();
        $questions = DB::table('questions')->orderBy('question','ASC')->get();
        $service_questions = DB::table('layout_plan_questions')->where('plan_id','=',$plan->id)->orderBy('priority')->get();
        return back();
    }

    public function Categoryquestions(){
        $notices = '';
        if(isset($_POST['add'])){
            $service_id = $_POST['service_id'];
            $question_id = $_POST['question_id'];
            $priority = $_POST['priority'];
            DB::insert("INSERT INTO service_category_questions (service_id,question_id,priority) VALUE ('$service_id','$question_id','$priority')");
            $notices .= "<div class='alert mini alert-success'> Question has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $service_id = $_POST['service_id'];
            $question_id = $_POST['question_id'];
            $priority = $_POST['priority'];
            DB::update("UPDATE service_category_questions SET service_id = '$service_id',question_id = '$question_id',priority = '$priority' WHERE id = '".$_GET['edit']."'");
            $notices .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("service_category_questions")->where('id', '=', $_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Question has been deleted successfully !</div>";
        }
        $header = $this->header('Service Category Question Master','service_category_questions');
        $services = DB::table("services_category")->orderBy('id','DESC')->get();
        $que = DB::table('service_category_questions')->orderBy('id','DESC')->get();
        $questions = DB::table('questions')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $ques = DB::table('service_category_questions')->where('id','=',$_GET['edit'])->first();
        }
        $footer = $this->footer();
        return view('admin/service_category_questions')->with(compact('header','notices','services','questions','ques','que','question','option_1','option_2','option_3','option_4','weightage','type','footer'));
    }

    public function Categoryimage(){
        $notices = '';
        $header = $this->header('Update Category Images','category-image-entry');
        if(isset($_POST['update'])){
            $gallery_category_id = $_POST['gallery_category_id'];
            $image_1 = $_POST['image_1'];
            $image_2 = $_POST['image_2'];
            $image_3 = $_POST['image_3'];
            $image_4 = $_POST['image_4'];
            $image_5 = $_POST['image_5'];
            DB::insert("INSERT INTO services_category_image (service_category_id,gallery_category_id,image_1,image_2,image_3,image_4,image_5) 
            VALUE ('".$_GET['edit']."','".$gallery_category_id."','".$image_1."','".$image_2."','".$image_3."','".$image_4."','".$image_5."')");
            $notices .= "<div class='alert mini alert-success'> Image has been successfully added !</div>";
        }
        if(isset($_GET['delete'])){
            DB::table('services_category_image')->where('id','=',$_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'>Question has been deleted successfully !</div>";
        }
        if(isset($_GET['delete'])){
            DB::table('services_category_image')->where('service_category_id','=',$_GET['edit'])->where('gallery_category_id','=',$_POST['gallery_category_id'])->delete();
            $notices .= "<div class='alert mini alert-success'> Image has been successfully deleted !</div>";
        }
        $footer = $this->footer();
        $category = DB::table('design_category')->orderBy('id','ASC')->get();
        $gallery = DB::table('services_category_image')->where('service_category_id','=',$_GET['edit'])->orderBy('id')->get();
        return view('admin/service_category_image')->with(compact('category', 'header', 'footer', 'gallery', 'notices', 'service_questions'));
    }

    public function Getimage(){
        $option = '<option value="">Please Select Image</option>';
        $category_id = $_POST['category_id'];
        $images = DB::table('photo_gallery')->where('category','=',$category_id)->get();
        foreach($images as $image) {
            $option .= '
            <option value="'.$image->id.'">'.$image->title.'</option>';
        }
        return $option;
    }

    public function banners(){
        $notices = '';
        $banner = '';
        if(isset($_POST['add'])){
            $title = escape($_POST['title']);
            $short_description = escape($_POST['short_description']);
            $priority = $_POST['priority'];
            $section = $_POST['section'];
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO banner (title,short_description,image,priority,section) VALUE ('$title','$short_description','$file','$priority', '$section')");
            $notices .= "<div class='alert mini alert-success'> Banner has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST['title']);
            $short_description = escape($_POST['short_description']);
            $priority = $_POST['priority'];
            $section = $_POST['section'];
            DB::update("UPDATE banner SET title = '$title',short_description = '$short_description',priority = '$priority',section='$section' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE banner SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $notices .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('banner')->where('id','=',$_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Banner has been deleted successfully !</div>";
        }
        $header = $this->header('Banners','banners');
        $banners = DB::table('banner')->orderBy('priority','ASC')->get();
        if(isset($_GET['edit'])) {
            $banner = DB::table('banner')->where('id','=',$_GET['edit'])->first();
        }
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/banners')->with(compact('header','notices','tp','banners','banner','footer'));
    }


    public function amminities(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = escape($_POST['name']);
            DB::insert("INSERT INTO amminities (name) VALUE ('$name')");
            $data['notices'] .= "<div class='alert mini alert-success'> Amminities has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = escape($_POST['name']);
            DB::update("UPDATE amminities SET name = '$name' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('amminities')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Amminities has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Amminities','amminities');
        $data['amminities'] = DB::table('amminities')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['amminity'] = DB::table('amminities')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/amminities')->with('data',$data);
    }

    public function getVariant(Request $request){
        $html = '';
        $response = array();
        $units = DB::table('units')->orderBy('name','ASC')->get();
        $lengths = DB::table('length')->orderBy('length','ASC')->get();
        $sizes = DB::table('size')->orderBy('id','DESC')->get();
        $response = DB::table('product_variants')->where('id','=',$_POST['id'])->first();
        $si = DB::table('size')->where('id',$response->variant_title)->first();
        $price = DB::table('multiple_size')->where('size_id',$si->id)->get();
        $sizeWeight = DB::table('size_weight')->where('size_id',$si->id)->first();
        $html ='<form action="products?edit_variants='.$_POST['id'].'&ret_id='.$_POST['ret_id'].'" method="post" enctype="multipart/form-data" style="max-width:100%">
            '.csrf_field().'
					<fieldset>
						  <div class="form-group">
								<label class="control-label">Variant Title</label>
								<select name="variant_title" class="form-control select2">';
        foreach ($sizes as $size){
            $sselected = '';
            if($size->id == $response->variant_title){
                $sselected = 'selected';
            }
            $html .= '<option value="'.$size->id.'" '.$sselected.'>'.$size->name.'</option>';
        }
        $html .= '</select>
						  </div>
						  <div class="form-group">
							<label class="control-label">Length</label>';
        if($sizeWeight){
            $html .= '<span>'.$sizeWeight->length.' - '.getUnitSymbol($sizeWeight->lunit).'<input type="hidden" value="'.$sizeWeight->length.'" name="length"></span>';
        }
        $html .= '</div>
						  <div class="form-group">
							<label class="control-label">Weight</label>';
        if($sizeWeight){
            $html .= '<span>'.$sizeWeight->weight.' - '.getUnitSymbol($sizeWeight->wunit).'<input type="hidden" value="'.$sizeWeight->weight.'" name="weight"></span>';
        }
        $html .= '</div>
						  <div class="form-group">
							<label class="control-label">Size Difference</label>
							<select name="price" class="form-control select2">';
        if($price){
            foreach($price as $pr){
                $lus = '';
                if($pr->id == $response->price){
                    $lus = 'selected';
                }
                $html .= '<option value="'.$pr->id.'" '.$lus.'>'.$pr->price.'</option>';
            }
        }
        $html .= '</select>
						  </div>
						  <div class="form-group">
							Display<br>';
        $ycheck = '';
        $ncheck = '';
        if($response->display == '1'){
            $ycheck = 'checked';
        }else{
            $ncheck = 'checked';
        }
        $html .= '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                   <input name="display" type="radio" id="dyes" value="1" '.$ycheck.'/>
                                   Yes
                                   <input name="display" type="radio" id="dyes" value="1" '.$ycheck.'/>
                                   <span class="checkmark"></span>
                               </label> <br> 
                               <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                   <input name="display" type="radio" id="dno" value="0" '.$ncheck.' />
                                   NO
                                   <input name="display" type="radio" id="dno" value="0" '.$ncheck.'/>
                                   <span class="checkmark"></span>
                               </label>
						  </div>
						  <div class="form-group">
							Manufacture<br>';
        $ycheck = '';
        $ncheck = '';
        if($response->manufacture == '1'){
            $ycheck = 'checked';
        }else{
            $ncheck = 'checked';
        }
        $html .= '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                   <input name="manufacture" type="radio" id="myes" value="1" '.$ycheck.'/>
                                   Yes
                                   <input name="manufacture" type="radio" id="myes" value="1" '.$ycheck.'/>
                                   <span class="checkmark"></span>
                               </label> <br> 
                               <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                   <input name="manufacture" type="radio" id="mno" value="0" '.$ncheck.' />
                                   NO
                                   <input name="manufacture" type="radio" id="mno" value="0" '.$ncheck.'/>
                                   <span class="checkmark"></span>
                               </label>
						  </div>
						  <input name="edit_variant" type="submit" value="Edit Variant" class="btn btn-primary btn-sm"/>
						  
					</fieldset>
				</form>';
        return $html;
    }

    public function getVariantData(Request $request){
        $var = DB::table('size')->where('id', $request->id)->first();
        if($var !== null){
            $l = explode(',', $var->length);
            $data['length1'] = '';
            if(isset($l[0])){
                $data['length1'] = $l[0];
            }
            $data['length2'] = '';
            if(isset($l[1])){
                $data['length2'] = $l[1];
            }
            $data['length3'] = '';
            if(isset($l[2])){
                $data['length3'] = $l[2];
            }
            $data['length4'] = '';
            if(isset($l[3])){
                $data['length4'] = $l[3];
            }
            $price = DB::table('multiple_size')->where('size_id',$var->id)->get();
            $p = '';
            if($price){
                foreach($price as $pr){
                    $p .= '<option value="'.$pr->id.'">'.$pr->price.'</option>';
                }
            }

            $size_weight = DB::table('size_weight')->where('size_id',$var->id)->first();
            $psw = '';
            if($size_weight !== null){
                $data['length_value'] = $size_weight->length.' - '.getUnitSymbol($size_weight->lunit);
                $data['weight_value'] = $size_weight->weight.' - '.getUnitSymbol($size_weight->wunit);
            }

            $data['price'] = $p;
            $data['w_available'] = $var->w_available;
            $data['l_available'] = $var->l_available;
            $data['s_available'] = $var->s_available;
            $data['weight'] = $var->weight;
            $data['light'] = $var->light;
            $data['super_light'] = $var->super_light;
            $data['wunit'] = $var->wunit;
            return response()->json($data, 200);
        }
        return response()->json(['error' => 'Failed'], 500);
    }

    public function getDiscount(Request $request){
        $response = array();
        $response = DB::table('product_discount')->where('id','=',$_POST['id'])->first();
        return json_encode($response);
    }

    public function home_page_design(){
        $data['notices'] = '';
        $data['header'] = $this->header('Home Page Design','home-page-design');
        if(isset($_POST['update'])){
            $text = escape($_POST['text']);
            $link = escape($_POST['link']);
            $title = escape($_POST['title']);
            DB::update("UPDATE home_page_design SET title = '$title', text = '$text',link = '$link' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'homepagedesign/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE home_page_design SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Home Page Design has been successfully updated !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['design'] = DB::select("SELECT * FROM home_page_design WHERE id = ".$_GET['edit'])[0];
        }
        $data['footer'] = $this->footer();
        $data['links'] = DB::table('design_category')->orderBy('id','ASC')->get();
        $data['designs'] = DB::table('home_page_design')->orderBy('id','ASC')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/home-page-design')->with('data',$data);
    }

    public function mega_menu_image(){
        $data['notices'] = '';
        $data['header'] = $this->header('Mega Menu Image','mega-menu-image');
        if(isset($_POST['update'])){
            $section = escape($_POST['section']);
            DB::update("UPDATE mega_menu_image SET section = '$section' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'megamenuimage/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE mega_menu_image SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Mega Menu Image has been successfully updated !</div>";
        }
        if(isset($_GET['edit'])) {
            $data['menu'] = DB::table('mega_menu_image')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['menus'] = DB::table('mega_menu_image')->orderBy('id','ASC')->get();
        return view('admin/mega-menu-image')->with('data',$data);
    }

    public function s_how_it_works(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cat = implode(',', $_POST['cat']);
            $tab_title = escape($_POST['tab_title']);
            $tab_icon = escape($_POST['tab_icon']);
            $content_title = escape($_POST['content_title']);
            $content = escape($_POST['content']);
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'how_it_works/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO s_how_it_works (cat,tab_title,tab_icon,content_title,content,image) 
                VALUE ('$cat','$tab_title','$tab_icon','$content_title','$content','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'> How it works has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $cat = implode(',',$_POST['cat']);
            $tab_title = escape($_POST['tab_title']);
            $tab_icon = escape($_POST['tab_icon']);
            $content_title = escape($_POST['content_title']);
            $content = escape($_POST['content']);
            DB::update("UPDATE s_how_it_works SET cat = '$cat',tab_title = '$tab_title',tab_icon = '$tab_icon',content_title='$content_title',content='$content' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'how_it_works/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE s_how_it_works SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('s_how_it_works')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> How it works has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('How it works','how it works');
        $data['s_how'] = DB::table('s_how_it_works')->get();
        $data['service_cats'] = DB::table('services_category')->get();
        if(isset($_GET['edit'])) {
            $data['how'] = DB::table('s_how_it_works')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/s-how-it-works')->with('data',$data);
    }

    /*Abhijeet code add*/

    public function pendingproduct(){
        $notices = '';

        if(isset($_GET['delete']))
        {
            $pid = $_GET['delete'];
            date_default_timezone_set('Asia/Kolkata');
            $dt_date = date("Y-m-d H:i:s");
            DB::delete("update products SET status='deleted', deleted_at = '".$dt_date."' where id = $pid ");
            $notices .= "<div class='alert alert-success'> Product deleted successfully !</div>";
            return back();
        }
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE products SET status = 'approve', `deleted_at` = NULL WHERE id = '".$_GET['approve']."'");
            $notices .= "<div class='alert alert-success'> Product Approve successfully !</div>";
            return back();
        }
        if(isset($_GET['reject']))
        {
            DB::update("UPDATE products SET status = 'reject', `deleted_at` = NULL WHERE id = '".$_GET['reject']."'");
            $notices .= "<div class='alert alert-success'> Products Reject successfully !</div>";
            return back();
        }
        $header = $this->header('Product','pendingproduct');
        $tp = url("/themes/".$this->cfg->theme);
        $products = DB::select("SELECT *,(select email from customers where id = added_by ) as email FROM `products` WHERE `status` = 'pending' and deleted_at IS NULL and `added_by` <> 0");
        $footer = $this->footer();
        return view('admin/pending-product')->with(compact('header','notices','tp','products','footer'));
    }
    public function deleteproduct(){
        $notices = '';
        $header = $this->header('Deleted Product','deleteproduct');
        $products = DB::select("SELECT * FROM `products` WHERE deleted_at  <> ''");
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/delete-product')->with(compact('header','notices','tp','products','footer'));
    }
    public function approvalproduct(){
        $notices = '';
        $header = $this->header('Approval Product','approvalproduct');
        $products = DB::select("SELECT * FROM `products` WHERE status = 'approve'");
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/approval-product')->with(compact('header','notices','tp','products','footer'));
    }
    public function rejectproduct(){
        $notices = '';
        $header = $this->header('Reject Product','rejectproduct');
        $products = DB::select("SELECT * FROM `products` WHERE status = 'reject'");
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/reject-product')->with(compact('header','notices','tp','products','footer'));
    }
    public function seller(){
        $notices = '';
        if(isset($_GET['delete']))
        {
            DB::table('customers')->where('id','=',$_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Customer deleted successfully !</div>";
        }
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE customers SET status = '1' WHERE id = '".$_GET['approve']."'");
            $notices .= "<div class='alert alert-success'> Customer Approve successfully !</div>";
        }
        if(isset($_GET['reject']))
        {
            DB::update("UPDATE customers SET status = '0' WHERE id = '".$_GET['reject']."'");
            $notices .= "<div class='alert alert-success'> Customer Reject successfully !</div>";
        }
        if(isset($_GET['insshow']))
        {
            DB::update("UPDATE customers SET institutional_status = '1' WHERE id = '".$_GET['insshow']."'");
            $notices .= "<div class='alert alert-success'> Institutional detail show successfully !</div>";
        }
        if(isset($_GET['inshide']))
        {
            DB::update("UPDATE customers SET institutional_status = '0' WHERE id = '".$_GET['inshide']."'");
            $notices .= "<div class='alert alert-danger'> Institutional detail Hide successfully !</div>";
        }
        $header = $this->header('Seller','seller');
        $tp = url("/themes/".$this->cfg->theme);
        $customers = DB::table('customers')->where('user_type','=','seller')->get();
        $footer = $this->footer();
        return view('admin/seller')->with(compact('header','notices','tp','customers','footer'));
    }

    public function myProduct(Request $request){
        $cid = $request->id;
        $notices = '';
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE products SET status = 'approve' WHERE id = '".$_GET['approve']."'");
            $notices .= "<div class='alert alert-success'> Customer Approve successfully !</div>";
        }
        if(isset($_GET['reject']))
        {
            DB::update("UPDATE products SET status = 'pending' WHERE id = '".$_GET['reject']."'");
            $notices .= "<div class='alert alert-success'> Customer Reject successfully !</div>";
        }
        $header = $this->header('Seller','seller');
        $products = DB::table('products')->where('added_by','=',$cid)->get();
        $username = DB::table('customers')->where('id','=',$cid)->get();
        $footer = $this->footer();
        return view('admin/myproduct')->with(compact('header','notices','username','products','footer'));
    }
    /// ABHIJEET CODE START

    public function institutional(){
        $data['notices'] = '';
        if(isset($_GET['delete']))
        {
            DB::table('customers')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Customer deleted successfully !</div>";
        }
        if(isset($_GET['approve']))
        {
            DB::update("UPDATE customers SET status = '1' WHERE id = '".$_GET['approve']."'");
            $data['notices'] .= "<div class='alert alert-success'> Customer Approve successfully !</div>";
        }
        if(isset($_GET['reject']))
        {
            DB::update("UPDATE customers SET status = '0' WHERE id = '".$_GET['reject']."'");
            $data['notices'] .= "<div class='alert alert-success'> Customer Reject successfully !</div>";
        }
        if(isset($_POST['add_price']))
        {
            $price  = $_POST['amount'];
            $id = $_GET['add_price'];
            DB::update("UPDATE customers SET institutional_price = '$price' WHERE id = '$id'");
            $data['notices'] .= "<div class='alert alert-success'> Set Amount successfully !</div>";
        }
        if(isset($_GET['add_discount']))
        {
            $data['category'] = DB::table('category')->get();
            $data['customer_discount'] = DB::table('customer_discount')->where('customer_id',$_GET['add_discount'])->get();
        }
        if(isset($_GET['edit']))
        {
            $data['category'] = DB::table('category')->get();
            $data['customerDiscount'] = DB::table('customer_discount')->where('id',$_GET['edit'])->first();
        }
        if(isset($_GET['edit_discount']))
        {
            $data['category'] = DB::table('category')->get();
            $data['lengths'] = DB::table('customer_discount')->where('id','=',$_GET['edit_discount'])->first();
        }
        if(isset($_POST['add_discount']))
        {
            $datas['category'] = $_POST['category'];
            $datas['discount'] = $_POST['discount'];
            $datas['action'] = $_POST['action'];
            $datas['discount_type'] = $_POST['discount_type'];
            $datas['customer_id'] = $_GET['add_discount'];
            DB::table('customer_discount')->insert($datas);
            $data['notices'] .= "<div class='alert alert-success'> Discount added successfully !</div>";
        }
        if(isset($_GET['deleteDiscount']))
        {
            DB::table('customer_discount')->where('id','=',$_GET['deleteDiscount'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Discount deleted successfully !</div>";
        }
        if(isset($_POST['edit_discount'])){
            $cat = $_POST['category'];
            $discount = $_POST['discount'];
            $action = $_POST['action'];
            $discount_type = $_POST['discount_type'];
            $id = $_GET['edit_discount'];
            DB::update("UPDATE customer_discount SET category='$cat', discount='$discount', action='$action', discount_type='$discount_type' WHERE id=$id");
            $data['notices'] .= "<div class='alert alert-success'> Discount edited successfully !</div>";
        }
        $data['header'] = $this->header('Institutional','institutional');
        $data['customers'] = DB::table('customers')->where('user_type','=','institutional')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        return view('admin/institutional')->with('data',$data);
    }
    public function bookings(Request $request){
        $notices = '';
        $bookings = '';
        $booking = '';
        define('API_ACCESS_KEY','AAAAd2L0SKc:APA91bHWNCFq52FHj1XBUMjD_dUn6iaCVdV0EZMKTjoSPygXCOLCEIUfLnoPHePCMPriu7l63ASRpgvPJSbXN_cErCsGacQMxUvLWsVXOv9YcZJj3hL6avefq5BdwfBug3h7aHaupKtA');
        if(isset($_GET['approve']))
        {
            $items = DB::update("UPDATE rfq_booking SET status = 1 WHERE id = '".$_GET['approve']."'");
            $notices .= "<div class='alert alert-success'> Booking ID #".$_GET['approve']." Approved!</div>";
            if($items == '1'){
                $bookings = DB::table('rfq_booking')->where('id', $_GET['approve'])->first();
                if($bookings->direct_booking == '0'){
                    $status = '3';
                    DB::update("UPDATE request_quotation SET status = '$status' WHERE id = '".$bookings->rfq_id."'");
                }
                $getrfq = DB::table('rfq_booking')
                    ->where('id','=',$_GET['approve'])
                    ->first();
                $coun = DB::table('booking_count')
                    ->select('customer_id','booking_counter')
                    ->where('customer_id','=',$getrfq->user_id)
                    ->first();
                if($coun){
                    $total = $coun->booking_counter+1;
                    DB::update("UPDATE booking_count SET booking_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO booking_count (customer_id,booking_counter)
					VALUE ('".$getrfq->user_id."',1)");
                }
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your booking has been approved'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
            }
        }
        if(isset($_GET['reject']))
        {
            $items = DB::update("UPDATE rfq_booking SET status = 2 WHERE id = '".$_GET['reject']."'");
            $notices .= "<div class='alert alert-danger'> Booking ID #".$_GET['reject']." Rejected!</div>";
            if($items == '1'){
                $bookings = DB::table('rfq_booking')->where('id', $_GET['reject'])->first();
                if($bookings->direct_booking == '0'){
                    $status = '4';
                    DB::update("UPDATE request_quotation SET status = '$status' WHERE id = '".$bookings->rfq_id."'");
                }
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your booking has been rejected'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
            }
        }
        if(isset($_GET['view']))
        {
            $booking = DB::table('rfq_booking')->where('id',$_GET['view'])->first();//("select  rfq_booking SET status = 2 WHERE id = '".$_GET['reject']."'");
        }
        if(isset($_POST['updateStatus'])){
            $pid = $_POST['id'];
            $status = $_POST['status'];
            $items = DB::update("UPDATE rfq_booking SET status = '$status' WHERE id = '".$pid."'");
            if($items){
                $bookings = DB::table('rfq_booking')->where('id', $_GET['updateStatus'])->first();
                if($bookings->direct_booking !== '0'){
                    if($status == '1'){
                        $getrfq = DB::table('rfq_booking')
                            ->where('id','=',$_GET['updateStatus'])
                            ->first();
                        $coun = DB::table('booking_count')
                            ->select('customer_id','booking_counter')
                            ->where('customer_id','=',$getrfq->user_id)
                            ->first();
                        if($coun){
                            $total = $coun->booking_counter+1;
                            DB::update("UPDATE booking_count SET booking_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                        }
                        else{
                            $id = DB::insert("INSERT INTO booking_count (customer_id,booking_counter)
							VALUE ('".$getrfq->user_id."',1)");
                        }
                        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                        $cust_id = $customers->id;
                        $note_read = 0;
                        $token =$customers->gsm_id;
                        $note = [
                            'title' =>'Bookings',
                            'body' => 'Your booking has beem approved'
                        ];
                        $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                        $fcmNotification = [
                            'to'        => $token, //single token
                            'notification' => $note
                        ];
                        $headers = [
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $result;
                    }
                }
                elseif($bookings->direct_booking == '0'){
                    if($status == '2'){
                        $getrfq = DB::table('rfq_booking')
                            ->where('id','=',$_GET['updateStatus'])
                            ->first();
                        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                        $cust_id = $customers->id;
                        $note_read = 0;
                        $token =$customers->gsm_id;
                        $note = [
                            'title' =>'Bookings',
                            'body' => 'Your booking has been rejected'
                        ];
                        $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                        $fcmNotification = [
                            'to'        => $token, //single token
                            'notification' => $note
                        ];
                        $headers = [
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $result;
                        $st = '4';
                        DB::update("UPDATE request_quotation SET status = '$st' WHERE id = '".$bookings->rfq_id."'");
                    }elseif($status == '1'){
                        $getrfq = DB::table('rfq_booking')
                            ->where('id','=',$_GET['updateStatus'])
                            ->first();
                        $coun = DB::table('booking_count')
                            ->select('customer_id','booking_counter')
                            ->where('customer_id','=',$getrfq->user_id)
                            ->first();
                        if($coun){
                            $total = $coun->booking_counter+1;
                            DB::update("UPDATE booking_count SET booking_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                        }
                        else{
                            $id = DB::insert("INSERT INTO booking_count (customer_id,booking_counter)
							VALUE ('".$getrfq->user_id."',1)");
                        }
                        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                        $cust_id = $customers->id;
                        $note_read = 0;
                        $token =$customers->gsm_id;
                        $note = [
                            'title' =>'Bookings',
                            'body' => 'Your booking has been approved'
                        ];
                        $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                        $fcmNotification = [
                            'to'        => $token, //single token
                            'notification' => $note
                        ];
                        $headers = [
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $result;

                        $st = '3';
                        DB::update("UPDATE request_quotation SET status = '$st' WHERE id = '".$bookings->rfq_id."'");
                    }elseif($status == '0'){
                        $getrfq = DB::table('rfq_booking')
                            ->where('id','=',$_GET['updateStatus'])
                            ->first();
                        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                        $cust_id = $customers->id;
                        $note_read = 0;
                        $token =$customers->gsm_id;
                        $note = [
                            'title' =>'Bookings',
                            'body' => 'Your booking has been rejected'
                        ];
                        $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                        $fcmNotification = [
                            'to'        => $token, //single token
                            'notification' => $note
                        ];
                        $headers = [
                            'Authorization: key=' . API_ACCESS_KEY,
                            'Content-Type: application/json'
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        $result = curl_exec($ch);
                        curl_close($ch);
                        $result;
                        $st = '2';
                        DB::update("UPDATE request_quotation SET status = '$st' WHERE id = '".$bookings->rfq_id."'");
                    }
                }
            }
            $notices .= "<div class='alert mini alert-success'>Status Changed successfully !</div>";
        }
        if(isset($_GET['edit']))
        {
            $booking = DB::table('rfq_booking')->where('id',$_GET['edit'])->first();//("select  rfq_booking SET status = 2 WHERE id = '".$_GET['reject']."'");
        }
        $header = $this->header('Bookings','bookings');
        $bookings = DB::table('rfq_booking')->orderBy('created_at', 'DESC')->get();//Bookings::orderBy('created_at', 'DESC')->get();
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/bookings')->with(compact('header','notices','tp','bookings','footer','booking'));
    }
    public function dispatchPrograms(){
        $notices = '';
        if(isset($_GET['status']) && isset($_GET['id']))
        {
            DB::update("UPDATE dispatch_programs SET status = ".$_GET['status']." WHERE id = '".$_GET['id']."'");
            $notices = "<div class='alert alert-success'> Status Changed successfully!</div>";
        }
        $header = $this->header('Dispatch Programs','dispatch');
        $dispatches = DispatchPrograms::orderBy('created_at', 'DESC')->get();
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        return view('admin/dispatch-programs')->with(compact('header','notices','tp','dispatches','footer'));
    }

    public function requestQuotation(){
        $data['notices'] = '';
        define('API_ACCESS_KEY','AAAAd2L0SKc:APA91bHWNCFq52FHj1XBUMjD_dUn6iaCVdV0EZMKTjoSPygXCOLCEIUfLnoPHePCMPriu7l63ASRpgvPJSbXN_cErCsGacQMxUvLWsVXOv9YcZJj3hL6avefq5BdwfBug3h7aHaupKtA');
        $retVar = array();
        if(isset($_POST['request_quotation'])){
            $pid = $_GET['edit'];
            $getrfq = DB::table('request_quotation')->where('id','=',$pid)->first();
            $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
            $data['price_diff'] = $_POST['diff_price'];
            $action = $_POST['action'];
            $new_var = '';
            $i=0;
            $var = DB::table('request_quotation')->where('id',$pid)->first();
            $total = DB::table('products')->where('id',$var->product_id)->first();
            $validity  = $_POST['datetime'];
            $val1 = str_replace(" ", "", $validity);
            $val =   date('Y-m-d H:i:s', strtotime($val1));
            $variant = (array)json_decode(str_replace("\\", "", $var->variants));
            if($var->method == 'bysize'){
                $variant = json_decode($var->variants);
                $bids = explode(',', $var->brand_id);
                foreach($bids as $bid){
                    foreach($variant->$bid as $j=>$var){
                        $var_price = 0;
                        if($action == 0){
                            if(is_numeric($variant->$bid[$j]->price_detail->price)){
                                $var_price = $variant->$bid[$j]->price_detail->price-$_POST['diff_price'];
                            }
                        }
                        elseif ($action == 1){
                            if(is_numeric($variant->$bid[$j]->price_detail->price)){
                                $var_price = $variant->$bid[$j]->price_detail->price+$_POST['diff_price'];
                            }
                        }
                        $variant->$bid[$j]->var_price = $var_price;
                        $variant->$bid[$j]->basic_price = $variant->$bid[$j]->price_detail->price;
                        $variant->$bid[$j]->action = $action;
                        $variant->$bid[$j]->action_price = $_POST['diff_price'];
                        $variant->$bid[$j]->total_quantity = $total->quantity-$variant->$bid[$j]->quantity;
                    }
                }
                $new_var = json_encode($variant);
            }else{
                foreach ($variant as  $vari){
                    $var_price = 0;
                    $title = $_POST['variant_title'][$i];
                    $pricet = $_POST['var_price'][$i];
                    if($_POST['updated_price'][$i] == 'yes') {
                        if($customers->institutional_price !== null) {
                            $basic_price = $_POST['basic_price'][$i] + $customers->institutional_price;
                        }else {
                            $basic_price = $_POST['basic_price'][$i];
                        }
                    }else {
                        $basic_price = $_POST['basic_price'][$i];
                    }
                    if($action == 0){
                        if(is_numeric($basic_price)){
                            $var_price = $basic_price-$_POST['diff_price'];
                        }
                    }
                    elseif ($action == 1){
                        if(is_numeric($basic_price)){
                            $var_price = $basic_price+$_POST['diff_price'];
                        }
                    }
                    $info = array();
                    if(isset($vari->price_detail)){
                        foreach ($vari->price_detail as  $vpd){
                            $info[] = array(
                                "basic" => $vpd->basic,
                                "size_diff" => $vpd->size_diff,
                                "loading" => $vpd->loading,
                                "tax" => $vpd->tax,
                                "total_amount" => $vpd->total_amount,
                            );
                        }
                    }
                    $retVar[] = array(
                        "id" => $vari->id,
                        "title" => $vari->title,
                        "quantity" => $vari->quantity,
                        "unit" => $vari->unit,
                        "var_price" => $_POST['diff_price'],
                        "basic_price" => $basic_price,
                        "action" => $action,
                        "action_price" => $pricet,
                        "total_quantity" =>$total->quantity-$vari->quantity,
                        "price_detail" => $info
                    );
                    $new_var = json_encode($retVar);
                    $i++;
                }
            }
            $payment_option = $_POST['payment_size'];
            date_default_timezone_set('Asia/Kolkata');
            $date = date('Y-m-d H:i:s');
            $pd = $_POST['diff_price'];
            if($val > $date){
                DB::update("UPDATE request_quotation SET payment_option = '$payment_option', variants = '$new_var',status = 1,validity = '$val', price_diff='$pd', action = $action WHERE id = '".$pid."'");
                $data['notices'] .= "<div class='alert mini alert-success'>Request Quotation successfully !</div>";
                $items = DB::table('rfq_count')->select('customer_id','rfq_counter')->where('customer_id','=',$getrfq->user_id)->first();
                if($items){
                    $total = $items->rfq_counter+1;
                    DB::update("UPDATE rfq_count SET rfq_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO rfq_count (customer_id,rfq_counter) 
				VALUE ('".$getrfq->user_id."','1')");
                }
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your quotation has been approved'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
                $data['notices'] .= "<div class='alert mini alert-success'>Updated successfully.</div>";
            }else{
                $data['notices'] .= "<div class='alert mini alert-success'>Please change date if you want to update.</div>";
            }
        }
        if(isset($_GET['status']) && isset($_GET['id']))
        {
            DB::update("UPDATE dispatch_programs SET status = ".$_GET['status']." WHERE id = '".$_GET['id']."'");
            $data['notices'] = "<div class='alert alert-success'> Status Changed successfully!</div>";
        }
        if(isset($_POST['updateStatus'])){
            $pid = $_POST['id'];
            $status = $_POST['status'];
            DB::update("UPDATE request_quotation SET status = '$status' WHERE id = '".$pid."'");
            $data['notices'] .= "<div class='alert mini alert-success'>Status Changed successfully !</div>";
            if($status == '1'){
                $getrfq = DB::table('request_quotation')
                    ->where('id','=',$pid)
                    ->first();
                $items = DB::table('rfq_count')
                    ->select('customer_id','rfq_counter')
                    ->where('customer_id','=',$getrfq->user_id)
                    ->first();
                if($items){
                    $total = $items->rfq_counter+1;
                    DB::update("UPDATE rfq_count SET rfq_counter = '$total' WHERE customer_id = '".$getrfq->user_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO rfq_count (customer_id,rfq_counter) 
				VALUE ('".$getrfq->user_id."','1')");
                }
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your quotation has been approved'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
                $data['notices'] .= "<div class='alert mini alert-success'>Updated successfully.</div>";
            }else{
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$getrfq->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your quotation has been rejected'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
                $data['notices'] .= "<div class='alert mini alert-success'>Updated successfully.</div>";
            }
        }
        if(isset($_GET['updateStatus']))
        {
            $data['qStatus'] = DB::table('request_quotation')->where('id',$_GET['updateStatus'])->first();

        }
        $data['header'] = $this->header('Request Quotation','dispatch');
        $data['quotations'] = DB::table('request_quotation')->orderBy('id', 'DESC')->get();
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['notices'] .= "";

        return view('admin/request-quotation')->with('data',$data);
    }

    /// ABHIJEET CODE END
    public function sellerView(){
        $notices = '';
        if(isset($_GET['id']))
        {
            $pid = $_GET['id'];
            $customer = DB::table('customers')->where('id','=',$pid)->first();
        }
        if(isset($_POST['update'])){
            $id = $_POST['cid'];
            $name = $_POST['name'];
            $email = $_POST['email'];
            $mobile = $_POST['mobile'];
            $company = $_POST['company'];
            $pan = $_POST['pan'];
            $gst = $_POST['gst'];
            $creditCharges = $_POST['credit_charges'];
            DB::update("UPDATE customers SET name = '$name',email = '$email',mobile = '$mobile',company = '$company',pan = '$pan',gst = '$gst',credit_charges = '$creditCharges' WHERE id = '".$id."'");
            $notices = "<div class='alert alert-success mini'> Profile updated successfully </div>";
        }
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->header('Seller','seller');
        $footer = $this->footer();
        return view('admin/sellerview')->with(compact('header','notices','tp','customer','footer'));
    }
    public function MatchSku(){
        $sku_get = $_POST['sku'];
        $sku_exp = explode('-',$sku_get);
        $sku = $sku_exp[0];
        $product = DB::table('products')->where('sku','LIKE',"%$sku%")->get();
        if(count($product)==1){
            echo "not_match";
        }
        else{
            echo "match";
        }
    }

    //Puneet New
    public function sellersPayment(){
        $notices = '';
        if(isset($_POST['add'])){
            $bal_amount = $_POST['balance_amount'] - $_POST['paid_amount'];
            DB::insert("INSERT INTO multiseller_payments (seller_id,order_id,payment_type,payment_status,payment_method,payment_data,amount,paid_amount,balance_amount,description,remark) 
              VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$_POST['payment_type']."''".$_POST['payment_status']."','".$_POST['payment_method']."','".$_POST['payment_data']."','0','".$_POST['paid_amount']."','".$bal_amount."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            $notices .= "<div class='alert mini alert-success'> Payment has been successfully added !</div>";
        }
        $header = $this->header('Sellers Payments','sellers_payments');
        $tp = url("/themes/".$this->cfg->theme);
        $footer = $this->footer();
        $mpayments = DB::table('multiseller_payments')->orderBy('id','DESC')->get();
        $orderid = DB::table('multiseller_payments')->groupBy('order_id')->get();
        $sellers = DB::table('customers')->where('user_type','=','seller')->orderBy('id','DESC')->get();
        return view('admin/sellers-payment')->with(compact('header','notices','mpayments','orderid','sellers','tp','footer'));
    }

    public function GetAmount(){
        $bal_amt = DB::table('multiseller_payments')->where('seller_id','=',$_POST['seller_id'])->where('order_id','=',$_POST['order_id'])->orderBy('id','DESC')->first();
        $amt = DB::table('multiseller_payments')->where('seller_id','=',$_POST['seller_id'])->where('order_id','=',$_POST['order_id'])->orderBy('id','ASC')->first();
        $response = array(
            'balance_amount' => array($bal_amt->balance_amount),
            'amount' => array($amt->amount)
        );
        return json_encode($response);
    }

    public function CrNote(){
        $notices = '';
        if(isset($_POST['add'])){
            $bal_amount = $_POST['balance_amount'] + $_POST['paid_amount'];
            $cr = DB::insert("INSERT INTO cr_note (seller_id,order_id,payment_data,amount,paid_amount,description,remark) 
              VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$_POST['payment_data']."','".$_POST['amount']."','".$_POST['paid_amount']."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            if($cr) {
                $id = DB::getPdo()->lastInsertId();
                $lid = 'cr_'.$id;
                $notices .= "<div class='alert mini alert-success'> Cr Note has been successfully added !</div>";

                DB::insert("INSERT INTO multiseller_payments (seller_id,order_id,cr_id,payment_data,amount,paid_amount,balance_amount,description,remark) 
              VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$lid."','".$_POST['payment_data']."','0','".$_POST['paid_amount']."','".$bal_amount."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            }
        }

        $header = $this->header('Cr. Note','cr_note');
        $tp = url("/themes/".$this->cfg->theme);
        $footer = $this->footer();
        $mpayments = DB::table('cr_note')->orderBy('id','DESC')->get();
        $orderid = DB::table('multiseller_payments')->groupBy('order_id')->get();
        $sellers = DB::table('customers')->where('user_type','=','seller')->orderBy('id','DESC')->get();
        return view('admin/cr-note')->with(compact('header','notices','mpayments','orderid','sellers','tp','footer'));
    }

    public function DrNote(){
        $notices = '';
        if(isset($_POST['add'])){
            $bal_amount = $_POST['balance_amount'] - $_POST['paid_amount'];
            $dr = DB::insert("INSERT INTO dr_note (seller_id,order_id,payment_data,amount,paid_amount,description,remark) 
                VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$_POST['payment_data']."','".$_POST['amount']."','".$_POST['paid_amount']."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            if($dr) {
                $id = DB::getPdo()->lastInsertId();
                $lid = 'dr_'.$id;
                $notices .= "<div class='alert mini alert-success'> Dr Note has been successfully added !</div>";

                DB::insert("INSERT INTO multiseller_payments (seller_id,order_id,dr_id,payment_data,amount,paid_amount,balance_amount,description,remark) 
                VALUE ('".$_POST['seller_id']."','".$_POST['order_id']."','".$lid."','".$_POST['payment_data']."','0','".$_POST['paid_amount']."','".$bal_amount."','".escape($_POST['description'])."','".escape($_POST['remark'])."')");
            }
        }
        $header = $this->header('Dr. Note','dr_note');
        $tp = url("/themes/".$this->cfg->theme);
        $footer = $this->footer();
        $mpayments = DB::table('dr_note')->orderBy('id','DESC')->get();
        $orderid = DB::table('multiseller_payments')->groupBy('order_id')->get();
        $sellers = DB::table('customers')->where('user_type','=','seller')->orderBy('id','DESC')->get();
        return view('admin/dr-note')->with(compact('header','notices','mpayments','orderid','sellers','tp','footer'));
    }

    public function SellerPo(){
        $data['notices'] = '';
        $data['header'] = $this->header('Seller Po.','seller_po');
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        if(isset($_GET['view'])) {
            $data['order'] = DB::table('seller_po')->where('id','=',$_GET['view'])->limit(1)->first();
        }
        $data['sellerpo'] = DB::table('seller_po')->orderBy('id','DESC')->get();
        return view('admin/seller_po')->with('data',$data);
    }

    public function DefaultImage(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $page = escape($_POST['page']);
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO default_image (image,page) VALUE ('$page','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'> Image has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $page = escape($_POST['page']);
            DB::update("UPDATE default_image SET page = '$page' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE default_image SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Image edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('default_image')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Image has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Deefault Image','default_image');
        $data['images'] = DB::table('default_image')->get();
        if(isset($_GET['edit'])) {
            $data['image'] = DB::table('default_image')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/default_image')->with('data',$data);
    }

    public function productType(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cid = $_POST['name'];
            DB::insert("INSERT INTO product_type (name) VALUE ('$cid')");
            $data['notices'] .= "<div class='alert mini alert-success'> Data saved successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $cid = $_POST['name'];
            DB::update("UPDATE product_type SET name = '$cid' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('product_type')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Category has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Product Type','product-type');
        if(isset($_GET['edit'])) {
            $data['type'] = DB::table('product_type')->where('id','=',$_GET['edit'])->get();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['types'] = DB::table('product_type')->orderBy('id','ASC')->get();
        return view('admin/product-type')->with('data',$data);
    }

    public function filters(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $cid = $_POST['name'];
            DB::insert("INSERT INTO filter (name) VALUE ('$cid')");
            $data['notices'] .= "<div class='alert mini alert-success'> Data saved successfully !</div>";
        }
        if(isset($_POST['edit'])){
            $cid = $_POST['name'];
            DB::update("UPDATE filter SET name = '$cid' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('filter')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Filter has been deleted successfully !</div>";
        }
        if(isset($_POST['add_option'])){
            $cid = $_POST['name'];
            foreach ($cid as $c) {
                $fid = $_POST['filter_id'];
                DB::insert("INSERT INTO filter_options (filter_id, name) VALUE ('$fid', '$c')");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Data saved successfully !</div>";
        }
        if(isset($_POST['edit_option'])){
            $cid = $_POST['name'];
            DB::update("UPDATE filter_options SET name = '$cid' WHERE id = '".$_GET['edit_option']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data updated successfully !</div>";
        }
        if(isset($_GET['delete_option']))
        {
            DB::table('filter_options')->where('id','=',$_GET['delete_option'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Filter has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Filters','filters');
        if(isset($_GET['edit'])) {
            $data['filter'] = DB::table('filter')->where('id','=',$_GET['edit'])->first();
        }
        if(isset($_GET['edit_option'])) {
            $data['filter_opt'] = DB::table('filter_options')->where('id','=',$_GET['edit_option'])->first();
        }
        $data['footer'] = $this->footer();
        if(isset($_GET['view_option'])) {
            $data['filter_opts'] = DB::table('filter_options')->where('filter_id','=',$_GET['view_option'])->get();
        }
        $data['filters'] = DB::table('filter')->orderBy('id','ASC')->get();
        return view('admin/filters')->with('data',$data);
    }

    public function BestForLayout(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['link'] = $_POST['link'];
            $datas['title'] = $_POST['title'];
            $datas['content'] = $_POST['content'];
            $post = DB::table('best_for_layout')->insertGetId($datas);
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_layout SET image = '".$bfile."' WHERE id = '".$post."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Best For Category has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $link = $_POST['link'];
            $category = $_POST['title'];
            $content = $_POST['content'];
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_layout SET image = '".$bfile."' WHERE id = '".$_GET['edit']."'");
            }
            DB::update("UPDATE best_for_layout SET link = '$link', title = '$category', content = '$content' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('best_for_layout')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Best For Category Master','best-for-cat');
        $data['bestforcats'] = DB::table('best_for_layout')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['bestforcat'] = DB::table('best_for_layout')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/best-for-layout')->with('data',$data);
    }

    public function DesignCatBanner(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $title = escape($_POST['title']);
            $short_description = escape($_POST['description']);
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            DB::insert("INSERT INTO design_cat_banner (title,description,image) VALUE ('$title','$short_description','$file')");
            $data['notices'] .= "<div class='alert mini alert-success'> Banner has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $title = escape($_POST['title']);
            $short_description = escape($_POST['description']);
            DB::update("UPDATE design_cat_banner SET title = '$title',description = '$short_description' WHERE id = '".$_GET['edit']."'");
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/'.'banners/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE design_cat_banner SET image = '$file' WHERE id = '".$_GET['edit']."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Page edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('design_cat_banner')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Banner has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Design Category Banner','design-cat-banner');
        $data['cbanners'] = DB::table('design_cat_banner')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['cbanner'] = DB::table('design_cat_banner')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/design-cat-banner')->with('data',$data);
    }

    public function BestForPhoto(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $data['link'] = $_POST['link'];
            $data['title'] = $_POST['title'];
            $data['content'] = $_POST['content'];
            $post = DB::table('best_for_photo')->insertGetId($data);
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_photo SET image = '".$bfile."' WHERE id = '".$post."'");
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Best For Photo has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $link = $_POST['link'];
            $category = $_POST['title'];
            $content = $_POST['content'];
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_photo SET image = '".$bfile."' WHERE id = '".$_GET['edit']."'");
            }
            DB::update("UPDATE best_for_photo SET link = '$link', title = '$category', content = '$content' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('best_for_photo')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Best For Category Master','best-for-cat');
        $data['bestforcats'] = DB::table('best_for_photo')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['bestforcat'] = DB::table('best_for_photo')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        return view('admin/best-for-photo')->with('data',$data);
    }

    public function BestForAdvices(){
        $notices = '';
        if(isset($_POST['add'])){
            $data['link'] = $_POST['link'];
            $data['title'] = $_POST['title'];
            $data['content'] = $_POST['content'];
            $post = DB::table('best_for_advices')->insertGetId($data);
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_advices SET image = '".$bfile."' WHERE id = '".$post."'");
            }
            $notices .= "<div class='alert mini alert-success'> Best For Advices has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $link = $_POST['link'];
            $category = $_POST['title'];
            $content = $_POST['content'];
            if (request()->file('image')) {
                $bname = request()->file('image')->getClientOriginalName();
                $bfile = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $bpath = base_path().'/assets/'.'products/';
                request()->file('image')->move($bpath,$bfile);
                $img = Image::make($bpath.$bfile)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($bpath.$bfile);
                DB::update("UPDATE best_for_advices SET image = '".$bfile."' WHERE id = '".$_GET['edit']."'");
            }
            DB::update("UPDATE best_for_advices SET link = '$link', title = '$category', content = '$content' WHERE id = '".$_GET['edit']."'");
            $notices .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('best_for_advices')->where('id','=',$_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $header = $this->header('Best For Category Master','best-for-cat');
        $bestforcats = DB::table('best_for_photo')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $bestforcat = DB::table('best_for_advices')->where('id','=',$_GET['edit'])->first();
        }
        $footer = $this->footer();
        return view('admin/best-for-advices')->with(compact('header','notices','bestforcats','bestforcat','footer'));
    }

    public function Length(){
        $notices = '';
        if(isset($_POST['add'])){
            $data['name'] = $_POST['name'];
            $data['symbol'] = $_POST['symbol'];
            $data['unit'] = $_POST['unit'];
            $post = DB::table('units')->insertGetId($data);
            $notices .= "<div class='alert mini alert-success'> Unit has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST['name'];
            $symbol = $_POST['symbol'];
            $unit = $_POST['unit'];
            DB::update("UPDATE units SET name = '$name', symbol = '$symbol', unit = '$unit' WHERE id = '".$_GET['edit']."'");
            $notices .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('units')->where('id','=',$_GET['delete'])->delete();
            $notices .= "<div class='alert alert-success'> Link has been deleted successfully !</div>";
        }
        $header = $this->header('Length Master','length');
        $links = DB::table('units')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $link = DB::table('units')->where('id','=',$_GET['edit'])->first();
        }
        $footer = $this->footer();
        return view('admin/length')->with(compact('header','notices','links','link','footer'));
    }

    public function Size(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $dataa['name'] = $_POST['name'];
            $group_id= $_POST['group_id'];
            $length = $_POST['length'];
            $lunit = $_POST['lunit'];
            $weight = $_POST['weight'];
            $wunit = $_POST['wunit'];
            if(isset($_POST['w_available'])){
                $dataa['w_available'] = 'available';
                $dataa['weight'] = $_POST['weight'];
            }
            if(isset($_POST['l_available'])){
                $dataa['l_available'] = 'available';
                $dataa['light'] = $_POST['light'];
            }
            if(isset($_POST['s_available'])){
                $dataa['s_available'] = 'available';
                $dataa['super_light'] = $_POST['super_light'];
            }
            $dataa['priority'] = $_POST['priority'];
            $get_sizes = DB::table('sizes_group')->where('id','=',$group_id)->first();
            $post = DB::table('size')->insertGetId($dataa);
            if($post){
                foreach($_POST['price'] as $price){
                    $id = DB::insert("INSERT INTO multiple_size (size_id,price) 
                VALUE ('".$post."','".$price."')");
                }

                $id = DB::insert("INSERT INTO size_weight (size_id,length,lunit,weight,wunit) 
					VALUE ('".$post."','".$length."','".$lunit."','".$weight."','".$wunit."')");
            }
            $v_sizes = $get_sizes->sizes.','.$post;
            DB::update("UPDATE sizes_group SET sizes = '$v_sizes' WHERE id = '$group_id'");
            $data['notices'] .= "<div class='alert mini alert-success'> Size has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $var_id = $_POST['var_id'];
            $group_id= $_POST['group_id'];
            $get_sizes = DB::table('sizes_group')->where('id','=',$group_id)->first();
            $get_size_array = explode(',',$get_sizes->sizes);
            if(!in_array($var_id,$get_size_array)){
                array_push($get_size_array,$var_id);
            }
            $v_sizes = implode(',',$get_size_array);
            $name = $_POST['name'];

            $length = $_POST['length'];
            $lunit = $_POST['lunit'];
            $weight = $_POST['weight'];
            $wunit = $_POST['wunit'];

            $priority = $_POST['priority'];
            DB::update("UPDATE sizes_group SET sizes = '$v_sizes' WHERE id = '$group_id'");
            $post = DB::update("UPDATE size SET name = '$name', priority = '$priority' WHERE id = '".$_GET['edit']."'");
            $ids = array();
            if(isset($_POST['price'])){
                foreach($_POST['price'] as $pkey=>$price){
                    $check = DB::table('multiple_size')->where('id',$pkey)->first();
                    if($price !== '') {
                        $ids[] = $pkey;
                        if ($check !== null) {
                            DB::update('UPDATE multiple_size SET price="'.$price.'" WHERE id='.$pkey);
                        }
                    }
                }
            }
            DB::table('multiple_size')->where('size_id', $_GET['edit'])->whereNotIn('id', $ids)->delete();
            if(isset($_POST['new_price'])){
                foreach($_POST['new_price'] as $newprice){
                    if($newprice !== '') {
                        $id = DB::insert("INSERT INTO multiple_size (size_id,price) VALUE ('".$_GET['edit']."','".$newprice."')");
                    }
                }
            }

            if(isset($_POST)){
                if($_POST['sizeId'] !== ''){
                    $post = DB::update("UPDATE size_weight SET length = '$length', lunit = '$lunit', weight = '$weight', wunit = '$wunit' WHERE id = '".$_POST['sizeId']."'");
                }elseif($length !== ''){
                    $id = DB::insert("INSERT INTO size_weight (size_id,length,lunit,weight,wunit) VALUE ('".$_GET['edit']."','".$length."','".$lunit."','".$weight."','".$wunit."')");
                }
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_POST['multiple_edit'])){
            $size_id = $_POST['size_id'];
            $priority = $_POST['priority'];
            $prices = array();
            $new_prices = array();
            if(isset($_POST['price'])) {
                $prices = $_POST['price'];
            }
            if(isset($_POST['new_price'])) {
                $new_prices = $_POST['new_price'];
            }
            foreach ($size_id as $key => $pv) {
                if (isset($priority[$pv])) {
                    foreach ($priority[$pv] as $keyP=>$prio) {
                        DB::update('UPDATE size SET priority="'.$prio.'" WHERE id='.$pv);
                    }
                }
                if (isset($prices[$pv])) {
                    foreach ($prices[$pv] as $key1=>$p) {
                        $check = DB::table('multiple_size')->where('id', $key1)->first();
                        if($check !== null){
                            DB::update('UPDATE multiple_size SET price="'.$p.'" WHERE id='.$key1);
                        }
                    }
                }
                if (isset($new_prices[$pv])) {
                    foreach ($new_prices[$pv] as $key2=>$p2) {
                        if($p2 != '') {
                            $id = DB::insert("INSERT INTO multiple_size (size_id,price) VALUE ('" . $pv . "','" . $p2 . "')");
                        }
                    }
                }
            }
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            $sg = DB::table('product_variants')->where("variant_title", $_GET['delete'])->count();
            if($sg){
                $data['notices'] .= "<div class='alert alert-danger'>Size is linked to one or more Products.</div>";
            }else {
                DB::table('size')->where('id','=',$_GET['delete'])->delete();
                $delcheck = DB::table('sizes_group')->whereRaw("FIND_IN_SET(?, sizes)", $_GET['delete'])->first();
                if($delcheck !== null){
                    $del = explode(',',$delcheck->sizes);
                    $i = array_search($_GET['delete'],$del);
                    if($i){
                        unset($del[$i]);
                    }
                    $imp = implode(',',$del);
                    DB::update('UPDATE sizes_group SET sizes="' . $imp . '" WHERE id=' .$delcheck->id);
                }
                $data['notices'] .= "<div class='alert alert-success'> Size has been deleted successfully !</div>";
            }
        }
        $data['header'] = $this->header('Sizes Master','sizes');
        $data['sizes'] = DB::table('size')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['size'] = DB::table('size')->where('id','=',$_GET['edit'])->first();
            $data['price'] = DB::table('multiple_size')->where('size_id',$_GET['edit'])->get();
            $data['sizeWeight'] = DB::table('size_weight')->where('size_id',$_GET['edit'])->first();
        }
        $data['groups'] = DB::table('sizes_group')->orderBy('id','DESC')->get();
        $data['units'] = DB::select("SELECT * FROM units ORDER BY name ASC");
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);

        return view('admin/size')->with('data', $data);
    }

    public function SizesGroup(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $datas['name'] = $_POST['name'];
            $datas['sizes'] = '';
            if(isset($_POST['sizes'])){
                $datas['sizes'] = implode(',', $_POST['sizes']);
            }
            $post = DB::table('sizes_group')->insertGetId($datas);
            $data['notices'] .= "<div class='alert mini alert-success'> Sizes group has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST['name'];
            $sizes = implode(',', $_POST['sizes']);
            DB::update("UPDATE sizes_group SET name = '$name', sizes = '$sizes' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Data Updated successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table('sizes_group')->where('id','=',$_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Sizes group has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Sizes Master','sizes');
        $data['sizes'] = DB::table('size')->orderBy('id','DESC')->get();
        $data['groups'] = DB::table('sizes_group')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['size'] = DB::table('sizes_group')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/sizes_group')->with('data',$data);
    }

    public function getSizesGroup(){
        $id = $_POST['id'];
        $units = DB::table('units')->orderBy('name','ASC')->get();
        $grou = DB::table('sizes_group')->where('id','=',$id)->first();
        $sid = explode(',', $grou->sizes);
        $html = '';
        $i = 0;
        foreach ($sid as $gr) {
            $sp = DB::table('size')->where('id', '=', $gr)->first();
            if($sp !== null){
                $html .= '<tr style=" border: 1px solid #000000;">
            <td style="padding: 10px;">
                <label class="control-label">Variant Title</label>
                <input type="hidden" name="variant_title[]" class="form-control" value="'.$sp->id.'"  style="background-color: white"> <br>' . $sp->name . '
              </td>
              <td style="padding: 10px;">
                <label class="control-label">Length</label>';
                $sizeWeight = DB::table('size_weight')->where('size_id',$gr)->first();
                if($sizeWeight){
                    $html .= '<input type="hidden" name="length'.$i.'[]" value="'.$sizeWeight->id.'"><span>'.$sizeWeight->length.' - '.$sizeWeight->lunit.'</span></br><span>'.$sizeWeight->weight.' - '.$sizeWeight->wunit.'</span>';
                }
                $html .= '</td>
              <td style="padding: 10px;">
                <label class="control-label">Size Difference</label>
                <select  name="price['.$sp->id.']" class="form-control select2" style="background-color: white" >';
                $price = DB::table('multiple_size')->where('size_id',$gr)->get();
                $p = '';
                if($price){
                    foreach($price as $pr){
                        $html .= '<option value="'.$pr->id.'">'.$pr->price.'</option>';
                    }
                }
                $html .= '</select>
              </td>
              <td style="padding: 10px;">
                 Display<br>
                <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                    <input name="display'.$i.'" type="radio" id="dyes" value="1" />
                    Yes
                    <input name="display'.$i.'" type="radio" id="dyes" value="1" />
                    <span class="checkmark"></span>
                </label> <br> 
                <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                    <input name="display'.$i.'" type="radio" id="dno" value="0" checked />
                    NO
                    <input name="display'.$i.'" type="radio" id="dno" value="0" checked/>
                    <span class="checkmark"></span>
                </label>
              </td>
              <td style="padding: 10px;">
                Manufacture<br>
                 <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                    <input name="manufacture'.$i.'" type="radio" id="myes" value="1" />
                    Yes
                    <input name="manufacture'.$i.'" type="radio" id="myes" value="1" />
                    <span class="checkmark"></span>
                </label> <br> 
                <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                    <input name="manufacture'.$i.'" type="radio" id="mno" value="0" checked />
                    NO
                    <input name="manufacture'.$i.'" type="radio" id="mno" value="0" checked/>
                    <span class="checkmark"></span>
                </label>
              </td>
          </tr>';
                $i++; } }
        $html .= '<tr>
            <td colspan="7" style="text-align: right"><input name="add_group_variant" type="submit" value="Add Variant" style="float: right" class="btn btn-primary btn-sm"/></td>
          </tr>';
        return $html;
    }
    public function getVariantGroup(){
        $id = $_POST['id'];
        $units = DB::table('units')->orderBy('name','ASC')->get();
        $grou = DB::table('sizes_group')->where('id','=',$id)->first();
        $sid = explode(',', $grou->sizes);
        $html = '';
        $i = 0;
        foreach ($sid as $gr) {
            $sp = DB::table('size')->where('id', '=', $gr)->first();
            $html .= '<tr style=" border: 1px solid #000000;">
            <td style="padding: 10px;">
                <label class="control-label">Variant Title</label>
                <input type="hidden" name="variant_title[]" class="form-control" value="'.htmlspecialchars($sp->name).'"  style="background-color: white"> <br>' . $sp->name . '
              </td>
              <td style="padding: 10px; width: 225px;">
                    <label class="control-label">Length</label>
                    <div class="col-lg-12">&nbsp;</div>
                    <input type="text" name="length1[]" class="form-control"  style="background-color: white; width: 50px; float: left; border: 1px solid #000000;">&nbsp;
                    <input type="text" name="length2[]" class="form-control"  style="background-color: white; width: 50px; float: left; border: 1px solid #000000;">&nbsp;
                    <input type="text" name="length3[]" class="form-control"  style="background-color: white; width: 50px; float: left; border: 1px solid #000000;">&nbsp;
                    <input type="text" name="length4[]" class="form-control"  style="background-color: white; width: 50px; float: left; border: 1px solid #000000;">
                         
              </td>
              <td style="padding: 10px;">
                    <label class="control-label">Unit</label>
                    <select name="lunit[]" class="form-control" style="background-color: white">';
            foreach ($units as $uni) {
                $html .= '<option value="' . $uni->name . '">' . $uni->name . '</option>';
            }
            $html .= '</select>
              </td>
              <td style="padding: 10px;">
                <label class="control-label">Size Difference</label>
                <input name="price[]" type="text" class="form-control"  value="' . $sp->price . '" style="background-color: white"/>
              </td>
              <td style="padding: 10px; width: 220px;">
                    <div style="width: 65px; float: left;">
                        <label class="control-label" style="font-size: 10px;">Heavy <input type="checkbox" name="w_available" value="available"> </label>
                        <input name="weight[]" type="text" value="' . $sp->weight . '" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
                    </div>
                    <div style="width: 65px; float: left;">
                        <label class="control-label" style="font-size: 10px;">Light <input type="checkbox" name="l_available" value="available"></label>
                        <input name="light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
                    </div>
                    <div style="width: 65px; float: left;">
                        <label class="control-label" style="font-size: 10px;">S. Light <input type="checkbox" name="s_available" value="available"></label>
                        <input name="super_light[]" type="text" class="form-control" style="background-color: white; border: 1px solid #000000;"/>
                    </div>
              </td>
              <td style="padding: 10px;">
                    <label class="control-label">Unit</label>
                    <select name="wunit[]" class="form-control" style="background-color: white">';
            foreach ($units as $unit) {
                $html .= '<option value="' . $unit->name . '">' . $unit->name . '</option>';
            }
            $html .= '</select>
              </td>
              <td style="padding: 10px;">
                <label class="control-label">Display</label><br>
                <input name="display'.$i.'" type="radio" id="yes" value="1" /><label for="yes">Yes</label> <br> 
                <input name="display'.$i.'" type="radio" id="no" value="0" checked /><label for="no">No</label>
              </td>
              <td style="padding: 10px;">
                <label class="control-label">Manufacture</label><br>
                <input name="manufacture'.$i.'" type="radio" id="yes" value="1" checked /><label for="yes">Yes</label> <br> 
                <input name="manufacture'.$i.'" type="radio" id="no" value="0" /><label for="no">No</label>
              </td>
          </tr>';
            $i++; }
        $html .= '<tr>
            <td colspan="7" style="text-align: right"><input name="add_group_variant" type="submit" value="Add Variant" style="float: right" class="btn btn-primary btn-sm"/></td>
          </tr>';
        return $html;
    }
    public function changeRfr(Request $request){
        $cust = Customer::find($request->id);
        if($cust !== null){
            if($request->type == 'rfq') {
                $cust->rfr = $request->rfr;
            }elseif($request->type == 'show_price'){
                $cust->show_price = $request->show_price;
            }
            $cust->save();
            return 'success';
        }
        return 'failed';
    }
    public function Notification(){
        define('API_ACCESS_KEY','AAAAd2L0SKc:APA91bHWNCFq52FHj1XBUMjD_dUn6iaCVdV0EZMKTjoSPygXCOLCEIUfLnoPHePCMPriu7l63ASRpgvPJSbXN_cErCsGacQMxUvLWsVXOv9YcZJj3hL6avefq5BdwfBug3h7aHaupKtA');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token='';
        $data['notices'] = '';
        $data['customer'] = array();
        $data['customer'] = DB::table('customers')->where('user_type','=','institutional')->get();
        if(isset($_POST['note_send'])){
            $cus = implode(',',$_POST['cust']);
            $customers = DB::table('customers')->whereIn('id',explode(',', $cus))->get();
            foreach($customers as $cust){
                $title = $_POST['title'];
                $cust_id = $cust->id;
                $note_read = 0;
                $token =$cust->gsm_id;
                $notification = $_POST['msg'];
                $note = [
                    'title' =>$title,
                    'body' => $notification
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                $result;
                $items = DB::table('notification_read')
                    ->select('customer_id','note_read')
                    ->where('customer_id','=',$cust_id)
                    ->first();
                if($items){
                    $total = $items->note_read+1;
                    DB::update("UPDATE notification_read SET note_read = '$total' WHERE customer_id = '".$cust_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO notification_read (customer_id,note_read) 
                VALUE ('".$cust_id."',1)");
                }
            }
            $id = DB::insert("INSERT INTO notification (cust_id,title,notification) 
                VALUE ('".$cus."','".$title."','".$notification."')");
            $data['notices'] .= "<div class='alert mini alert-success'> notification has been send successfully !</div>";
        }
        $data['customer_class'] = DB::table('customer_class')->get();
        $data['customer_type'] = DB::table('customer_type')->get();
        $data['customer_category'] = DB::table('customer_category')->get();
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/assets/crm/");
        $data['header'] = $this->header();
        $data['footer'] = $this->footer();
        $data['title'] = 'Admin';

        return view('admin/notification')->with('data',$data);
    }

    public function retailNotification(){
        define('API_ACCESS_KEY','AAAAeGblRAE:APA91bGsB_syWczmGNpO2Uq8hZmtW_zdxIzNeGo0iWNTnRehVaoN0DCO_vy0KxH9oJiK-1UN6-Y51yGs9Qtz9F94SXrt7AI9FiR3wqENEPS22qHXNioX_io5_EtJKRiQY_Z-AdZP87uL');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token='';
        $data['notices'] = '';
        $data['customer'] = array();
        $data['customer'] = DB::table('customers')->where('user_type','<>','institutional')->get();
        if(isset($_POST['note_send'])){
            $cus = implode(',',$_POST['cust']);
            $customers = DB::table('customers')->whereIn('id',explode(',', $cus))->get();
            foreach($customers as $cust){
                $title = $_POST['title'];
                $cust_id = $cust->id;
                $note_read = 0;
                $token =$cust->gsm_id;
                $notification = $_POST['msg'];
                $note = [
                    'title' =>$title,
                    'body' => $notification
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;
                $items = DB::table('notification_read')
                    ->select('customer_id','note_read')
                    ->where('customer_id','=',$cust_id)
                    ->first();
                if($items){
                    $total = $items->note_read+1;
                    DB::update("UPDATE notification_read SET note_read = '$total' WHERE customer_id = '".$cust_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO notification_read (customer_id,note_read) 
                VALUE ('".$cust_id."',1)");
                }
            }
            $id = DB::insert("INSERT INTO notification (cust_id,title,notification) 
                VALUE ('".$cus."','".$title."','".$notification."')");
            $data['notices'] .= "<div class='alert mini alert-success'> notification has been send successfully !</div>";
        }
        $data['customer_class'] = DB::table('customer_class')->get();
        $data['customer_type'] = DB::table('customer_type')->get();
        $data['customer_category'] = DB::table('customer_category')->get();
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/assets/crm/");
        $data['header'] = $this->header();
        $data['footer'] = $this->footer();
        $data['title'] = 'Admin';

        return view('admin/retail_notification')->with('data',$data);
    }


    public function notice(Request $request)
    {
        define('API_ACCESS_KEY','AAAAd2L0SKc:APA91bHWNCFq52FHj1XBUMjD_dUn6iaCVdV0EZMKTjoSPygXCOLCEIUfLnoPHePCMPriu7l63ASRpgvPJSbXN_cErCsGacQMxUvLWsVXOv9YcZJj3hL6avefq5BdwfBug3h7aHaupKtA');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $tokenList='cGPr63b0VdE:APA91bHxW1TMQS6bONI2JX9L64DMF302B__0EpgXLeKUCwh5zgt3beCotIAOFKDX6sYmcZUrViU1paV69blXlpmoztZ4NcP8pNtQLPCivfGN6xh-F3jePVOmBTorhx4xBVmWzoE-o155,dtRMVYW4Y7w:APA91bFOnY7lSj42DT02wPRVo6q_1voGrqbUf4g3M45osOL1nnGAG6nc4mHEKc3s7srViNa28IbvLT16YYyHirLplVSihLzazYCLV-9rucJ0q55qtXP2zeuOxym1cr2EVTAkdLjG2WeU';
        $notification = [
            'title' =>'Aakar360',
            'body' => 'Welcome Aakar360.',
            'icon' =>'myIcon',
            'sound' => 'mySound'
        ];
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];
        $fcmNotification = [
            'registration_ids' => $tokenList, //multple token array
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }

    public function variantMultipleDelete(Request $request){
        $ids = $request->ids;
        DB::table('product_variants')->whereIn('id',explode(",",$ids))->delete();
        return response()->json(['status'=>true,'message'=>"variants deleted successfully."]);
    }


    public function variantData()
    {
        $group = (!empty($_GET["group"])) ? ($_GET["group"]) : 0;
        $retData = null;
        if($group) {
            $sg = DB::table('sizes_group')->where('id', $group)->first();
            if($sg !== null){
                $sizes = explode(',', $sg->sizes);
                $retData = DB::table('size')->whereIn('id', $sizes);
            }else{
                $retData = DB::table('size');
            }
        }else{
            $retData = DB::table('size');
        }
        $users = $retData->select(['id', 'name'])->orderBy('name','ASC');
        return Datatables::of($users)
            ->addColumn('price', function($users){
                $price = DB::table('multiple_size')->where('size_id',$users->id)->get();
                $p = array();
                if($price){
                    foreach($price as $pr){
                        $variants = DB::table('product_variants')
                            ->join('products','products.id','=','product_variants.product_id')
                            ->where('product_variants.price',$pr->id)
                            ->select('products.title')
                            ->groupBy('product_variants.product_id')
                            ->get();
                        $px = $pr->price;
                        $v = array();
                        foreach($variants as $var){
                            $v[] = $var->title;
                        }
                        $p[] = '<b><u>'.$px . '</u></b> - '. implode(', ', $v);
                    }
                }
                return implode('<br/><hr/>', $p);
            })
            ->addColumn('action', function($users){
                return '<a href="size?delete='.$users->id.'"><i class="icon-trash"></i></a>
                <a href="size?edit='.$users->id.'"><i class="icon-pencil"></i></a>';
            })
            ->rawColumns(['price', 'action'])
            ->make(true);
    }

    public function getMultipleData(Request $request){
        $html = '';
        $sg = DB::table('sizes_group')->where('id', $request->id)->first();
        if($sg !== null){
            $sizes = explode(',', $sg->sizes);
            $retData = DB::table('size')->whereIn('id', $sizes)->get();
            foreach($retData as $ret){
                $html .= '<div class="col-lg-12" style=" border: 1px solid #000000; padding-bottom: 5px;">
                    <div class="col-lg-3">
                        '.$ret->name.'
                        <input type="hidden" name="size_id[]" value="'.$ret->id.'" class="form-control">
                    </div>
					<div class="col-lg-3">
						Priority
                        <input type="text" name="priority['.$ret->id.'][]" value="'.$ret->priority.'" class="form-control" >
                    </div>
					
					';
                $price = DB::table('multiple_size')->where('size_id',$ret->id)->get();
                if(count($price)){
                    $p = array();
                    $html .= '<div class="col-lg-6">';
                    foreach($price as $pr){
                        $html .= '<input type="hidden" name="id[]" value="'.$pr->id.'">';
                        $variants = DB::table('product_variants')
                            ->join('products','products.id','=','product_variants.product_id')
                            ->where('product_variants.price',$pr->id)
                            ->select('products.title')
                            ->groupBy('product_variants.product_id')
                            ->get();
                        $px = '<input type="text" name="price['.$ret->id.']['.$pr->id.']" value="'.$pr->price.'" class="form-control" style="height: 30px !important;">';
                        $v = array();
                        foreach($variants as $var){
                            $v[] = $var->title;
                        }
                        $p[] = $px.' - '. implode(', ', $v);

                    }
                    $html .= implode('<br/><hr/>', $p);
                    $html .= '</div>';
                    $html .= '<div id="add_option" class="button pull-right mini-button add_option" data-key="'.$ret->id.'"><i class="icon-plus"></i></div>
                    <div class="col-lg-12 options" id="options'.$ret->id.'"></div><div class="col-lg-1"></div>';
                }
                else{
                    $html .= '<div class="col-lg-6">';
                    $html .= '<input type="text" name="new_price['.$ret->id.'][]" class="form-control" style="height: 30px !important;">';
                    $html .= '</div>';
                    $html .= '<div id="add_option" class="button pull-right mini-button add_option" data-key="'.$ret->id.'"><i class="icon-plus"></i></div>
                    <div class="col-lg-12 options'.$ret->id.'" id="options"></div><div class="col-lg-1"></div>';
                }
                $html .= '</div><div class="col-lg-12">&nbsp;</div> ';
            }
        }
        return $html;
    }

    public function requestForCall(){
        $data['notices'] = '';
        $data['header'] = $this->header('Request For Call','request-for-call');
        $data['quotations'] = DB::table('request_for_call')->orderBy('created_at', 'DESC')->get();
        $data['footer'] = $this->footer();
        if(isset($_GET['edit']))
        {
            $data['requestCall'] = DB::table('request_for_call')->where('id', $_GET['edit'])->first();
        }
        if(isset($_POST['updateStatus']))
        {
            $pid = $_POST['id'];
            $status = $_POST['status'];
            $remark = $_POST['remark'];
            $items = DB::update("UPDATE request_for_call SET remark='$remark', status = '$status' WHERE id = '".$pid."'");
        }
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/request-for-call')->with('data',$data);
    }
    public function quickQuote(){
        $notices = '';
        $header = $this->header('Quick Quote','quick-quote');
        $quotations = DB::table('quick_quote')->orderBy('time', 'DESC')->get();
        $footer = $this->footer();
        $tp = url("/themes/".$this->cfg->theme);
        if(isset($_GET['status']) && isset($_GET['id']))
        {
            DB::update("UPDATE quick_quote SET status = ".$_GET['status']." WHERE id = '".$_GET['id']."'");
            $notices = "<div class='alert alert-success'> Status Changed successfully!</div>";
        }
        return view('admin/quick-quote')->with(compact('header','notices','tp','quotations','footer'));
    }

    public function mentorProgram(){
        $data['notices'] = '';
        $data['header'] = $this->header('Mentor Program','mentor-program');
        $data['quotations'] = DB::table('mentor')->orderBy('id', 'DESC')->get();
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        if(isset($_GET['edit']))
        {
            $data['requestCall'] = DB::table('mentor')->where('id', $_GET['edit'])->first();
        }
        if(isset($_POST['updateStatus']))
        {
            $pid = $_POST['id'];
            $status = $_POST['status'];
            $items = DB::update("UPDATE mentor SET status = '$status' WHERE id = '".$pid."'");
        }
        return view('admin/mentor-program')->with('data',$data);
    }

    public function creditCharges(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            DB::insert("INSERT INTO credit_charges (days,amount) VALUE ('".escape($_POST['days'])."','".$_POST['amount']."')");
            $data['notices'] .= "<div class='alert mini alert-success'> Charges has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            DB::update("UPDATE credit_charges SET days = '".escape($_POST['days'])."',amount = '".$_POST['amount']."' WHERE id = ".$_GET['edit']);
            $data['notices'] .= "<div class='alert mini alert-success'> Charges edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("credit_charges")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Charges has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Credit Charges','credit-charges');
        $data['pages'] = DB::table('credit_charges')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['page'] = DB::table('credit_charges')->where('id','=',$_GET['edit'])->first();
        }
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        return view('admin/credit-charges')->with('data',$data);
    }

    public function getCustomerDiscount(Request $request){
        $html = '';
        $response = array();
        $category = DB::table('category')->get();
        $lengths = DB::table('customer_discount')->where('id','=',$_POST['id'])->first();
        $html ='<form action="institutional?edit_discount='.$_POST['id'].'&ret_id='.$_POST['ret_id'].'" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
		   '.csrf_field().'
			<div class="row">
				<div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
					<label class="control-label">Category</label>
					<select id="user_type" name="category" class="form-control" required="required">
						<option value="">Select Category </option>';
        foreach ($category as $size){
            $catsel = '';
            if($lengths->category == $size->id){
                $catsel = 'selected';
            }
            $html .='<option value="'.$size->id.'" '.$catsel.'>'.htmlspecialchars($size->name).'</option>';
        }
        $html .='</select>
				</div>
				  
				<div class="form-group col-md-2" style="padding-left: 30px;padding-right:30px;">
					<label class="control-label">Discount</label>
					<input name="discount" type="text" class="form-control" value="'.$lengths->discount.'" required="required"/>
				</div>
				<div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
					<label class="control-label">Discount Action</label>
					<select name="action" class="form-control" required="required">
						<option value="">Select Discount Action </option>
						<option value="0"'; if($lengths->action == 0) { $html .= "selected"; } $html .= '> + </option>
						<option value="1"'; if($lengths->action == 1) { $html .= "selected"; } $html .= '> - </option>
					</select>
				</div>
				<div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
					<label class="control-label">Discount Type</label>
					<select name="discount_type" class="form-control" required="required">
						<option value="">Select Discount Type </option>
						<option value="0"'; if($lengths->discount_type == 0) { $html .= "selected"; } $html .= '> % </option>
						<option value="1"'; if($lengths->discount_type == 1) { $html .= "selected"; } $html .= '> Flat </option>
					</select>
				</div>
				<input name="edit_discount" type="submit" value="Edit Discount" style="padding:3px 25px;" class="btn btn-primary col-md-2" />
			</div>
		</form>';
        return $html;
    }

    public function counterOffer(){
        $data['notices'] = '';
        $data['header'] = $this->header('Counter Offer','counter-offer');
        $data['quotations'] = DB::table('counter_offer')->orderBy('id', 'DESC')->get();
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        if(isset($_POST['updateStatus']))
        {
            $pid = $_POST['id'];
            $status = $_POST['status'];

            DB::update("UPDATE counter_offer SET status = '$status' WHERE id = '".$pid."'");
            $co = DB::table('counter_offer')->where('id', $pid)->first();
            $pd = getInstitutionalPrice($co->product_id, $co->user_id, $co->hub_id, false,false,false);
            if($status == 2){

                //dd($co);
                $pro = DB::table('products')->where('id', $co->product_id)->first();
                $price_detail[] = getInstitutionalPrice($co->product_id, $co->user_id, $co->hub_id, false,false,false);
                $variants[] = array(
                    "id"=>$pro->id,
                    "title"=>$pro->title,
                    "quantity"=>$co->quantity,
                    "unit"=>$co->unit,
                    "price"=>$co->price,
                    "price_detail" => $price_detail
                );
                $product_id = $co->product_id;
                $brand_id = $co->product_id;
                $user_id = $co->user_id;
                $hub_id = $co->hub_id;
                $method = 'bybasic';
                $unit = $co->unit;
                $credit = $co->credit_charge_id;
                DB::insert("INSERT INTO rfq_booking (product_id,brand_id,credit_charge_id,user_id,hub_id,method,variants,without_variants,status)
					VALUE ('".$product_id."','".$brand_id."','".$credit."','".$user_id."','".$hub_id."','".$method."','".json_encode($variants)."','1','1')");
                $items = DB::table('booking_count')
                    ->select('customer_id','booking_counter')
                    ->where('customer_id','=',$co->user_id)
                    ->first();
                if($items){
                    $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                    $customers = DB::table('customers')->where('id',$co->user_id)->first();
                    $cust_id = $customers->id;
                    $note_read = 0;
                    $token =$customers->gsm_id;
                    $note = [
                        'title' =>'Bookings',
                        'body' => 'Your counter offer ₹ '.$co->price.' against ₹ '.$pd->basic.' for '.$co->quantity.'-'.$co->unit.' has approved'
                    ];
                    $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                    $fcmNotification = [
                        'to'        => $token, //single token
                        'notification' => $note
                    ];
                    $headers = [
                        'Authorization: key=' . API_ACCESS_KEY,
                        'Content-Type: application/json'
                    ];
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $total = $items->booking_counter+1;
                    DB::update("UPDATE booking_count SET booking_counter = '$total' WHERE customer_id = '".$co->user_id."'");
                }
                else{
                    $id = DB::insert("INSERT INTO booking_count (customer_id,booking_counter)
					VALUE ('".$co->user_id."',1)");
                }
            }elseif($status == 1){
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$co->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your counter offer ₹ '.$co->price.' against ₹ '.$pd->basic.' for '.$co->quantity.'-'.$co->unit.' has unapproved'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
            }else{
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $customers = DB::table('customers')->where('id',$co->user_id)->first();
                $cust_id = $customers->id;
                $note_read = 0;
                $token =$customers->gsm_id;
                $note = [
                    'title' =>'Bookings',
                    'body' => 'Your counter offer ₹ '.$co->price.' against ₹ '.$pd->basic.' for '.$co->quantity.'-'.$co->unit.' has rejected'
                ];
                $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
                $fcmNotification = [
                    'to'        => $token, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
            }
        }
        if(isset($_GET['edit']))
        {
            $data['counter'] = DB::table('counter_offer')->where('id',$_GET['edit'])->first();
        }
        return view('admin/counter-offer')->with('data',$data);
    }

    public function dealers(){
        $data['notices'] = '';
        if(isset($_POST['add'])){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $user_type = 'dealer';
            DB::insert("INSERT INTO customers (name,email,password,user_type) VALUE ('$name','$email','$password','$user_type')");
            $data['notices'] .= "<div class='alert mini alert-success'> Dealer has been successfully added !</div>";
        }
        if(isset($_POST['edit'])){
            $name = $_POST["name"];
            $email = $_POST["email"];
            $mobile = $_POST["mobile"];
            $status = $_POST["status"];
            DB::update("UPDATE customers SET name = '$name',email = '$email',mobile = '$mobile',status='$status' WHERE id = '".$_GET['edit']."'");
            $data['notices'] .= "<div class='alert mini alert-success'> Dealer edited successfully !</div>";
        }
        if(isset($_GET['delete']))
        {
            DB::table("customers")->where('id', '=', $_GET['delete'])->delete();
            $data['notices'] .= "<div class='alert alert-success'> Dealer has been deleted successfully !</div>";
        }
        $data['header'] = $this->header('Dealers','dealers');
        $data['dealers'] = DB::table('customers')->where('user_type','dealer')->orderBy('id','DESC')->get();
        if(isset($_GET['edit'])) {
            $data['dealer'] = DB::table('customers')->where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/dealers')->with('data',$data);
    }


    public function cities(){
        $data['notices'] = '';
        $data['buttons'] = "[]";
        if(isset($_POST['edit'])){
            $id = $_GET['edit'];
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images';
                request()->file('image')->move($path,$file);
            }
            $city = City::find($id);
            $city->is_available = $_POST['is_available'];
            $city->image = $file;
            $city->save();
            $data['notices'] .= "<div class='alert mini alert-success'> City updated successfully !</div>";
        }
        if(isset($_GET['add_pincodes'])){
            $data['id'] = $_GET['add_pincodes'];
            $data['categories'] = Category::get();
            $data['units'] = Unit::get();
            $data['pincodes'] = CityWiseShippingCharges::where('city_id','=',$_GET['add_pincodes'])->get();
        }
        if(isset($_GET['edit_pincode'])){
            $data['id'] = $_GET['edit_pincode'];
            $data['categories'] = Category::get();
            $data['units'] = Unit::get();
            $data['pincode'] = CityWiseShippingCharges::where('id','=',$_GET['edit_pincode'])->first();
        }
        if(isset($_POST['add_pincode'])){
            $data['id'] = $_GET['add_pincodes'];
            $pincode = new CityWiseShippingCharges();
            $pincode->from_weight = $_POST['from'];
            $pincode->to_weight = $_POST['to'];
            $pincode->unit = $_POST['unit'];
            $pincode->city_id = $_GET['add_pincodes'];
            $pincode->pincode = $_POST['pincode'];
            $pincode->price = $_POST['price'];
            $pincode->save();
            $data['notices'] .= "<div class='alert mini alert-success'> Pincode added successfully !</div>";
        }
        if(isset($_POST['edit_pincode'])){
            $data['id'] = $_GET['edit_pincode'];
            $pincode = CityWiseShippingCharges::find($_GET['edit_pincode']);
            $pincode->from_weight = $_POST['from'];
            $pincode->to_weight = $_POST['to'];
            $pincode->unit = $_POST['unit'];
            $pincode->pincode = $_POST['pincode'];
            $pincode->price = $_POST['price'];
            $pincode->save();
            $data['notices'] .= "<div class='alert mini alert-success'> Pincode updated successfully !</div>";
        }
        $data['header'] = $this->header('Cities','cities');
        $states = array_pluck(State::where('country_id', '101')->get(), 'id');
        $data['cities'] = City::whereIn('state_id',$states)->orderBy('name','ASC')->get();
        $data['states'] = State::where('country_id', '101')->get();
        if(isset($_GET['edit'])) {
            $data['city'] = City::where('id','=',$_GET['edit'])->first();
        }
        $data['footer'] = $this->footer();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        return view('admin/cities')->with('data',$data);
    }

    public function getCitiesData(Request $request){
        $states = array_pluck(State::where('country_id', '101')->get(), 'id');
        $records = City::whereIn('state_id', $states)->orderBy('name','ASC');

        $state_id = (isset($_GET["state_id"])) ? $_GET["state_id"] : false;
        if($state_id){
            $records = $records->where('state_id', $state_id);
        }
        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="cities?edit='.$records->id.'"><i class="material-icons">edit</i></a>
                <a style="padding:0 10px; margin-left: 10px" data-toggle="tooltip" title="Add Pincodes" href="cities?add_pincodes='.$records->id.'"><i class="icon-plus"></i></a>';
                return $html;
            })
            ->editColumn('state_id', function($records){
                return getStateName($records->state_id);
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
<?php

namespace App\Http\Controllers;

use App\BillingParties;
use App\BookingCount;
use App\Bookings;
use App\Brand;
use App\Category;
use App\CreditCharges;
use App\DispatchPrograms;
use App\MultipleSize;
use App\Products;
use App\ProductVariant;
use App\RequestQuotation;
use App\RfqBooking;
use App\RfqCount;
use App\Size;
use App\SizeWeight;
use App\Unit;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth;
use Yajra\Datatables\Datatables;
use DateTime;

class Institutional extends Controller
{
    private function header($title, $tp){
        return view('institutional/header')->with(compact('title','tp'))->render();
    }
    private function footer($tp){
        return view('institutional/footer')->with(compact('tp'))->render();
    }
    public function dashboard(){
        if(institutional('user_type') !== 'institutional'){
            abort(401);
        }
        $resent_quatation = RequestQuotation::latest()->limit(10)->get();
        $resent_booking = RfqBooking::latest()->limit(10)->get();
        $tp = url("/assets/institutional/");
        $title = 'Dashboard';
        $header = $this->header($title, $tp);
        $footer = $this->footer($tp);

        return view('institutional/dashboard')->with(compact('header', 'footer', 'tp','resent_quatation','resent_booking'));
    }
    public function myBookings(){
        if(institutional('user_type') !== 'institutional'){
            abort(401);
        }
        $ret = array();
        $appvret = array();
        $canlret= array();
        $variant = array();
        $tp = url("/assets/institutional/");
        $title = 'My Bookings';
        $header = $this->header($title, $tp);
        $footer = $this->footer($tp);
        /*Pending Booking*/
        $bookings = RfqBooking::where('user_id',institutional('id'))->where(function($q){
            $q->where('status', 0)->orWhere('status', 1);
        })->orderBy('id', 'DESC')->get();
        foreach ($bookings as $booking){
            $product = Products::where('id', $booking->product_id)->first();
            $pname = Products::where('id', $booking->brand_id)->first();
            $proName = '';
            if($pname !== null){
                $proName = $pname->title;
            }else{
                $proName = $product->title;
            }
            $product_variants = ProductVariant::where('product_id', $booking->product_id)->first();
            $ph = \App\PhRelations::where('products', $booking->product_id)->first();
            $data = json_decode($ph->hubs);
            $pids = array_pluck($data, 'hub');
            $key = array_search($booking->hub_id, $pids);
            $loading = 0;
            $pdata = $data[$key];
            if($product !== null){
                $variant_array = json_decode($booking->variants, true);

                foreach($variant_array as $key => $v){
                    $variant[] = (array)$v;
                }
                $retVar = array();
                $unit_array = array();
                $total_basic_price = array();
                $uni = '';
                $total_basic = 0;
                $total_size_diff = 0;
                $total_loading = 0;
                $total_tax_value = 0;
                $total_pro_quantity = 0;
                if($booking->direct_booking == 1){
                    if($booking->without_variants == 1){
                        $priceData = array();
                        foreach($variant_array as $key => $value){
                            if(!empty($value)){

                                $productin = Products::where('id', $booking->brand_id)->first();
                                $tax_value = 0;
                                $basic = $pdata->price;
                                if($productin){
                                    if($productin->tax!==''){
                                        $tax = $productin->tax;
                                        $tax_value = round(($basic)*(int)$tax/100);
                                    }
                                }
                                $total_amount = $basic+$tax_value;
                                $total_basic = $basic;
                                $total_tax_value = $tax_value;
                                $total_pro_quantity = (!empty($value->quantity)) ? $value->quantity : '';
                                $ppv = '0';
                                $info = array();
                                $info[] = array(
                                    "basic" => $basic,
                                    "tax" => $tax_value,
                                    "total_amount" => $total_amount,
                                );
                                $vid = (!empty($value->id)) ? $value->id : false;
                                if($productin){
                                    $priceData[] = getInstitutionalPrice($productin->id,institutional('sid'),$booking->hub_id,$vid, false,false);
                                }else{
                                    $priceData[] = getInstitutionalPrice(false,institutional('sid'),$booking->hub_id,$vid, false,false);
                                }
                                $pt = '';
                                if($productin){
                                    $pt = $productin->title;
                                }
                                $retVar[] = array(
                                    "id" => $vid,
                                    "title" => $pt,
                                    "price" => $basic,
                                    "quantity" => (!empty($value->quantity)) ? $value->quantity : '',
                                    "unit" => (!empty($value->unit)) ? $value->unit : '',
                                    "price_detail" => $priceData
                                );
                            }
                        }
                    }else{
                        foreach($variant_array as $key => $value){
                            if(isset($value->id)){
                                $info = array();
                                $priceData = array();
                                $var_price=0;
                                $price = 0;
                                $sizeName = '';
                                $productin = Products::where('id', $booking->product_id)->first();
                                if($booking->method == 'bysize'){
                                    $pvar = ProductVariant::where('id', $value->id)->first();
                                    if($pvar !== null){
                                        $size_name = Size::where('id', $pvar->variant_title)->first();
                                        $sizeName = $size_name->name;
                                        $prices = MultipleSize::where('size_id', $size_name->price)->first();
                                        if($prices) {
                                            $var_price = $prices->price;
                                            $price = $prices->price;
                                        }
                                    }
                                }else{
                                    $sizeName = $productin->title;
                                }
                                $total_amount ='';
                                $total_quantity = 0;
                                $product_quantity = $productin->quantity;
                                $tax_value='';
                                $quantity = '';
                                $action = 0;
                                $basic = $pdata->price;
                                $loading =$pdata->loading;;
                                if($productin->tax!==''){
                                    $tax = $productin->tax;
                                    $tax_value = round(($basic+$var_price)*(int)$tax/100);
                                }
                                $total_amount = round($basic+$var_price+$tax_value+$loading);
                                if($value->quantity !== ''){
                                    $total_basic = round(($total_basic + $basic) * $value->quantity);
                                    $total_size_diff = round(($total_size_diff + $var_price) * $value->quantity);
                                    $total_loading = round(($total_loading + $loading) * $value->quantity);
                                    $total_tax_value = round(($total_tax_value + $tax_value) * $value->quantity);
                                }else{
                                    $total_basic = round($total_basic + $basic);
                                    $total_size_diff = round($total_size_diff + $var_price);
                                    $total_loading = round($total_loading + $loading);
                                    $total_tax_value = round($total_tax_value + $tax_value);
                                }
                                if($value->quantity !== ''){
                                    $total_pro_quantity = $total_pro_quantity + $value->quantity;
                                }
                                $priceData[] = getInstitutionalPrice($productin->id,institutional('id'),$booking->hub_id,$value->id, false);
                                $variant_length = '';
                                $variant_length_unit = '';
                                $variant_weight = '';
                                $variant_weight_unit = '';
                                if($productin->show_weight_length == 1){
                                    $size = Size::where('id', $value->variant_title_id)->first();
                                    $sw = SizeWeight::where('size_id',$size->id)->first();
                                    if($sw){
                                        $variant_length = $sw->length;
                                        $variant_length_unit = getUnitSymbol($sw->lunit);
                                        $variant_weight = $sw->weight;
                                        $variant_weight_unit = getUnitSymbol($sw->wunit);
                                    }
                                }
                                $retVar[] = array(
                                    "id" => $value->id,
                                    "title" => $sizeName,
                                    "price" => $basic,
                                    "quantity" => $value->quantity,
                                    "unit" => $value->unit,
                                    "price_detail" => $priceData,
                                    "variant_length" => $variant_length,
                                    "variant_length_unit" => $variant_length_unit,
                                    "variant_weight" => $variant_weight,
                                    "variant_weight_unit" => $variant_weight_unit
                                );
                            }
                        }
                    }
                }
                else{
                    $rfqData = $booking;
                    if(isset($rfqData->variants)){
                        $var = json_decode($rfqData->variants);
                        $pid = $rfqData->product_id;
                        foreach($var as $key => $value){
                            if(isset($value->id)){
                                $info = array();
                                $price = 0;
                                $var_price=0;
                                $size_name = '';
                                $productin = Products::where('id', $booking->product_id)->first();
                                $total_amount ='';
                                $total_quantity = 0;
                                $product_quantity = $productin->quantity;
                                $tax_value='';
                                $quantity = '';
                                $uni = $value->unit;
                                $basic = floatval($value->price);
                                if(isset($value->var_price)){
                                    $var_price = floatval($value->var_price);
                                }
                                $loading =$pdata->loading;
                                if($productin->tax!==''){
                                    $tax = $productin->tax;
                                    $tax_value = round(($basic+$var_price+$loading)*(int)$tax/100);
                                }
                                $total_amount = ($basic+$var_price+$loading)+$tax_value;
                                if($value->quantity == ''){
                                    continue;
                                }
                                if($value->quantity !== ''){
                                    $total_basic = round($total_basic + ($basic * $value->quantity));
                                    $total_loading = round($total_loading + ($loading * $value->quantity));
                                    $total_tax_value = round($total_tax_value + ($tax_value * $value->quantity));
                                }else{
                                    $total_basic = round($total_basic + $basic);
                                    $total_loading = round($total_loading + $loading);
                                    $total_tax_value = round($total_tax_value + $tax_value);
                                }
                                if($value->quantity !== ''){
                                    $total_pro_quantity = $total_pro_quantity + $value->quantity;
                                }

                                $variant_length = '';
                                $variant_length_unit = '';
                                $variant_weight = '';
                                $variant_weight_unit = '';
                                if($productin->show_weight_length == 1){
                                    $pvar = DB::table('product_variants')->where('id', $value->id)->first();
                                    if($pvar !== null) {
                                        $size = DB::table('size')->where('id', $pvar->variant_title)->first();
                                        $sw = DB::table('size_weight')->where('size_id',$size->id)->first();
                                        if($sw){
                                            $variant_length = $sw->length;
                                            $variant_length_unit = getUnitSymbol($sw->lunit);
                                            $variant_weight = $sw->weight;
                                            $variant_weight_unit = getUnitSymbol($sw->wunit);
                                        }
                                    }
                                }
                                $info[] = array(
                                    "timer" => 0,
                                    "price" => "".$basic,
                                    "loading" => $loading,
                                    "tax" => "".$tax_value,
                                    "total" => "".$total_amount,
                                    "var" => "".$var_price,
                                );
                                $retVar[] = array(
                                    "id" => $value->id,
                                    "title" => $value->title,
                                    "price" => $basic,
                                    "quantity" => $value->quantity,
                                    "unit" => $value->unit,
                                    "price_detail" => $info,
                                    "variant_length" => $variant_length,
                                    "variant_length_unit" => $variant_length_unit,
                                    "variant_weight" => $variant_weight,
                                    "variant_weight_unit" => $variant_weight_unit
                                );
                            }
                        }
                    }
                }
                $booked = 0;
                BookingCount::where('customer_id',institutional('id'))
                    ->update(['booking_counter' => 0]);
                $pp = '0';
                $status = '';
                if($booking->status == 0){
                    $status = 'Unapproved';
                }elseif($booking->status == 1){
                    $status = 'Approved';
                }
                $credit_charge = array();
                if($booking->credit_charge_id !== 0 || $booking->credit_charge_id !== '' || $booking->credit_charge_id !== null){
                    $credit = CreditCharges::where('id', $booking->credit_charge_id)->first();
                    if($credit !== null){
                        $credit_charge[] = array(
                            'id'=>$credit->id,
                            'days'=>$credit->days,
                            'amount'=>$credit->amount,
                        );
                    }
                }
                $ret[] = array(
                    'id' => $booking->id,
                    'p_id' => $booking->product_id,
                    'c_id' => $product->category,
                    'location' => $booking->location,
                    'payment_option' => $booking->payment_option,
                    'image' => url('assets/products/'.image_order($product->images)),
                    'status' => $status,
                    'title' => $proName,
                    'unit' => $uni,
                    'method' => $booking->method,
                    'total_basic' => $total_basic,
                    'total_loading' => $total_loading,
                    'total_tax_value' => $total_tax_value,
                    'total_quantity' => $total_pro_quantity,
                    'variant' => $retVar,
                    'booking_date' => date("d-m-Y", strtotime($booking->created_at)),
                    'credit_charge' => $credit_charge
                );
            }
        }
        /*End Pending Booking*/
        /*Completed Booking*/
        $appvbookings = DB::table('rfq_booking')->where('user_id', institutional('id'))->where('status', 3)->orderBy('id', 'DESC')->get();
        foreach ($appvbookings as $appvbooking){
            $appvproduct = Products::where('id', $appvbooking->product_id)->first();
            $appvproduct_variants = ProductVariant::where('product_id', $appvbooking->product_id)->first();
            $appvph = \App\PhRelations::where('products', $appvbooking->product_id)->first();
            $appvdata = json_decode($appvph->hubs);
            $appvpids = array_pluck($appvdata, 'hub');
            $appvkey = array_search($appvbooking->hub_id, $appvpids);
            $appvpdata = $appvdata[$appvkey];
            if($appvproduct !== null){
                $appvvariant_array = (array)json_decode($appvbooking->variants);
                foreach($appvvariant_array as $appvkey => $appvv){
                    $appvvariant[] = (array)$appvv;
                }
                $appvretVar = array();
                $appvunit_array = array();
                $appvtotal_basic_price = array();
                $appvuni = '';
                $appvtotal_basic = 0;
                $appvtotal_size_diff = 0;
                $appvtotal_loading = 0;
                $appvtotal_tax_value = 0;
                $appvtotal_pro_quantity = 0;
                if($appvbooking->direct_booking == 1){
                    if($appvbooking->without_variants == 1){
                        $appvpriceData = array();
                        foreach($appvvariant_array as $appvkey => $appvvalue){
                            $appvproduct = Products::where('id', $appvbooking->product_id)->first();
                            $appvbasic = $appvpdata->price;
                            if($appvproduct->tax!==''){
                                $appvtax = $appvproduct->tax;
                                $appvtax_value = round(($appvbasic)*(int)$appvtax/100);
                            }
                            $appvtotal_amount = $appvbasic+$appvtax_value;
                            $appvtotal_basic = $appvbasic;
                            $appvtotal_tax_value = $appvtax_value;
                            $appvtotal_pro_quantity = $appvvalue->quantity;
                            $appvppv = '0';
                            $appvinfo = array();
                            $appvinfo[] = array(
                                "basic" => $appvbasic,
                                "tax" => $appvtax_value,
                                "total_amount" => $appvtotal_amount,
                            );
                            $appvpriceData[] = getInstitutionalPrice($appvproduct->id,institutional('id'),$appvbooking->hub_id,$appvvalue->id, false);
                            $appvvariant_length = '';
                            $appvvariant_length_unit = '';
                            $appvvariant_weight = '';
                            $appvvariant_weight_unit = '';
                            if($appvproduct->show_weight_length == 1){
                                $pvar = DB::table('product_variants')->where('id', $appvvalue->id)->first();
                                if($pvar !== null) {
                                    $size = DB::table('size')->where('id', $pvar->variant_title)->first();
                                    $sw = DB::table('size_weight')->where('size_id',$size->id)->first();
                                    if($sw){
                                        $variant_length = $sw->length;
                                        $variant_length_unit = getUnitSymbol($sw->lunit);
                                        $variant_weight = $sw->weight;
                                        $variant_weight_unit = getUnitSymbol($sw->wunit);
                                    }
                                }

                                /*$appvsize = Size::where('id', $appvvalue->variant_title_id)->first();
                                $appvsw = SizeWeight::where('size_id',$size->id)->first();
                                if($appvsw){
                                    $appvvariant_length = $appvsw->length;
                                    $appvvariant_length_unit = getUnitSymbol($appvsw->lunit);
                                    $appvvariant_weight = $appvsw->weight;
                                    $appvvariant_weight_unit = getUnitSymbol($appvsw->wunit);
                                }*/
                            }
                            $appvretVar[] = array(
                                "id" => $appvvalue->id,
                                "id" => $appvvalue->id,
                                "title" => $appvproduct->title,
                                "price" => $appvbasic,
                                "quantity" => $appvvalue->quantity,
                                "unit" => $appvvalue->unit,
                                "price_detail" => $appvpriceData,
                                "variant_length" => $appvvariant_length,
                                "variant_length_unit" => $appvvariant_length_unit,
                                "variant_weight" => $appvvariant_weight,
                                "variant_weight_unit" => $appvvariant_weight_unit
                            );
                        }
                    }else{
                        foreach($appvvariant_array as $appvkey => $appvvalue){
                            if(isset($appvvalue->id)){
                                $appvinfo = array();
                                $appvpriceData = array();
                                $appvpvar = ProductVariant::where('id', $appvvalue->id)->first();
                                $appvsize_name = Size::where('id', $appvpvar->variant_title)->first();
                                $appvprices = MultipleSize::where('size_id', $appvsize_name->price)->first();
                                $appvproduct = Products::where('id', $appvbooking->product_id)->first();
                                $appvtotal_amount ='';
                                $appvtotal_quantity = 0;
                                $appvproduct_quantity = $appvproduct->quantity;
                                $appvtax_value='';
                                $appvquantity = '';
                                $appvvar_price=0;
                                $appvprice = 0;
                                if($appvprices) {
                                    $appvvar_price = $appvprices->price;
                                    $appvprice = $appvprices->price;
                                }
                                $appvaction = 0;
                                $appvbasic = $appvpdata->price;
                                $appvloading =$appvpdata->loading;;
                                if($appvproduct->tax!==''){
                                    $appvtax = $product->tax;
                                    $appvtax_value = round(($appvbasic+$appvvar_price)*(int)$appvtax/100);
                                }
                                $total_amount = round($appvbasic+$appvvar_price+$appvtax_value+$appvloading);
                                if($appvvalue->quantity !== ''){
                                    $appvtotal_basic = round(($appvtotal_basic + $appvbasic) * $appvvalue->quantity);
                                    $appvtotal_size_diff = round(($appvtotal_size_diff + $appvvar_price) * $appvvalue->quantity);
                                    $appvtotal_loading = round(($appvtotal_loading + $appvloading) * $appvvalue->quantity);
                                    $appvtotal_tax_value = round(($appvtotal_tax_value + $appvtax_value) * $appvvalue->quantity);
                                }else{
                                    $appvtotal_basic = round($appvtotal_basic + $appvbasic);
                                    $appvtotal_size_diff = round($appvtotal_size_diff + $appvvar_price);
                                    $appvtotal_loading = round($appvtotal_loading + $appvloading);
                                    $appvtotal_tax_value = round($appvtotal_tax_value + $appvtax_value);
                                }
                                if($appvvalue->quantity !== ''){
                                    $appvtotal_pro_quantity = $appvtotal_pro_quantity + $appvvalue->quantity;
                                }
                                $appvpriceData[] = getInstitutionalPrice($appvproduct->id,institutional('id'),$appvbooking->hub_id,$appvvalue->id, false);
                                $appvvariant_length = '';
                                $appvvariant_length_unit = '';
                                $appvvariant_weight = '';
                                $appvvariant_weight_unit = '';
                                if($appvproduct->show_weight_length == 1){
                                    $pvar = DB::table('product_variants')->where('id', $appvproduct->id)->first();
                                    if($pvar !== null) {
                                        $size = DB::table('size')->where('id', $pvar->variant_title)->first();
                                        $sw = DB::table('size_weight')->where('size_id',$size->id)->first();
                                        if($sw){
                                            $variant_length = $sw->length;
                                            $variant_length_unit = getUnitSymbol($sw->lunit);
                                            $variant_weight = $sw->weight;
                                            $variant_weight_unit = getUnitSymbol($sw->wunit);
                                        }
                                    }
                                    $appvsize = Size::where('id', $appvvalue->variant_title_id)->first();
                                    $appvsw = SizeWeight::where('size_id',$size->id)->first();
                                    if($appvsw){
                                        $appvvariant_length = $appvsw->length;
                                        $appvvariant_length_unit = getUnitSymbol($appvsw->lunit);
                                        $appvvariant_weight = $appvsw->weight;
                                        $appvvariant_weight_unit = getUnitSymbol($appvsw->wunit);
                                    }
                                }
                                $appvretVar[] = array(
                                    "id" => $appvvalue->id,
                                    "title" => $appvsize_name->name,
                                    "price" => $appvbasic,
                                    "quantity" => $appvvalue->quantity,
                                    "unit" => $appvvalue->unit,
                                    "price_detail" => $appvpriceData,
                                    "variant_length" => $appvvariant_length,
                                    "variant_length_unit" => $appvvariant_length_unit,
                                    "variant_weight" => $appvvariant_weight,
                                    "variant_weight_unit" => $appvvariant_weight_unit
                                );
                            }
                        }
                    }
                }
                else{
                    $appvrfqData = $appvbooking;
                    if(isset($appvrfqData->variants)){
                        $appvvar = json_decode($appvrfqData->variants);
                        $appvpid = $appvrfqData->product_id;
                        foreach($appvvar as $appvkey => $appvvalue){
                            if(isset($appvvalue->id)){
                                $appvinfo = array();
                                $appvprice = 0;
                                $appvvar_price=0;
                                $appvsize_name = '';
                                $appvproduct = Products::where('id', $appvbooking->product_id)->first();
                                $appvtotal_amount ='';
                                $appvtotal_quantity = 0;
                                $appvproduct_quantity = $appvproduct->quantity;
                                $appvtax_value='';
                                $appvquantity = '';
                                $appvuni = $appvvalue->unit;
                                $appvbasic = floatval($appvvalue->price);
                                if(isset($appvvalue->var_price)){
                                    $appvvar_price = floatval($appvvalue->var_price);
                                }
                                $appvloading =$appvpdata->loading;
                                if($appvproduct->tax!==''){
                                    $appvtax = $appvproduct->tax;
                                    $appvtax_value = round(($appvbasic+$appvvar_price+$appvloading)*(int)$appvtax/100);
                                }
                                $appvtotal_amount = ($appvbasic+$appvvar_price+$appvloading)+$appvtax_value;
                                if($appvvalue->quantity == ''){
                                    continue;
                                }
                                if($appvvalue->quantity !== ''){
                                    $appvtotal_basic = round($appvtotal_basic + ($appvbasic * $appvvalue->quantity));
                                    $appvtotal_loading = round($appvtotal_loading + ($appvloading * $appvvalue->quantity));
                                    $appvtotal_tax_value = round($appvtotal_tax_value + ($appvtax_value * $appvvalue->quantity));
                                }else{
                                    $appvtotal_basic = round($appvtotal_basic + $appvbasic);
                                    $appvtotal_loading = round($appvtotal_loading + $appvloading);
                                    $appvtotal_tax_value = round($appvtotal_tax_value + $appvtax_value);
                                }
                                if($appvvalue->quantity !== ''){
                                    $appvtotal_pro_quantity = $appvtotal_pro_quantity + $appvvalue->quantity;
                                }
                                $appvinfo[] = array(
                                    "timer" => 0,
                                    "price" => "".$appvbasic,
                                    "loading" => $appvloading,
                                    "tax" => "".$appvtax_value,
                                    "total" => "".$appvtotal_amount,
                                    "var" => "".$appvvar_price,
                                );
                                $appvretVar[] = array(
                                    "id" => $appvvalue->id,
                                    "title" => $appvvalue->title,
                                    "price" => $appvbasic,
                                    "quantity" => $appvvalue->quantity,
                                    "unit" => $appvvalue->unit,
                                    "price_detail" => $appvinfo
                                );
                            }
                        }
                    }
                }
                $appvbooked = 0;
                BookingCount::where('customer_id',institutional('id'))
                    ->update(['booking_counter' => 0]);
                $appvpp = '0';
                $appvstatus = '';
                if($appvbooking->status == 0){
                    $appvstatus = 'Unapproved';
                }elseif($appvbooking->status == 1){
                    $appvstatus = 'Approved';
                }
                $appvcredit_charge = array();
                if($appvbooking->credit_charge_id !== 0 || $appvbooking->credit_charge_id !== '' || $appvbooking->credit_charge_id !== null){
                    $appvcredit = CreditCharges::where('id', $appvbooking->credit_charge_id)->first();
                    if($appvcredit !== null){
                        $appvcredit_charge[] = array(
                            'id'=>$appvcredit->id,
                            'days'=>$appvcredit->days,
                            'amount'=>$appvcredit->amount,
                        );
                    }
                }
                $appvret[] = array(
                    'id' => $appvbooking->id,
                    'p_id' => $appvbooking->product_id,
                    'c_id' => $appvproduct->category,
                    'location' => $appvbooking->location,
                    'payment_option' => $appvbooking->payment_option,
                    'image' => url('assets/products/'.image_order($appvproduct->images)),
                    'status' => 'Completed',
                    'title' => $appvproduct->title,
                    'unit' => $appvuni,
                    'method' => $appvbooking->method,
                    'total_basic' => $appvtotal_basic,
                    'total_loading' => $appvtotal_loading,
                    'total_tax_value' => $appvtotal_tax_value,
                    'total_quantity' => $appvtotal_pro_quantity,
                    'variant' => $appvretVar,
                    'booking_date' => date("d-m-Y", strtotime($appvbooking->created_at)),
                    'credit_charge' => $appvcredit_charge
                );
            }
        }
        /*End Completed Booking*/
        /*Cancelled Booking*/
        $canlbookings = DB::table('rfq_booking')->where('user_id', institutional('id'))->where('status', 2)->orderBy('id', 'DESC')->get();

        foreach ($canlbookings as $canlbooking){
            $canlproduct = Products::where('id', $canlbooking->product_id)->first();
            $canlproduct_variants = ProductVariant::where('product_id', $canlbooking->product_id)->first();
            $canlph = \App\PhRelations::where('products', $canlbooking->product_id)->first();
            $canldata = json_decode($canlph->hubs);
            $canlpids = array_pluck($canldata, 'hub');
            $canlkey = array_search($canlbooking->hub_id, $canlpids);
            $canlpdata = $canldata[$canlkey];
            if($canlproduct !== null){
                $canlvariant_array = (array)json_decode($canlbooking->variants);
                foreach($canlvariant_array as $canlkey => $canlv){
                    $canlvariant[] = (array)$canlv;
                }
                $canlretVar = array();
                $canlunit_array = array();
                $canltotal_basic_price = array();
                $canluni = '';
                $canltotal_basic = 0;
                $canltotal_size_diff = 0;
                $canltotal_loading = 0;
                $canltotal_tax_value = 0;
                $canltotal_pro_quantity = 0;
                if($canlbooking->direct_booking == 1){
                    if($canlbooking->without_variants == 1){
                        $canlpriceData = array();
                        foreach($canlvariant_array as $canlkey => $canlvalue){
                            $canlproduct = Products::where('id', $canlbooking->product_id)->first();
                            $canlbasic = $canlpdata->price;
                            if($canlproduct->tax!==''){
                                $canltax = $canlproduct->tax;
                                $canltax_value = round(($canlbasic)*(int)$canltax/100);
                            }
                            $canltotal_amount = $canlbasic+$canltax_value;
                            $canltotal_basic = $canlbasic;
                            $canltotal_tax_value = $canltax_value;
                            $canltotal_pro_quantity = $canlvalue->quantity;
                            $canlppv = '0';
                            $canlinfo = array();
                            $canlinfo[] = array(
                                "basic" => $canlbasic,
                                "tax" => $canltax_value,
                                "total_amount" => $canltotal_amount,
                            );
                            $canlpriceData[] = getInstitutionalPrice($canlproduct->id,institutional('id'),$canlbooking->hub_id,$canlvalue->id, false);
                            $canlvariant_length = '';
                            $canlvariant_length_unit = '';
                            $canlvariant_weight = '';
                            $canlvariant_weight_unit = '';
                            if($canlproduct->show_weight_length == 1){
                                $pvar = DB::table('product_variants')->where('id', $canlvalue->id)->first();
                                if($pvar !== null) {
                                    $size = DB::table('size')->where('id', $pvar->variant_title)->first();
                                    $sw = DB::table('size_weight')->where('size_id',$size->id)->first();
                                    if($sw){
                                        $variant_length = $sw->length;
                                        $variant_length_unit = getUnitSymbol($sw->lunit);
                                        $variant_weight = $sw->weight;
                                        $variant_weight_unit = getUnitSymbol($sw->wunit);
                                    }
                                }
                                /*$canlsize = Size::where('id', $canlvalue->variant_title_id)->first();
                                $canlsw = SizeWeight::where('size_id',$size->id)->first();
                                if($canlsw){
                                    $canlvariant_length = $canlsw->length;
                                    $canlvariant_length_unit = getUnitSymbol($canlsw->lunit);
                                    $canlvariant_weight = $canlsw->weight;
                                    $canlvariant_weight_unit = getUnitSymbol($canlsw->wunit);
                                }*/
                            }
                            $canlretVar[] = array(
                                "id" => $canlvalue->id,
                                "id" => $canlvalue->id,
                                "title" => $canlproduct->title,
                                "price" => $canlbasic,
                                "quantity" => $canlvalue->quantity,
                                "unit" => $canlvalue->unit,
                                "price_detail" => $canlpriceData,
                                "variant_length" => $canlvariant_length,
                                "variant_length_unit" => $canlvariant_length_unit,
                                "variant_weight" => $canlvariant_weight,
                                "variant_weight_unit" => $canlvariant_weight_unit
                            );
                        }
                    }else{
                        foreach($canlvariant_array as $canlkey => $canlvalue){
                            if(isset($canlvalue->id)){
                                $canlinfo = array();
                                $canlpriceData = array();
                                $canlpvar = ProductVariant::where('id', $canlvalue->id)->first();
                                $canlsize_name = Size::where('id', $canlpvar->variant_title)->first();
                                $canlprices = MultipleSize::where('size_id', $canlsize_name->price)->first();
                                $canlproduct = Products::where('id', $canlbooking->product_id)->first();
                                $canltotal_amount ='';
                                $canltotal_quantity = 0;
                                $canlproduct_quantity = $canlproduct->quantity;
                                $canltax_value='';
                                $canlquantity = '';
                                $canlvar_price=0;
                                $canlprice = 0;
                                if($canlprices) {
                                    $canlvar_price = $canlprices->price;
                                    $canlprice = $canlprices->price;
                                }
                                $canlaction = 0;
                                $canlbasic = $canlpdata->price;
                                $canlloading =$canlpdata->loading;;
                                if($canlproduct->tax!==''){
                                    $canltax = $product->tax;
                                    $canltax_value = round(($canlbasic+$canlvar_price)*(int)$canltax/100);
                                }
                                $total_amount = round($canlbasic+$canlvar_price+$canltax_value+$canlloading);
                                if($canlvalue->quantity !== ''){
                                    $canltotal_basic = round(($canltotal_basic + $canlbasic) * $canlvalue->quantity);
                                    $canltotal_size_diff = round(($canltotal_size_diff + $canlvar_price) * $canlvalue->quantity);
                                    $canltotal_loading = round(($canltotal_loading + $canlloading) * $canlvalue->quantity);
                                    $canltotal_tax_value = round(($canltotal_tax_value + $canltax_value) * $canlvalue->quantity);
                                }else{
                                    $canltotal_basic = round($canltotal_basic + $canlbasic);
                                    $canltotal_size_diff = round($canltotal_size_diff + $canlvar_price);
                                    $canltotal_loading = round($canltotal_loading + $canlloading);
                                    $canltotal_tax_value = round($canltotal_tax_value + $canltax_value);
                                }
                                if($canlvalue->quantity !== ''){
                                    $canltotal_pro_quantity = $canltotal_pro_quantity + $canlvalue->quantity;
                                }
                                $canlpriceData[] = getInstitutionalPrice($canlproduct->id,institutional('id'),$canlbooking->hub_id,$canlvalue->id, false);
                                $canlvariant_length = '';
                                $canlvariant_length_unit = '';
                                $canlvariant_weight = '';
                                $canlvariant_weight_unit = '';
                                if($canlproduct->show_weight_length == 1){
                                    $canlsize = Size::where('id', $canlvalue->variant_title_id)->first();
                                    $canlsw = SizeWeight::where('size_id',$size->id)->first();
                                    if($canlsw){
                                        $canlvariant_length = $canlsw->length;
                                        $canlvariant_length_unit = getUnitSymbol($canlsw->lunit);
                                        $canlvariant_weight = $canlsw->weight;
                                        $canlvariant_weight_unit = getUnitSymbol($canlsw->wunit);
                                    }
                                }
                                $canlretVar[] = array(
                                    "id" => $canlvalue->id,
                                    "title" => $canlsize_name->name,
                                    "price" => $canlbasic,
                                    "quantity" => $canlvalue->quantity,
                                    "unit" => $canlvalue->unit,
                                    "price_detail" => $canlpriceData,
                                    "variant_length" => $canlvariant_length,
                                    "variant_length_unit" => $canlvariant_length_unit,
                                    "variant_weight" => $canlvariant_weight,
                                    "variant_weight_unit" => $canlvariant_weight_unit
                                );
                            }
                        }
                    }
                }
                else{
                    $canlrfqData = $canlbooking;
                    if(isset($canlrfqData->variants)){
                        $canlvar = json_decode($canlrfqData->variants);
                        $canlpid = $canlrfqData->product_id;
                        foreach($canlvar as $canlkey => $canlvalue){
                            if(isset($canlvalue->id)){
                                $canlinfo = array();
                                $canlprice = 0;
                                $canlvar_price=0;
                                $canlsize_name = '';
                                $canlproduct = Products::where('id', $canlbooking->product_id)->first();
                                $canltotal_amount ='';
                                $canltotal_quantity = 0;
                                $canlproduct_quantity = $canlproduct->quantity;
                                $canltax_value='';
                                $canlquantity = '';
                                $canluni = $canlvalue->unit;
                                $canlbasic = floatval($canlvalue->price);
                                if(isset($canlvalue->var_price)){
                                    $canlvar_price = floatval($canlvalue->var_price);
                                }
                                $canlloading =$canlpdata->loading;
                                if($canlproduct->tax!==''){
                                    $canltax = $canlproduct->tax;
                                    $canltax_value = round(($canlbasic+$canlvar_price+$canlloading)*(int)$canltax/100);
                                }
                                $canltotal_amount = ($canlbasic+$canlvar_price+$canlloading)+$canltax_value;
                                if($canlvalue->quantity == ''){
                                    continue;
                                }
                                if($canlvalue->quantity !== ''){
                                    $canltotal_basic = round($canltotal_basic + ($canlbasic * $canlvalue->quantity));
                                    $canltotal_loading = round($canltotal_loading + ($canlloading * $canlvalue->quantity));
                                    $canltotal_tax_value = round($canltotal_tax_value + ($canltax_value * $canlvalue->quantity));
                                }else{
                                    $canltotal_basic = round($canltotal_basic + $canlbasic);
                                    $canltotal_loading = round($canltotal_loading + $canlloading);
                                    $canltotal_tax_value = round($canltotal_tax_value + $canltax_value);
                                }
                                if($canlvalue->quantity !== ''){
                                    $canltotal_pro_quantity = $canltotal_pro_quantity + $canlvalue->quantity;
                                }
                                $canlinfo[] = array(
                                    "timer" => 0,
                                    "price" => "".$canlbasic,
                                    "loading" => $canlloading,
                                    "tax" => "".$canltax_value,
                                    "total" => "".$canltotal_amount,
                                    "var" => "".$canlvar_price,
                                );
                                $canlretVar[] = array(
                                    "id" => $canlvalue->id,
                                    "title" => $canlvalue->title,
                                    "price" => $canlbasic,
                                    "quantity" => $canlvalue->quantity,
                                    "unit" => $canlvalue->unit,
                                    "price_detail" => $canlinfo
                                );
                            }
                        }
                    }
                }
                $canlbooked = 0;
                BookingCount::where('customer_id',institutional('id'))
                    ->update(['booking_counter' => 0]);
                $canlpp = '0';
                $canlstatus = '';
                if($canlbooking->status == 0){
                    $canlstatus = 'Unapproved';
                }elseif($canlbooking->status == 1){
                    $canlstatus = 'Approved';
                }
                $canlcredit_charge = array();
                if($canlbooking->credit_charge_id !== 0 || $canlbooking->credit_charge_id !== '' || $canlbooking->credit_charge_id !== null){
                    $canlcredit = CreditCharges::where('id', $canlbooking->credit_charge_id)->first();
                    if($canlcredit !== null){
                        $canlcredit_charge[] = array(
                            'id'=>$canlcredit->id,
                            'days'=>$canlcredit->days,
                            'amount'=>$canlcredit->amount,
                        );
                    }
                }
                $canlret[] = array(
                    'id' => $canlbooking->id,
                    'p_id' => $canlbooking->product_id,
                    'c_id' => $canlproduct->category,
                    'location' => $canlbooking->location,
                    'payment_option' => $canlbooking->payment_option,
                    'image' => url('assets/products/'.image_order($canlproduct->images)),
                    'status' => 'Cancelled',
                    'title' => $canlproduct->title,
                    'unit' => $canluni,
                    'method' => $canlbooking->method,
                    'total_basic' => $canltotal_basic,
                    'total_loading' => $canltotal_loading,
                    'total_tax_value' => $canltotal_tax_value,
                    'total_quantity' => $canltotal_pro_quantity,
                    'variant' => $canlretVar,
                    'booking_date' => date("d-m-Y", strtotime($canlbooking->created_at)),
                    'credit_charge' => $canlcredit_charge
                );
            }
        }
        /*End Cancelled Booking*/

        return view('institutional/my-bookings')->with(compact('header', 'footer', 'tp', 'bookings','ret','appvret','canlret'));
    }

    public function myQuotations(){
        if(institutional('user_type') !== 'institutional'){
            abort(401);
        }
        $tp = url("/assets/institutional/");
        $title = 'My Quatation';
        $header = $this->header($title, $tp);
        $footer = $this->footer($tp);
        /*Live Quatation*/
        $exbookings= array();
        $bookings= array();
        $quotation = RequestQuotation::where('user_id', institutional('id'))->where(function($q){
            $q->where('validity', '>=', date('Y-m-d H:i:s'));
            $q->orWhere('validity', null);
        })->orderBy('id', 'DESC')->get();
        RfqCount::where('customer_id',institutional('id'))
            ->update(['rfq_counter' => 0]);
        foreach ($quotation as $quote) {
            $cat_name = Category::where('id', $quote->cat_id)->first();
            $products = Products::where('id', $quote->brand_id)->first();
            $product_quantity = 0;
            if ($products !== null) {
                $product_quantity = $products->quantity;
            }
            $product_id = '';
            if ($products !== null) {
                $product_id = $products->id;
            }
            $total_quantity = 0;
            $retVar = array();
            $info = array();
            $jdata = json_decode(str_replace("\\", "", $quote->variants));
            $variant = (array)json_decode($quote->variants);

            $brand_ids = str_replace(' ', '', $quote->brand_id);
            $bids = explode(',', $brand_ids);
            $brand_names = array_pluck(Brand::whereIn('id', $bids)->get(), 'name');
            if ($quote->method == 'bysize') {
                $variant = json_decode($quote->variants);
                foreach ($bids as $bi) {
                    foreach ($variant->$bi as $vr) {
                        $total_quantity = $total_quantity + $vr->quantity;
                        $var_p = 0;
                        $basic = 0;
                        if ($products !== null) {
                            $basic = $products->purchase_price;
                        }
                        $loading = 0;
                        $tax_value = '';
                        $total_amount = '';
                        $tax = '';
                        if ($products !== null) {
                            if ($products->tax !== '') {
                                $tax = $products->tax;
                            }
                        }
                        $title = '';
                        $var_p = '';
                        $tax_value = '';
                        $total_amount = '';
                        if ($quote->method == 'bybasic') {
                            $pr = Products::where('id', $vr->id)->first();
                            if ($pr) {
                                $title = $pr->title;
                            }
                            $var_p = 0;
                            $tax_value = round(($basic + $var_p) * (int)$tax / 100);
                            $total_amount = $basic + $var_p + $tax_value + $loading;
                        } else {
                            $pvar = ProductVariant::where('id', $vr->id)->first();
                            if ($pvar !== null) {
                                $title = $pvar->variant_title;
                                $prices = MultipleSize::where('size_id', $pvar->price)->first();
                                $var_p = 0;
                                if ($prices) {
                                    $var_p = $prices->price;
                                }
                                $tax_value = round((floatval($basic) + floatval($var_p)) * floatval($tax) / 100);
                                $total_amount = floatval($basic) + floatval($var_p) + floatval($tax_value) + floatval($loading);
                            }
                        }
                        $price_detail = '';
                        if ($quote->method == 'bybasic') {
                            $info = array(
                                'price' => $vr->basic_price,
                                'loading' => "",
                                'tax' => "",
                                'var' => $vr->var_price,
                                'total' => ""
                            );
                            $price_detail = $info;
                        } else {
                            $info = array(
                                'price' => isset($vr->price_detail->price) ? $vr->price_detail->price : 'Waiting for update',
                                'loading' => isset($vr->price_detail->loading) ? $vr->price_detail->loading : 0,
                                'tax' => isset($vr->price_detail->tax) ? $vr->price_detail->tax : 0,
                                'var' => isset($vr->price_detail->var) ? $vr->price_detail->var : 0,
                                'total' => isset($vr->price_detail->total) ? $vr->price_detail->total : 0,
                            );
                            $price_detail = $info;
                        }
                        $var_price = '';
                        if (isset($vr->var_price)) {
                            $var_price = $vr->var_price;
                        }
                        $retVar[] = array(
                            "id" => $vr->id,
                            "title" => $vr->title,
                            "quantity" => $vr->quantity,
                            "price" => $var_price,
                            "price_detail" => $price_detail,
                        );
                    }
                }
            } else {
                foreach ($variant as $vr) {
                    $total_quantity = $total_quantity + $vr->quantity;
                    $var_p = 0;
                    $basic = '';
                    if ($products !== null) {
                        $basic = $products->purchase_price;
                    }
                    $loading = 0;
                    $tax_value = '';
                    $total_amount = '';
                    $tax = '';
                    if ($products !== null) {
                        if ($products->tax !== '') {
                            $tax = $products->tax;
                        }
                    }
                    $title = '';
                    $var_p = '';
                    $tax_value = '';
                    $total_amount = '';
                    if ($quote->method == 'bybasic') {

                        $pr = Products::where('id',$vr->id)->first();
                        if ($pr) {
                            $title = $pr->title;
                        }
                        $var_p = 0;
                        $tax_value = round(($basic + $var_p) * (int)$tax / 100);
                        $total_amount = $basic + $var_p + $tax_value + $loading;
                    } else {
                        $pvar = ProductVariant::where('id', $vr->id)->first();
                        if ($pvar !== null) {
                            $title = $pvar->variant_title;
                            $var_p = $pvar->price;
                            $tax_value = round(($basic + $var_p) * (int)$tax / 100);
                            $total_amount = $basic + $var_p + $tax_value + $loading;
                        }
                    }

                    $price_detail = getInstitutionalPrice($products->id,institutional('id'), $quote->hub_id, $vr->id, false);
                    if ($quote->method == 'bybasic') {
                        $info = array(
                            'price' => $vr->basic_price,
                            'loading' => "",
                            'tax' => "",
                            'var' => $vr->var_price,
                            'total' => ""
                        );
                        $price_detail = $info;
                    }
                    $var_price = '';
                    if (isset($vr->var_price)) {
                        $var_price = $vr->var_price;
                    }

                    $retVar[] = array(
                        "id" => $vr->id,
                        "title" => $title,
                        "quantity" => $vr->quantity,
                        "price" => $var_price,
                        "price_detail" => $price_detail,
                    );
                }
            }
            $st = 'Pending';
            $pvalidity = 0;
            if ($quote->status > intval(0)) {
                $st = 'Approved';
            }
            if ($quote->validity !== null) {
                date_default_timezone_set('Asia/Kolkata');
                $pvalidity = strtotime($quote->validity) - strtotime(date('Y-m-d H:i:s'));
                if (new \DateTime() > new \DateTime($quote->validity)) {
                    $st = 'Expired';
                }
            }
            $section = 'Live';
            if ($st == 'Expired') {
                $section = 'Expired';
            }
            $bookings[] = array(
                'id' => $quote->id,
                'product_name' => isset($products->title) ? $products->title : '',
                'product_id' => $product_id,
                'cat_name' => $cat_name->name,
                'cat_id' => $cat_name->id,
                'brand_id' => implode(', ', $brand_names),
                'status' => $st,
                'section' => $section,
                'payment_option' => $quote->payment_option,
                'other_option' => $quote->other_option,
                'location' => $quote->location,
                'method' => $quote->method,
                'variant' => $retVar,
                'booking_date' => date("d, M Y H:i", strtotime($quote->created_at)),
                'timer' => $pvalidity,
                'stock' => $product_quantity - $total_quantity
            );
            /* End Live Quatation*/
        }

        /* End Expired Quatation*/
        $exquotation = RequestQuotation::where('user_id',institutional('id'))->where('validity', '<', date('Y-m-d H:i:s'))->orderBy('id', 'DESC')->get();
        foreach ($exquotation as $exquote) {
            $excat_name = Category::where('id', $exquote->cat_id)->first();
            $exproducts = Products::where('id', $exquote->product_id)->first();
            $exretVar = array();
            $jdata = json_decode(str_replace("\\", "", $exquote->variants));
            $exvariant = (array)json_decode($exquote->variants);
            $exbrand_ids = str_replace(' ', '', $exquote->brand_id);
            $exbids = explode(',', $exbrand_ids);
            if ($exquote->method == 'bybasic') {
                foreach ($exvariant as $exvr) {
                    $expr = Products::where('id', $exvr->id)->first();
                    if ($expr !== null) {
                        $extitle = $expr->title;
                    }
                    $exretVar[] = array(
                        "id" => $exvr->id,
                        "title" => $extitle,
                        "quantity" => $exvr->quantity,
                        "price" => $exvr->var_price
                    );
                }
            } else {
                $exvariant = json_decode($exquote->variants);
                foreach ($exbids as $exbi) {
                    foreach ($exvariant->$exbi as $exvr) {
                        $expvar = ProductVariant::where('id', $exvr->id)->first();
                        if ($expvar) {
                            $extitle = $expvar->variant_title;
                        }
                        $exretVar[] = array(
                            "id" => $exvr->id,
                            "title" => $extitle,
                            "quantity" => $exvr->quantity,
                            "price" => $exvr->var_price
                        );
                    }
                }
            }
            $exbrand_names = array_pluck(Brand::whereIn('id', $exbids)->get(), 'name');
            $exst = 'Pending';
            if ($exquote->status > intval(0)) {
                $exst = 'Approved';
            }
            if ($exquote->validity !== null) {
                if (new \DateTime() > new \DateTime($exquote->validity)) {
                    $exst = 'Expired';
                }
            }
            $exsection = 'Live';
            if ($exst == 'Expired') {
                $exsection = 'Expired';
            }
            $exbookings[] = array(
                'id' => $exquote->id,
                'product_id' => $exquote->product_id,
                'cat_id' => $exquote->cat_id,
                'cat_name' => $excat_name->name,
                'brand_id' => implode(', ', $exbrand_names),
                'status' => $exst,
                'section' => $exsection,
                'payment_option' => $exquote->payment_option,
                'other_option' => $exquote->other_option,
                'location' => $exquote->location,
                'method' => $exquote->method,
                'variant' => $exretVar,
                'booking_date' => date("d, M Y", strtotime($exquote->created_at)),
                'stock' => $exproducts->quantity
            );
        }
        /* End Expired Quatation*/
        return view('institutional/my-quotation')->with(compact('header', 'footer', 'tp','bookings','exbookings'));
    }

    public function myQuotationsDetail(Request $request){
        if(institutional('user_type') !== 'institutional'){
            abort(401);
        }
        $tp = url("/assets/institutional/");
        $title = 'My Quatation Detail';
        $header = $this->header($title, $tp);
        $footer = $this->footer($tp);
        $quote_id = isset($_GET['id'])?$_GET['id']:false;
        $met = isset($_GET['method'])?$_GET['method']:false;
        $category = isset($_GET['cat_id'])?$_GET['cat_id']:false;
        $product_id = isset($_GET['product_id'])?$_GET['product_id']:false;
        $product = Products::where('id',$product_id)->first();
        $related_products = getRelatedProducts($category,$product_id);
        if($quote_id!==null){
            $quotation = RequestQuotation::where('id',$quote_id)->first();
            if($quotation===null){
                return response()->json(['message' => 'id not found'], 401);
            }
            $brand_ids = str_replace(' ', '', $quotation->brand_id);
            $bids = explode(',', $brand_ids);
            $brand_names = array_pluck(Brand::whereIn('id', $bids)->get(), 'name');
            $cat = Category::where('id', '=', $quotation->cat_id)->first();
            $wunits = Unit::whereIn('id', explode(',', $cat->weight_unit))->get();
            $var_ids = array_pluck(ProductVariant::where('product_id', $quotation->product_id)
                ->join('size','size.id','=','product_variants.variant_title')
                ->select(['product_variants.*'])
                ->orderBy('size.priority')
                ->get(), 'id');
            $cat_name = Category::where('id', $quotation->cat_id)->first();
            $quantity = 0;
            $ii = 0;
            $ret = array();
            foreach($bids as $key=>$bid){
                $brand_name = DB::table('products')->where('id', $bid)->first();
                $ph = \App\PhRelations::where('products', $bid)->first();
                $data = json_decode($ph->hubs);
                $pids = array_pluck($data, 'hub');
                $keyx = array_search($quotation->hub_id, $pids);
                $pdata = $data[$keyx];
                $retVar = array();
                $quantity = 0;
                if($met =='bysize'){
                    $retVar = array();
                    $variant = json_decode($quotation->variants);
                    $product_quantity = 0;
                    $total_quantity = 0;
                    foreach($var_ids as $var){
                        $info = array();
                        $var_price=0;
                        $basic_price=0;
                        $vp = 0;
                        $sizename = '';
                        $sizeid = '';
                        $pvar = DB::table('product_variants')->where('id', $var)->first();
                        if($pvar !== null){
                            $size_name = DB::table('size')->where('id', $pvar->variant_title)->first();
                            $prices = DB::table('multiple_size')->where('id', $pvar->price)->first();
                            if($prices !== null) {
                                $vp = $var_price = is_numeric($prices->price) ? $prices->price : 0;
                            }
                            $sizename = $size_name->name;
                            $sizeid = $size_name->id;
                        }
                        $product = DB::table('products')->where('id', $quotation->product_id)->first();
                        $total_amount =0;
                        $total_quantity = 0;
                        $product_quantity = $product->quantity;
                        $tax_value=0;
                        $quantity = 0;
                        $action = 0;
                        $basic = $pdata->price;
                        $loading =$pdata->loading;
                        $tax = 0;
                        if($product->tax!==''){
                            $tax = $product->tax;
                            $tax_value = round(($basic+$var_price+$loading)*(int)$tax/100);
                        }
                        $total_amount = $basic+$var_price+$loading;
                        $tamtdata = round((($total_amount)*(int)$tax/100));
                        $tamt = 0;
                        $new_price = 0;
                        $unit = '';
                        $bprice = round($total_amount+$tamtdata);
                        foreach($variant->$bid as $var_id){
                            $size_diff = $vp;
                            if($quotation->action){
                                $var_price = $basic + $quotation->price_diff;
                                $tamtdata = round(($var_price + $size_diff + $loading)*(int)$tax/100);
                                $bprice = round(($var_price + $size_diff + $loading)+$tamtdata);
                            }else{
                                $var_price = $basic - $quotation->price_diff;
                                $tamtdata = round(($var_price + $size_diff + $loading)*(int)$tax/100);
                                $bprice = round(($var_price + $size_diff + $loading)+$tamtdata);
                            }
                            if($var == $var_id->id){
                                $quantity = $var_id->quantity;
                                $var_price = is_numeric($var_id->var_price) ? $var_id->var_price : 0;;
                                $size_diff = $var_id->price_detail->var;
                                if($quantity > 0){
                                    $action = 1;
                                }
                                $total_quantity = $total_quantity+$var_id->quantity;
                                $new_price = $var_price;
                                if($product->tax!==''){
                                    $tax = $product->tax;
                                    $tamtdata = round(((floatval($var_price)+floatval($size_diff))+floatval($pdata->loading))*(int)$tax/100);
                                    if($quantity > 0){
                                        $tamt = round((((floatval($var_price)+floatval($size_diff))+floatval($pdata->loading)+$tamtdata)*$var_id->quantity));
                                    }else{
                                        $tamt = round((((floatval($var_price)+floatval($size_diff))+floatval($pdata->loading)+$tamtdata)));
                                    }
                                    $bprice = round(((floatval($var_price)+floatval($size_diff))+floatval($pdata->loading)+$tamtdata));
                                }
                                $unit = $var_id->unit;
                                continue;
                            }
                            $variant_length = '';
                            $variant_length_unit = '';
                            $variant_weight = '';
                            $variant_weight_unit = '';
                            if($product->show_weight_length == 1){
                                $sw = DB::table('size_weight')->where('size_id',$size_name->id)->first();
                                if($sw){
                                    $variant_length = $sw->length;
                                    $variant_length_unit = getUnitSymbol($sw->lunit);
                                    $variant_weight = $sw->weight;
                                    $variant_weight_unit = getUnitSymbol($sw->wunit);
                                }
                            }
                        }
                        $info = array(
                            "basic" => $var_price,
                            "size_diff" => $size_diff,
                            "loading" => $loading,
                            "tax" => $tamtdata,
                            "total_amount" => $tamt,
                            "bprice" => $bprice,
                            "new_price"=>$new_price
                        );
                        $retVar[] =  array(
                            "id" => $var,
                            "title" => $sizename,
                            "variant_title_id" => $sizeid,
                            "quantity" => $quantity,
                            "unit" => $unit,
                            'cat_id' => $product->category,
                            "var_price" => $var_price,
                            "basic_price" => $basic_price,
                            "total_price" => $total_amount,
                            "action" => $action,
                            "basic" => $basic,
                            "price_detail" => $info,
                            "variant_length" => $variant_length,
                            "variant_length_unit" => $variant_length_unit,
                            "variant_weight" => $variant_weight,
                            "variant_weight_unit" => $variant_weight_unit
                        );
                    }
                    $unit_cat =array();
                    foreach ($wunits as $unit){
                        $unit_cat[] = array(
                            'id' => $unit->id,
                            'symbol' => $unit->symbol
                        );
                    }
                    $st = 'Pending';
                    $pvalidity= 0;
                    if($quotation->status == 1){
                        $st = 'Approved';
                        if($quotation->validity !== null){
                            date_default_timezone_set('Asia/Kolkata');
                            $pvalidity = strtotime($quotation->validity) - strtotime(date('Y-m-d H:i:s'));
                            if(new DateTime() > new DateTime($quotation->validity)){
                                $st = 'Expired';
                            }
                        }
                    }
                    $bt = 'N/A';
                    if($brand_name){ $bt = $brand_name->title; }
                    $section = 'Live';
                    if($st == 'Expired'){
                        $section = 'Expired';
                    }
                    $credit_charge = array();
                    if($quotation->credit_charge_id !== 0 || $quotation->credit_charge_id !== '' || $quotation->credit_charge_id !== null){
                        $credit = DB::table('credit_charges')->where('id', $quotation->credit_charge_id)->first();
                        if($credit !== null){
                            $credit_charge[] = array(
                                'id'=>$credit->id,
                                'days'=>$credit->days,
                                'amount'=>$credit->amount,
                            );
                        }
                    }
                    $ret[] = array(
                        'id' => $quotation->id,
                        'product_id' => $quotation->product_id,
                        'product_name' => $brand_name->title,
                        'brand_id' => $bid,
                        'cat_name' => $cat_name->name,
                        'cat_id' => $cat_name->id,
                        'brand_ids' => implode(', ', $brand_names),
                        'status' => $st,
                        'section' => $section,
                        'payment_option' => $quotation->payment_option,
                        'credit_charge_id' => $quotation->credit_charge_id,
                        'other_option' => $quotation->other_option,
                        'location' => $quotation->location,
                        'method' => $quotation->method,
                        'brand_name' => $bt,
                        'variant' => $retVar,
                        'stock'=> $product_quantity,
                        'unit'=>$unit_cat,
                        'booking_date' => date("d, M Y", strtotime($quotation->created_at)),
                        'timer' => $pvalidity,
                        'hub_id' => $quotation->hub_id,
                        'credit_charge' => $credit_charge
                    );
                }else{
                    $product = DB::table('products')->where('id', $bid)->first();
                    $base_variant = json_decode($quotation->variants);
                    $quantity = 0;
                    $total_quantity = 0;
                    $product_quantity = $product->quantity;
                    $tamt = 0;
                    $bprice = 0;
                    $tamtdata = 0;
                    $new_price = 0;
                    $base = $base_variant[$key];

                    $quantity = $base->quantity;
                    $total_quantity = $total_quantity+$base->quantity;
                    if($base->action == 0){
                        if(is_numeric($base->basic_price) && is_numeric($base->var_price)){
                            $new_price = $base->basic_price-$base->var_price;
                        }
                        if($product->tax!==''){
                            $tax = $product->tax;
                            if(is_numeric($base->basic_price) && is_numeric($base->var_price)){
                                $tamtdata = round((($base->basic_price-$base->var_price)+$pdata->loading)*(int)$tax/100);
                                $tamt = round(((($base->basic_price-$base->var_price)+$pdata->loading+$tamtdata)*$base->quantity));
                                $bprice = round((($base->basic_price-$base->var_price)+$pdata->loading+$tamtdata));
                            }
                        }
                    }elseif($base->action == 1){
                        if(is_numeric($base->basic_price) && is_numeric($base->var_price)){
                            $new_price = $base->basic_price+$base->var_price;
                        }
                        if($product->tax!==''){
                            $tax = $product->tax;
                            if(is_numeric($base->basic_price) && is_numeric($base->var_price)){
                                $tamtdata = round((($base->basic_price+$base->var_price)+$pdata->loading)*(int)$tax/100);
                                $tamt = round((($base->basic_price+$base->var_price+$pdata->loading+$tamtdata)*$base->quantity));
                                $bprice = round(($base->basic_price+$base->var_price+$pdata->loading+$tamtdata));
                            }
                        }
                    }
                    $tax_base ='';
                    $total_base = '';
                    $basic = $pdata->price;
                    $loading =$pdata->loading;
                    if($product->tax!==''){
                        $tax = $product->tax;
                    }
                    $unit_cat =array();
                    foreach ($wunits as $unit){
                        $unit_cat[] = array(
                            'id' => $unit->id,
                            'symbol' => $unit->symbol
                        );
                    }
                    $st = 'Pending';
                    $pvalidity= 0;
                    if($quotation->status == 1){
                        $st = 'Approved';
                        if($quotation->validity !== null){
                            date_default_timezone_set('Asia/Kolkata');
                            $pvalidity = strtotime($quotation->validity) - strtotime(date('Y-m-d H:i:s'));
                            if(new \DateTime() > new \DateTime($quotation->validity)){
                                $st = 'Expired';
                            }
                        }
                    }
                    $bt = 'N/A';
                    if($brand_name !== null){
                        $bt = $brand_name->title;
                    }
                    $section = 'Live';
                    if($st == 'Expired'){
                        $section = 'Expired';
                    }
                    $credit_charge = array();
                    if($quotation->credit_charge_id !== 0 || $quotation->credit_charge_id !== '' || $quotation->credit_charge_id !== null){
                        $credit = DB::table('credit_charges')->where('id', $quotation->credit_charge_id)->first();
                        if($credit !== null){
                            $credit_charge[] = array(
                                'id'=>$credit->id,
                                'days'=>$credit->days,
                                'amount'=>$credit->amount,
                            );
                        }
                    }
                    $ret[] = array(
                        'id' => $quotation->id,
                        'product_id' => $quotation->product_id,
                        'product_name' => $bt,
                        'cat_id' => $product->category,
                        'brand_id' => $bid,
                        'cat_name' => $cat_name->name,
                        'cat_id' => $cat_name->id,
                        'brand_ids' => implode(', ', $brand_names),
                        'status' => $st,
                        'section' => $section,
                        'payment_option' => $quotation->payment_option,
                        'other_option' => $quotation->other_option,
                        'location' => $quotation->location,
                        'method' => $quotation->method,
                        'brand_name' => $bt,
                        'quantity' => $quantity,
                        'booking_date' => date("d, M Y", strtotime($quotation->created_at)),
                        "basic" => $basic,
                        "loading" => $loading,
                        "tax" => $tamtdata,
                        'stock'=> $product_quantity,
                        'unit'=>$unit_cat,
                        'bprice'=>$bprice,
                        'new_price'=>$new_price,
                        "total_amount" => $tamt,
                        'timer' => $pvalidity,
                        'hub_id' => $quotation->hub_id,
                        'credit_charge' => $credit_charge
                    );
                }
            }

        }

        return view('institutional/my-quotation-detail')->with(compact('header', 'footer', 'tp','ret','related_products','product'));
    }

    public function dispatchPrograms(){
        if(institutional('user_type') !== 'institutional'){
            abort(401);
        }
        $tp = url("/assets/institutional/");
        $title = 'Dispatch Programs';
        $header = $this->header($title, $tp);
        $footer = $this->footer($tp);
        $dp = DispatchPrograms::where('user_id', institutional('id'))->count();

        return view('institutional/dispatch-programs')->with(compact('header', 'footer', 'tp', 'title', 'dp'));
    }

    public function getDispatchPrograms(){
        $records = DispatchPrograms::where('user_id', institutional('id'));
        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $html = 'No Action';
                if($records->status == 0) {
                    $html = '<button value="' . $records->id . '" class="btn btn-danger btn-sm disp_delete"><i class="mdi mdi-trash-can-outline"></i></button><a class="btn btn-warning btn-sm" style="margin-left: 10px" href="edit-dispatch?id=' . $records->id . '"><i class="mdi mdi-pencil"></i></a>';
                }
                return $html;
            })
            ->editColumn('status', function($records){
                if($records->status == 0){
                    return '<span class="badge bg-warning text-white">Pending</span>';
                }else{
                    return '<ul class="progressbar">
                                  <li '.($records->status > 0 ? 'class="active"' : '').'>Approved</li>
                                  <li '.($records->status > 1 ? 'class="active"' : '').'>Loading</li>
                                  <li '.($records->status > 2 ? 'class="active"' : '').'>In Transit</li>
                                  <li '.($records->status > 3 ? 'class="active"' : '').'>Delivered</li>
                          </ul>';
                }
            })
            ->editColumn('schedule', function($records){
                return date('d, M Y', strtotime($records->schedule));
            })
            ->editColumn('created_at', function($records){
                return date('d, M Y h:i:s a', strtotime($records->created_at));
            })
            ->editColumn('items', function($records){
                $items = json_decode($records->items);
                $party = json_decode($records->party);
                $variants = json_decode($records->variants);
                $quantity = json_decode($records->quantity);
                $unit = json_decode($records->unit);
                $html = '<ul id="myUL">';
                $i = 0;
                foreach ($party as $p){
                    $html .= '<li><span class="caret">'.getPartyName($p[0]).'</span>';
                    $html .= '<ul class="nested">';
                    foreach ($items[$i] as $item){
                        $html .= '<li><span class="caret">'.getProductName($item).'</span>';
                        $html .= '<ul class="nested">';
                        $j = 0;
                        foreach($variants[$i] as $var){
                            $html .= '<li>'.getVariantName($var).' - '.$quantity[$i][$j].' '.getUnitSymbol($unit[$i][$j]).'</li>';
                            $j++;
                        }
                        $html .= '</ul>';
                        $html .= '</li>';
                    }
                    $html .= '</ul>';
                    $html .= '</li>';
                    $i++;
                }

                return $html;
            })
            ->rawColumns(['items','status', 'action'])
            ->make(true);
    }
    public function createDispatch(){
        if(institutional('user_type') !== 'institutional'){
            abort(401);
        }
        $tp = url("/assets/institutional/");
        $title = 'Create Dispatch';
        $header = $this->header($title, $tp);
        $footer = $this->footer($tp);
        $select = 0;
        if(isset($_GET['bid'])){
            $select = $_GET['bid'];
        }
        $bookings = Bookings::where('user_id', institutional('id'))->where('status', '<>', 0)->get();
        $dp = count($bookings);
        $states = array_pluck(DB::table('states')->where('country_id', 101)->get(), 'name', 'id');
        $billing = BillingParties::where('user_id', institutional('id'))->get();
        $units = DB::table('units')->get();
        return view('institutional/create-dispatch')->with(compact('header', 'footer', 'tp', 'title', 'dp', 'bookings', 'billing', 'states', 'units', 'select'));
    }
    public function getVariants(Request $request){
        $books = $request->books;
        $variants = DB::table('product_variants')->whereIn('product_id', $books)->get();
        $pids = array_pluck($variants, 'product_id');
        $upids = array_unique($pids);
        $fpids = array_diff($books, $upids);
        $html = '<option vaue="">Select Variant</option>';
        if(count($fpids)){
            $products = DB::table('products')->whereIn('id', $fpids)->get();
            foreach($products as $product){
                $html.='<option value="product-'.$product->id.'">'.$product->title.'</option>';
            }
        }
        foreach ($variants as $variant){
            $html.='<option value="variant-'.$variant->id.'">'.$variant->variant_title.'</option>';
        }
        return $html;
    }
    public function storeDispatch(Request $request){
        $timezone  = +5.5;
        $data = array();
        $data['user_id'] = institutional('id');
        $data['party'] = json_encode($request->party);
        $data['items'] = json_encode($request->booking);
        $variantsx = (array)$request->variants;
        $quantityx = (array)$request->quantity;
        $unitx = (array)$request->unit;
        $variants = array();
        foreach ($variantsx as $variant){
            array_shift($variant);
            $variants[] = $variant;
        }
        $quantity = array();
        foreach ($quantityx as $qty){
            array_shift($qty);
            $quantity[] = $qty;
        }
        $unit = array();
        foreach ($unitx as $u){
            array_shift($u);
            $unit[] = $u;
        }
        $data['variants'] = json_encode($variants);
        $data['quantity'] = json_encode($quantity);
        $data['unit'] = json_encode($unit);
        if(isset($request->schedule)) {
            $data['schedule'] = $request->schedule;
        }else{
            $data['schedule'] = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
        }
        $id = DispatchPrograms::insertGetId($data);
        $notice = 'Dispatch programs created successfully!';
        return redirect('dispatch-programs')->with('notice', $notice);
    }
    public function editDispatch(){
        if(institutional('user_type') !== 'institutional'){
            abort(401);
        }
        $program = false;

        if(isset($_GET['id'])){
            $program = DispatchPrograms::where('id', $_GET['id'])->first();
        }else{
            abort(404);
        }
        $tp = url("/assets/institutional/");
        $title = 'Edit Dispatch';
        $header = $this->header($title, $tp);
        $footer = $this->footer($tp);
        $select = 0;
        if(isset($_GET['bid'])){
            $select = $_GET['bid'];
        }
        $bookings = Bookings::where('user_id', institutional('id'))->where('status', '<>', 0)->get();
        $dp = count($bookings);
        $states = array_pluck(DB::table('states')->where('country_id', 101)->get(), 'name', 'id');
        $billing = BillingParties::where('user_id', institutional('id'))->get();
        $units = DB::table('units')->get();

        return view('institutional/edit-dispatch')->with(compact('header', 'footer', 'tp', 'title', 'dp', 'bookings', 'billing', 'states', 'units', 'select', 'program'));
    }

    public function updateDispatch(Request $request){
        if(isset($request->id)){
            $dispatch = DispatchPrograms::where('id', $request->id)->first();
            $dispatch->delete();
        }
        $timezone  = +5.5;
        $data = array();
        $data['id'] = $request->id;
        $data['user_id'] = institutional('id');
        $data['party'] = json_encode($request->party);
        $data['items'] = json_encode($request->booking);
        $variantsx = (array)$request->variants;
        $quantityx = (array)$request->quantity;
        $unitx = (array)$request->unit;
        $variants = array();
        foreach ($variantsx as $variant){
            array_shift($variant);
            $variants[] = $variant;
        }
        $quantity = array();
        foreach ($quantityx as $qty){
            array_shift($qty);
            $quantity[] = $qty;
        }
        $unit = array();
        foreach ($unitx as $u){
            array_shift($u);
            $unit[] = $u;
        }
        $data['variants'] = json_encode($variants);
        $data['quantity'] = json_encode($quantity);
        $data['unit'] = json_encode($unit);
        if(isset($request->schedule)) {
            $data['schedule'] = $request->schedule;
        }else{
            $data['schedule'] = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
        }
        $id = DispatchPrograms::insertGetId($data);
        $notice = 'Dispatch programs created successfully!';
        return redirect('dispatch-programs')->with('notice', $notice);
    }
    public function deleteDispatch(Request $request){
        if(isset($request->id)){
            $dispatch = DispatchPrograms::where('id', $request->id)->first();
            $dispatch->delete();
            return 'success';
        }
        return 'failed';
    }

    public function saveParty(Request $request){
        $data['user_id'] = institutional('id');

        $data['company'] = $request->company;
        $data['address1'] = $request->address1;
//        $data['address2'] = $request->address2;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['country'] = 101;
        $data['postcode'] = $request->postcode;
        $data['pan'] = $request->pan;
        $data['gst'] = $request->gst;
        $data['mobile'] = $request->mobile;

        $id = DB::table('billing_parties')->insertGetId($data);
//        $id = BillingParties::insertGetId($data);
//
        return '<option selected="selected" value="party-'.$id.'">'.$data['company'].'</option>';
    }
    public function getCities(Request $request){
        $cities = DB::table('cities')->where('state_id', $request->id)->get();
        $html = '<option value="">Select City</option>';
        foreach ($cities as $city){
            $html .= '<option value="'.$city->id.'">'.$city->name.'</option>';
        }
        return $html;
    }
    public function myOrders(){
        if(institutional('user_type') !== 'institutional'){
            abort(401);
        }
        $tp = url("/assets/institutional/");
        $title = 'My Orders';
        $header = $this->header($title, $tp);
        $footer = $this->footer($tp);
        $orders = Bookings::where('user_id', institutional('id'))->orderBy('id', 'DESC')->get();

        return view('institutional/myorders')->with(compact('header', 'footer', 'tp', 'orders'));
    }

    public function myProfile(Request $request){
        if(institutional('user_type') !== 'institutional'){
            abort(401);
        }
        $tp = url("/assets/institutional/");
        $title = 'My Orders';
        $header = $this->header($title, $tp);
        $footer = $this->footer($tp);
//        $orders = Bookings::where('user_id', customer('id'))->orderBy('id', 'DESC')->get();

        return view('institutional/institutional_account')->with(compact('header', 'footer', 'tp'));

    }
}

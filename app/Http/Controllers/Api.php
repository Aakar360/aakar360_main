<?php

namespace App\Http\Controllers;

use App\CityWiseShippingCharges;
use App\ProductDiscount;
use App\Products;
use App\ProductVariant;
use App\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth;
use PhpParser\Node\Expr\StaticCall;

class Api extends Controller
{
	public $cfg;
    public $cart;
    public $cart_options;	    public $cart_variants;

    public function __construct()
    {	$this->cfg = DB::select('SELECT * FROM config WHERE id = 1')[0];
        $this->cart = isset($_COOKIE['cart']) ? json_decode(stripslashes($_COOKIE['cart']),true) : array();
        $this->cart_options = isset($_COOKIE['cart_options']) ? json_decode($_COOKIE['cart_options'],true) : array();
        $this->cart_variants = isset($_COOKIE['cart_variants']) ? json_decode($_COOKIE['cart_variants'],true) : array();
    }
	public function register()
	{
		if ($this->cfg->registration == 0) {
			return json_encode(["error"=>"Registration is disabled"]);
		}
		if(isset($_POST['register'])){
			if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['confirm_password']) && !empty($_POST['cust_type'])){
				// Escape the user entries and insert them to database
				$name = escape(htmlspecialchars($_POST['name']));
				$email = escape(htmlspecialchars($_POST['email']));
				$password = md5($_POST['password']);
				if($_POST['password'] != $_POST['confirm_password']){
					return json_encode(["error"=>"Password mismatch!"]);
				}
				if(DB::select("SELECT COUNT(*) as count FROM customers WHERE email = '".$email."'")[0]->count > 0) {
					return json_encode(["error"=>"This email is already registerd !"]);
				} else {
					DB::insert("INSERT INTO customers (name,email,password,sid) VALUE ('".$name."','".$email."','".$password."','".md5(microtime())."')");
					return json_encode(["success"=>"Your account has been registred successfully!"]);
				}
			} else {
				return json_encode(["error"=>"All fields are required !"]);
			}
			
		}
	}
	public function login()
	{
		if ($this->cfg->registration == 0) {
			return json_encode(["error"=>"Registration is disabled"]);
		}
		if(isset($_POST['login'])){
			// Check email and password and redirect to the account
			$email = escape($_POST['email']);
			$pass = md5($_POST['password']);
			if(empty($email) or empty($pass)){
				return json_encode(["error"=>"All fields are required !"]);
			} else {
				if(DB::select("SELECT COUNT(*) as count FROM customers WHERE email = '".$email."' AND password = '".$pass."' ")[0]->count > 0) {
					// Generate a new secure ID for this session and redirect to dashboard
					$secure_id = md5(microtime());
					DB::update("UPDATE customers SET sid = '$secure_id' WHERE email = '".$email."'");
					session(['customer' => $secure_id]);
					return json_encode(["success"=>"You logged in successfully!","sid"=>$secure_id]);
				} else {
					return json_encode(["error"=>"Wrong email or password"]);
				}
			}
		}
	}
	public function logout(Request $request)
	{
		// Clear the customer seesion
		$request->session()->forget('customer');
		session(['customer' => '']);
		return "success";
	}

    public function billing(Request $request){
        $type = $_POST['type'];
        $countries = getAllCountries();
        $states = State::where('country_id','101')->get();;
        $cities = getCities(customer('state'));
        $html = '<form action="" method="post" style="max-width: 100%;">
					'.csrf_field().'
					<div class="row">
						<div class="input-field col-md-6">
							<label class="control-label">'.translate('Full Name').'</label>
							<input name="fname" type="text" value="'.customer('name').'" class="form-control"  />
						</div>
						<div class="input-field col-md-6">
							<label class="control-label">'.translate('Company').'</label>
							<input name="company" type="text" value="'.customer('company').'" class="form-control"  />
						</div>';
                        if($type=='register'){
                            $html.='<div class="input-field col-md-6">
                                <label class="control-label">'.translate('Email Address').'</label>
                                <input name="email" type="text" value="'.customer('email').'" class="form-control"  />
                            </div>';
                        }
                        if($type=='register'){
                            $html.='<div class="input-field col-md-6">
                                <label class="control-label">'.translate('Password').'</label>
                                <input name="password" type="text" class="form-control"  />
                            </div>
                            <div class="input-field col-md-6">
                                <label class="control-label">'.translate('Confirm Password').'</label>
                                <input name="confirm_password" type="text" class="form-control"  />
                            </div>';
                        }
                        $html.='</div>
						<div class="row">
                            <div class="input-field col-md-6">
                                <label class="control-label">'.translate('State').'</label>
                                <select name="state" class="form-control" onChange="getCities(this.value, \'city_blling\')">
                                    <option value="">Please select state or province</option>';
                                    foreach($states as $state){
                                        if($state->id == customer('state')){
                                            $html.='<option value="'.$state->id.'" selected="selected">'.$state->name.'</option>';
                                        }else{
                                            $html.='<option value="'.$state->id.'">'.$state->name.'</option>';
                                        }
                                    }
                                    $html.='
                                </select>
                            </div>
                            <div class="input-field col-md-6">
                                <label class="control-label">'.translate('City').'</label>
                                <select name="city" class="form-control" id="city_blling">
                                    <option value="">Please select city or locality</option>';
                                    foreach($cities as $city){
                                        if($city->id == customer('city')){
                                            $html.='<option value="'.$city->id.'" selected="selected">'.$city->name.'</option>';
                                        }else{
                                            $html.='<option value="'.$city->id.'">'.$city->name.'</option>';
                                        }
                                    }
                                    $html.='
                                </select>
                            </div>
						</div>
						<div class="row">
                            <div class="input-field col-md-6">
                                <label class="control-label">'.translate('Zip/Postal Code').'</label>
                                <input name="postcode" value="'.customer('postcode').'" type="text" class="form-control"  />
                            </div>
                            <div class="input-field col-md-6">
                                <label class="control-label">'.translate('Mobile No.').'</label>
                                <input name="mobile" value="'.customer('mobile').'" type="text" class="form-control"  />
                            </div>
						</div>
						<div class="row">
                            <div class="input-field col-md-6">
                                <label class="control-label">'.translate('Alternate Contact').'</label>
                                <input name="alternate_contact" value="'.customer('alternate_mobile').'" type="text" class="form-control"  />
                            </div>
                            <div class="input-field col-md-6">
                                <label class="control-label">'.translate('Address Line 1').'</label>
							    <input name="address_line_1" value="'.customer('address_line_1').'" type="text" class="form-control"  />
                            </div>
						</div>';
                        $html.='<div class="row">
                            <div class="input-field col-md-6">
                                <label class="control-label">'.translate('Address Line 2').'</label>
                                <input name="address_line_2" value="'.customer('address_line_2').'" type="text" class="form-control"  />
                            </div>
                            <div class="input-field col-md-6">
                                <p><label for="ship1"><input type="radio" name="shipping_address_option" value="same" id="ship1" checked="true"/> Ship to above address</label></p>
                                <p><label for="ship2"><input type="radio" name="shipping_address_option" value="different" id="ship2"/> Ship to different address</label></p>
                            </div>
						</div>
					</fieldset>
				</form>';
        return $html;
    }

    public function shipping(Request $request){
        $type = $request->type;
        $countries = getAllCountries();
        $states = getStates(customer('country'));
        $cities = getCities(customer('state'));
        setcookie('pincode', $request->postcode,time()+31536000,'/');
        if($type == "same"){
            $html = '<form action="" method="post" style="max-width: 100%;">

						'.csrf_field().'

						<div class="row">
						<div class="input-field col-md-6">
								<label class="control-label">'.translate('Full Name').'</label>

								<input name="shipping_fname" type="text" value="'.customer('name').'" class="form-control"  />

							</div>
							<div class="input-field col-md-6">

								<label class="control-label">'.translate('Company').'</label>

								<input name="shipping_company" type="text" value="'.customer('company').'" class="form-control"  />

							</div></div>';

            $html.='
							
						<div class="row">
						<div class="input-field col-md-6">
								<label class="control-label">'.translate('Country').'</label>
								<select name="shipping_country" id="country_shipping" class="form-control" onChange="getStates(this.value, \'state_shipping\')">
									<option value="">Select country</option>';
            foreach($countries as $country){
                if($country->id == customer('country')){
                    $html.='<option value="'.$country->id.'" selected="selected">'.$country->name.'</option>';
                }else{
                    $html.='<option value="'.$country->id.'">'.$country->name.'</option>';
                }
            }
            $html.='
								</select>
							</div>
							<div class="input-field col-md-6">
								<label class="control-label">'.translate('State').'</label>
								<select name="shipping_state" class="form-control" id="state_shipping" onChange="getCities(this.value, \'city_shipping\')">
									<option value="">Please select region, state or province</option>';
            foreach($states as $state){
                if($state->id == customer('state')){
                    $html.='<option value="'.$state->id.'" selected="selected">'.$state->name.'</option>';
                }else{
                    $html.='<option value="'.$state->id.'">'.$state->name.'</option>';
                }
            }
            $html.='
								</select>
							</div>
							</div>
						<div class="row">
						<div class="input-field col-md-6">
								<label class="control-label">'.translate('City').'</label>
								<select name="shipping_city" class="form-control" id="city_shipping">
									<option value="">Please select city or locality</option>';
            foreach($cities as $city){
                if($city->id == customer('city')){
                    $html.='<option value="'.$city->id.'" selected="selected">'.$city->name.'</option>';
                }else{
                    $html.='<option value="'.$city->id.'">'.$city->name.'</option>';
                }
            }
            $html.='
								</select>
							</div>
							<div class="input-field col-md-6">
								<label class="control-label">'.translate('Zip/Postal Code').'</label>
								<input name="shipping_postcode" value="'.customer('postcode').'" type="text" class="form-control"  />
							</div>
							</div>
						<div class="row">
						<div class="input-field col-md-6">
								<label class="control-label">'.translate('Mobile No.').'</label>
								<input name="shipping_mobile" value="'.customer('mobile').'" type="text" class="form-control"  />
							</div>
							<div class="input-field col-md-6">
								<label class="control-label">'.translate('Alternate Contact').'</label>
								<input name="shipping_alternate_contact" value="'.customer('alternate_mobile').'" type="text" class="form-control"  />
							</div>
							</div>
							<div class="form-group">

								<label class="control-label">'.translate('Address').'</label>

								<input name="shipping_address_line_1" value="'.customer('address_line_1').'" type="text" class="form-control"  />
								<input name="shipping_address_line_2" value="'.customer('address_line_2').'" type="text" class="form-control"  />

							</div>';
            $html.='
						
					</form>';

        }else{
            $html = '<form action="" method="post" class="form-horizontal single">
						'.csrf_field().'
						<fieldset>
							<div class="form-group">
								<label class="control-label">'.translate('Full Name').'</label>
								<input name="shipping_fname" type="text" value="" class="form-control"  />
							</div>
							<div class="form-group">
								<label class="control-label">'.translate('Company').'</label>
								<input name="shipping_company" type="text" value="" class="form-control"  />
							</div>';
            $html.='<div class="form-group">
								<label class="control-label">'.translate('Address').'</label>
								<input name="shipping_address_line_1" value="" type="text" class="form-control"  />
								<input name="shipping_address_line_2" value="" type="text" class="form-control"  />
							</div>
							<div class="form-group">
								<label class="control-label">'.translate('Country').'</label>
								<select name="shipping_country" id="country_shipping" class="form-control" onChange="getStates(this.value, \'state_shipping\')">
									<option value="">Select country</option>';
            foreach($countries as $country){
                $html.='<option value="'.$country->id.'">'.$country->name.'</option>';
            }
            $html.='
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">'.translate('State').'</label>
								<select name="shipping_state" class="form-control" id="state_shipping" onChange="getCities(this.value, \'city_shipping\')">
									<option value="">Please select region, state or province</option>';
            $html.='
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">'.translate('City').'</label>
								<select name="shipping_city" class="form-control" id="city_shipping">
									<option value="">Please select city or locality</option>';
            $html.='
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">'.translate('Zip/Postal Code').'</label>
								<input name="shipping_postcode" value="" type="text" class="form-control"  />
							</div>
							<div class="form-group">
								<label class="control-label">'.translate('Mobile No.').'</label>
								<input name="shipping_mobile" value="" type="text" class="form-control"  />
							</div>
							<div class="form-group">
								<label class="control-label">'.translate('Alternate Contact').'</label>
								<input name="shipping_alternate_contact" value="" type="text" class="form-control"  />
							</div>';
            $html.='
						</fieldset>
					</form>';
        }
        return $html;
    }


    public function shippingMethod(Request $request){
		$json = json_encode($request->all());
        $cart_items = $this->cart;
        if($request->shipping_postcode != null){
            setcookie('pincode', $request->shipping_postcode,time()+31536000,'/');
        }
        $response["count"] = count($cart_items);
        setcookie('shipping_address', $json,time()+31536000,'/');
        $totalQty = 0;
        $unitid = array();
        $uids = '';
        if($response["count"] > 0){
            $ids = "";
            foreach($cart_items as $id=>$name){
                $ids .= $id . ",";
            }
            $ids = rtrim($ids, ',');
            $cart_products = DB::select("SELECT * FROM products WHERE id IN ({$ids}) ORDER BY id DESC ");

            foreach ($cart_products as $row) {
                $uids .= $row->weight_unit.',';
                if(!isset($this->cart_variants[$row->id])){
                    $totalQty += $this->cart[$row->id];
                }else {
                    $variants = json_decode($this->cart_variants[$row->id], true);

                    $price = 0;
                    if (count($this->cart_variants) > 0) {
                        foreach ($variants as $var) {
                            if ($var) {
                                if ($var['quantity'] != '0' || $var['quantity'] !== '0' || $var['quantity'] !== null || $var['quantity'] !== '' || !empty($var['quantity'])) {
                                    $totalQty += (int)$var['quantity'];
                                }
                            }
                        }
                    }
                }
            }
        }
        $unitid[] = rtrim($uids, ',');
        $pincode = $_COOKIE['pincode'];
        $records = CityWiseShippingCharges::whereRaw('FIND_IN_SET('.$pincode.',Pincode)')->whereIn('unit',$unitid)->where('from_weight','<=',$totalQty)->where('to_weight','>=',$totalQty)->get();
        $html = '';
        if(count($records)) {
            $html .= '<form>';
            $i = 0;
            foreach ($records as $record) {
                if ($i == 0) {
                    $html .= '<div class="custom-control custom-radio">
                      <input type="radio" class="custom-control-input" id="' . $record->id . '" name="shippingMethod" value="' . $record->price . '">
                      <label class="custom-control-label" for="' . $record->id . '">' . getCityName($record->city_id) . ' (' . c($record->price) . ')</label>
                </div>';
                } else {
                    $html .= '<div class="custom-control custom-radio">
                      <input type="radio" class="custom-control-input" id="' . $record->id . '" name="shippingMethod" value="' . $record->price9 . '">
                      <label class="custom-control-label" for="' . $record->id . '">' . getCityName($record->city_id) . ' (' . c($record->price) . ')</label>
                </div>';
                }
                $i++;
            }
            $html .= '</form';
        }else{
            $html .= '<div class="col-lg-12"><h5>No shipping method found.</h5></div>';
        }
		return $html;
	}
	public function paymentMethod(Request $request){
		$json = json_encode($request->all());
		setcookie('shipping_method', $json,time()+31536000,'/');
       $records = DB::select("select * from payments where active = 1");
        $html = '<form>';
        $i = 0;
        foreach($records as $record){
            if($i == 0) {
                $html .= '<div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="' . $record->id . '" name="paymentMethod" value="' . $record->id . '">
                          <label class="custom-control-label" for="' . $record->id . '">' . $record->title . '</label>
                          ';
                if ($record->options != '{}') {
                    $data = json_decode($record->options);
                    $html .= '<blockquote>';
                    foreach ($data as $key => $value) {
                        if ($value != '')
                            $html .= $key . ': ' . $value . '<br>';
                    }
                    $html .= '</blockquote>';
                }
                $html .= '
                    </div>';
            }else{
                $html .= '<div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="' . $record->id . '" name="paymentMethod" value="' . $record->id . '">
                          <label class="custom-control-label" for="' . $record->id . '">' . $record->title . '</label>
                          ';
                if ($record->options != '{}') {
                    $data = json_decode($record->options);
                    $html .= '<blockquote>';
                    foreach ($data as $key => $value) {
                        if ($value != '')
                            $html .= $key . ': ' . $value . '<br>';
                    }
                    $html .= '</blockquote>';
                }
                $html .= '
                    </div>';
            }
            $i++;
        }
        $html.='</form';
		return $html;
	}
    public function confirmOrder(Request $request){
        $json = json_encode($request->all());
        setcookie('shipping_method', $json,time()+31536000,'/');
        $cart_items = $this->cart;
        $response["count"] = count($cart_items);
        $response['header'] = '';

        $html = '<div class="table-responsive"><table class="table table-bordered">
            <thead>
              <tr>
                <th>Image</th>
                <th>Product Name</th>
                <th class="text-center">Quantity</th>
                <th class="text-right">Price</th>
                <th class="text-right">Tax(%)</th>
                <th class="text-right">Tax Amount</th>
                <th class="text-right">Amount</th>
              </tr>
            </thead>';

            if($response["count"] > 0){
                $ids = "";
                foreach($cart_items as $id=>$name){
                    $ids .= $id . ",";
                }
                $ids = rtrim($ids, ',');
                $cart_products = DB::select("SELECT * FROM products WHERE id IN ({$ids}) ORDER BY id DESC ");
                $total_price = 0;
                $total_p = 0;
                $response["products"] = array();
                foreach ($cart_products as $row){
                    $q = $this->cart[$row->id];
                    $options = json_decode($this->cart_options[$row->id],true);
                    $option_array = array();
                    foreach ($options as $option) {
                        $option_array[] = '<i>'.$option['title'].'</i> : '.$option['value'];
                    }
                    $data['id'] = $row->id;
                    $data['images'] = image_order($row->images);
                    $data['title'] = $row->title;
                    $data['price'] = c($row->price);
                    $data['quantity'] = $q;
                    $data['options'] = implode('<br/>',$option_array);
                    $data['total'] = 0.00;
                    $data['variants'] = '';
                    if(isset($this->cart_variants[$row->id]))
                    $variants = json_decode($this->cart_variants[$row->id],true);
                    $variant_array = array();
                    $p_var = DB::select("SELECT * FROM product_variants WHERE product_id = '".$row->id."' ORDER BY id ASC ");
                    $price_total = 0;
                    $price = 0;
                    if(count($this->cart_variants) > 0){
                        foreach ($variants as $var) {
                            if($var) {
                                if ($var['quantity'] != '0' || $var['quantity'] !== '0' || $var['quantity'] !== null || $var['quantity'] !== '' || !empty($var['quantity'])) {
                                    $variant_array[] = '<i>' . variantTitle($var['title']) . '</i> : ' . $var['quantity'] . ' ' . $var['unit'] . ' x ' . c($var['price']);
                                    $price_total += (int)$var['price'] * (int)$var['quantity'];
                                }
                            }
                        }
                        $data['variants'] = implode('<br/>',$variant_array);
                    }
                    $price = $row->price;
                    if($row->price > $row->sale){
                        $price = $row->sale;
                    }
                    $data['total'] = c($price*$q);
                    $s = $price*$q;
                    if($price_total){
                        $price = $price_total;
                        $data['total'] = c($price);
                        $total_price += $price;
                        $s = $price;
                    }else{
                        $total_price += $price*$q;
                    }
                    $data['price'] = c($price);
                    array_push($response["products"], $data);
                    $total_p+= $q;
                    $t = 100;
                    $p = $s/$t;
                    $tax = $s - $p;
                    $html.='<tr>
                        <td><img src="'.url('assets/products/'.$data['images']).'" style="max-width: 100px;" /></td>
                        <td>'.$data['title'].'<br><p>'.$data['variants'].'</p></td>
                        <td class="text-center">';if(empty($data['variants'])){ $data['quantity']; $html.=' Unit(s)'; } $html .='</td>
                        <td class="text-right">';if(empty($data['variants'])) c(number_format($p, 2, '.', '')); $html.='</td>
                        <td class="text-right">'.$row->tax.'</td>
                        <td class="text-right">'.c(number_format($tax, 2, '.', '')).'</td>
                        <td class="text-right">'.$data['total'].'</td>
                    </tr>';
                }
                $html.='<tr>
                    <td colspan="6" class="text-right"><b>Sub-Total : </b></td>
                    <td class="text-right"><b>'.c(number_format($total_price, 2, '.', '')).'</b></td>
                </tr>';
                $dis = 0;
                if (isset($_COOKIE["coupon"])) {
                    // Check if coupon applied
                    if (DB::select("SELECT COUNT(*) as count FROM coupons WHERE code = '" . $_COOKIE['coupon'] . "'")[0]->count > 0) {
                        $coupon = DB::select("SELECT * FROM coupons WHERE code = '" . $_COOKIE["coupon"] . "'")[0];
                        $dis = 0;
                        if($coupon->type == '%'){
                            $dis = number_format(($total_price*$coupon->discount)/100, 2, '.', '');
                        }else{
                            $dis = $coupon->discount;
                        }
                        $html.='<tr>
                            <td colspan="6" class="text-right"><b>Discount : </b></td>
                            <td class="text-right"><b>'.c($dis).'</b></td>
                        </tr>';
                    }
                }
                $meth = 0.00;
                if(isset($_COOKIE["shipping_method"])){
                    $sm = json_decode($_COOKIE["shipping_method"]);
                    if(!empty($sm)) {
                        $meth = $sm->shippingMethod;
                    }
                }
                $html.='<tr>
                            <td colspan="6" class="text-right"><b>Shipping Cost : </b></td>
                            <td class="text-right"><b>'.c($meth).'</b></td>
                        </tr>';
                $paid = '';
                if(isset($_COOKIE['payment_method'])){
                    $pm = json_decode($_COOKIE['payment_method']);
                    if(!empty($pm)){
                        $pmId = $pm->paymentMethod;
                        if (DB::select("SELECT COUNT(*) as count FROM payments WHERE id = " . $pmId)[0]->count > 0) {
                            $payment = DB::select("SELECT * FROM payments WHERE id = " . $pmId)[0];
                            $paid = $payment->title;
                        }
                    }
                }
                $html.='<tr>
                    <td colspan="6" class="text-right"><b>Grand Total : </b></td>
                    <td class="text-right"><b>'.c(number_format(number_format($total_price, 2, '.', '')-$dis, 2, '.', '')+number_format($meth, 2, '.', '')).'</b></td>
                </tr>';
            }
        $html.='</table></div>';

        return $html;
    }

    public function confirm(Request $request){
        $json = json_encode($request->all());
        setcookie('payment_method', $json,time()+31536000,'/');
        $datax = array();
        $code = "";
        $datax['customer'] = customer('id');
        $datax['name'] = customer('name');
        $datax['email'] = customer('email');
        $datax['city'] = getCityName(customer('city'));
        $datax['address'] = $_COOKIE['shipping_address'];
        $datax['mobile'] = customer('mobile') != '' ? customer('mobile') : customer('alternate_mobile');
        $cart_items = $this->cart;
        $response["count"] = count($cart_items);
        $total_price = 0;
        if($response["count"] > 0){
            $ids = '';
            foreach($cart_items as $id=>$name){
                $ids .= $id . ",";
            }
            $ids = explode(',',$ids);
            $cart_products = Products::whereIn('id',$ids)->orderBy('id','DESC')->get();
            $total_price = 0;
            $total_p = 0;
            $response["products"] = array();
            $sid = '';
            $poid = 1;
            foreach ($cart_products as $row){
                $q = $this->cart[$row->id];
                $options = json_decode($this->cart_options[$row->id],true);
                $option_array = array();
                foreach ($options as $option) {
                    $option_array[] = '<i>'.$option['title'].'</i> : '.$option['value'];
                }
                $data['id'] = $row->id;
                $data['images'] = image_order($row->images);
                $data['title'] = $row->title;
                $data['price'] = c($row->price);
                $data['quantity'] = $q;
                $data['options'] = implode('<br/>',$option_array);
                $data['total'] = 0.00;
                $data['variants'] = '';
                if(isset($this->cart_variants[$row->id]))
                $variants = json_decode($this->cart_variants[$row->id],true);
                $variant_array = array();
                $p_var = DB::select("SELECT * FROM product_variants WHERE product_id = '".$row->id."' ORDER BY id ASC ");
                $price_total = 0;
                $price = 0;
                if(count($variants) > 0){
                    foreach ($variants as $var) {
                        if($var['quantity'] != '0'){
                            $variant_array[] = '<i>'.$var['title'].'</i> : '.$var['quantity'].' unit x '.c($var['price']);
                            $price_total += (int)$var['price']*(int)$var['quantity'];
                        }
                    }
                    $data['variants'] = implode('<br/>',$variant_array);
                }
                $price = $row->price;
                if($row->price > $row->sale){
                    $price = $row->sale;
                }
                $data['total'] = c($price*$q);
                if($price_total){
                    $price = $price_total;
                    $data['total'] = c($price);
                    $total_price = $price;
                }else{
                    $total_price = $price*$q;
                }
                $data['price'] = c($price);
                array_push($response["products"], $data);
                $total_p+= $q;
                $sid = $sid.$row->added_by.',';
            }
        }
        $datay = array();
        $response["multiseller_payment"] = array();
        $datay['seller_id'] = '1';
        $datay['order_id'] = '1';
        if(customer('user_type') == 'dealer') {
            $datay['dealer_id'] = customer('id');
        }
        $datay['amount'] = '1';
        $datay['balance_amount'] = '1';
        $datay['date_created'] = date('Y-m-d H:i:s');
        $multiseller_payment = DB::table('multiseller_payments')->insertGetId($datay);
        $datax['products'] = json_encode($response["products"]);
        $dis = 0;
        if (isset($_COOKIE["coupon"])) {
            if (DB::select("SELECT COUNT(*) as count FROM coupons WHERE code = '" . $_COOKIE['coupon'] . "'")[0]->count > 0) {
                $coupon = DB::select("SELECT * FROM coupons WHERE code = '" . $_COOKIE["coupon"] . "'")[0];
                $dis = 0;
                $code = $coupon->code;
                if($coupon->type == '%'){
                    $dis = number_format(($total_price*$coupon->discount)/100, 2, '.', '');
                }else{
                    $dis = $coupon->discount;
                }
            }
        }
        $sp = 0.00;
        if(isset($_COOKIE["shipping_method"])){
            $sm = json_decode($_COOKIE["shipping_method"]);
            if(!empty($sm)) {
                $meth = $sm->shippingMethod;
                if (DB::select("SELECT COUNT(*) as count FROM shipping WHERE id = " . $meth)[0]->count > 0) {
                    $shipping = DB::select("SELECT * FROM shipping WHERE id = " . $meth)[0];
                    $sp = $shipping->cost;
                }
            }
        }
        $datax['shipping'] = $meth;
        $paid = array();
        if(isset($_COOKIE['payment_method'])){
            $pm = json_decode($_COOKIE['payment_method']);
            if(!empty($pm)) {
                $pmId = $pm->paymentMethod;
                if (DB::select("SELECT COUNT(*) as count FROM payments WHERE id = " . $pmId)[0]->count > 0) {
                    $payment = DB::select("SELECT * FROM payments WHERE id = " . $pmId)[0];
                    $paid['method'] = $payment->code;
                    if ($payment->code == 'cash') {
                        $paid['payment_status'] = 'paid';
                    } else {
                        $paid['payment_status'] = 'unpaid';
                    }
                }
            }
        }
        $ssid = rtrim($sid,',');
        $datax['seller_id'] = $ssid;
        $datax['payment'] = json_encode($paid);
        $sum = ($total_price-$dis)+$sp;
        $datax['summ'] = number_format($sum, 2, '.', '');
        $datax['date'] = date('Y-m-d');
        $datax['coupon'] = $code;
        $datax['time'] = time();
        $datax['stat'] = 1;
        $datax['country'] = 'IN';

        $order = DB::table('orders')->insertGetId($datax);
        $newid = DB::getPdo()->lastInsertId();
        $sids = explode(',', $sid);
        $tot = count($sids);
        if($tot!=1){
            array_pop($sids);
        }
        $unique_arr = array_unique($sids);
        $max_poid = DB::select("SELECT CASE WHEN max(id) is NULL then 0 else max(id) END as maxx FROM `seller_po`")[0];
        $mx = $max_poid ->maxx;
        $poid = $mx+1;
        $poo  = $mx+1;
        foreach($unique_arr as $key ){
            $idss = '';
            $tot_check = count($cart_items);
            if($tot_check==1){
                foreach($cart_items as $id => $val){
                    $cart_products = DB::select("SELECT * FROM products WHERE id IN ({$id}) ORDER BY id DESC ");
                }
            }else{
                $ids = "";
                foreach($cart_items as $id=>$name){
                    $idss = $idss . $id . ",";
                }
                $idss = rtrim($idss, ',');
                $cart_products = DB::select("SELECT * FROM products WHERE id IN ({$idss}) ORDER BY id DESC ");
            }
            $total_price = 0;
            $total_p = 0;
            $response["sproducts"] = array();
            $sid = '';
            foreach ($cart_products as $row){
                $psid = $row->added_by;
                if($key==$psid){
                    $q = $this->cart[$row->id];
                    $options = json_decode($this->cart_options[$row->id],true);
                    $option_array = array();
                    foreach ($options as $option) {
                        $option_array[] = '<i>'.$option['title'].'</i> : '.$option['value'];
                    }
                    $data['id'] = $row->id;
                    $data['images'] = image_order($row->images);
                    $data['title'] = $row->title;
                    $data['price'] = c($row->price);
                    $data['quantity'] = $q;
                    $data['options'] = implode('<br/>',$option_array);
                    $data['total'] = 0.00;
                    $data['variants'] = '';
                    if(isset($this->cart_variants[$row->id]))
                    $variants = json_decode($this->cart_variants[$row->id],true);
                    $variant_array = array();
                    $p_var = DB::select("SELECT * FROM product_variants WHERE product_id = '".$row->id."' ORDER BY id ASC ");
                    $price_total = 0;
                    $price = 0;
                    if(count($variants) > 0){
                        foreach ($variants as $var) {
                            if($var['quantity'] != '0'){
                                $variant_array[] = '<i>'.$var['title'].'</i> : '.$var['quantity'].' unit x '.c($var['price']);
                                $price_total += (int)$var['price']*(int)$var['quantity'];
                            }
                        }
                        $data['variants'] = implode('<br/>',$variant_array);
                    }
                    $price = $row->price;
                    if($row->price > $row->sale){
                        $price = $row->sale;
                    }
                    $data['total'] = c($price*$q);
                    if($price_total){
                        $price = $price_total;
                        $data['total'] = c($price);
                        $total_price = $price;
                    }else{
                        $total_price = $price*$q;
                    }
                    $data['price'] = c($price);
                    array_push($response["sproducts"], $data);
                    $total_p+= $q;
                }
                else{
                    continue;
                }
                $poo = 'po_'.$poid;
            }
            $product_seller_id = $key;
            $seller_product = addslashes(json_encode($response["sproducts"]));
            DB::insert("INSERT INTO seller_po (`order_id`,`po_no`,`customer_id`,`products`,`order_status`) VALUE ('$newid','$poo','$product_seller_id','$seller_product','pending')");
            $poid++;
        }
        if($order)
            return 'success';
        else
            return 'failure';
    }

    public function servicebilling(Request $request){
        $type = $_POST['type'];
        $countries = getAllCountries();
        $states = getStates(customer('country'));
        $cities = getCities(customer('state'));
        $html = '<form action="" method="post" class="form-horizontal single">
                    '.csrf_field().'
                    <fieldset>
                        <div class="form-group">
                            <label class="control-label">'.translate('Full Name').'</label>
                            <input name="fname" type="text" value="'.customer('name').'" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">'.translate('Company').'</label>
                            <input name="company" type="text" value="'.customer('company').'" class="form-control"  />
                        </div>';
                        if($type=='register'){
                        $html.='<div class="form-group">
                                <label class="control-label">'.translate('Email Address').'</label>
                                <input name="email" type="text" value="'.customer('email').'" class="form-control"  />
                            </div>';
                        }
                        $html.='<div class="form-group">
                            <label class="control-label">'.translate('Address').'</label>
                            <input name="address_line_1" value="'.customer('address_line_1').'" type="text" class="form-control"  />
                            <input name="address_line_2" value="'.customer('address_line_2').'" type="text" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">'.translate('Country').'</label>
                                <select name="country" id="country_billing" class="form-control" onChange="getStates(this.value, \'state_billing\')">
                                    <option value="">Select country</option>';
                                    foreach($countries as $country){
                                        if($country->id == customer('country')){
                                            $html.='<option value="'.$country->id.'" selected="selected">'.$country->name.'</option>';
                                        }else{
                                            $html.='<option value="'.$country->id.'">'.$country->name.'</option>';
                                        }
                                    }
                                    $html.='
                                </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">'.translate('State').'</label>
                            <select name="state" class="form-control" id="state_billing" onChange="getCities(this.value, \'city_blling\')">
                                <option value="">Please select region, state or province</option>';
                                foreach($states as $state){
                                    if($state->id == customer('state')){
                                        $html.='<option value="'.$state->id.'" selected="selected">'.$state->name.'</option>';
                                    }else{
                                        $html.='<option value="'.$state->id.'">'.$state->name.'</option>';
                                    }
                                }
                                $html.='
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">'.translate('City').'</label>
                            <select name="city" class="form-control" id="city_blling">
                                <option value="">Please select city or locality</option>';
                                foreach($cities as $city){
                                    if($city->id == customer('city')){
                                        $html.='<option value="'.$city->id.'" selected="selected">'.$city->name.'</option>';
                                    }else{
                                        $html.='<option value="'.$city->id.'">'.$city->name.'</option>';
                                    }
                                }
                                $html.='
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">'.translate('Zip/Postal Code').'</label>
                            <input name="postcode" value="'.customer('postcode').'" type="text" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">'.translate('Mobile No.').'</label>
                            <input name="mobile" value="'.customer('mobile').'" type="text" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">'.translate('Alternate Contact').'</label>
                            <input name="alternate_contact" value="'.customer('alternate_mobile').'" type="text" class="form-control"  />
                        </div>';
                        if($type=='register'){
                        $html.='<div class="form-group">
                            <label class="control-label">'.translate('Password').'</label>
                            <input name="password" type="text" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">'.translate('Confirm Password').'</label>
                            <input name="confirm_password" type="text" class="form-control"  />
                        </div>';
                        }
                        $html.='
                        <div class="form-group" style="display: none;">
                            <p><label for="ship1"><input type="radio" name="shipping_address_option" value="same" id="ship1" checked="true"/> Ship to above address</label></p>
                            <p><label for="ship2"><input type="radio" name="shipping_address_option" value="different" id="ship2"/> Ship to different address</label></p>
                        </div>
                    </fieldset>
				</form>';
        return $html;
    }

    public function serviceShipping(Request $request){
        $json = json_encode($request->all());
        setcookie('billing_address', $json,time()+31536000,'/');
        $records = DB::select("select * from payments where active = 1");
        $html = '<form>';
        $i = 0;
        foreach($records as $record){
            if($i == 0) {
                $html .= '<div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="' . $record->id . '" name="paymentMethod" value="' . $record->id . '" checked>
                          <label class="custom-control-label" for="' . $record->id . '">' . $record->title . '</label>';
                if ($record->options != '{}') {
                    $data = json_decode($record->options);
                    $html .= '<blockquote>';
                    foreach ($data as $key => $value) {
                        if ($value != '')
                            $html .= $key . ': ' . $value . '<br>';
                    }
                    $html .= '</blockquote>';
                }
                $html .= '
                    </div>';
            }else{
                $html .= '<div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="' . $record->id . '" name="paymentMethod" value="' . $record->id . '">
                          <label class="custom-control-label" for="' . $record->id . '">' . $record->title . '</label>
                          ';
                if ($record->options != '{}') {
                    $data = json_decode($record->options);
                    $html .= '<blockquote>';
                    foreach ($data as $key => $value) {
                        if ($value != '')
                            $html .= $key . ': ' . $value . '<br>';
                    }
                    $html .= '</blockquote>';
                }
                $html .= '
                    </div>';
            }
            $i++;
        }
        $html.='</form';
        return $html;
    }

    public function ServiceconfirmOrder(Request $request){
        $user_id = session('user_id');
        $service_id = session('service_id');
        $response['header'] = '';
        $html = '<div class="table-responsive"><table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Service Name</th>
                        <th class="text-right">Price</th>
                        <th class="text-right">Tax(%)</th>
                        <th class="text-right">Tax Amount</th>
                        <th class="text-right">Amount</th>
                      </tr>
                    </thead>';
        $serv = DB::select("SELECT * FROM services WHERE id = '".$service_id."'")[0];

        $price = $serv->price;
        $tax = 0;
        $taxamt= 0;
        if($serv->tax !== null) {
            $rate = $serv->price;
            $tax = $serv->tax;
            $taxa = 1 + ($tax / 100);
            $price = $price/$taxa;
            $tprice = $serv->price;
            $taxamt = $rate - $price;
        } else { $price = $serv->price; $tprice = $serv->price; }
        $html.='<tr>
                            <td>'.$serv->title.'</p></td>
                            <td class="text-right">'.c(number_format($price, 2, '.', '')).'</td>
                            <td class="text-right">'.$tax.'</td>
                            <td class="text-right">'.c(number_format($taxamt, 2, '.', '')).'</td>
                            <td class="text-right">'.$tprice.'</td>
                        </tr>';

        $html.='</table></div>';

        return $html;
    }

    public function serviceconfirm(){
        $user_id = session('user_id');
        $service_id = session('service_id');
        $pay = DB::select("SELECT * FROM services WHERE id = '$service_id'")[0];
        $payment_mode = $pay->payment_mode;
        $answer = session('answer');
        $status = 'Pending';
        if(isset($_COOKIE["billing_address"])) {
            $ba = json_decode($_COOKIE["billing_address"]);
            $fname = $ba->fname;
            $company = $ba->company;
            $address_line_1 = $ba->address_line_1;
            $address_line_2 = $ba->address_line_2;
            $country = $ba->country;
            $state = $ba->state;
            $city = $ba->city;
            $postcode = $ba->postcode;
            $mobile = $ba->mobile;
            $alternate_contact = $ba->alternate_contact;

            $user = DB::update("UPDATE customers SET name = '".$fname."', mobile = '".$mobile."', alternate_mobile = '".$alternate_contact."', company = '".$company."',
             address_line_1 = '".$address_line_1."', address_line_2 = '".$address_line_2."', city = '".$city."', state = '".$state."', country = '".$country."', postcode = '".$postcode."' WHERE id = '".$user_id."'");
        }
        $paid = array();
        if(isset($_COOKIE['payment_method'])){
            $pm = json_decode($_COOKIE['payment_method']);
            $pmId = $pm->paymentMethod;
            if (DB::select("SELECT COUNT(*) as count FROM payments WHERE id = " . $pmId)[0]->count > 0) {
                $payment = DB::select("SELECT * FROM payments WHERE id = " . $pmId)[0];
                $paid['method'] = $payment->code;
                if($payment->code == 'cash') {
                    $paid['payment_status'] = 'paid';
                }else{
                    $paid['payment_status'] = 'unpaid';
                }
            }

        }
        $paid = json_encode($paid);
        $or = DB::insert("INSERT INTO services_answer (service_id,user_id,answer,status,payment_mode,payment_method) VALUES ('".$service_id."','".$user_id."','".$answer."','$status','".$payment_mode."','".$paid."')");
        if($or)
            return 'success';
        else
            return 'failure';
    }

    /*PLAN*/
    public function planbilling(Request $request){
        $type = $_POST['type'];
        $countries = getAllCountries();
        $states = getStates(customer('country'));
        $cities = getCities(customer('state'));
        $html = '<form action="" method="post" class="form-horizontal single">
					'.csrf_field().'
					<fieldset>
						<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('Full Name').'</label>
							<input name="fname" type="text" value="'.customer('name').'" class="form-control"  />
						</div>
						<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('Company').'</label>
							<input name="company" type="text" value="'.customer('company').'" class="form-control"  />
						</div>';
        if($type=='register'){
            $html.='<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Email Address').'</label>
								<input name="email" type="text" value="'.customer('email').'" class="form-control"  />
							</div>';
        }
        $html.='<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('Address').'</label>
							<input name="address_line_1" value="'.customer('address_line_1').'" type="text" class="form-control"  />
							<input name="address_line_2" value="'.customer('address_line_2').'" type="text" class="form-control"  />
						</div>
						<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('Country').'</label>
							<select name="country" id="country_billing" class="form-control" onChange="getStates(this.value, \'state_billing\')">
								<option value="">Select country</option>';
        foreach($countries as $country){
            if($country->id == customer('country')){
                $html.='<option value="'.$country->id.'" selected="selected">'.$country->name.'</option>';
            }else{
                $html.='<option value="'.$country->id.'">'.$country->name.'</option>';
            }
        }
        $html.='
							</select>
						</div>
						<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('State').'</label>
							<select name="state" class="form-control" id="state_billing" onChange="getCities(this.value, \'city_blling\')">
								<option value="">Please select region, state or province</option>';
        foreach($states as $state){
            if($state->id == customer('state')){
                $html.='<option value="'.$state->id.'" selected="selected">'.$state->name.'</option>';
            }else{
                $html.='<option value="'.$state->id.'">'.$state->name.'</option>';
            }
        }
        $html.='
							</select>
						</div>
						<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('City').'</label>
							<select name="city" class="form-control" id="city_blling">
								<option value="">Please select city or locality</option>';
        foreach($cities as $city){
            if($city->id == customer('city')){
                $html.='<option value="'.$city->id.'" selected="selected">'.$city->name.'</option>';
            }else{
                $html.='<option value="'.$city->id.'">'.$city->name.'</option>';
            }
        }
        $html.='
							</select>
						</div>
						<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('Zip/Postal Code').'</label>
							<input name="postcode" value="'.customer('postcode').'" type="text" class="form-control"  />
						</div>
						<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('Mobile No.').'</label>
							<input name="mobile" value="'.customer('mobile').'" type="text" class="form-control"  />
						</div>
						<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('Alternate Contact').'</label>
							<input name="alternate_contact" value="'.customer('alternate_mobile').'" type="text" class="form-control"  />
						</div>';
        if($type=='register'){
            $html.='<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('Password').'</label>
							<input name="password" type="text" class="form-control"  />
						</div>
						<div class="form-group col-md-6" style="margin: 0;">
							<label class="control-label">'.translate('Confirm Password').'</label>
							<input name="confirm_password" type="text" class="form-control"  />
						</div>';
        }
        $html.='
						<div class="form-group col-md-6" style="margin-top: 15px;">
							<p style="margin: 0"><label for="ship1"><input type="radio" name="shipping_address_option" value="same" id="ship1" checked="true"/> Ship to above address</label></p>
							<p style="margin: 0"><label for="ship2"><input type="radio" name="shipping_address_option" value="different" id="ship2"/> Ship to different address</label></p>
						</div>
					</fieldset>
				</form>';
        return $html;
    }

    public function planshipping(Request $request){
        $type = $request->type;
        $countries = getAllCountries();
        $states = getStates(customer('country'));
        $cities = getCities(customer('state'));

        if($type == "same"){
            $html = '<form action="" method="post" class="form-horizontal single">

						'.csrf_field().'

						<fieldset style="margin-bottom: 15px;">

							<div class="form-group col-md-6" style="margin: 0;">

								<label class="control-label">'.translate('Full Name').'</label>

								<input name="shipping_fname" type="text" value="'.customer('name').'" class="form-control"  />

							</div>
							<div class="form-group col-md-6" style="margin: 0;">

								<label class="control-label">'.translate('Company').'</label>

								<input name="shipping_company" type="text" value="'.customer('company').'" class="form-control"  />

							</div>';

            $html.='<div class="form-group col-md-6" style="margin: 0;">

								<label class="control-label">'.translate('Address').'</label>

								<input name="shipping_address_line_1" value="'.customer('address_line_1').'" type="text" class="form-control"  />
								<input name="shipping_address_line_2" value="'.customer('address_line_2').'" type="text" class="form-control"  />

							</div>
							
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Country').'</label>
								<select name="shipping_country" id="country_shipping" class="form-control" onChange="getStates(this.value, \'state_shipping\')">
									<option value="">Select country</option>';
            foreach($countries as $country){
                if($country->id == customer('country')){
                    $html.='<option value="'.$country->id.'" selected="selected">'.$country->name.'</option>';
                }else{
                    $html.='<option value="'.$country->id.'">'.$country->name.'</option>';
                }
            }
            $html.='
								</select>
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('State').'</label>
								<select name="shipping_state" class="form-control" id="state_shipping" onChange="getCities(this.value, \'city_shipping\')">
									<option value="">Please select region, state or province</option>';
            foreach($states as $state){
                if($state->id == customer('state')){
                    $html.='<option value="'.$state->id.'" selected="selected">'.$state->name.'</option>';
                }else{
                    $html.='<option value="'.$state->id.'">'.$state->name.'</option>';
                }
            }
            $html.='
								</select>
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('City').'</label>
								<select name="shipping_city" class="form-control" id="city_shipping">
									<option value="">Please select city or locality</option>';
            foreach($cities as $city){
                if($city->id == customer('city')){
                    $html.='<option value="'.$city->id.'" selected="selected">'.$city->name.'</option>';
                }else{
                    $html.='<option value="'.$city->id.'">'.$city->name.'</option>';
                }
            }
            $html.='
								</select>
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Zip/Postal Code').'</label>
								<input name="shipping_postcode" value="'.customer('postcode').'" type="text" class="form-control"  />
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Mobile No.').'</label>
								<input name="shipping_mobile" value="'.customer('mobile').'" type="text" class="form-control"  />
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Alternate Contact').'</label>
								<input name="shipping_alternate_contact" value="'.customer('alternate_mobile').'" type="text" class="form-control"  />
							</div>';
            $html.='
						</fieldset>
					</form>';
        }else{
            $html = '<form action="" method="post" class="form-horizontal single">
						'.csrf_field().'
						<fieldset style="margin-bottom: 15px;">
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Full Name').'</label>
								<input name="shipping_fname" type="text" value="" class="form-control"  />
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Company').'</label>
								<input name="shipping_company" type="text" value="" class="form-control"  />
							</div>';
            $html.='<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Address').'</label>
								<input name="shipping_address_line_1" value="" type="text" class="form-control"  />
								<input name="shipping_address_line_2" value="" type="text" class="form-control"  />
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Country').'</label>
								<select name="shipping_country" id="country_shipping" class="form-control" onChange="getStates(this.value, \'state_shipping\')">
									<option value="">Select country</option>';
            foreach($countries as $country){
                $html.='<option value="'.$country->id.'">'.$country->name.'</option>';
            }
            $html.='
								</select>
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('State').'</label>
								<select name="shipping_state" class="form-control" id="state_shipping" onChange="getCities(this.value, \'city_shipping\')">
									<option value="">Please select region, state or province</option>';
            $html.='
								</select>
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('City').'</label>
								<select name="shipping_city" class="form-control" id="city_shipping">
									<option value="">Please select city or locality</option>';
            $html.='
								</select>
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Zip/Postal Code').'</label>
								<input name="shipping_postcode" value="" type="text" class="form-control"  />
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Mobile No.').'</label>
								<input name="shipping_mobile" value="" type="text" class="form-control"  />
							</div>
							<div class="form-group col-md-6" style="margin: 0;">
								<label class="control-label">'.translate('Alternate Contact').'</label>
								<input name="shipping_alternate_contact" value="" type="text" class="form-control"  />
							</div>';
            $html.='
						</fieldset>
					</form>';
        }
        return $html;
    }

    public function planshippingMethod(Request $request){
        $json = json_encode($request->all());
        setcookie('shipping_address', $json,time()+31536000,'/');
        $records = DB::select("select * from shipping where postcodes LIKE '%".$request->pincode."%'");
        $html = '<form>';
        $i = 0;
        foreach($records as $record){
            if($i == 0) {
                $html .= '<div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="' . $record->id . '" name="shippingMethod" value="'.$record->id.'" checked>
                          <label class="custom-control-label" for="' . $record->id . '">' . $record->name . ' (' . c($record->cost) . ')</label>
                    </div>';
            }else{
                $html .= '<div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="' . $record->id . '" name="shippingMethod" value="'.$record->id.'">
                          <label class="custom-control-label" for="' . $record->id . '">' . $record->name . ' (' . c($record->cost) . ')</label>
                    </div>';
            }
            $i++;
        }
        $html.='</form';
        return $html;
    }
    public function planpaymentMethod(Request $request){
        $json = json_encode($request->all());
        setcookie('shipping_method', $json,time()+31536000,'/');
        $records = DB::select("select * from payments where active = 1");
        $html = '<form>';
        $i = 0;
        foreach($records as $record){
            if($i == 0) {
                $html .= '<div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="' . $record->id . '" name="paymentMethod" value="' . $record->id . '" checked>
                          <label class="custom-control-label" for="' . $record->id . '">' . $record->title . '</label>
                          ';
                if ($record->options != '{}') {
                    $data = json_decode($record->options);
                    $html .= '<blockquote>';
                    foreach ($data as $key => $value) {
                        if ($value != '')
                            $html .= $key . ': ' . $value . '<br>';
                    }
                    $html .= '</blockquote>';
                }
                $html .= '
                    </div>';
            }else{
                $html .= '<div class="custom-control custom-radio">
                          <input type="radio" class="custom-control-input" id="' . $record->id . '" name="paymentMethod" value="' . $record->id . '">
                          <label class="custom-control-label" for="' . $record->id . '">' . $record->title . '</label>
                          ';
                if ($record->options != '{}') {
                    $data = json_decode($record->options);
                    $html .= '<blockquote>';
                    foreach ($data as $key => $value) {
                        if ($value != '')
                            $html .= $key . ': ' . $value . '<br>';
                    }
                    $html .= '</blockquote>';
                }
                $html .= '
                    </div>';
            }
            $i++;
        }
        $html.='</form';
        return $html;
    }
    public function planconfirmOrder(Request $request){
        $json = json_encode($request->all());
        setcookie('payment_method', $json,time()+31536000,'/');
        $layout_id = session('layout_id');
        $addons = session('adons');
        $package = session('package');
        $totalprice = session('package_price');
        $response['header'] = '';
        $html = '<div class="table-responsive">
            <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Plan Name</th>
                        <th>Package Name</th>
                        <th>Addons Detail</th>
                        <th class="text-right">Package Price</th>
                        <th class="text-right">Addons Price</th>
                        <th class="text-right">Total Price</th>
                        <th class="text-right">Tax(%)</th>
                        <th class="text-right">Tax Amount</th>
                        <th class="text-right">Total Amount</th>
                      </tr>
                    </thead>';
        $serv = DB::select("SELECT * FROM layout_plans WHERE id = '".$layout_id."'")[0];
        $pack = '';
        $pprice = 0.00;
        if($package == '1'){
            $pack = $serv->p1_name;
            $pprice = $serv->p1_price;
        }elseif($package == '2'){
            $pack = $serv->p2_name;
            $pprice = $serv->p2_price;
        }elseif($package == '3'){
            $pack = $serv->p3_name;
            $pprice = $serv->p3_price;
        }
        if($pprice == '') {
            $pprice = $serv->price;
        }else{
            $pprice = floatval($pprice);
        }
        if($totalprice == '') {
            $totalprice = 0.00;
        }else{
            $totalprice = floatval($totalprice);
        }

        $aprice = $totalprice - $pprice;
        $tax = 0;
        $taxamt= 0;
        $totaladonprice = 0;
        $taxprice = 0;
        $html.='<tr>
            <td>'.$serv->title.'</td>
            <td>'.$pack.'</td>
            <td>';
        if($addons !== '') {
            $ad = explode(',', $addons);
            foreach ($ad as $a) {
                $as = DB::select("SELECT * FROM layout_addon WHERE id = '" . $a . "'")[0];
                $html .= $as->name . ' - ' . c($as->price) . '<br>';
                $totaladonprice += $as->price;
            }
        }else{
            $html.='No addons selected.';
        }
            $html.= '</td>
            <td class="text-right">'.c($pprice).'</td>';

            $html .= '<td class="text-right">' . c($totaladonprice);

        if($serv->tax !== null) {
            $tax = $serv->tax;
            $taxa = 1 + ($tax / 100);
            $taxprice = ($pprice + $totaladonprice) - ($pprice + $totaladonprice)/$taxa;

        } else { $tprice = $totalprice; }
            $html.= '</td>
            <td class="text-right">'.c(($pprice + $totaladonprice)).'</td>
            <td class="text-right">'.$tax.'</td>
            <td class="text-right">'.c(number_format($taxprice, 2, '.', '')).'</td>
            <td class="text-right">'.c(($pprice + $totaladonprice)).'</td>
        </tr>';

        $html.='<tr><td colspan="9" class="alert-warning">Note : Prices shown in above table are inclusive of all taxes.</td></tr></table></div>';

        return $html;
    }

    public function planconfirm(){
        $datax = array();

        $datax['user_id'] = customer('id');

        $datax['plan_id'] = session('layout_id');
        $datax['addon_id'] = session('adons');
        $datax['package_id'] = session('package');
        $datax['package_price'] = session('package_price');
        $datax['status'] = 'Pending';
        $datax['address'] = $_COOKIE['shipping_address'];


        $sp = 0.00;
        if(isset($_COOKIE["shipping_method"])){
            $sm = json_decode($_COOKIE["shipping_method"]);
            $meth = $sm->shippingMethod;
            if (DB::select("SELECT COUNT(*) as count FROM shipping WHERE id = " . $meth)[0]->count > 0) {
                $shipping = DB::select("SELECT * FROM shipping WHERE id = " . $meth)[0];
                $sp = $shipping->name;
            }

        }
        $datax['shipping_method'] = $sp;

        $paid = array();
        if(isset($_COOKIE['payment_method'])){
            $pm = json_decode($_COOKIE['payment_method']);
            $pmId = $pm->paymentMethod;
            if (DB::select("SELECT COUNT(*) as count FROM payments WHERE id = " . $pmId)[0]->count > 0) {
                $payment = DB::select("SELECT * FROM payments WHERE id = " . $pmId)[0];
                $paid['method'] = $payment->code;
                if($payment->code == 'cash') {
                    $paid['payment_status'] = 'paid';
                }else{
                    $paid['payment_status'] = 'unpaid';
                }
            }

        }
        $datax['payment_method'] = json_encode($paid);
        $datax['created'] = date('Y-m-d');
        $order = DB::table('layout_order')->insertGetId($datax);
        if($order)
            return 'success';
        else
            return 'failure';
    }

    public function products()
	{
		// Apply the product filters
		$where = array();
		if (!empty($_GET['min']) && !empty($_GET['max']))
		{
			$where['price'] = "price BETWEEN '".escape($_GET['min'])."' AND '".escape($_GET['max'])."'";
		}
		if (!empty($_GET['search']))
		{
			$where['search'] = "(title LIKE '%".escape($_GET['search'])."%' OR text LIKE '%".escape($_GET['search'])."%')";
		}
		if (!empty($_GET['cat']))
		{
			$category = DB::select("SELECT * FROM category WHERE id = '".escape($_GET['cat'])."'")[0];
			$categories[] = $category->id;
			if ($category->parent == 0){
				$childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
				foreach ($childs as $child){
					$categories[] = $child->id;
				}
			}
			$where['cat'] = "category IN (".implode(',',$categories).")";
		}
		$where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
		$products = DB::select("SELECT * FROM products $where ORDER BY title DESC LIMIT 4");
		$response = array();
		$response['products'] = array();
		// fetch products and return them in json format
		foreach ($products as $row){
			$data['id'] = $row->id;
			$data['images'] = url('/assets/products/'.image_order($row->images));
			$data['title'] = $row->title;
			$data['text'] = mb_substr(translate($row->text),0,200);
			$data['path'] = 'product/'.path($row->title,$row->id);
			$data['price'] = c($row->price);
			$data['in'] = "Products";
			array_push($response["products"], $data);
		}



        // Apply the product filters
        $where = array();
        if (!empty($_GET['search']))
        {
            $where['search'] = "(title LIKE '%".escape($_GET['search'])."%' OR description LIKE '%".escape($_GET['search'])."%')";
        }
        $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
        $products = DB::select("SELECT * FROM services $where ORDER BY title DESC LIMIT 4");
        // fetch products and return them in json format
        foreach ($products as $row){
            $data['id'] = $row->id;
            $data['images'] = url('/assets/services/'.image_order($row->images));
            $data['title'] = $row->title;
            $data['text'] = mb_substr(translate($row->description),0,200);
            $data['path'] = 'services/'.path($row->title,$row->id);
            $data['price'] = c($row->price);
            $data['in'] = "Services";
            array_push($response["products"], $data);
        }
		return json_encode($response);
	}
    public function posts()
	{
		// Apply the posts filter
		$where = array();
		if (!empty($_GET['search']))
		{
			$where['search'] = "(title LIKE '%".escape($_GET['search'])."%' OR content LIKE '%".escape($_GET['search'])."%')";
		}
		$where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
		$posts = DB::select("SELECT * FROM blog $where ORDER BY id DESC ");
		$response = array();
		$response['posts'] = array();
		// fetch posts and return them in json format
		foreach ($posts as $row){
			$data['id'] = $row->id;
			$data['image'] = url('/assets/products/'.$row->images);
			$data['title'] = $row->title;
			$data['content'] = mb_substr(translate($row->content),0,200);
			$data['path'] = 'blog/'.path($row->title,$row->id);
			$data['time'] = timegap($row->time).translate(' ago');
			$data['timestamp'] = $row->time;
			array_push($response["posts"], $data);
		}
		return json_encode($response);
	}
	public function add(Request $request)
	{
		if (!isset($request->id)) { exit; }
		$id = $request->id;
		$product = DB::select("SELECT * FROM products WHERE id = ".$id)[0];
		$q = isset($request->q) ? (int)$request->q : "1";
		$min = isset($request->min) ? (int)$request->min : 0;
		$max = isset($request->max) ? (int)$request->max : 0;
		$v = $request->v;
		$variants = array();
        $variant_list = array();
		if($v){
			$vex = explode(',',$v);
			$qex = explode(',',$request->q);
			$pex = explode(',',$request->price);
			$uex = explode(',',$request->units);

			foreach($vex as $key=>$var){
			    $vard = ProductVariant::where('id',$var)->first();
                $discount = ProductDiscount::where('quantity','<=',$q)->where('product_id',$id)->first();
                $title = $vard->variant_title;
                $price = $product->price;
                if($product->price > $product->sale){
                    $price = $product->sale;
                }
                if($discount){
                    if($discount->discount_type == 'Flat') {
                        $price = ($vard->price + $price) - $discount->discount;
                    }else{
                        $dis = number_format((($vard->price+$price)*$discount->discount)/100, 2, '.', '');
                        $price = ($vard->price+$price)-$dis;
                    }
                }
                $variant_list[] = array('title'=>$title, 'price'=>$pex[$key], 'quantity'=>$qex[$key], 'unit'=>$uex[$key]);
			}
		}
		$qt = explode(',',$request->q);
		if($min == 0){
			if($q<=$min){
				return 'invalid';
			}
		}else{
			if($q<$min){
				return 'minimum';
			}
		}
        $qtot = 0;
        if($v) {
            foreach ($qt as $qtt){
                $qtot += (int)$qtt;
            }
        }else{
            $qtot = $q;
        }
        if($max == 0){
            if($q<=$max){
                return 'invalid';
            }
        }else{
            if($qtot>$max){
                return 'maximum';
            }
        }
        $cart_variants[$id] = json_encode($variant_list);
		$cart_items = array();
		$cart_items[$id] = $q;
		$options = json_decode(DB::select("SELECT options FROM products WHERE id = ".$id)[0]->options,true);
		$option_list = array();
        $quantity = "";
		if(!empty($options)){
			foreach ($options as $option) {
				$name = $option['name'];
				$title = $option['title'];
				$option_list[$name] = array('title'=>$title,'value'=>(isset($_GET[$name]) ? ($option['type'] == 'multi_select' ? implode(' , ',$_GET[$name]) : $_GET[$name]) : ''));
			}
		}
		$cart_options[$id] = json_encode($option_list);
		$bulk_discounts = DB::select("SELECT * FROM product_discount where quantity<='".$q."' AND product_id = '".$id."' ORDER BY quantity DESC LIMIT 1");
		$variantsx = DB::select("SELECT * FROM product_variants WHERE display = '1' AND product_id = ".$id);
//		$variant_list = array();
//		if(count($bulk_discounts) > 0){
//            $discount = $bulk_discounts[0];
//			if(!empty($variantsx)){
//				foreach ($variantsx as $variant) {
//					$title = $variant->variant_title;
//					$price = $product->price;
//					if($product->price > $product->sale){
//						$price = $product->sale;
//					}
//					if($discount->discount_type == 'Flat'){
//						$price = ($variant->price+$price)-$discount->discount;
//					}else{
//						$dis = number_format((($variant->price+$price)*$discount->discount)/100, 2, '.', '');
//						$price = ($variant->price+$price)-$dis;
//					}
//                    if(isset($variants[$variant->id])){
//                        $quantity = $variants[$variant->id];
//                    }
//					$variant_list[$variant->id] = array('title'=>$title,'price'=>$price, 'quantity'=>$quantity);
//				}
//			}else{
//
//            }
//		}else{
//		    foreach($variantsx as $variant){
//				$title = $variant->variant_title;
//				$price = $product->price;
//				if($product->price > $product->sale){
//					$price = $product->sale;
//				}
//				$price = $price + $variant->price;
//                if(isset($variants[$variant->id])){
//                    $quantity = $variants[$variant->id];
//                }
//				$variant_list[$variant->id] = array('title'=>$title,'price'=>$price, 'quantity'=>$quantity);
//			}
//		}

		if (DB::select("SELECT quantity FROM products WHERE id = ".$id)[0]->quantity < $q){
			// Throw stock unavailable error
			return 'unavailable';
		}
		// Check if the item is in the array, if it is, update quantity
		if(array_key_exists($id, $this->cart)){
			foreach($this->cart as $key => $value){
				$cart_items[$key] = $value;
			}
			unset($cart_items[$id]);
			if ($q > 0){
				$cart_items[$id] = $q;
			}
			$json = json_encode($cart_items, true);
			// Update cart cookie
			setcookie('cart', $json,time()+31536000,'/');
			foreach($this->cart_options as $key => $value){
				$cart_options[$key] = $value;
			}
			unset($cart_options[$id]);
			$cart_options[$id] = json_encode($option_list);
			$json = json_encode($cart_options, true);
			// Update cart options cookie
			setcookie('cart_options', $json,time()+31536000,'/');
			foreach($this->cart_variants as $key => $value){
				$cart_variants[$key] = $value;
			}
			unset($cart_variants[$id]);
			$cart_variants[$id] = json_encode($variant_list);
			$json = json_encode($cart_variants, true);
			setcookie('cart_variants', $json,time()+31536000,'/');
			return 'updated';
		} else {
			if(count($this->cart)>0){
				foreach($this->cart as $key=>$value){
					// add old item to array, it will prevent duplicate keys
					$cart_items[$key]=$value;
				}
			}
			$json = json_encode($cart_items, true);
			// Update cart cookies
			setcookie('cart', $json,time()+31536000,'/');
			if(count($this->cart_options)>0){
				foreach($this->cart_options as $key=>$value){
					// add old item to array, it will prevent duplicate keys
					$cart_options[$key]=$value;
				}
			}
			$json = json_encode($cart_options, true);
			setcookie('cart_options', $json,time()+31536000,'/');
			
			if(count($this->cart_variants)>0){
				foreach($this->cart_variants as $key=>$value){
					
					$cart_variants[$key]=$value;
				}
			}
			$json = json_encode($cart_variants, true);
			setcookie('cart_variants', $json,time()+31536000,'/');
			return 'success';
		}
	}

    public function book()
    {
        if (!isset($_GET['id'])) { exit; }
        $id = $_GET['id'];
        $type = $_GET['type'];
        $product = DB::select("SELECT * FROM products WHERE id = ".$id)[0];
        $q = isset($_GET['q']) ? (int)$_GET['q'] : "1";
        $min = isset($_GET['min']) ? (int)$_GET['min'] : 0;

        $variants = array();
        if($q == 0){
            $variants = (array)$_GET['quantity'];
            foreach($variants as $var){
                $q = $q + $var;
            }
        }
        if($min == 0){
            if($q<=$min){
                return 'invalid';
            }
        }else{
            if($q<$min){
                return 'minimum';
            }
        }
        $cart_variants[$id] = $variants;
        $cart_items = array();
        $cart_items[$id] = $q;
        $options = json_decode(DB::select("SELECT options FROM products WHERE id = ".$id)[0]->options,true);
        $option_list = array();
        if(!empty($options)){
            foreach ($options as $option) {
                $name = $option['name'];
                $title = $option['title'];
                $option_list[$name] = array('title'=>$title,'value'=>(isset($_GET[$name]) ? ($option['type'] == 'multi_select' ? implode(' , ',$_GET[$name]) : $_GET[$name]) : ''));
            }
        }
        $cart_options[$id] = json_encode($option_list);
        $bulk_discounts = DB::select("SELECT * FROM product_discount where quantity<='".$q."' AND product_id = '".$id."' ORDER BY quantity DESC LIMIT 1");
        $variantsx = DB::select("SELECT * FROM product_variants WHERE display = '1' AND product_id = ".$id);
        $variant_list = array();
        if(count($bulk_discounts) > 0){
            $discount = $bulk_discounts[0];
            if(!empty($variantsx) && $type == 'variants'){
                foreach ($variantsx as $variant) {
                    $title = $variant->variant_title;
                    $price = $product->institutional_price;

                    if($discount->discount_type == 'Flat'){
                        $price = ($variant->price+$price)-$discount->discount;
                    }else{
                        $dis = number_format((($variant->price+$price)*$discount->discount)/100, 2, '.', '');
                        $price = ($variant->price+$price)-$dis;
                    }
                    $quantity = $variants[$variant->id];
                    $variant_list[$variant->id] = array('title'=>$title,'price'=>$price, 'quantity'=>$quantity);
                }
            }
        }else{
            if(!empty($variantsx) && $type == 'variants') {
                foreach ($variantsx as $variant) {
                    $title = $variant->variant_title;
                    $price = $product->institutional_price;
                    $price = $price + $variant->price;
                    $quantity = $variants[$variant->id];
                    $variant_list[$variant->id] = array('title' => $title, 'price' => $price, 'quantity' => $quantity);
                }
            }
        }
        $cart_variants[$id] = json_encode($variant_list);
        if (DB::select("SELECT quantity FROM products WHERE id = ".$id)[0]->quantity < $q){
            // Throw stock unavailable error
            return 'unavailable';
        }
        $data['items'] = json_encode($cart_items, true);
        // Update cart cookies
        $data['options'] = json_encode($cart_options, true);
        $data['variants'] = json_encode($cart_variants, true);
        $data['product_id'] = $id;
        $data['user_id'] = customer('id');
        $data['price'] = $product->institutional_price;
        DB::table('bookings')->insert($data);
        return 'success';
    }
	public function cart()
	{
		$cart_items = $this->cart;
		$response["count"] = count($cart_items);
		$response['header'] = translate('Cart').'<button class="pull-right" onclick="$(\'#cart\').toggleClass(\'cart-open\');$(\'#cart\').toggle(\'300\');"><i class="icon-close"></i></button>';
		if($response["count"] > 0){
			$ids = "";
			foreach($cart_items as $id=>$name){
				$ids .= $id . ",";
			}
			$ids = rtrim($ids, ',');
			$cart_products = DB::select("SELECT * FROM products WHERE id IN ({$ids}) ORDER BY id DESC ");
			$total_price = 0;
			$total_p = 0;
			$response["products"] = array();
			foreach ($cart_products as $row){
				$q = $this->cart[$row->id];
				$options = json_decode($this->cart_options[$row->id],true);
				$option_array = array();
				foreach ($options as $option) {
                    $option_array[] = '<i>'.$option['title'].'</i> : '.$option['value'];
                }
                $data['id'] = $row->id;
				$data['images'] = image_order($row->images);
				$data['title'] = $row->title;
				$data['price'] = c($row->price);
				$data['quantity'] = $q;
				$data['options'] = implode('<br/>',$option_array);
				$data['total'] = 0.00;
				$data['variants'] = '';
				$variants = array();
				if(isset($this->cart_variants[$row->id]))
				$variants = json_decode($this->cart_variants[$row->id],true);
				$variant_array = array();
				$p_var = DB::select("SELECT * FROM product_variants WHERE product_id = '".$row->id."' ORDER BY id ASC ");
				$price_total = 0;
				$price = 0;
				if(count($variants) > 0){
					foreach ($variants as $var) {
						if(!empty($var['quantity']) && $var['quantity'] !== '0' && $var['quantity'] !== ''){
							$variant_array[] = '<i>'.variantTitle($var['title']).'</i> : '.$var['quantity'].' '.$var['unit'].' x '.c($var['price']);
							$price_total += (int)$var['price']*(int)$var['quantity'];
						}
					}
					$data['variants'] = implode('<br/>',$variant_array);
				}
				$price = $row->price;
				if($row->price > $row->sale){
					$price = $row->sale;
				}
				$data['total'] = c($price*$q);
				if($price_total){
					$price = $price_total;
					$data['total'] = c($price);
				}
				$data['price'] = c($price);
				array_push($response["products"], $data);
				$total_p+= $q;
			}
		}
		if (isset($_COOKIE["coupon"])){
			if (DB::select("SELECT COUNT(*) as count FROM coupons WHERE code = '".$_COOKIE['coupon']."'")[0]->count > 0){
				$coupon = DB::select("SELECT * FROM coupons WHERE code = '".$_COOKIE["coupon"]."'")[0];
				$response["coupon"] = '<button data-toggle="collapse" class="btn-coupon" data-target="#coupon">'.translate('Coupon').' : <b>'.$_COOKIE["coupon"].' ('.$coupon->discount.$coupon->type.')</b></button>
				<div id="coupon" class="collapse">
				<input placeholder="'.translate('Coupon code').'" value="'.$_COOKIE["coupon"].'" class="coupon" id="code"/>
				<button id="apply" class="btn-apply">'.translate('Apply').'</button>
				</div>';
			}
		} else {
			$response["coupon"] = '
			<button data-toggle="collapse" class="btn-coupon" data-target="#coupon">'.translate('Have a coupon code ?').'</button>
			<div id="coupon" class="collapse">
			<input placeholder="'.translate('Coupon code').'" class="coupon" id="code"/>
			<button id="apply" class="btn-apply">'.translate('Apply').'</button>
			</div>';
		}
		return json_encode($response);
	}
	public function remove()
	{
		// Remove product from cart
		$cart_items = array();
		foreach($this->cart as $key=>$value){
			$cart_items[$key] = $value;
		}
		unset($cart_items[$_GET['id']]);
		$json = json_encode($cart_items, true);
		setcookie('cart', $json,time()+31536000,'/');
		$cart_options = array();
		foreach($this->cart_options as $key=>$value){
			$cart_options[$key] = $value;
		}
		unset($cart_options[$_GET['id']]);
		$json = json_encode($cart_options, true);
		setcookie('cart_options', $json,time()+31536000,'/');
		
		$cart_variants = array();
		foreach($this->cart_variants as $key=>$value){
			$cart_variants[$key] = $value;
		}
		unset($cart_variants[$_GET['id']]);
		$json = json_encode($cart_variants, true);
		setcookie('cart_variants', $json,time()+31536000,'/');
		return 'removed';
	}
	public function checkout()
	{
		$response = array();
		// Checkout header
		$response['header'] = '<button class="pull-left load-cart"><i class="icon-arrow-left"></i></button>'.translate('Checkout').'<button class="pull-right" onclick="$(\'#cart\').toggleClass(\'cart-open\');$(\'#cart\').toggle(\'300\');"><i class="icon-close"></i></button>';
		// Get custom fields from database
		$fields = DB::select("SELECT * FROM fields ORDER BY id ASC ");
		$response[] = '<form id="customer"><div id="errors"></div>';
		foreach ($fields as $field){
			if ($field->code == 'country'){
				// Return country selector if code of field is country
				$options = '';
				$countries = DB::select("SELECT * FROM country ORDER BY nicename ASC");
				foreach ($countries as $country){
					$options .= '<option value="'.$country->iso.'" data-phone="'.$country->phonecode.'">'.$country->nicename.'</option>';
				}
				$response[] = '<div class="form-group">
					<label class="control-label">'.translate($field->name).'</label>
					<select id="country" name="'.$field->code.'" class="form-control" type="text">'.$options.'</select>
				</div>';
			} else {
				$response[] = '<div class="form-group">
					<label class="control-label">'.translate($field->name).'</label>
					<input name="'.$field->code.'" value="'.customer($field->code).'" class="form-control" type="'.($field->code == 'mobile' ? 'number' : 'text').'">
				</div>';
			}
		}
		$response[] = '</form>';
		return json_encode($response);
	}
	public function payment()
	{
		$response = array();
		$error = '';
		$success = '';
		// Payment header
		$response['header'] = translate('Payment').'<button class="pull-right" onclick="$(\'#cart\').toggleClass(\'cart-open\');$(\'#cart\').toggle(\'300\');"><i class="icon-close"></i></button>';
		$response['message'] = '';
		$email_fields = '';
		$email_products = '';
		$fields = DB::select("SELECT * FROM fields ORDER BY id ASC ");
		foreach ($fields as $field){
			// Retrieve POSTed data , or throw error if they aren't filled
			if(!empty($_POST[$field->code])){
				$data[$field->code] = escape($_POST[$field->code]);
				$email_fields .= $data[$field->code].'<br />';
			} else {
				$response['error'] = 'true';
				$response['message'] .= translate($field->name).' '.translate('field is required').'<br/>';
			}
		}
		if (isset($response['error'])){
			return json_encode($response);
		}
		$cart_items = $this->cart;
		if(count($cart_items) > 0){
			$ids = "";
			foreach($cart_items as $id=>$name){
				$ids = $ids . $id . ",";
			}
			$ids = rtrim($ids, ',');
			$cart_products = DB::select("SELECT * FROM products WHERE id IN ({$ids})  ORDER BY id DESC ");
			$total = 0;
			$products = array();
			foreach ($cart_products as $row){
				$q = $this->cart[$row->id];
				$product['id'] = $row->id;
				$product['quantity'] = $q;
				$product['options'] = $this->cart_options[$row->id];
				array_push($products,$product);
				$total += $row->price * $q;
				$email_products .= '<div>'.$row->title.' x '.$q.'<b style="float:right">'.c($row->price * $q).'</b></div><hr>';
				// Update product quantity
				DB::update("UPDATE products SET quantity = quantity - $q WHERE id = '".$product['id']."'");
			}
		} else {
			return 'Your cart is empty';
		}
		$sub_total = $total;
		$coupon_response = '';
		$data['coupon'] = '';
		if (isset($_COOKIE['coupon'])) {
			// Check if coupon is valid
			if (DB::select("SELECT COUNT(*) as count FROM coupons WHERE code = '".$_COOKIE['coupon']."'")[0]->count > 0){
				$coupon_data = DB::select("SELECT * FROM coupons WHERE code = '".$_COOKIE["coupon"]."'")[0];
				if ($coupon_data->type == '%'){
					$total = $total - ($total * $coupon_data->discount / 100);
				} else {
					$total = $total - $coupon_data->discount;
				}
				$email_products .= '<div>Coupon discount<b style="float:right">'.$coupon_data->discount.$coupon_data->type.'</b></div><hr>';
				$coupon_response = '<div>Coupon discount <b>'.$coupon_data->discount.$coupon_data->type.'</b></div>';
				$data['coupon'] = $_COOKIE['coupon'];
			}
		}
		$shipping_response = '';
		$data['shipping'] = '0';
		if (!empty($data['country'])) {
			// Add shipping cost
			if (DB::select("SELECT COUNT(*) as count FROM shipping WHERE country = '".$data['country']."'")[0]->count > 0){
				$shipping_data = DB::select("SELECT * FROM shipping WHERE country = '".$data['country']."'")[0];
				$total = $total + $shipping_data->cost;
				$email_products .= '<div>Shipping<b style="float:right">'.c($shipping_data->cost).'</b></div><hr>';
				$shipping_response = '<div>Shipping <b>'.c($shipping_data->cost).'</b></div>';
				$data['shipping'] = $shipping_data->cost;
			}
		}
		$data['products'] = json_encode($products,true);
		$data['customer'] = (session('customer') == '' ? '0' : customer('id'));
		$data['summ'] = $total;
		$data['time'] = time();
		$data['date'] = date('Y-m-d');
		$data['stat'] = 1;
		$data['payment'] = '{"payment_status":"unpaid"}';
		$columns = implode(", ",array_keys($data));
		$values  = implode("', '", $data);
		// Update orders per country
		DB::update("UPDATE country SET orders = orders+1 WHERE iso = '".$data['country']."'");
		// Save order in database
		$order = DB::table('orders')->insertGetId($data);
		// Send an email to customer
		//mailing('order',array('buyer_name'=>$data['name'],'buyer_email'=>$data['email'],'buyer_fields'=>$email_fields,'name'=>$this->cfg->name,'address'=>$this->cfg->address,'phone'=>$this->cfg->phone,'products'=>$email_products,'total'=>c($total)),'Order Confirmation #'.$order,$data['email']);
		// Get payment methods
		$methods = DB::select("SELECT * FROM payments WHERE active = 1 ORDER BY id ASC ");
		$response[] = '<div class="payment_total"><div>Sub total <b>'.c($sub_total).'</b></div>'.$coupon_response.$shipping_response.'<div>Total <b>'.c($total).'</b></div></div><div class="payments">';
		foreach($methods as $method){
			// Get method options and include it 
			$options = json_decode(stripslashes($method->options), true);
			include app_path()."/Plugins/".$method->code."/checkout.php";
		}
		$response[] = '</div>';
		echo json_encode($response);
	}
	public function pay()
	{
		// Process order payment
		$order = (int)$_GET['order'];
		if (!isset($_GET['method']) || !isset($_GET['order'])){
			return 'Invalid parameters';
		}
		if (!in_array($_GET['method'],array('stripe','cash','bank'))){
			return 'Invalid method';
		}
		if (DB::select("SELECT COUNT(*) as count FROM orders WHERE id = {$order}")[0]->count == 0){
			return 'Invalid order';
		}
		$error = '';
		$success = '';
		$response = array();
		$unpaid = false;
		$will_pay = false;
		$response['message'] = '';
		$method = $_GET['method'];
		$cart_items = json_decode(DB::select("SELECT products FROM orders WHERE id = {$order}  ORDER BY id DESC ")[0]->products,true);
		if(count($cart_items) > 0){
			$ids = "";
			foreach($cart_items as $cart_item){
				$ids = $ids . $cart_item['id'] . ",";
				$quantity[$cart_item['id']] = $cart_item['quantity'];
			}
			$ids = rtrim($ids, ',');
			$cart_products = DB::select("SELECT * FROM products WHERE id IN ({$ids})  ORDER BY id DESC ");
			$total=0;
			$products = array();
			foreach ($cart_products as $row){
				$total += $row->price * $quantity[$row->id];
			}
		}
		$sub_total = $total;
		if (isset($_COOKIE['coupon'])) {
			// Check if coupon is valid
			if (DB::select("SELECT COUNT(*) as count FROM coupons WHERE code = '".$_COOKIE['coupon']."'")[0]->count > 0){
				$coupon_data = DB::select("SELECT * FROM coupons WHERE code = '".$_COOKIE["coupon"]."'")[0];
				if ($coupon_data->type == '%'){
					$total = $total - ($total * $coupon_data->discount / 100);
				} else {
					$total = $total - $coupon_data->discount;
				}
			}
		}
		$order_shipping = DB::select("SELECT shipping FROM orders WHERE id = ".$order)[0]->shipping;
		$total = $total + $order_shipping;
		$payment_method = DB::select("SELECT * FROM payments WHERE code='".$method."'")[0];
		if ($payment_method->active == 1){
			include app_path()."/Plugins/".$payment_method->code."/payments.php";
		} else {
			return 'Method inactive';
		}
		if ($unpaid == true) {
			// If the order isn't paid show payment methods again
			$methods = DB::select("SELECT * FROM payments WHERE active = 1 ORDER BY id ASC ");
			$response['header'] = translate('Payment').'<button class="pull-right" onclick="$(\'#cart\').toggleClass(\'cart-open\');$(\'#cart\').toggle(\'300\');"><i class="icon-close"></i></button>';
			$response[] = '<div class="payments">';
			foreach($methods as $method){
				// Get method options and include it 
				$options = json_decode(stripslashes($method->options), true);
				include app_path()."/Plugins/".$method->code."/checkout.php";
			}
		} else {
			// If the order has been paid , Show success message
			setcookie('cart', '',time()+31536000,'/');
			if ($will_pay == false) {
				// Send download link for digital products if paid successfully
				$this->send_downloads($order);
			}
			$response['header'] = translate('Success').'<button class="pull-right" onclick="$(\'#cart\').toggleClass(\'cart-open\');$(\'#cart\').toggle(\'300\');"><i class="icon-close"></i></button>';
			$response[] = '<div class="payment-success"><h3>'.translate('Thank you').'</h3>'.translate('Your order has been placed successfully').'</div>';
		}
		$response[] = '</div>';
		return json_encode($response);
	}
	public function paypal(){
		// Redirect to paypal to complete payment
		$method = DB::select("SELECT options FROM payments WHERE active = 1 AND code = 'paypal' ORDER BY id ASC ")[0]->options;
		$paypal_email = json_decode(stripslashes($method), true)['email'];
		if(isset($_GET['order']) || isset($_POST['custom'])){
			$order_id = isset($_GET['order']) ? $_GET['order'] : $_POST['custom'];
			$order_json = DB::select("SELECT * FROM orders WHERE id = ".$order_id)[0];
			$coupon = $order_json->coupon;
			$shipping = $order_json->shipping;
			$order = json_decode($order_json->products,true);
			$ids = "";
			foreach($order as $o){
				$ids = $ids . $o['id'] . ",";
				$q[$o['id']] = $o['quantity'];
			}
			$ids = rtrim($ids, ',');
			// Get order products
			$product = DB::select("SELECT * FROM products WHERE id IN ({$ids}) ORDER BY id DESC ");
			$items = '';
			$i = 1;
			foreach ($product as $row){
				$items .= "item_name_".$i."=".urlencode(translate($row->title))."&";
				$items .= "amount_".$i."=".urlencode($row->price)."&";
				$items .= "quantity_".$i."=".urlencode($q[$row->id])."&";
				$i++;
			}
			$couponquery = '';
			if (!empty($coupon)) {
				// Check if coupon is valid
				if (DB::select("SELECT COUNT(*) as count FROM coupons WHERE code = '".$coupon."'")[0]->count > 0){
					$coupon_data = DB::select("SELECT * FROM coupons WHERE code = '".$coupon."'")[0];
					if ($coupon_data->type == '%'){
						$couponquery = "discount_rate_cart=".urlencode($coupon_data->discount)."&";
					} else {
						$couponquery = "discount_amount_cart=".urlencode($coupon_data['discount'])."&";
					}
				}
			}
			$shippingquery = "handling_cart=".urlencode($shipping)."&";
			// PayPal settings
			$return_url = url('/success');
			$cancel_url = url('/failed');
			$notify_url = url('/api/paypal');
			// Check if paypal request or response
			if (!isset($_POST["txn_id"]) && !isset($_POST["txn_type"])){
				$querystring = '';
				
				// Firstly Append paypal account to querystring
				$querystring .= "?cmd=_cart&";
				$querystring .= "upload=1&";
				$querystring .= "business=".urlencode($paypal_email)."&";
				// $querystring .= "shopping_url =".urlencode($url)."&";
				
				//The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
				$querystring .= $items;
				$querystring .= $couponquery;
				$querystring .= $shippingquery;
				$querystring .= "currency_code=USD&";
				// Append paypal return addresses
				$querystring .= "return=".urlencode(stripslashes($return_url))."&";
				$querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
				$querystring .= "notify_url=".urlencode($notify_url);
				$querystring .= "&custom=".$_GET['order'];			
				// Redirect to paypal IPN
				header('location:https://www.paypal.com/cgi-bin/webscr'.$querystring);
				exit();
			} else {
				// Response from Paypal
				// read the post from PayPal system and add 'cmd'
				$req = 'cmd=_notify-validate';
				foreach ($_POST as $key => $value) {
					$value = urlencode(stripslashes($value));
					$value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}',$value);// IPN fix
					$req .= "&$key=$value";
				}
				
				// assign posted variables to local variables
				$data['method'] 		= 'paypal';
				$data['payment_status'] 	= $_POST['payment_status'];
				$data['payment_amount'] 	= $_POST['mc_gross'];
				$data['payment_currency']	= $_POST['mc_currency'];
				$data['txn_id']			= $_POST['txn_id'];
				$data['receiver_email'] 	= $_POST['receiver_email'];
				$data['payer_email'] 		= $_POST['payer_email'];
				$data['order'] 			= $_POST['custom'];
				echo $payment = json_encode($data, true);
				
				// post back to PayPal system to validate
				$header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
				$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
				$header .= "Host: www.paypal.com\r\n";  // www.paypal.com for a live site
				$header .= "Content-Length: " . strlen($req) . "\r\n";
				$header .= "Connection: close\r\n\r\n";
				
				$fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30);
				
				if (!$fp) {
					// HTTP ERROR
					logger('HTTP ERROR');
				} else {
					fputs($fp, $header . $req);
					while (!feof($fp)) {
						$res = fgets($fp);
						// Payment verification
						if (strcmp($res, "VERIFIED") >= 0) {
							// another validation layer 
							if ($data['payment_status'] == 'Completed' && $data['receiver_email'] == $paypal_email) {
								// The payment is successful
								DB::update("UPDATE orders SET payment = '$payment' WHERE id = ".$data['order']);
								// Send download link if digital product
								$this->send_downloads($data['order']);
								// for debugging
								logger('Successful payment');
							} else {
								// Payment unsuccessful - for debugging
								logger("The payment isn't completed yet !".$data['payment_status'].$data['receiver_email']);
							}
						} else if (strcmp ($res, "INVALID") == 0) {
							// Payment invalid - for debugging
							logger("The payment is invalid !");
						}
					}
					
					fclose ($fp);
				}
			}
		} else {
			header('location:'.url(''));
		}
	}
	public function send_downloads($order){
		$order_info = DB::select("SELECT * FROM orders WHERE id = ".$order)[0];
		$order_products = json_decode($order_info->products,true);
		$ids = "";
		foreach($order_products as $product){
			$ids = $ids . $product['id'] . ",";
		}
		$ids = rtrim($ids, ',');
		// Get order products that can be downloaded
		$products = DB::select("SELECT * FROM products WHERE id IN ({$ids}) AND download != '' ORDER BY id DESC ");
		$email_downloads = '';
		foreach ($products as $row) {
			if ($row->download != ''){
				$email_downloads .= '<div>'.$row->title.'<b style="float:right"><a href="'.url('assets/downloads/'.$row->download).'">Download</a></b></div><hr>';
			}
		}
		if ($email_downloads != '') {
			mailing('download',array('downloads'=>$email_downloads),'Order Downloads #'.$order,$order_info->email);
		}
	}
	public function review(){
		if (empty($_POST['email']) || empty($_POST['name']) || empty($_GET['product']) || empty($_POST['review']) ||empty($_POST['rating'])){
			return 'All fields are required';
		} else {
			// escape review details and insert them into the database
			$name = escape(htmlspecialchars($_POST['name']));
			$rating = (int)$_POST['rating'];
			$review = escape(htmlspecialchars($_POST['review']));
			$product = (int)$_GET['product'];
			DB::insert("INSERT INTO reviews (name,rating,review,product,time,active) VALUE ('$name','$rating','$review','$product','".time()."','0')");
			return 'success';
		}
	}




	public function coupon(){
		// Check if coupon is valid and save it in cookies
		$code = htmlspecialchars($_GET['code']);
		if (DB::select("SELECT COUNT(*) as count FROM coupons WHERE code = '$code'")[0]->count > 0){
			setcookie('coupon', $code);
			return 'success';
		} else {
			return 'invalid';
		}
	}
	public function subscribe(){
		// Email subscribe
		$email = htmlspecialchars($_GET['email']);
		if (DB::select("SELECT COUNT(*) as count FROM subscribers WHERE email = '$email'")[0]->count > 0){
			return 'Already subscribed';
		} else {
			DB::insert("INSERT INTO subscribers (email) VALUE ('$email')");
			return 'successfully subscribed';
		}
	}
	public function orders(){
		$orders = DB::select("SELECT * FROM orders ORDER BY id DESC ");
		$fields = DB::select("SELECT code FROM fields ORDER BY id ASC");
		$response = array();
		$response['orders'] = array();
		// fetch reviews and return them in json format
		foreach ($orders as $row){
			$data['id'] = $row->id;
			// return data per field
			foreach($fields as $field){
				$code = $field->code;
				if ($code == 'country') {
					$row->$code = country($row->$code);
				}
				$data[$code] = $row->$code;
			}
			$data['products'] = array();
			$products = json_decode($row->products, true);
			if(count($products)>0){
				// search for products and return product data and selected quantity
				$ids = "";
				foreach($products as $item){
					$ids = $ids . $item['id'] . ",";
					$q[$item['id']] = $item['quantity'];
				}
				$ids = rtrim($ids, ',');
				$products = DB::select("SELECT * FROM products WHERE id IN (".$ids.")  ORDER BY id DESC ");
				$total_price=0;
				foreach ($products as $item){
					$product['id'] = $item->id;
					$product['title'] = $item->title;
					$product['price'] = $item->price;
					$product['quantity'] = $q[$item->id];
					$product['total'] = $item->price*$q[$item->id];
					array_push($data['products'], $product);
				}
			}
			$data['coupon'] = $row->coupon;
			$data['total'] = $row->summ;
			array_push($response["orders"], $data);
		}
		return json_encode($response);
	}
	public function reviews(){
		$reviews = DB::select("SELECT * FROM reviews ORDER BY id DESC ");
		$response = array();
		$response['reviews'] = array();
		// fetch reviews and return them in json format
		foreach ($reviews as $row){
			$data['id'] = $row->id;
			$data['name'] = $row->name;
			$data['name'] = $row->name;
			$data['email'] = $row->email;
			$data['rating'] = $row->rating;
			$data['review'] = $row->review;
			$data['time'] = timegap($row->time);
			$data['timestamp'] = $row->time;
			array_push($response["reviews"], $data);
		}
		return json_encode($response);
	}
	public function getStates(Request $request){
		$country_id = $request->id;
		$states = DB::select("SELECT * FROM states WHERE country_id='".$country_id."' ORDER BY name ASC");
		$response = array();
		foreach($states as $state){
			$response[] = array(
				'id' => $state->id,
				'name' => $state->name
			);
		}
		return json_encode($response);
	}
	public function getCities(Request $request){
		$state_id = $request->id;
        $cities = \App\City::where('state_id',$state_id)->where('is_available','1')->orderBy('name','ASC')->get();
		$response = array();
		foreach($cities as $city){
			$response[] = array(
				'id' => $city->id,
				'name' => $city->name
			);
		}
		return json_encode($response);
	}
}

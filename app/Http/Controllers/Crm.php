<?php

namespace App\Http\Controllers;

use App\Crm_Customer_Sec_Detail;
use App\CrmCall;
use App\CrmCustomer;
use App\CrmTickets;
use App\Customer;
use App\District;
use App\LeadOrder;
use App\State;
use App\TempAvg;
use App\Timeline;
use App\Transportor;
use Carbon\CarbonPeriod;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use ZipArchive;
use Illuminate\Support\Facades\Auth;
use Image;
use Vsmoraes\Pdf\Pdf;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class Crm extends Controller
{
    public $cfg;
    public $style;
    private $pdf;
    public $buttons;

    public function __construct(Pdf $pdf)
    {
        $this->cfg = DB::table('config')->where('id', '=', 1)->first();
        $this->style = DB::table('style')->first();
        $this->pdf = $pdf;
        $this->buttons = "['copy', 'excel', 'pdf']";
    }

    public function loginUsingToken()
    {
        if (isset($_GET['access_token'])) {
            if ($_GET['access_token'] != "") {
                if (DB::table('user')->where('secure', $_GET['access_token'])->count()) {
                    session(['crm' => $_GET['access_token']]);
                    return redirect('crm/index');
                }
            } else {
                return redirect()->away('activity:login');
            }
        } else {
            return redirect()->away('activity:login');
        }
    }

    public function postLogin(Request $request)
    {
        if (isset($request->email) && isset($request->password)) {
            $pass = md5($request->password);
            if (DB::table('user')->where('u_email', $request->email)->where('u_pass', $pass)->where('user_type', 'crm_user')->count() > 0) {
                $secure_id = md5(microtime());
                DB::update("UPDATE user SET secure = '$secure_id' WHERE u_email = '" . $request->email . "'");
                session(['crm' => $secure_id]);
                return response()->json(['success' => 'Login successfull', 'url' => 'crm/index', 'access_token' => $secure_id]);
            } else {
                return response()->json(['error' => 'Login id or Password in incorrect.']);
            }
        } else {
            return response()->json(['error' => 'All fields are mandatory!']);
        }
    }

    public function header($title = 'CRM', $area = false)
    {
        $title = $title . ' | ' . $this->cfg->name;
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        return view('crm/header')->with(compact('title', 'cfg', 'tp', 'area'))->render();
    }

    public function footer()
    {
        $tp = url("/assets/crm/");
        return view('crm/footer')->with(compact('tp'))->render();
    }

    public function login()
    {
        $error = '';
        if (isset($_POST['login'])) {
            // Check email and password and redirect to the dashboard
            $email = escape($_POST['email']);
            $pass = md5($_POST['password']);
            if (empty($email) or empty($pass)) {
                $error = '<div class="alert alert-dismissible alert-danger"> All fields are required </div>';
            } else {
                if (DB::table('user')->select(DB::raw('COUNT(*) as count'))->where('u_email', '=', $email)->where('u_pass', '=', $pass)->where('user_type', '=', 'crm_user')->count() > 0) {
                    // Generate a new secure ID for this session and redirect to dashboard
                    $secure_id = md5(microtime());

                    DB::update("UPDATE user SET secure = '$secure_id' WHERE u_email = '" . $email . "'");
                    session(['crm' => $secure_id]);
                    return redirect('crm/index');
                } else {
                    $error = '<div class="alert alert-dismissible alert-danger"> Wrong email or password </div>';
                }
            }
        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $data['tp'] = url("/themes/" . $this->cfg->theme);
        $title = 'CRM Login';
        return view('crm/login')->with(compact('cfg', 'error', 'tp', 'title', 'data'))->render();
    }

    public function logout()
    {
        // Clear the admin seesion
        session(['crm' => '']);
        return redirect('crm/login');
    }

    public function index()
    {
        //Program to update and sync timeline data in database
        /*$tickets = DB::table('crm_tickets')->get();
        $notes = DB::table('crm_notes')->get();
        $projects = DB::table('crm_customer_projects')->get();
        $visits = DB::table('crm_visits')->get();
        $messages = DB::table('crm_message_save')->get();
        $calls = DB::table('crm_call')->get();
        $products = DB::table('crm_products')->get();
        $promotional = DB::table('crm_promotional')->get();
        $luser = DB::table('user')->where('secure', session('crm'))->first();
        foreach($tickets as $ticket){
            $timeline = Timeline::where('customer_id', $ticket->crm_customer_id)->where('section', 'tickets')->where('section_id', $ticket->id)->count();
            if(!$timeline){
                $timel = new Timeline();
                $timel->customer_id = $ticket->crm_customer_id;
                $timel->section = 'tickets';
                $timel->section_id = $ticket->id;
                $timel->created_by = $luser->u_id;
                $timel->date = date('Y-m-d h:i:s', strtotime($ticket->created_date));
                $timel->save();
            }
        }
        foreach($notes as $note){
            $timeline = Timeline::where('customer_id', $note->crm_customer_id)->where('section', 'notes')->where('section_id', $note->id)->count();
            if(!$timeline){
                $timel = new Timeline();
                $timel->customer_id = $note->crm_customer_id;
                $timel->section = 'notes';
                $timel->section_id = $note->id;
                $timel->created_by = $luser->u_id;
                $timel->date = date('Y-m-d h:i:s', strtotime($note->select_date));
                $timel->save();
            }
        }
        foreach($projects as $project){
            $timeline = Timeline::where('customer_id', $project->crm_customer_id)->where('section', 'projects')->where('section_id', $project->id)->count();
            if(!$timeline){
                $timel = new Timeline();
                $timel->customer_id = $project->crm_customer_id;
                $timel->section = 'projects';
                $timel->section_id = $project->id;
                $timel->created_by = $luser->u_id;
                $timel->date = date('Y-m-d h:i:s', strtotime($project->created_date));
                $timel->save();
            }
        }
        foreach($visits as $visit){
            $timeline = Timeline::where('customer_id', $visit->crm_customer_id)->where('section', 'visits')->where('section_id', $visit->id)->count();
            if(!$timeline){
                $timel = new Timeline();
                $timel->customer_id = $visit->crm_customer_id;
                $timel->section = 'visits';
                $timel->section_id = $visit->id;
                $timel->created_by = $luser->u_id;
                $timel->date = date('Y-m-d h:i:s', strtotime($visit->select_date));
                $timel->save();
            }
        }
        foreach($messages as $message){
            $cust = DB::table('crm_customers')->whereRaw('FIND_IN_SET(?, contact_no)', $message->mobile)->first();
            if(!($cust === null)) {
                $timeline = Timeline::where('customer_id', $cust->id)->where('section', 'sms')->where('section_id', $message->id)->count();
                if (!$timeline) {
                    $timel = new Timeline();
                    $timel->customer_id = $cust->id;
                    $timel->section = 'sms';
                    $timel->section_id = $message->id;
                    $timel->created_by = $luser->u_id;
                    $timel->date = date('Y-m-d h:i:s', strtotime($message->created_at));
                    $timel->save();
                }
            }
        }
        foreach($calls as $call){
            $timeline = Timeline::where('customer_id', $call->crm_customer_id)->where('section', 'calls')->where('section_id', $call->id)->count();
            if(!$timeline){
                $timel = new Timeline();
                $timel->customer_id = $call->crm_customer_id;
                $timel->section = 'calls';
                $timel->section_id = $call->id;
                $timel->created_by = $luser->u_id;
                $timel->date = date('Y-m-d h:i:s', strtotime($call->select_date));
                $timel->save();
            }
        }
        foreach($products as $product){
            $timeline = Timeline::where('customer_id', $product->crm_customer_id)->where('section', 'products')->where('section_id', $product->id)->count();
            if(!$timeline){
                $timel = new Timeline();
                $timel->customer_id = $product->crm_customer_id;
                $timel->section = 'products';
                $timel->section_id = $product->id;
                $timel->created_by = $luser->u_id;
                $timel->date = date('Y-m-d h:i:s', strtotime($product->created_date));
                $timel->save();
            }
        }
	    foreach($promotional as $promo){
            $timeline = Timeline::where('customer_id', $promo->crm_customer_id)->where('section', 'products')->where('section_id', $promo->id)->count();
            if(!$timeline){
                $timel = new Timeline();
                $timel->customer_id = $promo->crm_customer_id;
                $timel->section = 'promotional';
                $timel->section_id = $promo->id;
                $timel->created_by = $luser->u_id;
                $timel->date = date('Y-m-d h:i:s', strtotime($promo->select_date));
                $timel->save();
            }
        }*/

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm', 'index');
        $footer = $this->footer();
        $title = 'CRM';

        $raw = DB::table('crm_customers')->where('class', 1);
        $contacted = DB::table('crm_customers')->where('class', 2);
        $interested = DB::table('crm_customers')->where('class', 3);
        $converted = DB::table('crm_customers')->where('class', 4);
        $unqualified = DB::table('crm_customers')->where('class', 5);
        $not_in = DB::table('crm_customers')->where('class', 6);
        $unreachable = DB::table('crm_customers')->where('class', 7);
        $varify = DB::table('crm_customers')->where('verified', 1);

        $start_date1 = date('Y-m-d 00:00:00', strtotime('1th January 2019'));
        $end_date1 = date('Y-m-d 23:59:59');

        $dates1 = array($start_date1, date('Y-m-d', strtotime($end_date1 . ' +1 day')));

        if (isset($_GET['start1']) && isset($_GET['end1'])) {
            $start_date1 = date('Y-m-d 00:00:00', strtotime($_GET['start1']));
            $end_date1 = date('Y-m-d 23:59:59', strtotime($_GET['end1']));
            $dates1 = array($start_date1, date('Y-m-d', strtotime($end_date1 . ' +1 day')));
        }
        $raw->whereBetween('created_at', [$dates1[0], $dates1[1]]);
        $contacted->whereBetween('created_at', [$dates1[0], $dates1[1]]);
        $unreachable->whereBetween('created_at', [$dates1[0], $dates1[1]]);
        $interested->whereBetween('created_at', [$dates1[0], $dates1[1]]);
        $converted->whereBetween('created_at', [$dates1[0], $dates1[1]]);
        $unqualified->whereBetween('created_at', [$dates1[0], $dates1[1]]);
        $not_in->whereBetween('created_at', [$dates1[0], $dates1[1]]);
        $varify->whereBetween('created_at', [$dates1[0], $dates1[1]]);
        $data['raw'] = $raw->get();
        $data['contacted'] = $contacted->get();
        $data['unreachable'] = $unreachable->get();
        $data['interested'] = $interested->get();
        $data['converted'] = $converted->get();
        $data['unqualified'] = $unqualified->get();
        $data['not_in'] = $not_in->get();
        $data['varify'] = $varify->get();

        //raw Graph
        $raw->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(created_at) as created_at")]);
        $Traw = $raw->orderBy('created_at', 'ASC')->groupBy(DB::raw('Date(created_at)'))->get();

        $data['raw_dataset'] = array_pluck($Traw, 'total');
//        $data['raw_label'] = array_pluck($Traw, 'created_at');

        $date_data = $raw->get();
        $data['raw_label'] = array();
        foreach ($date_data as $dd) {
            $data['raw_label'][] = date('d-m-Y', strtotime($dd->created_at));
        }

        //interested Graph
        $interested->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(created_at) as created_at")]);
        $Tinterested = $interested->orderBy('created_at', 'ASC')->groupBy(DB::raw('Date(created_at)'))->get();

        $data['interested_dataset'] = array_pluck($Tinterested, 'total');
//        $data['interested_label'] = array_pluck($Tinterested, 'created_at');

        $date_data1 = $interested->get();
        $data['interested_label'] = array();
        foreach ($date_data1 as $dd) {
            $data['interested_label'][] = date('d-m-Y', strtotime($dd->created_at));
        }

        //contacted Graph
        $contacted->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(created_at) as created_at")]);
        $Tcontacted = $contacted->orderBy('created_at', 'ASC')->groupBy(DB::raw('Date(created_at)'))->get();

        $data['contacted_dataset'] = array_pluck($Tcontacted, 'total');
//        $data['contacted_label'] = array_pluck($Tcontacted, 'created_at');

        $date_data2 = $contacted->get();
        $data['contacted_label'] = array();
        foreach ($date_data2 as $dd) {
            $data['contacted_label'][] = date('d-m-Y', strtotime($dd->created_at));
        }

        //converted Graph
        $converted->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(created_at) as created_at")]);
        $Tconverted = $converted->orderBy('created_at', 'ASC')->groupBy(DB::raw('Date(created_at)'))->get();

        $data['converted_dataset'] = array_pluck($Tconverted, 'total');
//        $data['converted_label'] = array_pluck($Tconverted, 'created_at');

        $date_data3 = $converted->get();
        $data['converted_label'] = array();
        foreach ($date_data3 as $dd) {
            $data['converted_label'][] = date('d-m-Y', strtotime($dd->created_at));
        }

//        $data['not_in_label'] = array_pluck($Tnot_in, 'created_at');
        $date_data5 = $not_in->get();
        $data['not_in_label'] = array();
        foreach ($date_data5 as $dd) {
            $data['not_in_label'][] = date('d-m-Y', strtotime($dd->created_at));
        }
        //unqualified Graph
        $unqualified->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(created_at) as created_at")]);
        $Tunqualified = $unqualified->orderBy('created_at', 'ASC')->groupBy(DB::raw('Date(created_at)'))->get();

        $data['unqualified_dataset'] = array_pluck($Tunqualified, 'total');
//        $data['unqualified_label'] = array_pluck($Tunqualified, 'created_at');

        $date_data4 = $unqualified->get();
        $data['unqualified_label'] = array();
        foreach ($date_data4 as $dd) {
            $data['unqualified_label'][] = date('d-m-Y', strtotime($dd->created_at));
        }

        //unqualified Graph
        $not_in->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(created_at) as created_at")]);
        $Tnot_in = $not_in->orderBy('created_at', 'ASC')->groupBy(DB::raw('Date(created_at)'))->get();

        $data['not_in_dataset'] = array_pluck($Tnot_in, 'total');

        //unreachable Graph
        $unreachable->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(created_at) as created_at")]);
        $Tun_reachable = $unreachable->orderBy('created_at', 'ASC')->groupBy(DB::raw('Date(created_at)'))->get();

        $data['un_reachable'] = array_pluck($Tun_reachable, 'total');

        $date_data8 = $unreachable->get();
        $data['unreachable_label'] = array();
        foreach ($date_data8 as $dd) {
            $data['unreachable_label'][] = date('d-m-Y', strtotime($dd->created_at));
        }

        //Varified Graph
        $varify->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(created_at) as created_at")]);
        $Tvarify = $varify->orderBy('created_at', 'ASC')->groupBy(DB::raw('Date(created_at)'))->get();

        $data['varified'] = array_pluck($Tvarify, 'total');

        $date_data9 = $varify->get();
        $data['varify_label'] = array();
        foreach ($date_data9 as $dd) {
            $data['varify_label'][] = date('d-m-Y', strtotime($dd->created_at));
        }


        $msgs = DB::table('crm_message_save');
        $calls = DB::table('crm_call');
        $visits = DB::table('crm_visits');
        $promotional = DB::table('crm_promotional');

        $start_date = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $end_date = date('Y-m-d 23:59:59');
        $dates = array($start_date, date('Y-m-d', strtotime($end_date . ' +1 day')));
        if (isset($_GET['start']) && isset($_GET['end'])) {
            $start_date = date('Y-m-d 00:00:00', strtotime($_GET['start']));
            $end_date = date('Y-m-d 23:59:59', strtotime($_GET['end']));
            $dates = array($start_date, date('Y-m-d', strtotime($end_date . ' +1 day')));
        }
        //dd($dates);
        $msgs->whereBetween('created_at', [$dates[0], $dates[1]]);
        $calls->whereBetween('select_date', [$dates[0], $dates[1]]);
        $visits->whereBetween('select_date', [$dates[0], $dates[1]]);
        $promotional->whereBetween('select_date', [$dates[0], $dates[1]]);
        $data['messages'] = $msgs->get();
        $data['calls'] = $calls->get();
        $data['visits'] = $visits->get();
        $data['promotional'] = $promotional->get();
        //dd($data['messages']);
        //Message Graph
        $msgs->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(created_at) as created_at")]);
        $fmsg = $msgs->orderBy('created_at', 'ASC')->groupBy(DB::raw('Date(created_at)'))->get();

        $data['msg_dataset'] = array_pluck($fmsg, 'total');
//        $data['msg_label'] = array_pluck($fmsg, 'created_at');

        $date_data6 = $msgs->get();
        $data['msg_label'] = array();
        foreach ($date_data6 as $dd) {
            $data['msg_label'][] = date('d-m-Y', strtotime($dd->created_at));
        }

        //Calls Graph
        $calls->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(select_date) as select_date")]);
        $fcalls = $calls->orderBy('select_date', 'ASC')->groupBy(DB::raw('Date(select_date)'))->get();

        $data['call_dataset'] = array_pluck($fcalls, 'total');
//        $data['call_label'] = array_pluck($fcalls, 'select_date');

        $date_data7 = $calls->get();
        $data['call_label'] = array();
        foreach ($date_data7 as $dd) {
            $data['call_label'][] = date('d-m-Y', strtotime($dd->select_date));
        }

        //Visits Graph
        $visits->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(select_date) as select_date")]);
        $fvisits = $visits->orderBy('select_date', 'ASC')->groupBy(DB::raw('Date(select_date)'))->get();

        $data['visit_dataset'] = array_pluck($fvisits, 'total');
//        $data['visit_label'] = array_pluck($fvisits, 'select_date');

        $date_data8 = $visits->get();
        $data['visit_label'] = array();
        foreach ($date_data8 as $dd) {
            $data['visit_label'][] = date('d-m-Y', strtotime($dd->select_date));
        }

        //Promotional Graph
        $promotional->select([DB::raw('COUNT(*) as total'), DB::raw("DATE(select_date) as select_date")]);
        $fpromotional = $promotional->orderBy('select_date', 'ASC')->groupBy(DB::raw('Date(select_date)'))->get();

        $data['promo_dataset'] = array_pluck($fpromotional, 'total');
//        $data['promo_label'] = array_pluck($fpromotional, 'select_date');
        $date_data9 = $promotional->get();
        $data['promo_label'] = array();
        foreach ($date_data9 as $dd) {
            $data['promo_label'][] = date('d-m-Y', strtotime($dd->select_date));
        }
        //dd($data['visit_dataset']);
        return view('crm/index')->with(compact('header', 'cfg', 'tp', 'footer', 'title', 'data', 'start_date', 'end_date', 'start_date1', 'end_date1'));
    }

    public function Sms()
    {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if (!checkRole($user->u_id, "camp_sms")) {
            return redirect()->to('crm/index')->with('ermsg', 'You don\'t have access to this section.');
        }

        $sid = '';
        $did = '';
        $lid = '';
        $notices = '';
        $category_set = '';
        $type_set = '';
        $class_set = '';
        $district = array();
        $locality = array();
        $cid = '';
        $acc_manager = '';
        $pr_groups = '';
        $type = '';
        $class = '';
        $visited = 'all';
        $states = DB::table('states')->where('country_id', '=', 101)->get();
        $custcategory = DB::table('customer_category')->get();
        $custtype = DB::table('customer_type')->get();
        $users = DB::table('crm_user_manager')->get();
        $cat_groups = DB::table('category_groups')->join('category', 'category.id', '=', 'category_groups.category_id')
            ->select(['category_groups.id as id', 'group_name', 'category.name'])
            ->get();

        $customers = array();
        if (isset($_POST['find'])) {
            $sid = implode(',', $_POST['state']);
            $district = DB::table('district')->whereIn('state_id', $_POST['state'])->get();
            if (isset($_POST['state'])) {
                $sid = implode(',', $_POST['state']);
                $where[] = ' state IN (' . $sid . ') ';
            }
            if (isset($_POST['district'])) {
                $locality = DB::table('locality')->whereIn('district_id', array_pluck($district, 'id'))->get();
                $where = array();
                $did = implode(',', $_POST['district']);
                $where[] = ' district IN (' . $did . ') ';
            }
            if (isset($_POST['locality'])) {
                $locale = implode(',', $_POST['locality']);
                $where[] = ' locality IN (' . $locale . ') ';
            }
            if (isset($_POST['category'])) {
                $cid = implode(',', $_POST['category']);
                $where[] = ' customer_category IN (' . $cid . ') ';
            }
            if (isset($_POST['type'])) {
                $type = implode(',', $_POST['type']);
                $where[] = ' type IN (' . $type . ') ';
            }
            if (isset($_POST['class'])) {
                $class = implode(',', $_POST['class']);
                $where[] = ' class IN (' . $class . ') ';
            }
            if (isset($_POST['acc_manager'])) {
                $acc_manager = implode(',', $_POST['acc_manager']);
                $where[] = ' FIND_IN_SET_X(\'' . $acc_manager . '\', assign_users) ';
            }
            if (isset($_POST['price_group'])) {
                $pr_groups = implode(',', $_POST['price_group']);
                $where[] = ' id IN (select crm_customer_id from crm_products where FIND_IN_SET_X(\'' . $pr_groups . '\', price_groups)) ';
            }
            if (isset($_POST['visited'])) {
                $visited = $_POST['visited'];
                if ($visited == 'yes') {
                    $where[] = ' id IN (select crm_customer_id from crm_visits) ';
                }
            }
            $where[] = ' msg_active = 0 ';
            $category_set = $cid;
            $type_set = $type;
            $class_set = $class;
            $customers = DB::select("SELECT * FROM `crm_customers` WHERE " . implode('AND', $where));

        }
        if (isset($_POST['find_district'])) {
            // dd($_POST);
            $sid = implode(',', $_POST['state']);
            $district = DB::table('district')->whereIn('state_id', $_POST['state'])->get();
            $locality = DB::table('locality')->whereIn('district_id', array_pluck($district, 'id'))->get();
        }

        if (isset($_POST['msg_send'])) {
            $msg = $_POST['msg'];
            $cus = implode(',', $_POST['cust']);
            /*if(isset($_POST['cust'])){
                $cus = implode(',',$_POST['cust']);
                foreach ($cus as $d) {
                    $sms = urlencode($msg);
                    $no = urlencode($d);
                    echo $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_FAILONERROR, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    $output = curl_exec($ch);
                    curl_error($ch);
                    curl_close($ch);
                    if($output) {
                        $notices .= '<div class="card-alert card green">
                            <div class="card-content white-text">
                              <p>Sms sent Successfully.</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>';
                    }else {
                        $notices .= '<div class="card-alert card green">
                            <div class="card-content white-text">
                              <p>Failed</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>';
                    }
                }
            }else{
                $sms = urlencode($msg);
                $no = urlencode($_POST['new_no']);
                echo $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FAILONERROR, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $output = curl_exec($ch);
                curl_error($ch);
                curl_close($ch);
                if($output) {
                    $notices .= '<div class="card-alert card green">
                        <div class="card-content white-text">
                          <p>Sms sent Successfully.</p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>';
                }else {
                    $notices .= '<div class="card-alert card green">
                        <div class="card-content white-text">
                          <p>Failed</p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>';
                }
            }*/
        }
        $customer_class = DB::table('customer_class')->get();
        $customer_type = DB::table('customer_type')->get();
        $customer_category = DB::table('customer_category')->get();
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('CRM', 'sms');
        $footer = $this->footer();
        $title = 'CRM';

        return view('crm/sms')->with(compact('header', 'cfg', 'tp', 'footer', 'notices', 'title', 'states', 'district', 'locality', 'custcategory', 'custtype', 'customers', 'sid', 'did', 'lid', 'category_set', 'type_set', 'class_set', 'customer_class', 'customer_type', 'customer_category', 'users', 'cat_groups', 'acc_manager', 'pr_groups', 'visited'));
    }


    public function Variantcompare()
    {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if (!checkRole($user->u_id, "quot")) {
            return redirect()->to('crm/index')->withErrors(['ermsg' => 'You don\'t have access to this section.']);
        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm', 'index');
        $footer = $this->footer();
        $title = 'Variant Compare';
        $brands = array();
        $variants = array();
        $cid = '';
        $selected_category = array();
        $selected_variant = '';
        $category = DB::table('category')->get();
        $mhs = DB::table('manufacturing_hubs')->get();
        $all_variants = DB::table('product_variants')->join('size', 'size.id', '=', 'product_variants.variant_title')->select('product_variants.id', 'size.name as variant_title')->orderBy('variant_title', 'ASC')->get();
        $report_type = '';
        $view_type = '';
        $set_price = 0;
        $cust_id = '0';
        $company_id = '';
        $mhid = 0;
        if (isset($_POST['hub'])) {
            $mhid = $_POST['hub'];
        }
        $cust_type = '';
        if (isset($_POST['cust_type'])) {
            $cust_type = $_POST['cust_type'];
        }
        if (isset($_POST['customer_search'])) {
            $cust_id = $_POST['customer_search'];
            if (isset($_POST['variants']) && isset($_POST['category'])) {
                if ($_POST['variants'] != '' && $_POST['category'] != '') {
                    //dd($_POST);
                    //dd($_POST['variants']);
                    $vids = $_POST['variants'];
                    $vid = implode(',', $vids);
                    $cids = $_POST['category'];
                    $selected_variant = $vid;
                    $report_type = $_POST['report_type'];
                    $view_type = $_POST['view_type'];
                    if (isset($_POST['price'])) {
                        $set_price = $_POST['price'];
                    }
                    if (isset($_POST['company']))
                        $company_id = $_POST['company'];
                    $get_prod = DB::select("SELECT GROUP_CONCAT(p.category) as cat_id FROM products as p INNER JOIN (SELECT DISTINCT(pv.product_id) FROM `product_variants`  as pv WHERE id IN ($vid)) as t1 ON p.id = t1.product_id")[0];
                    $get_brand_val = $get_prod->cat_id;
                    $brands = DB::table('products')->whereIn('category', $cids)->get();
                    //dd($brands);
                    $p_ids = DB::table('products')->selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $cids)->first();
                    $tot = explode(',', $p_ids->prod_ids);

                    if (count($tot) != 0)
                        $variants = DB::table('product_variants')->whereIn('id', $vids)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('size.name', 'ASC')->get();
                } else {
                    $cid = $_POST['category'];

                    if (!is_array($_POST['category'])) {
                        $cid = explode(',', $_POST['category']);
                    }
                    $selected_category = $cid;
                    $report_type = $_POST['report_type'];
                    $view_type = $_POST['view_type'];
                    if (isset($_POST['price']))
                        $set_price = $_POST['price'];
                    $brands = DB::table('products')->whereIn('category', $cid)->get();
                    $p_ids = DB::table('products')->selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $cid)->first();
                    $tot = explode(',', $p_ids->prod_ids);
                    if (count($tot) != 0)
                        $variants = DB::table('product_variants')->whereIn('product_id', $tot)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('name', 'ASC')->groupBy('variant_title')->get();
                }
            } else {
                $cid = $_POST['category'];
                $selected_category = $cid;
                $report_type = $_POST['report_type'];
                $view_type = $_POST['view_type'];
                if (isset($_POST['price']))
                    $set_price = $_POST['price'];
                $brands = DB::table('products')->whereIn('category', $cid)->get();
                $p_ids = DB::table('products')->selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $cid)->first();
                $tot = explode(',', $p_ids->prod_ids);
                if (count($tot) != 0)
                    $variants = DB::table('product_variants')->whereIn('product_id', $tot)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('name', 'ASC')->groupBy('variant_title')->get();
            }
        }
        if (isset($_POST['category_find'])) {
            if (isset($_POST['customer_search'])) {
                $cust_id = $_POST['customer_search'];
            }
            $report_type = $_POST['report_type'];
            $view_type = $_POST['view_type'];
            if (isset($_POST['variants'])) {
                $vid = implode(',', $_POST['variants']);
                $vidArray = $_POST['variants'];
                $selected_variant = $vid;
                $get_prod = DB::select("SELECT GROUP_CONCAT(p.category) as cat_id FROM products as p INNER JOIN 
(SELECT DISTINCT(pv.product_id) FROM `product_variants`  as pv WHERE id IN ($vid)) as t1 ON p.id = t1.product_id")[0];
                $get_brand_val = $get_prod->cat_id;
                if (is_array($get_brand_val)) {
                    $brands = DB::table('products')->whereIn('category', $get_brand_val)->get();
                    $p_ids = DB::table('products')->selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $get_brand_val)->first();
                } else {
                    $brands = DB::table('products')->where('category', $get_brand_val)->get();
                    $p_ids = DB::table('products')->selectRaw('GROUP_CONCAT(id) as prod_ids')->where('category', $get_brand_val)->first();
                }
                $tot = explode(',', $p_ids->prod_ids);
                if (count($tot))
                    $variants = DB::table('product_variants')->whereIn('product_variants.id', $vidArray)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('size.name', 'ASC')->get();
            } else {
                $cid = $_POST['category'];
                $selected_category = $cid;
                $brands = DB::table('products')->whereIn('category', $cid)->get();
                $p_ids = DB::table('products')->selectRaw('GROUP_CONCAT(id) as prod_ids')->whereIn('category', $cid)->first();
                $tot = explode(',', $p_ids->prod_ids);
                if ($tot != 0)
                    $variants = DB::table('product_variants')->whereIn('product_id', $tot)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('name', 'ASC')->groupBy('variant_title')->get();
            }
        }
        if (isset($_POST['category'])) {
            $selected_category = $_POST['category'];
            if (!is_array($_POST['category'])) {
                $selected_category = explode(',', $_POST['category']);
            }
        }
        $customers = DB::table('crm_customers')->get();
        $company = DB::table('company')->get();
        $customer_class = DB::table('customer_class')->get();
        $customer_type = DB::table('customer_type')->get();
        $customer_category = DB::table('customer_category')->get();
        $states = DB::table('states')->where('country_id', '=', 101)->get();
        return view('crm/variant-compare')->with(compact('header', 'cfg', 'tp', 'footer', 'customer_category', 'company', 'customer_class', 'customer_type', 'view_type', 'states', 'title', 'category', 'all_variants', 'brands', 'variants', 'cid', 'report_type', 'selected_category', 'selected_variant', 'set_price', 'customers', 'cust_id', 'company_id', 'mhs', 'mhid', 'cust_type'));
    }

    public function PDF()
    {
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/assets/crm/");
        $data['header'] = $this->header('Crm', 'index');
        $data['footer'] = $this->footer();
        $data['title'] = 'Variant Compare';
        $data['customer_class'] = DB::table('customer_class')->get();
        $data['customer_type'] = DB::table('customer_type')->get();
        $data['customer_category'] = DB::table('customer_category')->get();
        $data['states'] = DB::table('states')->where('country_id', '=', 101)->get();
        $datai['name'] = $_POST['customer_name'];
        $datai['contact'] = $_POST['mobile'];
        $datai['gst'] = $_POST['gst'];
        $datai['address'] = $_POST['address'];
        $datai['city'] = $_POST['city'];
        $datai['state'] = $_POST['state'];
        $datai['date'] = $_POST['date'];
        $post = DB::table('quotation')->insertGetId($datai);
        if ($post) {
            $i = 0;
            foreach ($_POST['size'] as $p) {
                $size = $_POST['size'][$i];
                $basic_price = $_POST['basic_price'][$i];
                $size_diffrence_price = $_POST['size_diffrence_price'][$i];
                $tax_amount = $_POST['tax_amount'][$i];
                $total = $_POST['total'][$i];
                $remark = $_POST['remark'][$i];
                DB::insert("INSERT INTO quotation_particular (q_id,size,basic_price,size_diffrence_price,tax_amount,total,remark) 
                VALUE ('" . $post . "','" . $p . "','" . $basic_price . "','" . $size_diffrence_price . "','" . $tax_amount . "','" . $total . "','" . $remark . "')");
                $i++;
            }
        }
        $html = view('crm/pdf')->with($data)->render();
        return $this->pdf
            ->load($html)
            ->download('quotation.pdf');
    }

    public function PdfPreview()
    {
        $data['cfg'] = $this->cfg;
        $data['tp'] = url("/assets/crm/");
        $data['header'] = $this->header('Crm', 'index');
        $data['footer'] = $this->footer();
        $data['title'] = 'Quotation';
        $data['customers'] = DB::table('crm_customers')->get();
        $data['set_price'] = isset($_POST['set_price']) ? $_POST['set_price'] : 0;
        $data['cust_id'] = isset($_POST['cust_id']) ? $_POST['cust_id'] : 0;
        $data['cust_type'] = isset($_POST['cust_type']) ? $_POST['cust_type'] : 0;
        $data['mhid'] = isset($_POST['hub']) ? $_POST['hub'] : 0;
        $data['quot'] = '';
        $data['brn_id'] = '';
        $data['var_id'] = array();
        $br_id = array();
        $data['hide_status'] = array();
        if (isset($_POST['quotation'])) {
            $bid_array = array();
            $vid_array = array();
            $data['company_id'] = $_POST['company'];
            if ($data['company_id'] !== '') {
                $data['company'] = DB::table('company')->where('id', '=', $data['company_id'])->get();
            }
            $check_b = 0;
            $check_v = 0;
            foreach ($_POST['brand_id'] as $b) {
                $bid1 = $b;
                $bid2 = $_POST['bid'][$check_b];
                if ($bid1 == $bid2) {
                    array_push($bid_array, $bid1);
                }
                $check_b++;
            }
            foreach ($_POST['vid'] as $v) {
                $vid = $v;
                $vid1 = $_POST['vid1'][$check_v];
                $vid2 = $_POST['vid2'][$check_v];
                if ($vid1 == 1 && $vid2 == 1) {
                    array_push($vid_array, $vid);
                }
                $check_v++;
            }
            $bid = implode(',', $bid_array);
            $data['brands'] = DB::table('products')->whereIn('id', $bid_array)->get();
            $data['quot'] = 'brand_wise';
            $data['var_id'] = $_POST['var_id'];
            $data['brn_id'] = $_POST['brn_id'];
            $data['hide_status'] = $_POST['hide_status'];
        } else {
            $data['company_id'] = $_POST['company'];
            if ($data['company_id'] !== '') {
                $data['company'] = DB::table('company')->where('id', '=', $data['company_id'])->get();
            }
            $vid_array = array();
            $check_v = 0;
            $vv = isset($_POST['vid']) ? $_POST['vid'] : array();
            foreach ($vv as $v) {
                $vid = $v;
                $vid1 = $_POST['vid1'][$check_v];
                $vid2 = $_POST['vid2'][$check_v];
                if (($vid1 != $vid2) || ($vid1 == 1 && $vid2 == 1)) {
                    array_push($vid_array, $vid);
                }
                $check_v++;
            }
            $data['quot'] = 'price_wise';
        }
        $vid = implode(',', $vid_array);
        if (!$vid) {
            return back();
        }
        $data['category'] = '';
        $data['variants'] = DB::table('product_variants')->whereIn('product_variants.id', $vid_array)->join('size', 'size.id', '=', 'product_variants.variant_title')->select(['product_variants.*', 'size.name'])->orderBy('size.name', 'ASC')->get();
        return view('crm/pdf-preview')->with($data)->render();
    }

    public function Getreportoption(Request $request)
    {
        $cname = $request->cname;
        if ($cname == 'Angle' || $cname == 'TMT Bars' || $cname == 'Cement') {
            echo '<label>Select Report Type</label>
                <select class="browser-default" name="report_type" id="get_report_view">
                    <option value="">Select Report Type</option>
                    <option value="brand_wise">Brand Wise</option>
                    <option value="price_wise">Price Wise</option>
                </select> ';
        }
    }

    public function addDefaultPipeline(Request $request)
    {
        $tp = url("/assets/crm/");
        echo view('crm/defaultpipeline')->with('tp')->render();
    }

    public function insertPipeline(Request $request)
    {
        $data['name'] = $request->name;

        $id = DB::table('sales_pipelines')->insertGetId($data);
        $stages = json_decode($request->stages);

        foreach ($stages as $stage) {

            DB::insert("INSERT INTO `pipeline_stages`(`pipeline_id`,`stage_name`) VALUES ('$id','$stage')");
        }

    }

    public function updatePipeline(Request $request)
    {

        DB::update("UPDATE `sales_pipelines` SET `name` = '$request->name' WHERE id = '$request->id'");
        $stages = json_decode($request->stages);
        foreach ($stages as $stage) {
            if (empty($stage->id) && $stage->stages) {
                DB::insert("INSERT INTO `pipeline_stages`(`pipeline_id`,`stage_name`) VALUES ('$request->id','$stage->stages')");
            }
            DB::update("UPDATE `pipeline_stages` SET `stage_name` = '$stage->stages' WHERE id = '$stage->id'");

        }

    }

    public function updateDeletePipeline(Request $request)
    {

        $checkleadorder = DB::table('lead_orders')->where('pipelines_id', $request->pipelineid)->where('pipelines_stage', $request->state)->get();

        if (count($checkleadorder) > 0) {
            return "Pipeline Stages Uses Somewhere So You Can't Delete It. ";
        } else {
            DB::table('pipeline_stages')->where('id', $request->id)->delete();
            return "Deleted Successfully!!! ";
        }

    }


    public function Getdistrict(Request $request)
    {
        $sid = $request->sid;
        $ab = '';
        $district = DB::table('district')->where('state_id', '=', $sid)->get();
        $a = '';
        foreach ($district as $dist) {
            $ab.='<option value="'.$dist->id.'">'.$dist->name.'</option>';
        }
        $a = $a.$ab;
        return $a;
    }

    public function Customerdelete(Request  $request)
    {
        $cid = $request->cid;
        DB::table('crm_customers')->where('id','=',$cid)->delete();
    }
    public function Localitydelete(Request  $request)
    {
        $cid = $request->lid;
        DB::table('locality')->where('id','=',$cid)->delete();
        return 'success';
    }


    public function Companydelete(Request  $request)
    {
        $cid = $request->cid;
        DB::table('company')->where('id','=',$cid)->delete();
        return 'success';
    }
    public function verifyCustomer(){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"verify_cust")){
            return back();
        }
        $get_cust = CrmCustomer::where('id', $_GET['edit'])->first();
        $action = 1;
        if(isset($_GET['action'])){
            $action = $_GET['action'];
        }
        date_default_timezone_set("Asia/Kolkata");
        $date = date('Y-m-d H:i:s');
        $get_cust->verified = $action;
        $get_cust->verified_by = $user->u_id;
        $get_cust->verified_at = $date;
        $get_cust->save();
        return back();

    }
    public function verifyTransportor(){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"tran_mas")){
            return back();
        }
        $get_trans = Transportor::where('id', $_GET['edit'])->first();
        $action = 1;
        if(isset($_GET['action'])){
            $action = $_GET['action'];
        }
        date_default_timezone_set("Asia/Kolkata");
        $date = date('Y-m-d H:i:s');
        $get_trans->verified = $action;
        $get_trans->verified_by = $user->u_id;
        $get_trans->verified_at = $date;
        $get_trans->save();
        return back();

    }
    public function Customers(Request  $request)
    {
        $names = '';
        $emails = '';
        $contacts = '';
        $descs = '';
        $landlines = '';
        $user = DB::table('user')->where('secure', session('crm'))->first();
        $luser = $user;
        if(!checkRole($user->u_id,"mas_cst")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $pcid = '';
        $price = '';
        $selected_locality = '';
        $selected_block = '';
        $selected_district=array();
        if(isset($_POST['add'])){

            if(isset($_POST['pcid'])){
                $pcid = implode(',',$_POST['pcid']);
                $price = implode(',',$_POST['price']);
            }
            $customer_category = $_POST['customer_category'];
            $cid = $_POST['cid'];
            $name = $_POST['name'];
            $proprieter_name = $_POST['proprieter_name'];
            $type = $_POST['type'];
            $desg = $_POST['desg'];
            $landline = $_POST['landline'];
            $postal_address = $_POST['postal_address'];
            $email = $_POST['email'];
            $contact_no = $_POST['contact_no'];
            $district = $_POST['district'];
            $block = $_POST['block'];
            $locality = $_POST['locality'];
            $state = $_POST['state'];
            $msg_active = $_POST['msg_active'];
            $class = $_POST['msg_class'];
            $reason = $_POST['reason_value'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];
            $payment_days = $_POST['payment_days'];
            $user = '';
            if(isset($_POST['user'])) {
                $user = implode(',', $_POST['user']);
            }
            $assoc = '';
            if(isset($_POST['association'])) {
                $assoc = implode(',', $_POST['association']);
            }
            if(isset($_POST['names'])) {
                $names= serialize($_POST['names']);
            }if(isset($_POST['emails'])) {
                $emails= serialize($_POST['emails']);
            }if(isset($_POST['contacts'])) {
                $contacts= serialize($_POST['contacts']);
            }if(isset($_POST['descs'])) {
                $descs= serialize($_POST['descs']);
            }if(isset($_POST['landlines'])) {
                $landlines= serialize($_POST['landlines']);
            }

            DB::insert("INSERT INTO `crm_customers`(`customer_category`,`product_category`, `price`, `cid`,`converted_by`,`name`, `proprieter_name`,  `type`,`class`, `postal_address`, `email`, `contact_no`, `blocks`,`locality`, `state`, `msg_active`, `lat`, `lng`, `unqualified_reason`,`payment_days`,`crm_association`,`designation`,`landline`,`district`,`sec_names`,`sec_emails`,`sec_contact`,`sec_desc`,`sec_landline`) VALUES ('$customer_category','$pcid','$price','$cid','$user','$name','$proprieter_name','$type','$class','$postal_address','$email','$contact_no','$block','$locality','$state','$msg_active', '$lat', '$lng','$reason','$payment_days','$assoc','$desg','$landline','$district','$names','$emails','$contacts','$descs','$landlines')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Customer Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            /*if($_POST['uv']){
                $id = DB::getPdo()->lastInsertId();
                return redirect()->to('crm/customer-profile?cid='.$id);
            }*/
            $id = DB::getPdo()->lastInsertId();
            if($_POST['quick']){
                $qdata = DB::table('crm_master_quick_entry')->where('id', $_POST['quick'])->first();
                $images = explode('<**>', $qdata->image);
                $oldPath = base_path().'/assets/crm/images/user/';
                $newPath = base_path().'/assets/crm/images/visits/';
                foreach($images as $image){
                    try {
                        rename($oldPath . $image, $newPath . $image);
                    }catch(\Exception $ex){

                    }
                }

                DB::insert("INSERT INTO crm_visits(`crm_customer_id`, `name`, `phone_no`, `address`, `select_date`, `description`, `user_id`, `image`, `customer_status`) VALUE ('".$id."', '".$name."', '".$contact_no."', '".$postal_address."', '".date_format(date_create($qdata->created_at), 'Y-m-d')."', '".$qdata->discription."', '".$user."', '".implode(',', $images)."', '".$class."')");
                DB::table('crm_master_quick_entry')->where('id', $_POST['quick'])->delete();
            }
            if($_POST['goto']){

                return redirect()->to('crm/customer-profile?cid='.$id);
            }
        }
        if(isset($_POST['edit'])){

            if(isset($_POST['pcid'])){
                $pcid = implode(',',$_POST['pcid']);
                $price = json_encode($_POST['price']);
            }
            $strict = 0;
            if(isset($_POST['strict'])){
                $strict = 1;
            }

            $cidd = $_POST['cidd'];
            $customer_category = $_POST['customer_category'];
            $cid = $_POST['cid'];
            $name = $_POST['name'];
            $proprieter_name = $_POST['proprieter_name'];
            $type = $_POST['type'];
            $postal_address = $_POST['postal_address'];
            $email = $_POST['email'];
            $contact_no = $_POST['contact_no'];
            $desg = $_POST['desg'];
            $landline = $_POST['landline'];
            $district = $_POST['district'];
            $block = $_POST['block'];
            $locality = $_POST['locality'];
            $state = $_POST['state'];
            $msg_active = $_POST['msg_active'];
            $class = $_POST['msg_class'];
            $reason = $_POST['reason_value'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];
            $payment_days =$_POST['payment_days'];
//            dd($lat);
            $customer = DB::table('crm_customers')->find($cidd);
            if($class >= 5){
                if($customer->class != $class)
                    $msg_active = 1;
            }

            $user = '';
            if(isset($_POST['user'])) {
                $user = implode(',', $_POST['user']);

            }

            $assoc = '';
            if(isset($_POST['association'])) {
                $assoc = implode(',', $_POST['association']);
            }
            if(isset($_POST['names'])) {
                $names= serialize($_POST['names']);
            }if(isset($_POST['emails'])) {
                $emails= serialize($_POST['emails']);
            }if(isset($_POST['contacts'])) {
                $contacts= serialize($_POST['contacts']);
            }if(isset($_POST['descs'])) {
                $descs= serialize($_POST['descs']);
            }if(isset($_POST['landlines'])) {
                $landlines= serialize($_POST['landlines']);
            }
            DB::update("UPDATE `crm_customers` SET `customer_category`='$customer_category',`product_category` = '$pcid',`price` = '$price',`strict_mode`='$strict', `cid`='$cid',`converted_by` = '$user',`name`='$name',`proprieter_name`='$proprieter_name',`type`='$type',`class`='$class',`postal_address`='$postal_address',`email`='$email',`contact_no`='$contact_no',`designation`='$desg',`landline`='$landline',`district`='$district',`blocks`='$block', `locality`='$locality', `sec_names`='$names', `sec_emails`='$emails', `sec_contact`='$contacts', `sec_desc`='$descs', `sec_landline`='$landlines',`state`='$state',`msg_active`='$msg_active', `lat`='$lat', `lng`='$lng',`unqualified_reason`='$reason',`payment_days`='$payment_days',`crm_association`='$assoc' WHERE id = '$cidd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Customer Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        $localities = array();
        $blocks = array();
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];
            $selected_district = DB::table('crm_customers')
                ->leftJoin('district', 'state_id', '=', 'crm_customers.state')
                ->where('crm_customers.id', '=', $cid)
                ->select('district.name as d_name','district.id as did')
                ->get();
            $selected_block = DB::table('crm_customers')
                ->leftJoin('blocks', 'district_id', '=', 'crm_customers.district')
                ->where('crm_customers.id', '=', $cid)
                ->select('blocks.block as b_name','blocks.id as bid')
                ->get();

            $selected_locality =DB::table('crm_customers')
                ->leftJoin('locality', 'block_id', '=', 'crm_customers.blocks')
                ->where('crm_customers.id', '=', $cid)
                ->select('locality.locality as l_name','locality.id as lid')
                ->get();
            $localities = DB::table('locality')->whereIn('block_id', array_pluck($selected_block, 'bid'))->get();
            $blocks = DB::table('blocks')->whereIn('district_id', array_pluck($selected_district, 'did'))->get();
        }


        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM';

        $states = DB::table('states')->where('country_id','=',101)->get();
        $std_date = date('Y-m-d', strtotime('first day of january this year'));
        $end_date = date('Y-m-d');
        $districts = DB::table('district')->get();
        $customer_class = DB::table('customer_class')->get();
        $customer_type = DB::table('customer_type')->get();
        $association = DB::table('crm_association')->get();
        $customer_category = DB::table('customer_category')->get();
        $users = DB::table('user')->where('user_type','=','crm_user')->get();
        $designation = DB::table('customer_designation')->groupBY('name')->get();
        $crm_contax = DB::table('crm_contax')->groupBY('name')->get();
        return view('crm/customers')->with(compact('header','cfg','tp','footer', 'luser', 'association','title','notices','states','customer_class','customer_type','customer_category','selected_district','user','users', 'designation','selected_locality', 'localities', 'districts','selected_block','blocks', 'buttons','std_date','end_date','crm_contax'));

    }

    public function getCustomers(Request $request){

        $records = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select(['crm_customers.*', 'customer_category.type as category']);

        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $user = (isset($_GET["user"])) ? $_GET["user"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $group = (isset($_GET["group"])) ? ($_GET["group"]) : false;
        $locality = (isset($_GET["locality"])) ? ($_GET["locality"]) : false;
        $block = (isset($_GET["block"])) ? ($_GET["block"]) : false;
        $ptype = (isset($_GET["ptype"])) ? ($_GET["ptype"]) : false;
        $pstype = (isset($_GET["pstype"])) ? ($_GET["pstype"]) : false;
        $start = (isset($_GET["startd"])) ? ($_GET["startd"]) : false;

        $end = (isset($_GET["endd"])) ? ($_GET["endd"]) : false;
        /*if($start == ""){
            $start = date('yy-m-d');

        }
        if($end == ""){
            $end = date('yy-m-d', strtotime('+2 days'));
        }*/
        if($category){
            $records = $records->whereIn('customer_category.id', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($locality){
            $records = $records->whereIn('locality', $locality);
        }
        if($block){
            $records = $records->whereIn('blocks',$block);
        }
        if($user){
            $records = $records->whereRaw('FIND_IN_SET(?, crm_customers.converted_by)', $user);
        }
        if($start){
            $records = $records->whereBetween('crm_customers.created_at', [$start, $end]);
        }
        if($ptype){
            $c = array_pluck(DB::table('crm_customer_projects')->where('project_type',$ptype)->get()->toArray(), 'crm_customer_id');
            $records = $records
                ->whereIn('crm_customers.id',$c);

        }
        if($pstype){
            $cd = array_pluck(DB::table('crm_customer_projects')->where('project_type',$_GET["ptype"])->whereIn('project_subtype',$pstype)->get()->toArray(), 'crm_customer_id');
            $records = $records
                ->whereIn('crm_customers.id',$cd);
        }

        $records = $records->orderBy('crm_customers.id', 'asc');

        return Datatables::of($records)
            ->addColumn('action', function($row){
                $html ='<button value="'.$row->id.'" class="btn myred waves-light cust_delete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="customers?edit='.$row->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })->editColumn('name', function($row){
                $html = '';
                if($row->verified){
                    if(!empty($row->sec_names)) {
                        $html = $row->name .','.implode(',',unserialize($row->sec_names)).' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                    }
                    $html = $row->name .' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    if(!empty($row->sec_names)) {
                        $html = $row->name.','.implode(',',unserialize($row->sec_names));
                    }else{
                        $html = $row->name;
                    }
                }
                return $html;
            })
            ->editColumn('sec_names', function($row){
                $html = '';
                if(!empty($row->sec_names)) {
                        $html = implode(',',unserialize($row->sec_names));

                }
                return $html;
            })->editColumn('contact_no', function($row){
                $html = '';
                if(!empty($row->sec_contact)){
                    $nos = implode(',',unserialize($row->sec_contact));
                    $html = $row->contact_no.','.'</br>'.$nos;
                }else{
                    $html = $row->contact_no;
                }

                return $html;
            })->editColumn('sec_contact', function($row){
                $html = '';
                if(!empty($row->sec_contact)) {
                    $html = implode(',',unserialize($row->sec_contact));
                }
                return $html;
            })
            ->editColumn('created_at', function($row){
                return date("d, M Y h:ia", strtotime($row->created_at));
            })
            ->editColumn('converted_by', function($row){
                if($row->assign_users!==''){
                    $users = explode(',', $row->converted_by);
                    $u = array_pluck(DB::table('user')->whereIn('u_id', $users)->get(), 'u_name');
                    return implode(',', $u);
                }
                return 'NA';
            })
            ->rawColumns(['name','contact_no','action'])
            ->make(true);
    }

    public function getReferedCustomer(Request $request)
    {
        $searchdata = array();
        $customeres = CrmCustomer::where('name', 'like', '%' . $request->key . '%')->select('name')->get();
        foreach ($customeres as $cust){
            $searchdata[] = $cust->name;
        }

        return $searchdata;
    }
    public function getCompany(Request $request){

        $records = DB::table('company')->get();
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light cust_delete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="company?edit='.$records->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('logo', function($records){
                return '<a target="_blank" href="../assets/crm/images/company_logo/'.$records->logo.'"><img src="../assets/crm/images/company_logo/'.$records->logo.'" width="30px" height="30px"/></a>';
            })
            ->editColumn('latter_head', function($records){
                return '<a target="_blank" href="../assets/crm/images/letter_head/'.$records->latter_head.'"><img src="../assets/crm/images/letter_head/'.$records->latter_head.'" width="30px" height="30px"/></a>';
            })
            ->rawColumns(['latter_head','logo','action'])
            ->make(true);
    }

    public function getCustomersData1(Request $request){


//        $project_ids = array_pluck(DB::table('crm_customer_projects')->whereIn($ptype, $temp)->get());

        $records = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.contact_no','customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = crm_customers.id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = crm_customers.id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = crm_customers.id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'));


        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $group = (isset($_GET["group"])) ? $_GET["group"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $class = (isset($_GET["cust_class"])) ? ($_GET["cust_class"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;
//        $ptype = (isset($_GET["ptype"])) ? ($_GET["ptype"]) : false;
//        $pstype = (isset($_GET["pstype"])) ? ($_GET["pstype"]) : false;


        if($category){
            $records = $records->whereIn('customer_category', $category);
        }
        if($group){
            $grrecord = explode(',',$records);
            if(substr( $grrecord->converted_by, 0, 5 ) == 'group'){
                $groupid = substr( $grrecord->converted_by, 5, 100);
                $records = $records->select(DB::raw('(select name form customer_designation whereIn(customer_designation.id = '.$groupid.') as group)'));
            }else{
                $records = $records->select(DB::raw('(select u_name form user whereIn(user.u_id = '.$grrecord->converted_by.') as group)'));
            }

        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($class){
            if($class !== 'all'){
                $records = $records->where('class', $class);
            }
        }
        if($type){
            $records = $records->where('crm_customers.type',$type);
        }
//        if($ptype && $pstype){
//
//
//            $records = $records->where('crm_customer_projects.project_type',$ptype)->where('crm_customer_projects.project_subtype',$pstype);
//        }


        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="customer-data";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('name', function($records){
                $html = '';
                if($records->verified){
                    $html = $records->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    $html = $records->name;
                }
                return $html;
            })

            ->rawColumns(['name', 'action'])
            ->make(true);
    }
    public function getCustomersData(Request $request){


        $records = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('crm_customers.id', 'crm_customers.name', 'crm_customers.sec_names','crm_customers.proprieter_name','crm_customers.converted_by','crm_customers.managed_by','crm_customers.product_category','crm_customers.contact_no','crm_customers.sec_contact', 'customer_category.type', 'crm_customers.blocks', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = crm_customers.id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = crm_customers.id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = crm_customers.id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'), 'crm_customers.verified' );
        $count_total = $records->count();
        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $group = (isset($_GET["group"])) ? ($_GET["group"]) : false;
        $class = (isset($_GET["cust_class"])) ? ($_GET["cust_class"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;
        $ptype = (isset($_GET["ptype"])) ? ($_GET["ptype"]) : false;
        $pstype = (isset($_GET["pstype"])) ? ($_GET["pstype"]) : false;
        $supplier = (isset($_GET["supplier"])) ? ($_GET["supplier"]) : false;
        $brand = (isset($_GET["brand"])) ? ($_GET["brand"]) : false;
        $verified = (isset($_GET["verified"])) ? ($_GET["verified"]) : false;
        $pro_category = (isset($_GET["pro_category"])) ? ($_GET["pro_category"]) : false;

        $start = $_GET['start'];
        $length = $_GET['length'];
        if($category){
            $records = $records->whereIn('customer_category', $category);
        }

        if($group){
            $records = $records->whereIn('customer_designation', $group);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($class){
            $records = $records->whereIn('class', $class);

        }
        if($type){
            $records = $records->where('crm_customers.type',$type);
        }
        if($ptype){
            $c = array_pluck(DB::table('crm_customer_projects')->where('project_type',$ptype)->get()->toArray(), 'crm_customer_id');
            $records = $records
                ->whereIn('crm_customers.id',$c);

        }
        if($pstype){
            $cd = array_pluck(DB::table('crm_customer_projects')->where('project_type',$_GET["ptype"])->whereIn('project_subtype',$pstype)->get()->toArray(), 'crm_customer_id');
            $records = $records
                ->whereIn('crm_customers.id',$cd);
        }
        if($supplier){
            $c = array_pluck(DB::table('crm_supplier')->whereIn('id',$supplier)->get()->toArray(), 'id');
            $sup = array_pluck(DB::table('crm_products')->whereRaw('FIND_IN_SET(?, supplier_id)', $c)->get()->toArray(), 'crm_customer_id');
            $records = $records
                ->whereIn('crm_customers.id',$sup);

        }
        if($brand){
            $cz = array_pluck(DB::table('brand')->whereIn('id',$brand)->get()->toArray(), 'id');
            $br = array_pluck(DB::table('crm_products')->whereRaw('FIND_IN_SET(?, brands)', $cz)->get()->toArray(), 'crm_customer_id');
            $records = $records->whereIn('crm_customers.id',$br);

        }

        if($pro_category){

            $pro_category = implode(',',$pro_category);
            $prop = array_pluck(DB::table('crm_products')->whereRaw('FIND_IN_SET_X(?, product_id)', $pro_category)->get(), 'crm_customer_id');
            $records = $records->whereIn('crm_customers.id',$prop);

        }
        if($verified){

            if($verified == "1"){
                $records = $records->where('crm_customers.verified',$verified);
            }else{
                $records = $records->where('crm_customers.verified','0');
            }

        }
        $count_filter = $records->count();

        //dd($start.'-'.$length);
        /*if($start > 0){
            $records = $records->skip($start)->take($length);
        }else{
            $records = $records->take($length);
        }*/

        return Datatables::of($records)
            ->addColumn('action', function($row) use($start, $length){
                $a="customer-data";
                $html = '<a href="customer-profile?cid='.$row->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('name', function($row) {
                $html = '';
                if ($row->verified) {
                    if(!empty($row->sec_names)) {
                        $html = $row->name .','.implode(',',unserialize($row->sec_names)).' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                    }
                    $html = $row->name .' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    if(!empty($row->sec_names)) {
                        $html = $row->name.','.implode(',',unserialize($row->sec_names));
                    }else{
                        $html = $row->name;
                    }
                }
                return $html;
            })
            ->editColumn('sec_names', function($row){
                $html = '';
                if(!empty($row->sec_names)) {
                    $html = implode(',',unserialize($row->sec_names));

                }
                return $html;
            })->editColumn('contact_no', function($row){
                $html = '';
                if(!empty($row->sec_contact)){
                    $nos = implode(',',unserialize($row->sec_contact));
                    $html = $row->contact_no.','.'<br>'.$nos;
                }else{
                    $html = $row->contact_no;
                }

                return $html;
            })->editColumn('sec_contact', function($row){
                $html = '';
                if(!empty($row->sec_contact)) {
                    $html = implode(',',unserialize($row->sec_contact));
                }
                return $html;
            })->editColumn('proprieter_name', function($row){
                if($row->proprieter_name){
                    return $row->proprieter_name;
                }
                return 'NA';
            })
            ->addColumn('group', function($row){

                $dex = array();
                $users = explode(',', $row->managed_by);
                $u = array_pluck(DB::table('user')->whereIn('u_id', $users)->get(), 'u_name');

                $d = DB::table('customer_designation')->get();
                foreach ($d as $des) {
                    if (in_array('des' . $des->id, $users)) {
                        $dex[] = $des->name;
                    }
                }

                $data =  array_merge($dex,$u);
                return implode(',',$data);
            })->addColumn('blocks', function($row){
                $blocks = array_pluck(DB::table('blocks')->where('id',$row->blocks)->get(),'block');
                return $blocks;
            })->with([
                "recordsTotal" => $count_total,
                "recordsFiltered" => $count_filter
            ])
            ->rawColumns(['name','contact_no','action'])
            ->make(true);
    }

    public function ProductPriceList(){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"pric_ent")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $notices = '';
        $cat_id = '';
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Product Price List';
        $brands = array();
        $variants= array();
        $pro1= array();
        $cid='';
        $selected_category = '';
        $category = DB::table('category')->get();
        $report_type = '';
        if(isset($_POST['category_find'])){
            $cid = $_POST['category'];
            $cid1[] = $_POST['category'];
            $c = array();
            $cont = true;
            $temp = $cid1;
            while($cont){
                $c = array_pluck(DB::table('category')->whereIn('parent', $temp)->get(), 'id');
                if(count($c)){
                    $cid1 = array_merge($cid1, $c);
                    $temp = $c;
                }else{
                    $cont = false;
                }
            }
            $pro1 = DB::table('products')->whereIn('category', $cid1)->get();

            $select_category = DB::table('category')->where('id','=',$cid)->first();
            $selected_category = $select_category->name;
            $cat_id = $select_category->id;
            $brands = DB::table('products')->where('category','=',$cid)->get();
            $variants = DB::table('products')->where('category','=',$cid)->get();

        }
        if(isset($_POST['price_save'])){
            $i=0;
            foreach ($_POST['pid'] as $pid){

                $product_id = $_POST['pid'][$i];
                $new_price = $_POST['new_price'][$i];
                DB::update("UPDATE products SET purchase_price = '$new_price' WHERE id = '".$pid."'");
                $i++;
            }
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Price Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }

        return view('crm/product-price-list')->with(compact('header','cfg','tp','footer', 'title','category','brands','cat_id','variants','cid','selected_category','pro1','notices'));
    }

    public function AddProductPrice(){
        $html = '';
        $product_id = $_POST['product_id'];
        $new_price = $_POST['new_price'];

        DB::update("UPDATE products SET purchase_price = '$new_price' WHERE id = '".$product_id."'");

        return back();
    }

    public function getCustData()
    {
        $cid = $_POST['cid'];
//        $com = $_POST['did'];
        $html = '';
//        $company = DB::table('company')->where('id', '=', $com)->first();
        $cus = DB::table('crm_customers')->where('id', '=', $cid)->first();
        $dist = DB::table('district')->where('id', '=', $cus->district)->first();
        $state = DB::table('states')->where('id', '=', $cus->state)->first();

        $html ='<p>To,</p>
		<p>Kind Attention : Sir <input type="hidden" name="kindly" class="form-control" value="'.$cus->name.'">'.$cus->name.'</p>
        <br />
		<p>Company Name : <input type="hidden" name="customer_name" class="form-control" value="'.$cus->proprieter_name.'">'.$cus->proprieter_name.'</p>
        <p>Address : <input type="hidden" name="address" class="form-control" value="'.$cus->postal_address.'">'.$cus->postal_address.' </p>
        
        <p>Contact No. : <input type="hidden" class="form-control" name="mobile" value="'.$cus->contact_no.'">'.$cus->contact_no.'</p>
        <br />
        
        <input type="hidden" value="" class="form-control" name="gst">         
        <input type="hidden" name="city" class="form-control" value="'.$dist->name.'"> 
        <input type="hidden" name="state" class="form-control" value="'.$state->name.'">
        
        <p>Ref : Your inquiry through Mail / Message / Whatsapp / Telephone. </p>
        <br />';

        $ary = array(
            'cno' => $cus->contact_no,
            'cmail' => $cus->email,
            'html' => $html
        );
        return json_encode($ary);
        //return $html;
    }

    public function Districtmaster(Request  $request)
    {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_dis")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $selected_district=array();
        if(isset($_POST['add'])){
            // dd($_POST);
//            $population = $_POST['population'];
//            $area = $_POST['area'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];
//            $density = $_POST['density'];
            DB::insert("INSERT INTO `district`(`state_id`, `name`,`lat`, `lng`) VALUES ('$state','$district','$lat','$lng')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New District Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){

            $didd = $_POST['didd'];
            $district = $_POST['district'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];
//            $population = $_POST['population'];
//            $area = $_POST['area'];
//            $Density = $_POST['density'];

            DB::update("UPDATE `district` SET  `name`='$district',`lat`='$lat',`lng`='$lng'  WHERE id = '$didd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>District Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];
            $selected_district = DB::table('crm_customers')
                ->leftJoin('district', 'state_id', '=', 'crm_customers.state')
                ->where('crm_customers.id', '=', $cid)
                ->select('district.name as d_name','district.id as did')
                ->get();
        }
        $states = DB::table('states')->where('country_id','=',101)->get();
        $district = DB::table('district')
            ->join('states', 'district.state_id', '=', 'states.id')
            ->where('states.country_id', '=', 101)
            ->select('district.id as district_id','district.name as district_name','states.name as state_name','states.id as state_id')
            ->get();
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM';
        $customers = DB::table('crm_customers')
            ->join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
            ->select('crm_customers.*','customer_category.type')
            ->get();
        $customer_class = DB::table('customer_class')->get();
        $customer_type = DB::table('customer_type')->get();
        $customer_category = DB::table('customer_category')->get();

        return view('crm/district-master')->with(compact('header','cfg','tp','footer', 'title','notices','customers','states','customer_class','customer_type','customer_category','selected_district','district', 'buttons'));

    }
    public function NoOrderReason(Request  $request)
    {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"no_order")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $selected_district=array();
        if(isset($_POST['add'])){
            // dd($_POST);
            $reason = $_POST['reason'];

            DB::insert("INSERT INTO `no_order`(`reason`) VALUES ('$reason')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>No Order Reason Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){
            $cid = $_POST['cid'];
            $reason = $_POST['reason'];

            DB::update("UPDATE `no_order` SET  `reason`='$reason'  WHERE id = '$cid'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>No Order Reason Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM';
        $customers = DB::table('crm_customers')
            ->join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
            ->select('crm_customers.*','customer_category.type')
            ->get();
        $no_orders  = DB::table('no_order')->get();


        return view('crm/no-order-reason-master')->with(compact('header','cfg','tp','footer', 'title','notices','customers','no_orders', 'buttons'));

    }
    public function blockmaster(Request  $request)
    {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_block")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $selected_district=array();
        $districts = array();
        if(isset($_POST['add'])){
            // dd($_POST);
            $block = $_POST['block'];
            $district = $_POST['district'];
            $population = $_POST['population'];
            $area = $_POST['area'];
            $density = $_POST['density'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];
            DB::insert("INSERT INTO `blocks`(`block`, `district_id`, `population`, `area`, `density`,`lat`, `lng`) VALUES ('$block','$district', '$population', '$area', '$density','$lat', '$lng')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Block Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){

            $didd = $_POST['didd'];
            $block = $_POST['block'];
            $population = $_POST['population'];
            $area = $_POST['area'];
            $density = $_POST['density'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];

            DB::update("UPDATE `blocks` SET `block`='$block', `population`='$population', `area`='$area', `density`='$density',`lat`='$lat',`lng`='$lng' WHERE id = '$didd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Block Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        $block = null;
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];
            $block = DB::table('blocks')->where("id", $cid)->first();
            $sd = DB::table('district')->where('id', $block->district_id)->first();
            $districts = DB::table('district')->where('state_id', $sd->state_id)->get();
        }

        $states = DB::table('states')->where('country_id','=',101)->get();

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM - Block Master';

        return view('crm/block-master')->with(compact('header','cfg','tp','footer', 'title','notices','states', 'block', 'districts', 'buttons'));

    }

    public function brandPenetrationReport($slug = ''){

        if($slug == 'state-wise'){
            $user = DB::table('user')->where('secure', session('crm'))->first();
            if(!checkRole($user->u_id,"bpen_sw")){
                return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
            }
            $buttons = "[]";
            if(checkRole($user->u_id,"export")){
                $buttons = $this->buttons;
            }
            $notices = '';

            $cfg = $this->cfg;
            $tp = url("/assets/crm/");
            $header = $this->header('Crm', 'index');
            $footer = $this->footer();
            $title = 'Brand Penetration Report (SW)';
            $category =  DB::table('category')->get();
            $states = DB::table('states')->where('country_id','=',101)->get();
            $brands = false;
            $precat = false;
            if (isset($_GET['category'])) {
                $precat = $_GET['category'];
                $cb = DB::table('category')->where('id', $precat)->first();
                $brands = DB::table('brand')->whereIn('id', explode(',', $cb->brands))->get();
            } else {
                $brands = DB::table('brand')->get();
            }
            $columns = [];
            $columns[] = array(
                'data' => 'state_name',
                'name' => 'name',
                'searchable' => true
            );
            if ($brands) {
                foreach ($brands as $brand) {
                    $col_name = str_replace(' ', '', strtolower($brand->name));
                    $columns[] = array(
                        'data' => $col_name,
                        'name' => $col_name,
                        'searchable' => false
                    );
                }
            }


            return view('crm/b-p-report-sw')->with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'category', 'brands', 'columns','states', 'precat', 'buttons'));
        }
        else if($slug == 'district-wise'){
            $user = DB::table('user')->where('secure', session('crm'))->first();
            if(!checkRole($user->u_id,"bpen_dw")){
                return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
            }
            $buttons = "[]";
            if(checkRole($user->u_id,"export")){
                $buttons = $this->buttons;
            }
            $notices = '';
//            dd($request->category);
            $cfg = $this->cfg;
            $tp = url("/assets/crm/");
            $header = $this->header('Crm', 'index');
            $footer = $this->footer();
            $title = 'Brand Penetration Report (DW)';
            $category = DB::table('category')->get();
            $states = DB::table('states')->where('country_id','=',101)->get();
            $brands = false;
            $precat = false;
            $records = DB::table('district')->select(['id','state_id','name as district_name']);
            if (isset($_GET['category'])) {
                $precat = $_GET['category'];
                $cb = DB::table('category')->where('id', $precat)->first();
                $brands = DB::table('brand')->whereIn('id', explode(',', $cb->brands))->get();
            }

            else{
                $brands = DB::table('brand')->get();
            }


            $columns = [];
            $columns[] = array(
                'data' => 'district_name',
                'name' => 'name',
                'searchable' => true
            );
            if ($brands) {
                foreach ($brands as $brand) {
                    $col_name = str_replace(' ', '', strtolower($brand->name));
                    $columns[] = array(
                        'data' => $col_name,
                        'name' => $col_name,
                        'searchable' => false
                    );
                }
            }


            return view('crm/b-p-report')->with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'category', 'states', 'brands', 'columns', 'brand_filter','precat', 'buttons'));
        }else{
            abort(404);
        }
    }
    public function crm_ticket_report(){
        $users = DB::table('user')->get();
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"tkt_rep")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $tickets = '';
        $notices = '';
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Ticket Report';
        $customer_type = DB::table('customer_category')->get();
        return view('crm/crm-ticket') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'users','notices', 'tickets','customer_type', 'buttons'));
    }

    public function crm_quick_entry_report(Request $request){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_qe_rep")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $entries = DB::table('crm_master_quick_entry')->get();
        foreach($entries as $entry){
            $final = array();
            $images = explode('<**>', $entry->image);
            foreach($images as $image) {
                try {
                    if(strpos($image, '.')){
                        $finalz = $image;
                    }else {
                        $imgdata = base64_decode($image);
                        if ($imgdata) {
                            $f = finfo_open();
                            $ext = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
                            finfo_close($f);
                            $filex = md5(time()) . '.' . substr($ext, 6);
                            $path = base_path() . '/assets/crm/images/user/';
                            Image::make($image)->save($path . $filex);
                            $finalz = $filex;
                        }
                    }
                    $final[] = $finalz;
                }catch(\Exception $ex){

                }

            }
            $fx = implode('<**>', $final);
            DB::update("Update crm_master_quick_entry set image='".$fx."' where id=".$entry->id);
        }
        $notices = '';
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Customer Quick Entry Report';
        $customer_type = DB::table('customer_category')->get();

        if(isset($_POST['name'])){
            $data['name'] = $_POST['name'];
            $data['company_name'] = $_POST['company_name'];
            $data['discription'] = $_POST['description'];
            $data['location'] = $_POST['location'];
            $data['lat'] = $_POST['lat'];
            $data['lng'] = $_POST['lng'];
            $user = DB::table('user')->where('secure', session('crm'))->first();
            $data['created_by'] = $user->u_id;
            $files = array();
            if($request->hasFile('image')) {
                foreach ($request->file('image') as $file){
                    $files[] = $filex = md5(time()).'.'.$file->getClientOriginalExtension();
                    $path = base_path().'/assets/crm/images/user/';
                    $img = Image::make($path.$filex)->save($path.$filex);
                    //$files[] = base64_encode(file_get_contents($file->path()));
                }
                //$data['image'] = base64_encode(file_get_contents($request->file('image')->path()));
            }
            $data['image'] = implode('<**>', $files);

            DB::table('crm_master_quick_entry')->insert($data);
        }
        return view('crm/crm-quick-entry-report') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices','customer_type', 'buttons'));
    }

    public function crm_quick_report_data(){
        $records = DB::table('crm_master_quick_entry');

        $selected_locality = '';
        return Datatables::of($records)
            ->editColumn('image',function($records){
                $images = explode('<**>', $records->image);
                $html = '';
                foreach($images as $image) {
                    /*$imgdata = base64_decode($image);
                    $f = finfo_open();
                    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);*/
                    $path = '/assets/crm/images/user/';
                    $html .= '<a style="margin-right: 5px;" href="'.url($path.$image).'" download><img height="40px" width="40px" src="'.url($path.$image).'" /></a>';
                    /*finfo_close($f);*/
                }
                return $html;
            })
            ->editColumn('location',function($records){
                if($records->location == null){
                    return $records->lat.', '.$records->lng;
                }else{
                    return $records->location;
                }
            })
            ->editColumn('created_by',function($records){
                if($records->created_by !== '') {
                    $cust = DB::table('user')->where('u_id', $records->created_by)->first();
                    if($cust !== null) {
                        return $cust->u_name;
                    }
                }
                return $records->created_by;
            })
            ->addColumn('action', function($records){
                $html = '<button value="'.$records->id.'" class="btn myred waves-light" id="quickdelete" style="padding:0 10px; margin-right: 5px"><i class="material-icons">delete</i></button><a href="customers?add='.$records->id.'" class="btn myblue waves-light" style="padding: 0 1rem">ADD</a>';
                return $html;
            })
            ->rawColumns(['action','image'])
            ->make(true);
    }

    function quickreportdelete(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        DB::table('crm_master_quick_entry')->where('id','=',$pid)->delete();
        $data = 'Data  Deleted Successfully.';
        return $data;
    }

    public function supplierCompetitionReport($slug = ''){
        if($slug == 'district-wise') {
            $user = DB::table('user')->where('secure', session('crm'))->first();
            if(!checkRole($user->u_id,"scomp_dw")){
                return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
            }
            $buttons = "[]";
            if(checkRole($user->u_id,"export")){
                $buttons = $this->buttons;
            }
            $notices = '';
            $cfg = $this->cfg;
            $tp = url("/assets/crm/");
            $header = $this->header('Crm', 'index');
            $footer = $this->footer();
            $title = 'Supplier Competition Report (DW)';
            $category = DB::table('category')->get();
            $states = DB::table('states')->where('country_id','=',101)->get();
            $suppliers = false;
            $precat = false;
            $prestate = false;
            if (isset($_GET['category'])) {
                $precat = $_GET['category'];
                $allcats = array($precat);
                $recat = 0;
                $recatx = true;
                $curcat[] = $precat;
                do {
                    $recats = array();
                    $recats = array_pluck(DB::table('category')->whereIn('parent', $curcat)->get(), 'id');
                    $curcat = $recats;
                    $recat = count($recats);
                    if ($recat == 0) {
                        $recatx = false;
                    }
                    array_push($allcats, implode(',', $recats));
                } while ($recatx);

                $suppliers = DB::table('crm_supplier')->whereRaw('FIND_IN_SET(?, product_category)', $allcats);

            } else {
                $suppliers = DB::table('crm_supplier');
            }
            if (isset($_GET['state'])) {
                $prestate = $_GET['state'];
                if ($_GET['state'] != 'all') {
                    $dis = array_pluck(DB::table('district')->where('state_id', $_GET['state'])->get(), 'id');
                    $pdata = array_pluck(DB::table('crm_products')->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
                        ->whereIn('district', $dis)->get(), 'supplier_id');
                    $sids = explode(',', implode(',', $pdata));
                    $suppliers = $suppliers->whereIn('id', $sids);
                }
            }
            $suppliers = $suppliers->get();
            $columns = [];
            $columns[] = array(
                'data' => 'district_name',
                'name' => 'name',
                'searchable' => true
            );
            if ($suppliers) {
                foreach ($suppliers as $sup) {
                    $col_name = $sup->id . str_replace('.', '', str_replace(' ', '', strtolower($sup->name)));
                    $columns[] = array(
                        'data' => $col_name,
                        'name' => $col_name,
                        'searchable' => false
                    );
                }
            }


            return view('crm/s-c-report')->with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'category', 'states', 'suppliers', 'columns', 'precat', 'prestate', 'buttons'));
        }elseif($slug == 'state-wise'){
            $user = DB::table('user')->where('secure', session('crm'))->first();
            if(!checkRole($user->u_id,"scomp_sw")){
                return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
            }
            $buttons = "[]";
            if(checkRole($user->u_id,"export")){
                $buttons = $this->buttons;
            }
            $notices = '';
            $cfg = $this->cfg;
            $tp = url("/assets/crm/");
            $header = $this->header('Crm', 'index');
            $footer = $this->footer();
            $title = 'Supplier Competition Report (SW)';
            $category = DB::table('category')->get();
            $states = DB::table('states')->where('country_id','=',101)->get();
            $suppliers = false;
            $precat = false;
            if (isset($_GET['category'])) {
                $precat = $_GET['category'];
                $allcats = array($precat);
                $recat = 0;
                $recatx = true;
                $curcat[] = $precat;
                do {
                    $recats = array();
                    $recats = array_pluck(DB::table('category')->whereIn('parent', $curcat)->get(), 'id');
                    $curcat = $recats;
                    $recat = count($recats);
                    if ($recat == 0) {
                        $recatx = false;
                    }
                    array_push($allcats, implode(',', $recats));
                } while ($recatx);

                $suppliers = DB::table('crm_supplier')->whereRaw('FIND_IN_SET(?, product_category)', $allcats);

            } else {
                $suppliers = DB::table('crm_supplier');
            }

            $suppliers = $suppliers->get();
//            $states = array_pluck(DB::table('states')->where('country_id', '101')->get(), 'id');
//            $pdata = array_pluck(DB::table('crm_products')->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
//                ->whereIn('state', $states)->get(), 'supplier_id');
//            $sids = explode(',', implode(',', $pdata));
//            $suppliers = $suppliers->whereIn('id', $sids)->get();

            $columns = [];
            $columns[] = array(
                'data' => 'state_name',
                'name' => 'name',
                'searchable' => true
            );
            if ($suppliers) {
                foreach ($suppliers as $sup) {
                    $col_name = $sup->id . str_replace('.', '', str_replace(' ', '', strtolower($sup->name)));
                    $columns[] = array(
                        'data' => $col_name,
                        'name' => $col_name,
                        'searchable' => false
                    );
                }
            }


            return view('crm/s-c-report-sw')->with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'category', 'suppliers', 'columns', 'precat','states' ,'buttons'));
        }else{
            abort(404);
        }

    }
    public function saveLocalityAjax(Request $request){
        if($request->locality_pop){
            $data = array();
            $data['district_id'] = $request->district_pop;
            $data['locality'] = $request->locality_pop;
            $data['block_id'] = $request->block_pop;
            DB::table('locality')->insert($data);
            return 'success';
        }else{
            return 'failed';
        }
    }
    public function manageBySaveAjax(Request $request){

        $cid = $request->cid;
        $desig_id = '';
        if($request->designation){
            $desig_id= implode(',',$request->designation);
            DB::update("UPDATE `crm_customers` SET `managed_by`='$desig_id' WHERE id = '$cid'");
            return 'success';
        }else{
            DB::update("UPDATE `crm_customers` SET `managed_by`='$desig_id' WHERE id = '$cid'");
            return 'success';
        }
    }

    public function LocalityMaster(Request  $request)
    {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_area")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $selected_district=array();
        if(isset($_POST['add'])){
            $data['district_id'] = $_POST['district'];
            $data['locality'] = $_POST['locality'];
            $data['block_id'] = $_POST['block'];
            DB::table('locality')->insert($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Area / Locality Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){
            $lid = $_POST['lid'];
            $did = $_POST['district'];
            $locality = $_POST['locality'];
            $block = $_POST['block'];

            DB::update("UPDATE `locality` SET `district_id`='$did',`locality`='$locality',`block_id`='$block' WHERE id = '$lid'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Area / Locality Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];

            $selected_district = DB::table('crm_customers')
                ->leftJoin('district', 'state_id', '=', 'crm_customers.state')
                ->where('crm_customers.id', '=', $cid)
                ->select('district.name as d_name','district.id as did')
                ->get();
        }
        $districts = DB::table('district')->get();
        $blocks = DB::table('blocks')->get();
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'CRM - Area / Locality';

        return view('crm/locality-master')->with(compact('header','cfg','tp','footer', 'title','notices','districts','blocks' ,'buttons'));

    }

    public function MarketingData(){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"ar_data")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Marketing Data';
        $selected_category = '';
        $selected_variant= '';
        $category = DB::table('category')->get();
        $all_variants = DB::table('product_variants')->get();
        $statesx = DB::table('states')->where('country_id','=',101)->get();
        $report_data = false;
        $districtsx = false;
        $pcc = '0.00';
        $cat =false;
        if(isset($_GET['category']) && isset($_GET['state'])){
            $cat = DB::table('category')->where('id', $_GET['category'])->first();
            $states = DB::table('states')->whereIn('id', $_GET['state'])->get();
            $districtsx = DB::table('district')->whereIn('state_id', $_GET['state'])->get();
            foreach($states as $state){
                if(isset($_GET['district'])){
                    $districts = DB::table('district')->whereIn('id', $_GET['district'])->where('state_id', $state->id)->get();
                }else{
                    $districts = DB::table('district')->where('state_id', $state->id)->get();
                }

                foreach($districts as $district){
                    $blocks = DB::table('blocks')->where('district_id', $district->id)->get();
                    $pop = 0;
                    foreach($blocks as $block){
                        $pop += ($block->population == 0 || !is_numeric($block->population)) ? 0 : $block->population;
                    }
                    //$pop = ($district->population === null || !is_numeric($district->population)) ? 0 : $district->population;
                    $pcc = ($cat->consumption_ratio === null || !is_numeric($cat->consumption_ratio)) ? 0 : $cat->consumption_ratio;
                    $contacted = DB::table('crm_customers')->where('district', $district->id)->where('class', 2)->count();
                    $interested = DB::table('crm_customers')->where('district', $district->id)->where('class', 3)->count();
                    $converted = DB::table('crm_customers')->where('district', $district->id)->where('class', 4)->count();
                    $report_data[] = array(
                        'state' => $state->name,
                        'district' => $district->name,
                        'population' => $pop,
                        'consumption' => ($pop*$pcc),
                        'contacted' => $contacted,
                        'interested' => $interested,
                        'converted' => $converted
                    );
                    /*usort($report_data, function($a, $b) {
                        return $a['popuation'] - $b['population'];
                    });*/
                }
            }
        }

        return view('crm/marketing-data')->with(compact('cfg','tp','header','footer','districtsx','title','selected_category','selected_variant','category','all_variants','statesx', 'report_data', 'pcc', 'cat'))->render();
    }

    public function Categorymaster(Request $request) {
        $cfg = '';
        $customer_type = '';
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_cat")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $data['type'] = $_POST['name'];

            DB::table('customer_category') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Category Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back();
        }

        if(isset($_POST['edit'])){
            $pro_cat = $_POST['product_category'];
            $cust_cat = $_POST['cid'];
            $record = DB::table('customer_category_price')->where('customer_category', $cust_cat)->where('product_category', $pro_cat);
            if($record->count()){
                $record->update(['price' => $_POST['price']]);
            }else{
                $data['customer_category'] = $cust_cat;
                $data['product_category'] = $pro_cat;
                $data['price'] = $_POST['price'];
                $rid = DB::table('customer_category_price') -> insertGetId($data);
            }
            return back();
        }

        $product_category = DB::table('category')->get();
        $customer_category = DB::table('customer_category')->get();
        if (isset($_GET['edit'])) {
            $records = DB::table('customer_category_price')->join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->where('customer_category_price.customer_category', $_GET['edit'])->orderBy('category.id', 'ASC')->get();
        }else{
            $records = DB::table('customer_category_price')->join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->orderBy('category.id', 'ASC')->get();
        }
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        return view('crm/category-master') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'customer_type', 'product_category', 'customer_category', 'records', 'buttons'));
    }


    public function Designationmaster(){
        $cfg = '';
        $customer_type = '';
        $user = DB::table('user')->where('secure', session('crm'))->first();
        $users = DB::table('user')->where('user_type','=','crm_user')->get();
        //$des = DB::table('customer_designation')->where('id',$_GET['edit'])->get();

        //$user_name = $des[0]->users;

        if(!checkRole($user->u_id,"mas_des")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $data['name'] = $_POST['name'];
            DB::table('customer_designation') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Designation Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back();
        }

        if(isset($_POST['edit'])){
            $users = '';
            $id = $_POST['cid'];
            $name = $_POST['name'];

            DB::update("UPDATE customer_designation set `name`='$name' where `id`='$id'");
            return back();
        }

        $product_category = DB::table('category')->get();
        $customer_designation = DB::table('customer_designation')->get();
        if (isset($_GET['edit'])) {
            $records = DB::table('customer_category_price')->join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->where('customer_category_price.customer_category', $_GET['edit'])->orderBy('category.id', 'ASC')->get();
        }else{
            $records = DB::table('customer_category_price')->join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->orderBy('category.id', 'ASC')->get();
        }
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        return view('crm/designation-master') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'customer_type', 'product_category', 'customer_designation','users', 'records', 'buttons'));
    }
    public function transportorMaster(){
        $cfg = '';
        $customer_type = '';

        $user = DB::table('user')->where('secure', session('crm'))->first();
        $users = DB::table('user')->where('user_type','=','crm_user')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $districts = DB::table('district')->get();
        $crm_contax = DB::table('crm_contax')->groupBY('name')->get();
        $customers = DB::table('crm_customers')->select('id','name')->paginate();

        if(!checkRole($user->u_id,"tran_mas")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if (isset($_POST['add'])) {

            $names = isset($_POST['names'])?serialize($_POST['names']):false;
            $contacts = isset($_POST['contacts'])?serialize($_POST['contacts']):false;
            $designations = isset($_POST['descs'])?serialize($_POST['descs']):false;
            $landlines = isset($_POST['landlines'])?serialize($_POST['landlines']):false;
            $name = $_POST['name'];
            $contact_no = $_POST['contact_number'];
            $contact_designation = $_POST['designation'];
            $landline = $_POST['landline'];

            $data['name'] = $name;
            $data['company_name'] = $_POST['cname'];
            $data['email'] = isset($_POST['emails'])?implode(',',$_POST['emails']):'';
            $data['company_address'] = $_POST['caddress'];
            $data['state'] = $_POST['states'];
            $data['district'] = $_POST['district'];
            $data['contact_no'] = $contact_no;
            $data['contact_designation'] = $contact_designation;
            $data['landline'] = $landline;
            $data['sec_names'] = $names;
            $data['sec_contacts'] = $contacts;
            $data['sec_landline'] = $landlines;
            $data['sec_designation'] = $designations;
            $data['refered_by'] = isset($_POST['listcust'])?implode(',',$_POST['listcust']):'';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $id =DB::table('transportor') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Transportor Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            if($_POST['goto']){

                return redirect()->to('crm/transportor_order-edit?cid='.$id);
            }
            return back();

        }

        if(isset($_POST['edit'])){
            $users = '';
            $id = $_POST['cid'];
            $names = isset($_POST['names'])?serialize($_POST['names']):false;
            $contacts = isset($_POST['contacts'])?serialize($_POST['contacts']):false;
            $designations = isset($_POST['descs'])?serialize($_POST['descs']):false;
            $landlines = isset($_POST['landlines'])?serialize($_POST['landlines']):false;
            $name = $_POST['name'];
            $company_name = $_POST['cname'];
            $email = isset($_POST['emails'])?implode(',',$_POST['emails']):false;
            $caddress = $_POST['caddress'];
            $state = $_POST['states'];
            $district = $_POST['district'];
            $contact_no = $_POST['contact_number'];
            $contact_designation = $_POST['designation'];
            $landline = $_POST['landline'];
            $refered_by = isset($_POST['listcust'])?implode(',',$_POST['listcust']):false;
            $updated_at = date('Y-m-d H:i:s');

            DB::update("UPDATE transportor set `name`='$name',`state`='$state' ,`company_name`='$company_name',`company_address`='$caddress',
`email`='$email',`district`='$district',`state`='$state',`contact_no`='$contact_no',`contact_designation`='$contact_designation',`landline`='$landline',`sec_names`='$names',`sec_contacts`='$contacts',`sec_landline`='$landlines',`sec_designation`='$designations',`refered_by`='$refered_by',`updated_at`='$updated_at' where `id`='$id'");
            return back();
        }


        $transportors = DB::table('transportor')->get();

        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        return view('crm/transportor-master') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'customer_type','transportors','users', 'buttons','states','crm_contax','customers','districts'));
    }
    public function Contactdesignation(){
        $cfg = '';
        $customer_type = '';
        $user = DB::table('user')->where('secure', session('crm'))->first();
        $users = DB::table('user')->where('user_type','=','crm_user')->get();
        //$des = DB::table('customer_designation')->where('id',$_GET['edit'])->get();

        //$user_name = $des[0]->users;

        if(!checkRole($user->u_id,"mas_cont")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $data['name'] = $_POST['name'];
            DB::table('crm_contax') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Contact  Designation Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back();
        }

        if(isset($_POST['edit'])){
            $users = '';
            $id = $_POST['cid'];
            $name = $_POST['name'];

            DB::update("UPDATE crm_contax set `name`='$name' where `id`='$id'");
            return back();
        }

        $product_category = DB::table('category')->get();
        $crm_contax = DB::table('crm_contax')->get();
        if (isset($_GET['edit'])) {
            $records = DB::table('customer_category_price')->join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->where('customer_category_price.customer_category', $_GET['edit'])->orderBy('category.id', 'ASC')->get();
        }else{
            $records = DB::table('customer_category_price')->join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->orderBy('category.id', 'ASC')->get();
        }
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        return view('crm/contact-designation') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'customer_type', 'product_category', 'crm_contax','users', 'records', 'buttons'));
    }

    public function assocMaster(Request $request) {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        $user_name = $user->u_id;
        if(!checkRole($user->u_id,"mas_cat")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $cfg='';
        $customer_type ='';
        if (isset($_POST['add'])) {
            $data['name'] = $_POST['name'];

            DB::table('crm_association') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Association Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }

        if(isset($_POST['edit'])){
            $id = $_POST['cidd'];
            $name = $_POST['name'];
            DB::update("UPDATE crm_association set `name`='$name' where `id`='$id'");
            $notices = '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Association Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';


        }

        $association = DB::table('crm_association')->get();

        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'Association';
        return view('crm/assoc-master') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'buttons','association'));
    }

    //om code
    public function Companymaster(Request $request) {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_cat")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $file = '';
            $file1 = '';
            if (request()->hasFile('image')){
                // Upload the downloadable file to product downloads directory
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/company_logo/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            if (request()->hasFile('latter_head')){
                // Upload the downloadable file to product downloads directory
                $name = request()->file('latter_head')->getClientOriginalName();
                $file1 = md5(time()).'.'.request()->file('latter_head')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/letter_head/';
                request()->file('latter_head')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            $data['name'] = $_POST['name'];
            $data['contact'] = $_POST['contact'];
            $data['address'] = $_POST['address'];
            $data['acc_detail'] = $_POST['acc_detail'];
            $data['gstno'] = $_POST['gstno'];
            $data['latter_head'] = $file1;
            $data['logo'] = $file;

            DB::table('company') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Company Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back();
        }

        if(isset($_POST['edit'])){
            $uid = $_POST['uid'];
            $name = $_POST['name'];
            $contact = $_POST['contact'];
            $address = $_POST['address'];


            DB::update("UPDATE `company` SET `name`='$name',`contact`='$contact',`address`='$address' WHERE id = '$uid'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Company Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }

//        $product_category = DB::table('category')->get();
        $company = DB::table('company')->get();
//        if (isset($_GET['edit'])) {
//            $records = DB::table('customer_category_price')->join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->where('customer_category_price.customer_category', $_GET['edit'])->orderBy('category.id', 'ASC')->get();
//        }else{
//            $records = DB::table('customer_category_price')->join('customer_category', 'customer_category.id', '=', 'customer_category_price.customer_category')->join('category', 'category.id', '=', 'customer_category_price.product_category')->select(['customer_category_price.*', 'customer_category.type', 'category.name'])->orderBy('category.id', 'ASC')->get();
//        }
        $tp = url("/assets/crm/");
        $cfg = $this->cfg;
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        return view('crm/company') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'buttons','company'));
    }


    public function CategoryGroupsMaster(Request $request) {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_pgroup")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $data = array();
        $records = false;
        if(isset($_POST['add'])){
            $data['category_id'] = $_POST['category'];
            $data['group_name'] = $_POST['group_name'];
            DB::table('category_groups')->insert($data);
            $notices = '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Price Group Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return redirect()->to('crm/price-groups-master')->with('notices', $notices);
        }
        if(isset($_GET['edit'])){
            $records = DB::table('category_groups')->find($_GET['edit']);
        }
        if(isset($_POST['edit'])){
            $category_id = $_POST['category'];
            $group_name = $_POST['group_name'];
            $id = $_POST['edit'];
            DB::update("UPDATE category_groups set `category_id`='$category_id', `group_name`='$group_name' where `id`='$id'");
            $notices = '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Price Group Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return redirect()->to('crm/price-groups-master')->with('notices', $notices);
        }

        $product_category = DB::table('category')->get();
        $category_groups = DB::table('category_groups')
            ->join('category', 'category.id', '=', 'category_groups.category_id')
            ->select('category_groups.id as id', 'category.name as name', 'category_groups.group_name')
            ->get();

        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        $cfg = $this->cfg;
        return view('crm/category-groups-master') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'product_category', 'category_groups', 'records', 'buttons'));
    }

    function Getprice(Request $request){

        $data = '';
        $ccid = $_POST['ccid'];
        $pcid = $_POST['pcid'];
        $data1  = DB::table('customer_category_price')->where('customer_category','=',$ccid)->where('product_category','=',$pcid)->select('price')->get();
        if(!empty($data1)){
            $data = $data1[0]->price;
        }

        return $data;
    }

    function Getcategoryallprice(Request $request){

        $data = '';
        $cid = $_POST['cid'];
        $html = '';
        $data1  = DB::table('customer_category_price')
            ->join('category', 'customer_category_price.product_category', '=', 'category.id')
            ->where('customer_category','=',$cid)
            ->select('customer_category_price.price', 'category.name,category.id')
            ->get();
        if(!empty($data1)){
            foreach ($data1 as $dt){
                $html .='<div class="input-field col s4">                          
                                <input  type="hidden"  name="pcid[]" value="'.$dt->id.'">
                                <input  type="text" disabled value="'.$dt->name.'">
                                <input  name="price[]" type="text"  value="'.$dt->price.'">                              
                            </div>
                            ';

            }
        }
        return $html;
    }

    function Getcategoryallpricecust(Request $request){

        $data = '';
        $cid = $_POST['custid'];
        $ctgid = $_POST['ctgid'];
        $html = '';
        $data1  = DB::table('crm_customers')->where('id','=',$cid)->where('price','<>','')->get();
        if(!empty($data1)){

            $ct = $data1[0]->customer_category;
            $pcid = explode(',',$data1[0]->product_category);
            $price = explode(',',$data1[0]->price);
            if($ctgid == $ct){
                $i = 0;
                foreach ($pcid as $p){
                    $data2  = DB::table('category')->where('id','=',$p)->first();
                    $name = $data2->name;
                    $id = $data2->id;
                    $html .='<div class="input-field col s3">                          
                                <input  type="hidden"  name="pcid[]" value="'.$id.'">
                                <input  type="text" disabled value="'.$name.'">
                                <input  name="price[]" type="text"  value="'.$price[$i].'">                              
                            </div>
                            ';
                    $i++;
                }
            }
            else{

            }

        }
        return $html;
    }

    public function CustomerData( Request $request){
        $lat = '';

        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"cst_data")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }

        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Customer Data';

        /*$category = '';
        $states = '';
        $district = '';
        $a = '';
        if(isset($_POST['search'])){

            if(isset($_POST['category'])){
                $category = implode(',',$_POST['category']);
                $a .= ' and crm_customers.customer_category IN ('.$category.')';
            }

            if(isset($_POST['states'])){
               $states = implode(',',$_POST['states']);
                $a .= ' and crm_customers.state IN ('.$states.')';
            }
            if(isset($_POST['district'])){
                $district = implode(',',$_POST['district']);
                $a .= ' and crm_customers.district IN ('.$district.')';
            }

            $customers = DB::select("SELECT s.name as state_name , d.name as district_name,crm_customers.*,customer_category.type,(SELECT COUNT(id) FROM crm_tickets WHERE crm_customer_id =crm_customers.id ) as tot_ticket , (SELECT COUNT(id) FROM crm_visits WHERE crm_customer_id = crm_customers.id ) as tot_visit,(SELECT COUNT(id) FROM crm_customer_projects WHERE crm_customer_id =crm_customers.id ) as tot_project , (SELECT COUNT(id) FROM crm_promotional WHERE crm_customer_id =crm_customers.id ) as tot_promo ,(SELECT COUNT(id) FROM crm_call WHERE crm_customer_id = crm_customers.id ) as tot_call FROM `crm_customers` INNER JOIN customer_category ON crm_customers.customer_category = customer_category.id INNER JOIN states as s on crm_customers.state = s.id INNER JOIN district as d ON crm_customers.district = d.id WHERE  1=1 $a");

        }
        else{
            $customers = DB::select("SELECT s.name as state_name , d.name as district_name,crm_customers.*,customer_category.type,(SELECT COUNT(id) FROM crm_tickets WHERE crm_customer_id =crm_customers.id ) as tot_ticket , (SELECT COUNT(id) FROM crm_visits WHERE crm_customer_id = crm_customers.id ) as tot_visit,(SELECT COUNT(id) FROM crm_customer_projects WHERE crm_customer_id =crm_customers.id ) as tot_project , (SELECT COUNT(id) FROM crm_promotional WHERE crm_customer_id =crm_customers.id ) as tot_promo ,(SELECT COUNT(id) FROM crm_call WHERE crm_customer_id = crm_customers.id ) as tot_call FROM `crm_customers` INNER JOIN customer_category ON crm_customers.customer_category = customer_category.id INNER JOIN states as s on crm_customers.state = s.id INNER JOIN district as d ON crm_customers.district = d.id ");

        }*/

//            dd($lat);
        $customer_category = DB::table('customer_category')->get();
        $product_category = DB::table('category')->get();


        $product_category = DB::table('category')->get();


        $customer_supplier = DB::table('crm_supplier')->get();

        $customer_brand = DB::table('brand')->get();
        $customer_type = DB::table('customer_type')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $cust_classes = DB::table('customer_class')->get();

        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = DB::table('crm_customers')->find($_GET['edit']);
            $s_districts = DB::table('district')->where('state_id','=',$cdata->state)->get();
        }

        return view('crm/customer-data')->with(compact('header','cfg','tp','customer_supplier','customer_brand','footer', 'title','notices','customer_category','states', 's_districts', 'cust_classes','customer_type', 'buttons','lat','product_category'));
    }
    public function visitReport( Request $request){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"vst_rep")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Visit Report';
        $customer_category = DB::table('customer_category')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $start_date = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $end_date = date('Y-m-d 23:59:59');
        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = DB::table('crm_customers')->find($_GET['edit']);
            $s_districts = DB::table('district')->where('state_id','=',$cdata->state)->get();
        }
        $cust_classes = DB::table('customer_class')->get();
        $users = DB::table('user')->get();


        return view('crm/visit-report')->with(compact('header','cfg','tp','footer', 'title','notices','customer_category','states', 's_districts','cust_classes','start_date','end_date', 'users', 'buttons'));
    }
    // om code
    public function callReport( Request $request){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"vst_rep")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Call Report';
        $customer_category = DB::table('customer_category')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();

        $start_date = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $end_date = date('Y-m-d 23:59:59');
        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = DB::table('crm_customers')->find($_GET['edit']);
            $s_districts = DB::table('district')->where('state_id','=',$cdata->state)->get();
        }
        $cust_classes = DB::table('customer_class')->get();
        $users = DB::table('user')->get();


        return view('crm/call-report')->with(compact('header','cfg','tp','footer', 'title','notices','customer_category','states', 's_districts', 'cust_classes','start_date','end_date', 'users', 'buttons'));
    }

    public function unqualiReport( Request $request){
        $lat = '';
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"cst_data")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Unqualified Report';

        if(isset($_POST['edit_reason'])){
            $cidd = $_POST['cidd'];

            $cid = $_POST['cid'];
            $reason = $_POST['reason'];
            $user = implode(',', $_POST['user']);
            $customer = DB::table('crm_customers')->find($cidd);


            DB::update("UPDATE `crm_customers` SET `unqualified_reason`='$reason',`user_unqualified` = '$user' WHERE id = '$cidd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Customer Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        $customer_category = DB::table('customer_category')->get();
        $customer_type = DB::table('customer_type')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $cust_classes = DB::table('customer_class')->get();
        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = DB::table('crm_customers')->find($_GET['edit']);
            $s_districts = DB::table('district')->where('state_id','=',$cdata->state)->get();
        }
        //$customer_class = DB::table('crm_customers')->get();

        $users = DB::table('user')->get();

        return view('crm/unqualified-report')->with(compact('header','cfg','tp','footer','users', 'title','notices','customer_category','states', 's_districts', 'cust_classes','customer_type', 'buttons','lat'));
    }

    public function unreachableReport( Request $request){
        $lat = '';
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"un_reach")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Unreachable Report';

        if(isset($_POST['edit_reason'])){

            $cidd = $_POST['cidd'];

            $cid = $_POST['cid'];
            $reason = $_POST['reason'];
            if(isset($_POST['user'])){
                $user = implode(',', $_POST['user']);
            }
            $customer = DB::table('crm_customers')->find($cidd);


            DB::update("UPDATE `crm_customers` SET `ureason`='$reason',`user_unreachable` = '$user' WHERE id = '$cidd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Customer Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        $customer_category = DB::table('customer_category')->get();
        $customer_type = DB::table('customer_type')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $cust_classes = DB::table('customer_class')->get();
        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = DB::table('crm_customers')->find($_GET['edit']);
            $s_districts = DB::table('district')->where('state_id','=',$cdata->state)->get();
        }
        //$customer_class = DB::table('crm_customers')->get();

        $users = DB::table('user')->get();

        return view('crm/unreachable-report')->with(compact('header','cfg','tp','footer','users', 'title','notices','customer_category','states', 's_districts', 'cust_classes','customer_type', 'buttons','lat'));
    }

    public  function leadOrderReport(){
        $lat = '';
        $start_date1 = date('Y-m-d 00:00:00', strtotime('1th January 2019'));
        $end_date1 = date('Y-m-d 23:59:59');

        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"cst_data")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }

        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Lead/Order Report';

        $customer_category = DB::table('customer_category')->get();

        $customer_type = DB::table('customer_type')->get();
        $product_category = DB::table('category')->get();
        $pipelines = DB::table('sales_pipelines')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $cust_classes = DB::table('customer_class')->get();
        $users = DB::table('user')->get();

        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = DB::table('crm_customers')->find($_GET['edit']);
            $s_districts = DB::table('district')->where('state_id','=',$cdata->state)->get();
        }


        return view('crm/lead-order-report')->with(compact('header','cfg','tp','pipelines','footer', 'title','notices','customer_category','states', 's_districts', 'cust_classes','customer_type', 'buttons','lat','product_category','start_date1','end_date1','users'));
    }
    public  function transportorReport(){
        $lat = '';
        $start_date1 = date('Y-m-d 00:00:00', strtotime('1th January 2019'));
        $end_date1 = date('Y-m-d 23:59:59');
        $states = DB::table('states')->where('country_id','=',101)->get();

        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"tran_repo")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }

        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Transportor Report';
        $transportors = DB::table('transportor')->get();
        return view('crm/transportor-report')->with(compact('header','cfg','tp','footer', 'title','notices','buttons','transportors','states','start_date1','end_date1'));
    }

    public function notinterestReport( Request $request){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"cst_data")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Not-Interested Report';

        if(isset($_POST['edit_reason'])){
            $cidd = $_POST['cidd'];

            $cid = $_POST['cid'];
            $reason = $_POST['reason'];
            $user = implode(',', $_POST['user']);
            $customer = DB::table('crm_customers')->find($cidd);


            DB::update("UPDATE `crm_customers` SET `notinterested_reason`='$reason',`user_notinterested` = '$user' WHERE id = '$cidd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Customer Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        $customer_category = DB::table('customer_category')->get();
        $customer_type = DB::table('customer_type')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $cust_classes = DB::table('customer_class')->get();
        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = DB::table('crm_customers')->find($_GET['edit']);
            $s_districts = DB::table('district')->where('state_id','=',$cdata->state)->get();
        }
        //$customer_class = DB::table('crm_customers')->get();

        $users = DB::table('user')->get();

        return view('crm/notinterested-report')->with(compact('header','cfg','tp','footer','users', 'title','notices','customer_category','states', 's_districts', 'cust_classes','customer_type', 'buttons','lat'));
    }

    //om code
    public function CustomersMapReport(Request $request){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"vst_rep")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';




        $lat = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->join('district', 'crm_customers.district', '=', 'district.id')
            ->select('crm_customers.lat','crm_customers.lng','crm_customers.id','district.lat as latitude','district.lng as longitude','crm_customers.name','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = crm_customers.id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = crm_customers.id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = crm_customers.id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'));

        $selected_category = array();
        $selected_state =  array();
        $selected_district =  array();
        $selected_class =  array();
        $selected_type =  array();

        if(isset($_POST['search'])){


            $cat = $request->category;
            $state = $request->states;
            $district = $request->district;
            $cust_class = $request->cust_class;
            $type = $request->type;


            if($cat){
                $selected_category = $request->category;
                $lat = $lat->whereIn('customer_category', $cat);
            }
            if($state){
                $selected_state = $request->states;
                $lat = $lat->whereIn('state', $state);

            }
            if($district){
                $selected_district = $request->district;
                $lat = $lat->whereIn('district', $district);
            }
            if($cust_class){
                $selected_class = $request->cust_class;
                if($cust_class !== 'all'){
                    $lat = $lat->where('class', $cust_class);
                }
            }
            if($type){
                $selected_type = $request->type;
                $lat = $lat->where('crm_customers.type',$type);
            }
            $lat = $lat->get();

        }

        else{
            $lat = $lat->get();
        }



//        foreach ($lat as $lat1) {
//            $latitude = '';
//            $longitude = '';
//            if ($lat1->lat != '' AND $lat1->lng != '') {
//
//                $latitude = $lat1->lat;
//                $longitude = $lat1->lng;
//
//
//            }
//        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Map Report';
        $customer_category = DB::table('customer_category')->get();
        $customer_type = DB::table('customer_type')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $cust_classes = DB::table('customer_class')->get();

        return view('crm/customers-map')->with(compact('header','cfg','tp','customer_category','selected_category','selected_state','selected_district','selected_class','selected_type','customer_type','states','cust_classes','footer','title','notices', 'users', 'buttons','lat'));
    }


    public function visitMapReport( Request $request){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"vst_rep")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';




        $lat = DB::table('crm_visits')
            ->join('crm_customers', 'crm_customers.id', '=', 'crm_visits.crm_customer_id')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->join('district', 'crm_customers.district', '=', 'district.id')
            ->select('crm_customers.lat','crm_customers.lng','crm_customers.id','district.lat as latitude','district.lng as longitude','crm_customers.name','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = crm_customers.id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = crm_customers.id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = crm_customers.id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'));

        $selected_category = array();
        $selected_state =  array();
        $selected_district =  array();
        $selected_class =  array();
        $selected_type =  array();
        $selected_user =  '';

        if(isset($_POST['search'])){


            $cat = $request->category;
            $state = $request->states;
            $district = $request->district;
            $cust_class = $request->cust_class;
            $type = $request->type;
            $acc_manager = $request->acc_manager;




            if($cat){
                $selected_category = $request->category;
                $lat = $lat->whereIn('customer_category', $cat);
            }
            if($state){
                $selected_state = $request->states;
                $lat = $lat->whereIn('state', $state);

            }
            if($district){
                $selected_district = $request->district;
                $lat = $lat->whereIn('district', $district);
            }

            if($type){
                $selected_type = $request->type;
                $lat = $lat->where('crm_customers.type',$type);
            }
            if($acc_manager){
                $selected_user = $request->acc_manager;
                if($acc_manager !== 'all')
                    $lat = $lat->whereRaw('FIND_IN_SET(?, user_id)', $acc_manager);
            }

            $lat = $lat->get();

        }

        else{
            $lat = $lat->get();
        }



//        foreach ($lat as $lat1) {
//            $latitude = '';
//            $longitude = '';
//            if ($lat1->lat != '' AND $lat1->lng != '') {
//
//                $latitude = $lat1->lat;
//                $longitude = $lat1->lng;
//
//
//            }
//        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Map Report';
        $customer_category = DB::table('customer_category')->get();
        $customer_type = DB::table('customer_type')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $cust_classes = DB::table('customer_class')->get();
        $users = DB::table('user')->get();
        $start_date = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $end_date = date('Y-m-d 23:59:59');

        return view('crm/visit-map')->with(compact('header','cfg','tp','users','start_date','end_date','customer_category','selected_category','selected_state','selected_district','selected_class','selected_user','selected_type','customer_type','states','cust_classes','footer','title','notices', 'users', 'buttons','lat'));
    }

    public function cvFrequency( Request $request){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"cv_freq")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Call Frequency';
        $std_date = date('Y-m-d', strtotime('first day of january this year'));
        $end_date = date('Y-m-d');
        $customer_category = DB::table('customer_category')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $users = DB::table('user')->get();
        $customer_class = DB::table('customer_class')->get();
        $customer_type = DB::table('customer_type')->get();
        $product_category = DB::table('category')->get();

        return view('crm/cv-frequency')->with(compact('header','cfg','tp','footer', 'title','customer_category','states', 'users', 'buttons','customer_class','product_category','customer_type','std_date','end_date'));
    }
    public function cvFrequencyData(Request $request){

        $start = 0;
        if(isset($request->start)){
            $start = $request->start;
        }
        $length = 10;
        if(isset($request->length)) {
            $length = $request->length;
        }

        $dates = (isset($_GET["dates"])) ? ($_GET["dates"]) : false;
        $where = '';
        $wherex = '';
        if($dates){
            $date_range = explode('$$', $dates);
            if($date_range[0] != '' && $date_range[1] != '') {
                $wherex = ' (select_date BETWEEN CAST(\'' . $date_range[0] . '\' AS DATE) AND CAST(\'' . $date_range[1] . '\' AS DATE))';
                $where = ' AND '.$wherex;
            }
        }
        $records = CrmCustomer::join('customer_category as cc', 'cc.id', '=', 'crm_customers.customer_category')
            ->join('temp_avg', 'crm_customers.id', '=', 'temp_avg.customer_id');

        $count_total = CrmCustomer::count();

        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $acc_manager = (isset($_GET["acc_manager"])) ? ($_GET["acc_manager"]) : false;
        $customer_class = (isset($_GET["customer_class"])) ? ($_GET["customer_class"]) : false;
        $product_category = (isset($_GET["product_category"])) ? ($_GET["product_category"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;

        if($category){
            $records = $records->whereIn('crm_customers.customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($acc_manager){
            if($acc_manager !== 'all') {
                $records = $records->whereRaw('FIND_IN_SET(?, crm_customers.assign_users)', $acc_manager);
            }
        }
        if($customer_class){
            $records = $records->whereIn('crm_customers.class',$customer_class);
        }
        if($type){
            $records = $records->whereIn('crm_customers.type',$type);
        }
        if($product_category){
            $cats = array_pluck(DB::table('category')->whereIn('id', $product_category)->orWhereIn('parent', $product_category)->get(), 'id');
            $newCats = true;
            $temp = $cats;
            while($newCats){
                $temp = array_pluck(DB::table('category')->whereIn('parent', $temp)->get(), 'id');
                if(count($temp)){
                    $cats = array_merge($cats, $temp);
                }else{
                    $newCats = false;
                }
            }

            $cats = array_unique($cats);
            $products = array_pluck(DB::table('products')->whereIn('category', $cats)->get(), 'id');
            $cust_ids = array_pluck(DB::table('crm_products')->whereRaw('FIND_IN_SET_X(?, product_id)', $products)->get(), 'crm_customer_id');
            $records->whereIn('crm_customers.id', $cust_ids);
        }
        $count_filter = $records->count();
        $records->select('crm_customers.id', 'crm_customers.name','crm_customers.contact_no', 'cc.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'),  DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id '.$where.') as calls'), DB::raw('(select MAX(select_date) from crm_call where crm_call.crm_customer_id = crm_customers.id '.$where.') as last_call'), 'temp_avg.median as median', 'temp_avg.average as average', 'crm_customers.assign_users as user_id', 'crm_customers.verified')
            ->paginate();
        //dd($records);
        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="cv-frequency";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('last_call', function($records){
                $d =getLastCall($records->id);
                return '<span class="label label-default">'.$d['diff'].'</span><br/>'.date_format(date_create($d['date']), 'd M, Y');
            })
            /*->editColumn('median', function($records) use ($data){
                return isset($data[$records->median]['median']) ? $data[$records->median]['median'] : 0;
            })
            ->editColumn('average', function($records) use ($data){
                return isset($data[$records->median]['average']) ? $data[$records->median]['average'] : 0;
            })*/
            ->editColumn('user_id', function($records){
                $users = array_pluck(DB::table('user')->whereIn('u_id', explode(',', $records->user_id))->get(), 'u_name');
                return implode(', ', $users);
            })
            ->editColumn('name', function($records){
                $html = '';
                if($records->verified){
                    $html = $records->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    $html = $records->name;
                }
                return $html;
            })
            ->rawColumns(['name', 'last_call', 'action'])
            ->with([
                "recordsTotal" => $count_total,
                "recordsFiltered" => $count_filter
            ])
            ->make(true);

        /*$dates = (isset($_GET["dates"])) ? ($_GET["dates"]) : false;
        $where = '';
        if($dates){
            $date_range = explode('$$', $dates);
            if($date_range[0] != '' && $date_range[1] != '') {
                $where = ' AND (select_date BETWEEN CAST(\'' . $date_range[0] . '\' AS DATE) AND CAST(\'' . $date_range[1] . '\' AS DATE))';
            }
        }
        $tb = '(SELECT x.crm_customer_id as xcid, DATEDIFF(CAST(y.select_date as date), CAST(x.select_date as date)) as diff
                            FROM crm_call AS x
                            INNER JOIN crm_call AS y ON (x.crm_customer_id = y.crm_customer_id AND x.select_date < y.select_date)
                            GROUP BY x.id
ORDER BY `x`.`crm_customer_id`  ASC)';
        $averageQuery = '(select c.id as cid, AVG(diff) as average from '.$tb.' z INNER JOIN crm_customers c on c.id = z.xcid
GROUP BY xcid)';


        $medianQuery = '(SELECT xcid, AVG(dd.diff) as md
                        FROM (
                        SELECT d.xcid, d.diff, @rownum:=@rownum+1 as `row_number`, @total_rows:=@rownum
                          FROM '.$tb.' d, (SELECT @rownum:=0) r
                          GROUP BY d.xcid
                          ORDER BY d.diff ASC
                        ) as dd
                        WHERE dd.row_number IN ( FLOOR((@total_rows+1)/2), FLOOR((@total_rows+2)/2) )) z';

//        dd($medianQuery);
        $start = 0;
        $length = 10;
        if(isset($request->start)){
            $start = $request->start;
        }
        if(isset($request->length)){
            $length = $request->length;
        }
        $records = DB::table('crm_customers')
            ->join('customer_category as cc', 'cc.id', '=', 'crm_customers.customer_category')
            ->join(DB::raw($averageQuery.' avResult'), 'avResult.cid', '=', 'crm_customers.id')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.contact_no', 'cc.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'),  DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id '.$where.') as calls'), DB::raw('(select MAX(select_date) from crm_call where crm_call.crm_customer_id = crm_customers.id '.$where.') as last_call'), 'avResult.average as median', 'avResult.average', 'crm_customers.assign_users as user_id')
            ->skip($start)
            ->take($length);
        $recordsx = DB::table('crm_customers')
            ->join('customer_category as cc', 'cc.id', '=', 'crm_customers.customer_category')
            ->join('crm_call', 'crm_call.crm_customer_id', '=', 'crm_customers.id')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.contact_no', 'cc.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'),  DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id '.$where.') as calls'), DB::raw('(select MAX(select_date) from crm_call where crm_call.crm_customer_id = crm_customers.id '.$where.') as last_call'), DB::raw('"-1" as median'), DB::raw('"-1" as average'), 'crm_customers.assign_users as user_id')
            ->whereNotIn('crm_customers.id', array_pluck($records->get(), 'id'))->skip($start)
            ->take($length);

        //dd($recordsx->get());


        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $acc_manager = (isset($_GET["acc_manager"])) ? ($_GET["acc_manager"]) : false;
        $customer_class = (isset($_GET["customer_class"])) ? ($_GET["customer_class"]) : false;
        $product_category = (isset($_GET["product_category"])) ? ($_GET["product_category"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;

        if($category){
            $records = $records->whereIn('crm_customers.customer_category', $category);
            $recordsx = $recordsx->whereIn('crm_customers.customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
            $recordsx = $recordsx->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
            $recordsx = $recordsx->whereIn('district', $district);
        }
        if($acc_manager){
            if($acc_manager !== 'all') {
                $records = $records->whereRaw('FIND_IN_SET(?, crm_customers.assign_users)', $acc_manager);
                $recordsx = $recordsx->whereRaw('FIND_IN_SET(?, crm_customers.assign_users)', $acc_manager);
            }
        }
        if($customer_class){
            $records = $records->whereIn('crm_customers.class',$customer_class);
            $recordsx = $recordsx->whereIn('crm_customers.class',$customer_class);
        }
        if($type){
            $records = $records->whereIn('crm_customers.type',$type);
            $recordsx = $recordsx->whereIn('crm_customers.type',$type);
        }
        if($product_category){
            $cats = array_pluck(DB::table('category')->whereIn('id', $product_category)->orWhereIn('parent', $product_category)->get(), 'id');
            $newCats = true;
            $temp = $cats;
            while($newCats){
                $temp = array_pluck(DB::table('category')->whereIn('parent', $temp)->get(), 'id');
                if(count($temp)){
                    $cats = array_merge($cats, $temp);
                }else{
                    $newCats = false;
                }
            }

            $cats = array_unique($cats);
            $products = array_pluck(DB::table('products')->whereIn('category', $cats)->get(), 'id');
            $cust_ids = array_pluck(DB::table('crm_products')->whereRaw('FIND_IN_SET_X(?, product_id)', $products)->get(), 'crm_customer_id');
            $records->whereIn('crm_customers.id', $cust_ids);
            $recordsx->whereIn('crm_customers.id', $cust_ids);
        }
        $records = $records->unionAll($recordsx);


        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="cv-frequency";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('last_call', function($records){
                $d =getLastCall($records->id);
                return '<span class="label label-default">'.$d['diff'].'</span><br/>'.date_format(date_create($d['date']), 'd M, Y');
            })
            ->editColumn('median', function($records) use ($tb){
                $dataset = array_pluck(DB::select('Select * from '.$tb.' z where z.xcid='.$records->id), 'diff');
                return number_format(median($dataset), 2, '.', '');
            })
            ->editColumn('average', function($records){
                if($records->average == '-1'){
                    return number_format(0, 2, '.', '');
                }
                return number_format($records->average, 2, '.', '');
            })
            ->editColumn('user_id', function($records){
                $users = array_pluck(DB::table('user')->whereIn('u_id', explode(',', $records->user_id))->get(), 'u_name');
                return implode(', ', $users);
            })
            ->rawColumns(['last_call', 'action'])
            ->make(true);*/
    }
    public function cvFrequencyDataNew(Request $request){
        $start = 0;
        if(isset($request->start)){
            $start = $request->start;
        }
        $length = 10;
        if(isset($request->length)) {
            $length = $request->length;
        }
        $dates = (isset($_GET["dates"])) ? ($_GET["dates"]) : false;

        $records = CrmCustomer::join('customer_category as cc', 'cc.id', '=', 'crm_customers.customer_category')
                    ->select(['crm_customers.*', 'cc.type', 'cc.id as last_call', 'crm_customers.assign_users as manager', 'crm_customers.total_calls as calls', 'call_average as average', 'call_median as median'])
                    ->where('call_average', '<>', 0.00)->where('total_calls','!=',0);

        if($dates){
            $date_range = explode('$$', $dates);
            if($date_range[0] != '' && $date_range[1] != '') {
                $cids1 = CrmCall::whereRaw(' (select_date BETWEEN CAST(\'' . $date_range[0] . '\' AS DATE) AND CAST(\'' . $date_range[1] . '\' AS DATE))')->get()->pluck('crm_customer_id');
                $records = $records->whereIn('crm_customers.id', $cids1);
            }
        }

        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $acc_manager = (isset($_GET["acc_manager"])) ? ($_GET["acc_manager"]) : false;
        $customer_class = (isset($_GET["customer_class"])) ? ($_GET["customer_class"]) : false;
        $product_category = (isset($_GET["product_category"])) ? ($_GET["product_category"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;
        if($category){
            $records = $records->whereIn('crm_customers.customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($acc_manager){
            if($acc_manager !== 'all') {
                $records = $records->whereRaw('FIND_IN_SET(?, crm_customers.assign_users)', $acc_manager);
            }
        }
        if($customer_class){
            $records = $records->whereIn('crm_customers.class',$customer_class);
        }
        if($type){
            $records = $records->whereIn('crm_customers.type',$type);
        }
        if($product_category){
            $cats = array_pluck(DB::table('category')->whereIn('id', $product_category)->orWhereIn('parent', $product_category)->get(), 'id');
            $newCats = true;
            $temp = $cats;
            while($newCats){
                $temp = array_pluck(DB::table('category')->whereIn('parent', $temp)->get(), 'id');
                if(count($temp)){
                    $cats = array_merge($cats, $temp);
                }else{
                    $newCats = false;
                }
            }

            $cats = array_unique($cats);
            $products = array_pluck(DB::table('products')->whereIn('category', $cats)->get(), 'id');
            $cust_ids = array_pluck(DB::table('crm_products')->whereRaw('FIND_IN_SET_X(?, product_id)', $products)->get(), 'crm_customer_id');
            $records->whereIn('crm_customers.id', $cust_ids);
        }
        $count_total = $records->count();
        $count_filter = $records->count();
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="cv-frequency";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('state', function($records){
                $d =getStateName($records->state);
                return $d;
            })
            ->editColumn('district', function($records){
                $d =getDistrictName($records->district);
                return $d;
            })
            ->editColumn('locality', function($records){
                $d =getLocalityName($records->locality);
                return $d;
            })->editColumn('total_calls', function($records){
                $d =getTotalCall($records->id);
                return $records->total_calls;
            })->editColumn('median', function($records){
                return round($records->median);
            })->editColumn('average', function($records){
                return round($records->average);
            })
            ->editColumn('call_days', function($records){
                    $d =getLastCall($records->id);
                   return '<span class="label label-default">'.$records->call_days.' days</span><br/>'.$records->call_last_updated;

            })
            ->editColumn('managed_by', function($records){
                $dex = array();
                $users = explode(',', $records->managed_by);
                $u = array_pluck(DB::table('user')->whereIn('u_id', $users)->get(), 'u_name');

                $d = DB::table('customer_designation')->get();
                foreach ($d as $des) {
                    if (in_array('des' . $des->id, $users)) {
                        $dex[] = $des->name;
                    }
                }
                $data =  array_merge($dex,$u);
                return implode(',',$data);
            })
            ->editColumn('name', function($records){
                $html = '';
                if($records->verified){
                    $html = $records->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    $html = $records->name;
                }
                return $html;
            })
            ->rawColumns(['name', 'call_days', 'action'])
            ->with([
                "recordsTotal" => $count_total,
                "recordsFiltered" => $count_filter
            ])
            ->make(true);
    }
    public function updateCallRecords(){
        $cids = CrmCall::groupBy('crm_customer_id')->pluck('crm_customer_id');

        foreach ($cids as $cid) {
            $records = CrmCall::where('crm_customer_id', $cid)->pluck('select_date');
            $diff = array();
            for ($i = 0; $i < count($records) - 1; $i++) {
                $to = Carbon::createFromFormat('Y-m-d', $records[$i]);
                $from = Carbon::createFromFormat('Y-m-d', $records[$i + 1]);
                $diff[] = $to->diffInDays($from);
            }
            CrmCustomer::where('id', $cid)->update(['call_average'=>average($diff), 'call_median'=>median($diff), 'call_last_updated'=>Carbon::now()]);
            /*if($customer !== null) {
                $customer->call_average = average($diff);
                $customer->call_median = median($diff);
                $customer->call_last_updated = Carbon::now();
                $customer->save();
            }*/
        }
        return back();
    }
    public function updateVisitRecords(){

    }

    public function cvFrequencyVisit( Request $request){
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"cv_freq")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Visit Frequency';
        $customer_category = DB::table('customer_category')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $users = DB::table('user')->get();
        $customer_class = DB::table('customer_class')->get();
        $product_category = DB::table('category')->get();
        $customer_type = DB::table('customer_type')->get();

        return view('crm/cv-frequency-visit')->with(compact('header','cfg','tp','footer', 'title','customer_category','states', 'users', 'buttons','customer_class','product_category','customer_type'));
    }
    public function cvFrequencyVisitData( Request $request){
        $dates = (isset($_GET["dates"])) ? ($_GET["dates"]) : false;
        $where = '';
        if($dates){
            $date_range = explode('$$', $dates);
            if($date_range[0] != '' && $date_range[1] != '') {
                $where = ' AND (select_date BETWEEN CAST(\'' . $date_range[0] . '\' AS DATE) AND CAST(\'' . $date_range[1] . '\' AS DATE))';
            }
        }
        $tb = '(SELECT x.crm_customer_id as xcid, DATEDIFF(CAST(y.select_date as date), CAST(x.select_date as date)) as diff
                            FROM crm_visits AS x
                            INNER JOIN crm_visits AS y ON (x.crm_customer_id = y.crm_customer_id AND x.select_date < y.select_date)
                            GROUP BY x.id  
ORDER BY `x`.`crm_customer_id`  ASC)';
        $averageQuery = '(select c.id as cid, AVG(diff) as average from '.$tb.' z INNER JOIN crm_customers c on c.id = z.xcid
GROUP BY xcid)';

        $medianQuery = '(SELECT xcid, AVG(dd.diff) as md
                        FROM (
                        SELECT d.xcid, d.diff, @rownum:=@rownum+1 as `row_number`, @total_rows:=@rownum
                          FROM '.$tb.' d, (SELECT @rownum:=0) r
                          GROUP BY d.xcid
                          ORDER BY d.diff ASC
                        ) as dd
                        WHERE dd.row_number IN ( FLOOR((@total_rows+1)/2), FLOOR((@total_rows+2)/2) )) z';


        $records = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->join(DB::raw($averageQuery.' avResult'), 'avResult.cid', '=', 'crm_customers.id')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'),  DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id '.$where.') as visits'), DB::raw('(select MAX(select_date) from crm_visits where crm_visits.crm_customer_id = crm_customers.id '.$where.') as last_visit'), 'avResult.average as median', 'avResult.average', 'crm_customers.assign_users as user_id', 'crm_customers.verified');
//return $records->toSql();
        $recordsx = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->join('crm_visits', 'crm_visits.crm_customer_id', '=', 'crm_customers.id')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'),  DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id '.$where.') as visits'), DB::raw('(select MAX(select_date) from crm_visits where crm_visits.crm_customer_id = crm_customers.id '.$where.') as last_visit'), DB::raw('"-1" as median'), DB::raw('"-1" as average'), 'crm_customers.assign_users as user_id', 'crm_customers.verified')
            ->whereNotIn('crm_customers.id', array_pluck($records->get(), 'id'));



        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $acc_manager = (isset($_GET["acc_manager"])) ? ($_GET["acc_manager"]) : false;
        $customer_class = (isset($_GET["customer_class"])) ? ($_GET["customer_class"]) : false;
        $product_category = (isset($_GET["product_category"])) ? ($_GET["product_category"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;

        if($category){
            $records = $records->whereIn('customer_category', $category);
            $recordsx = $recordsx->whereIn('customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
            $recordsx = $recordsx->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
            $recordsx = $recordsx->whereIn('district', $district);
        }
        if($acc_manager){
            if($acc_manager !== 'all'){
                $records = $records->whereRaw('FIND_IN_SET(?, crm_customers.assign_users)', $acc_manager);
                $recordsx = $recordsx->whereRaw('FIND_IN_SET(?, crm_customers.assign_users)', $acc_manager);
            }

        }

        if($customer_class){
            $records = $records->whereIn('crm_customers.class', $customer_class);
            $recordsx = $recordsx->whereIn('crm_customers.class', $customer_class);
        }
        if($type){
            $records = $records->whereIn('crm_customers.type', $type);
            $recordsx = $recordsx->whereIn('crm_customers.type', $type);
        }
        if($product_category){
            $cats = array_pluck(DB::table('category')->whereIn('id', $product_category)->orWhereIn('parent', $product_category)->get(), 'id');
            $newCats = true;
            $temp = $cats;
            while($newCats){
                $temp = array_pluck(DB::table('category')->whereIn('parent', $temp)->get(), 'id');
                if(count($temp)){
                    $cats = array_merge($cats, $temp);
                }else{
                    $newCats = false;
                }
            }
            $cats = array_unique($cats);
            $products = array_pluck(DB::table('products')->whereIn('category', $cats)->get(), 'id');
            $cust_ids = array_pluck(DB::table('crm_products')->whereRaw('FIND_IN_SET_X(?, product_id)', $products)->get(), 'crm_customer_id');
            $records->whereIn('crm_customers.id', $cust_ids);
            $recordsx->whereIn('crm_customers.id', $cust_ids);
        }

        $records = $records->unionAll($recordsx);

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="cv-frequency-visit";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('last_visit', function($records){
                $d = getLastVisit($records->id);
                return '<span class="label label-default">'.$d['diff'].'</span><br/>'.date_format(date_create($d['date']), 'd M, Y');
            })
            ->editColumn('median', function($records) use ($tb){
                $dataset = array_pluck(DB::select('Select * from '.$tb.' z where z.xcid='.$records->id), 'diff');
                return number_format(median($dataset), 2, '.', '');
            })
            ->editColumn('average', function($records){
                if($records->average == '-1'){
                    return number_format(0, 2, '.', '');
                }
                return number_format($records->average, 2, '.', '');
            })
            ->editColumn('user_id', function($records){
                $users = array_pluck(DB::table('user')->whereIn('u_id', explode(',', $records->user_id))->get(), 'u_name');
                return implode(', ', $users);
            })
            ->editColumn('name', function($records){
                $html = '';
                if($records->verified){
                    $html = $records->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    $html = $records->name;
                }
                return $html;
            })
            ->rawColumns(['name', 'last_visit', 'action'])
            ->make(true);
    }
    public function ConvertedCustomers( Request $request){

        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"conv_cst")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Converted Customers';
        $customer_category = DB::table('customer_category')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $start_date = date('Y-01-01 00:00:00');
        $end_date = date('Y-m-d 23:59:59');
        $s_districts = array();
        if(isset($_GET['edit'])) {
            $cdata = DB::table('crm_customers')->find($_GET['edit']);

            $s_districts = DB::table('district')->where('state_id','=',$cdata->state)->get();
        }
        $customer_class = DB::table('crm_customers')->get();
        $users = DB::table('user')->get();

        return view('crm/converted-customers')->with(compact('header','cfg','tp','footer', 'title','notices','customer_category','states', 's_districts', 'customer_class', 'users','start_date','end_date', 'buttons'));
    }
    public function getVisitsData(Request $request){

        $records = DB::table('crm_visits')
            ->join('crm_customers', 'crm_customers.id', '=', 'crm_visits.crm_customer_id')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'), 'crm_visits.user_id', 'crm_visits.select_date', 'crm_customers.verified');


        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $acc_manager = (isset($_GET["acc_manager"])) ? ($_GET["acc_manager"]) : false;
        $class = (isset($_GET["cust_class"])) ? ($_GET["cust_class"]) : false;
        $dates = (isset($_GET["dates"])) ? ($_GET["dates"]) : false;

        if($category){
            $records = $records->whereIn('customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($class){
            $records = $records->whereIn('crm_customers.class', $class);
        }
        if($acc_manager){
            if($acc_manager !== 'all')
                $records = $records->whereRaw('FIND_IN_SET(?, user_id)', $acc_manager);
        }
        if($dates){
            if($dates == '$$'){
                $dates = date('Y-m-d 00:00:00',strtotime('-30 days')).'$$'.date('Y-m-d 23:59:59');
            }
            $date_range = explode('$$', $dates);
            $records->whereBetween('select_date', [$date_range[0].' 00:00:00', $date_range[1].' 23:59:59']);
        }else{
            $dates = date('Y-m-d 00:00:00').'$$'.date('Y-m-d 23:59:59');
            $date_range = explode('$$', $dates);
            $records->whereBetween('select_date', [$date_range[0], $date_range[1]]);
        }

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="visit-report";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('user_id', function($records){
                $users = explode(',', $records->user_id);
                $u = array_pluck(DB::table('user')->whereIn('u_id', $users)->get(), 'u_name');

                return implode(',', $u);
            })
            ->editColumn('select_date', function($records){
                return date("d-m-Y", strtotime($records->select_date));
            })
            ->editColumn('name', function($records){
                $html = '';
                if($records->verified){
                    $html = $records->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    $html = $records->name;
                }
                return $html;
            })
            ->rawColumns(['name', 'action'])
            ->make(true);
    }

    // om code
    public function getCallsData(Request $request){

        $records = DB::table('crm_customers')
            ->join('crm_call', 'crm_call.crm_customer_id', '=', 'crm_customers.id')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('crm_customers.id', 'crm_call.contact_person as name','crm_customers.name as cname','crm_call.customer_number','crm_customers.contact_no', 'customer_category.type','crm_call.user_id', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'),'crm_call.user_id','crm_call.select_date','crm_call.created_date','crm_call.time', 'crm_customers.verified');

        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $acc_manager = (isset($_GET["acc_manager"])) ? ($_GET["acc_manager"]) : false;
        $class = (isset($_GET["cust_class"])) ? ($_GET["cust_class"]) : false;
        $dates = (isset($_GET["dates"])) ? ($_GET["dates"]) : false;

        if($category){
            $records = $records->whereIn('customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($class){
            $records = $records->whereIn('crm_customers.class', $class);
        }
        if($acc_manager){
            if($acc_manager !== 'all')
                $records = $records->whereRaw('FIND_IN_SET(?, user_id)', $acc_manager);
        }
        if($dates){
            if($dates == '$$'){
                $dates = date('Y-m-d 00:00:00',strtotime('-30 days')).'$$'.date('Y-m-d 23:59:59');
            }
            $date_range = explode('$$', $dates);
            $records->whereBetween('select_date', [$date_range[0].' 00:00:00', $date_range[1].' 23:59:59']);
        }else{
            $dates = date('Y-m-d 00:00:00').'$$'.date('Y-m-d 23:59:59');
            $date_range = explode('$$', $dates);
            $records->whereBetween('select_date', [$date_range[0], $date_range[1]]);
        }
        
        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="call-report";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->addColumn('user_id', function($records){
                $users = explode(',', $records->user_id);
                $u = array_pluck(DB::table('user')->whereIn('u_id', $users)->get(), 'u_name');

                return implode(',', $u);
            })
            ->editColumn('select_date', function($records){
                return date("d-m-Y", strtotime($records->select_date));
            })->editColumn('customer_number', function($records){
                return $records->customer_number;
            })
            ->editColumn('name', function($records){

                $html = '';
                if($records->verified){
                    if($records->id == $records->name){
                        $html = $records->cname;
                    }else{
                        $html = $records->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                    }

                }else{
                    if($records->id == $records->name){
                        $html = $records->cname;
                    }else{
                        $html = $records->name;
                    }
                }
                return $html;
            })
            ->rawColumns(['name', 'action'])
            ->make(true);
    }
    public function getUnqualifiedData(Request $request){

        $records = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.unqualified_date','crm_customers.unqualified_reason','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = crm_customers.id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = crm_customers.id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = crm_customers.id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'))
            ->where('crm_customers.class','=',5);

        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $class = (isset($_GET["cust_class"])) ? ($_GET["cust_class"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;


        if($category){
            $records = $records->whereIn('customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($class){
            if($class !== 'all'){
                $records = $records->where('class', $class);
            }
        }
        if($type){
            $records = $records->where('crm_customers.type',$type);
        }


        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="customer-data";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a><br><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="unqualified-report?edit='.$records->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->make(true);
    }

    public function getUnreachableData(Request $request){

        $records = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.dunreachable','crm_customers.ureason','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = crm_customers.id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = crm_customers.id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = crm_customers.id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'))
            ->where('crm_customers.class','=',7);

        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $class = (isset($_GET["cust_class"])) ? ($_GET["cust_class"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;


        if($category){
            $records = $records->whereIn('customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($class){
            if($class !== 'all'){
                $records = $records->where('class', $class);
            }
        }
        if($type){
            $records = $records->where('crm_customers.type',$type);
        }


        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->editColumn('unreachable_date',function($records){
                return $records->dunreachable;
            })
            ->editColumn('unreachable_reason',function($records){
                return $records->ureason;
            })
            ->addColumn('action', function($records){
                $a="customer-data";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a><br><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="unreachable-report?edit='.$records->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->make(true);
    }


    public function getLeadsData(Request $request){

        $records = DB::table('crm_customers')
            ->join('lead_orders', 'lead_orders.crm_customer_id', '=', 'crm_customers.id')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('lead_orders.id as id','lead_orders.crm_customer_id as cid','crm_customers.name','crm_customers.sec_names','crm_customers.sec_contact','lead_orders.id','lead_orders.pipelines_id','lead_orders.pipelines_stage','lead_orders.select_date','lead_orders.created_date','crm_customers.product_category','lead_orders.edited_by', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = lead_orders.crm_customer_id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = lead_orders.crm_customer_id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = lead_orders.crm_customer_id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = lead_orders.crm_customer_id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = lead_orders.crm_customer_id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'), 'crm_customers.verified');

        $count_total = $records->count();
        $category = $request->category?$request->category:false;
        $state = $request->state?$request->state:false;
        $district = $request->district?$request->district:false;
        $group = $request->group?$request->group:false;
        $class = $request->cust_class?$request->cust_class:false;
        $type = $request->type?$request->type:false;
        $verified = $request->verified?$request->verified:false;
        $pro_category = $request->pro_category?$request->pro_category:false;
        $pipeline = $request->pipeline?$request->pipeline:false;
        $pipeline_stage = $request->stage?$request->stage:false;
        $startdate1 = $request->date_start1?$request->date_start1:false;
        $enddate1 = $request->date_end1?$request->date_end1:false;
        $suser = $request->user?$request->user:false;
        $searchuser = $request->search?$request->search:'';

        $start = $_GET['start'];
        $length = $_GET['length'];
        if($category){
            $records = $records->whereIn('customer_category', $category);
        }

        if($group){
            $records = $records->whereIn('customer_designation', $group);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($class){
            $records = $records->whereIn('class', $class);

        }
        if($type){
            $records = $records->where('crm_customers.type',$type);
        }

        if($pro_category){
            $proc = array_pluck(DB::table('category')->whereIn('id',$pro_category)->get()->toArray(), 'id');
            $prop = array_pluck(DB::table('crm_customers')->whereRaw('FIND_IN_SET(?, product_category)', $proc)->get()->toArray(), 'id');
            $records = $records->whereIn('lead_orders.crm_customer_id',$prop);

        }
        if($verified){

            if($verified == "1"){
                $records = $records->where('crm_customers.verified',$verified);
            }else{
                $records = $records->where('crm_customers.verified','0');
            }
        }

        if($pipeline){
            $records = $records->where('lead_orders.pipelines_id',$pipeline);
        }
        if($pipeline_stage){
            $records = $records->whereIn('lead_orders.pipelines_stage',$pipeline_stage);
        }
        if($suser){
            $records = $records->whereIn('lead_orders.user_id',$suser);
        }

        if($startdate1 && $enddate1){
            $start_date1 = date('Y-m-d', strtotime($startdate1));
            $end_date1 = date('Y-m-d', strtotime($enddate1));
            $dates1 = array($start_date1, date('Y-m-d', strtotime($end_date1.' +1 day')));
            $records->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]]);

        }

        $count_filter = $records->count();

        return Datatables::of($records)
            ->addColumn('action', function($records) use($start, $length){
                $a="customer-data";
                $html = '<button value="'.$records->id.'" class="btn myred waves-light piplinedelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="lead_order-edit?id='.$records->id.'&cid='.$records->cid.'"><i class="material-icons">edit</i></a><a href="customer-profile?cid='.$records->cid.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';

                return $html;
            })
            ->editColumn('name', function($records){
                $html = '';
                if($records->verified){
                    if(!empty($records->sec_names)){
                        $html = $records->name.$records->sec_names.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                    }
                    $html = $records->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    if(!empty($records->sec_names)) {
                        $html = $records->name .','. $records->sec_names;
                    }else{
                        $html = $records->name;
                    }
                }
                return $html;
            })->editColumn('sec_names', function($records){
                $html = $records->sec_names;
                return $html;
            })->addColumn('category', function($records){

                return $records->type;
            })->addColumn('state_name', function($records){

                return $records->state_name;
            })->addColumn('district_name', function($records){

                return $records->district_name;
            })->editColumn('pipelines_id', function($records){
                $pepiline = DB::table('sales_pipelines')->where('id',$records->pipelines_id)->get();
                foreach ($pepiline as $pipeline){
                    return $pipeline->name;
                }
                return 'NA';
            })->editColumn('pipelines_stage', function($records){
                return $records->pipelines_stage;
            })->editColumn('select_date', function($records){

                return $records->select_date;
            })->editColumn('edited_by', function($records){

                return $records->edited_by;
            })->addColumn('created_date', function($records){
                return $records->created_date;
            })
            ->with([
                "recordsTotal" => $count_total,
                "recordsFiltered" => $count_filter
            ])
            ->rawColumns(['name','action'])
            ->make(true);
    }
    public function getTransportorData(Request $request){

        $records = DB::table('transportor');

        $tname = $request->tname?$request->tname:false;
        $cname = $request->cname?$request->cname:false;
        $states = $request->states?$request->states:false;
        $ststates = $request->ststates?$request->ststates:false;
        $states2 = $request->states2?$request->states2:false;
        $district2 = $request->district2?$request->district2:false;
        $district = $request->district2?$request->district:false;
        $date_start1 = $request->date_start1?$request->date_start1:false;
        $date_end1 = $request->date_end1?$request->date_end1:false;

        if($tname){
            $records = $records->where('name', $tname);
        }

        if($cname){
            $records = $records->where('company_name', $cname);
        }
        if($states){
            $records = $records->where('state', $states);
        }
        if($ststates){
            $records = $records->whereIn('ststate', $ststates);
        }
        if($states2){
            $records = $records->whereIn('edstate', $states2);
        }
        if($district){
            $records = $records->whereIn('stdistrict', $district);
        }
        if($district2){
            $records = $records->whereIn('eddistrict', $district2);
        }
        if($date_start1 && $date_end1){
            $start_date1 = date('Y-m-d', strtotime($startdate1));
            $end_date1 = date('Y-m-d', strtotime($enddate1));
            $dates1 = array($start_date1, date('Y-m-d', strtotime($end_date1.' +1 day')));
            $records->whereBetween(DB::raw('date(created_at)'), [$dates1[0], $dates1[1]]);

        }

        return Datatables::of($records)
            ->addColumn('action', function($row){
                $a="customer-data";
                $html = '<button value="'.$row->id.'" class="btn myred waves-light" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="transportor_order-edit?cid='.$row->id.'"><i class="material-icons">edit</i></a></a>';

                return $html;
            })
            ->editColumn('name', function($row){
                $html = '';
                if($row->verified){
                    if(!empty($row->sec_names)) {
                        $html = $row->name .','.implode(unserialize($row->sec_names)).' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                    }
                    $html = $row->name .' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    if(!empty($row->sec_names)) {
                        $html = $row->name.','.implode(unserialize($row->sec_names));
                    }else{
                        $html = $row->name;
                    }
                }
                return $html;
            })->editColumn('company_name', function($row){
                return $row->company_name;
            })->addColumn('company_address', function($row){
                return $row->company_address;
            })->addColumn('state', function($row){
                if ($row->state){
                    $state = array_pluck(State::where('id',$row->state)->where('country_id','101')->get(),'name');
                    return $state;
                }
                return 'NA';
            })->addColumn('ststate', function($row){
                if ($row->ststate){
                    $states = unserialize($row->ststate);
                    $state = array_pluck(State::whereIn('id',$this->array_flatten($states))->where('country_id','101')->get(),'name');
                    return $state;
                }
                return 'NA';
            })->editColumn('stdistrict', function($row){
               if ($row->stdistrict){
                   $stdistrict = unserialize($row->stdistrict);
                    $district = array_pluck(District::whereIn('id',$this->array_flatten($stdistrict))->get(),'name');
                   return $district;
                }
                return 'NA';
            })->editColumn('edstate', function($row){
                if ($row->edstate){
                    $edstate = unserialize($row->edstate);
                    $state = array_pluck(State::whereIn('id',$this->array_flatten($edstate))->where('country_id','101')->get(),'name');
                    return $state;
                }
                return 'NA';
            })->editColumn('eddistrict', function($row){
                if ($row->eddistrict){
                    $eddistrict =unserialize($row->eddistrict);
                    $district = array_pluck(District::whereIn('id',$this->array_flatten($eddistrict))->get(),'name');
                    return $district;
                }
                return 'NA';
            })->editColumn('created_at', function($row){
                return $row->created_at;
            })
            ->rawColumns(['name','action'])
            ->make(true);
    }

    public function getNotinterestedData(Request $request){

        $records = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.notinterested_date','crm_customers.notinterested_reason','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = crm_customers.id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = crm_customers.id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = crm_customers.id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'))
            ->where('crm_customers.class','=',6);

        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $class = (isset($_GET["cust_class"])) ? ($_GET["cust_class"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;


        if($category){
            $records = $records->whereIn('customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($class){
            if($class !== 'all'){
                $records = $records->where('class', $class);
            }
        }
        if($type){
            $records = $records->where('crm_customers.type',$type);
        }


        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="customer-data";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="notinterested-report?edit='.$records->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->make(true);
    }

    public function getConvertedCustomers(Request $request){

        $records = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select('crm_customers.id', 'crm_customers.name','crm_customers.converted_by','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'), 'crm_customers.conversion_date', 'crm_customers.verified')
            ->where('crm_customers.class', 4);


        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $converted_by = (isset($_GET["converted_by"])) ? ($_GET["converted_by"]) : false;
        $dates = (isset($_GET["dates"])) ? ($_GET["dates"]) : false;

        if($category){
            $records = $records->whereIn('customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($converted_by){
            if($converted_by !== 'all')
                $records = $records->whereRaw('FIND_IN_SET(?, crm_customers.converted_by)', $converted_by);
        }

        if($dates){
            if($dates == '$$'){
                $dates = date('Y-m-d 00:00:00',strtotime('1th January 2019')).'$$'.date('Y-m-d 23:59:59');
            }
            $date_range = explode('$$', $dates);
            $records->whereBetween('crm_customers.conversion_date', [$date_range[0].' 00:00:00', $date_range[1].' 23:59:59']);
        }else{
            $dates = date('Y-m-d 00:00:00').'$$'.date('Y-m-d 23:59:59');
            $date_range = explode('$$', $dates);
            $records->whereBetween('crm_customers.conversion_date', [$date_range[0], $date_range[1]]);
        }

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="converted-customers";
                $html = '<a href="customer-profile?cid='.$records->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('converted_by', function($records){
                $users = explode(',', $records->converted_by);
                $u = array_pluck(DB::table('user')->whereIn('u_id', $users)->get(), 'u_name');

                return implode(',', $u);
            })
            ->editColumn('name', function($records){
                $html = '';
                if($records->verified){
                    $html = $records->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    $html = $records->name;
                }
                return $html;
            })
            ->editColumn('conversion_date', function($records){
                return date("d-m-Y", strtotime($records->conversion_date));
            })
            ->rawColumns(['name', 'action'])
            ->make(true);
    }
    public function getConversionChartData(Request $request){

        $records = DB::table('crm_customers')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select(DB::raw('COUNT(*) as total'), 'crm_customers.id', 'crm_customers.name','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name'), 'crm_customers.conversion_date')->where('crm_customers.class', 4);


        $category = (isset($_POST["category"])) ? $_POST["category"] : false;
        $state = (isset($_POST["state"])) ? ($_POST["state"]) : false;
        $district = (isset($_POST["district"])) ? ($_POST["district"]) : false;
        $acc_manager = (isset($_POST["acc_manager"])) ? ($_POST["acc_manager"]) : false;
        $dates = (isset($_POST["dates"])) ? ($_POST["dates"]) : false;

        if($category){
            $records = $records->whereIn('customer_category', explode(',', $category));
        }
        if($state){
            $records = $records->whereIn('state', explode(',', $state));
        }
        if($district){
            $records = $records->whereIn('district', explode(',', $district));
        }
        if($acc_manager){
            if($acc_manager !== 'all')
                $records = $records->whereRaw('FIND_IN_SET(?, crm_customers.assign_users)', $acc_manager);
        }
        if($dates){
            if($dates == '$$'){
                $dates = date('Y-m-d 00:00:00').'$$'.date('Y-m-d 23:59:59');
            }
            $date_range = explode('$$', $dates);
            $records->whereBetween('conversion_date', [$date_range[0].' 00:00:00', $date_range[1].' 23:59:59']);
        }else{
            $dates = date('Y-m-d 00:00:00').'$$'.date('Y-m-d 23:59:59');
            $date_range = explode('$$', $dates);
            $records->whereBetween('conversion_date', [$date_range[0], $date_range[1]]);
        }
        $records->groupBy(DB::raw('Date(conversion_date)'))->orderBy('conversion_date', 'ASC');
        $data['cdata'] = array_pluck($records->get(), 'total');
        $date_data = $records->get();
        $data['labels'] = array();
        foreach($date_data as $dd){
            $data['labels'][] = date('d-m-Y', strtotime($dd->conversion_date));
        }
        //$data['labels'] = array_pluck($records->get(), 'conversion_date');

        return json_encode($data);

    }
    public function getBPRData(Request $request){

        $records = DB::table('district')->select(['id','state_id','name as district_name']);
        $brands = DB::table('brand');
        $category = (isset($_POST["category"])) ? $_POST["category"] : false;
        $state = (isset($_POST["states"])) ? ($_POST["states"]) : false;
        $district = (isset($_POST["district"])) ? ($_POST["district"]) : false;
        $brand = (isset($_POST["brand"])) ? ($_POST["brand"]) : false;
        if($category){
            $cb = DB::table('category')->where('id', $category)->first();

//           $cb = array_pluck(DB::table('category')
//                ->where('id', $category)->get(), 'brands');
            $brands =  $brands->whereIn('id', explode(',', $cb->brands));
//            dd($brands);
        }
        if($state){
//            dd($state);
            $records = $records->whereIn('state_id', $state);
        }
        if($district){
//            dd($state);
            $records = $records->whereIn('id', $district);
        }
        if($brand){
//            dd($state);
            $brands = $brands->whereIn('id', $brand);
        }
        $brands = $brands->get();
//        dd($brands);
        //$records = $records->orderBy('crm_customers.id', 'asc');
        $dt =  Datatables::of($records);
        foreach($brands as $brand){
            $col_name = str_replace(' ', '', strtolower($brand->name));
            $dt->addColumn($col_name, function($records) use ($brand){
                $cust_ids = array_pluck(DB::table('crm_products')
                    ->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
                    ->where('district', $records->id)->where('state', $records->state_id)->get(), 'crm_customer_id');
                return DB::table('crm_products')->whereIn('crm_customer_id', $cust_ids)->whereRaw('FIND_IN_SET(?, brands)', $brand->id)->count();
            });
        }
        return $dt->make(true);
    }
    public function getBPRDataSW(Request $request){

        $records = DB::table('states')->select(['id', 'name as state_name'])->where('country_id', '101');
        $brands = DB::table('brand');
        $category = (isset($_POST["category"])) ? $_POST["category"] : false;
        $state = (isset($_POST["states"])) ? $_POST["states"] : false;
        $brand = (isset($_POST["brand"])) ? ($_POST["brand"]) : false;
        if($category){
            $cb = DB::table('category')->where('id', $category)->first();
            $brands = $brands->whereIn('id', explode(',', $cb->brands));
        }
        if($state){
            $records = $records->whereIn('id', $state);
        }
        if($brand){
            $brands = $brands->whereIn('id', $brand);
        }
        $brands = $brands->get();
        //$records = $records->orderBy('crm_customers.id', 'asc');
        $dt =  Datatables::of($records);
        foreach($brands as $brand){
            $col_name = str_replace(' ', '', strtolower($brand->name));
            $dt->addColumn($col_name, function($records) use ($brand){
                $cust_ids = array_pluck(DB::table('crm_products')
                    ->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
                    ->where('state', $records->id)->get(), 'crm_customer_id');
                return DB::table('crm_products')->whereIn('crm_customer_id', $cust_ids)->whereRaw('FIND_IN_SET(?, brands)', $brand->id)->count();
            });
        }
        return $dt->make(true);
    }
    public function getSCRData(Request $request){

        $records = DB::table('district')->select(['id','state_id', 'name as district_name']);
        $suppliers = DB::table('crm_supplier');
        $category = (isset($_POST["category"])) ? $_POST["category"] : false;
        $state = (isset($_POST["states"])) ? ($_POST["states"]) : false;
        $district = (isset($_POST["district"])) ? ($_POST["district"]) : false;
        $supplier = (isset($_POST["supplier"])) ? ($_POST["supplier"]) : false;

        if($category){
            $allcats = array($category);
            $recat = 0;
            $recatx = true;
            $curcat[] = $category;
            do{
                $recats = array_pluck(DB::table('category')->whereIn('parent', $curcat)->get(), 'id');
                $curcat = $recats;
                $recat = count($recats);
                array_push($allcats, implode(',', $recats));
                if($recat == 0){
                    $recatx = false;
                }
            }while($recatx);
//            dd(explode(',',implode(',', $allcats)));
            $suppliers = $suppliers->whereRaw('FIND_IN_SET(?, product_category)', $allcats);

        }
        if($state && $state != 'all'){
            $records = $records->whereIn('state_id', $state);
            $dis = array_pluck(DB::table('states')->where('id', $state)->get(), 'id');
            $pdata = array_pluck(DB::table('crm_products')->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
                ->whereIn('state', $dis)->get(), 'supplier_id');
            $sids = explode(',', implode(',', $pdata));
            $suppliers = $suppliers->whereIn('id', $sids);
        }
        if($district && $district != 'all'){
            $records = $records->where('id', $district);
            $dis = array_pluck(DB::table('district')->where('id', $district)->get(), 'id');
            $pdata = array_pluck(DB::table('crm_products')->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
                ->whereIn('district', $dis)->get(), 'supplier_id');
            $sids = explode(',', implode(',', $pdata));
            $suppliers = $suppliers->whereIn('id', $sids);
        }
        if($supplier){
            $suppliers = $suppliers->whereIn('id', $supplier);
        }
        $suppliers = $suppliers->get();
        //$records = $records->orderBy('crm_customers.id', 'asc');
        $dt =  Datatables::of($records);
        foreach($suppliers as $supplier){
            $col_name = $supplier->id.str_replace('.', '', str_replace(' ', '', strtolower($supplier->name)));
            $dt->addColumn($col_name, function($records) use ($supplier){
                $cust_ids = array_pluck(DB::table('crm_products')
                    ->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
                    ->where('district', $records->id)->get(), 'crm_customer_id');
                return DB::table('crm_products')->whereIn('crm_customer_id', $cust_ids)->whereRaw('FIND_IN_SET(?, supplier_id)', $supplier->id)->count();
            });
        }
        return $dt->make(true);
    }
    public function getSCRDataSW(Request $request){

        $records = DB::table('states')->where('country_id', '101')->select(['id', 'name as state_name']);
        $suppliers = DB::table('crm_supplier');
        $category = (isset($_POST["category"])) ? $_POST["category"] : false;
        $state = (isset($_POST["states"])) ? ($_POST["states"]) : false;
        $supplier = (isset($_POST["supplier"])) ? ($_POST["supplier"]) : false;

        if($category && $category !== ''){
            $allcats = array($category);
            $recat = 0;
            $recatx = true;
            $curcat[] = $category;
            do{
                $recats = array_pluck(DB::table('category')->whereIn('parent', $curcat)->get(), 'id');
                $curcat = $recats;
                $recat = count($recats);
                array_push($allcats, implode(',', $recats));
                if($recat == 0){
                    $recatx = false;
                }

            }while($recatx);

            $suppliers = $suppliers->whereRaw('FIND_IN_SET(?, product_category)', $allcats);
        }

        if($state && $state != 'all'){
            $records = $records->whereIn('id', $state);
            $dis = array_pluck(DB::table('states')->where('id', $state)->get(), 'id');
            $pdata = array_pluck(DB::table('crm_products')->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
                ->whereIn('state', $dis)->get(), 'supplier_id');
            $sids = explode(',', implode(',', $pdata));
            $suppliers = $suppliers->whereIn('id', $sids);
        }

        if($supplier){
            $suppliers = $suppliers->whereIn('id', $supplier);
        }
        $suppliers = $suppliers->get();
//        $states = array_pluck(DB::table('states')->where('country_id', '101')->get(), 'id');
//        $pdata = array_pluck(DB::table('crm_products')->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
//            ->whereIn('state', $states)->get(), 'supplier_id');
//        $sids = explode(',', implode(',', $pdata));
//        $suppliers = $suppliers->whereIn('id', $sids)->get();
//        $records = $records->orderBy('crm_customers.id', 'asc');
        $dt =  Datatables::of($records);
        foreach($suppliers as $supplier){
            $col_name = $supplier->id.str_replace('.', '', str_replace(' ', '', strtolower($supplier->name)));
            $dt->addColumn($col_name, function($records) use ($supplier){
                $cust_ids = array_pluck(DB::table('crm_products')
                    ->join('crm_customers', 'crm_customers.id', '=', 'crm_products.crm_customer_id')
                    ->where('state', $records->id)->get(), 'crm_customer_id');
                return DB::table('crm_products')->whereIn('crm_customer_id', $cust_ids)->whereRaw('FIND_IN_SET(?, supplier_id)', $supplier->id)->count();
            });
        }
        return $dt->make(true);
    }
    public function getVisitsChartData(Request $request){

        $records = DB::table('crm_visits')
            ->join('crm_customers', 'crm_customers.id', '=', 'crm_visits.crm_customer_id')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select(DB::raw('COUNT(*) as total'), 'crm_customers.id', 'crm_customers.name','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), 'crm_visits.user_id', 'crm_visits.select_date');


        $category = (isset($_POST["category"])) ? $_POST["category"] : false;
        $state = (isset($_POST["state"])) ? ($_POST["state"]) : false;
        $district = (isset($_POST["district"])) ? ($_POST["district"]) : false;
        $acc_manager = (isset($_POST["acc_manager"])) ? ($_POST["acc_manager"]) : false;
        $dates = (isset($_POST["dates"])) ? ($_POST["dates"]) : false;

        if($category){
            $records = $records->whereIn('customer_category', explode(',', $category));
        }
        if($state){
            $records = $records->whereIn('state', explode(',', $state));
        }
        if($district){
            $records = $records->whereIn('district', explode(',', $district));
        }
        if($acc_manager){
            if($acc_manager !== 'all')
                $records = $records->whereRaw('FIND_IN_SET(?, crm_visits.user_id)', $acc_manager);
        }
        if($dates){
            if($dates == '$$'){
                $dates = date('Y-m-d').'$$'.date('Y-m-d');
            }
            $date_range = explode('$$', $dates);
            $records->whereBetween('select_date', [$date_range[0], $date_range[1]]);
        }else{
            $dates = date('Y-m-d').'$$'.date('Y-m-d');
            $date_range = explode('$$', $dates);
            $records->whereBetween('select_date', [$date_range[0], $date_range[1]]);
        }
        $records->groupBy('select_date')->orderBy('select_date', 'ASC');
        $data['cdata'] = array_pluck($records->get(), 'total');
//        $data['labels'] = array_pluck($records->get(), 'select_date');

        $date_data = $records->get();
        $data['labels'] = array();
        foreach($date_data as $dd){
            $data['labels'][] = date('d-m-Y', strtotime($dd->select_date));
        }
        return json_encode($data);

    }

    public function getVisitsMapData(Request $request){

        $records = DB::table('crm_customers')
            ->join('crm_visits', 'crm_customers.id', '=', 'crm_visits.crm_customer_id')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->join('district', 'crm_customers.district', '=', 'district.id')
            ->select(DB::raw('COUNT(*) as total'),'crm_customers.lat','crm_customers.lng','crm_customers.id','district.lat as latitude','district.lng as longitude','crm_customers.name','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), DB::raw('(select locality from locality where locality.id = crm_customers.locality) as locality_name') ,DB::raw('(select count(*) from crm_tickets where crm_tickets.crm_customer_id = crm_customers.id) as t_tickets'), DB::raw('(select count(*) from crm_visits where crm_visits.crm_customer_id = crm_customers.id) as t_visits'), DB::raw('(select count(*) from crm_customer_projects where crm_customer_projects.crm_customer_id = crm_customers.id) as t_projects'), DB::raw('(select count(*) from crm_promotional where crm_promotional.crm_customer_id = crm_customers.id) as t_promotional'), DB::raw('(select count(*) from crm_call where crm_call.crm_customer_id = crm_customers.id) as t_call'), DB::raw('(select type from customer_type where customer_type.id = crm_customers.type) as c_type'));



        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $class = (isset($_GET["cust_class"])) ? ($_GET["cust_class"]) : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;
        $ptype = (isset($_GET["ptype"])) ? ($_GET["ptype"]) : false;
        $pstype = (isset($_GET["pstype"])) ? ($_GET["pstype"]) : false;

        if($category){
            $records = $records->whereIn('customer_category', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($class){
            if($class !== 'all'){
                $records = $records->where('class', $class);
            }
        }
        if($type){
            $records = $records->where('crm_customers.type',$type);
        }
        if($ptype){
            $c = array_pluck(DB::table('crm_customer_projects')->where('project_type',$ptype)->get()->toArray(), 'crm_customer_id');
            $records = $records
                ->whereIn('crm_customers.id',$c);

        }
        if($pstype){
            $cd = array_pluck(DB::table('crm_customer_projects')->where('project_type',$_GET["ptype"])->whereIn('project_subtype',$pstype)->get()->toArray(), 'crm_customer_id');
            $records = $records
                ->whereIn('crm_customers.id',$cd);
        }

//        $data['cdata'] = array_pluck($records->get(), 'total');
        $date_data = $records->get();
        $data['cdata'] = array();
        foreach ($date_data as $lat1)
        {
            $latitude='';
            $longitude='';
            if($lat1->lat!='' AND $lat1->lng!='')
            {

                $latitude = $lat1->lat;
                $longitude = $lat1->lng;
                $data['cdata'][] = "{lat : ".$latitude.", lng : ".$longitude."}";

            }

//    else
            elseif($lat1->lat=='' && $lat1->lng=='' && $lat1->latitude!='' && $lat1->longitude!='')
            {
                $latitude = $lat1->latitude;
                $longitude = $lat1->longitude;
                $data['cdata'][] = "{lat : ".$latitude.", lng : ".$longitude."}";
            }

        }



//        $data['labels'] = array_pluck($records->get());
        return json_encode($data);

    }

    //om code

    public function getCallsChartData(Request $request){

        $records = DB::table('crm_call')
            ->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category')
            ->select(DB::raw('COUNT(*) as total'), 'crm_customers.id', 'crm_customers.name','crm_customers.contact_no', 'customer_category.type', DB::raw('(select name from states where states.id = crm_customers.state) as state_name') , DB::raw('(select name from district where district.id = crm_customers.district) as district_name'), 'crm_call.select_date');


        $category = (isset($_POST["category"])) ? $_POST["category"] : false;
        $state = (isset($_POST["state"])) ? ($_POST["state"]) : false;
        $district = (isset($_POST["district"])) ? ($_POST["district"]) : false;
        $acc_manager = (isset($_POST["acc_manager"])) ? ($_POST["acc_manager"]) : false;
        $dates = (isset($_POST["dates"])) ? ($_POST["dates"]) : false;

        if($category){
            $records = $records->whereIn('customer_category', explode(',', $category));
        }
        if($state){
            $records = $records->whereIn('state', explode(',', $state));
        }
        if($district){
            $records = $records->whereIn('district', explode(',', $district));
        }
        if($acc_manager){
            if($acc_manager !== 'all')
                $records = $records->whereRaw('FIND_IN_SET(?, crm_call.user_id)', $acc_manager);
        }
        if($dates){
            if($dates == '$$'){
                $dates = date('Y-m-d').'$$'.date('Y-m-d');
            }
            $date_range = explode('$$', $dates);
            $records->whereBetween('select_date', [$date_range[0], $date_range[1]]);
        }else{
            $dates = date('Y-m-d').'$$'.date('Y-m-d');
            $date_range = explode('$$', $dates);
            $records->whereBetween('select_date', [$date_range[0], $date_range[1]]);
        }
        $records->groupBy('select_date')->orderBy('select_date', 'ASC');
        $data['cdata'] = array_pluck($records->get(), 'total');
//        $data['labels'] = array_pluck($records->get(), 'select_date');

        $date_data = $records->get();
        $data['labels'] = array();
        foreach($date_data as $dd){
            $data['labels'][] = date('d-m-Y', strtotime($dd->select_date));
        }
        return json_encode($data);

    }
    public function CustomerProfile( Request $request){

        $notices = '';
        $customer = false;
        $cid = '';
        $tickets = array();
        $visits = array();
        $notes = array();
        $projects = array();
        $promotionals = array();
        $products = array();
        $user = DB::table('user')->where('secure', session('crm'))->first();
        $sales_pipelines = DB::table('sales_pipelines')->get();
        $designations = DB::table('customer_designation')->get();
        $no_order_reasons = DB::table('no_order')->get();
        $selected_option = $user->u_name;
        if(isset($_GET['cid'])){
            $cid = $_GET['cid'];
            $customer = DB::table('crm_customers')
                ->join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
                ->where('crm_customers.id', '=', $cid)
                ->select('crm_customers.*','crm_customers.type as type_id','customer_category.type')
                ->first();

            $visits = DB::table('crm_visits')
                ->where('crm_customer_id', '=', $cid)
                ->get();
            $promotionals = DB::table('crm_promotional')
                ->where('crm_customer_id', '=', $cid)
                ->get();
            $products = DB::table('crm_products')
                ->where('crm_customer_id', '=', $cid)
                ->get();
            $calls = DB::table('crm_call')
                ->where('crm_customer_id', '=', $cid)
                ->get();
            $messages = DB::select("SELECT * FROM `crm_message_save` WHERE mobile IN (".$customer->contact_no.")");
        }



        if(isset($_POST['add_ticket'])){
            $file = '';
            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/tickets/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            //dd($_POST);
            $cid = $_POST['crm_cust_id'];
            /*$email = $_POST['email'];
            $subject = $_POST['subject'];
            $phone = $_POST['phone'];
            $cc = $_POST['cc'];*/
            $data['crm_customer_id'] = $cid;
            $data['type'] = $_POST['type'];
            $data['create_date'] = $_POST['ticket_create_date'];
            $data['due_date'] = $_POST['ticket_due_date'];
            $data['status'] = $_POST['status'];
            $data['priority'] = $_POST['priority'];
            $data['assign_to'] = $_POST['Assign_to'];
            $data['description'] = $_POST['description'];
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;
            $data['acc_manager'] = $_POST['acc_manager'];

            /*$tag = $_POST['tag'];*/
            $id = DB::table('crm_tickets')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'tickets';
            $timeline->section_id = $id;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['create_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Ticket Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_ticket'])){
            $cid = $_POST['crm_customer_id'];
            $tid = $_POST['ticket_id'];
            /*$email = $_POST['email'];
            $subject = $_POST['subject'];
            $phone = $_POST['phone'];
            $cc = $_POST['cc'];*/
            $type = $_POST['type'];
            $status = $_POST['status'];
            $priority = $_POST['priority'];
            $assign_to = $_POST['assign_to'];
            $description = $_POST['description'];
            $acc_manager = $_POST['acc_manager'];
            $due_date = date('Y-m-d', strtotime($_POST['ticket_due_date']));

            /* $tag = $_POST['tag'];*/
            DB::update("UPDATE `crm_tickets` SET `type`='$type',`status`='$status',`priority`='$priority',`assign_to`='$assign_to',`description`='$description',`acc_manager`='$acc_manager',  `due_date`='$due_date' WHERE id = '$tid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'tickets_edit';
            $timeline->section_id = $tid;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Ticket Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if(isset($_POST['add_visit'])){
            //dd($_POST);
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/visits/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['name'] = $_POST['name'];
            $data['phone_no'] = $_POST['phone_no'];
            $data['address'] = $_POST['address'];
            $data['select_date'] = $_POST['date'];
            $data['description'] = $_POST['description'];
            $data['user_id'] = implode(',',$_POST['user']);
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;

            $user_name = $user->u_id;

            if($customer){
                $data['customer_class'] = $customer->class;
            }
            $id = DB::table('crm_visits')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'visits';
            $timeline->section_id = $id;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Visit Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_visit'])){
            $notices = '';
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/visits/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            $cid = $_POST['crm_customer_id'];
            $vid = $_POST['visit_id'];
            $name = $_POST['name'];
            $phone_no = $_POST['phone_no'];
            $address = $_POST['address'];
            $date = $_POST['date'];
            $description = $_POST['description'];
            $user_id = implode(',',$_POST['user']);
            $visit = DB::table('crm_visits')->where('id', $vid)->first();

            if($file != ''){

//                 $file.= $visit->image.','.$file;
                $file = $file.','.$visit->image;
//                 dd($file);

            }


//             dd($file);
            DB::update("UPDATE `crm_visits` SET `name`='$name',`phone_no`='$phone_no',`address`='$address',`select_date`='$date',`description`='$description',`user_id` = '$user_id',`image`='$file' WHERE id = '$vid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'visits_edit';
            $timeline->section_id = $vid;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Visit Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if(isset($_POST['add_notes'])){

            // dd($_POST);
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/notes/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['select_date'] = $_POST['select_date'];
            $data['description'] = $_POST['description'];
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;

            $id = DB::table('crm_notes')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'notes';
            $timeline->section_id = $id;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Notes Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_note'])){
            $cid = $_POST['crm_customer_id'];
            $nid = $_POST['note_id'];
            $date = $_POST['select_date'];
            $description = $_POST['description'];
            DB::update("UPDATE `crm_notes` SET `select_date`='$date',`description`='$description' WHERE id = '$nid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'notes_edit';
            $timeline->section_id = $nid;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Note Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if(isset($_POST['add_project'])){

            //dd($_POST);
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/projects/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['project_name'] = $_POST['pname'];
            $data['project_type'] = $_POST['ptype'];
            $data['project_subtype'] = implode(',',$_POST['pstype']);
            $data['sqft'] = $_POST['sqft'];
            $data['project_status'] = $_POST['pstatus'];
            $data['state'] = $_POST['state'];
            $data['district'] = $_POST['district'];
            $data['locality'] = $_POST['locality'];

//             $data['block'] = $_POST['block'];

            $data['block']  = (isset($_POST['block'])) ? $_POST['block'] : '';

            $data['project_address'] = $_POST['paddress'];
            $data['person_name'] = $_POST['cpname'];
            $data['person_phone'] = $_POST['cpnumber'];
            $data['person_designation'] = $_POST['cpdesignation'];
            $data['description'] = $_POST['description'];
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;
//             dd($data);


            $id = DB::table('crm_customer_projects')->insertGetId($data);


            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'projects';
            $timeline->section_id = $id;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Projects Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_project'])){

            //dd($_POST);
            $pid = $_POST['project_id'];
            $cid = $_POST['crm_customer_id'];
            $project_name = $_POST['pname'];
            $project_type = $_POST['ptype'];
            $project_subtype = implode(',',$_POST['pstype']);
            $sqft = $_POST['sqft'];
            $project_status = $_POST['pstatus'];
            $state = $_POST['state'];
            $district = $_POST['district'];
            $locality = $_POST['locality'];
            $block = $_POST['block'];
            $project_address = $_POST['paddress'];
            $person_name = $_POST['cpname'];
            $person_phone = $_POST['cpnumber'];
            $person_designation = $_POST['cpdesignation'];
            $description = $_POST['description'];


            DB::update("UPDATE `crm_customer_projects` SET `project_name`='$project_name',`project_type`='$project_type',`project_subtype`='$project_subtype',`block`='$block',`sqft`='$sqft',`project_status`='$project_status',`project_address`='$project_address',`person_name`='$person_name',`person_phone`='$person_phone',`person_designation`='$person_designation',`description`='$description', `state`='$state', `district`='$district', `locality`='$locality' WHERE id = '$pid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'projects_edit';
            $timeline->section_id = $pid;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Project Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if(isset($_POST['add_promotional'])){

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['select_date'] = $_POST['select_date'];
            $data['description'] = $_POST['description'];
            $data['edited_by'] = $user->u_name;

            $id = DB::table('crm_promotional')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'promotional';
            $timeline->section_id = $id;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Promotional Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_promotional'])){
            //dd($_POST);
            $pid = $_POST['promotional_id'];
            $cid = $_POST['crm_customer_id'];
            $date = $_POST['select_date'];
            $description = $_POST['description'];

            DB::update("UPDATE `crm_promotional` SET `select_date`='$date',`description`='$description' WHERE id = '$pid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'promotional_edit';
            $timeline->section_id = $pid;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Promotion Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        if(isset($_POST['add_product'])){

            $file = '';
            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/products/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['product_id'] = implode(',',$_POST['category']);
            if(isset($_POST['price_group'])== ""){
                $data['price_groups'] = "Group Not Selected";
            }else{
                $data['price_groups'] = implode(',',$_POST['price_group']);
            }
            $data['description'] = $_POST['description'];
            $data['brands'] = isset($_POST['brand']) ? implode(',',$_POST['brand']) : '';
            $data['supplier_id'] = isset($_POST['supplier'])? implode(',',$_POST['supplier']) : '';
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;

            $id = DB::table('crm_products')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'products';
            $timeline->section_id = $id;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Product Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_product'])){

            $pid = $_POST['product_id'];
            $cid = $_POST['crm_customer_id'];

            $products = implode(',',$_POST['category']);
            if(isset($_POST['price_group'])== ""){
                $price_groups = "Group Not Selected";
            }else{
                $price_groups = implode(',',$_POST['price_group']);
            }
            $description = $_POST['description'];
            $brand = isset($_POST['brand']) ? implode(',',$_POST['brand']) : '';
            $supplier = isset($_POST['supplier'])? implode(',',$_POST['supplier']) : '';

            DB::update("UPDATE `crm_products` SET `product_id`='$products', `price_groups`='$price_groups',`brands` = '$brand' ,`supplier_id` = '$supplier',`description`='$description' WHERE id = '$pid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'products_edit';
            $timeline->section_id = $pid;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s');
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Product Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['convert_customer'])){

            $cid = $_POST['cid'];
            $user = implode(',',$_POST['user']);
            $date = $_POST['conversion_date'];
            $class = $_POST['convert_customer'];

            DB::update("UPDATE `crm_customers` SET `converted_by`='$user',`conversion_date`='$date',`class`='$class' WHERE id = '$cid'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Converted Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_call'])){
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['customer_number'] = $_POST['customer_no'];
            $data['landline_no'] = $_POST['landline_no'];
            $data['conperson_id'] = $_POST['conperson'];
            $data['contact_person'] = $_POST['conperson'];
            $data['contact_designation'] = $_POST['contact-designation'];
            $data['select_date'] = $_POST['select_date'];
            $data['message'] = $_POST['message'];
            $data['user_id'] = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $data['call_type'] = $request->ctype;

            if(!empty($_POST['no_order_reason'])){
                $data['no_order_reason'] = implode(',',$_POST['no_order_reason'])?implode(',',$_POST['no_order_reason']):'';
            }

            if((!empty($_POST['no_order_reason'])) && (!empty($_POST['name']) || !empty($_POST['stage'])  || !empty($_POST['product_category']))){
                $notices .= '<div class="card-alert card red">
                            <div class="card-content white-text">
                              <p>Select No Order Reason Or Pipelines Section Only</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>';
            }
            else if(!empty($_POST['name']) && !empty($_POST['stage'])  && !empty($_POST['product_category'])){
                $lo = new LeadOrder();
                $lo->crm_customer_id = $cid;
                $lo->edited_by = $user->u_name;
                $lo->user_id = implode(',',$_POST['user']);
                $lo->pipelines_id = $_POST['name'];
                $lo->pipelines_stage = $_POST['stage'];
                $lo->select_date = $_POST['select_date'];
                if (isset($_POST['product_category'])) {
                    $lo->product_category = implode(',', $_POST['product_category']) ? implode(',', $_POST['product_category']) : '';
                }
                if (isset($_POST['lead_no_order_reason'])) {
                    $lo->no_order_reason = implode(',', $_POST['lead_no_order_reason']) ? implode(',', $_POST['lead_no_order_reason']) : '';
                }

                $lo->save();
                $leadid =  $lo->id;
                $data['lead_id'] = $leadid;
                $id = DB::table('crm_call')->insertGetId($data);

                $timeline = new Timeline();
                $timeline->customer_id = $cid;
                $timeline->section = 'calls';
                $timeline->section_id = $id;
                $luser = DB::table('user')->where('secure', session('crm'))->first();
                $timeline->created_by = $luser->u_id;
                $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
                $timeline->save();

                $notices .= '<div class="card-alert card green">
                    <div class="card-content white-text">
                      <p>New Call Added Successfully.</p>
                    </div>
                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>';
            }
            else if((!empty($_POST['no_order_reason'])) && (empty($_POST['name']) || empty($_POST['stage'])  || empty($_POST['product_category']))){

                $id = DB::table('crm_call')->insertGetId($data);
                $timeline = new Timeline();
                $timeline->customer_id = $cid;
                $timeline->section = 'calls';
                $timeline->section_id = $id;
                $luser = DB::table('user')->where('secure', session('crm'))->first();
                $timeline->created_by = $luser->u_id;
                $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
                $timeline->save();

                $notices .= '<div class="card-alert card green">
                    <div class="card-content white-text">
                      <p>New Call Added Successfully.</p>
                    </div>
                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>';
            }
            else{
                $notices .= '<div class="card-alert card red">
                        <div class="card-content white-text">
                          <p>Select Pipeline Sections</p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>';

            }


            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_lead'])){
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['pipelines_id'] = $_POST['name'];
            $data['select_date'] = $_POST['select_date'];
            $data['pipelines_stage'] = $_POST['stage'];
            $data['edited_by'] = $user->u_name;
            $data['user_id'] = $user->u_id;
            if(isset($_POST['product_category'])){
                $data['product_category'] = implode(',',$_POST['product_category'])?implode(',',$_POST['product_category']):'';
            }
            if (isset($_POST['lead_reason'])) {
                $data['no_order_reason'] = implode(',', $_POST['lead_reason']) ? implode(',', $_POST['lead_reason']) : '';
            }
            $id = DB::table('lead_orders')->insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Lead Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_lead_call'])){
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['customer_number'] = $_POST['customer_no'];
            //$data['landline_no'] = $_POST['landline_no3'];
            //$data['conperson_id'] = $_POST['conperson3'];
            $data['select_date'] = $_POST['select_date'];
            $data['message'] = $_POST['message'];
            $data['user_id'] = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $data['call_type'] = $request->ctype;

            if(!empty($_POST['no_order_reason'])){
                $data['no_order_reason'] = implode(',',$_POST['no_order_reason'])?implode(',',$_POST['no_order_reason']):'';
            }

            $id = DB::table('crm_call')->insertGetId($data);
            $leadid = DB::table('lead_orders')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'calls';
            $timeline->section_id = $id;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Call Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }


        if(isset($_POST['add_lead'])){

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['pipelines_id'] = $_POST['name'];
            $data['select_date'] = $_POST['select_date'];
            $data['pipelines_stage'] = $_POST['stage'];
            $data['edited_by'] = $user->u_name;
            $data['user_id'] = $user->u_id;
            if(isset($_POST['product_category'])){
                $data['product_category'] = implode(',',$_POST['product_category'])?implode(',',$_POST['product_category']):'';
            }
            if (isset($_POST['lead_reason'])) {
                $data['no_order_reason'] = implode(',', $_POST['lead_reason']) ? implode(',', $_POST['lead_reason']) : '';
            }
            $id = DB::table('lead_orders')->insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Lead Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_msg'])){
            $cid = $_POST['crm_cust_id'];
            $data['mobile'] = $_POST['customer_no'];
            $data['message'] = $_POST['message'];
            $data['user_id'] = implode(',',$_POST['user']);
            $cus = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $id = DB::table('crm_message_save')->insertGetId($data);
            /*foreach ($cus as $d) {
                $c = DB::select("SELECT * FROM crm_customers WHERE id = '".$d."'");
                $sms = urlencode($_POST['contact_no']);
                $no = urlencode($c[0]->mobile);
                echo $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FAILONERROR, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $output = curl_exec($ch);
                curl_error($ch);
                curl_close($ch);
            }*/

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New message Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_lead_msg'])){
            $cid = $_POST['crm_cust_id'];
            $data['mobile'] = $_POST['lead_customer_no'];
            $data['message'] = $_POST['lead_message'];
            $data['user_id'] = implode(',',$_POST['lead_user']);
            $cus = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $leadids = DB::table('lead_message_logs')->insertGetId($data);
            $id = DB::table('crm_message_save')->insertGetId($data);
            /*foreach ($cus as $d) {
                $c = DB::select("SELECT * FROM crm_customers WHERE id = '".$d."'");
                $sms = urlencode($_POST['contact_no']);
                $no = urlencode($c[0]->mobile);
                echo $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FAILONERROR, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $output = curl_exec($ch);
                curl_error($ch);
                curl_close($ch);
            }*/

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New message Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_call'])){
            $cid = $_POST['crm_customer_id'];
            $caid = $_POST['call_id'];
            $customer_no = $_POST['customer_no'];
            $date = $_POST['select_date'];
            $message = $_POST['message'];
            if(isset($_POST['no_order_reason'])){
                $no_order = implode(',',$_POST['no_order_reason']);
            }
            DB::update("UPDATE `crm_call` SET `customer_number`='$customer_no',`select_date`='$date',`message`='$message',`no_order_reason`='$no_order' WHERE id = '$caid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'calls_edit';
            $timeline->section_id = $caid;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($date));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Call Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_lead_call'])){
            $cid = $_POST['crm_customer_id'];
            $caid = $_POST['call_id'];
            $customer_no = $_POST['customer_no'];
            $date = $_POST['select_date'];
            $message = $_POST['message'];
            if(isset($_POST['no_order_reason'])){
                $no_order = implode(',',$_POST['no_order_reason']);
            }
            DB::update("UPDATE `crm_call` SET `customer_number`='$customer_no',`select_date`='$date',`message`='$message',`no_order_reason`='$no_order' WHERE id = '$caid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'calls_edit';
            $timeline->section_id = $caid;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($date));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Call Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        $cfg = $this->cfg;
//         if($request->session()->has('u_name')){
//             $session_name = $request->session()->get('u_name');
//             dd($session_name);
//         }
        $user = DB::table('user')->where('secure', session('crm'))->first();
        $c_id = $_GET['cid'];
        $crm_customer =  DB::table('crm_customers')->where('id',$c_id)->get();

        $converted_by= $crm_customer[0]->converted_by?$crm_customer[0]->converted_by:'';

        $user_name= $user->u_id;

        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Customer Data';
        $customers = DB::table('crm_customers')
            ->join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
            ->select('crm_customers.*','customer_category.type')
            ->get();
        $product_category = DB::table('category')->get();
        $brand = DB::table('brand')->get();
        $suppliers = DB::table('crm_supplier')->get();
        $users = DB::table('user')->get();
        $month = date('m');
        $start_date = date('Y-m-01 h:i:s');
        $end_date  = date('Y-m-t h:i:s');
        if(isset($_GET['month'])){
            $month=$_GET['month'];
            $start_date = date('Y-'.$month.'-01 h:i:s');
            $end_date  = date('Y-'.$month.'-t 23:59:59');
        }
        $timeline_data = false;
        if(isset($_GET['cid'])) {
            $timeline_count = Timeline::where('customer_id', $_GET['cid'])->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'DESC')->count();

            if ($timeline_count) {
                $timeline_data = Timeline::where('customer_id', $_GET['cid'])->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'DESC')->get()->groupBy(function ($date) {
                    return Carbon::parse($date->date)->format('d');
                });
            }
        }
        $states = DB::table('states')->where('country_id','=',101)->get();
        $districts = DB::table('district')->get();
        $blocks = DB::table('blocks')->get();
        return view('crm/customer-profile')->with(compact('header','cfg','tp','footer', 'title','notices','customers','customer','cid','visits','promotionals','product_category','products','calls','brand','suppliers','users','messages', 'month', 'timeline_data', 'states', 'districts','user','user_name','designations','converted_by','blocks','no_order_reasons','sales_pipelines'));
    }

    public function Ticketedit( Request $request){
        $notices = '';
        $tid = $request->id;

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Ticket Edit';
        $tickets = DB::table('crm_tickets')->where('id','=',$tid)->first();
        $users = DB::table('user')->get();

        return view('crm/ticket-edit')->with(compact('header','cfg','tp','footer', 'title','notices','tickets','users'));

    }
    public function Promoedit( Request $request){
        $notices = '';
        $tid = $request->id;

        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Promotional Edit';
        $promotional = DB::table('crm_promotional')->where('id','=',$tid)->first();
        return view('crm/promotional-edit')->with(compact('header','cfg','tp','footer', 'title','notices','tickets','promotional'));

    }
    public function Projectedit( Request $request){
        $notices = '';
        $tid = $request->id;
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Project Edit';
        $projects = DB::table('crm_customer_projects')->find($tid);
        $project_subtype = DB::table('crm_project_subtype')->where('project_type_id','=',$projects->project_type)->get();
        $states = DB::table('states')->where('country_id', 101)->get();
        $districts = DB::table('district')->get();
        $blocks = DB::table('blocks')->get();
        $districtsx = false;
        $localityx = false;
        $blocksx = false;
        //dd($projects);
        if($projects->state !== ''){
            $districtsx = DB::table('district')->where('state_id', $projects->state)->get();
        }
        if($projects->district !== ''){
            $blocksx = DB::table('blocks')->where('district_id', $projects->district)->get();

        }
        if($projects->block !== ''){
            $localityx = DB::table('locality')->where('block_id', $projects->block)->get();
        }

        //dd($districtsx);
//        echo "SELECT * FROM `crm_project_subtype` WHERE project_type =  '$projects->project_type'";
//        dd($project_subtype );
        return view('crm/project-edit')->with(compact('header','cfg','tp','footer', 'title','notices','projects','project_subtype', 'states', 'districts', 'districtsx', 'localityx','blocks','blocksx'));

    }

    public function Customerclassupdate (){

        $status = $_POST['class'];
        $reason = $_POST['reason'];
        $notreason = $_POST['notreason'];
        $dunqualified = $_POST['dunqualied'];
        $dcontacted = $_POST['dcontacted'];
        $dinterested = $_POST['dinterested'];
        $dnotinterest = $_POST['dnotinterest'];
//        $dconverted = $_POST['dconverted'];
        $user_unqualified    = $_POST['user_unqualified'];
        $user_notinterested    = $_POST['user_notinterested'];
        $user_contacted    = $_POST['user_contacted'];
        $user_interested    = $_POST['user_interested'];
        $user_converted    = $_POST['user_converted'];
        $user_unreachable    = $_POST['user_unreachable'];
        $ureason    = $_POST['ureason'];
        $dunreachable    = $_POST['dunreachable'];
        $cid = $_POST['cid'];
        $customer = DB::table('crm_customers')->join('customer_class', 'crm_customers.class', '=', 'customer_class.id')->where('crm_customers.id', $cid)->first();
        $class = DB::table('customer_class')->find($status);
        $msg=0;

        $customerx = CrmCustomer::find($cid);
        if($status>=5){
            $customerx->unqualified_reason = $reason;
            $customerx->unqualified_date = $dunqualified;
            $customerx->user_unqualified = $user_unqualified;
            $msg = 1;
        }
        if($status>=7){
            $customerx->ureason = $ureason;
            $customerx->dunreachable = $dunreachable;
            $customerx->user_unreachable = $user_unreachable;
            $msg = 1;
        }
        if($status>=6){
            $customerx->notinterested_reason = $notreason;
            $customerx->notinterested_date = $dnotinterest;
            $customerx->user_notinterested = $user_notinterested;
            $msg = 1;
        }
        if($status == 4){
            $customerx->conversion_date = date('Y-m-d h:i:s');
            $customerx->user_converted = $user_converted;
        }
        if($status == 2){
            $customerx->contacted_date = $dcontacted;
            $customerx->user_contacted = $user_contacted;
        }
        if($status == 3){
            $customerx->interested_date = $dinterested;
            $customerx->user_interested = $user_interested;
        }
        $customerx->class = $status;
        $customerx->msg_active = $msg;
        $customerx->save();
        // DB::update("update crm_customers set class = '$status', msg_active = '$msg' WHERE id =  '$cid'");

        $timeline = new Timeline();
        $timeline->customer_id = $cid;
        $timeline->section = 'class';
        $timeline->section_id = $class->id;
        $luser = DB::table('user')->where('secure', session('crm'))->first();
        if($luser !== null) {
            $timeline->created_by = $luser->u_id;
        }
        $timeline->date = date('Y-m-d h:i:s');
        $timeline->remark = 'Customer class changed from '.$customer->type.' to '.$class->type;
        $timeline->save();
        return 'success';
    }

    function Getsubtype(Request $request){
        $data = '';
        $tid = $_POST['tid'];
        $data1  = DB::table('crm_project_subtype')->where('project_type_id','=',$tid)->get();
        foreach ($data1 as $dta){
            $data .= '<option value="'.$dta->id.'">'.$dta->project_subtype.'</option>';
        }
        return $data;
    }
    function GetPriceGroupsAjax(Request $request){
        $data = '<option value="">Select Price Group</option>';
        $cid = $_POST['cid'];
        $data1  = DB::table('category_groups')->join('category', 'category.id', '=', 'category_groups.category_id')->whereIn('category_id', explode(',', $cid))
            ->select(['category_groups.id as id', 'group_name', 'category.name'])
            ->get();
        foreach ($data1 as $dta){
            $data .= '<option value="'.$dta->id.'">'.$dta->name.' - '.$dta->group_name.'</option>';
        }
        return $data;
    }

    public function Projectsubtypemaster(Request $request) {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_proj_stype")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $selected_district = array();
        $all_category_price = '';
        if (isset($_POST['add'])) {
            $data['project_type_id'] = $_POST['ptype'];
            $data['project_subtype'] = $_POST['subtype'];
            DB::table('crm_project_subtype') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Sub Type Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }

        if (isset($_POST['edit'])) {
            $ptid = $_POST['cid'];
            $subtype = $_POST['subtype'];

            DB::update("UPDATE `crm_project_subtype` SET `project_subtype` = '$subtype' WHERE id = '$ptid'");
            $notices .= '<div class="card-alert card green"> <div class = "card-content white-text" >
            <p> Sub Type Added Successfully. </p> </div> <button type = "button" class = "close white-text" data - dismiss = "alert" aria - label = "Close" >
            <span aria - hidden = "true" > × < /span> < /button> </div>';

        }

        if (isset($_GET['edit'])) {

            $cid = $_GET['edit'];
            $selected_district = DB::table('crm_customers')
                ->leftJoin('district', 'state_id', '=', 'crm_customers.state')
                ->where('crm_customers.id','=',$cid)
                ->select('district.name as d_name','district.id as did')
                ->get();
            $all_category_price = DB::table('customer_category_price')
                ->join('customer_category', 'customer_category_price.customer_category', '=', 'customer_category.id')
                ->join('category', 'customer_category_price.product_category', '=', 'category.id')
                ->orderBy('category.id')
                ->select('customer_category_price.*','customer_category.type','category.name')
                ->get();
        }

        $states = DB::table('states')->where('country_id','=',101)->get();
        $district = DB::table('district')
            ->join('states', 'district.state_id', '=', 'states.id')
            ->where('states.country_id','=',101)
            ->select('district.name as district_name','district.id as district_id','states.name as state_name','states.id as state_id')
            ->get();
        $cfg = $this -> cfg;
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';


        $project_subtype = DB::table('crm_project_subtype')
            ->join('crm_project_type', 'crm_project_subtype.project_type_id', '=', 'crm_project_type.id')
            ->select('crm_project_subtype.*','crm_project_type.project_type')
            ->get();
        $project_type = DB::table('crm_project_type')->get();

        return view('crm/project-subtype-master') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'project_subtype', 'project_type', 'buttons'));

    }

    function Customerprojectdelete(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        $cid = $_POST['cid'];
        DB::table('crm_customer_projects')->where('id','=',$pid)->delete();
        $data = 'Project  Deleted Successfully.';
        $timeline = Timeline::where('section', 'projects')->where('section_id', $pid);
        $timeline->delete();
        $timelinex = Timeline::where('section', 'projects_edit')->where('section_id', $pid);
        $timelinex->delete();
        return $data;
    }

    function Customercategorydelete(Request $request){
        $data = '';
        $cid = $_POST['cid'];

        $data1  = DB::table('crm_customers')->where('customer_category','=',$cid)->first();
//        dd($data1);
        if(empty($data1)){
            DB::table('customer_category')->where('id','=',$cid)->delete();
            $data = 'Category Deleted Successfully.';
        }
        else{
            $data = 'Category Already in Used.';
        }
        return $data;
    }
    function CustomerDesignationdelete(Request $request){
        $data = '';
        $cid = $_POST['cid'];
         DB::table('customer_designation')->where('id','=',$cid)->delete();
            $data = 'Designation Deleted Successfully.';

        return $data;
    }
    function CustomerTranportordelete(Request $request){
        $data = '';
        $cid = $_POST['cid'];
         DB::table('transportor')->where('id','=',$cid)->delete();
            $data = 'Transportor Deleted Successfully.';

        return $data;
    }
    function CustomerContaxdelete(Request $request){
        $data = '';
        $cid = $_POST['cid'];
        DB::table('crm_contax')->where('id','=',$cid)->delete();
        $data = 'Contax Deleted Successfully.';

        return $data;
    }

    function AssociationDelete(Request $request){
        $data = '';
        $cid = $_POST['cid'];

//        dd($data1);
        DB::table('crm_association')->where('id','=',$cid)->delete();
        $data = 'Association Deleted Successfully.';


        return $data;
    }


    function Projectsubtypedelete(Request $request){
        $data = '';
        $cid = $_POST['cid'];

        $data1  = DB::table('crm_customer_projects')->where('project_subtype','=',$cid)->first();
        if(empty($data1)){
            DB::table('crm_project_subtype')->where('id','=',$cid)->delete();
            $data = 'Project Subtype Deleted Successfully.';
        }
        else{
            $data = 'Project Subtype Already in Used.';
        }
        return $data;
    }

    function Customerticketdelete(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        $cid = $_POST['cid'];
        DB::table('crm_tickets')->where('id','=',$pid)->delete();
        $data = 'Ticket  Deleted Successfully.';
        $timeline = Timeline::where('section', 'ticktes')->where('section_id', $pid);
        $timeline->delete();
        $timelinex = Timeline::where('section', 'tickets_edit')->where('section_id', $pid);
        $timelinex->delete();
        return $data;
    }

    function Customernotedelete(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        $cid = $_POST['cid'];
        DB::table('crm_notes')->where('id','=',$pid)->delete();
        $data = 'Notes  Deleted Successfully.';
        $timeline = Timeline::where('section', 'notes')->where('section_id', $pid);
        $timeline->delete();
        $timelinex = Timeline::where('section', 'notes_edit')->where('section_id', $pid);
        $timelinex->delete();
        return $data;
    }
    function TransIssuedelete(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        DB::table('trans_issues')->where('id','=',$pid)->delete();
        $data = 'Issue  Deleted Successfully.';
        return $data;
    }

    public function Noteedit( Request $request){
        $notices = '';
        $tid = $request->id;
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Note Edit';
        $notes = DB::table('crm_notes')->where('id','=',$tid)->first();
        // $project_subtype = DB::select("SELECT * FROM `crm_project_subtype` WHERE project_type_id =  '$projects->project_type'");
//        echo "SELECT * FROM `crm_project_subtype` WHERE project_type =  '$projects->project_type'";
//        dd($project_subtype );
        return view('crm/note-edit')->with(compact('header','cfg','tp','footer', 'title','notices','notes'));

    }
    public function Issueedit( Request $request){
        $notices = '';
        $tid = $request->id;
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Issue Edit';
        $issue = DB::table('trans_issues')->where('id','=',$tid)->first();

        return view('crm/issue-edit')->with(compact('header','cfg','tp','footer', 'title','notices','issue'));

    }

    public function Visitedit( Request $request){
        $notices = '';
        $tid = $request->id;
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Visit Edit';
        $visit = DB::table('crm_visits')->where('id','=',$tid)->first();
        $users = DB::table('user')->get();
        return view('crm/visit-edit')->with(compact('header','cfg','tp','footer', 'title','notices','visit','users'));

    }

    public function Promotionaledit( Request $request){
        $notices = '';
        $tid = $request->id;
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Promotional Edit';
        $promotional = DB::table('crm_promotional')->where('id','=',$tid)->first();
        return view('crm/promotional-edit')->with(compact('header','cfg','tp','footer', 'title','notices','promotional'));

    }

    function Customervisitdelete(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        $cid = $_POST['cid'];
//        dd($pid);
        DB::table('crm_visits')->where('id','=',$pid)->delete();
        $data = 'Visits  Deleted Successfully.';
        $timeline = Timeline::where('section', 'visits')->where('section_id', $pid);
        $timeline->delete();
        $timelinex = Timeline::where('section', 'visits_edit')->where('section_id', $pid);
        $timelinex->delete();
        return $data;
    }

    function Customerpromotionaldelete(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        $cid = $_POST['cid'];
        DB::table('crm_promotional')->where('id','=',$pid)->delete();
        $data = 'Promotional  Deleted Successfully.';
        $timeline = Timeline::where('section', 'promotional')->where('section_id', $pid);
        $timeline->delete();
        $timelinex = Timeline::where('section', 'promotional_edit')->where('section_id', $pid);
        $timelinex->delete();
        return $data;
    }

    public function Productedit( Request $request){

        //dd($request);
        $notices = '';
        $tid = $request->id;
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Product Edit';
        $product =  DB::table('crm_products')->where('id','=',$tid)->first();
        $products = DB::table('category')->get();
        $brand =  DB::table('brand')->get();
        $suppliers = DB::table('crm_supplier')->get();
        $price_groups  = DB::table('category_groups')->join('category', 'category.id', '=', 'category_groups.category_id')->whereIn('category_id', array_pluck($products, 'id'))
            ->select(['category_groups.id as id', 'group_name', 'category.name'])
            ->get();
        return view('crm/product-edit')->with(compact('header','cfg','tp','footer', 'title','notices','product','products','brand','suppliers', 'price_groups'));

    }

    function Customerproductdelete(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        $cid = $_POST['cid'];
//        dd($pid);
        DB::table('crm_products')->where('id','=',$pid)->delete();
        $data = 'Product  Deleted Successfully.';
        $timeline = Timeline::where('section', 'products')->where('section_id', $pid);
        $timeline->delete();
        $timelinex = Timeline::where('section', 'products_edit')->where('section_id', $pid);
        $timelinex->delete();
        return $data;
    }

    function GetDistrictAjax(Request $request){
        $data = '';
        $cid = $_POST['cid'];
        if($cid =='')$cid = 0;
        $data1  = DB::table('district')->where('state_id',$cid)->get();
        foreach ($data1 as $dt){
            $data .= '<option value ="'.$dt->id.'">'.$dt->name.'</option>';
        }

        return $data;
    }
    function GetStagesAjax(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        if($pid =='')$pid = 0;
        $data1  = array_pluck(DB::table('pipeline_stages')->where('pipeline_id',$pid)->get(),'stage_name');
        foreach ($data1 as $stage){
            $data .= '<option value ="'.$stage.'">'.$stage.'</option>';
        }

        return '<option value="">Select Option</option>'.'<option value ="new">New</option>'.$data.'<option value ="won">Won</option>'.'<option value ="lost">Lost</option>';
    }
    function UpdateCallLogAjax(Request $request){
        $id = $request->id;
        $lid = $request->leadid;
        $message = $request->message;
        DB::update("UPDATE `crm_call` SET `message`='$message' WHERE id = '$id' AND lead_id = '$lid'");

    }
    function DeleteCallLogAjax(Request $request){

        $id = $request->id;
        $lid = $request->leadid;
        $lead_call_log_id = $request->lead_call_log_id;
        DB::table('crm_call')->where('id',$id)->where('lead_id','=',$lid)->delete();


    }
    function DeletePipelineAjax(Request $request){
        $id = $request->id;
        $checkleadorder = DB::table('lead_orders')->where('pipelines_id',$id)->get();

        if(count($checkleadorder)>0){
            return "Pipeline Uses Somewhere So You Can't Delete It. ";
        }else{
            DB::table('sales_pipelines')->where('id','=',$id)->delete();
            return "Deleted Successfully!!! ";
        }
    }
    function GetLocalityAjax(Request $request){
        $data = '<option value="">Select Locality</option>';
        $cid = $_POST['did'];
        if($cid =='')$cid = 0;
        $data1  = DB::table('locality')->where('block_id',$cid)->get();
        foreach ($data1 as $dt){
            $data .= '<option value ="'.$dt->id.'">'.$dt->locality.'</option>';
        }

        return $data;
    }
    function GetBlockAjax(Request $request){
        $data = '<option value="">Select Block</option>';
        $cid = $_POST['did'];
        if($cid =='')$cid = 0;
        $data1  = DB::table('blocks')->where('district_id',$cid)->get();
        foreach ($data1 as $dt){
            $data .= '<option value ="'.$dt->id.'">'.$dt->block.'</option>';
        }

        return $data;
    }

    public function Suppliers(Request  $request)
    {
        $suppliers = '';
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_sup")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        $pcid = '';
        $selected_district=array();
        if(isset($_POST['add'])){

            //dd($_POST);
            if(isset($_POST['category'])){
                $pcid = implode(',',$_POST['category']);
            }
            $customer_category = $_POST['customer_category'];
            $name = $_POST['name'];
            $proprieter_name = $_POST['proprieter_name'];
            $postal_address = $_POST['postal_address'];
            $email = $_POST['email'];
            $contact_no = $_POST['contact_no'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $msg_active = $_POST['msg_active'];
            DB::insert("INSERT INTO `crm_supplier`(`customer_category`, `product_category`, `name`, `proprieter_name`, `postal_address`, `email`, `contact_no`, `district`, `state`, `msg_active`) VALUES ('$customer_category','$pcid','$name','$proprieter_name','$postal_address','$email','$contact_no','$district','$state','$msg_active')");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Supplier Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
        }
        if(isset($_POST['edit'])){
            // dd($_POST);
            if(isset($_POST['category'])){
                $pcid = implode(',',$_POST['category']);
            }

            $cidd = $_POST['cidd'];
            $customer_category = $_POST['customer_category'];
            $name = $_POST['name'];
            $proprieter_name = $_POST['proprieter_name'];
            $postal_address = $_POST['postal_address'];
            $email = $_POST['email'];
            $contact_no = $_POST['contact_no'];
            $district = $_POST['district'];
            $state = $_POST['state'];
            $msg_active = $_POST['msg_active'];
            DB::update("UPDATE `crm_supplier` SET `customer_category`='$customer_category',`product_category` = '$pcid',`name`='$name',`proprieter_name`='$proprieter_name',`postal_address`='$postal_address',`email`='$email',`contact_no`='$contact_no',`district`='$district',`state`='$state',`msg_active`='$msg_active' WHERE id = '$cidd'");
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p> Supplier Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

        }
        if(isset($_GET['edit'])){
            $cid = $_GET['edit'];
            $selected_district = DB::table('crm_supplier')
                ->leftJoin('district', 'state_id', '=', 'crm_supplier.state')
                ->where('crm_supplier.id', '=', $cid)
                ->select('district.name as d_name','district.id as did')
                ->get();
        }


        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Supplier';
        $a = '';

        $states = DB::table('states')->where('country_id','=',101)->get();

        $customer_class = DB::table('customer_class')->get();
        $customer_type = DB::table('customer_type')->get();
        $customer_category = DB::table('customer_category')->get();
        $product_category = DB::table('category')->get();
        return view('crm/suppliers')->with(compact('header','cfg','tp','footer', 'title','notices','states','customer_class','customer_type','customer_category','selected_district','product_category', 'suppliers','buttons'));

    }
    public function getSuppliers(Request $request){

        $records = DB::table('crm_supplier')
            ->join('customer_category', 'customer_category.id', '=', 'crm_supplier.customer_category')
            ->select(['crm_supplier.*', 'customer_category.type as category']);

        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $products = (isset($_GET["products"])) ? ($_GET["products"]) : false;

        if($category){
            $records = $records->whereIn('customer_category.id', $category);
        }
        if($state){
            $records = $records->whereIn('state', $state);
        }
        if($district){
            $records = $records->whereIn('district', $district);
        }
        if($products){
            $records = $records->whereRaw('FIND_IN_SET(?, product_category)', $products);
        }

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $html = '<button value="'.$records->id.'" class="btn myred waves-light supp_delete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="suppliers?edit='.$records->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('product_category', function($records){
                $pcs = DB::table('category')->whereIn('id', explode(',', $records->product_category))->get();
                $html = '';
                foreach($pcs as $pc){
                    $html.='<span class="badge success">'.$pc->name.'</span>';
                }
                return $html;
            })
            ->rawColumns(['product_category', 'action'])
            ->make(true);
    }

    function Supplierdelete(Request $request){
        $data = '';
        $cid = $_POST['cid'];
        DB::table('crm_supplier')->where('id','=',$cid)->get();
        $data = 'Product  Deleted Successfully.';
        return $data;
    }

    public function Calledit( Request $request){
        $notices = '';
        $tid = $request->id;
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Note Edit';
        $calls = DB::table('crm_call')->where('id','=',$tid)->first();
        $no_orders = DB::table('no_order')->get();
        return view('crm/call-edit')->with(compact('header','cfg','tp','footer', 'title','notices','calls','no_orders'));

    }

    public function LeadOrderEdit(Request $request){
        $notices = '';
        $customer = false;
        $cid = '';
        $tickets = array();
        $visits = array();
        $notes = array();
        $projects = array();
        $promotionals = array();
        $products = array();
        $tid = $request->id;


        $user = DB::table('user')->where('secure', session('crm'))->first();
        $sales_pipelines = DB::table('sales_pipelines')->get();
        $designations = DB::table('customer_designation')->get();
        $no_order_reasons = DB::table('no_order')->get();
        $selected_option = $user->u_name;

        if(isset($_GET['cid'])){
            $cid = $_GET['cid'];
            $customer = DB::table('crm_customers')
                ->join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
                ->where('crm_customers.id', '=', $cid)
                ->select('crm_customers.*','crm_customers.type as type_id','customer_category.type')
                ->first();

            $visits = DB::table('crm_visits')
                ->where('crm_customer_id', '=', $cid)
                ->get();
            $promotionals = DB::table('crm_promotional')
                ->where('crm_customer_id', '=', $cid)
                ->get();
            $products = DB::table('crm_products')
                ->where('crm_customer_id', '=', $cid)
                ->get();
            $calls = DB::table('crm_call')
                ->where('crm_customer_id', '=', $cid)
                ->get();
            $messages = DB::select("SELECT * FROM `crm_message_save` WHERE mobile IN (".$customer->contact_no.")");
        }

        if(isset($_POST['add_call'])){
            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['customer_number'] = $_POST['customer_no'];
            //$data['landline_no'] = $_POST['landline_no'];
            //$data['conperson_id'] = $_POST['conperson'];
            $data['select_date'] = $_POST['select_date'];
            $data['message'] = $_POST['message'];
            $data['user_id'] = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $data['call_type'] = $request->ctype;
            if(!empty($_POST['no_order_reason'])){
                $data['no_order_reason'] = implode(',',$_POST['no_order_reason'])?implode(',',$_POST['no_order_reason']):'';
            }

            $id = DB::table('crm_call')->insertGetId($data);

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'calls';
            $timeline->section_id = $id;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($data['select_date']));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Call Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['add_lead'])){

            $cid = $_POST['crm_cust_id'];
            $data['crm_customer_id'] = $cid;
            $data['select_date'] = $_POST['select_date'];
            $data['pipelines_id'] = $_POST['name'];
            $data['pipelines_stage'] = $_POST['stage'];
            $data['edited_by'] = $user->u_name;
            if(isset($_POST['product_category'])){
                $data['product_category'] = implode(',',$_POST['product_category'])?implode(',',$_POST['product_category']):'';
            }

            $id = DB::table('lead_orders')->insertGetId($data);


            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Lead Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['save_msg'])){
            $cid = $_POST['crm_cust_id'];
            $data['mobile'] = $_POST['customer_no'];
            $data['message'] = $_POST['message'];
            $data['user_id'] = implode(',',$_POST['user']);
            $cus = implode(',',$_POST['user']);
            $data['edited_by'] = $user->u_name;
            $id = DB::table('crm_message_save')->insertGetId($data);
            /*foreach ($cus as $d) {
                $c = DB::select("SELECT * FROM crm_customers WHERE id = '".$d."'");
                $sms = urlencode($_POST['contact_no']);
                $no = urlencode($c[0]->mobile);
                echo $url = "https://merasandesh.com/api/sendsms?username=smshop&password=Smshop@123&senderid=AALERT&message=" . $sms . "&numbers=" . $no . "&unicode=0";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FAILONERROR, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $output = curl_exec($ch);
                curl_error($ch);
                curl_close($ch);
            }*/

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New message Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_call'])){
            $cid = $_POST['crm_customer_id'];
            $caid = $_POST['call_id'];
            $customer_no = $_POST['customer_no'];
            $date = $_POST['select_date'];
            $message = $_POST['message'];
            if(isset($_POST['no_order_reason'])){
                $no_order = implode(',',$_POST['no_order_reason']);
            }
            DB::update("UPDATE `crm_call` SET `customer_number`='$customer_no',`select_date`='$date',`message`='$message',`no_order_reason`='$no_order' WHERE id = '$caid'");

            $timeline = new Timeline();
            $timeline->customer_id = $cid;
            $timeline->section = 'calls_edit';
            $timeline->section_id = $caid;
            $luser = DB::table('user')->where('secure', session('crm'))->first();
            $timeline->created_by = $luser->u_id;
            $timeline->date = date('Y-m-d h:i:s', strtotime($date));
            $timeline->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Call Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_lead_call'])){
            $cid = $_POST['crm_cust_id'];
            $data1['crm_customer_id'] = $cid;
            $data1['customer_number'] = $_POST['customer_no'];
            $data1['select_date'] = $_POST['select_date'];
            $data1['message'] = $_POST['message'];
            $data1['user_id'] = implode(',',$_POST['user']);
            $data1['edited_by'] = $user->u_name;
            $data1['call_type'] = $request->ctype;
            $data1['lead_id'] = $_POST['id'];
            $leadid = DB::table('crm_call')->insertGetId($data1);
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Call Log Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_lead_pipeline'])){
            $u_id = $user->u_id;
            if(isset($_POST['id'])){
                $id = $_POST['id'];
            }
            $pipelines_id = $_POST['name'];
            $pipelines_stage = $_POST['stage'];
            $select_date = $_POST['date'];
            $no_order_reason='';
            if(isset($_POST['no_order_reason'])){
                $no_order_reason = implode(',',$_POST['no_order_reason']);
            }
            if(isset($_POST['product_category'])){
                $product_category = implode(',',$_POST['product_category']);
            }

            DB::update("UPDATE `lead_orders` SET `pipelines_id` = '$pipelines_id',`pipelines_stage` = '$pipelines_stage',`select_date`='$select_date',`user_id`='$u_id',`product_category`='$product_category',`no_order_reason`='$no_order_reason' WHERE id = '$id'");

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Pipeline Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }

        $cfg = $this->cfg;
//         if($request->session()->has('u_name')){
//             $session_name = $request->session()->get('u_name');
//             dd($session_name);
//         }
        $user = DB::table('user')->where('secure', session('crm'))->first();
        $c_id = $_GET['cid'];
        $crm_customer =  DB::table('crm_customers')->where('id',$c_id)->get();

        $converted_by= $crm_customer[0]->converted_by?$crm_customer[0]->converted_by:'';

        $user_name= $user->u_id;

        $tp = url("/assets/crm/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Lead Edit';
        $customers = DB::table('crm_customers')
            ->join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
            ->select('crm_customers.*','customer_category.type')
            ->get();
        $product_category = DB::table('category')->get();
        $brand = DB::table('brand')->get();
        $suppliers = DB::table('crm_supplier')->get();
        $users = DB::table('user')->get();
        $month = date('m');
        $start_date = date('Y-m-01 h:i:s');
        $end_date  = date('Y-m-t h:i:s');
        if(isset($_GET['month'])){
            $month=$_GET['month'];
            $start_date = date('Y-'.$month.'-01 h:i:s');
            $end_date  = date('Y-'.$month.'-t 23:59:59');
        }
        $timeline_data = false;
        if(isset($_GET['cid'])) {
            $timeline_count = Timeline::where('customer_id', $_GET['cid'])->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'DESC')->count();

            if ($timeline_count) {
                $timeline_data = Timeline::where('customer_id', $_GET['cid'])->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'DESC')->get()->groupBy(function ($date) {
                    return Carbon::parse($date->date)->format('d');
                });
            }
        }
        $states = DB::table('states')->where('country_id','=',101)->get();
        $districts = DB::table('district')->get();
        $blocks = DB::table('blocks')->get();
        $crm_calls = DB::table('lead_orders')->where('crm_customer_id', $request->cid)->where('pipelines_id','!=','')->get();
        $leads = DB::table('lead_orders')->where('id','=',$tid)->first();
        $call_logs= DB::table('crm_call')->where('lead_id','=',$leads->id)->get();
        $customer = DB::table('crm_customers')->join('customer_class', 'customer_class.id', '=', 'crm_customers.class')->where('crm_customers.id', $cid)->first();

        return view('crm/lead_order-edit')->with(compact('header','cfg','tp','footer', 'title','notices','customers','customer','cid','visits','promotionals','product_category','products','calls','brand','suppliers','users','messages', 'month', 'timeline_data', 'states', 'districts','user','user_name','designations','converted_by','blocks','no_order_reasons','sales_pipelines','leads','call_logs','crm_calls'));

    }
    public function TransportorOrderEdit(Request $request){
        $cfg = '';
        $customer_type = '';
        $user = DB::table('user')->where('secure', session('crm'))->first();
        $users = DB::table('user')->where('user_type','=','crm_user')->get();
        $states = DB::table('states')->where('country_id','=',101)->get();
        $districts = DB::table('district')->get();

        if(!checkRole($user->u_id,"tran_mas")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if(isset($_GET['cid'])){
            $tid = $_GET['cid'];
            $transportor = DB::table('transportor')->where('id',$tid)->first();

        }
        $transportors = DB::table('transportor')->get();
        $customers = DB::table('crm_customers')->select('id','name','contact_no')->get()->toArray();

        if (isset($_POST['add'])) {
            $users = '';
            $id = $_POST['tid'];
            $names = isset($_POST['names'])?implode(',',$_POST['names']):false;
            $contacts = isset($_POST['contacts'])?implode(',',$_POST['contacts']):false;
            $designations = isset($_POST['descs'])?implode(',',$_POST['descs']):false;
            $landlines = isset($_POST['landlines'])?implode(',',$_POST['landlines']):false;
            $name = $_POST['name'];
            if($names){
                $name = $name .','.$names;
            }
            $company_name = $_POST['cname'];
            $email = isset($_POST['emails'])?implode(',',$_POST['emails']):false;
            $caddress = $_POST['caddress'];
            $state = $_POST['states'];
            $district = $_POST['district'];
            $contact_no = $_POST['contact_number'];
            if($contacts){
                $contact_no = $contact_no.','.$contacts;
            }
            $contact_designation = $_POST['designation'];
            if($designations){
                $contact_designation = $contact_designation.','.$designations;
            }
            $landline = $_POST['landline'];
            if($landlines){
                $landline = $landline.','.$landlines;
            }
            $refered_by = isset($_POST['listcust'])?implode(',',$_POST['listcust']):false;
            $updated_at = date('Y-m-d H:i:s');
            DB::update("UPDATE transportor set `name`='$name',`state`='$state',`district`='$district' ,`company_name`='$company_name',`company_address`='$caddress',
`email`='$email',`contact_no`='$contact_no',`contact_designation`='$contact_designation',`landline`='$landline',`refered_by`='$refered_by',`updated_at`='$updated_at' where `id`='$id'");
            return back();
            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Transportor Updated Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back();

        }

        if(isset($_POST['edit'])){
            $users = '';
            $id = $_POST['cid'];
            $name = $_POST['name'];
            $company_name = $_POST['cname'];
            $caddress = $_POST['caddress'];
            $state = $_POST['states'];
            $st_state = $_POST['ststates'];
            $st_district = $_POST['district'];
            $ed_state = $_POST['states2'];
            $ed_district = $_POST['district2'];
            $updated_at = $_POST['updated_at'];

            DB::update("UPDATE transportor set `name`='$name',`state`='$state' ,`company_name`='$company_name',`company_address`='$caddress',
`ststate`='$st_state',`stdistrict`='$st_district',`edstate`='$ed_state',`eddistrict`='$ed_district',`updated_at`='$updated_at' where `id`='$id'");
            return back();
        }

        if (isset($_POST['addroot'])) {

            $id = $_POST['tid'];
            $st_state = isset($_POST['ststates'])?serialize($_POST['ststates']):false;
            $st_district = isset($_POST['stdistrict'])?serialize($_POST['stdistrict']):false;
            $ed_state = isset($_POST['edstates'])?serialize($_POST['edstates']):false;
            $ed_district = isset($_POST['eddistrict'])?serialize($_POST['eddistrict']):false;
            $updated_at = date('Y-m-d H:i:s');
            $tans   =    Transportor::find($id);
            $tans->ststate = $st_state;
            $tans->stdistrict = $st_district;
            $tans->edstate = $ed_state;
            $tans->eddistrict = $ed_district;
            $tans->save();

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Transportor Root Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';
            return back();
        }

        if(isset($_POST['add_issue'])){

            // dd($_POST);
            $file = '';
            if (request()->file('image')) {
                $name = request()->file('image')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().'/assets/crm/images/issue/';
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
            }
            $cid = $_POST['crm_cust_id'];
            $data['trans_id'] = $cid;
            $data['select_date'] = $_POST['select_date'];
            $data['description'] = $_POST['description'];
            $data['image'] = $file;
            $data['edited_by'] = $user->u_name;

            $id = DB::table('trans_issues')->insertGetId($data);


            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>New Notes Added Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        if(isset($_POST['edit_issue'])){
            $tid = $_POST['trans_id'];
            $issue_id = $_POST['issue_id'];
            $date = $_POST['select_date'];
            $description = $_POST['description'];
            DB::update("UPDATE `trans_issues` SET `select_date`='$date',`description`='$description' WHERE id = '$issue_id'");

            $notices .= '<div class="card-alert card green">
                <div class="card-content white-text">
                  <p>Issue Update Successfully.</p>
                </div>
                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>';

            return back()->with('notices', $notices);
        }
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        $crm_contax = DB::table('crm_contax')->groupBY('name')->get();
        return view('crm/transportor_report-edit')-> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'customer_type','transportors','user','users', 'buttons','states','crm_contax','transportor','customers','districts'));
    }

    function Customercalldelete(Request $request){

        $data = '';
        $pid = $_POST['pid'];
        $cid = $_POST['cid'];
        $lid = $_POST['lead_id'];

        if(empty($lid) || $lid ==='undefined'){
            DB::table('crm_call')->where('id',$pid)->delete();
        }
        if(!empty($lid) || $lid !=='undefined'){
            DB::table('crm_call')->where('id',$pid)->where('lead_id','=',$lid)->delete();
            DB::table('lead_orders')->where('id','=',$lid)->delete();
        }

        $data = 'Call  Deleted Successfully.';
        $timeline = Timeline::where('section', 'calls')->where('section_id', $pid);
        $timeline->delete();
        $timelinex = Timeline::where('section', 'calls_edit')->where('section_id', $pid);
        $timelinex->delete();
        return $data;
    }
    function Customerleaddelete(Request $request){

        $data = '';
        $lid = $_POST['lid'];
        $cid = $_POST['cid'];
        DB::table('lead_orders')->where('id',$lid)->delete();
        DB::table('crm_call')->where('lead_id',$lid)->delete();
        $data = 'Lead  Deleted Successfully.';
        $timeline = Timeline::where('section', 'calls')->where('section_id', $lid);
        $timeline->delete();
        $timelinex = Timeline::where('section', 'calls_edit')->where('section_id', $lid);
        $timelinex->delete();
        return $data;
    }

    public function CrmUserManager(Request $request) {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        $users = DB::table('user')->get();
        $designations = DB::table('customer_designation')->get();

        if(!checkRole($user->u_id,"mas_usr")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $buttons = "[]";
        if(checkRole($user->u_id,"export")){
            $buttons = $this->buttons;
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $user = DB::table('user')->where('u_email', $_POST['username'])->first();

            if($user !== null){
                return back()->withErrors(['error' => 'Email ID already exist.']);
            }
            $data['u_name'] = $_POST['name'];
            $data['display_name'] = $_POST['display_name'];
            $data['u_email'] = $_POST['username'];
            $data['u_pass'] = md5($_POST['password']);
            $data['role_id'] = $_POST['role'];
            $data['desigantion_id'] = $_POST['desigantion'];
            $data['user_type'] = 'crm_user';
            DB::table('user') -> insertGetId($data);

            $notices .= '<div class="card-alert card green">
                          <div class = "card-content white-text" >
                         <p> New User  Added Successfully. </p>
                         </div>
                         <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                         <span aria-hidden = "true" >×</span>
                         </button> </div>';
        }

        if (isset($_POST['edit'])) {

            $uid = $_POST['uid'];
            $name = $_POST['name'];
            $dname = $_POST['display_name'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $role = $_POST['role'];
            $designation=$_POST['desigantion'];


            DB::update("UPDATE `user` SET `u_name` = '$name',`u_email` = '$username',`display_name`='$dname',`role_id`='$role',`desigantion_id`='$designation' WHERE u_id = '$uid'");
            if($password != ''){
                $password = md5($password);
                DB::update("UPDATE `user` SET `u_pass` = '$password' WHERE u_id = '$uid'");
            }
            $notices .= '<div class="card-alert card green">
                          <div class = "card-content white-text" >
                         <p> New User  Update Successfully. </p>
                         </div>
                         <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                         <span aria-hidden = "true" >×</span>
                         </button> </div>';

        }

        $cfg = $this -> cfg;
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        $roles = DB::table('crm_user_role')->get();
        return view('crm/user-manager') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices','roles','user','users','designations', 'buttons'));

    }
    public function getUsers(Request $request){

        $records = DB::table('user')->where('user_type', 'crm_user');

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $html = '<a class="btn myblue waves-light" style="padding:0 10px;" href="user-manager?edit='.$records->u_id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('role_id', function($records){
                $pcs = DB::table('crm_user_role')->where('id', $records->role_id)->get();
                $html = '';
                foreach($pcs as $pc){
                    $html.='<span class="badge success">'.$pc->role.'</span>';
                }
                return $html;
            })
            ->rawColumns(['action', 'role_id'])
            ->make(true);
    }
    public function getDistricts(Request $request){

        $records = DB::table('district')
            ->join('states', 'states.id', '=', 'district.state_id')
            ->select(['district.*', 'states.name as state_name']);

        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;

        if($state){
            $records = $records->whereIn('states.id', $state);
        }
        if($district){
            $records = $records->whereIn('district.id', $district);
        }

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="district-master?edit='.$records->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->make(true);
    }
    public function getBlocks(Request $request){

        $records = DB::table('blocks')
            ->join('district', 'district.id', '=', 'blocks.district_id')
            ->join('states', 'states.id', '=', 'district.state_id')
            ->select(['blocks.*', 'states.name as state_name', 'district.name as district_name']);

        $state = (isset($_GET["state"])) ? ($_GET["state"]) : false;
        $district = (isset($_GET["district"])) ? ($_GET["district"]) : false;
        $block = (isset($_GET["block"])) ? ($_GET["block"]) : false;

        if($state){
            $records = $records->whereIn('states.id', $state);
        }
        if($district){
            $records = $records->whereIn('district.id', $district);
        }
        if($block){
            $records = $records->whereIn('blocks.id', $block);
      }

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light bdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="block-master?edit='.$records->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->make(true);
    }
    public function blockDelete(Request $request){
        if(isset($request->id)){
            DB::table('blocks')->where('id', $request->id)->delete();
            return 'success';
        }
        return 'failed';
    }
    public function getLocality(Request $request){

        $records = DB::table('locality')
            ->join('district', 'district.id', '=', 'locality.district_id')

            ->select(['locality.*', 'district.name as district_name',DB::raw('(select block from blocks where blocks.id=locality.block_id) as block')]);

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light locality_delete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="locality-master?edit='.$records->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->make(true);
    }
    public function getProjectsSubtype(Request $request){

        $records = DB::table('crm_project_subtype')
            ->join('crm_project_type', 'crm_project_type.id', '=', 'crm_project_subtype.project_type_id')
            ->select(['crm_project_subtype.*', 'crm_project_type.project_type'])
            ->get();

        //$records = $records->orderBy('crm_customers.id', 'asc');
        return Datatables::of($records)
            ->addColumn('action', function($records){
                $html = '<button value="'.$records->id.'" class="btn myred waves-light project_subtype_delete" style="padding:0 10px;"><i class="material-icons">delete</i></button>
                         <a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="project-subtype-master?edit='.$records->id.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->make(true);
    }
    public function Customeruserdelete(Request $request){
        $data = '';
        $pid = $_POST['pid'];
        DB::table('crm_user_manager')->where('id',$pid)->delete();
        $data = 'User  Deleted Successfully.';
        return $data;
    }

    public function CrmUserRole(Request $request) {
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_rol")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        $notices = '';
        if (isset($_POST['add'])) {
            $data['role'] = $_POST['role'];
            DB::table('crm_user_role') -> insertGetId($data);
            $notices .= '<div class="card-alert card green">
                          <div class = "card-content white-text" >
                         <p> New Role  Added Successfully. </p>
                         </div>
                         <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                         <span aria-hidden = "true" >×</span>
                         </button> </div>';
        }

        if (isset($_POST['edit'])) {
            $uid = $_POST['uid'];
            $role = $_POST['role'];
            $permissions = "";
            if(isset($_POST['permission']))
                $permissions = "".implode(",",$_POST['permission'])."";
            DB::update("UPDATE `crm_user_role` SET `role`='$role', `permissions`='$permissions' WHERE id = '$uid'");
            $notices .= '<div class="card-alert card green">
                          <div class = "card-content white-text" >
                         <p> Role  Update Successfully. </p>
                         </div>
                         <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                         <span aria-hidden = "true" >×</span>
                         </button> </div>';

        }

        $cfg = $this -> cfg;
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'CRM';
        $roles = DB::table('crm_user_role')->get();
        return view('crm/user-role') -> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'roles'));

    }

    public function CrmMsgSave(Request $request){
        $data = array();
        $data['message'] = $_POST['message'];
        $mo = $_POST['numbers'];
        $number = explode(',',$mo);
        foreach ($number as $nm){
            $data['mobile'] = $nm;
            $cust = DB::table('crm_customers')->whereRaw('FIND_IN_SET(?, contact_no)', $nm)->first();

            $id = DB::table('crm_message_save')->insertGetId($data);
            if($cust !== null) {
                $timeline = new Timeline();
                $timeline->customer_id = $cust->id;
                $timeline->section = 'sms';
                $timeline->section_id = $id;
                $luser = DB::table('user')->where('secure', session('crm'))->first();
                $timeline->created_by = $luser->u_id;
                $timeline->date = date('Y-m-d h:i:s');
                $timeline->save();
            }
        }
        return $data;
    }
    public function getTicketsData(Request $request){

        $records = DB::table('crm_tickets')->join('user','user.u_id','=','crm_tickets.assign_to')->select('crm_tickets.*','user.u_name')->where('crm_customer_id', $request->cid)->get();

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light tdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="ticket-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('acc_manager', function($records){
                return $records->u_name;
            })
            ->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/tickets/'.$records->image.'"><img src="../assets/crm/images/tickets/'.$records->image.'" width="30px" height="30px"/></a>';
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
   }

    public function getNotesData(Request $request){
        $records = DB::table('crm_notes')->where('crm_customer_id', $request->cid);

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
               $html ='<button value="'.$records->id.'" class="btn myred waves-light ndelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="note-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/notes/'.$records->image.'"><img src="../assets/crm/images/notes/'.$records->image.'" width="30px" height="30px"/></a>';
            })
           ->rawColumns(['image', 'action'])
           ->make(true);
   }

  public function getIssueData(Request $request){
       $records = DB::table('trans_issues')->where('trans_id', $request->cid);

      return Datatables::of($records)
           ->addColumn('action', function($records) use ($request){
              $html ='<button value="'.$records->id.'" class="btn myred waves-light idelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="issue-edit?id='.$records->id.'&tid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/issue/'.$records->image.'"><img src="../assets/crm/images/issue/'.$records->image.'" width="30px" height="30px"/></a>';
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }

    public function getPromoData(Request $request){
        $records = DB::table('crm_promotional')->where('crm_customer_id', $request->cid);

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light prodelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="promotinal-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    public function getCallData(Request $request){
        $records = DB::table('crm_call')->where('crm_customer_id', $request->cid)->get();

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                if(!empty($records->lead_id)){
                    $html = '<button value="'.$records->id.'" data-id="'.$records->lead_id.'"  class="btn myred waves-light calldelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px"  href="lead_order-edit?id='.$records->lead_id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                }else{
                    $html ='<button value="'.$records->id.'"  class="btn myred waves-light calldelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px"  href="call-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                }
                return $html;
            })
            ->editColumn('user_id', function($records){
                $ids = explode(',',$records->user_id);
                $st = array_pluck(DB::table('user')->whereIn('u_id', $ids)->get(), 'u_name');
                return implode(', ', $st);
            })->editColumn('contact_person', function($records){
                $name = array_pluck(CrmCustomer::where('id',$_GET['cid'])->get(),'name');
                $sec_name = array_pluck(CrmCustomer::where('id',$_GET['cid'])->get(),'sec_names');

                if(isset($_GET['cid']) == $records->contact_person){
                    return $name;
                }else{
                    if($sec_name){
                        $names = unserialize($sec_name[0]);
                        if($names){
                            foreach($names as $key=>$nam){
                                if($key == $records->contact_person){
                                    return $nam;
                                }
                            }
                        }
                    }
                    return 'NA';
                }

            })->editColumn('customer_number', function($records){
                return $records->customer_number;
            })->editColumn('contact_designation', function($records){
                if($records->contact_designation){
                    return $records->contact_designation;
                }
                return 'NA';
            })->editColumn('user_id', function($records){
                $ids = explode(',',$records->user_id);
                $st = array_pluck(DB::table('user')->whereIn('u_id', $ids)->get(), 'u_name');
                return implode(', ', $st);
            })->editColumn('pipeline', function($records){
                if(!empty($records->lead_id)){
                    $leads = array_pluck(DB::table('lead_orders')->where('lead_orders.id',$records->lead_id)->get(),'pipelines_id');
                    if(!empty($leads)){
                        $pipelinename = array_pluck(DB::table('sales_pipelines')->where('id',$leads[0])->get(),'name');
                        if(!empty($pipelinename)){
                            return $pipelinename[0];
                        }else{
                            return 'NA';
                        }
                    }else{
                        return 'NA';
                    }
                }else{
                    return 'NA';
                }

                return 'NA';
            })
            ->rawColumns(['action'])

            ->make(true);
    }
    public function getLeaveOrderData(Request $request){

        $records = DB::table('lead_orders')->where('crm_customer_id', $request->cid)->get();

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light leaddelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="lead_order-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('pipeline', function($records){
                if(!empty($records->pipelines_id)){
                    $pipeline = DB::table('sales_pipelines')->where('id',$records->pipelines_id)->first();
                    if(!empty($pipeline)){
                        return $pipeline->name;
                    }else{
                        return 'NA';
                    }

                }else{
                    return 'NA';
                }


            })->editColumn('stage', function($records){
                return $records->pipelines_stage;
            })->editColumn('created_date', function($records){
                return $records->created_date;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    public function getMonthOrderData(Request $request){

        $months = [];
        $year = [];
        $month = [];
        $users = DB::table('crm_customers')
            ->select('crm_customers.id','crm_customers.name','crm_customers.state','crm_customers.district','crm_customers.verified','crm_customers.grand_total','crm_customers.verified','crm_customers.managed_by')
            ->where('class', '4');
        $startmonth = $request->startmonth;
        $endmonth = $request->endmonth;
        if(!empty($startmonth) && !empty($endmonth)){

            $result = CarbonPeriod::create(date('Y-m-d',strtotime('01'.'-'.$startmonth)), '1 month', date('Y-m-d',strtotime('01'.'-'.$endmonth)));
            foreach ($result as $dt) {
                $months[] = $dt->format("Y-m");
                $year[] = $dt->format("Y");
                $month[] = $dt->format("m");
            }
        }

        $monthcustomers = CrmCustomer::where('class', '4')->get();
        foreach ($monthcustomers as $customer){
            $records = CrmCustomer::join('lead_orders','lead_orders.crm_customer_id','=','crm_customers.id')->where('pipelines_stage','won')->whereIn(DB::RAW('month(lead_orders.select_date)'), $month)->whereIn(DB::RAW('year(lead_orders.select_date)'), $year)->where('lead_orders.crm_customer_id', $customer->id)->get();
            $total =$records->count();
            CrmCustomer::where('id', $customer->id)->update(['grand_total'=>$total]);
        }

        $state = $request->state?explode(',',$request->state):false;
        $district = $request->district?explode(',',$request->district):false;
        $user = $request->user?explode(',',$request->user):false;
        $verified = $request->verified?$request->verified:'';
        $category = $request->category?explode(',',$request->category):false;
        $managedby = $request->managedby?explode(',',$request->managedby):false;

        if($user){
            $users = $users->whereIn('crm_customers.user_converted',$user);
        }
        if($state){
            $users = $users->whereIn('crm_customers.state',$state);
        } if($district){
            $users = $users->whereIn('crm_customers.district',$district);
        }if($verified){
            if($verified == "1"){
                $users = $users->where('crm_customers.verified',$verified);
            }else{
                $users = $users->where('crm_customers.verified','0');
            }
        }if($category){
            $users = $users->whereIn('crm_customers.customer_category',$category);
        }if($managedby){
            //$users = $users->whereIn('crm_customers.managed_by',$managedby);
            $users = $users->whereRaw('FIND_IN_SET(?, managed_by)', $managedby);
        }

        $datatable = Datatables::of($users);
        $datatable->editColumn('name', function($row){
            $html = '';
            if($row->verified){
                $html = $row->name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
            }else{
                $html = $row->name;
            }
            return $html;
        })->editColumn('managed_by', function($row){
            $dex = array();
            $user = explode(',', $row->managed_by);
            $u = array_pluck(DB::table('user')->whereIn('u_id', $user)->get(), 'u_name');

            $d = DB::table('customer_designation')->get();
            foreach ($d as $des) {
                if (in_array('des' . $des->id, $user)) {
                    $dex[] = $des->name;
                }
            }

            $data =  array_merge($dex,$u);
            return implode(',',$data);
        });

        foreach ($months as $key=>$month) {
            $nmonth = explode('-',$month);
            $month_name = date("F", mktime(0, 0, 0, $nmonth[1], 10));
            $datatable->addColumn($month_name, function ($row) use($key,$nmonth){
                $records = CrmCustomer::join('lead_orders','lead_orders.crm_customer_id','=','crm_customers.id')->where('pipelines_stage','won')->where('lead_orders.crm_customer_id', $row->id)->whereMonth('lead_orders.select_date','=',$nmonth[1])->whereYear('lead_orders.select_date','=',$nmonth[0])->get();
                if (!empty($records)) {
                    return $records->count();
                }
                return 'NA';
            });
        }

        $datatable->editColumn('grand_total', function($row){
            return (int)$row->grand_total;
        })->addColumn('action', function($row){
               $a="customer-data";
               $html = '<a href="customer-profile?cid='.$row->id.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
               return $html;
        })
            ->rawColumns(['name','action']);
        return   $datatable->make(true);
    }

    public function getMessageData(Request $request){

        $customer = DB::table('crm_customers')
            ->join('customer_category', 'crm_customers.customer_category', '=', 'customer_category.id')
            ->where('crm_customers.id','=',$request->cid)
            ->select(['crm_customers.*', 'crm_customers.type as type_id','customer_category.type'])
            ->first();
//        dd($customer->get());
        $number = $customer->contact_no;

        $records = DB::table('crm_message_save')->whereRaw('FIND_IN_SET(?, mobile)', $number);

        return Datatables::of($records)
            ->editColumn('user_id', function($records){
                $ids = explode(',',$records->user_id);
                $st = array_pluck(DB::table('user')->whereIn('u_id', $ids)->get(), 'u_name');
                return implode(', ', $st);
            })
            ->make(true);
    }

    public function getProjectsData(Request $request){
        $records = DB::table('crm_customer_projects')
            ->join('crm_project_type', 'crm_project_type.id', '=', 'crm_customer_projects.project_type')
            ->where('crm_customer_id', $request->cid)
            ->select(['crm_customer_projects.id', 'crm_customer_projects.project_name', 'crm_project_type.project_type', 'crm_customer_projects.project_subtype', 'sqft', 'description', 'image','edited_by']);

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light pdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="project-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/projects/'.$records->image.'"><img src="../assets/crm/images/projects/'.$records->image.'" width="30px" height="30px"/></a>';
            })
            ->editColumn('project_subtype', function($records){
                $p_subtype = explode(',',$records->project_subtype);
                $st = array_pluck(DB::table('crm_project_subtype')->whereIn('id', $p_subtype)->get(), 'project_subtype');
                return implode(', ', $st);
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }

    public function getProductsData(Request $request){
        $records = DB::table('crm_products')
            ->where('crm_customer_id', $request->cid);

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light proddelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="product-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/products/'.$records->image.'"><img src="../assets/crm/images/products/'.$records->image.'" width="30px" height="30px"/></a>';
            })
            ->editColumn('product_id', function($records){
                $p_subtype =explode(',',$records->product_id);
                $st = array_pluck(DB::table('category')->whereIn('id', $p_subtype)->get(), 'name');
                return implode(', ', $st);
            })
            ->editColumn('brands', function($records){
                $p_subtype1 = explode(',',$records->brands);
                $st1 = array_pluck(DB::table('brand')->whereIn('id', $p_subtype1)->get(), 'name');
                return implode(', ', $st1);
            })
            ->editColumn('supplier_id', function($records){
                $p_subtype2 = explode(',',$records->supplier_id);
                $st2 = array_pluck(DB::table('crm_supplier')->whereIn('id', $p_subtype2)->get(), 'name');
                return implode(', ', $st2);
            })
            ->editColumn('price_groups', function($records){
                $p_subtype3 = explode(',',$records->price_groups);
                $st3 = array_pluck(DB::table('category_groups')->whereIn('id', $p_subtype3)->get(), 'group_name');
                return implode(', ', $st3);
            })->editColumn('created_at', function($records){
               return $records->created_date;
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }



    public function getVisitsDataP(Request $request){
        $records = DB::table('crm_visits')
            ->where('crm_customer_id', $request->cid);

        return Datatables::of($records)
            ->addColumn('action', function($records) use ($request){
                $html ='<button value="'.$records->id.'" class="btn myred waves-light vdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="visit-edit?id='.$records->id.'&cid='.$request->cid.'"><i class="material-icons">edit</i></a>';
                return $html;
            })
            ->editColumn('image', function($records){

                $images = explode(',', $records->image);
                $html = '';
                foreach($images as $image) {
                    /*$imgdata = base64_decode($image);
                    $f = finfo_open();
                    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);*/
                    $path = '/assets/crm/images/visits/';
                    $html .= '<a style="margin-right: 5px;" href="'.url($path.$image).'"><img height="40px" width="40px" src="'.url($path.$image).'" /></a>';
                    /*finfo_close($f);*/
                }
                return $html;
            })
            ->editColumn('user_id', function($records){
                $ids = explode(',',$records->user_id);
                $st = array_pluck(DB::table('user')->whereIn('u_id', $ids)->get(), 'u_name');
                return implode(', ', $st);
            })
            ->rawColumns(['image', 'action'])
            ->make(true);
    }

    public function crm_ticket_report_data(){

        $records = CrmTickets::join('crm_customers', 'crm_customers.id', '=', 'crm_tickets.crm_customer_id')
            ->join('customer_category', 'customer_category.id', '=', 'crm_customers.customer_category');


        $category = (isset($_GET["category"])) ? $_GET["category"] : false;
        $type = (isset($_GET["type"])) ? ($_GET["type"]) : false;
        $priority = (isset($_GET["priority"])) ? ($_GET["priority"]) : false;
        $assign_to = (isset($_GET["assign_to"])) ? ($_GET["assign_to"]) : false;
        $status = (isset($_GET["status"])) ? ($_GET["status"]) : false;
        $date_type = (isset($_GET["date_type"])) ? ($_GET["date_type"]) : false;
        $start = (isset($_GET["startd"])) ? ($_GET["startd"]) : false;
        $end = (isset($_GET["endd"])) ? ($_GET["endd"]) : false;
        if($start == ""){
            $start = date('yyyy-mm-dd');
            $date_type = 0;
        }
        if($end == ""){
            $end = date('yyyy-mm-dd');
            $date_type = 0;
        }
        if($category){
            $records = $records->where('customer_category.id', $category);
        }
        if($type){
            $records = $records->where('crm_tickets.type', $type);
        }
        if($priority){
            $records = $records->where('crm_tickets.priority', $priority);
        }
        if($assign_to){
            if($assign_to !== 'all')
                $records = $records->whereRaw('FIND_IN_SET(?, crm_tickets.assign_to)', $assign_to);
        }
        if($status){
            $records = $records->where('crm_tickets.status', $status);
        }
        if($date_type == 1){
            $records = $records->whereBetween('crm_tickets.create_date', [$start, $end]);
        }else if($date_type == 2){
            $records = $records->whereBetween('crm_tickets.due_date', [$start, $end]);
        }
        $records = $records->select(['crm_tickets.id', 'crm_customers.name as c_name', 'crm_customers.id as cid', 'customer_category.type as cc_type', 'crm_tickets.type as t_type', 'crm_tickets.priority', 'crm_tickets.description', 'crm_tickets.assign_to','crm_tickets.image', 'crm_tickets.create_date', 'crm_tickets.due_date', 'crm_tickets.status', 'crm_customers.verified']);

        return Datatables::of($records)
            ->addColumn('action', function($records){
                $a="customer-data";
                $html ='<button value="'.$records->id.'" class="btn myred waves-light pdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button><a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="ticket-edit?id='.$records->id.'&cid='.$records->cid.'&return=crm-ticket"><i class="material-icons">edit</i></a><a href="customer-profile?cid='.$records->cid.'&link='.$a.'" class="btn myblue waves-light" style="padding: 0 1rem"><i class="material-icons">account_circle</i></a>';
                return $html;
            })
            ->editColumn('image', function($records){
                return '<a target="_blank" href="../assets/crm/images/visits/'.$records->image.'"><img src="../assets/crm/images/visits/'.$records->image.'" width="30px" height="30px"/></a>';
            })
            ->addColumn('assign_to',function($records) {

                $users = explode(',', $records->assign_to);
                $u = array_pluck(DB::table('user')->whereIn('u_id', $users)->get(), 'u_name');

                return implode(',', $u);
            })
            ->editColumn('create_date', function($records){
                return date("d, M Y", strtotime($records->create_date));
            })
            ->editColumn('due_date', function($records){
                return date("d, M Y", strtotime($records->due_date));
            })
            ->editColumn('c_name', function($records){
                $html = '';
                if($records->verified){
                    $html = $records->c_name.' <i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i>';
                }else{
                    $html = $records->c_name;
                }
                return $html;
            })
            ->rawColumns(['c_name', 'image', 'action'])
            ->make(true);
    }
    public function crmUpdateMedian(){
        crmCallMedian();
    }
    public function notification(Request $request)
    {

        define('API_ACCESS_KEY', 'AAAAd2L0SKc:APA91bHWNCFq52FHj1XBUMjD_dUn6iaCVdV0EZMKTjoSPygXCOLCEIUfLnoPHePCMPriu7l63ASRpgvPJSbXN_cErCsGacQMxUvLWsVXOv9YcZJj3hL6avefq5BdwfBug3h7aHaupKtA');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token = 'fGLGX9Kz3Vg:APA91bG3BGZkBLe3l-ZMvBc7gWb4NBqjy2Psv_wpjrWzDT0nzRvMe8hFsldeAl7okGjTJ0YNEqsTV6h42k1HbNNeue6UaFe5Ztv36I7fuxrfKLRYIEjsO-ZK-Rl0Sl2aifrczpqa5Ish';

        $notification = [
            'title' => 'Aakar360',
            'body' => 'Welcome Aakar360.',
            'icon' => 'myIcon',
            'sound' => 'mySound'
        ];
        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to' => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);


        echo $result;
    }

    public function SalesPipeline(){
        $notices = '';
        $salespipelines = DB::table('sales_pipelines')->get();


        $cfg = $this -> cfg;
        $tp = url("/assets/crm/");
        $header = $this -> header('Crm', 'index');
        $footer = $this -> footer();
        $title = 'Sales Pipelines';
        $roles = DB::table('crm_user_role')->get();
        return view('crm/sales-pipeline')-> with(compact('header', 'cfg', 'tp', 'footer', 'title', 'notices', 'roles','salespipelines'));
    }
    public function array_flatten($array) {
        if (!is_array($array)) {
            return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, array_flatten($value));
            }
            else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    public function ScoreCard(Request $request){
        $cfg = $this->cfg;
        $tp = url("/assets/crm/");
        $tp1 = url("/themes/default/admin/");
        $header = $this->header('Crm','index');
        $footer = $this->footer();
        $title = 'Score Card';
        $months = [];
        $customer_category = DB::table('customer_category')->get();
        $manageuser = DB::table('user')->get();
        $customer_desgnation = DB::table('customer_designation')->get();
        $cust_classes = DB::table('customer_class')->get();
        $type = isset($_GET['type'])?($_GET['type']):false;
        $gt = isset($_GET['graphType'])?($_GET['graphType']):false;
        $startmonth = isset($_GET['startmonth']) ? $_GET['startmonth'] : date('01-Y');
        $endmonth = isset($_GET['endmonth']) ? $_GET['endmonth'] : date('m-Y');
        $getstate=isset($_GET['state']) ? $_GET['state'] : false;
        $getdistrict=isset($_GET['district']) ? $_GET['district'] : false;
        $getuser=isset($_GET['user']) ? $_GET['user'] : false;
        if($getuser){
            $getuser = implode(',',$getuser);
        }
        if(!empty($startmonth) && !empty($endmonth)){

            $result = CarbonPeriod::create(date('Y-m-d',strtotime('01'.'-'.$startmonth)), '1 month', date('Y-m-d',strtotime('01'.'-'.$endmonth)));
            foreach ($result as $dt) {
                $months[] = $dt->format("Y-m");
            }
        }
        $totalmonths = $months?count($months):0;
        $searchtype = isset($_GET['searchtype'])?($_GET['searchtype']):false;
        $states = DB::table('states')->where('country_id','=',101)->get();
        $user = DB::table('user')->where('secure', session('crm'))->first();
        if(!checkRole($user->u_id,"mas_scr")){
            return redirect()->to('crm/index')->withErrors(['ermsg'=> 'You don\'t have access to this section.']);
        }
        /* User Wise Chart*/
        $userfilter = DB::table('user')->get();
        $designations = DB::table('customer_designation')->get();
        $users = DB::table('user');
        if(isset($_GET['desigantion'])){
            $users = $users->whereIn('user.desigantion_id',$_GET['desigantion']);

        }

        $date_data = $users->get();
        $data['user_label'] = array();
        $start_date1 = date('Y-m-d', strtotime('first day of this month'));
        $end_date1 = date('Y-m-d', strtotime('last day of this month'));

        $dates1 = array($start_date1, date('Y-m-d', strtotime($end_date1)));

        if(isset($_GET['start1']) && isset($_GET['end1'])){
            $start_date1 = date('Y-m-d 00:00:00', strtotime($_GET['start1']));
            $end_date1 = date('Y-m-d 23:59:59', strtotime($_GET['end1']));
            $dates1 = array($start_date1, date('Y-m-d', strtotime($end_date1.' +1 day')));
        }

        foreach($date_data as $key=>$dd){

            $data['user_label'][] = $dd->u_name;
            if(isset($_GET['serachcategory'])) {
                $calls = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->where('user_id', $dd->u_id)->whereIn('crm_customers.customer_category', $_GET['serachcategory'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $lead = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->where('user_id', $dd->u_id)->whereIn('crm_customers.customer_category', $_GET['serachcategory'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
                $orders = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->where([['user_id','=', $dd->u_id],['pipelines_stage','=','won']])->whereIn('crm_customers.customer_category', $_GET['serachcategory'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
            }else if(isset($_GET['lead_cust_class'])){
                $calls = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->where('user_id', $dd->u_id)->whereIn('crm_customers.class', $_GET['lead_cust_class'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $lead = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->where('user_id', $dd->u_id)->whereIn('crm_customers.class', $_GET['lead_cust_class'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
                $orders = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->where([['user_id','=', $dd->u_id],['pipelines_stage','=','won']])->whereIn('crm_customers.class', $_GET['lead_cust_class'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
            }
            else{
                $calls = DB::table('crm_call')->where('user_id', $dd->u_id)->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $lead = DB::table('lead_orders')->where('user_id', $dd->u_id)->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
                $orders = DB::table('lead_orders')->where([['user_id','=', $dd->u_id],['pipelines_stage','=','won']])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
            }
            $data['calls_dataset'][] = array_pluck($calls, 'total');
            $data['leads_dataset'][] = array_pluck($lead, 'total');
            $data['orders_dataset'][] = array_pluck($orders, 'total');

        }

        $start_date = date('Y-m-d 00:00:00', strtotime('-30 days'));
        $end_date = date('Y-m-d 23:59:59');
        $dates = array($start_date, date('Y-m-d', strtotime($end_date.' +1 day')));
        if(isset($_GET['start']) && isset($_GET['end'])){
            $start_date = date('Y-m-d 00:00:00', strtotime($_GET['start']));
            $end_date = date('Y-m-d 23:59:59', strtotime($_GET['end']));
            $dates = array($start_date, date('Y-m-d', strtotime($end_date.' +1 day')));
        }
        $data['calls'] = array_flatten($data['calls_dataset']);
        $data['leads'] = array_flatten($data['leads_dataset']);
        $data['orders'] = array_flatten($data['orders_dataset']);
        /* End User Wise Chart*/

        /*Type Wise Data Filter*/

        $types = ["Call","Lead","Order"];
        foreach ($types as $type){
            $data['typelebel'][] = $type;
        }
        if(isset($_GET['serachcategory'])) {
            $call = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->whereIn('crm_customers.customer_category', $_GET['serachcategory'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total')])->get();
            $leads = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->whereIn('crm_customers.customer_category', $_GET['serachcategory'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
            $order = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->where('pipelines_stage','=','won')->whereIn('crm_customers.customer_category', $_GET['serachcategory'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
        }else if(isset($_GET['lead_cust_class'])){
            $call = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->whereIn('crm_customers.class', $_GET['lead_cust_class'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total')])->get();
            $leads = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->whereIn('crm_customers.class', $_GET['lead_cust_class'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
            $order = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->where('pipelines_stage','=','won')->whereIn('crm_customers.class', $_GET['lead_cust_class'])->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total,select_date')])->get();
        }
        else{
            $call = DB::table('crm_call')->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total ,select_date')])->get();
            $leads = DB::table('lead_orders')->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total')])->get();
            $order = DB::table('lead_orders')->where('pipelines_stage','=','won')->whereBetween(DB::raw('date(select_date)'), [$dates1[0], $dates1[1]])->select([DB::raw('COUNT(*) as total')])->get();

        }
        $putzero = [0];
        $totalCalls = array_pluck($call, 'total');
        $data['totalCalls'] = array_merge($totalCalls, $putzero, $putzero);
        $totalLead = array_pluck($leads, 'total');
        $data['totalLead'] = array_merge($putzero, $totalLead, $putzero);
        $totalOrder = array_pluck($order, 'total');
        $data['totalOrder'] = array_merge($putzero,$putzero,$totalOrder);

        /*End Type Wise Data Filter*/
        /* Score Card For No Order    */
        $no_order_reasons = DB::table('no_order')->get();
        $no_order_start_date = date('Y-m-d', strtotime('today - 28 days'));
        $no_order_end_date = date('Y-m-d',strtotime('+1 day'));
        $no_order_dates = array($no_order_start_date, date('Y-m-d', strtotime($no_order_end_date)));
        if(isset($_GET['start2']) && isset($_GET['end2'])){
            $no_order_start_date = date('Y-m-d', strtotime($_GET['start2']));
            $no_order_end_date = date('Y-m-d', strtotime($_GET['end2']));
            $no_order_dates = array($no_order_start_date, date('Y-m-d', strtotime($no_order_end_date.' +1 day')));
        }

        foreach($no_order_reasons as $key=>$value){
            $data['reason_label'][] = $value->reason;
            $comcall = DB::table('crm_call')->where('crm_call.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
            $leadcall = DB::table('lead_orders')->where('lead_orders.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
            if(isset($_GET['category'])) {
                $comcall = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->whereIn('crm_customers.customer_category', $_GET['category'])->where('crm_call.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $leadcall = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->whereIn('crm_customers.customer_category', $_GET['category'])->where('lead_orders.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
            } if(isset($_GET['cust_class'])){
                $comcall = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->whereIn('crm_customers.class', $_GET['cust_class'])->where('crm_call.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $leadcall = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->whereIn('crm_customers.class', $_GET['cust_class'])->where('lead_orders.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
            } if(isset($_GET['user_filter'])){
                $comcall = DB::table('crm_call')->where('crm_call.no_order_reason', '=', $value->id)->whereIn('user_id',$_GET['user_filter'])->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $leadcall = DB::table('lead_orders')->where('lead_orders.no_order_reason', '=', $value->id)->whereIn('user_id',$_GET['user_filter'])->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
            } if(isset($_GET['category']) && isset($_GET['cust_class'])){
                $comcall = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->whereIn('crm_customers.customer_category', $_GET['category'])->whereIn('crm_customers.class', $_GET['cust_class'])->where('crm_call.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $leadcall = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->whereIn('crm_customers.customer_category', $_GET['category'])->whereIn('crm_customers.class', $_GET['cust_class'])->where('lead_orders.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
            }
            if(isset($_GET['category']) && isset($_GET['user_filter'])){
                $comcall = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->whereIn('crm_customers.customer_category', $_GET['category'])->whereIn('user_id',$_GET['user_filter'])->where('crm_call.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $leadcall = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->whereIn('crm_customers.customer_category', $_GET['category'])->whereIn('user_id',$_GET['user_filter'])->where('lead_orders.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();

            }
            if(isset($_GET['cust_class']) && isset($_GET['user_filter'])){
                $comcall = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->whereIn('crm_customers.class', $_GET['cust_class'])->whereIn('user_id',$_GET['user_filter'])->where('crm_call.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $leadcall = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->whereIn('crm_customers.class', $_GET['cust_class'])->whereIn('user_id',$_GET['user_filter'])->where('lead_orders.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();

            } if(isset($_GET['category']) && isset($_GET['cust_class']) && isset($_GET['user_filter'])){
                $comcall = DB::table('crm_call')->join('crm_customers', 'crm_customers.id', '=', 'crm_call.crm_customer_id')->whereIn('crm_customers.class', $_GET['cust_class'])->whereIn('user_id',$_GET['user_filter'])->whereIn('crm_customers.customer_category', $_GET['category'])->where('crm_call.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();
                $leadcall = DB::table('lead_orders')->join('crm_customers', 'crm_customers.id', '=', 'lead_orders.crm_customer_id')->whereIn('crm_customers.class', $_GET['cust_class'])->whereIn('user_id',$_GET['user_filter'])->whereIn('crm_customers.customer_category', $_GET['category'])->where('lead_orders.no_order_reason', '=', $value->id)->whereBetween(DB::raw('date(select_date)'), [$no_order_dates[0], $no_order_dates[1]])->select([DB::raw('COUNT(*) as total')])->get();

            }


            $data['comtotal_dataset'][] = array_pluck($comcall, 'total');
            $data['leadtotal_dataset'][] = array_pluck($leadcall, 'total');

        }
        //dd($data['comtotal_dataset']);
        $data['comtotal'] = array_flatten($data['comtotal_dataset']);
        $data['leadtotal'] = array_flatten($data['leadtotal_dataset']);


        /* End Score Card For No Order   */
        return view('crm/score-card')->with(compact('header','cfg','tp','tp1','footer', 'title', 'data', 'start_date', 'end_date','start_date1', 'end_date1','designations','customer_category','cust_classes','userfilter','searchtype','no_order_start_date','no_order_end_date','states','months','totalmonths','customer_desgnation','manageuser'));
    }
}

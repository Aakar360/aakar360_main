<?php

namespace App\Http\Controllers\Retailer;

use App\AdviceCategory;
use App\Banner;
use App\BestForCategory;
use App\Blocs;
use App\Blog;
use App\Brand;
use App\Browser;
use App\Category;
use App\City;
use App\CityWise;
use App\Config;
use App\Countries;
use App\Country;
use App\Currency;
use App\Customer;
use App\CustomerCache;
use App\DefaultImage;
use App\DesignCategory;
use App\FeaturedProducts;
use App\Filter;
use App\Footer;
use App\Http\Controllers\FrontendBaseController;
use App\ImageInspiration;
use App\ImageLike;
use App\Lang;
use App\LayoutWishlist;
use App\Link;
use App\ManufacturingHubs;
use App\MegaMenuImage;
use App\Menu;
use App\Os;
use App\PasswordReset;
use App\Payments;
use App\PhRelations;
use App\ProductDiscount;
use App\ProductFaq;
use App\Products;
use App\ProductVariant;
use App\ProductWishlist;
use App\RateRequests;
use App\Referrer;
use App\RegisterImage;
use App\Review;
use App\ServiceCategory;
use App\ServiceWishlist;
use App\SpecialRequirements;
use App\Style;
use App\Tracking;
use App\Unit;
use App\UserProject;
use App\Visitor;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Node\Directory;
use Image;
use Illuminate\Support\Facades\Session;
use Stripe\Product;

//use Auth;

class RetailerProducts extends FrontendBaseController
{
    public function products($cat = '', $sub = false)
    {

        $storearray = array();
        $data['hub'] = Cookie::get('aakar_hub');
        if($cat == ''){
            abort(404);
        }
        $data['show_cities'] = false;
        $preferences = null;
        if(isset($_POST['cities'])){
            setcookie('cities', $_POST['cities'], time() + (86400 * 30));
            return redirect('r/index');
        }
        if(isset($_COOKIE['cities'])){
            $preferences = explode(',', $_COOKIE['cities']);
        }else{
            $preferences = null;
        }
        if($preferences === null){
            $data['show_cities'] = true;
        }
        $data['rem_pre'] = 0;
        if($preferences === null){
            $data['pcss'] = CityWise::get();
            $storearray = array();
            foreach ($data['pcss'] as $cvalues){
                $cpdata = json_decode($cvalues->prices);
                $storearray[] .= $cpdata[0]->product;
            }
        }else{

            $data['pcss'] = CityWise::whereIn('city', $preferences)->get();
            foreach ($data['pcss'] as $cvalues){
                $cpdata = json_decode($cvalues->prices);
                $storearray[] .= $cpdata[0]->product;
            }
        }
        $data['show_filter'] = true;
        if(customer('user_type') == ''){
            $data['show_filter'] = false;
        }
        $data['show_products'] = false;
        if($sub){
            $cat = $sub;
            $data['show_filter'] = true;
        }
        $data['categ'] = Category::where('path', $cat)->first();
        if($data['categ'] === null){
            abort(404);
        }
        $pdata = Category::where('parent', $data['categ']->id)->get();
        if(count($pdata) == 0){
            $data['show_products'] = true;
        }
        $data['header'] = $this->retailHeader(translate('Products'),false,true);
        $data['price'] = array();
        $where = array();
        $data['bids'] = array();
        if (isset($_GET['min']) && isset($_GET['max']))
        {
            if(customer('user_type') != 'institutional') {
                $where['price'] = "price BETWEEN '" . escape($_GET['min']) . "' AND '" . escape($_GET['max']) . "'";
            }else{}
        }
        if (isset($_GET['brands']))
        {
            $data['bids'] = array_keys($_GET['brands']);
        }
        if (!empty($catlink))
        {
            $where['search'] = "(category = ".$catlink.")";
        }
        if ($cat)
        {
            $check = Category::where('id',$data['categ']->id)->get();
            if (count($check) == 0){
                abort(404);
            }
            $data['category'] = Category::where('id', $data['categ']->id)->first();
            $categories[] = $data['category']->id;
            if ($data['category']->parent == 0){
                $childs = Category::where('parent',$data['category']->id)->get();
                foreach ($childs as $child){
                    $categories[] = $child->id;
                }
            }
            $where['cat'] = "category IN (".implode(',',$categories).")";
        }
        $tpbrands = '';
        $data['suggested'] = false;
        $data['top_brands'] = [];
        if($data['categ']){
            $data['category'] = Category::where('id', $data['categ']->id)->first();
            $tpbrands =  explode(',',$data['category']->brands);
            if($data['category']->brands !== '') {
                $data['top_brands'] = Brand::whereIn('id',$tpbrands)->orderBy('name','ASC')->get();
            }
            $sg = FeaturedProducts::where('category_id',$data['categ']->id)->first();
            $sug_prods = '';
            $data['suggested'] = [];
            if($sg){
                $sug_prods = explode(',',$sg->product_id);
                $data['suggested'] = Products::whereIn('id',$sug_prods)->get();
            }
            $data['product_categories'] = Category::where('parent',$data['categ']->id)->orderBy('name','ASC')->get();
            $data['procount'] = Category::where('parent',$data['categ']->id)->count();
        }
        $categoriescon = Category::where('parent',0)->orderBy('name','ASC')->count();
        if($categoriescon !== '') {
            $data['cats'] = Category::where('parent',0)->orderBy('name','ASC')->get();
        }else{
            $data['cats'] = 'Data Not Available';
        }
        $data['faqs'] = ProductFaq::whereRaw('FIND_IN_SET("'.$data['categ']->id.'",category)')->where('section','retail')->orderBy('id','DESC')->take(5)->get();
        $data['ladvices'] = Blog::whereRaw('FIND_IN_SET("'.$data['categ']->id.'",pro_cat)')->where('section','retail')->orderBy('id','DESC')->take(4)->get();
        $data['tadvices'] = Blog::whereRaw('FIND_IN_SET("'.$data['categ']->id.'",pro_cat)')->where('section','retail')->orderBy('visits','DESC')->take(4)->get();
        $data['best'] = BestForCategory::where('category',$data['categ']->id)->orderBy('id','DESC')->get();
        $data['brands'] = array();
        if($data['categ']->brands){
            $tpbrands =  explode(',',$data['categ']->brands);
            $data['brands'] = Brand::whereIn('id',$tpbrands)->orderBy('id','DESC')->get();
        }
        $data['filters'] = [];
        $fc = Category::where('id', $data['categ']->id)->first();
        $cfc = $fc->filter;
        //dd($cfc);
        if($cfc !== '') {
            $ccx = explode( ',', $cfc);
            foreach ($ccx as $cx) {
                $data['filters'] = Filter::where('id',$cx)->get();
            }
        } else {
            $data['filters'] = '';
        }
        $cid[] = $data['categ']->id;
        $c = array();
        $cont = true;
        $temp = $cid;
        while($cont){
            $c = array_pluck(Category::whereIn('parent', $temp)->get(), 'id');
            if(count($c)){
                $cid = array_merge($cid, $c);
                $temp = $c;
            }else{
                $cont = false;
            }
        }
        $products = Products::whereIn('category', $cid)->whereIn('id',$storearray);
        $data['link'] = Link::where('page', 'Products')->where('link', '<>', '')->first();
        $data['price']['set_min'] = 0;
        $data['price']['set_max'] = 0;
        $p_min = DB::select("SELECT MIN(price) as min_price, MAX(price) as max_price FROM products where category IN (".implode(',', $cid).")");
        if(count($p_min) > 0){
            $data['price']['min'] = $p_min[0]->min_price;
            $data['price']['set_min'] = $p_min[0]->min_price;
            $data['price']['max'] = $p_min[0]->max_price;
            $data['price']['set_max'] = $p_min[0]->max_price;
        }else{
            $data['price']['min'] = 0;
            $data['price']['set_min'] = 0;
            $data['price']['max'] = 0;
        }
        if(isset($_GET['min'])){
            $data['price']['set_min'] = $_GET['min'];
            if(customer('user_type') != 'institutional')
                $products = $products->where('price', '>=', $_GET['min']);
        }
        if(isset($_GET['max'])){
            $data['price']['set_max'] = $_GET['max'];
            if(customer('user_type') != 'institutional')
                $products = $products->where('price', '<=', $_GET['max']);
        }
        if(count($data['bids']) > 0){
            $products = $products->whereIn('brand_id', $data['bids']);
        }

        $data['products'] = $products->get();
        $data['footer'] = $this->footer();
        $data['hubs'] = ManufacturingHubs::orderBy('title')->get();
        $data['vtype']=Session::get('aakar360.visitor_type');
        $data['inst'] = Customer::get();
        $data['preferences'] = $preferences;
        return view('retailer/products')->with($data);
    }

    public function product(Request $request, $product_id)
    {
        $data['hub'] = Cookie::get('aakar_hub');
        $data['recent_viewed'] = 0;
        $data['variants'] = false;
        $cookie_name = "sellerkit.recent";
        if($request->session()->get($cookie_name) == null){
            $request->session()->put($cookie_name, explode('-',$product_id)[0]);
        }else{
            $data['recent_viewed'] = Products::where('id',$request->session()->get($cookie_name))->get();
            $request->session()->put($cookie_name, $request->session()->get($cookie_name).','.explode('-',$product_id)[0]);
        }
        $check = Products::where('id',explode('-',$product_id)[0])->get();
        if (count($check) == 0){
            abort(404);
        }
        $exp_array = explode('-',$product_id);
        $pid = explode('-',$product_id)[0];
        if(count($exp_array)==2){
            $data['product'] = DB::select("SELECT * FROM `products` as p1 WHERE p1.sku in (SELECT p2.sku FROM products as p2 WHERE p2.id=$pid) and p1.sale in (Select min(p3.sale) from products as p3 where p3.sku=p1.sku)")[0];
        }
        else{
            $data['product'] = Products::where('id',$pid)->first();
        }
        $data['cat'] = Category::where('id',$data['product']->category)->first();
        $prid = $data['product']->id;
        $data['variants'] = ProductVariant::where('display','1')->where('product_id',$prid)->get();
        $data['dis_var'] = ProductVariant::where('display','1')->where('product_id',$prid)->get();
        $data['bulk_discounts'] = ProductDiscount::where('product_id',$prid)->orderBy('quantity','ASC')->get();
        $data['total_ratings'] = Review::where('active','1')->where('product',$prid)->count();
        $data['rating'] = 0;
        if ($data['total_ratings'] > 0){
            $rating_summ = Review::where('active','1')->where('product',$prid)->sum('rating');
            $data['rating'] = $rating_summ / $data['total_ratings'];
        }
        $data['total_reviews'] = Review::where('active','1')->where('review','<>','')->where('product',$prid)->count();
        $data['ratinga'] = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM reviews WHERE active = '1' AND product = '".$prid."'")[0];
        $data['total_rating'] =  $data['ratinga']->total_rating;
        $data['total_user'] = $data['ratinga']->total_user;
        if($data['total_rating']==0){
            $data['avg_rating'] = 0;
        }
        else{
            $data['avg_rating'] = round($data['total_rating']/$data['total_user'],1);
        }
        $data['rating1']= Review::where('rating','1')->where('product',$prid)->count('id','rating1_user');
        $data['rating2']= Review::where('rating','2')->where('product',$prid)->count('id','rating2_user');
        $data['rating3']= Review::where('rating','3')->where('product',$prid)->count('id','rating3_user');
        $data['rating4']= Review::where('rating','4')->where('product',$prid)->count('id','rating4_user');
        $data['rating5']= Review::where('rating','5')->where('product',$prid)->count('id','rating5_user');
        $data['images'] = explode(',',$data['product']->images);
        $title = $data['product']->title;
        $desc = $data['product']->text;
        $data['specification'] = $data['product']->specification;
        $data['header'] = $this->retailHeader(translate($title),$desc,true);
        $data['reviews'] = Review::where('active','1')->where('product',$prid)->orderBy('time','DESC')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        $data['related_products'] = getRelatedProducts($data['product']->category, $data['product']->id);
        $get_sku = Products::where('id',$pid)->select('sku')->first();
        $data['sellers'] = DB::table('products')->join('customers', 'customers.id', '=', 'products.added_by')->where('products.sku', $get_sku->sku)
            ->select('customers.*','products.*')->take('5')->get();
        if(customer('user_type') == 'institutional'){
            $data['faqs'] = ProductFaq::where('section','institutional')->where('status','1')->orderBy('id','DESC')->get();
        }else{
            ProductFaq::where('section','retail')->where('status','1')->orderBy('id','DESC')->get();
        }
        $data['product'] = Products::where('id', '=', $data['product']->id)->first();
        $data['cat'] = Category::where('id', '=', $data['product']->category)->first();
        $data['wunits'] = Unit::whereIn('id', explode(',', $data['cat']->weight_unit))->get();
        $data['link'] = Link::where('page', 'Single Product')->first();
        $data['brands'] = Brand::get();
        $data['vtype']=Session::get('aakar360.visitor_type');
        $data['cities'] = City::where('is_available','1')->orderBy('name','ASC')->get();
        return view('retailer/product')->with($data);
    }

    public function checkPincode(Request $request){
        $pincode = $request->pincode;
        $html = '';
        if(!empty($pincode)){
            $city = City::whereRaw('FIND_IN_SET('.$pincode.',pincodes)')->first();
            if($city != null){
                $html = '<div class="alert alert-success">
                            Delivery is available in this location.
                        </div>';
                return $html;
            }else {
                $html = '<div class="alert alert-warning">
                            Delivery not available in this location.
                        </div>';
                return $html;
            }
        }
    }

    public function selectCity(Request $request){
        $cityid = $request->cityid;
        setcookie('cities', $cityid, time() + (86400 * 30));
        return redirect('r/index');
    }
}

<?php

namespace App\Http\Controllers\Institutional;

use App\AdviceCategory;
use App\Banner;
use App\BestForCategory;
use App\Blocs;
use App\Blog;
use App\Brand;
use App\Browser;
use App\Category;
use App\City;
use App\CityWise;
use App\Config;
use App\CounterOffer;
use App\Countries;
use App\Country;
use App\CreditCharges;
use App\Currency;
use App\Customer;
use App\CustomerCache;
use App\DefaultImage;
use App\DesignCategory;
use App\FeaturedProducts;
use App\Filter;
use App\Footer;
use App\Http\Controllers\FrontendBaseController;
use App\Http\Middleware\Institutional;
use App\ImageInspiration;
use App\ImageLike;
use App\Lang;
use App\LayoutWishlist;
use App\Link;
use App\ManufacturingHubs;
use App\MegaMenuImage;
use App\Menu;
use App\Os;
use App\PasswordReset;
use App\Payments;
use App\PhRelations;
use App\ProductDiscount;
use App\ProductFaq;
use App\Products;
use App\ProductVariant;
use App\ProductWishlist;
use App\RateRequests;
use App\Referrer;
use App\RegisterImage;
use App\RequestQuotation;
use App\Review;
use App\ServiceCategory;
use App\ServiceWishlist;
use App\SpecialRequirements;
use App\Style;
use App\Tracking;
use App\Unit;
use App\UserProject;
use App\Visitor;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Node\Directory;
use Image;
use Illuminate\Support\Facades\Session;
use Stripe\Product;

//use Auth;

class InstitutionalProducts extends FrontendBaseController
{
    public function products($cat = '', $sub = false)
    {


        if($cat == ''){
            abort(404);
        }

        if(isset($_POST['hub'])){

            setcookie('aakar_hub', $_POST['hub'], time()+60*60*24*365);
            if($sub){
                return redirect('i/products/'.$cat.'/'.$sub);
            }
            return redirect('i/products/'.$cat);
        }
        $storearray = array();
        $data['show_cities'] = false;
        $preferences = null;
        if(institutional('id') !== ''){
            if(isset($_POST['cities'])){
                setcookie('cities', $_POST['cities'], time() + (86400 * 30));
                return redirect('i/index');
            }
            if(isset($_COOKIE['cities'])){
                $preferences = explode(',', $_COOKIE['cities']);
            }else{
                $preferences = null;
            }
            if($preferences === null){
                $data['show_cities'] = true;
            }
        }else{
            if(isset($_POST['cities'])){
                setcookie('cities', $_POST['cities'], time() + (86400 * 30));
                return redirect('i/index');
            }
            if(isset($_COOKIE['cities'])){
                $preferences = explode(',', $_COOKIE['cities']);
            }else{
                $preferences = null;
            }
            if($preferences === null){
                $data['show_cities'] = true;
            }
        }
        $data['rem_pre'] = 0;
        if($preferences === null){
            $data['pcss'] = CityWise::get();
        }else{
            $data['pcss'] = CityWise::whereIn('city', $preferences)->get();
            foreach ($data['pcss'] as $cvalues){
                $cpdata = json_decode($cvalues->prices);
                $storearray[] .= $cpdata[0]->product;
            }
        }
        $data['show_filter'] = true;
        if(institutional('user_type') == ''){
            $data['show_filter'] = false;
        }
        $data['show_products'] = false;
        if($sub){
            $cat = $sub;
            $data['show_filter'] = true;
        }
        $data['categ'] = Category::where('path', $cat)->first();
        if($data['categ'] === null){
            abort(404);
        }
        $pdata = Category::where('parent', $data['categ']->id)->get();

        if(count($pdata) == 0){
            $data['show_products'] = true;
        }
        $data['header'] = $this->institutionalHeader(translate('Products'),false,true);
        $data['price'] = array();
        $where = array();
        $data['bids'] = array();
        if (isset($_GET['min']) && isset($_GET['max']))
        {
            if(institutional('user_type') != 'institutional') {
                $where['price'] = "price BETWEEN '" . escape($_GET['min']) . "' AND '" . escape($_GET['max']) . "'";
            }else{}
        }
        if (isset($_GET['brands']))
        {
            $data['bids'] = array_keys($_GET['brands']);
        }
        if (!empty($catlink))
        {
            $where['search'] = "(category = ".$catlink.")";
        }
        if ($cat)
        {
            $check = Category::where('id',$data['categ']->id)->get();

            if (count($check) == 0){
                abort(404);
            }
            $data['category'] = Category::where('id', $data['categ']->id)->first();
            $categories[] = $data['category']->id;
            if ($data['category']->parent == 0){
                $childs = Category::where('parent',$data['category']->id)->get();
                foreach ($childs as $child){
                    $categories[] = $child->id;
                }
            }
            $where['cat'] = "category IN (".implode(',',$categories).")";
        }
        $tpbrands = '';
        $data['suggested'] = false;
        $data['top_brands'] = [];
        if($data['categ']){
            $data['category'] = Category::where('id', $data['categ']->id)->first();
            $tpbrands =  explode(',',$data['category']->brands);
            if($data['category']->brands !== '') {
                $data['top_brands'] = Brand::whereIn('id',$tpbrands)->orderBy('name','ASC')->get();
            }
            $sg = Products::where('category',$data['categ']->id)->first();

            $sug_prods = '';
            $data['suggested'] = [];
            if($sg){
                $sug_prods = explode(',',$sg->product_id);
                $data['suggested'] = Products::where('category',$data['categ']->id)->get();
            }
            $data['product_categories'] = Category::where('parent',$data['categ']->id)->orderBy('name','ASC')->get();
            $data['procount'] = Category::where('parent',$data['categ']->id)->count();
        }
        $categoriescon = Category::where('parent',0)->orderBy('name','ASC')->count();
        if($categoriescon !== '') {
            $data['cats'] = Category::where('parent',0)->orderBy('name','ASC')->get();
        }else{
            $data['cats'] = 'Data Not Available';
        }
        $data['faqs'] = ProductFaq::whereRaw('FIND_IN_SET("'.$data['categ']->id.'",category)')->where('section','retail')->orderBy('id','DESC')->take(5)->get();
        $data['ladvices'] = Blog::whereRaw('FIND_IN_SET("'.$data['categ']->id.'",pro_cat)')->where('section','retail')->orderBy('id','DESC')->take(4)->get();
        $data['tadvices'] = Blog::whereRaw('FIND_IN_SET("'.$data['categ']->id.'",pro_cat)')->where('section','retail')->orderBy('visits','DESC')->take(4)->get();
        $data['best'] = BestForCategory::where('category',$data['categ']->id)->orderBy('id','DESC')->get();
        $data['brands'] = array();
        if($data['categ']->brands){
            $tpbrands =  explode(',',$data['categ']->brands);
            $data['brands'] = Brand::whereIn('id',$tpbrands)->orderBy('id','DESC')->get();
        }
        $data['filters'] = [];
        $fc = Category::where('id', $data['categ']->id)->first();
        $cfc = $fc->filter;
        if($cfc !== '') {
            $ccx = explode( ',', $cfc);
            foreach ($ccx as $cx) {
                $data['filters'] = Filter::where('id',$cx)->get();
            }
        } else {
            $data['filters'] = '';
        }
        $cid[] = $data['categ']->id;
        $c = array();
        $cont = true;
        $temp = $cid;
        while($cont){
            $c = array_pluck(Category::whereIn('parent', $temp)->get(), 'id');
            if(count($c)){
                $cid = array_merge($cid, $c);
                $temp = $c;
            }else{
                $cont = false;
            }
        }
        $products = Products::whereIn('category', $cid)->whereIn('id',$storearray);
        $data['link'] = Link::where('page', 'Products')->where('link', '<>', '')->first();
        $data['price']['set_min'] = 0;
        $data['price']['set_max'] = 0;
        $p_min = DB::select("SELECT MIN(price) as min_price, MAX(price) as max_price FROM products where category IN (".implode(',', $cid).")");
        if(count($p_min) > 0){
            $data['price']['min'] = $p_min[0]->min_price;
            $data['price']['set_min'] = $p_min[0]->min_price;
            $data['price']['max'] = $p_min[0]->max_price;
            $data['price']['set_max'] = $p_min[0]->max_price;
        }else{
            $data['price']['min'] = 0;
            $data['price']['set_min'] = 0;
            $data['price']['max'] = 0;
        }
        if(isset($_GET['min'])){
            $data['price']['set_min'] = $_GET['min'];
            if(institutional('user_type') != 'institutional')
                $products = $products->where('price', '>=', $_GET['min']);
        }
        if(isset($_GET['max'])){
            $data['price']['set_max'] = $_GET['max'];
            if(institutional('user_type') != 'institutional')
                $products = $products->where('price', '<=', $_GET['max']);
        }
        if(count($data['bids']) > 0){
            $products = $products->whereIn('brand_id', $data['bids']);
        }
        $data['hub'] = 0;
        if(isset($_COOKIE['aakar_hub'])) {
            $data['hub'] = $_COOKIE['aakar_hub'];
        }
        $data['products'] = $products->get();
        $data['footer'] = $this->footer();
        $data['hubs'] = ManufacturingHubs::orderBy('title')->get();
        $data['vtype']=Session::get('aakar360.visitor_type');
        $data['inst'] = Customer::get();
        return view('frontInstitutional/products')->with($data);
    }

    public function product(Request $request, $product_id)
    {

        $data['hub'] = 0;
        if(isset($_COOKIE['aakar_hub'])) {
            $data['hub'] = $_COOKIE['aakar_hub'];
        }
        $data['recent_viewed'] = 0;
        $variants = false;
        $cookie_name = "sellerkit.recent";
        if($request->session()->get($cookie_name) == null){
            $request->session()->put($cookie_name, explode('-',$product_id)[0]);
        }else{
            $data['recent_viewed'] = Products::where('id',$request->session()->get($cookie_name))->get();
            $request->session()->put($cookie_name, $request->session()->get($cookie_name).','.explode('-',$product_id)[0]);
        }
        $check = Products::where('id',explode('-',$product_id)[0])->get();
        if (count($check) == 0){
            abort(404);
        }
        $exp_array = explode('-',$product_id);
        $pid = explode('-',$product_id)[0];
        if(count($exp_array)==2){
            $data['product'] = DB::select("SELECT * FROM `products` as p1 WHERE p1.sku in (SELECT p2.sku FROM products as p2 WHERE p2.id=$pid) and p1.sale in (Select min(p3.sale) from products as p3 where p3.sku=p1.sku)")[0];
        }
        else{
            $data['product'] = Products::where('id',$pid)->first();
        }
        $data['cat'] = Category::where('id',$data['product']->category)->first();
        if(empty($data['hub']) && institutional('user_type') == 'institutional'){
            return redirect('i/products/'.$data['cat']->path);
        }
        $prid = $data['product']->id;
        $data['variants'] = ProductVariant::where('display','1')->where('product_id',$prid)->get();
        $data['creditvariant'] = ProductVariant::where('display','1')->where('product_id',$prid)->first();
        $data['dis_var'] = ProductVariant::where('display','1')->where('product_id',$prid)->get();
        $data['bulk_discounts'] = ProductDiscount::where('product_id',$prid)->orderBy('quantity','ASC')->get();
        $data['total_ratings'] = Review::where('active','1')->where('product',$prid)->count();
        $data['rating'] = 0;
        if ($data['total_ratings'] > 0){
            $rating_summ = Review::where('active','1')->where('product',$prid)->sum('rating');
            $data['rating'] = $rating_summ / $data['total_ratings'];
        }
        $data['total_reviews'] = Review::where('active','1')->where('review','<>','')->where('product',$prid)->count();
        $data['ratinga'] = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM reviews WHERE active = '1' AND product = '".$prid."'")[0];
        $data['total_rating'] =  $data['ratinga']->total_rating;
        $data['total_user'] = $data['ratinga']->total_user;
        if($data['total_rating']==0){
            $data['avg_rating'] = 0;
        }
        else{
            $data['avg_rating'] = round($data['total_rating']/$data['total_user'],1);
        }
        $data['rating1']= Review::where('rating','1')->where('product',$prid)->count('id','rating1_user');
        $data['rating2']= Review::where('rating','2')->where('product',$prid)->count('id','rating2_user');
        $data['rating3']= Review::where('rating','3')->where('product',$prid)->count('id','rating3_user');
        $data['rating4']= Review::where('rating','4')->where('product',$prid)->count('id','rating4_user');
        $data['rating5']= Review::where('rating','5')->where('product',$prid)->count('id','rating5_user');
        $data['images'] = explode(',',$data['product']->images);
        $title = $data['product']->title;
        $desc = $data['product']->text;
        $data['specification'] = $data['product']->specification;
        $data['header'] = $this->institutionalHeader(translate($title),$desc,true);
        $data['reviews'] = Review::where('active','1')->where('product',$prid)->orderBy('time','DESC')->get();
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['footer'] = $this->footer();
        $data['related_products'] = getRelatedProducts($data['product']->category, $data['product']->id);
        $get_sku = Products::where('id',$pid)->select('sku')->first();
        $data['sellers'] = DB::table('products')->join('customers', 'customers.id', '=', 'products.added_by')->where('products.sku', $get_sku->sku)
            ->select('customers.*','products.*')->take('5')->get();
        $data['faqs'] = ProductFaq::where('section','institutional')->where('status','1')->orderBy('id','DESC')->get();
        $data['product'] = Products::where('id', '=', $data['product']->id)->first();
        $data['cat'] = Category::where('id', '=', $data['product']->category)->first();
        $data['wunits'] = Unit::whereIn('id', explode(',', $data['cat']->weight_unit))->get();
        $data['link'] = Link::where('page', 'Single Product')->first();
        $data['brands'] = Brand::get();
        $data['vtype']=Session::get('aakar360.visitor_type');
        $data['cities'] = City::where('is_available','1')->orderBy('name','ASC')->get();
        $data['projects'] = UserProject::all();
        $data['units'] = Unit::all();
        $data['creditCharges'] = CreditCharges::all();
        return view('frontInstitutional/product')->with($data);
    }

    public function checkPincode(Request $request){
        $pincode = $request->pincode;
        $html = '';
        if(!empty($pincode)){
            $city = City::whereRaw('FIND_IN_SET('.$pincode.',pincodes)')->first();
            if($city != null){
                $html = '<div class="alert alert-success">
                            Delivery is available in this location.
                        </div>';
                return $html;
            }else {
                $html = '<div class="alert alert-warning">
                            Delivery not available in this location.
                        </div>';
                return $html;
            }
        }
    }

    public function selectCity(Request $request){
        $cityid = $request->cityid;
        setcookie('cities', $cityid, time() + (86400 * 30));
        return redirect('r/index');
    }

    public function requestQuotation(Request $request){
        $id = false;
        $req = json_decode($request->variant);
        $other='';
        $location ='';
        $quotation = array();
        if($request->other_option){
            if($request->other_option == ''){
                $other = '';
            }
            else{
                $other = $request->other_option;
            }
        }
        if($request->location){
            if($request->location == ''){
                $location = '';
            }
            else{
                $location = $request->location;
            }
        }
        $vars = stripslashes($request->variants);
        $xx = 0;
        if($request->met == 'bybasic'){
            $bids = explode(',', $request->b_id);
            $var_data = json_decode(stripcslashes($request->variants));

            foreach($bids as $bid){
                $varsx = array();
                $br = DB::table('products')->where('id', $bid)->first();
                if($br !== null){
                    $pricing = getInstitutionalPrice($br->id, Institutional('sid'), $request->hub_id, false, false);
                    foreach($var_data as $vd){
                        $varsx[] = array(
                            'id' => $bid,
                            'title' => $br->title,
                            'quantity' => $vd->quantity,
                            'unit' => $vd->unit,
                            'hub_id' => $vd->hub_id,
                            'basic_price' => (is_numeric($pricing['price'])) ? round($pricing['price']) : $pricing['price'],
                            'action' => 0,
                            'var_price' => 0,
                            'new_price' => 0,
                        );
                    }
                }
                $quotation[] = array(
                    'cat_id' => $request->c_id,
                    'hub_id' => $request->hub_id,
                    'product_id' => $request->id,
                    'brand_id' => $bid,
                    'payment_option' => $request->payment_option,
                    'other_option' => $other,
                    'method' => $request->met,
                    'location' => $location,
                    'variants' => json_encode($varsx),
                    'user_id' => $request->user_id,
                    'credit_charge_id' => $request->credit_charge_id
                );
                $xx++;
            }
        }else{
            $bids = explode(',', $request->b_id);
            $var_data = [];
            $variant = explode(',',$request->variants);
            $quantity = explode(',',$request->q);
            $unit = explode(',',$request->units);
            $request->title;

            $var_data = (array)json_decode(stripcslashes($request->variants));

            foreach($bids as $key=>$bid){
                $varsx = array();
                $br = Products::where('id', $bid)->first();
                $brId = false;
                if($br !== null){
                    if($br->id){
                        $brId = $br->id;
                    }
                }
                foreach(explode(',',$request->title) as $key=>$vd){

                    $varId = false;
                    if($variant[$key]){
                        $varId = $variant[$key];
                    }
                    $varsx[$bid][] = array(
                        "id"=>$variant[$key],
                        "title"=>$vd,
                        "quantity"=>$quantity[$key],
                        "unit"=>$unit[$key],
                        "action"=>'',
                        "var_price"=>'',
                        "price_detail"=>getInstitutionalPrice($brId, Institutional('sid'), $request->hub_id, $varId, false, false),
                    );
                }
                //dd($varsx);
                $quotation[] = array(
                    'cat_id' => $request->c_id,
                    'hub_id' => $request->hub_id,
                    'product_id' => $request->id,
                    'brand_id' => $bid,
                    'payment_option' => $request->payment_option,
                    'other_option' => $other,
                    'method' => $request->met,
                    'location' => $location,
                    'variants' => json_encode($varsx),
                    'user_id' => $request->user_id,
                    'credit_charge_id' => $request->credit_charge_id
                );
                $xx++;
            }
        }
        DB::table('request_quotation')->insert($quotation);

        return response()->json(['message' => 'entry successfully in request quotation ', 'code' => 200], 200);
    }

    public function rfqRequestQuotation(Request $request){

        $id = false;
        $req = json_decode($request->variant);
        $other='';
        $location ='';
        $quotation = array();
        if(isset($request->other_option)){
            if($request->other_option == ''){
                $other == '';
            }
            else{
                $other == $request->other_option;
            }
        }
        if($request->location){
            if($request->location == ''){
                $location == '';
            }
            else{
                $location == $request->location;
            }
        }

        $quotation = array(
            'rfq_id' => $request->rfq_id,
            'cat_id' => $request->cat_id,
            'product_id' => $request->product_id,
            'brand_id' => $request->brand_id,
            'credit_charge_id' => $request->credit_charge_id,
            'hub_id' => $request->hub_id,
            'payment_option' => $request->payment_option,
            'other_option' => $other,
            'method' => $request->method,
            'location' => $request->location,
            'variants' => $request->variants,
            'user_id' => customer('id')
        );
        $items = DB::table('rfq_booking')->insert($quotation);
        if($items){
            $status = '2';
            $rq = RequestQuotation::where('id',$request->rfq_id)->first();
            $rq->status = $status;
            $rq->save();
            /*DB::update("UPDATE request_quotation SET status = '$status' WHERE id = '".$request->rfq_id."'");*/
        }
        return response()->json(['message' => 'booked successfully ', 'code' => 200], 200);

        return back()->with("success", ["Booked Successfully !"]);
    }

    public function productWishlist(Request $request){
        $prodcutWishlist = new ProductWishlist();
        $prodcutWishlist->user_id = Auth::user()->id;
        $prodcutWishlist->product_id = $request->product_id;
        $prodcutWishlist->project_id = $request->project_id;
        $prodcutWishlist->save();
    }
    public function counterOffer(Request $request){
            $counterOffer = new CounterOffer();
            $counterOffer->user_id = $request->user_id;
            $counterOffer->product_id = $request->product_id;
            $counterOffer->credit_charge_id = $request->credit_charge_id;
            $counterOffer->hub_id = $request->hub_id;
            $counterOffer->price = $request->price;
            $counterOffer->quantity = $request->counterqty;
            $counterOffer->unit = $request->unit;
            $counterOffer->status = "0";
            $id = $counterOffer->save();
            if($id){
                return back()->with("success", ["Counter Saved !"]);
            }
        return back()->withErrors('msgx', ['Oops! Something went wrong. Try again later.']);

    }

    public function getVarPriceDetail(Request $request){

        $diffrence = $loading = $tax = $credit = 0;
        $quantity = explode(',',$request->quantity);
        $variant_id = explode(',',$request->variant_id);
        $credit_id = $request->credit_id;
        $weight_unit = $request->weight_unit;
        $creditText = '';

        for($i=0;$i<count($variant_id);$i++){
            $quan = $quantity[$i];
            $varid = $variant_id[$i];
            $product_id = $request->product_id;
            $customer_id = $request->customer_id;
            $hub_id = $request->hub_id;
            $priceDetail = getInstitutionalPrice($product_id,$customer_id,$hub_id,$varid,false);
            $diffrence += ($priceDetail['var']*$quan);
            $loading  += ($priceDetail['loading']*$quan);
            $tax += ($priceDetail['tax']*$quan);
            if($credit_id !== 'undefined') {
                $creditCharge = CreditCharges::where('id', $credit_id)->first();
                if ($creditCharge !== null)
                    $creditText = $creditCharge->amount . ' per <span class="setUnit">' . getUnitSymbol($weight_unit) . '</span> for ' . $creditCharge->days;
                $credit += ($creditCharge->amount * $quan);
            }
        }

        $html = array();
        $html['difference'] = $diffrence;
        $html['loading'] = $loading;
        $html['tax'] = $tax;
        $html['credit'] = $credit;
        $html['creditText'] = $creditText;
        return $html;
    }

    public function rfqBooking(Request $request){
        if (empty($request->q)) {
            return 'quantity';
        }
        $id = $request->id;
        $type = $request->type;
        $credit_id = '';
        if(isset($request->credit_id)) {
            $credit_id = $request->credit_id;
        }
        $product = Products::where('id',$id)->first();

        $basic = '';
        if($product !== null) {
            $basic = $product->purchase_price;
        }
        $tax = '';
        if($product !== null) {
            if ($product->tax !== '') {
                $tax = $product->tax;
            }
        }
        $q = isset($request->q) ? (int)$request->q : "1";
        $min = isset($request->min) ? (int)$request->min : 0;
        $variants = array();
        if ($q == 0 && $type == 'bysize') {
            $variants = (array)json_decode(stripcslashes($request->quantity));
            foreach ($variants as $var) {
                $q = $q + $var;
            }
        }
        if ($min == 0) {
            if ($q <= $min) {
                return 'invalid';
            }
        } else {
            if ($q < $min) {
                return 'minimum';
            }
        }

        if (Products::where('id',$id)->first()->quantity < $q) {
            return 'unavailable';
        }
        $varData = array();
        if($request->type == 'bysize' && $request->variants !== null) {
            $variant_id = explode(',', $request->variants);
            $quantity = explode(',', $request->q);
            $unit = explode(',', $request->units);
            foreach ($variant_id as $i=>$vi) {
                $customer_id = institutional('id');
                $hub_id = $request->hub_id;
                $priceDetail = getInstitutionalPrice($id, $customer_id, $hub_id, $vi, false);
                $vari = ProductVariant::where('id', $vi)->first();
                $varData[] = array(
                    "id" => $vi,
                    "title" => variantTitle($vi),
                    "quantity" => $quantity[$i],
                    "unit" => $unit[$i],
                    "price_detail" => array(array(
                        "basic" => $priceDetail['pprice'],
                        "loading" => $priceDetail['loading'],
                        "tax" => $priceDetail['tax'],
                        "size_difference" => $priceDetail['var'],
                        "total_amount" => $priceDetail['total']
                    ))
                );
            }
        }else{
            $customer_id = institutional('id');
            $hub_id = $request->hub_id;
            $displayPrice = $request->price;
            $weightUnit = $request->unit;
            $priceDetail = getInstitutionalPrice($id, $customer_id, $hub_id, false, false);
            $varData[] = array(
                "id"=>$product->id,
                "title"=>$product->title,
                "quantity"=>$q,
                "price"=>$displayPrice,
                "unit"=>$weightUnit,
                "price_detail" => array(array(
                    "basic" => $priceDetail['pprice'],
                    "loading" => $priceDetail['loading'],
                    "tax" => $priceDetail['tax'],
                    "total_amount" => $priceDetail['total']
                ))
            );
        }
        if($request->type == 'bysize' && $request->units !== null) {
            $udata = '{';
            for ($u = 0; $u < count($unit); $u++) {
                $udata .= '"' . $variant_id[$u] . '"' . ':' . '"' . $unit[$u] . '",';
            }
            $udata .= '}';
        }else{
            $udata = '';
        }
        $data['variants'] = json_encode($varData);
        $data['product_id'] = $id;
        $data['brand_id'] = $id;
        $data['credit_charge_id'] = $credit_id;
        $data['user_id'] = institutional('id');
        $data['direct_booking'] = '1';
        $data['hub_id'] = $request->hub_id;
        $data['method'] = $request->type;
        if(isset($request->without_variants)){
            $data['without_variants'] = $request->without_variants;
        }
        $data['unit'] = $udata;
        $in = DB::table('rfq_booking')->insert($data);
        if($in){
            return 'success';
        }
    }
}

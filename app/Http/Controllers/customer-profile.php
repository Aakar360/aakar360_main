<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>

    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
<!--    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>-->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .file-field input.file-path{
        height: 33px !important;
    }
    .file-field .btn, .file-field .btn-large, .file-field .btn-small{
        height: 33px !important;
        line-height: 33px !important;
    }

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }
    @media only screen and (max-width: 600px) {
        .modal-window>div {
            width: 340px;
            position: relative;
            margin: 10% auto;
            padding: 2rem;
            background: #fff;
            color: #444;
        }
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    .mygray{
        background-color: #aab2ae !important;
    }
    .myred{
        background-color: #eb6667 !important;
    }
    .mygreen{
        background-color: #4bbb50 !important;
    }
    .btn-arrow-right,
    .btn-arrow-left {
        position: relative;
        padding-left: 18px;
        padding-right: 18px;
    }
    .btn-arrow-right {
        padding-left: 36px;
    }
    .btn-arrow-left {
        padding-right: 36px;
    }
    .btn-arrow-right:before,
    .btn-arrow-right:after,
    .btn-arrow-left:before,
    .btn-arrow-left:after { /* make two squares (before and after), looking similar to the button */
        content:"";
        position: absolute;
        top: 7px; /* move it down because of rounded corners */
        width: 22px; /* same as height */
        height: 22px; /* button_outer_height / sqrt(2) */
        background: inherit; /* use parent background */
        border: inherit; /* use parent border */
        border-left-color: transparent; /* hide left border */
        border-bottom-color: transparent; /* hide bottom border */
        border-radius: 0px 4px 0px 0px; /* round arrow corner, the shorthand property doesn't accept "inherit" so it is set to 4px */
        -webkit-border-radius: 0px 4px 0px 0px;
        -moz-border-radius: 0px 4px 0px 0px;
    }
    .btn-arrow-right:before,
    .btn-arrow-right:after {
        transform: rotate(45deg); /* rotate right arrow squares 45 deg to point right */
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
    }
    .btn-arrow-left:before,
    .btn-arrow-left:after {
        transform: rotate(225deg); /* rotate left arrow squares 225 deg to point left */
        -webkit-transform: rotate(225deg);
        -moz-transform: rotate(225deg);
        -o-transform: rotate(225deg);
        -ms-transform: rotate(225deg);
    }
    .btn-arrow-right:before,
    .btn-arrow-left:before { /* align the "before" square to the left */
        left: -11px;
    }
    .btn-arrow-right:after,
    .btn-arrow-left:after { /* align the "after" square to the right */
        right: -11px;
    }
    .btn-arrow-right:after,
    .btn-arrow-left:before { /* bring arrow pointers to front */
        z-index: 1;
    }
    .btn-arrow-right:before,
    .btn-arrow-left:after { /* hide arrow tails background */
        background-color: white;
    }

    .visit {
        display: block !important;
    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs">
            <div class="card-content" style="min-height: 500px;">
                <?php

                if(session()->get('notices') != ''){
                    echo session()->get('notices');
                }
                ?>
                <?=$notices;?>

                <div class="card-title">
                    <div class="row">
                        <center><a class="btn myblue waves-light" style="padding:0 5px;" href="<?php echo isset($_GET['link']) ? $_GET['link'] : 'customer-data';?>">
                                <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                            </a></center>

                        <div class="col s12 m12 l12"><?php //dd($customer)?>
                            <h5><?=$customer->name." [ ".$customer->proprieter_name." ] ";?></h5>

                            <form method="post">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" value="<?=$customer->id?>" class="cust_id">
                                <button  value="1" type="button" class="btn btn-arrow-right change_status
                            <?php
                                if($customer->class == 4){
                                    echo 'mygreen';
                                }
                                else if($customer->class == 5){
                                    echo 'myred';
                                }
                                else if($customer->class == 6){
                                    echo 'mypurple';
                                }
                                else {
                                    echo 'myblue';
                                }
                                ?>" >Raw </button>
                                <a href="#open-modal5"><button id="button1" type="button" class="btn btn-arrow-right
<?php if($customer->class == 4){
                                        echo 'mygreen';
                                    }
                                    else if($customer->class == 5){
                                        echo 'myred';
                                    }
                                    else if($customer->class == 6){
                                        echo 'mypurple';
                                    }
                                    else if($customer->class == 2 || $customer->class == 3 ){
                                        echo 'myblue';
                                    }
                                    else {
                                        echo 'mygray';
                                    } //($customer->class == 4)?'mygreen':($customer->class == 5)?'myred':($customer->class == 2 || $customer->class == 3 )?'myblue':'mygray';
                                    ?>

">Contacted</button></a>

                                <a href="#open-modal6"><button type="button" class="btn btn-arrow-right
<?php
                                    if($customer->class == 4){
                                        echo 'mygreen';
                                    }
                                    else if($customer->class == 5){
                                        echo 'myred';
                                    }
                                    else if($customer->class == 6){
                                        echo 'mypurple';
                                    }
                                    else if($customer->class == 3){
                                        echo 'myblue';
                                    }
                                    else {
                                        echo 'mygray';
                                    }
                                    ?>
                                    ">Interested</button></a>
                                <a class="btn dropdown-settings btn btn-arrow-right <?php
                                if($customer->class == 4){
                                    echo 'mygreen';
                                }
                                else if($customer->class == 5){
                                    echo 'myred';
                                }
                                else if($customer->class == 6){
                                    echo 'mypurple';
                                }
                                else {
                                    echo 'mygray';
                                }
                                ?> " href="#!" data-target="dropdown1">

                                <span class="hide-on-small-onl"><?php if($customer->class == 5){echo 'Unqualified';} else if($customer->class == 4) { echo 'Converted'; }else if($customer->class == 6) { echo 'Not Interested'; } else {
                                        echo 'Con / Not-In / Unq';
                                    } ?></span>
                                    <i class="material-icons right">arrow_drop_down</i>
                                </a>
                                <ul class="dropdown-content" id="dropdown1" tabindex="0" style="padding: 20px">
                                    <li tabindex="0">
                                        <a href="#open-modal7" class="update_reason btn mygreen" data-value="4" style="line-height: 10px !important; color: white !important;">Converted</a>
                                    </li>

                                    <li tabindex="0">
                                        <a href="#open-modal4" class="update_reason btn mypurple" data-value="6" style="line-height: 10px !important; color: white !important;">Not Interested</a>
                                        <!--<button value ='6' class="col m12 s12 btn mypurple change_status" ></button>-->
                                        <!--<button value ='5' class="btn myred change_status">Unqualified</button>-->
                                    </li>
                                    <li tabindex="0">
                                        <a href="#open-modal2" class="update_reason btn myred" data-value="5" style="line-height: 10px !important; color: white !important;">Unqualified</a>
                                        <!--<a style="visibility: hidden;" id="open_reason" href="#open-modal2" data-toggle="modal">open</a>-->
                                    </li>
                                </ul>
                            </form>
                        </div>

                        <div class="col s12 m12 l12">
                            <ul class="row tabs" >
                                <li class="tab col s5 m5 xl1"><a class="active p-0" href="#Visits">Visits</a></li>
                                <li class="tab col s5 m5 xl2"><a class="p-0" href="#Products">Products</a></li>
                                <li class="tab col s5 m5 xl1"><a class="p-0" href="#Project">Project</a></li>
                                <li class="tab col s12 m12 xl2"><a class="p-0" href="#Promotional-material">Promotional</a></li>
                                <li class="tab col s5 m5 xl1"><a class="p-0" href="#Notes">Notes</a></li>
                                <li class="tab col s12 m12 xl2"><a class="p-0" href="#Communication">Communication</a></li>
                                <li class="tab col s5 m5 xl1"><a target="_blank" class="p-0" href="customers?edit=<?=$customer->id?>">Master</a></li>
                                <li class="tab col s5 m5 xl1"><a class="p-0" href="#Tickets">Tickets</a></li>
                                <li class="tab col s5 m5 xl1"><a class="p-0" href="#Timeline">Timeline</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="Timeline" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="container">
                            <form method="get" action="" class="right col s6 m2" id="timeline_form">
                                <input type="hidden" name="cid" value="<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>"/>
                                <div class="input-field">
                                    <select name="month" id="month"> Month
                                        <option value="1" <?=($month==1) ? 'selected' : ''?>>January</option>
                                        <option value="2" <?=($month==2) ? 'selected' : ''?>>February</option>
                                        <option value="3" <?=($month==3) ? 'selected' : ''?>>March</option>
                                        <option value="4" <?=($month==4) ? 'selected' : ''?>>April</option>
                                        <option value="5" <?=($month==5) ? 'selected' : ''?>>May</option>
                                        <option value="6" <?=($month==6) ? 'selected' : ''?>>June</option>
                                        <option value="7" <?=($month==7) ? 'selected' : ''?>>July</option>
                                        <option value="8" <?=($month==8) ? 'selected' : ''?>>August</option>
                                        <option value="9" <?=($month==9) ? 'selected' : ''?>>September</option>
                                        <option value="10" <?=($month==10) ? 'selected' : ''?>>October</option>
                                        <option value="11" <?=($month==11) ? 'selected' : ''?>>November</option>
                                        <option value="12" <?=($month==12) ? 'selected' : ''?>>December</option>
                                    </select>
                                    <label for="month">Month</label>
                                </div>
                            </form>
                            <?php

                            if(!$timeline_data){
                                echo '<h5>No data available for timeline.</h5>';
                            }else{ ?>
                                <div class="timeline">
                                    <div class="timeline-month">
                                        <?php $date = date('Y-'.$month.'-01');
                                        echo date('F, Y', strtotime($date));
                                        ?>
                                        <span><?=count($timeline_data);?> Days</span>
                                    </div>
                                    <?php foreach($timeline_data as $td){ ?>
                                        <div class="timeline-section">
                                            <div class="timeline-date">
                                                <?php
                                                echo date('d, l', strtotime($td[0]->date));
                                                ?>
                                            </div>
                                            <div class="row">
                                                <?php foreach($td as $t){ ?>
                                                    <?php
                                                    $data = null;
                                                    $tl_user = DB::table('user')->where('u_id', $t->created_by)->first();
                                                    switch ($t->section) {
                                                        case 'tickets':
                                                            $data = getTimelineData('crm_tickets', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box">
<div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">card_membership</i>
                                                                    New Ticket
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Ticket Id</strong>: ' . $data->id . '</div>
                                                                    <div class="box-item"><strong>Type</strong>: '.$data->type.'</div>
                                                                    <div class="box-item"><strong>Priority</strong>: '.$data->priority.'</div>
                                                                    <div class="box-item"><strong>Status</strong>: '.$data->status.'</div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'tickets_edit':
                                                            $data = getTimelineData('crm_tickets', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                                    <div class="timeline-box">
                                                                        <div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">card_membership</i>
                                                                    Ticket Update
                                                                 </div>
                                                                                                                             
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Ticket Id</strong>: ' . $data->id . '</div>
                                                                    <div class="box-item"><strong>Type</strong>: '.$data->type.'</div>
                                                                    <div class="box-item"><strong>Priority</strong>: '.$data->priority.'</div>
                                                                    <div class="box-item"><strong>Status</strong>: '.$data->status.'</div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'visits':
                                                            $data = getTimelineData('crm_visits', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">transfer_within_a_station</i>
                                                                    New Visit
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Detail</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
//                                                        case 'visits_edit':
//                                                            $data = getTimelineData('crm_visits', $t->section_id);
//                                                            if(!($data === null)) {
//                                                                echo '<div class="col s12 m4">
//                                                <div class="timeline-box"><div class="box-title">
//                                                                    <i class="material-icons text-success" aria-hidden="true">transfer_within_a_station</i>
//                                                                    Visit Update
//                                                                 </div>
//                                                                 <div class="box-content">
//                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
//                                                                    <div class="box-item"><strong>Detail</strong>: '.$data->description.'</div>
//                                                                    <div class="box-item"><strong></strong></div>
//                                                                    <div class="box-item"><strong></strong></div>
//                                                                 </div><div class="box-footer">- ';
//                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
//                                                                echo '</div></div></div>';
//                                                            }
//                                                            break;
                                                        case 'notes':
                                                            $data = getTimelineData('crm_notes', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">speaker_notes</i>
                                                                    New Note
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'notes_edit':
                                                            $data = getTimelineData('crm_notes', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">speaker_notes</i>
                                                                    Note Update
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'projects':
                                                            $data = getTimelineData('crm_customer_projects', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">work_outline</i>
                                                                    New Project
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Title</strong>: '.$data->project_name.'</div>
                                                                    <div class="box-item"><strong>Type</strong>: '.$data->project_type.'</div>
                                                                    <div class="box-item"><strong>Status</strong>: '.$data->project_status.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'projects_edit':
                                                            $data = getTimelineData('crm_customer_projects', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                                            <div class="timeline-box">
                                                                                <div class="box-title">
                                                                                    <i class="material-icons text-success" aria-hidden="true">work_outline</i> Project Update
                                                                                 </div>
                                                                                 <div class="box-content">
                                                                                    <div class="box-item"><strong>Title</strong>: '.$data->project_name.'</div>
                                                                                    <div class="box-item"><strong>Type</strong>: '.$data->project_type.'</div>
                                                                                    <div class="box-item"><strong>Status</strong>: '.$data->project_status.'</div>
                                                                                    <div class="box-item"><strong></strong></div>
                                                                                 </div>
                                                                                 <div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'promotional':
                                                            $data = getTimelineData('crm_promotional', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">store</i>
                                                                    New Promotional
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'promotional_edit':
                                                            $data = getTimelineData('crm_promotional', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">store</i>
                                                                    Promotional Update
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'products':
                                                            $data = getTimelineData('crm_products', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">store</i>
                                                                    New Products
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Category(s)</strong>: '.getCategoryNames($data->product_id).'</div>
                                                                    <div class="box-item"><strong>Brand(s)</strong>: '.getBrandNames($data->brands).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'products_edit':
                                                            $data = getTimelineData('crm_products', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">store</i>
                                                                    Products Update
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Category(s)</strong>: '.getCategoryNames($data->product_id).'</div>
                                                                    <div class="box-item"><strong>Brand(s)</strong>: '.getBrandNames($data->brands).'</div>
                                                                    <div class="box-item"><strong>Notes</strong>: '.$data->description.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'calls':
                                                            $data = getTimelineData('crm_call', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">phone</i>
                                                                    New Call
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Number</strong>: '.$data->customer_number.'</div>
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Remarks</strong>: '.$data->message.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'calls_edit':
                                                            $data = getTimelineData('crm_call', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">phone</i>
                                                                    Call Update
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Number</strong>: '.$data->customer_number.'</div>
                                                                    <div class="box-item"><strong>Date</strong>: '.date('d F, Y', strtotime($data->select_date)).'</div>
                                                                    <div class="box-item"><strong>Remarks</strong>: '.$data->message.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'sms':
                                                            $data = getTimelineData('crm_message_save', $t->section_id);
                                                            if(!($data === null)) {
                                                                echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">mail_outline</i>
                                                                    New SMS
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Number</strong>: '.$data->mobile.'</div>
                                                                    <div class="box-item"><strong>Remarks</strong>: '.$data->message.'</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                                echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                                echo '</div></div></div>';
                                                            }
                                                            break;
                                                        case 'class':
                                                            echo '<div class="col s12 m4">
                                                <div class="timeline-box"><div class="box-title">
                                                                    <i class="material-icons text-success" aria-hidden="true">card_membership</i>
                                                                    Class Changed
                                                                 </div>
                                                                 <div class="box-content">
                                                                    <div class="box-item"><strong>Remark</strong>: ' . $t->remark . '</div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                    <div class="box-item"><strong></strong></div>
                                                                 </div><div class="box-footer">- ';
                                                            echo ($tl_user !== null) ? $tl_user->u_name : '';
                                                            echo '</div></div></div>';
                                                            break;
                                                        default:
                                                            echo '';
                                                    }


                                                    ?>


                                                <?php } ?>

                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>

                    </div>
                    <div id="Tickets" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="col s12 m12 " style="text-align:center; margin-bottom: 10px" >
                            <button class="btn myblue waves-light ticket_form_button" style="padding:0 5px;">
                                <i class="material-icons right">note_add</i> New Ticket
                            </button>
                        </div>
                        <div class="ticket_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">forum</i>
                                        <select name="type" class="type" required> type
                                            <option selected disabled value="">Select Type</option>
                                            <option value="Question">Question</option>
                                            <option value="Quotation">Quotation</option>
                                            <option value="Problem">Problem</option>
                                        </select>
                                        <label for="type">Type</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">low_priority</i>
                                        <select name="priority" class="Priority" required> Priority
                                            <option value="" selected disabled>Select Priority</option>
                                            <option value="Low">Low</option>
                                            <option value="Medium">Medium</option>
                                            <option value="High">High</option>
                                            <option value="Urgent">Urgent</option>
                                        </select>
                                        <label for="priority">Priority</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">thumbs_up_down</i>
                                        <select name="status" class="Status" required> Status
                                            <option value="" selected disabled>Select Status</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Resolved">Resolved</option>
                                            <option value="Closed">Closed</option>
                                        </select>
                                        <label for="status">Status</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">transfer_within_a_station</i>
                                        <input id="Assign_to" type="text" class="validate" name="assign_to">
                                        <label for="Assign_to">Assign To</label>
                                    </div>
                                    <div class="col s12 m6 xl3">
                            <div class="col s1 m1"style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">person</i></div>
                            <div class="col s11 m11" style="margin-top: -7px">
                                <label>Select Account Manager</label>
                                <select class="browser-default acc_manager" id="acc_manager" name="acc_manager" tabindex="-1">
                                    <option value="all">All</option>
                                    <?php
                                    foreach($users as $user){
                                        echo '<option value="'.$user->u_id.'">'.$user->u_name.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">watch_later</i>
                                        <input type="date" class="datepicker" id="ticket_create" name="ticket_create_date">
                                        <label for="ticket_create">Create Date</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">watch_later</i>
                                        <input type="date" class="datepicker" id="ticket_due_date" name="ticket_due_date">
                                        <label for="ticket_due_date">Due Date</label>
                                    </div>
                                </div>

                                <!--<div class="row">
                                    <div class="input-field col s6">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Tag" type="tel" class="validate" name="tag">
                                        <label for="Tag">Tag</label>
                                    </div>
                                </div>-->
                                <div class="row">
                                    <div class="input-field col s12 m12">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description" type="text" required class="validate" name="description">
                                        <label for="Description">Description</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <!--<input type="file" name="image" style="width: 100%" class="btn myblue" />-->
                                        <div class="file-field">
                                            <div class="btn myblue waves-light left" style="padding:0 5px;">
                                                <span>Choose file</span>
                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="ticket_image">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload your file">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="input-field col s12 m3">
                                        <p id="ticket_img_div">
                                            <img id="ticket_image_show" src="#" height="200px" width="200px"  />
                                        </p>
                                    </div>
                                    <div class="input-field col s12 m3" style="text-align: center">
                                        <button class="btn myblue waves-light " style="padding:0 5px;" type="submit" name="add_ticket">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="divider"></div>
                        <h6>All Tickets</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="tickets-table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>Ticket ID</th>
                                        <th>Type</th>
                                        <th>Priority</th>
                                        <th>Description</th>
                                        <th>File</th>
                                        <th>Account Manager</th>
                                        <th>Create Date</th>
                                        <th>Due Date</th>
                                        <th>Status</th>
                                        <th>edited_by</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="Notes" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="notes_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">watch_later</i>
                                        <input type="date" class="datepicker" id="dob1" name="select_date" >
                                        <label for="dob1">Date</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m12">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description3" type="text" class="validate" name="description" required>
                                        <label for="Description3">Description</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <div class="file-field">
                                            <div class="btn myblue waves-light left" style="padding:0 5px;">
                                                <span>Choose file</span>
                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="notes_image">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload your file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m3">
                                        <p id="notes_img_div">
                                            <img id="notes_image_show" src="#" height="200px" width="200px"  />
                                        </p>
                                    </div>

                                    <div class="input-field col s12 m3" style="text-align: center">
                                        <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_notes">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="divider"></div>
                        <h6>All Notes</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="notes-table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>Sr.No.</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>File</th>
                                        <th>edited_by</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="Project" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="project_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">create_new_folder</i>
                                        <input type="text"  id="pname" required name="pname">
                                        <label for="pname">Project Name</label>
                                    </div>
                                    <div class="input-field col s12 m6 xl3">
                                        <i class="material-icons prefix">list</i>
                                        <select name="ptype" class="ptype" required> type
                                            <option value="" disabled selected>Select Project Type</option>
                                            <option value="1">Private</option>
                                            <option value="2">Government</option>
                                        </select>
                                        <label for="ptype">Project Type</label>
                                    </div>
                                    <div class="input-field col s12 m12 xl3" style="margin-top: 0px;">

                                        <i class="material-icons prefix" style="margin-top: 10px">dns</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Project Sub Type
                                            <select class="browser-default pstype" required name="pstype[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">thumbs_up_down</i>
                                        <select name="pstatus" id="pstatus" class="type" required> status
                                            <option value="">Select Status</option>
                                            <option value="Ongoing">Ongoing</option>
                                            <option value="Start">Start</option>
                                            <option value="Completed">Completed</option>
                                        </select>
                                        <label for="status">Status</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">border_style</i>
                                        <input type="text"  id="sqft" name="sqft">
                                        <label for="sqft">Total Sq.Feet</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">flag</i>
                                        <div class="col s12 m12" style="margin-left: 23px">
                                            <select  class="browser-default states state_list" name="state" style="width: 100%" required>
                                                <option value="">Select State</option>
                                                <?php
                                                foreach ($states as $state){
                                                    ?>
                                                    <option value="<?=$state->id;?>"><?=$state->name?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m6" >
                                        <i class="material-icons prefix">adjust</i>
                                        <div class="col s12 m12" style="margin-left: 23px">
                                            <select  class="browser-default district dist_list" name="district" style="width: 100%" required>
                                                <option value="" disabled selected>Select District</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m6" >
                                        <i class="material-icons prefix">adjust</i>
                                        <div class="col s12 m12" style="margin-left: 23px">
                                            <select  class="browser-default block" name="block" style="width: 100%" id="block">
                                                <option value="" disabled selected>Select Block</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="input-field col s12 m4" >
                                        <i class="material-icons prefix">location_on</i>
                                        <div class="col s12 m12" style="margin-left: 23px">
                                            <select  class="browser-default locality local_list" id="locality" name="locality" style="width: 100%">
                                                <option value="">Select Locality</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="input-field col s12 m2">
                                        <a href="#open-modal3" class="btn myblue waves-light" style="padding: 0 5px; width: fit-content;">Add New
                                            <i class="material-icons right">add_circle_outline</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">navigation</i>
                                        <input type="text"  id="paddress" name="paddress">
                                        <label for="paddress">Project Address</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input type="text"  id="cpname" name="cpname">
                                        <label for="cpname">Contact Person Name</label>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">contact_phone</i>
                                        <input type="text"  id="cpnumber" name="cpnumber">
                                        <label for="cpnumber">Contact Person Number</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">school</i>
                                        <input type="text"  id="cpdesignation" name="cpdesignation">
                                        <label for="cpdesignation">Contact Person Designation</label>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="input-field col s12 m12">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description4" type="text" class="validate" name="description">
                                        <label for="Description4">Description</label>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <div class="file-field">
                                            <div class="btn myblue waves-light left" style="padding:0 5px;">
                                                <span>Choose file</span>
                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="project_image">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload your file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m3">
                                        <p id="project_img_div">
                                            <img id="project_image_show" src="#" height="200px" width="200px"  />
                                        </p>
                                    </div>

                                    <div class="input-field col s12 m3" style="text-align: center;">
                                        <button class="btn myblue waves-light"  style="padding:0 5px;" type="submit" name="add_project">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="divider"></div>
                        <h6>All Project</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="projects-table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>ID</th>
                                        <th>Project Name</th>
                                        <th>Project Type</th>
                                        <th>Project Subtype</th>
                                        <th>Total Sq. Feet</th>
                                        <th>Description</th>
                                        <th>File</th>
                                        <th>Edited By</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="Visits" class="col-lg-12 col-xs-12">
                        <div class="visits_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">

                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="name" type="text" class="validate" required name="name" value="<?=$customer->name;?>">
                                        <label for="name">Name</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">contact_phone</i>
                                        <input id="phone_no" type="tel" class="validate" required name="phone_no" value="<?=$customer->contact_no;?>">
                                        <label for="phone_no"> Phone No</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">navigation</i>
                                        <input id="address" type="text" class="validate" name="address" required value="<?=$customer->postal_address;?>">
                                        <label for="address">Address</label>
                                    </div>
                                    <div class="input-field col s12 m6">

                                        <i class="material-icons prefix">watch_later</i>
                                        <input type="date" class="datepicker" required id="dob" name="date">
                                        <label for="dob">Date</label>
                                        <label for="dob">Date</label>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description_v" type="text" class="validate" name="description">
                                        <label for="Description_v">Description</label>
                                    </div>
                                    <div class="input-field col s12 m6" style="margin-top: 0px;">
                                        <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Select User

                                            <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                <?php

                                                foreach ($users as $user){

                                                    ?>
                                                    <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <div class="file-field">
                                            <div class="btn myblue waves-light left" style="padding:0 5px; height: 33px !important;line-height: 33px !important;">
                                                <span>Choose file</span>
                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="visit_image">
                                            </div>
                                            <div class="file-path-wrapper" >
                                                <input  class="file-path validate "  type="text" placeholder="Upload your file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m3">
                                        <p id="visit_img_div">
                                            <img id="visit_image_show" src="#" height="200px" width="200px"  />
                                        </p>
                                    </div>

                                    <div class="input-field col s12 m3" style="text-align: center">
                                        <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_visit">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="divider"></div>
                        <h6>All Vistis</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="visits-table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>User Name</th>
                                        <th>Description</th>
                                        <th>file</th>
                                        <th>edited_by</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!--                                                                        <td>=DB::table('crm_visits')->join('crm_user_manager','crm_user_manager.id','=','crm_visits.user_id')->select('crm_user_manager.name','crm_visits.id','=','$u_id')->get();<!--</td>-->
                                    <?php $i = 1;  foreach ($visits as $visit) {

                                        $p_subtype1 =explode(',',$visit->user_id);
//                                    $image_file =explode(',',$visit->image);
                                        $st1 = '';
                                        foreach ($p_subtype1 as $stid1) {
                                            $stval1 = DB::table('crm_user_manager')->where('id', $stid1)->first();
                                            if($stval1 !== null)
                                                $st1 = $stval1->name;
                                        }

                                        ?>
                                        <tr>
                                            <td><?=$i?></td>
                                            <td><?=$visit->select_date?></td>
                                            <td><?=$i?></td>

                                            <td><?=$visit->description?></td>
                                            <td>

                                                <a href="../assets/crm/images/visits/<?=$i?>"><img src="../assets/crm/images/visits/<?=$i?>" width="30px" height="30px"/></a></td>

                                            <td>
                                                <button value="<?=$visit->id?>" class="btn myred waves-light vdelete" style="padding:0 10px;"><i class="material-icons">delete</i></button>
                                                <a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="visit-edit?id=<?=$visit->id?>&cid=<?=isset($_GET['cid']) ? $_GET['cid'] : '';?>"><i class="material-icons">edit</i></a>

                                            </td>
                                        </tr>
                                        <?php $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="Communication" style="display: none;" class="col-lg-12 col-xs-12">

                        <ul class="tabs">
                            <li class="tab col s4 p-0"><a class="active p-0" href="#Message">Message</a></li>
                            <li class="tab col s4 p-0"><a class="p-0" href="#Email">Email</a></li>
                            <li class="tab col s4 p-0"><a class="p-0" href="#Call">Call</a></li>
                        </ul>

                        <div id="Message">
                            <pre></pre>
                            <div class="Message_form">
                                <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                    <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input type="tel" class="validate" id="customer_no" name="customer_no" value="<?php echo  $customer->contact_no;?>">
                                            <label for="customer_no">Customer No</label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">email</i>
                                            <input id="message" type="text" class="validate" name="message">
                                            <label for="message">Message</label>
                                        </div>
                                        <div class="input-field col s12 m6" style="margin-top: 0px;">
                                            <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Select User

                                                <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                    <?php

                                                    foreach ($users as $user){

                                                        ?>
                                                        <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="input-field col s12 m12" style="text-align: center">
                                            <button class="btn myblue waves-light send_msg" style="padding:0 5px;" type="submit" name="add_msg">Send
                                                <i class="material-icons right">save</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="divider"></div>
                            <h6>All Message</h6>
                            <div class="row">
                                <div class="col s12 m12 table-responsive">
                                    <table id="message_table" class="display" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th>Sr.No.</th>
                                            <th>user</th>
                                            <th>Message</th>
                                            <th>Date</th>
                                            <th>edited_by</th>

                                        </tr>
                                        </thead>

                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="Email">
                            <pre></pre>
                            <div class="Email_form">
                                <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                    <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">

                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">contact_mail</i>
                                            <input type="text" class="validate" id="customer_email" name="customer_email" value="<?php echo  $customer->email;?>">
                                            <label for="customer_email">Customer Email</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">comment</i>
                                            <input type="text" class="validate" id="email_subject" name="email_subject">
                                            <label for="email_subject">Subject</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">email</i>
                                            <input id="message1" type="text" class="validate" name="message">
                                            <label for="message1">Message</label>
                                        </div>
                                        <div class="input-field col s12 m6" style="margin-top: 0px;">
                                            <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Select User

                                                <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                    <?php

                                                    foreach ($users as $user){

                                                        ?>
                                                        <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="input-field col s12 m12" style="text-align: center">
                                            <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_notes">Send
                                                <i class="material-icons right">save</i>
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="divider"></div>
                            <h6>All Email</h6>
                            <div class="row">
                                <div class="col s12 table-responsive">
                                    <table id="email_table" class="display" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th>Sr.No.</th>
                                            <th>user</th>
                                            <th>Date</th>
                                            <th>Message</th>
                                            <th>edited_by</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="Call">
                            <pre></pre>
                            <div class="Call_form">
                                <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                    <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">


                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input type="tel" class="validate" id="customer_no1" name="customer_no" value="<?php echo  $customer->contact_no;?>">
                                            <label for="customer_no1">Customer No</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">watch_later</i>
                                            <input type="date" class="datepicker" id="dob3" name="select_date">
                                            <label for="dob3">Date</label>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <i class="material-icons prefix">content_paste</i>
                                            <input id="message_call" type="text" class="validate" name="message" required>
                                            <label for="message_call">Notes</label>
                                        </div>
                                        <div class="input-field col s12 m6" style="margin-top: 0px;">
                                            <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                                            <div class="col s12 m12" style="margin-left: 20px">
                                                Select User

                                                <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                                    <?php

                                                    foreach ($users as $user){

                                                        ?>
                                                        <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">list</i>
                                        <select name="ctype" class="ctype" required> type
                                            <option value="" disabled selected>Select Call Type</option>
                                            <option value="1">Incoming</option>
                                            <option value="2" selected>Outgoing</option>
                                            <option value="3">Missed call</option>
                                        </select>
                                        <label for="ptype">Call Type</label>
                                    </div>
                                    <div class="input-field col s12 m6">



                                        <input class="timepicker form-control" type="text" name="time">
                                        <label for="time">Call Time</label>

                                    </div>

                            </div>
                                    <div class="row">

                                        <div class="input-field col s12 m12" style="text-align: center">
                                            <button class="btn myblue waves-light " style="padding:0 5px;" type="submit" name="add_call">Save
                                                <i class="material-icons right">save</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="divider"></div>
                            <h6>All Call</h6>
                            <div class="row">
                                <div class="col s12 table-responsive">
                                    <table id="call_table" class="display" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th>Sr.No.</th>
                                            <th>user</th>
                                            <th>Date</th>
                                            <th>Message</th>
                                            <th>edited_by</th>
                                            <th>Time</th>
                                            <th>Created at</th>
                                            <th>Duration</th>
                                            <th>Call Type</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="Products" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="product_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">

                                <div class="row">
                                    <div class="input-field col s12 m6">

                                        <i class="material-icons prefix" style="margin-top: 10px">list</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Select Category
                                            <select  class="browser-default category" name="category[]" required multiple tabindex="-1" style="width: 100% !important;">
                                                <?php foreach ($product_category as $category){
                                                    ?>
                                                    <option value="<?=$category->id?>"> <?=$category->name?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m6">

                                        <i class="material-icons prefix" style="margin-top: 10px">dns</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Price Group
                                            <select  class="browser-default price_group" name="price_group[]" multiple tabindex="-1" style="width: 100% !important;">
                                                <option value="">Select Category First</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m6">

                                        <i class="material-icons prefix" style="margin-top: 10px">local_mall</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Select Brand
                                            <select  class="browser-default brand" name="brand[]" multiple tabindex="-1" style="width: 100% !important;">
                                                <?php foreach ($brand as $bnd){
                                                    ?>
                                                    <option value="<?=$bnd->id?>"> <?=$bnd->name?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix" style="margin-top: 10px">transfer_within_a_station</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Select Supplier
                                            <select  class="browser-default supplier" name="supplier[]" multiple tabindex="-1" style="width: 100% !important;">

                                                <?php foreach ($suppliers as $sup){
                                                    ?>
                                                    <option value="<?=$sup->id?>"> <?=$sup->name?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m12">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description5" type="text" class="validate" name="description">
                                        <label for="Description5">Description</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <div class="file-field">
                                            <div class="btn myblue waves-light left" style="padding:0 5px;">
                                                <span>Choose file</span>
                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="category_image">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload your file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m3">
                                        <p id="category_img_div">
                                            <img id="category_image_show" src="#" height="200px" width="200px"  />
                                        </p>
                                    </div>

                                    <div class="input-field col s12 m3" style="text-align: center">
                                        <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_product">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="divider"></div>
                        <h6>All Products</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="products_table" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>Sr.No.</th>
                                        <th>Product</th>
                                        <th>Brand</th>
                                        <th>Supplier</th>
                                        <th>Price Group</th>
                                        <th>Description</th>
                                        <th>file</th>
                                        <th>edited_by</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="Promotional-material" style="display: none;" class="col-lg-12 col-xs-12">
                        <div class="promotional_form">
                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden" name="crm_cust_id" value="<?php echo  $customer->id; ?>">


                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">watch_later</i>
                                        <input type="date" class="datepicker" id="dob2" name="select_date">
                                        <label for="dob2">Date</label>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="input-field col s12 m12">
                                        <i class="material-icons prefix">view_headline</i>
                                        <input id="Description6" type="text" class="validate" name="description" required>
                                        <label for="Description6">Description</label>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m12" style="text-align: center">
                                        <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_promotional">Save
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            </fieldset>
                        </div>
                        <div class="divider"></div>
                        <h6>All Promotional Material</h6>
                        <div class="row">
                            <div class="col s12 table-responsive">
                                <table id="promotinal_material" class="display" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th>Sr.No.</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>edited_by</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/form-layouts.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>





</body>

</html>
<script type="text/javascript">

    $('.timepicker').datetimepicker({

        format: 'HH:mm:ss'

    });

</script>
<script>
    var ttable = false;
    var ptable = false;
    var protable = false;
    var prodtable = false;
    var calltable = false;
    var vtable = false;
    var ntable = false;
    var promotable = false;
    var msgtable = false;
    var emailtable = false;
    $(document).ready(function(){
//        user_notinterested
        $('.category').select2();
        $('.pstype').select2();
        $('.brand').select2();
        $('.user').select2();
        $('.supplier').select2();
        $('.states').select2();
        $('.district').select2();
        $('.block').select2();
        $('.locality').select2();
        $('.price_group').select2();
        $('.user_unqualified').select2();
        $('.user_contacted').select2();
        $('.user_interested').select2();
        $('.user_notinterested').select2();
        $('.user_converted').select2();
        $(".category").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-price-groups-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".price_group").html(data);
                }
            });
        });

        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".district").html(data).append('<option value="" disabled selected>Select District</option>');
                }
            });
        });
        $('.district').change(function () {
            var did = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-block",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',

                success: function(data){
                    $(".block").html(data).append('<option value="" disabled selected>Select Block</option>');

                }
            });
        });
        $('.block').change(function () {
            var did = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-locality",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $('#locality').html(data);
                }
            });
        });
//        $("#dob1").datepicker.regional[""].dateFormat = 'mm/dd/yy';
//        $("#dob1").datepicker("setDate", new Date());

        //$("#dob1").datepicker().datepicker("setDate", '01/02/2014');


        // $(".datepicker").datepicker('setDate','12/31/2000');
//            $(".visit_date2").datepicker();
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear() + '-' +
            ((''+month).length<2 ? '0' : '') + month + '-' +
            ((''+day).length<2 ? '0' : '') + day;

        $(".datepicker").val(output);
        $(".datepicker").datepicker("setDate", new Date());



        $('.type1').select2();
        $('.variants').select2();
        $('.ticket_form').hide();

        $('.ticket_form_button').click(function(){
            $('.ticket_form').show();
        });


        $(".category").change(function(){
            var val = $(this).val();
            var cname = $(".category option:selected").html();

            $.ajax({
                type: "POST",
                url: "get-report-option",
                data:'cid='+val+'&cname='+cname+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data){
                    $(".report_div").html(data);
                    $("#model_report_type").val(data);
                }
            });
        });
        var get_view_type = $('#get_report_view').val();
        if(get_view_type=='brand_wise'){
            $('.brand_wise').show();
        }
        if(get_view_type=='price_wise'){
            $('.price_wise').show();
        }


        $('#visit_img_div').hide();
        function readURLVisit(input) {
            $('#visit_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#visit_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#visit_image").change(function(){
            readURLVisit(this);
        });


        $('#ticket_img_div').hide();
        function readURLTicket(input) {
            $('#ticket_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ticket_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#ticket_image").change(function(){
            readURLTicket(this);
        });

        $('#notes_img_div').hide();
        function readURLNotes(input) {
            $('#notes_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#notes_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#notes_image").change(function(){
            readURLNotes(this);
        });

        $('#project_img_div').hide();
        function readURLproject(input) {
            $('#project_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#project_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#project_image").change(function(){
            readURLproject(this);
        });

        $('#category_img_div').hide();
        function readURLcategory(input) {
            $('#category_img_div').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#category_image_show').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#category_image").change(function(){
            readURLcategory(this);
        });



        $('.change_status').click(function () {
            var val = $(this).val();
            var cid = $(".cust_id").val();
            var user_unqualified = $(".user_unqualified").val();
            var user_notinterested = $(".user_notinterested").val();
            var user_contacted = $(".user_contacted").val();
            var user_interested = $(".user_interested").val();
            var user_converted = $(".user_converted").val();
            var reason = $("#reason").val();
            var notreason = $("#notreason").val();
            var dunqualied = $("#dunqualied").val();
            var dnotinterest = $("#dnotinterest").val();
            var dcontacted = $("#dcontacted").val();
            var dinterested = $("#dinterested").val();
            var dconverted = $("#dconverted").val();
            $.ajax({
                type: "POST",
                url: "customer-class-update",
                data:'cid='+cid+'&class='+val+'&reason='+reason+'&dunqualied='+dunqualied+'&user_unqualified='+user_unqualified+'&user_notinterested='+user_notinterested+'&user_contacted='+user_contacted+'&user_interested='+user_interested+'&notreason='+notreason+'&user_converted='+user_converted+'&dnotinterest='+dnotinterest+'&dcontacted='+dcontacted+'&dinterested='+dinterested+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    //alert(data);
                    window.location.href = "customer-profile?cid="+cid;
                    //window.location.reload();

                }
            });
        });

        $('.send_msg').click(function () {
            var mob = $.trim($("#customer_no").val());
            var cname = encodeURIComponent($("#message").val());
            var cid = $(".cust_id").val();
            if(mob==''){
                alert('Please Enter Number');
                return false;
            }
            else{
                $.ajax({
                    type: "GET",
                    url: "https://merasandesh.com/api/sendsms",
                    data:'username=smshop&password=Smshop@123&senderid=SUPORT&message='+cname+'&numbers='+mob+'&unicode=0',
                    async: true,
                    beforeSend: function(){
                        $('.send_msg').html('Wait...');
                        $.ajax({
                            type: "POST",
                            url: "msg_save",
                            data:'message='+cname+'&numbers='+mob+'&_token=<?=csrf_token(); ?>',
                            success: function(data){

                            }
                        });
                    },
                    success: function(data){

                    },
                    error: function(){
//                        alert('Message sending failed.');
                    },
                    complete: function(data){
                        location.reload();
                    }

                });
            }

        });

        $('.ptype').change(function () {
            var val = $(this).val();

            $.ajax({
                type: "POST",
                url: "get-subtype",
                data:'tid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.pstype').html(data);
                }
            });
        });

        $(document).on('click', '.pdelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-project-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(ptable){
                        $('#projects-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.tdelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-ticket-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(ttable){
                        $('#tickets-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.ndelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-note-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(ntable){
                        $('#notes-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.vdelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-visit-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(vtable){
                        $('#visits-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.calldelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);

            $.ajax({
                type: "POST",
                url: "customer-call-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);

                    $('#calls-table').DataTable().ajax.reload();

                    location.reload();

                }
            });
        });



        $(document).on('click', '.prodelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);



            $.ajax({
                type: "POST",
                url: "customer-promotional-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    if(protable){
                        $('#promos-table').DataTable().ajax.reload();
                    }else{
                        location.reload();
                    }
                }
            });
        });

        $(document).on('click', '.proddelete', function () {
            var pid = $(this).val();
            var cid = $(".cust_id").val();
            var a = $(this);
//            alert(pid);
            $.ajax({
                type: "POST",
                url: "customer-product-delete",
                data:'pid='+pid+'&cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);

                    $('#products-table').DataTable().ajax.reload();

                    location.reload();

                }
            });
        });
        $('#month').on('change', function(){
            $('#timeline_form').submit();
        });
        $('.update_reason').on('click', function(){
            var status = $(this).data('value');
            if(status == 5){
                $('#open-modal2 h1').html('UNQUALIFIED');
                $('#open-modal2 .change_status').attr('value', 5);
                $('#open_reason').trigger('click');
            }else if(status == 6){
                $('#open-modal2 h1').html('NOT INTERESTED');
                $('#open-modal2 .change_status').attr('value', 6);
                $('#open_reason').trigger('click');
            }
        });
        $('#localitySave').on('click', function(){
            var formData = $('form#localityForm').serialize();
            var btn = $(this);
            $.ajax({
                url: 'save-locality',
                type: 'post',
                data: formData,
                beforeSend: function(){
                    btn.html('Saving...');
                },
                success: function(data){
                    if(data == 'success') {
                        alert('Locality Saved.');
                    }else{
                        alert('Invalid Data.');
                    }
                    btn.html('Save');
                }
            });
        });
    });

</script>
<script>
$(document).ready(function(){
    $(function() {
        ttable = $('#tickets-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-tickets-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'type', name: 'type' },
                { data: 'priority', name: 'priority' },
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'acc_manager', name: 'acc_manager', searchable: false, sortable:false },
                { data: 'created_date', name: 'created_date'},
                { data: 'due_date', name: 'due_date',searchable: false },
                { data: 'status', name: 't_visits', searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
    $(function() {
        ntable = $('#notes-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-notes-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'select_date', name: 'select_date' },
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
    $(function() {
        ptable = $('#projects-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-projects-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'project_name', name: 'project_name' },
                { data: 'project_type', name: 'crm_project_type.project_type' },
                { data: 'project_subtype', name: 'project_subtype'},
                { data: 'sqft', name: 'sqft'},
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by'},
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
    $(function() {
        vtable = $('#visits-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-visits-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'select_date', name: 'select_date'},
                { data: 'user_id', name: 'user_id'},
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
        prodtable = $('#products_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-products-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'product_id', name: 'product_id'},
                { data: 'brands', name: 'brands'},
                { data: 'supplier_id', name: 'supplier_id'},
                { data: 'price_groups', name: 'price_groups'},
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
        promotable = $('#promotinal_material').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-promotion-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'select_date', name: 'select_date'},
                { data: 'description', name: 'description'},
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
        msgtable = $('#message_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-message-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },

            columns: [
                { data: 'id', name: 'id' },
                { data: 'user_id', name: 'user_id'},
                { data: 'message', name: 'message'},
                { data: 'created_at', name: 'created_at'},
                { data: 'edited_by', name: 'edited_by', searchable: false }

            ]
        });
        calltable = $('#call_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-call-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'user_id', name: 'user_id'},
                { data: 'select_date', name: 'select_date'},
                { data: 'message', name: 'message'},
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'time', name: 'time', searchable: false },
                { data: 'created_date', name: 'created_date', searchable: false },
                { data: 'duration', name: 'duration', searchable: false,"render": function (data, type, row) {
                    var seconds = row.duration ;
                    if(seconds!==null){
                        var date = new Date(null);
                        date.setSeconds(seconds); // specify value for SECONDS here
                        var result = date.toISOString().substr(11, 8);
                        return result;
                    }
                    else{
                        return seconds;
                    }


                } },

                { data: 'call_type', name: 'call_type', searchable: false,"render": function (data, type, row) {

                    switch(row.call_type) {
                        case '1' : return 'Incoming Call'; break;
                        case '2' : return 'Outgoing Call'; break;
                        case '3' : return 'Missed Call'; break;
                        default  : return 'N/A';
                    }

                } },
                { data: 'action', name: 'action', orderable: false, searchable: false }

            ],
            order: [[0, 'desc']],
        });
    });
});
</script>


<div id="open-modal2" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>UNQUALIFIED </h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">

                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Reason</td>
                        <td ><input type="text" id="reason" name="reason" class="form-control" value="<?=$customer->unqualified_reason;?>"></td>
                    </tr>
                    <tr>
                        <td>Select User</td>
                        <td ><div class="input-field col s12 m6" style="margin-top: 0px;">

                                <div class="col s12 m12" style="margin-left: 20px">


                                    <select class="browser-default user_unqualified" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php

                                        foreach ($users as $user){

                                            ?>
                                            <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td ><div class="input-field col s12 m6">

                                <i class="material-icons prefix">watch_later</i>
                                <input type="date" class="datepicker" required id="dunqualied" name="date">
                                <label for="dob">Date</label>
                                <label for="dob">Date</label>

                            </div></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" value="5" class="btn myblue change_status">Save</td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>

<div id="open-modal4" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>NOT Interested </h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">

                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Reason</td>
                        <td ><input type="text" id="notreason" name="reason" class="form-control" value="<?=$customer->notinterested_reason;?>"></td>
                    </tr>
                    <tr>
                        <td>Select User</td>
                        <td ><div class="input-field col s12 m6" style="margin-top: 0px;">

                                <div class="col s12 m12" style="margin-left: 20px">


                                    <select class="browser-default user_notinterested" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php

                                        foreach ($users as $user){

                                            ?>
                                            <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td ><div class="input-field col s12 m6">

                                <i class="material-icons prefix">watch_later</i>
                                <input type="date" class="datepicker" required id="dnotinterest" name="date">
                                <label for="dob">Date</label>

                            </div></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" value="6" class="btn myblue change_status">Save</td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>

<div id="open-modal7" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Converted </h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">

                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>Select User</td>
                        <td ><div class="input-field col s12 m6" style="margin-top: 0px;">

                                <div class="col s12 m12" style="margin-left: 20px">


                                    <select class="browser-default user_converted" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php

                                        foreach ($users as $user){

                                            ?>
                                            <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td ><div class="input-field col s12 m6">

                                <i class="material-icons prefix">watch_later</i>
                                <input type="date" class="datepicker" required id="dconverted" name="date">
                                <label for="dob">Date</label>

                            </div></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" value="4" class="btn myblue change_status">Save</td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>

<div id="open-modal5" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Contacted</h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">

                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">

                    <tr>
                        <td>Select User</td>
                        <td ><div class="input-field col s12 m6" style="margin-top: 0px;">

                                <div class="col s12 m12" style="margin-left: 20px">


                                    <select class="browser-default user_contacted" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php

                                        foreach ($users as $user){

                                            ?>
                                            <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td ><div class="input-field col s12 m6">

                                <i class="material-icons prefix">watch_later</i>
                                <input type="date" class="datepicker" required id="dcontacted" name="date">
                                <label for="dob">Date</label>


                            </div></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" value="2" class="btn myblue change_status">Save</td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>

<div id="open-modal6" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Interested</h1>
        <div>
            <table border="0">
                <form action="customer-profile" method="post">

                    <input type="hidden" id="model_report_type" value="<?php ?>"  name="report_type">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">

                    <tr>
                        <td>Select User</td>
                        <td ><div class="input-field col s12 m6" style="margin-top: 0px;">

                                <div class="col s12 m12" style="margin-left: 20px">


                                    <select class="browser-default user_interested" name="user[]" multiple tabindex="-1" style="width: 100% !important;" required> type
                                        <?php

                                        foreach ($users as $user){

                                            ?>
                                            <option <?php if($user_name==$user->u_id){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div></td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td ><div class="input-field col s12 m6">

                                <i class="material-icons prefix">watch_later</i>
                                <input type="date" class="datepicker" required id="dinterested" name="date">
                                <label for="dob">Date</label>


                            </div></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" value="3" class="btn myblue change_status">Save</td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>

<div id="open-modal3" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Add Area / Locality</h1>
        <div>
            <table border="0">
                <form method="post" id="localityForm">

                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>District</td>
                        <td ><select name="district_pop" class="browser-default district" tabindex="-1">
                                <?php
                                foreach ($districts as $district){
                                    $s_id = $district->id;
                                    ?>
                                    <option  value="<?=$s_id;?>"><?=$district->name?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Block</td>
                        <td ><select name="block_pop" class="browser-default block" tabindex="-1">
                                <?php
                                foreach ($blocks as $block){
                                    $s_id = $block->id;
                                    ?>
                                    <option  value="<?=$s_id;?>"><?=$block->block?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Locality Name</td>
                        <td ><input type="text" id="locality_pop" name="locality_pop" class="form-control" value=""></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" id="localitySave" class="btn myblue">Save</td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>
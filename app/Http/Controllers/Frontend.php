<?php

namespace App\Http\Controllers;

use App\AdviceCategory;
use App\AdviceFaq;
use App\AdviceLike;
use App\AdviceReview;
use App\Banner;
use App\BestForAdvice;
use App\Blocs;
use App\Blog;
use App\Brand;
use App\Browser;
use App\Category;
use App\Config;
use App\Countries;
use App\Country;
use App\Currency;
use App\Customer;
use App\CustomerCache;
use App\DefaultImage;
use App\DesignCategory;
use App\Footer;
use App\ImageInspiration;
use App\ImageLike;
use App\Lang;
use App\LayoutWishlist;
use App\Link;
use App\ManufacturingHubs;
use App\MegaMenuImage;
use App\Menu;
use App\Order;
use App\Os;
use App\PasswordReset;
use App\Payments;
use App\Products;
use App\ProductWishlist;
use App\RateRequests;
use App\Referrer;
use App\RegisterImage;
use App\ServiceCategory;
use App\ServiceWishlist;
use App\SpecialRequirements;
use App\Style;
use App\Tracking;
use App\UserProject;
use App\Visitor;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Node\Directory;
use Image;
use Illuminate\Support\Facades\Session;

//use Auth;

class Frontend extends FrontendBaseController
{
    public function index()
    {
        /*if(session('customer') !== null){
            if(customer('user_type') == 'institutional'){
                return redirect('institutional-index');
            }
        }*/
        $data['hub'] = 0;
        if(isset($_COOKIE['aakar_hub'])) {
            $data['hub'] = $_COOKIE['aakar_hub'];
        }
        $data['header'] = $this->retailHeader(false,false,false,true);
        $data['footer'] = $this->footer();
        $vtype=Session::get('aakar360.visitor_type');
        if($vtype == null){
            return redirect()->to('welcome');
        }elseif($vtype == 'institutional'){
            return redirect()->to('institutional-index');
        }
        $data['style'] = $this->style;
        if(preg_match("/(youtube.com)\/(watch)?(\?v=)?(\S+)?/", $this->style->media)){
            // Show video image from youtube
            parse_str(parse_url($this->style->media, PHP_URL_QUERY),$video);
            $data['media'] = '<a target="_blank" href="'.$this->style->media.'"><img class="landing-video" src="https://i3.ytimg.com/vi/'.$video['v'].'/mqdefault.jpg"></a>';
        } else {
            // Show image
            $data['media'] = '<img class="landing-image" src="'.$this->style->media.'">';
        }
        // Select the page builder blocs from database
        $blocs1 = Blocs::where('area','home')->first();
        if(!empty($blocs1)) {
            $data['blocs'] = $blocs1;
        }else{
            $data['blocs'] = 'Data Not Available';
        }
        $banners1 = Banner::where('section','home')->orderBy('priority','ASC')->get();
        if(count($banners1)){
            $data['banners'] = $banners1;
        }else{
            $data['banners'] = false;
        }
        $posts1 = Blog::orderBy('time','DESC')->take(6)->get();
        if(count($posts1)){
            $data['posts'] = $posts1;
        }else{
            $data['posts'] = 'Data Not Available';
        }
        $categoryforhomebrand1 = Category::where('popular_in_brands',1)->orderBy('popular_brands_priority','ASC')->take(1)->get();
        if(count($categoryforhomebrand1)){
            $data['categoryforhomebrand'] = $categoryforhomebrand1;
        }else{
            $data['categoryforhomebrand'] = 'Data Not Available';
        }
        $service_categories1 = ServiceCategory::where('parent',0)->orderBy('name','ASC')->get();
        if(!empty($service_categories1)){
            $data['service_categories'] = $service_categories1;
        }else{
            $data['service_categories'] = 'Data Not Available';
        }
        $design_categories1 = DesignCategory::where('parent',0)->orderBy('name','ASC')->get();
        if (!empty($design_categories1)){
            $data['design_categories'] = $design_categories1;
        }else{
            $data['design_categories'] = 'Data Not Available';
        }
        $show_preferences = false;
        $preferences = null;
        if(customer('id') !== ''){
            if(isset($_POST['categories'])){
                if($_POST['categories'] != ''){
                    $preferences = Customer::find(customer('id'))->preferences()->where('type', 'category')->first();
                    if($preferences === null){
                        $pre = new CustomerCache();
                        $pre->user_id = customer('id');
                        $pre->type = 'category';
                        $pre->cache = $_POST['categories'];
                        $pre->save();
                        return redirect('institutional-index');
                    }else{
                        $preferences->cache = $_POST['categories'];
                        $preferences->save();
                    }
                }
            }
            $pre = Customer::find(customer('id'))->preferences()->where('type', 'category')->first();
            if($pre === null){
                $preferences = null;
                $show_preferences = true;
            }else{
                $preferences = explode(',', $pre->cache);
            }
        }else{
            if(isset($_POST['categories'])){
                setcookie('categories', $_POST['categories'], time() + (86400 * 30));
                //Cookie::queue('aakar360.institutional.categories', $_POST['categories'], 17280);
                return redirect('institutional-index');
            }
            if(isset($_COOKIE['categories'])){
                $preferences = explode(',', $_COOKIE['categories']);
            }else{
                $preferences = null;
            }
            //dd($preferences);
            if($preferences === null){
                $show_preferences = true;
            }

        }
        $data['rem_pre'] = 0;
        if($preferences === null){
            $data['pcss'] = Category::where('popular', 1)->orderBy('popular_priority', 'ASC')->skip(0)->take(5)->get();
        }else{
            $data['pcss'] = Category::whereIn('id', $preferences)->orderBy('popular_priority', 'ASC')->skip(0)->take(5)->get();
            $re = count($data['pcss']);
            $limit = 5 - $re;
            if($limit){
                $more = Category::where('popular', 1)->orderBy('popular_priority', 'ASC')->skip(0)->take($limit)->get();
                $data['pcss'] = $data['pcss']->merge($more);
            }else{
                $data['rem_pre'] = $re-5;
            }
        }

        $data['tp'] = url("themes/default/");
       dd();
        return view('index')->with('data',$data);
    }
    public function getBrands(Request $request){
        $brands = $request->brands;
        $ret= array();
        $html = '';
        $categoryforhomebrand = Category::whereNotIn('id',$brands)->where('popular_in_brands',1)->orderBy('popular_brands_priority','ASC')->take(1)->get();
        if(count($categoryforhomebrand)){
            $topbrand = $categoryforhomebrand[0];
            $brands = $brands.','.$topbrand->id;
            $html = '<div class="col-md-12 col-sm-12 col-xs-12 marginTop30">
				<div class="brand-panel">
					<div class="brand-panel-title text-center">
						<h5>Top '.$topbrand->name.' Brands</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 brand-panel-left">
						<div class="img-container">
							<img src="assets/products/'.$topbrand->brand_banner.'"/>
						</div>
						<a class="btn btn-primary btn-above" href="'.url('products/?catlink='.$topbrand->id).'">Shop Now</a>
					</div>
					<div class="col-md-8 col-sm-12 col-xs-12 brand-panel-items new">
						<div class="agent-carousel" data-slick=\'{"slidesToShow": 4, "slidesToScroll": 1}\'>';
						$brand = $topbrand->brands;
						$i=1;
						$expbrands = explode(',',$brand);
						foreach($expbrands as $br){
							$bd = Brand::where('id',$br)->orderBy('id','DESC')->first();
							if($i%2 !=0)
								$html.='<div class="at-agent-col">';
								$html.='<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="hover-effect">
												<div>
													<a href="'.$bd->path.'" >
														<img src="assets/brand/'.$bd->image.'" >
													</a>
												</div>
											</div>
										</div>';
									if($i%2 == 0)
										$html.='</div>'; 
									$i++;
						}
						if($i%2 == 0)
							$html.='</div>'; 
				$html.='</div>
					</div>
				</div>
			</div>';
        }
        $ret['brands'] = $brands;
        $ret['html'] = $html;
        $nextCat = Category::whereNotIn('id',$brands)->where('popular_in_brands',1)->orderBy('id','DESC')->take(1)->get();
        if(count($nextCat)){
            $ret['collapse'] = false;
        }else{
            $ret['collapse'] = true;
        }
        return json_encode($ret);
    }
    public function getBrand(Request $request){
        $brands = $request->brands;
        $ret= array();
        $html = '';
        $categoryforhomebrand = Category::whereIn('id',$brands)->where('popular_in_brands',1)->orderBy('id','DESC')->take(1)->get();
        if(count($categoryforhomebrand)){
            $topbrand = $categoryforhomebrand[0];
            $brands = $brands.','.$topbrand->id;
            $html = '<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="brand-panel">
					<div class="brand-panel-title text-center">
						<h5>Top '.$topbrand->name.' Brands</h5>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 brand-panel-left">
						<div class="img-container">
							<img src="assets/products/'.$topbrand->brand_banner.'"/>
						</div>
						<a class="btn btn-primary btn-above" href="'.url('products/?catlink='.$topbrand->id).'">Shop Now</a>
					</div>
					<div class="col-md-8 col-sm-12 col-xs-12 brand-panel-items">
						<div class="agent-carousel" data-slick=\'{"slidesToShow": 4, "slidesToScroll": 1}\'>';
						$brand = $topbrand->brands;
						$i=1;
						$expbrands = explode(',',$brand);
						foreach($expbrands as $br){
							$bd = Brand::where('id',$br)->orderBy('id','DESC')->first();
							if($i%2 !=0)
								$html.='<div class="at-agent-col">';
								$html.='<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="hover-effect">
												<div>
													<a href="'.$bd->path.'" >
														<img src="assets/brand/'.$bd->image.'" >
													</a>
												</div>
											</div>
										</div>';
									if($i%2 == 0)
										$html.='</div>'; 
									$i++;
						}
						if($i%2 == 0)
							$html.='</div>'; 
				$html.='</div>
					</div>
				</div>
			</div>';
        }
        $ret['brands'] = $brands;
        $ret['html'] = $html;
        $nextCat = Category::whereNotIn('id',$brands)->where('popular_in_brands',1)->orderBy('id','DESC')->take(1)->get();
        if(count($nextCat)){
            $ret['collapse'] = false;
        }else{
            $ret['collapse'] = true;
        }
        return json_encode($ret);
    }
    public function cart()
    {
        if ($this->cfg->floating_cart == 1) {
            abort('404');
        }
        $header = $this->retailHeader(translate('Cart'),false,true);
        $footer = $this->footer();
        return view('cart')->with(compact('header','footer'));
    }
    public function checkout(){
        $header = $this->retailHeader(translate('Checkout'),false,true);
        $dimg = DefaultImage::where('page','Checkout')->first();
        $footer = $this->footer();
        return view('checkout')->with(compact('header','dimg','footer'));
    }
    public function serviceCheckout(){
        $header = $this->retailHeader(translate('Service Checkout'),false,true);
        $footer = $this->footer();
        return view('service_checkout')->with(compact('header','footer'));
    }

    public function planCheckout(){
        $header = $this->retailHeader(translate('Service Checkout'),false,true);
        $dimg = DefaultImage::where('page','Checkout')->first();
        $footer = $this->footer();
        return view('plan_checkout')->with(compact('header','dimg','footer'));
    }

    public function register($slug,$title = false,$desc = false,$page = false,$landing = false,$bg = false)
    {

        $all_img = RegisterImage::get();
        $seller_img='';
        $professional_img = '';
        $cusotmer_img = '';
        $institutional_img = '';
        foreach($all_img as $img){
            if($img->user_type=='Seller'){
                $seller_img = $img->image;
            }
            if($img->user_type=='Professional'){
                $professional_img = $img->image;
            }
            if($img->user_type=='Customer'){
                $cusotmer_img = $img->image;
            }
            if($img->user_type=='Institutional'){
                $institutional_img = $img->image;
            }
            if($img->user_type=='Institutional'){
                $institutional_img = $img->image;
            }
        }
        $data = array();
        if($slug=='institutional'){
            $data['img'] = $institutional_img;
            $data['rtitle'] = 'Institutional';
            $data['type'] = 'institutional';
        }
        else if($slug=='seller'){
            $data['img'] = $seller_img;
            $data['rtitle'] = 'Manufacture / Supplier';
            $data['type'] = 'seller';
        }else if($slug=='individual'){
            $data['img'] = $cusotmer_img;
            $data['rtitle'] = 'Individual';
            $data['type'] = 'individual';
        }else if($slug=='professional'){
            $data['img'] = $professional_img;
            $data['rtitle'] = 'Professional';
            $data['type'] = 'professional';
        }else if($slug=='dealer'){
            $data['img'] = $institutional_img;
            $data['rtitle'] = 'Dealer';
            $data['type'] = 'dealer';
        }
        if($this->cfg->registration == 0 || session('customer') != '' || session('institutional') != '') {
            abort('404');
        }
        if(isset($_POST['register'])){

            if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['confirm_password']) && !empty($_POST['user_type'] && !empty($_POST['mobile']))){
                $name = escape(htmlspecialchars($_POST['name']));
                $email = escape(htmlspecialchars($_POST['email']));
                $user_type = escape(htmlspecialchars($_POST['user_type']));
                $password = md5($_POST['password']);
                $mobile = escape(htmlspecialchars($_POST['mobile']));
                if (strlen($_POST["password"]) <= 8) {
                    $data['error'] = "Your Password Must Contain At Least 8 Characters!";
                }
                else if(!preg_match("#[0-9]+#",$_POST["password"])) {
                    $data['error'] = "Your Password Must Contain At Least 1 Number!";
                }
                else if(!preg_match("#[A-Z]+#",$_POST["password"])) {
                    $data['error'] = "Your Password Must Contain At Least 1 Capital Letter!";
                }
                else if(!preg_match("#[a-z]+#",$_POST["password"])) {
                    $data['error'] = "Your Password Must Contain At Least 1 Lowercase Letter!";
                }else if($_POST['password'] != $_POST['confirm_password']){
                    $data['error'] = "Password mismatch!";
                }else if(Customer::where('email',$email)->count() > 0) {
                    $data['error'] = 'This email is already registered!';
                } else{
                    if($_POST['user_type']=='institutional'){
                        $address_line_1 = escape(htmlspecialchars($_POST['address_line_1']));
                        $gst = escape(htmlspecialchars($_POST['gst']));
                        $cont_person_name = escape(htmlspecialchars($_POST['cont_person_name']));
                        $cont_person_phone = escape(htmlspecialchars($_POST['cont_person_phone']));

                        $customer = new Customer();
                        $customer->name = $name;
                        $customer->email = $email;
                        $customer->password = $password;
                        $customer->user_type = $user_type;
                        $customer->sid = md5(microtime());
                        $customer->address_line_1 = $address_line_1;
                        $customer->gst = $gst;
                        $customer->cont_person_name = $cont_person_name;
                        $customer->cont_person_phone = $cont_person_phone;
                        $customer->save();
                        $data['error'] = 'Register Successfully ! Please Wait For Approval.';
                    }else{
                        $customer = new Customer();
                        $customer->name = $name;
                        $customer->email = $email;
                        $customer->password = $password;
                        $customer->user_type = $user_type;
                        $customer->sid = md5(microtime());
                        $customer->save();
                        $data['error'] = 'Register Successfully !';
                    }
                }
            } else {
                $data['error'] = 'All fields are required !';
            }
        }
        $data['header'] = $this->retailHeader(translate('Registration'),false,true);
        $data['dimg'] = DefaultImage::where('page','Register')->first();
        $data['footer'] = $this->footer();
        $data['title'] = ($title) ? translate($title).' | '.translate($this->cfg->name) : translate($this->cfg->name);
        $data['desc'] = ($desc) ? $desc : $this->cfg->desc;
        $data['keywords'] = $this->cfg->key;
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['cfg'] = $this->cfg;
        return view('register')->with('data',$data);
    }

    public function changePassword()
    {
        $data['error'] = '';
        if(isset($_POST['login'])) {
            if (!empty($_POST['oldpassword']) && !empty($_POST['confirm_password']) && !empty($_POST['password'])) {
                // Escape the user entries and insert them to database
                $password = md5($_POST['password']);
                $opassword = md5($_POST['oldpassword']);
                $old = Customer::where('password',$opassword)->first();
                if (!empty($old)) {
                    if ($_POST['password'] != $_POST['confirm_password']) {
                        $error = '<div class="alert alert-warning">Password Missmatch!</div>';
                    } else {
                        $visitor = Customer::where('id',$old->id)->first();
                        $visitor->password = $password;
                        $visitor->save();
                        $data['error'] = '<div class="alert alert-success">Password Successfully Changed</div>';
                    }
                }
            } else {
                $data['error'] = '<div class="alert alert-warning">All fields are required !</div>';
            }
        }
        $data['header'] = $this->retailHeader(translate('Change Password'),false,true);
        $data['dimg'] = DefaultImage::where('page','Change Password')->first();
        $data['footer'] = $this->footer();
        return view('change-password')->with($data);
    }

    public function login($title = false,$desc = false,$page = false,$landing = false,$bg = false, Request $request)
    {
        if ($this->cfg->registration == 0) {
            abort('404');
        }
        $error = '';
        if(isset($_POST['login'])){
            // Check email and password and redirect to the account
            $email = escape($_POST['email']);
            $pass = md5($_POST['password']);
            if (strlen($_POST["password"]) <= 8) {
                $error = "Your Password Must Contain At Least 8 Characters!";
            }
            else if(!preg_match("#[0-9]+#",$_POST["password"])) {
                $error = "Your Password Must Contain At Least 1 Number!";
            }
            else if(!preg_match("#[A-Z]+#",$_POST["password"])) {
                $error = "Your Password Must Contain At Least 1 Capital Letter!";
            }
            else if(!preg_match("#[a-z]+#",$_POST["password"])) {
                $data['error'] = "Your Password Must Contain At Least 1 Lowercase Letter!";
            }else if(empty($email) or empty($pass)){
                $error = 'All fields are required';
            } else {
                if(Customer::where('email',$email)->where('password',$pass)->count() > 0) {
                    $cust = Customer::where('email',$email)->where('password',$pass)->first();
                    $secure_id = md5(microtime());
                    if($cust->user_type == 'institutional'){
                        if($cust->status == 0){
                            $error = 'Wait For Approval';
                        }
                        else{
                            $cus = Customer::where('email',$email)->first();
                            $cus->sid = $secure_id;
                            $cus->save();
                            session(['institutional' => $secure_id]);
                            if(session('redirect') !== null){
                                return redirect(session('redirect'));
                            }
                            return redirect('i/index');
                        }
                    }

                    // Generate a new secure ID for this session and redirect to dashboard
                    $cus = Customer::where('email',$email)->first();
                    $cus->sid = $secure_id;
                    $cus->save();
                    $preferences = '';
                    if(isset($_COOKIE['cities'])){
                        $preferences = explode(',', $_COOKIE['cities']);
                    }else{
                        $preferences = null;
                    }

                    Cookie::forever('cities', $preferences);
                    if(session('redirect') !== null){
                        return redirect(session('redirect'));
                    }
                    if(customer('user_type') == 'customer') {
                        session(['customer' => $secure_id]);
                        return redirect('account');
                    }elseif (customer('user_type') == 'seller'){
                        session(['customer' => $secure_id]);
                        return redirect('seller_account');
                    }elseif (customer('user_type') == 'professional'){
                        session(['customer' => $secure_id]);
                        return redirect('professional_account');
                    }elseif (customer('user_type') == 'institutional'){
                        session(['institutional' => $secure_id]);
                        return redirect('i/index');
                    }
                } else {
                    $error = 'Wrong email or password';
                }

            }
        }else{
            if($request->server('HTTP_REFERER') !== null){
                $app_url =  env('APP_URL') !== null ? env('APP_URL') : 'http://localhost/aakar360';
                if(strpos($request->server('HTTP_REFERER'), $app_url) !== false && $request->server('HTTP_REFERER') !== url('/')) {
                    session(['redirect' => $request->server('HTTP_REFERER')]);
                }else{
                    session(['redirect' => null]);
                }
            }
        }
        $header = $this->retailHeader(translate('Login'),false,true);
        $dimg = DefaultImage::where('page', 'Login')->first();
        $footer = $this->footer();

        $data['title'] = ($title) ? translate($title).' | '.translate($this->cfg->name) : translate($this->cfg->name);
        $data['desc'] = ($desc) ? $desc : $this->cfg->desc;
        $data['keywords'] = $this->cfg->key;
        $data['tp'] = url("/themes/".$this->cfg->theme);
        $data['cfg'] = $this->cfg;

        return view('login')->with(compact('header','error','dimg','footer','data'))->render();
    }

    public function likeImage(){
        $user_id = customer('id');
        $image_id = $_POST['image_id'];
        $il = new ImageLike();
        $il->user_id = $user_id;
        $il->image_id = $image_id;
        $il->save();
    }

    public function ImageInspiration(){
        $user_id = customer('id');
        $image_id = $_POST['image_id'];
        $html = '';
        $pro = UserProject::where('user_id',$user_id)->get();
        if($pro !== ''){
            foreach($pro as $p){
                $html .='<div class="item col-lg-3">
                    <a href="javascript:void(0)" onclick="addProject(\''.$p->id.'\');" class="serviceImg" style="width: 100%;height: auto;margin:0;">
                        <img src="'.url('assets/projects/'.$p->image).'" style="width: 100%;height: 145px;"/>
                        <span class="serviceImgtitle" style="font-size: 16px;">'.$p->title.'</span>
                        <span class="overlay"></span>
                    </a>
                </div>';
            }
        }
        return $html;
    }


    public function myProjects(){
        $html = '';
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('View Projects'),false,true);
        $pros = UserProject::where('user_id',customer('id'))->orderBy('id','DESC')->get();
        $footer = $this->footer();
        $countries = Countries::orderBy('id','ASC')->get();
        return view('my-projects')->with(compact('header','pros','footer','countries'))->render();
    }
    
    public function delProject(){
        $id = $_GET['id'];
        $pros = UserProject::where('user_id',customer('id'))->where('id',$id)->delete();
        return back();
    }

    public function delPic(){
        $id = $_GET['id'];
        $pros = ImageInspiration::where('user_id',customer('id'))->where('id',$id)->delete();
        return back();
    }

    public function delLay(){
        $id = $_GET['id'];
        $pros = LayoutWishlist::where('user_id',customer('id'))->where('id',$id)->delete();
        return back();
    }

    public function delSer(){
        $id = $_GET['id'];
        $pros = ServiceWishlist::where('user_id',customer('id'))->where('id',$id)->delete();
        return back();
    }

    public function delPro(){
        $id = $_GET['id'];
        $pros = ProductWishlist::where('user_id',customer('id'))->where('id',$id)->delete();
        return back();
    }


    public function showProject(){
        $user_id = customer('id');
        $project_id = $_GET['id'];
        $html = '';
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('View Projects'),false,true);
        $pro = ImageInspiration::where('project_id',$project_id)->where('user_id',$user_id)->get();
        $prod = ProductWishlist::where('project_id',$project_id)->where('user_id',$user_id)->get();
        $proser = ServiceWishlist::where('project_id',$project_id)->where('user_id',$user_id)->get();
        $lay = LayoutWishlist::where('project_id',$project_id)->where('user_id',$user_id)->get();

        $footer = $this->footer();
        return view('view_projects')->with(compact('header','pro','lay','prod','proser','footer'))->render();
    }

    public function showProProject(){
        $user_id = customer('id');
        $project_id = $_POST['project_id'];
        $html = '';
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('View Projects'),false,true);
        $pro = ProductWishlist::where('project_id',$project_id)->where('user_id',$user_id)->get();

        $footer = $this->footer();
        return view('view_pro_projects')->with(compact('header','pro','footer'))->render();
    }

    public function showSerProject(){
        $user_id = customer('id');
        $project_id = $_POST['project_id'];
        $html = '';
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('View Projects'),false,true);
        $pro = ServiceWishlist::where('project_id',$project_id)->where('user_id',$user_id)->get();

        $footer = $this->footer();
        return view('view_ser_projects')->with(compact('header','pro','footer'))->render();
    }

    public function addToProject(){
        $user_id = customer('id');
        $image_id = $_POST['image_id'];
        $project_id = $_POST['project_id'];
        $ii = new ImageInspiration();
        $ii->user_id = $user_id;
        $ii->image_id = $image_id;
        $ii->project_id = $project_id;
        $ii->save();
        return back();
    }

    public function addSubSerProject(){
        $user_id = customer('id');
        $project_id = $_POST['project_id'];
        $sub_service_id = $_POST['sub_service_id'];
        $sw = new ServiceWishlist();
        $sw->user_id = $user_id;
        $sw->sub_service_id = $sub_service_id;
        $sw->project_id = $project_id;
        $sw->save();
        return back();
    }

    public function addSerProject(){
        $user_id = customer('id');
        $project_id = $_POST['project_id'];
        $service_id = $_POST['service_id'];
        $sw = new ServiceWishlist();
        $sw->user_id = $user_id;
        $sw->service_id = $service_id;
        $sw->project_id = $project_id;
        $sw->save();
        return back();
    }

    public function addLayProject(){
        $user_id = customer('id');
        $project_id = $_POST['project_id'];
        $layout_id = $_POST['layout_id'];
        $lw = new LayoutWishlist();
        $lw->user_id = $user_id;
        $lw->layout_id = $layout_id;
        $lw->save();
        return back();
    }

    public function addProProject(){
        $user_id = customer('id');
        $project_id = $_POST['project_id'];
        $product_id = $_POST['product_id'];
        $pw = new ProductWishlist();
        $pw->user_id = $user_id;
        $pw->product_id = $product_id;
        $pw->project_id = $project_id;
        $pw->save();
        return back();
    }

    public function layoutWishlist(){
        $user_id = customer('id');
        $html = '';
        $pro = UserProject::where('user_id',$user_id)->get();
        if($pro !== ''){
            foreach($pro as $p){
                $html .='<div class="item col-lg-3">
                    <a href="javascript:void(0)" onclick="addLayProject(\''.$p->id.'\');" id="ghanta" class="serviceImg" style="width: 100%;height: auto;margin:0;">
                        <img src="'.url('assets/projects/'.$p->image).'" style="width: 100%;height: 145px;"/>
                        <span class="serviceImgtitle" style="font-size: 16px;">'.$p->title.'</span>
                        <span class="overlay"></span>
                    </a>
                </div>';
            }
        }
        return $html;
        //$images = DB::insert("INSERT INTO layout_wishlist (user_id, layout_id) VALUES ('".$user_id."','".$layout_id."')");
    }

    public function productWishlist(){
        $user_id = customer('id');
        $html = '';
        $subid = $_POST['product_id'];
        $pro = UserProject::where('user_id',$user_id)->get();
        if($pro !== ''){
            foreach($pro as $p){
                $html .='<div class="item col-lg-3">
                    <a href="javascript:void(0)" onclick="addProject(\''.$p->id.'\');" id="ghanta" data-id="'.$subid.'" class="serviceImg" style="width: 100%;height: auto;margin:0;">
                        <img src="'.url('assets/projects/'.$p->image).'" style="width: 100%;height: 145px;"/>
                        <span class="serviceImgtitle" style="font-size: 16px;">'.$p->title.'</span>
                        <span class="overlay"></span>
                    </a>
                </div>';
            }
        }
        return $html;
    }

    public function serviceWishlist(){
        $user_id = customer('id');
        $html = '';
        $pro = UserProject::where('user_id',$user_id)->get();
        if($pro !== ''){
            foreach($pro as $p){
                $html .='<div class="item col-lg-3">
                    <a href="javascript:void(0)" onclick="addSerProject(\''.$p->id.'\');" class="serviceImg" style="width: 100%;height: auto;margin:0;">
                        <img src="'.url('assets/projects/'.$p->image).'" style="width: 100%;height: 145px;"/>
                        <span class="serviceImgtitle" style="font-size: 16px;">'.$p->title.'</span>
                        <span class="overlay"></span>
                    </a>
                </div>';
            }
        }
        return $html;
        //$images = DB::insert("INSERT INTO service_wishlist (user_id, service_id, sub_service_id) VALUES ('".$user_id."','".$service_id."','".$sub_service_id."')");
    }

    public function subserviceWishlist(){
        $user_id = customer('id');
        $html = '';
        $subid = $_POST['image_id'];
        $pro = UserProject::where('user_id',$user_id)->get();
        if($pro !== ''){
            foreach($pro as $p){
                $html .='<div class="item col-lg-3">
                    <a href="javascript:void(0)" onclick="addProject(\''.$p->id.'\');" id="ghanta" data-id="'.$subid.'" class="serviceImg" style="width: 100%;height: auto;margin:0;">
                        <img src="'.url('assets/projects/'.$p->image).'" style="width: 100%;height: 145px;"/>
                        <span class="serviceImgtitle" style="font-size: 16px;">'.$p->title.'</span>
                        <span class="overlay"></span>
                    </a>
                </div>';
            }
        }
        return $html;
    }

    public function loginModal()
    {
        if ($this->cfg->registration == 0) {
            abort('404');
        }
        if(isset($_POST['login'])){
            // Check email and password and redirect to the account
            $email = escape($_POST['email']);
            $pass = md5($_POST['password']);
            if(empty($email) or empty($pass)){
                $error = 'All fields are required';
                request()->session()->put('error', $error);
//                return redirect('sub_service/'.path($service_id).'');
            } else {
                if(Customer::where('email',$email)->where('password',$pass)->count() > 0) {
                    // Generate a new secure ID for this session and redirect to dashboard
                    $secure_id = md5(microtime());
                    $cus = Customer::where('email',$email)->first();
                    $cus->sid = $secure_id;
                    $cus->save();
                    session(['customer' => $secure_id]);
                    return back();
                } else {
                    $error = 'Wrong email or password';
                    request()->session()->put('error', $error);
                    return back();
                }
            }
        }
    }


    public function reset_password($token = false)
    {
        if (session('customer') != '') {
            abort('404');
        }
        $error = '';
        if(isset($_POST['send'])){
            $email = escape($_POST['email']);
            if(empty($email)){
                $error = 'All fields are required';
            } else {
                if(Customer::where('email',$email)->count() > 0) {
                    $customer = Customer::where('email',$email)->select('id','name')->first();
                    $key = md5(uniqid());
                    $pr = new PasswordReset();
                    $pr->customer = $customer->id;
                    $pr->key = $key;
                    $pr->save();
                    mailing('reset-password',array('link'=>url('reset-password/'.$key)),'Reset your password',$email);
                    $error = false;
                } else {
                    $error = 'This account does not exist';
                }
            }
        }
        if($token){
            if(PasswordReset::where('key',escape($token))->count() == 0) {
                abort(404);
            }
            $id = PasswordReset::where('key',escape($token))->select('customer')->first();
            if(isset($_POST['reset'])){
                $pass = md5($_POST['password']);
                if(empty($_POST['password'])){
                    $error = 'Please insert your new password';
                } else {
                    $secure_id = md5(microtime());
                    $cus = Customer::where('id',$id)->first();
                    $cus->sid = $secure_id;
                    $cus->password = $pass;
                    $cus->save();
                    PasswordReset::where('customer')->delete();
                    DB::delete("DELETE FROM password_resets WHERE customer = '".$id."'");
                    session(['customer' => $secure_id]);
                    return redirect('account');
                }
            }
            $header = $this->retailHeader('Reset your password',false,true);
            $footer = $this->footer();
            return view('reset-password')->with(compact('header','error','footer'))->render();
        }
        $header = $this->retailHeader('Forgot password ?',false,true);
        $dimg = DB::table('default_image')->where('page', 'Register')->first();
        $footer = $this->footer();
        return view('forgot-password')->with(compact('header','error','dimg','footer'))->render();
    }
    public function account()
    {
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Account'),false,true);
        if(isset($_POST['edit'])){

            $name = $_POST['name'];
            $mobile = $_POST['mobile'];
            $alternate_mobile = $_POST['alternate_mobile'];
            $email = $_POST['email'];
            $company = $_POST['company'];
            $address_line_1 = $_POST['address_line_1'];
            $address_line_2 = $_POST['address_line_2'];
            $city = $_POST['city'];
            $state = $_POST['state'];
            $country = $_POST['country'];
            $postcode = $_POST['postcode'];
            $id = $_POST['id'];

            if (request()->file('header_image')) {
                // Upload the downloadable file to product downloads directory
                $name1 = request()->file('header_image')->getClientOriginalName();
                //dd($name);
                $file = md5(time()).'1.'.request()->file('header_image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('header_image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);

                DB::update("UPDATE customers SET header_image = '".$file."' WHERE id = '".$id."'");
            }

            if (request()->file('header_image_1')) {
                $name2 = request()->file('header_image_1')->getClientOriginalName();
                $file1 = md5(time()).'2.'.request()->file('header_image_1')->getClientOriginalExtension();
                $path1 = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('header_image_1')->move($path1,$file1);
                $img = Image::make($path1.$file1)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path1.$file1);

                DB::update("UPDATE customers SET header_image_1 = '".$file1."' WHERE id = '".$id."'");
            }

            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name3 = request()->file('image')->getClientOriginalName();
                $file2 = md5(time()).'3.'.request()->file('image')->getClientOriginalExtension();
                $path2 = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('image')->move($path2,$file2);
                $img = Image::make($path2.$file2)->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path2.$file2);

                DB::update("UPDATE customers SET image = '".$file2."' WHERE id = '".$id."'");
            }


            DB::update("UPDATE customers SET name = '$name',mobile = '$mobile',alternate_mobile = '$alternate_mobile',
                email = '$email',company = '$company',address_line_1 = '$address_line_1',address_line_2 = '$address_line_2',
                 city = '$city',state = '$state',country = '$country',postcode = '$postcode' WHERE id = '".$id."'");
        }

        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");
        $porders = DB::select("SELECT * FROM layout_order WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $sorders = DB::select("SELECT * FROM services_answer WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $prowishlist = DB::select("SELECT * FROM product_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $planwishlist = DB::select("SELECT * FROM layout_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $servicewishlist = DB::select("SELECT * FROM service_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $imglike = DB::select("SELECT * FROM image_like WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $imgins = DB::select("SELECT * FROM image_inspiration WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        return view('account')->with(compact('header','orders','porders','sorders','imglike','imgins','dimg','servicewishlist','prowishlist','planwishlist','footer','tp','cities','states','countries'))->render();
    }

    public function professionalaccount()
    {
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Account'),false,true);
        if(isset($_POST['submit'])){
            $name = $_POST['name'];
            $mobile = $_POST['mobile'];
            $alternate_mobile = $_POST['alternate_mobile'];
            $email = $_POST['email'];
            $id = $_POST['id'];

            if (request()->file('header_image')) {
                // Upload the downloadable file to product downloads directory
                $name1 = request()->file('header_image')->getClientOriginalName();
                //dd($name);
                $file = md5(time()).'1.'.request()->file('header_image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('header_image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);

                DB::update("UPDATE customers SET header_image = '".$file."' WHERE id = '".$id."'");
            }

            if (request()->file('header_image_1')) {

                $name2 = request()->file('header_image_1')->getClientOriginalName();
                $file1 = md5(time()).'2.'.request()->file('header_image_1')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('header_image_1')->move($path,$file1);
                $img = Image::make($path.$file1)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file1);

                DB::update("UPDATE customers SET header_image_1 = '".$file1."' WHERE id = '".$id."'");
            }

            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name3 = request()->file('image')->getClientOriginalName();
                $file2 = md5(time()).'3.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('image')->move($path,$file2);
                $img = Image::make($path.$file2)->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file2);

                DB::update("UPDATE customers SET image = '".$file2."' WHERE id = '".$id."'");
            }

            DB::update("UPDATE customers SET name = '$name',mobile = '$mobile',alternate_mobile = '$alternate_mobile',
                email = '$email' WHERE id = '".$id."'");
        }
        $orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");

        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        return view('professional_account')->with(compact('header','orders','dimg','footer','tp','cities','states','countries'))->render();
    }
    public function professionalbusiness()
    {
        if(isset($_POST['submit'])){
            $company = $_POST['company'];
            $cont_person_name = $_POST['cont_person_name'];
            $cont_person_email = $_POST['cont_person_email'];
            $cont_person_phone = $_POST['cont_person_phone'];
            $cont_person_designtion = $_POST['cont_person_designation'];
            $gst = $_POST['gst'];
            $pan = $_POST['pan'];
            $address_line_1 = $_POST['address_line_1'];
            $address_line_2 = $_POST['address_line_2'];
            $city = $_POST['city'];
            $state = $_POST['state'];
            $country = $_POST['country'];
            $postcode = $_POST['postcode'];
            $id = $_POST['id'];


            DB::update("UPDATE customers SET company = '$company',address_line_1 = '$address_line_1',address_line_2 = '$address_line_2',
                 city = '$city',state = '$state',country = '$country',postcode = '$postcode', cont_person_name = '$cont_person_name',
                  cont_person_email = '$cont_person_email', cont_person_phone = '$cont_person_phone', cont_person_designation = '$cont_person_designtion',
                   gst = '$gst', pan = '$pan' WHERE id = '".$id."'");
        }
        return back();
    }

    public function professionalbank()
    {
        if(isset($_POST['submit'])){
            $holder_name = $_POST['holder_name'];
            $ac_no = $_POST['ac_no'];
            $bank_name = $_POST['bank_name'];
            $branch_name = $_POST['branch_name'];
            $ifsc = $_POST['ifsc'];
            $id = $_POST['id'];

            DB::update("UPDATE customers SET holder_name = '$holder_name', ac_no = '$ac_no', bank_name = '$bank_name',
                 branch_name = '$branch_name', ifsc = '$ifsc' WHERE id = '".$id."'");
        }
        return back();
    }



    public function sellerbusiness()
    {
        if(isset($_POST['submit'])){
            $company = $_POST['company'];
            $cont_person_name = $_POST['cont_person_name'];
            $cont_person_email = $_POST['cont_person_email'];
            $cont_person_phone = $_POST['cont_person_phone'];
            $cont_person_designtion = $_POST['cont_person_designation'];
            $gst = $_POST['gst'];
            $pan = $_POST['pan'];
            $address_line_1 = $_POST['address_line_1'];
            $address_line_2 = $_POST['address_line_2'];
            $city = $_POST['city'];
            $state = $_POST['state'];
            $country = $_POST['country'];
            $postcode = $_POST['postcode'];
            $id = $_POST['id'];


            DB::update("UPDATE customers SET company = '$company',address_line_1 = '$address_line_1',address_line_2 = '$address_line_2',
                 city = '$city',state = '$state',country = '$country',postcode = '$postcode', cont_person_name = '$cont_person_name',
                  cont_person_email = '$cont_person_email', cont_person_phone = '$cont_person_phone', cont_person_designation = '$cont_person_designtion',
                   gst = '$gst', pan = '$pan' WHERE id = '".$id."'");
        }
        return back();
    }

    public function sellerbank()
    {
        if(isset($_POST['submit'])){
            $holder_name = $_POST['holder_name'];
            $ac_no = $_POST['ac_no'];
            $bank_name = $_POST['bank_name'];
            $branch_name = $_POST['branch_name'];
            $ifsc = $_POST['ifsc'];
            $id = $_POST['id'];

            DB::update("UPDATE customers SET holder_name = '$holder_name', ac_no = '$ac_no', bank_name = '$bank_name',
                 branch_name = '$branch_name', ifsc = '$ifsc' WHERE id = '".$id."'");
        }
        return back();
    }

    public function sellerstore()
    {
        if(isset($_POST['submit'])){
            $display_name = $_POST['display_name'];
            $business_description = $_POST['business_description'];
            $id = $_POST['id'];

            DB::update("UPDATE customers SET display_name = '$display_name', business_description = '$business_description' WHERE id = '".$id."'");
        }
        return back();
    }

    public function myorders()
    {
        $header = $this->retailHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");
        $footer = $this->footer();
        $prowishlist = DB::select("SELECT * FROM product_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        return view('myorders')->with(compact('header','orders','prowishlist','footer','dimg','pros','cities','states','countries'))->render();
    }
 
    public function userAdvices()
    {
        $header = $this->retailHeader(translate('Account'),false,true);
        $saved = DB::select("SELECT * FROM advices_save WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $footer = $this->footer();
        $liked = DB::select("SELECT * FROM advices_like WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        return view('useradvices')->with(compact('header','saved','liked','footer','dimg','cities','states','countries'))->render();
    }

    public function sellerAdvices()
    {
        $header = $this->retailHeader(translate('Account'),false,true);
        $saved = DB::select("SELECT * FROM advices_save WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $footer = $this->footer();
        $liked = DB::select("SELECT * FROM advices_like WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        return view('seller_advices')->with(compact('header','saved','liked','footer','dimg','cities','states','countries'))->render();
    }


    public function professionalproduct()
    {
        $header = $this->retailHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");
        $footer = $this->footer();
        $prowishlist = DB::select("SELECT * FROM product_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        return view('professional_product')->with(compact('header','orders','prowishlist','pros','dimg','footer','cities','states','countries'))->render();
    }

    public function professionaldesign()
    {
        $header = $this->retailHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM layout_order WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $planwishlist = DB::select("SELECT * FROM layout_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $imgins = DB::select("SELECT * FROM image_inspiration WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $amminities = DB::select("SELECT * FROM amminities ORDER BY id DESC");
        $categories = DB::select("SELECT * FROM design_category WHERE parent = 0 ORDER BY id DESC");
        $plans = DB::select("SELECT * FROM layout_plans WHERE added_by = ".customer('id')." ORDER BY id DESC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        return view('professional_design')->with(compact('header','orders','planwishlist','plans','pros','dimg','imgins','categories','amminities','footer','cities','states','countries'))->render();
    }

    public function planOrders()
    {
        $header = $this->retailHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM layout_order WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $planwishlist = DB::select("SELECT * FROM layout_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $imgins = DB::select("SELECT * FROM image_inspiration WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        return view('planorders')->with(compact('header','orders','planwishlist','imgins','dimg','pros','footer','cities','states','countries'))->render();
    }

    public function serviceOrders()
    {
        $header = $this->retailHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM services_answer WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $servicewishlist = DB::select("SELECT * FROM service_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $footer = $this->footer();
        return view('serviceorders')->with(compact('header','orders','dimg','pros','servicewishlist','footer','cities','states','countries'))->render();
    }

    public function professionalservices()
    {
        $header = $this->retailHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM services_answer WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $servicewishlist = DB::select("SELECT * FROM service_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $cus_id = customer('id');
        $services = DB::select("SELECT * FROM services WHERE added_by = '".$cus_id."' ORDER BY id ASC");



        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $parentsx = DB::select("SELECT * FROM services_category WHERE parent <> 0");
        $footer = $this->footer();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        return view('professional_services')->with(compact('header','orders','servicewishlist','dimg','pros','parentsx','services','footer','cities','states','countries'))->render();
    }

    public function professionaldesignadd()
    {
        $notices = '';
        $data['title'] = $_POST['title'];
        $data['slug'] = makeSlugOf($data['title'], 'layout_plans');
        $data['plan_code'] = $_POST['plan_code'];
        $data['description'] = $_POST['description'];
        $data['specification'] = $_POST['specification'];
        $data['price'] = $_POST['price'];
        $data['p1_name'] = $_POST['p1_name'];
        $data['p1_price'] = $_POST['p1_price'];
        $data['p1_duration'] = $_POST['p1_duration'];
        $data['p1_description'] = $_POST['p1_description'];
        $data['p2_name'] = $_POST['p2_name'];
        $data['p2_price'] = $_POST['p2_price'];
        $data['p2_duration'] = $_POST['p2_duration'];
        $data['p2_description'] = $_POST['p2_description'];
        $data['p3_name'] = $_POST['p3_name'];
        $data['p3_price'] = $_POST['p3_price'];
        $data['p3_duration'] = $_POST['p3_duration'];
        $data['p3_description'] = $_POST['p3_description'];
        $data['added_by'] = $_POST['added_by'];
        $data['status'] = 'Pending';

        $ammi = array();
        $x =0;
        foreach ($_POST['ammenities'] as $ammen){
            $ammi[$ammen] = $_POST['interior_ammenities'][$x];
            $x++;
        }
        //$ammi = array($key = $_POST['ammenities'], $value = $_POST['interior_ammenities']);
        //dd($ammi);


        $data['interior_ammenities'] = json_encode($ammi);
        $data['category'] = (int)$_POST['category'];
        $data['drawing_views'] = json_encode($_POST['drawing_views']);
        $data['file'] = '';
        $data['images'] = '';

        $layout_plan = DB::table('layout_plans')->insertGetId($data);
        if (request()->file('images')) {
            // Upload selected images to product assets directory
            $order = 0;
            $images = array();
            foreach (request()->file('images') as $file) {
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                    $file->move($path,$image);
                    if($image) {
                        $img = Image::make($path . $image)->resize(800, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($path . $image);
                        $order++;
                    }
                } else {
                    $notices .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
            }
            DB::update("UPDATE layout_plans SET images = '".implode(',',$images)."' WHERE id = '".$layout_plan."'");
        }
        if (request()->file('file') || request()->file('file1')) {
            // Upload the downloadable file to product downloads directory
            $name = request()->file('file')->getClientOriginalName();
            $file = md5(time()).'.'.request()->file('file')->getClientOriginalExtension();
            $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
            request()->file('file')->move($path,$file);
            $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.$file);

            $name = request()->file('file1')->getClientOriginalName();
            $file1 = md5(time()).'.'.request()->file('file1')->getClientOriginalExtension();
            $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
            request()->file('file1')->move($path,$file1);
            $img = Image::make($path.$file1)->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.$file1);

            $nfile = $file.','.$file1;
            DB::update("UPDATE layout_plans SET file = '".$nfile."' WHERE id = '".$layout_plan."'");
        }
    }

    public function professionaledit()
    {
        $notices = '';
        $title = escape($_POST["title"]);
        $plan_code = $_POST['plan_code'];
        $description = escape($_POST['description']);
        $specification = escape($_POST['specification']);
        $price = $_POST['price'];
        $tax = $_POST['tax'];
        $p1_name = $_POST['p1_name'];
        $p1_price = $_POST['p1_price'];
        $p1_duration = $_POST['p1_duration'];
        $p1_description = $_POST['p1_description'];
        $p2_name = $_POST['p2_name'];
        $p2_price = $_POST['p2_price'];
        $p2_duration = $_POST['p2_duration'];
        $p2_description = $_POST['p2_description'];
        $p3_name = $_POST['p3_name'];
        $p3_price = $_POST['p3_price'];
        $p3_duration = $_POST['p3_duration'];
        $p3_description = $_POST['p3_description'];
        $id = $_POST['id'];

        $ammi = array();
        $x =0;
        foreach ($_POST['ammenities'] as $ammen){
            $ammi[$ammen] = $_POST['interior_ammenities'][$x];
            $x++;
        }

        $interior_ammenities = json_encode($ammi);
        $category = (int)$_POST['category'];
        $drawing_views = json_encode($_POST['drawing_views']);
        $file = '';
        $images = '';


        DB::update("UPDATE layout_plans SET plan_code='$plan_code', title = '$title', price = '$price', 
			tax = '$tax', description = '$description', category = '$category', specification = '$specification', 
			interior_ammenities = '$interior_ammenities', drawing_views = '$drawing_views', p1_name = '$p1_name', p1_price = '$p1_price',
			 p1_duration = '$p1_duration', p1_description = '$p1_description', p2_name = '$p2_name', p2_price = '$p2_price',
			 p2_duration = '$p2_duration', p2_description = '$p2_description', p3_name = '$p3_name', p3_price = '$p3_price',
			 p3_duration = '$p3_duration', p3_description = '$p3_description'  WHERE id = '".$id."'");
        if (request()->file('images')) {
            // Update product images
            $order = 0;
            $images = array();
            foreach (request()->file('images') as $file) {
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                    $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
                    $file->move($path,$image);
                    if($image) {
                        $img = Image::make($path. $image)->resize(800, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($path. $image);
                        $order++;
                    }
                } else {
                    $notices .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
            }
            DB::update("UPDATE layout_plans SET images = '".implode(',',$images)."' WHERE id = '".$id."'");
        }
        if (request()->file('file') || request()->file('file1')) {
            // Upload the downloadable file to product downloads directory
            $name = request()->file('file')->getClientOriginalName();
            $file = md5(time()).'.'.request()->file('file')->getClientOriginalExtension();
            $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
            request()->file('file')->move($path,$file);
            $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.$file);

            $name = request()->file('file1')->getClientOriginalName();
            $file1 = md5(time()).'.'.request()->file('file1')->getClientOriginalExtension();
            $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'layout_plans/';
            request()->file('file1')->move($path,$file1);
            $img = Image::make($path.$file1)->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path.$file1);

            $nfile = $file.','.$file1;
            DB::update("UPDATE layout_plans SET file = '".$nfile."' WHERE id = '".$id."'");
        }
    }

    public function professionalserviceadd()
    {
        $notices = '';
        if(isset($_POST['submit'])){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $price = $_POST['price'];
            $category = $_POST['category'];
            $payment_mode = $_POST['payment_mode'];
            $added_by = $_POST['added_by'];
            if (request()->file('images')) {
                // Update product images
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().'/assets/'.'services/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path. $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path. $image);
                            $order++;
                        }
                    } else {
                        $notices .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                    }
                }
            }

            DB::insert("INSERT INTO services (title,description,price,category,images,payment_mode,added_by,status) VALUE ('$title','$description','$price','$category','".implode(',',$images)."','$payment_mode','$added_by','Pending')");
        }
        return back();
    }

    public function invoice($order_id)
    {
        // Check if the order exists and return order details
        $data['check'] = Order::where('id',$order_id)->where('customer',customer('id'))->first();
        if (empty($data['check'])){
            abort(404);
        }
        $order = DB::select("SELECT * FROM orders WHERE id = '".$order_id."'")[0];
        $header = $this->retailHeader(translate('Invoice'),false,true);
        $fields = DB::select("SELECT name,code FROM fields ORDER BY id ASC");
        $footer = $this->footer();
        return view('invoice')->with(compact('header','order','fields','footer'));
    }


    public function getprofessionaleditdetail()
    {
        $id = $_POST['id'];
        $html = '';
        $plan = DB::select("SELECT * FROM layout_plans WHERE id = '".$id."' ")[0];
        $amminities = DB::select("SELECT * FROM amminities ORDER BY id DESC");
        $categories = Category::get();
        $html = '<form action="" method="post" enctype="multipart/form-data" class="form-horizontal single">
			'.csrf_field().'
				<h5>Update Layout Plan</h5>
					<table class="responsive-table bordered" style="width: 80%; margin: auto;">
						  <tr>
							<td>Plan code</td>
							<td><input name="plan_code" type="text" value="'.$plan->plan_code.'" class="form-control"  required/></td>
						  </tr>
						  <div class="form-group">
							<label class="control-label">Plan name</label>
							<input name="title" type="text" value="'.$plan->title.'" class="form-control"  required/>
						  </div>
						  <div class="form-group">
							<label class="control-label">Plan category</label>
							<select name="category" class="form-control">';
        foreach ($categories as $category){
            echo '<option value="'.$category->id.'" '.($category->id == $plan->category ? 'selected' : '').'>'.$category->name.'</option>';
            $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$category->id." ORDER BY id DESC");
            foreach ($childs as $child){
                echo '<option value="'.$child->id.'" '.($child->id == $plan->category ? 'selected' : '').'>- '.$child->name.'</option>';
                $subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id." ORDER BY id DESC");
                foreach ($subchilds as $subchild){
                    echo '<option value="'.$subchild->id.'" '.($subchild->id == $plan->category ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                }
            }
        }
        echo '</select>
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan description</label>
							<textarea name="description" type="text" id="editor1" class="form-control" required>'.$plan->description.'</textarea>
						  </div>
						  <div class="form-group">
							<label class="control-label">Plan specification</label>
							<textarea name="specification" type="text" id="editor" class="form-control" id="editor1" required>'.$plan->specification.'</textarea>
						  </div>
						  
						  
						  <div class="form-group">
							<label class="control-label">Plan price</label>
							<input name="price" type="text" value="'.$plan->price.'" class="form-control"  required/>
						  </div>
						  <div class="form-group">
							<label class="control-label">Tax (%)</label>
							<input name="tax" type="text" value="'.$plan->tax.'" class="form-control"  required/>
						  </div>
						  <div class="form-group">
							<div><label class="control-label">Drawing Views</label></div>';
        if (!empty($plan->file)){
            $fl = explode(',',$plan->file);
            echo $fl[0];
        }
        $dv = json_decode($plan->drawing_views);
        echo '<input name="drawing_views[]" type="text" class="form-control col-lg-6 pull-left" value="'.$dv[0].'"  style="width: 50%;" required/>
							<input type="file" class="form-control col-lg-6 pull-right" name="file1" accept="pdf/*"  style="width: 50%;" />
						  </div>
						  
						  <div class="form-group">
							<div><label class="control-label">Drawing Views</label></div>';
        if (!empty($plan->file)){
            $fl = explode(',',$plan->file);
            echo $fl[1];
        }
        $dv = json_decode($plan->drawing_views);
        echo '<input name="drawing_views[]" type="text" class="form-control col-lg-6 pull-left" value="'.$dv[1].'"  style="width: 50%;" required/>
							<input type="file" class="form-control col-lg-6 pull-right" name="file1" accept="pdf/*"  style="width: 50%;" />
						  </div>
						  <div class="row">';
        if (!empty($plan->images)){
            echo '<p>Uploading new images will overwrtite current images .</p>';
            $images = explode(',',$plan->images);
            foreach($images as $image){
                echo '<img class="col-md-2" src="'.url('/assets/layout_plans/'.$image).'" />';
            }
            echo '<div class="clearfix"></div>';
        }
        echo '</div><div class="form-group">
							<label class="control-label">Plan images</label>
							<input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"/>
						  </div>
						  <div class="row">
                              <div class="col-sm-12">
                                  <div class="form-group">';
        $amm = json_decode($plan->interior_ammenities);
        $i = 1;
        $selected = '';
        foreach($amm as $key=>$value) {

            //dd($a);
            echo '<div class="col-lg-12">
                                            <div class="col-lg-6" style="padding: 0px;">
                                                <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                                    <option value="">Please Select Amminity</option>';
            foreach ($amminities as $ammy) {
                $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                if($ammy->id == $a->id){
                    $selected = 'selected';
                }
                echo '<option value="' . $ammy->id . '" '.$selected.'>' . $ammy->name . '</option>';
            }
            echo '</select>
                                            </div>
                                            <div class="col-lg-6" style="padding: 0px;">
                                                <input type="text" class="form-control" name="interior_ammenities[]"  value="'.$value.'" style="border-radius: 0px; border: 1px solid;"/> 
                                            </div>
                                        </div>';
            $i++; }
        echo '</div>
                                </div>
                            </div>
                            
                            <hr>
						  
						  
						  <div class="form-group">
							<label class="control-label">Plan 1 Name</label>
							<input name="p1_name" type="text" value="'.$plan->p1_name.'" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 1 Price</label>
							<input name="p1_price" type="text" value="'.$plan->p1_price.'" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 1 Duration</label>
							<input name="p1_duration" type="text" value="'.$plan->p1_duration.'" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 1 description</label>
							<textarea name="p1_description" type="text" id="editorp1" class="form-control">'.$plan->p1_description.'</textarea>
						  </div>
						  
						  
						  <div class="form-group">
							<label class="control-label">Plan 2 Name</label>
							<input name="p2_name" type="text" value="'.$plan->p2_name.'" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 2 Price</label>
							<input name="p2_price" type="text" value="'.$plan->p2_price.'" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 2 Duration</label>
							<input name="p2_duration" type="text" value="'.$plan->p2_duration.'" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 2 description</label>
							<textarea name="p2_description" type="text" id="editorp2" class="form-control">'.$plan->p2_description.'</textarea>
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 3 Name</label>
							<input name="p3_name" type="text" value="'.$plan->p3_name.'" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 3 Price</label>
							<input name="p3_price" type="text" value="'.$plan->p3_price.'" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 3 Duration</label>
							<input name="p3_duration" type="text" value="'.$plan->p3_duration.'" class="form-control" />
						  </div>
						  
						  <div class="form-group">
							<label class="control-label">Plan 3 description</label>
							<textarea name="p3_description" type="text" id="editorp3" class="form-control">'.$plan->p3_description.'</textarea>
						  </div>
						  
						  <input name="edit" type="submit" value="Update plan" class="btn btn-primary" />
					</table>
				</form>';
    }


    public function geteditdetail()
    {
        $id = $_POST['id'];
        $html = '';
        $service = DB::select("SELECT * FROM services WHERE id = '".$id."'")[0];
        $parents = DB::select("SELECT * FROM services_category WHERE parent <> 0 ORDER BY id DESC");
        $html ='<form action="'.url('professional_service_edit').'" method="post" enctype="multipart/form-data" class="form-horizontal single">
            '.csrf_field().'
            <h5 style="padding-left: 30px;">Edit services</h5>
                <table class="responsive-table bordered" style="width: 80%; margin: auto;">
                    <input type="hidden" name="added_by" value="'.customer('id').'">
                        <tr>
                            <td>Title</td>
                            <td><input name="title" type="text"  value="'.$service->title.'" class="form-control" /></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>                    
                        <tr>
                            <td>Description</td>
                            <td><textarea name="description" class="form-control">'.$service->description.'</textarea></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>   
                        <tr>
                            <td>Services images</td>
                            <td>';
                            if (!empty($service->images)){
                                echo '<span>Uploading new images will overwrtite current images .</span>';
                                $images = explode(',',$service->images);
                                foreach($images as $image){
                                    $html.= '<img class="col-md-2" src="'.url('/assets/services/'.$image).'" />';
                                }
                                $html.= '<div class="clearfix"></div>';
                            }
                            $html.= '</td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>   
                        <tr>
                            <td>Price</td>
                            <td><input name="price" type="text" value="'.$service->price.'" class="form-control"  /></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>   
                        <tr>
                            <td>Category</td>
                            <td>
                                <select name="category" class="form-control select2">
                                    <option value="0"></option>';
                                    foreach ($parents as $parent){
                                        $html.= '<option value="'.$parent->id.'" '.($parent->id == $service->category ? 'selected' : '').'>'.$parent->name.'</option>';
                                    }
                                $html.= '</select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>   
                        <tr>
                            <td>Payment Mode</td>
                            <td>
                                <select name="payment_mode" class="form-control">';
                                    if($service->payment_mode == 'instant') {
                                        $html.= '<option value="">Please Select Payment Mode</option>
                                        <option value="instant" selected>Instant</option>
                                        <option value="later">Later</option>';
                                    }else if($service->payment_mode == 'later') {
                                        $html.= '<option value="">Please Select Payment Mode</option>
                                        <option value="instant" >Instant</option>
                                        <option value="later" selected>Later</option>';
                                    }
                                $html.= '
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>   
                        <tr>
                            <td>Tax In Percent</td>
                            <td><input name="price" type="text" value="'.$service->tax.'" class="form-control"  /></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>   
                        <tr>
                            <td>
                                <input name="submit" type="submit" value="Edit services" class="btn btn-primary" />
                            </td>
                        </tr>
                      
                    </table>
            </form>';
        return $html;
    }

    public function professionalserviceedit()
    {
        $notices = '';
        $title = $_POST['title'];
        $description = $_POST['description'];
        $price = $_POST['price'];
        $category = $_POST['category'];
        $display = $_POST['display'];
        $payment_mode = $_POST['payment_mode'];
        $id = $_POST['id'];


        DB::update("UPDATE services SET title = '$title',description = '$description',price = '$price',category = '$category',display = '$display',payment_mode = '$payment_mode' WHERE id = '".$id."'");

        if (request()->file('images')) {
            // Update product images
            $order = 0;
            $images = array();
            foreach (request()->file('images') as $file) {
                $name = $file->getClientOriginalName();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                    $images[] = $image = md5(time()).'.'.$order.'.'.$file->getClientOriginalExtension();
                    $path = base_path().'/assets/'.'services/';
                    $file->move($path,$image);
                    if($image) {
                        $img = Image::make($path. $image)->resize(800, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($path. $image);
                        $order++;
                    }
                } else {
                    $notices .= "<div class='alert mini alert-warning'> $name is not a valid format</div>";
                }
            }
            DB::update("UPDATE services SET images = '".implode(',',$images)."' WHERE id = '".$id."'");
        }
        return back();
    }


    public function professionalservicedelete()
    {
        DB::delete("DELETE FROM services WHERE id = '".$_GET['id']."' ");
        return back();
    }


    public function serviceInvoice($order_id)
    {
        // Check if the order exists and return order details
        $check = DB::select("SELECT COUNT(*) as count FROM services_answer WHERE id = '".$order_id."' AND user_id = '".customer('id')."' LIMIT 1")[0];
        if ($check->count == 0){
            abort(404);
        }
        $order = DB::select("SELECT * FROM services_answer WHERE id = '".$order_id."'")[0];
        $header = $this->retailHeader(translate('Invoice'),false,true);
        $fields = DB::select("SELECT name,code FROM fields ORDER BY id ASC");
        $footer = $this->footer();
        return view('serviceinvoice')->with(compact('header','order','fields','footer'));
    }

    public function planInvoice($order_id)
    {
        // Check if the order exists and return order details
        $check = DB::select("SELECT COUNT(*) as count FROM layout_order WHERE id = '".$order_id."' AND user_id = '".customer('id')."' LIMIT 1")[0];
        if ($check->count == 0){
            abort(404);
        }
        $order = DB::select("SELECT * FROM layout_order WHERE id = '".$order_id."'")[0];
        $header = $this->retailHeader(translate('Invoice'),false,true);
        $fields = DB::select("SELECT name,code FROM fields ORDER BY id ASC");
        $footer = $this->footer();
        return view('planinvoice')->with(compact('header','order','fields','footer'));
    }

    public function profile()
    {
        if(isset($_POST['update'])){
            if (!empty($_POST['name']) && !empty($_POST['email'])){
                // Escape the user entries and update the database
                $name = escape(htmlspecialchars($_POST['name']));
                $email = escape(htmlspecialchars($_POST['email']));
                if (!empty($_POST['password'])) {
                    $password = md5($_POST['password']);
                } else {
                    $password = customer('password');
                }
                DB::update("UPDATE customers SET name = '$name',email = '$email',password = '$password' WHERE id = '".customer('id')."'");
                $error = false;
            } else {
                $error = 'Name and email fields are required !';
            }
        }
        $header = $this->retailHeader(translate('Profile'),false,true);
        $footer = $this->footer();
        return view('profile')->with(compact('header','error','footer'));
    }
    public function logout(Request $request)
    {
        // Clear the customer session
        $request->session()->forget('customer');
        session(['customer' => '']);
        return redirect('/');
    }

    public function products($cat = '', $sub = false)
    {
        if($cat == ''){
            abort(404);
        }
        $hub = ManufacturingHubs::find($_COOKIE['aakar_hub']);

        $data['show_filter'] = true;
        if(customer('user_type') == ''){
            $data['show_filter'] = false;
        }
        $show_products = false;
        if($sub){
            $cat = $sub;
            $data['show_filter'] = true;
        }

        $categ = DB::table('category')->where('path', $cat)->first();
        if($categ === null){
            abort(404);
        }
        $pdata = DB::table('category')->where('parent', $categ->id)->count();
        if($pdata == 0){
            $show_products = true;
        }
        $header = $this->header(translate('Products'),false,true);
        // Apply the product filters
        $price = array();
        $where = array();
        $bids = array();
        if (isset($_GET['min']) && isset($_GET['max']))
        {
            if(customer('user_type') != 'institutional') {
                $where['price'] = "price BETWEEN '" . escape($_GET['min']) . "' AND '" . escape($_GET['max']) . "'";
            }else{

            }
        }


        if (isset($_GET['brands']))
        {
            $bids = array_keys($_GET['brands']);
        }

        if (!empty($catlink))
        {
            $where['search'] = "(category = ".$catlink.")";
        }
        if ($cat)
        {
            $check = DB::select("SELECT COUNT(*) as count FROM category WHERE id = ".$categ->id);
            if (count($check) == 0){
                abort(404);
            }
            $category = DB::table('category')->where('id', $categ->id)->first();
            $categories[] = $category->id;
            if ($category->parent == 0){
                $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
                foreach ($childs as $child){
                    $categories[] = $child->id;
                }
            }
            $where['cat'] = "category IN (".implode(',',$categories).")";
        }
        $tpbrands = '';
        $suggested = false;
        $top_brands = [];
        if($categ){
            $category = DB::table('category')->where('id', $categ->id)->first();
            $tpbrands =  '('.$category->brands.')';
            if($category->brands !== '') {
                $top_brands = DB::select("SELECT * FROM brand WHERE id IN ".$tpbrands." ORDER BY name ASC");
            }
            $sg = DB::select("SELECT product_id from featured_products where category_id = ".$categ->id);
            $sug_prods = '';
            $suggested = [];
            //dd($sg);
            if(count($sg) > 0){
                $sug_prods = $sg[0]->product_id;
                $suggested = DB::select("SELECT * FROM products WHERE id in (".$sug_prods.")");
            }
            $product_categories = DB::select("SELECT * FROM category WHERE parent = ".$categ->id." ORDER BY name ASC");
            $procount = DB::select("SELECT COUNT(*) as count FROM category WHERE parent = ".$categ->id)[0]->count;
        }
        $categoriescon = DB::select('SELECT COUNT(*) as count FROM category WHERE parent = 0 ORDER BY name ASC');
        if($categoriescon !== '') {
            $cats = DB::select('SELECT * FROM category WHERE parent = 0 ORDER BY name ASC');
        }else{
            $cats = 'Data Not Available';
        }
        if(customer('user_type') == 'institutional'){
            $faqs = DB::select("SELECT * FROM product_faq WHERE  FIND_IN_SET (".$categ->id.", category) AND section='institutional' ORDER BY id DESC LIMIT 5");
        }else{
            $faqs = DB::select("SELECT * FROM product_faq WHERE  FIND_IN_SET (".$categ->id.", category) AND section='retail' ORDER BY id DESC LIMIT 5");
        }
        //$faqs = DB::select("SELECT * FROM product_faq WHERE  FIND_IN_SET (".$categ->id.", category) ORDER BY id DESC LIMIT 5");
        $section = ' AND section = "retail"';
        if(customer('user_type') == 'institutional'){
            $section = ' AND section = "institutional"';
        }
        $ladvices = DB::select("SELECT * FROM blog WHERE FIND_IN_SET (".$categ->id.", pro_cat) ".$section." ORDER BY id DESC LIMIT 4");
        $tadvices = DB::select("SELECT * FROM blog WHERE FIND_IN_SET (".$categ->id.", pro_cat) ".$section." ORDER BY visits DESC LIMIT 4");
        $best = DB::select("SELECT * FROM best_for_category WHERE category = ".$categ->id." ORDER BY id DESC ");
		$brands = array();
		if($categ->brands){
			$brands = DB::select("SELECT * FROM brand where id In (".$categ->brands.") ORDER BY id DESC ");
		}
        $filters = [];
        $fc = DB::table('category')->where('id', $categ->id)->first();
        $cfc = $fc->filter;
        //dd($cfc);
        if($cfc !== '') {
            $ccx = explode( ',', $cfc);
            foreach ($ccx as $cx) {
                $filters = DB::select("SELECT * FROM filter WHERE id = '$cx'");
            }
            //dd($filters);
        } else {
            $filters = '';
        }
        $cid[] = $categ->id;
        //Kd codes
        $c = array();
        $cont = true;
        $temp = $cid;
        while($cont){
            $c = array_pluck(DB::table('category')->whereIn('parent', $temp)->get(), 'id');
            if(count($c)){
                $cid = array_merge($cid, $c);
                $temp = $c;
            }else{
                $cont = false;
            }
        }
        $products = DB::table('products')->whereIn('category', $cid);

        $link = DB::table('link')->where('page', 'Products')->where('link', '<>', '')->first();
        $price['set_min'] = 0;
        $price['set_max'] = 0;

        	$p_min = DB::select("SELECT MIN(price) as min_price, MAX(price) as max_price FROM products where category IN (".implode(',', $cid).")");
			if(count($p_min) > 0){
				$price['min'] = $p_min[0]->min_price;
				$price['set_min'] = $p_min[0]->min_price;
                $price['max'] = $p_min[0]->max_price;
                $price['set_max'] = $p_min[0]->max_price;
			}else{
				$price['min'] = 0;
                $price['set_min'] = 0;
                $price['max'] = 0;
			}

            if(isset($_GET['min'])){
                $price['set_min'] = $_GET['min'];
                if(customer('user_type') != 'institutional')
                    $products = $products->where('price', '>=', $_GET['min']);
            }
            if(isset($_GET['max'])){
                $price['set_max'] = $_GET['max'];
                if(customer('user_type') != 'institutional')
                    $products = $products->where('price', '<=', $_GET['max']);
            }
            if(count($bids) > 0){
                $products = $products->whereIn('brand_id', $bids);
            }
        if ($hub !== null) {
            $ph_products = array();
            $ph_relations = DB::table('ph_relations')->get();
            foreach($ph_relations as $ph){
                $hub_ids = json_decode($ph->hubs);
                foreach($hub_ids as $hub_id){
                    if($hub_id->hub == $hub->id){
                        if(isset($_GET['min']) && isset($_GET['max'])) {
                            if(intval($_GET['max']) >= intval($hub_id->price) && intval($hub_id->price) >= intval($_GET['min'])) {
                                $ph_products[] = $ph->products;
                            }
                        }else if(isset($_GET['min'])) {
                            if(intval($hub_id->price) >= intval($_GET['min'])) {
                                $ph_products[] = $ph->products;
                            }
                        }else if(isset($_GET['max'])) {
                            if(intval($hub_id->price) <= intval($_GET['max'])) {
                                $ph_products[] = $ph->products;
                            }
                        }else{
                            $ph_products[] = $ph->products;
                        }

                    }
                }
            }
            $products = $products->whereIn('id', $ph_products);
        }
            $products = $products->get();
		$footer = $this->footer();
		$hubs = ManufacturingHubs::orderBy('title')->get();
        $data['vtype']=Session::get('aakar360.visitor_type');
        $inst = DB::table('customers')->get();
        return view('products')->with(compact('header','category','price','categ','ladvices','brands','tadvices','filters','procount','faqs','best','cats','link','products','footer', 'product_categories', 'top_brands', 'suggested', 'bids', 'data', 'show_products', 'hub', 'hubs','inst'));
    }


    public function brand($cat = '')
    {
        if($cat == ''){
            abort(404);
        }
        $header = $this->header(translate('Products'),false,true);
        // Apply the product filters
        $tpbrands = '';
        $suggested = false;
        $top_brands = [];
        $br_id = DB::table('brand')->where('path', $cat)->first();
        //dd($br_id);
        $categ = DB::table('products')->where('brand_id', $br_id->id)->first();
		$price = array();
        $where = array();
        if (!empty($_GET['min']) && !empty($_GET['max']))
        {
            $where['price'] = "price BETWEEN '".escape($_GET['min'])."' AND '".escape($_GET['max'])."'";
        }
        if (!empty($_GET['search']))
        {
            $where['search'] = "(title LIKE '%".escape($_GET['search'])."%' OR text LIKE '%".escape($_GET['search'])."%')";
        }
        if (!empty($category))
        {
            $where['search'] = "(category = ".$categ->id.")";
        }
        if (!empty($catlink))
        {
            $where['search'] = "(category = ".$catlink.")";
        }
        if ($cat)
        {
            $check = DB::select("SELECT COUNT(*) as count FROM category WHERE id = ".$categ->id);
            if (count($check) == 0){
                abort(404);
            }
            $category = DB::table('category')->where('id', $categ->category)->first();
            $categories[] = $category->id;
            if ($category->parent == 0){
                $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
                foreach ($childs as $child){
                    $categories[] = $child->id;
                }
            }
            $where['cat'] = "category IN (".implode(',',$categories).")";
        }
        if($categ){
            $category = DB::table('category')->where('id', $categ->category)->first();
            $tpbrands =  '('.$category->brands.')';
            if($category->brands !== '') {
                $top_brands = DB::select("SELECT * FROM brand WHERE id in " . $tpbrands . " ORDER BY name ASC");
            }
            $sg = DB::select("SELECT product_id from featured_products where category_id = ".$categ->id);
            $sug_prods = '';
            $suggested = [];
            //dd($sg);
            if(count($sg) > 0){
                $sug_prods = $sg[0]->product_id;
                $suggested = DB::select("SELECT * FROM products WHERE id in (".$sug_prods.")");
            }
            $product_categories = DB::select("SELECT * FROM category WHERE parent = ".$categ->id." ORDER BY name ASC");
        }
        $brands = DB::select("SELECT * FROM brand ORDER BY id DESC ");
        $filters = [];
        $fc = DB::table('category')->where('id', $categ->category)->first();

        $cfc = $fc->filter;
        //dd($cfc);
        if($cfc !== '') {
            $ccx = explode( ',', $cfc);
            foreach ($ccx as $cx) {
                $filters = DB::select("SELECT * FROM filter WHERE id = '$cx'");
            }
            //dd($filters);
        } else {
            $filters = '';
        }
		$where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
		$products = array();
        $products = DB::select("SELECT * FROM products WHERE brand_id = '$cat' ORDER BY id DESC ");
        $bran = DB::table('brand')->where('path', $cat)->first();
        $cats = DB::select("SELECT * FROM category WHERE parent = '0' ORDER BY id DESC ");
            $link = DB::table('link')->where('page', 'Brand')->first();
        if(customer('user_type') == 'institutional'){
            $faqs = DB::select("SELECT * FROM product_faq WHERE  FIND_IN_SET (".$categ->id.", category) AND section='institutional' ORDER BY id DESC LIMIT 5");
        }else{
            $faqs = DB::select("SELECT * FROM product_faq WHERE  FIND_IN_SET (".$categ->id.", category) AND section='retail' ORDER BY id DESC LIMIT 5");
        }
            //$faqs = DB::select("SELECT * FROM product_faq WHERE user_id = '0' AND FIND_IN_SET (".$categ->id.", category) ORDER BY id DESC LIMIT 5");
        $ladvices = DB::select("SELECT * FROM blog WHERE pro_cat in (".$categ->category.") ORDER BY id DESC LIMIT 2");
        $tadvices = DB::select("SELECT * FROM blog WHERE pro_cat in (".$categ->category.") ORDER BY visits DESC LIMIT 2");
        $best = DB::select("SELECT * FROM best_for_category WHERE category = ".$categ->category." ORDER BY id DESC ");
        $footer = $this->footer();
		if(count($products) > 0){
			$p_min = DB::select("SELECT price FROM products $where ORDER BY price ASC LIMIT 1");
			if(count($p_min) > 0){
				$price['min'] = $p_min[0]->price;
			}else{
				$price['min'] = 0;
			}
			$p_max = DB::select("SELECT price FROM products $where ORDER BY price DESC LIMIT 1");
			if(count($p_max) > 0){
				$price['max'] = $p_max[0]->price;
			}else{
				$price['max'] = 0;
			}
		}else{
			$price['min'] = 0;
			$price['max'] = 0;
		}
        return view('brand')->with(compact('header','category','price','link','bran','cats','categ','br_id','faqs','ladvices','tadvices','best','brands','filters','products','footer', 'product_categories', 'top_brands', 'suggested'));
    }


    public function product(Request $request, $product_id)
    {
        $hub = Cookie::get('aakar_hub');
        $recent_viewed = 0;
        $variants = false;
        $cookie_name = "sellerkit.recent";
        if($request->session()->get($cookie_name) == null){
            $request->session()->put($cookie_name, explode('-',$product_id)[0]);
        }else{
            $recent_viewed = DB::select('SELECT * FROM products WHERE id in ('.$request->session()->get($cookie_name).')');;
            $request->session()->put($cookie_name, $request->session()->get($cookie_name).','.explode('-',$product_id)[0]);
        }
        $check = DB::select("SELECT COUNT(*) as count FROM products WHERE id = '".explode('-',$product_id)[0]."'")[0];
        if ($check->count == 0){
            abort(404);
        }
        $exp_array = explode('-',$product_id);
        $pid = explode('-',$product_id)[0];
        if(count($exp_array)==2){
            $product = DB::select("SELECT * FROM `products` as p1 WHERE p1.sku in (SELECT p2.sku FROM products as p2 WHERE p2.id=$pid) and p1.sale in (Select min(p3.sale) from products as p3 where p3.sku=p1.sku)")[0];
        }
        else{
            $product = DB::select("SELECT * FROM `products` as p1 WHERE p1.id=$pid")[0];
        }
        $cat = DB::select("SELECT * FROM category WHERE id = ".$product->category)[0];
        if(empty($hub) && customer('user_type') == 'institutional'){
            return redirect('products/'.$cat->path);
        }
        $variants = DB::select("SELECT * FROM product_variants where display = '1' AND product_id=$product->id");
        $dis_var = DB::select("SELECT * FROM product_variants where product_id=$product->id AND display=1");
        $bulk_discounts = DB::select("SELECT * FROM product_discount where product_id=$product->id ORDER BY quantity ASC");
        $total_ratings = DB::select("SELECT COUNT(*) as count FROM reviews WHERE active = 1 AND product = ".$product->id)[0]->count;
        $rating = 0;
        if ($total_ratings > 0){
            $rating_summ = DB::select("SELECT SUM(rating) as sum FROM reviews WHERE active = 1 AND product = ".$product->id)[0]->sum;
            $rating = $rating_summ / $total_ratings;
        }
        $total_reviews = DB::select("SELECT COUNT(*) as count FROM reviews WHERE active = 1 AND review <> '' AND product = ".$product->id)[0]->count;
        $ratinga = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM reviews WHERE active = '1' AND product = '".$product->id."'")[0];
        $total_rating =  $ratinga->total_rating;
        $total_user = $ratinga->total_user;
        if($total_rating==0){
            $avg_rating = 0;
        }
        else{
            $avg_rating = round($total_rating/$total_user,1);
        }
        $rating1= DB::select("SELECT count(id) as rating1_user FROM reviews WHERE rating = '1' AND product = '".$product->id."' AND active = '1'")[0];
        $rating2= DB::select("SELECT count(id) as rating2_user FROM reviews WHERE rating = '2' AND product = '".$product->id."' AND active = '1'")[0];
        $rating3= DB::select("SELECT count(id) as rating3_user FROM reviews WHERE rating = '3' AND product = '".$product->id."' AND active = '1'")[0];
        $rating4= DB::select("SELECT count(id) as rating4_user FROM reviews WHERE rating = '4' AND product = '".$product->id."' AND active = '1'")[0];
        $rating5= DB::select("SELECT count(id) as rating5_user FROM reviews WHERE rating = '5' AND product = '".$product->id."' AND active = '1'")[0];
        $images = explode(',',$product->images);
        $title = $product->title;
        $desc = $product->text;
        $specification = $product->specification;
        $header = $this->retailHeader(translate($title),$desc,true);
        $reviews = DB::select("SELECT * FROM reviews WHERE product = ".$product->id." AND active = 1 ORDER BY time DESC");
        $tp = url("/themes/".$this->cfg->theme);
        $footer = $this->footer();
        $related_products = getRelatedProducts($product->category, $product->id);
        $get_sku = DB::select("SELECT sku from `products` WHERE id = '$pid'")[0];
        $sellers = DB::select("SELECT c.*,p.* FROM `products` as p INNER JOIN customers as c on p.added_by = c.id WHERE p.sku = '$get_sku->sku' limit 2");
        if(customer('user_type') == 'institutional'){
            $faqs = DB::select("SELECT * FROM product_faq WHERE section='institutional' AND status = 1 ORDER BY id DESC");
        }else{
            $faqs = DB::select("SELECT * FROM product_faq WHERE  section='retail' AND status = 1 ORDER BY id DESC");
        }
        $product = DB::table("products")->where('id', '=', $product->id)->first();
        $cat = DB::table('category')->where('id', '=', $product->category)->first();
        $wunits = DB::table('units')->whereIn('id', explode(',', $cat->weight_unit))->get();
        $link = DB::table('link')->where('page', 'Single Product')->first();
        $brands = DB::table('brand')->get();
        $vtype=Session::get('aakar360.visitor_type');
        return view('product')->with(compact('header','product','cat','brands','faqs','related_products','rating','total_ratings','link',
            'variants','dis_var','bulk_discounts','images','reviews','specification','tp','footer', 'recent_viewed', 'total_reviews', 'rating1', 'rating2', 'rating3','rating4', 'rating5', 'avg_rating', 'ratinga', 'total_user','sellers', 'wunits', 'hub', 'vtype'));
    }
    public function getProductPrice(Request $request){
        if(customer('user_type') === ''){
            return json_encode(array("reason"=>"login"));
        }
        $product = DB::select("SELECT * FROM products WHERE id = '".$request->id."'")[0];
        if(customer('user_type') == 'institutional'){
            $bulk_discounts = DB::select("SELECT * FROM product_discount where user_type = 'institutional' AND quantity<='".$request->quantity."' AND product_id = '".$request->id."' ORDER BY quantity DESC LIMIT 1");
            $variants = DB::select("SELECT * FROM product_variants where product_id='".$request->id."'");
            $result = array();
            if(count($bulk_discounts) > 0){
                $discount = $bulk_discounts[0];
                if(count($variants)) {
                    foreach ($variants as $variant) {
                        $price = getInstitutionalPriceFancy($product->id, $request->hub, $variant->id);

                        if ($discount->discount_type == 'Flat') {
                            $price = $price - $discount->discount;
                        } else {
                            $dis = number_format(($price * $discount->discount) / 100, 2, '.', '');
                            $price = $price - $dis;
                        }
                        $result[] = array(
                            'id' => $variant->id,
                            'price' => $price
                        );
                    }
                }else{
                    $iprice = getInstitutionalPriceFancy($product->id, $request->hub);

                    if ($discount->discount_type == 'Flat') {
                        $price = $iprice - $discount->discount;
                    } else {
                        $dis = number_format(($iprice * $discount->discount) / 100, 2, '.', '');
                        $price = $iprice - $dis;
                    }
                    $result[] = array(
                        'id' => $product->id,
                        'price' => $price,
                        'actual_price' => $iprice
                    );
                }

            }else{
                if(count($variants)) {
                    foreach ($variants as $variant) {
                        $price = getInstitutionalPriceFancy($product->id, $request->hub, $variant->id);

                        $result[] = array(
                            'id' => $variant->id,
                            'price' => $price
                        );
                    }
                }else{
                    $price = getInstitutionalPriceFancy($product->id, $request->hub);
                    $result[] = array(
                        'id' => $product->id,
                        'price' => $price,
                        'actual_price' => $price
                    );
                }
            }
            return json_encode($result);
        }
        $bulk_discounts = DB::select("SELECT * FROM product_discount where user_type = 'individual' AND quantity<='".$request->quantity."' AND product_id = '".$request->id."' ORDER BY quantity DESC LIMIT 1");
        $variants = DB::select("SELECT * FROM product_variants where product_id='".$request->id."'");
        $result = array();
        if(count($bulk_discounts) > 0){
            $discount = $bulk_discounts[0];
            if(count($variants)) {
                foreach ($variants as $variant) {
                    $price = $product->price;
                    if ($product->price > $product->sale) {
                        $price = $product->sale;
                    }
                    if ($discount->discount_type == 'Flat') {
                        $price = ($variant->price + $price) - $discount->discount;
                    } else {
                        $dis = number_format((($variant->price + $price) * $discount->discount) / 100, 2, '.', '');
                        $price = ($variant->price + $price) - $dis;
                    }
                    $result[] = array(
                        'id' => $variant->id,
                        'price' => $price
                    );
                }
            }else{
                $price = $product->price;
                if ($product->price > $product->sale) {
                    $price = $product->sale;
                }
                if ($discount->discount_type == 'Flat') {
                    $price = ($price) - $discount->discount;
                } else {
                    $dis = number_format((($price) * $discount->discount) / 100, 2, '.', '');
                    $price = ($price) - $dis;
                }
                $result[] = array(
                    'id' => $product->id,
                    'price' => $price,
                    'actual_price' => $product->price
                );
            }

        }else{
            if(count($variants)) {
                foreach ($variants as $variant) {
                    $price = $product->price;
                    if ($product->price > $product->sale) {
                        $price = $product->sale;
                    }
                    $price = $price + $variant->price;
                    $result[] = array(
                        'id' => $variant->id,
                        'price' => $price
                    );
                }
            }else{
                $price = $product->price;
                if ($product->price > $product->sale) {
                    $price = $product->sale;
                }
                $result[] = array(
                    'id' => $product->id,
                    'price' => $price,
                    'actual_price' => $product->price
                );
            }
        }
        return json_encode($result);
    }


    public function services($services_category = false)
    {
        $header = $this->retailHeader(translate('Services'),false,true);
        // Apply the product filters

        $categoriescon = DB::select('SELECT COUNT(*) as count FROM category WHERE parent = 0 ORDER BY name ASC');
        if($categoriescon !== '') {
            $cats = DB::select('SELECT * FROM category WHERE parent = 0 ORDER BY name ASC');
        }else{
            $cats = 'Data Not Available';
        }
        if (!empty($_GET['search']))
        {
            $where['search'] = "(name LIKE '%".escape($_GET['search'])."%')";
        }
        if (empty($_GET['search']))
        {
            $where['search'] = "(parent = '0')";
        }

        $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
        $services = DB::select("SELECT * FROM services_category $where ORDER BY id DESC ");
        $serv = DB::select("SELECT * FROM services_category WHERE parent = '".explode('-',$services_category)[0]."'");

        $footer = $this->footer();
        return view('services')->with(compact('header','services_category','serv','cats','services','footer'));
    }


    public function services_category($services_category = '')
    {
        if ($services_category == ''){
            abort(404);
        }
        $header = $this->retailHeader(translate(explode('-',$services_category)[1]),false,true);
        // Apply the product filters

        $categoriescon = DB::select('SELECT COUNT(*) as count FROM category WHERE parent = 0 ORDER BY name ASC');
        if($categoriescon !== '') {
            $cats = DB::select('SELECT * FROM category WHERE parent = 0 ORDER BY name ASC');
        }else{
            $cats = 'Data Not Available';
        }
        $check = DB::select("SELECT COUNT(*) as count FROM services_category WHERE parent = '".explode('-',$services_category)[0]."'")[0];
        if ($check->count == 0){
            abort(404);
        }
        if (!empty($_GET['search']))
        {
            $where['search'] = "(name LIKE '%".escape($_GET['search'])."%' AND parent = '".explode('-',$services_category)[0]."')";
        }
        if (empty($_GET['search']))
        {
            $where['search'] = "(parent = '".explode('-',$services_category)[0]."')";
        }

        $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
        $services_categories = DB::select("SELECT * FROM services_category $where");
        $serv = DB::select("SELECT * FROM services_category WHERE parent = '".explode('-',$services_category)[0]."'");
        $how = DB::select("SELECT * FROM s_how_it_works WHERE FIND_IN_SET('".explode('-',$services_category)[0]."', cat)");
        $icat = DB::select("SELECT * FROM services_category_image WHERE service_category_id = '".explode('-',$services_category)[0]."'");
        $link = DB::table('link')->where('page', 'Services Category')->first();
        $footer = $this->footer();
        return view('services_category')->with(compact('header','cats','serv','link','services_categories','services_category', 'how','footer', 'icat'));
    }

    public function getCatImage(Request $request){
        $data = array('image1' => '', 'image2' => '', 'image3' => '', 'image4' => '', 'image5' => '');
        $ret_data = array();
        $service_cat_id = $request->service_category_id;
        $gallery_cat_id = $request->gallery_category_id;
        $image_data = DB::select("SELECT * FROM services_category_image where service_category_id='".$service_cat_id."' and gallery_category_id='".$gallery_cat_id."'");
        if(count($image_data)){
            $img_data = $image_data[0];

            $data['image1'] = $img_data->image_1;
            $data['image2'] = $img_data->image_2;
            $data['image3'] = $img_data->image_3;
            $data['image4'] = $img_data->image_4;
            $data['image5'] = $img_data->image_5;

            foreach ($data as $key=>$value){
                $img = DB::select("select * from photo_gallery where id = ".$value)[0];
                $ret_data[] = array(
                    'slug' => $img->slug,
                    'img' => $img->image,
                );
            }
        }
        return json_encode($ret_data);
    }

    public function getServiceQuestions(Request $request){
        $questions = DB::select("SELECT * FROM services_questions WHERE service_id = '".$request->service_id."' Order By priority ASC");
        $html = '';
        if($questions){
            $html='<div class="stepwizard">
					<div class="stepwizard-row setup-panel">
						<div class="stepwizard-step col-xs-3"> 
							<a href="#step-x0" type="button" class="btn btn-success btn-circle">0</a>
						</div>';
            $sr = 1;
            foreach($questions as $question){
                $html.='<div class="stepwizard-step col-xs-3"> 
				<a href="#step-x'.$sr.'" type="button" class="btn btn-default btn-circle">'.$sr.'</a>
			</div>';
                $sr++; }
            $html.='</div>
				</div>
				
				<form role="form" method="post" action="'.url("update-answer").'">
				    '.csrf_field().'
				    <input type="hidden" name="service_id" value="'.$request->service_id.'">
				    <div class="panel panel-primary setup-content" id="step-x0">
						
						<div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
							<div class="form-group">
								<h2 class="text-center" style="text-transform: none;">What is the location of your Project?</h2>
								<hr style="position: relative; max-width: 100px; margin:40px auto;">
								<center><i class="fa fa-map-marker" style="font-size:48px;"></i><br><label class="location">Pincode</label><br>
								    <input maxlength="6" minlength="6" type="text"  required="required" name="answer[pincode]" class="form-control pincode" style="max-width: 200px;margin:20px 0;" placeholder="Enter Location" /></center>
									
							</div>
							<div style="padding: 20px;text-align:center">
								<hr>
								<button class="btn btn-primary prevBtn" disabled type="button">Previous</button>
								<button class="btn btn-primary nextBtn" type="button">Next</button>
							</div>
						</div>
						
					</div>';
            $sr = 1;
            foreach($questions as $question){
                $html.='<div class="panel panel-primary setup-content" id="step-x'.$sr.'">
						
						<div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
							<div class="form-group">
								<h2 class="text-center" style="text-transform: none;">'.getQuestion($question->question_id).'</h2>
								<hr style="position: relative; max-width: 100px; margin:40px auto;">';

                $type = getQuestionType($question->question_id);
                if($type == 'text'){

                    $html.='<center><i class="fa fa-map-marker" style="font-size:48px;"></i><br><label class="location">Pincode</label><br>
                                <input maxlength="6" minlength="6" type="text" name="answer['.$question->question_id.']" required="required" class="form-control pincode" style="max-width: 200px;margin:20px 0;" placeholder="Enter Location" /></center>';
                }else if($type == 'single'){
                    $html.='<div style="position:relative; max-width: 350px;margin:auto;">';
                    $options = json_decode(getQuestionOptions($question->question_id));
                    $i = 0;
                    foreach($options as $option){
                        $html.='<input type="radio" name="answer['.$question->question_id.']" value="'.$option.'" style="margin-right:10px;" id="'.$sr.$i.'"></input> <label class="label-hover" for="'.$sr.$i.'" style="cursor: pointer;"">'.$option.'</label><br>';
                        $i++;
                    }
                    $html.= '</div>';
                }
                else if($type == 'multiple'){
                    $html.= '<div style="position:relative; max-width: 350px;margin:auto;">';
                    $options = json_decode(getQuestionOptions($question->question_id));
                    $i = 0;
                    foreach($options as $option){
                        $html.='<input type="checkbox" name="answer['.$question->question_id.']" value="'.$option.'" style="margin-right:10px;" id="'.$sr.$i.'"></input> <label class="label-hover" for="'.$sr.$i.'" style="cursor: pointer;"">'.$option.'</label><br>';
                        $i++;
                    }
                    $html.= '</div>';
                }
                else if($type == 'textarea'){
                    $html.= '<div style="position:relative;width: 60%;margin:auto;">
										<textarea name="answer['.$question->question_id.']" style="margin-right:10px;" rows="7" class="form-control" placeholder="Enter a description..."></textarea>
									</div>';
                }
                else if($type == 'dropdown'){
                    $html.= '<div style="position:relative; max-width: 350px;margin:auto;">
													<select class="form-control select2" name="answer['.$question->question_id.']">';
                    $options = json_decode(getQuestionOptions($question->question_id));
                    $i = 0;
                    foreach($options as $option){
                        $html.='<option value="'.$option.'">'.$option.'</option>';
                        $i++;
                    }
                    $html.= '</select></div>';
                }
                $html.='</div>
							<div style="padding: 20px;text-align:center">
								<hr>
								<button class="btn btn-primary prevBtn"';
                if($sr == 0){
                    $html.="disabled";
                }
                $html.='
							type="button">Previous</button>';
                if($sr != count($questions)){
                    $html.='<button class="btn btn-primary nextBtn" type="button">Next</button>';
                }else{
                    $html.='<button class="btn btn-primary" type="submit">Finish</button>';
                }
                $html.='</div>
							</div>
						</div>';
                $sr++;
            }

            $html.='</form>';
        }else{
            $html = 'Ooops...! Something Went Wrong. Please try again later';
        }
        return $html;

    }
    public function updateAnswer(Request $request)
    {
        $header = $this->retailHeader(translate('Service Checkout'),false,true);
        $footer = $this->footer();
        $answerarray = array();
        $answerarray = $request->answer;
        //dd($answerarray);
        $service_id = $request->service_id;
        $user_id = Customer('id');
        $answer = json_encode($answerarray);



        $pay = DB::select("SELECT * FROM services WHERE id = '$service_id'")[0];
        $payment_mode = $pay->payment_mode;
        //dd($pay);
        if($pay->payment_mode == 'instant') {
            //dd($answer);
            $answer = request()->session()->put('answer', $answer);
            $service_id = request()->session()->put('service_id', $service_id);
            $user_id = request()->session()->put('user_id', $user_id);
            return view('service_checkout')->with(compact('header','answer','service_id','user_id','footer'));
        }else {
            DB::insert("INSERT INTO services_answer (service_id,user_id,answer,payment_mode,payment_method,status) VALUES ('$service_id','$user_id','$answer','$payment_mode','None', 'Pending')");
            return back();
        }

    }

    public function addplaninsession(Request $request){
        $layout_id = request()->session()->put('layout_id', $_POST['layout_id']);
        $adons = request()->session()->put('adons', $_POST['adons']);
        $package = request()->session()->put('package', $_POST['package']);
        $package_price = request()->session()->put('package_price', $_POST['package_price']);
        return redirect('plan_checkout');
    }

    public function updatePlanAnswer(Request $request)
    {
        $header = $this->retailHeader(translate('Modify Plan'),false,true);
        $footer = $this->footer();
        $answerarray = array();
        $answerarray = $request->answer;
        //dd($answerarray);
        $plan_id = $request->plan_id;
        $user_id = Customer('id');
        $answer = json_encode($answerarray);

        DB::insert("INSERT INTO modify_plan (plan_id,user_id,answer,status) VALUES ('$plan_id','$user_id','$answer','Pending')");
        return back();
    }

    public function Faq(Request $request)
    {

        $service_id = $_POST['ser_id'];
        $ser = explode('-', $service_id);
        $question = $_POST['question'];
        $user_id = Customer('id');
        $status = 'Pending';

        DB::insert("INSERT INTO faq (service_id,user_id,question,status) VALUES ('$ser[0]','$user_id','$question','$status')");
        return back();
    }

    public function layoutFaq(Request $request)
    {

        $layout_id = $_POST['layout_id'];
        $question = $_POST['question'];
        $user_id = Customer('id');
        $status = 'Pending';

        DB::insert("INSERT INTO layout_faq (layout_id,user_id,question,status) VALUES ('$layout_id','$user_id','$question','$status')");
        return back();
    }

    public function productFaq(Request $request)
    {

        $product_id = $_POST['product_id'];
        $question = $_POST['question'];
//        $user_id = Customer('id');
        $status = 'Pending';

        DB::insert("INSERT INTO product_faq (category,question,status) VALUES ('$product_id','$question','$status')");
        return back();
    }

    public function sub_service($category_id = '')
    {
        if ($category_id == ''){
            abort(404);
        }
        $header = $this->retailHeader(translate(explode('-',$category_id)[1]),false,true);
        // Apply the product filters

        $categoriescon = DB::select('SELECT COUNT(*) as count FROM category WHERE parent = 0 ORDER BY name ASC');
        if($categoriescon !== '') {
            $cats = DB::select('SELECT * FROM category WHERE parent = 0 ORDER BY name ASC');
        }else{
            $cats = 'Data Not Available';
        }
        $check = DB::select("SELECT COUNT(*) as count FROM services WHERE category = '".explode('-',$category_id)[0]."'")[0];
        if ($check->count == 0){
            abort(404);
        }
        if (!empty($_GET['search']))
        {
            $where['search'] = "(name LIKE '%".escape($_GET['search'])."%' AND category = '".explode('-',$category_id)[0]."')";
        }
        if (empty($_GET['search']))
        {
            $where['search'] = "(category = '".explode('-',$category_id)[0]."')";
        }
        $questions = DB::select("SELECT * FROM service_category_questions WHERE service_id = '".explode('-',$category_id)[0]."' Order By priority ASC");
        $where = ($where) ? 'WHERE ' . implode(' AND ', $where) : '';
        $sub_service = DB::select("SELECT * FROM services $where");
        $serv = DB::select("SELECT * FROM services_category");

        $faqs = DB::select("SELECT * FROM faq WHERE service_id = '".explode('-',$category_id)[0]."' AND status = 'Approved' ORDER BY id DESC");
        $link = DB::table('link')->where('page', 'Sub Services')->first();
        $footer = $this->footer();
        return view('sub_service')->with(compact('header','cats','link','sub_service','serv','category_id','footer', 'questions', 'faqs'));
    }

    public function layout_plans($slug = '')
    {
        if($slug == ''){
            abort(404);
        }
        $cat = [];
        $plan_cat = false;
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$slug."'");
        if(count($plan_cat) <= 0){
            abort(404);
        }else{
            $cat = $plan_cat[0];
        }

        $header = $this->retailHeader(translate($cat->name),false,true);
        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        $cats = null;
        if($design_categorycon !== '') {
            $cats = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $cats = 'Data Not Available';
        }
        $layout_plans = DB::select('SELECT * FROM layout_plans Where category = '.$cat->id.' ORDER BY `created_at` DESC');
        $link = DB::table('link')->where('page', 'Layout Plan')->first();
        $footer = $this->footer();
        return view('layout_plan')->with(compact('header','cat','link','cats', 'layout_plans','plan_cat','footer'));
    }
    public function layout_plans_cat($slug = '')
    {
        if($slug == ''){
            abort(404);
        }
        $cat = [];
        $plan_cat = false;
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$slug."'");
        if(count($plan_cat) <= 0){
            abort(404);
        }else{
            $cat = $plan_cat[0];
        }

        $header = $this->retailHeader(translate($cat->name),false,true);
        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $cats = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $cats = 'Data Not Available';
        }
        $banner = DB::select("SELECT * FROM design_cat_banner");
        $layout_plans = DB::select('SELECT * FROM design_category Where parent = '.$cat->id.' ORDER BY `name` ASC');
        $featured = DB::select("SELECT * FROM layout_plans Where featured = '1' LIMIT 12");
        $best = DB::select("SELECT * FROM best_for_layout ORDER BY id DESC LIMIT 2");

        $link = DB::table('link')->where('page', 'Layout Plan Category')->first();
        $footer = $this->footer();
        return view('layout_plan_cat')->with(compact('header','cat','link','cats', 'banner', 'best', 'featured', 'layout_plans','footer'));
    }

    public function layout_plan(Request $request, $slug = ''){
        if($slug == ''){
            abort(404);
        }
        $layout = false;
        $layout = DB::table('layout_plans')->where('slug', '=', $slug)->first();
        if(!$layout){
            abort(404);
        }
        $cookie_name = "sellerkit.recentplans";
        if($request->session()->get($cookie_name) == null){
            $request->session()->put($cookie_name, $layout->id);
        }else{
            $request->session()->put($cookie_name, $request->session()->get($cookie_name).','.$layout->id);
            //$response->withCookie(cookie($cookie_name, $request->cookie($cookie_name)).','.$product_id, 43200);
        }
        $questions = DB::select("SELECT * FROM layout_plan_questions WHERE plan_id = '".$layout->id."' Order By priority ASC");
        $addons = DB::select("SELECT * FROM layout_plan_addon WHERE layout_plan_id = '".$layout->id."'");
        if($addons === null){
            $addons = false;
        }
        $tp = url("/themes/".$this->cfg->theme);
        $images = explode(',',$layout->images);
        $header = $this->retailHeader(translate($layout->title),false,true);
        $related = DB::select("SELECT * FROM layout_plans WHERE id <> ".$layout->id." and category=".$layout->category." ORDER BY Rand() LIMIT 18");
        $footer = $this->footer();

        $total_ratings = DB::select("SELECT COUNT(*) as count FROM layout_reviews WHERE active = 1 AND layout = ".$layout->id)[0]->count;
        $total_reviews = DB::select("SELECT COUNT(*) as count FROM layout_reviews WHERE active = 1 AND review <> '' AND layout = ".$layout->id)[0]->count;
        $rating = 0;
        if ($total_ratings > 0){
            $rating_summ = DB::select("SELECT SUM(rating) as sum FROM layout_reviews WHERE active = 1 AND layout = ".$layout->id)[0]->sum;
            $rating = $rating_summ / $total_ratings;
        }
        $reviews = DB::select("SELECT * FROM layout_reviews WHERE layout = ".$layout->id." AND active = 1 ORDER BY time DESC");
        $faqs = DB::select("SELECT * FROM layout_faq WHERE layout_id = ".$layout->id." AND status = 'Approved' ORDER BY id DESC");
        $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM layout_reviews WHERE active = '1' AND layout = '".$layout->id."'")[0];
        $total_rating =  $rating->total_rating;
        $total_user = $rating->total_user;
        if($total_rating==0){
            $avg_rating = 0;
        }
        else{
            $avg_rating = round($total_rating/$total_user,1);
        }
        $rating1= DB::select("SELECT count(id) as rating1_user FROM layout_reviews WHERE rating = '1' AND layout = '".$layout->id."' AND active = '1'")[0];
        $rating2= DB::select("SELECT count(id) as rating2_user FROM layout_reviews WHERE rating = '2' AND layout = '".$layout->id."' AND active = '1'")[0];
        $rating3= DB::select("SELECT count(id) as rating3_user FROM layout_reviews WHERE rating = '3' AND layout = '".$layout->id."' AND active = '1'")[0];
        $rating4= DB::select("SELECT count(id) as rating4_user FROM layout_reviews WHERE rating = '4' AND layout = '".$layout->id."' AND active = '1'")[0];
        $rating5= DB::select("SELECT count(id) as rating5_user FROM layout_reviews WHERE rating = '5' AND layout = '".$layout->id."' AND active = '1'")[0];
        $link = DB::table('link')->where('page', 'Single Plan')->first();
        DB::update("UPDATE layout_plans SET visits = visits+1 WHERE id = '".$layout->id."'");
        return view('plan')->with(compact('header','layout','link', 'images', 'tp', 'questions', 'related', 'footer', 'addons', 'total_ratings',
            'rating', 'reviews', 'faqs', 'rating1', 'rating2', 'rating3', 'rating4', 'rating5', 'avg_rating', 'total_user', 'total_reviews'));
    }

    public function layoutreview(){
        if (isset($_POST['review'])){

            //dd('hello');
            // escape review details and insert them into the database
            //$name = escape(htmlspecialchars($_POST['name']));
            if(isset($_POST['rating'])) {
                $rating = (int)$_POST['rating'];
            }else {
                $rating = 0;
            }
            $review = escape(htmlspecialchars($_POST['review']));
            $layout = (int)$_POST['layout_id'];
            $name = Customer('id');
            DB::insert("INSERT INTO layout_reviews (name,rating,review,layout,time,active) VALUE ('$name','$rating','$review','$layout','".time()."','0')");
            return back();
        }
    }

    public function createproject(){
        if (isset($_POST['submit'])){

            //dd('hello');
            // escape review details and insert them into the database
            $title = escape(htmlspecialchars($_POST['title']));
            $user_id = (int)$_POST['user_id'];
                if (request()->file('image')) {
                    // Upload the downloadable file to product downloads directory
                    $name = request()->file('image')->getClientOriginalName();
                    $file = md5(time()).'.'.request()->file('image')->getClientOriginalExtension();
                    $path = base_path().'/assets/'.'projects/';
                    request()->file('image')->move($path,$file);
                    $img = Image::make($path.$file)->resize(300, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path.$file);
                }
            DB::insert("INSERT INTO user_project (title,user_id,image) VALUE ('$title','$user_id','$file')");
            return back();
        }
    }

    public function photoreview(){
        if (isset($_POST['review'])){

            //dd('hello');
            // escape review details and insert them into the database
            //$name = escape(htmlspecialchars($_POST['name']));
            if(isset($_POST['rating'])) {
                $rating = (int)$_POST['rating'];
            }else {
                $rating = 0;
            }
            $review = escape(htmlspecialchars($_POST['review']));
            $photo_id = (int)$_POST['photo_id'];
            $name = Customer('id');
            DB::insert("INSERT INTO photo_reviews (name,rating,photo,review,time,active) VALUE ('$name','$rating','$photo_id','$review','".time()."','0')");
            return back();
        }
    }

    //puneet 06/02/2019

    public function advicesreview(){
        if (isset($_POST['review'])){

            //dd('hello');
            // escape review details and insert them into the database
            //$name = escape(htmlspecialchars($_POST['name']));
            if(isset($_POST['rating'])) {
                $rating = (int)$_POST['rating'];
            }else {
                $rating = 0;
            }
            $review = escape(htmlspecialchars($_POST['review']));
            $advice_id = (int)$_POST['advice_id'];
            $name = Customer('id');
            DB::insert("INSERT INTO advices_reviews (name,advice_id,rating,review,time,active) VALUE ('$name','$advice_id','$rating','$review','".time()."','0')");
            return back();
        }
    }

    //end
    
    public function review(){
        if (isset($_POST['review'])){
            // escape review details and insert them into the database
            $name = (int)$_POST['name'];
            $rating = (int)$_POST['rating'];
            $review = escape(htmlspecialchars($_POST['review']));
            $product = (int)$_POST['product'];
            DB::insert("INSERT INTO reviews (name,rating,review,product,time,active) VALUE ('$name','$rating','$review','$product','".time()."','0')");
            return back();
        }
    }


    public function getRelatedPlansWithDesign(Request $request){
        $layout = DB::table('layout_plans')->where('slug', '=', $request->slug)->first();
        $related = DB::select("SELECT * FROM layout_plans WHERE id <> ".$layout->id." and category=".$layout->category." ORDER BY Rand() LIMIT 18");

        $html = '<div class="grid">';
        foreach($related as $rel){
            $html.='<div class="grid-item">
					<div class="tiles">
						<a href="'.url('/layout_plan/'.$rel->slug).'">
							<img src="'.url('/assets/layout_plans/'.image_order($rel->images)).'"/>
							<div class="icon-holder">
								<div class="icons mrgntop"><b>960</b> sq ft</div>
								<div class="icons mrgntop"><b>1</b> bed</div>
								<div class="icons mrgntop borderRight0"><b>1</b> bath</div>
								<div class="icons mrgnbot"><b>1</b> story</div>
								<div class="icons mrgnbot"><b>30ft</b> wide</div>
								<div class="icons mrgnbot borderRight0"><b>48ft</b> depth</div>
							</div>
						</a>
						
					</div>
				</div>';

        }
        $html.='</div>';
        return $html;
    }
    public function getRecentViewdPlansWithDesign(Request $request){
        $html='';
        $cookie_name = "sellerkit.recentplans";
        if($request->session()->get($cookie_name) != null){
            $related = DB::select("SELECT * FROM layout_plans WHERE slug <> '".$request->slug."' and id in (".$request->session()->get($cookie_name).") LIMIT 18");
            $html = '<div class="grid">';
            foreach($related as $rel){
                $html.='<div class="grid-item">
						<div class="tiles">
							<a href="'.url('/layout_plan/'.$rel->slug).'">
								<img src="'.url('/assets/layout_plans/'.image_order($rel->images)).'"/>
								<div class="icon-holder">
									<div class="icons mrgntop"><b>960</b> sq ft</div>
									<div class="icons mrgntop"><b>1</b> bed</div>
									<div class="icons mrgntop borderRight0"><b>1</b> bath</div>
									<div class="icons mrgnbot"><b>1</b> story</div>
									<div class="icons mrgnbot"><b>30ft</b> wide</div>
									<div class="icons mrgnbot borderRight0"><b>48ft</b> depth</div>
								</div>
							</a>
							
						</div>
					</div>';

            }
            $html.='</div>';
        }
        return $html;
    }
    public function getRelatedGalleryWithDesign(Request $request){
        $gallery = DB::table('photo_gallery')->where('slug', '=', $request->slug)->first();
        $related = DB::select("SELECT * FROM photo_gallery WHERE id <> ".$gallery->id." and category=".$gallery->category." ORDER BY Rand() LIMIT 18");

        $html = '<div class="grid">';
        foreach($related as $rel){
            $html.='<div class="grid-item">
					<div class="tiles">
						<a href="'.url('/photo-gallery/'.$rel->slug).'">
							<img src="'.url('/assets/images/gallery/'.$rel->image).'"/>
							<div class="icon-holder">
								<div class="icons1 mrgntop">'.substr(translate($rel->title), 0, 20).'';
            if(strlen($rel->title)>20) $html.= '...';
            $html.='</div>
									<div class="icons2 mrgntop borderRight0">'.getPlanPhotsCount($rel->id).' Photos</div>
							</div>
						</a>
						
					</div>
				</div>';

        }
        $html.='</div>';
        return $html;
    }
    public function getRecentViewdGalleryWithDesign(Request $request){
        $html='';
        $cookie_name = "sellerkit.recentgalleries";
        if($request->session()->get($cookie_name) != null){
            $related = DB::select("SELECT * FROM photo_gallery WHERE slug <> '".$request->slug."' and id in (".$request->session()->get($cookie_name).") LIMIT 18");
            $html = '<div class="grid">';
            foreach($related as $rel){
                $html.='<div class="grid-item">
						<div class="tiles">
							<a href="'.url('/photo-gallery/'.$rel->slug).'">
								<img src="'.url('/assets/images/gallery/'.$rel->image).'"/>
								<div class="icon-holder">
									<div class="icons1 mrgntop">'.substr(translate($rel->title), 0, 20).'';
                if(strlen($rel->title)>20) $html.= '...';
                $html.='</div>
									<div class="icons2 mrgntop borderRight0">'.getPlanPhotsCount($rel->id).' Photos</div>
								</div>
							</a>
							
						</div>
					</div>';

            }
            $html.='</div>';
        }
        return $html;
    }

    public function photos($slug = '')
    {
        $cat = [];
        $plan_cat = false;
        if ($slug == ''){
            return redirect('/');
        }
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$slug."'");
        if(count($plan_cat) <= 0){
            abort(404);
        }else{
            $cat = $plan_cat[0];
        }

        $header = $this->retailHeader(translate($cat->name),false,true);
        $galleries = DB::select("SELECT * FROM photo_gallery where category = ".$cat->id." ORDER BY created_at DESC");
        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $cats = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $cats = 'Data Not Available';
        }
        $footer = $this->footer();
        return view('photos')->with(compact('header', 'cat', 'cats', 'galleries', 'footer'));
    }

    public function photos_cat($slug = '')
    {
        $cat = [];
        $plan_cat = false;
        if ($slug == ''){
            return redirect('/');
        }
        $plan_cat = DB::select("SELECT * FROM design_category WHERE slug = '".$slug."'");
        if(count($plan_cat) <= 0){
            abort(404);
        }else{
            $cat = $plan_cat[0];
        }

        $header = $this->retailHeader(translate($cat->name),false,true);
        $galleries = DB::select("SELECT * FROM design_category where parent = ".$cat->id." ORDER BY name ASC");
        $featured = DB::select("SELECT * FROM photo_gallery where featured = '1' ORDER BY id ASC LIMIT 12");
        $best = DB::select("SELECT * FROM best_for_photo ORDER BY id ASC LIMIT 2");
        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $cats = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $cats = 'Data Not Available';
        }
        $link = DB::table('link')->where('page', 'Photos Category')->first();
        $footer = $this->footer();
        return view('photos_cat')->with(compact('header', 'cat', 'link','cats', 'featured','best', 'galleries', 'footer'));
    }

    public function photo_gallery(Request $request, $slug = '')
    {
        $cat = [];
        $photo_cat = false;
        if ($slug == ''){
            return redirect('/');
        }
        $plan_cat = DB::select("SELECT * FROM photo_gallery WHERE slug = '".$slug."'");

        if(count($plan_cat) <= 0){
            abort(404);
        }else{
            $cat = $plan_cat[0];
        }
        $cookie_name = "sellerkit.recentgalleries";
        if($request->session()->get($cookie_name) == null){
            $request->session()->put($cookie_name, $cat->id);
        }else{
            $request->session()->put($cookie_name, $request->session()->get($cookie_name).','.$cat->id);
            //$response->withCookie(cookie($cookie_name, $request->cookie($cookie_name)).','.$product_id, 43200);
        }

        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate($cat->title),false,true);
        $images = DB::table('photos')->where('gallery_id', '=', $cat->id)->get();
        $related = DB::select("SELECT * FROM photo_gallery WHERE id <> ".$cat->id." and category=".$cat->category." ORDER BY Rand() LIMIT 18");
        // Apply the product filters
        $design_categorycon = DB::select('SELECT COUNT(*) as count FROM design_category WHERE parent = 0 ORDER BY name ASC');
        if($design_categorycon !== '') {
            $cats = DB::select('SELECT * FROM design_category WHERE parent = 0 ORDER BY name ASC');
        }else {
            $cats = 'Data Not Available';
        }
        $likes = DB::select("SELECT COUNT(*) as count FROM image_like WHERE image_id = '".$cat->id."'")[0]->count;
        $inspi = DB::select("SELECT COUNT(*) as count FROM image_inspiration WHERE image_id = '".$cat->id."'")[0]->count;

        $total_ratings = DB::select("SELECT COUNT(*) as count FROM photo_reviews WHERE active = 1 AND photo = ".$cat->id)[0]->count;
        $total_reviews = DB::select("SELECT COUNT(*) as count FROM photo_reviews WHERE active = 1 AND review <> '' AND photo = ".$cat->id)[0]->count;
        $rating = 0;
        if ($total_ratings > 0){
            $rating_summ = DB::select("SELECT SUM(rating) as sum FROM photo_reviews WHERE active = 1 AND photo = ".$cat->id)[0]->sum;
            $rating = $rating_summ / $total_ratings;
        }
        $reviews = DB::select("SELECT * FROM photo_reviews WHERE photo = ".$cat->id." AND active = 1 ORDER BY time DESC");
        $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM photo_reviews WHERE active = '1' AND photo = '".$cat->id."'")[0];
        $total_rating =  $rating->total_rating;
        $total_user = $rating->total_user;
        if($total_rating==0){
            $avg_rating = 0;
        }
        else{
            $avg_rating = round($total_rating/$total_user,1);
        }
        $rating1= DB::select("SELECT count(id) as rating1_user FROM photo_reviews WHERE rating = '1' AND photo = '".$cat->id."' AND active = '1'")[0];
        $rating2= DB::select("SELECT count(id) as rating2_user FROM photo_reviews WHERE rating = '2' AND photo = '".$cat->id."' AND active = '1'")[0];
        $rating3= DB::select("SELECT count(id) as rating3_user FROM photo_reviews WHERE rating = '3' AND photo = '".$cat->id."' AND active = '1'")[0];
        $rating4= DB::select("SELECT count(id) as rating4_user FROM photo_reviews WHERE rating = '4' AND photo = '".$cat->id."' AND active = '1'")[0];
        $rating5= DB::select("SELECT count(id) as rating5_user FROM photo_reviews WHERE rating = '5' AND photo = '".$cat->id."' AND active = '1'")[0];
        $link = DB::table('link')->where('page', 'Photo Gallery')->first();
        DB::update("UPDATE photo_gallery SET visits = visits+1 WHERE id = '".$cat->id."'");
        $footer = $this->footer();
        return view('photo_gallery')->with(compact('header', 'cat','link', 'tp', 'likes', 'inspi', 'cats', 'images','related', 'footer',
            'total_user', 'total_reviews', 'total_rating', 'total_ratings', 'avg_rating', 'reviews', 'rating', 'rating1', 'rating2', 'rating3', 'rating4', 'rating5'));
    }
    
   public function advicesCat($cat)
    {
        $header = $this->retailHeader(false,false,false,true);
        $style = $this->style;
        if($cat) {
            $posts = AdviceCategory::where('parent',$cat)->orderBy('id','DESC')->get();
        }else {
            $posts = Blog::orderBy('time','DESC')->get();
        }
        $footer = $this->footer();
        $cats = AdviceCategory::where('parent','0')->get();
        $data['tp'] = url("/assets/");
        $dimg = AdviceCategory::where('slug', $cat)->first();
        $btotal = Blog::where('category',$cat)->get();
        $featured = Blog::where('featured','1')->limit('12')->get();
        $best = BestForAdvice::orderBy('id','DESC')->limit(2)->get();
        $link = Link::where('page', 'Advices')->first();
        return view('advices_cat')->with(compact('header','style','link','footer','dimg','featured','best','btotal','data','posts','cats'));
    }


    /*public function sub_service($category_id)
    {
        // Check if product exists and return product details
        $check = DB::select("SELECT COUNT(*) as count FROM services WHERE category = '".explode('-',$category_id)[0]."'")[0];
        if ($check->count == 0){
            abort(404);
        }
        $service = DB::select("SELECT * FROM services WHERE category = '".explode('-',$category_id)[0]."'")[0];
        $cat = DB::select("SELECT * FROM services_category WHERE id = ".$service->category)[0];
        $images = explode(',',$service->images);
        $title = $service->title;
        $desc = mb_substr($service->description,0,75);
        $header = $this->header(translate($title),$desc,true);
        // Select product reviews from the database

        $tp = url("/themes/".$this->cfg->theme);
        $footer = $this->footer();
        return view('sub_service')->with(compact('header','service','images','cat','tp','footer'));
    }*/




    public function service($category_id)
    {
        // Check if product exists and return product details
        $check = DB::select("SELECT COUNT(*) as count FROM services WHERE id = '".explode('-',$category_id)[0]."'")[0];
        if ($check->count == 0){
            abort(404);
        }
        $service = DB::select("SELECT * FROM services WHERE id = '".explode('-',$category_id)[0]."'")[0];
        $cat = DB::select("SELECT * FROM services_category WHERE id = ".$service->category)[0];
        $images = explode(',',$service->images);
        $title = $service->title;
        $desc = mb_substr($service->description,0,75);
        $header = $this->retailHeader(translate($title),$desc,true);
        // Select product reviews from the database
        $faqs = DB::select("SELECT * FROM faq WHERE service_id = '".explode('-',$category_id)[0]."' AND status = 'Approved' ORDER BY id DESC");
        $tp = url("/themes/".$this->cfg->theme);
        $ladvices = DB::select("SELECT * FROM blog WHERE FIND_IN_SET (".$category_id.", services) ORDER BY id DESC LIMIT 2");
        $tadvices = DB::select("SELECT * FROM blog WHERE FIND_IN_SET (".$category_id.", services) ORDER BY visits DESC LIMIT 2");
        $reviews = DB::select("SELECT * FROM services_reviews WHERE services = ".$category_id." AND active = 1 ORDER BY time DESC LIMIT 2");
        $questions = DB::select("SELECT * FROM service_category_questions WHERE service_id = '".explode('-',$category_id)[0]."' Order By priority ASC");
        $footer = $this->footer();
        return view('service')->with(compact('header','service','faqs','ladvices','tadvices','questions','reviews','images','cat','tp','footer'));
    }

    public function questions($service_id)
    {
        $tp = url("/themes/".$this->cfg->theme);
        $footer = $this->footer();
        $header = $this->retailHeader();
        $service = DB::select("SELECT * FROM services WHERE id = '".$service_id."'")[0];
        // Check if product exists and return product details
        $check = DB::select("SELECT COUNT(*) as count FROM services_questions WHERE service_id = '".$service_id."'")[0];
        if ($check->count == 0){
            $questions = null;
            return view('questions')->with(compact('header','questions','tp','footer', 'service'));
        }


        $title = $service->title;
        $questions = DB::select("SELECT * FROM services_questions WHERE service_id = '".$service_id."' Order By priority ASC");


        // Select product reviews from the database
        $header = $this->retailHeader(translate($title),true);

        return view('questions')->with(compact('header','questions','service','tp','footer'));
    }




    public function blog()
    {
        $header = $this->retailHeader(translate('Blog'),false,true);
        $posts = DB::select("SELECT * FROM blog ORDER BY time DESC");
        $footer = $this->footer();
        return view('blog')->with(compact('header','posts','footer'));
    }
    public function post(Request $request, $post_id)
    {
        $tp = url("/themes/".$this->cfg->theme);
        $recent_viewed = 0;
        $cookie_name = "sellerkit.recent";
        if($request->session()->get($cookie_name) == null){
            $request->session()->put($cookie_name, $post_id);
        }else{
            $recent_viewed = Blog::where('slug',$request->session()->get($cookie_name))->first();
            $request->session()->put($cookie_name, $request->session()->get($cookie_name).','.$post_id);

            //$response->withCookie(cookie($cookie_name, $request->cookie($cookie_name)).','.$product_id, 43200);
        }
        // Check if blog post exists and return post details
        $check = Blog::where('slug',$post_id)->count();
        if ($check == 0){
            abort(404);
        }
        $post = Blog::where('slug',$post_id)->first();
        $updatevisit = Blog::find($post->id);
        $updatevisit->visits = $updatevisit->visits+1;
        $updatevisit->save();
        $title = $post->title;
        $desc = mb_substr($post->content,0,75);
        $header = $this->retailHeader(translate($post->title),$desc,false,true,url('/assets/blog/'.$post->images));
        // Get blocs of the page builder
        $blocs = Blocs::where('area','=','post')->orderBy('o','DESC')->get();
        $auditorials = Blog::where('auditorial_pics','=','Yes')->orderBy('id','DESC')->get();
        $total_ratings = AdviceReview::where('active','=','1')->where('advice_id','=',$post->id)->count();
        $total_reviews = AdviceReview::where('active','=','1')->where('advice_id','=',$post->id)->where('review','!=','')->count();
        $like = AdviceLike::where('advice_id',$post->id)->count();
        $rating = 0;
        if ($total_ratings > 0){
            $rating_summ = AdviceReview::where('active','=','1')->where('advice_id','=',$post->id)->sum('rating');
            $rating = $rating_summ / $total_ratings;
        }
        $reviews = AdviceReview::where('active','=','1')->where('advice_id','=',$post->id)->orderBy('time','DESC')->get();
        $faqs = AdviceFaq::where('advice_id',$post->id)->where('status','=','Approved')->orderBy('id','DESC')->get();
        $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$post->id."'")[0];
        $total_rating =  $rating->total_rating;
        $total_user = $rating->total_user;
        if($total_rating==0){
            $avg_rating = 0;
        }
        else{
            $avg_rating = round($total_rating/$total_user,1);
        }
        $rating1= DB::select("SELECT count(id) as rating1_user FROM advices_reviews WHERE rating = '1' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $rating2= DB::select("SELECT count(id) as rating2_user FROM advices_reviews WHERE rating = '2' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $rating3= DB::select("SELECT count(id) as rating3_user FROM advices_reviews WHERE rating = '3' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $rating4= DB::select("SELECT count(id) as rating4_user FROM advices_reviews WHERE rating = '4' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $rating5= DB::select("SELECT count(id) as rating5_user FROM advices_reviews WHERE rating = '5' AND advice_id = '".$post->id."' AND active = '1'")[0];
        $footer = $this->footer();
        $link = DB::table('link')->where('page', 'Post')->first();
        $related_advices = getRelatedAdvices($post->category, $post->id);
         $cats =AdviceCategory::where('parent','=','0')->get();
        return view('post')->with(compact('header','post','blocs','link','tp','footer', 'total_ratings','recent_viewed','like','related_advices',
            'rating', 'reviews', 'faqs', 'rating1', 'rating2', 'rating3', 'rating4','cats', 'rating5', 'avg_rating', 'total_user', 'total_reviews','auditorials'));
    }

    public function page($page_id)
    {
        // Check if the page exists and return page title and content
        $check = DB::select("SELECT COUNT(*) as count FROM pages WHERE path = '".$page_id."'")[0];
        if ($check->count == 0){
            abort(404);
        }
        $page = DB::select("SELECT * FROM pages WHERE path = '".$page_id."'")[0];
        $title = $page->title;
        $desc = mb_substr($page->content,0,75);
        $header = $this->retailHeader(translate($title),$desc,true);
        // Get blocs of the page builder
        $blocs = DB::select("SELECT * FROM blocs WHERE area = 'page' ORDER BY o ASC");
        $footer = $this->footer();
        return view('page')->with(compact('header','page','blocs','footer'));
    }
    public function support()
    {
        if(isset($_POST['send'])){
            if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['subject']) && !empty($_POST['message'])){
                // Escape the user entries and insert them to database
                $name = escape(htmlspecialchars($_POST['name']));
                $subject = escape(htmlspecialchars($_POST['subject']));
                $email = escape(htmlspecialchars($_POST['email']));
                $message = escape(htmlspecialchars($_POST['message']));
                DB::insert("INSERT INTO tickets (name,email,message,title) VALUE ('".$name."','".$email."','".$message."','".$subject."')");
                $sent = true;
            } else {
                $sent = false;
            }
        }
        $header = $this->retailHeader(translate('Support'),false,false,true,url('/support/map'));
        $cfg = $this->cfg;
        $footer = $this->footer();
        return view('support')->with(compact('header','sent','cfg','footer'));
    }
    public function map()
    {
        // Return a static map from the Google maps api
        $map = 'http://maps.googleapis.com/maps/api/staticmap?zoom=12&format=png&maptype=roadmap&style=element:geometry|color:0xf5f5f5&style=element:labels.icon|visibility:off&style=element:labels.text.fill|color:0x616161&style=element:labels.text.stroke|color:0xf5f5f5&style=feature:administrative.land_parcel|element:labels.text.fill|color:0xbdbdbd&style=feature:poi|element:geometry|color:0xeeeeee&style=feature:poi|element:labels.text.fill|color:0x757575&style=feature:poi.business|visibility:off&style=feature:poi.park|element:geometry|color:0xe5e5e5&style=feature:poi.park|element:labels.text|visibility:off&style=feature:poi.park|element:labels.text.fill|color:0x9e9e9e&style=feature:road|element:geometry|color:0xffffff&style=feature:road.arterial|element:labels|visibility:off&style=feature:road.arterial|element:labels.text.fill|color:0x757575&style=feature:road.highway|element:geometry|color:0xdadada&style=feature:road.highway|element:labels|visibility:off&style=feature:road.highway|element:labels.text.fill|color:0x616161&style=feature:road.local|visibility:off&style=feature:road.local|element:labels.text.fill|color:0x9e9e9e&style=feature:transit.line|element:geometry|color:0xe5e5e5&style=feature:transit.station|element:geometry|color:0xeeeeee&style=feature:water|element:geometry|color:0xc9c9c9&style=feature:water|element:labels.text.fill|color:0x9e9e9e&size=640x250&scale=4&center='.urlencode(trim(preg_replace('/\s\s+/', ' ', $this->cfg->address)));
        $con = curl_init($map);
        curl_setopt($con, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($con, CURLOPT_HEADER, 0);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, 1);
        return response(curl_exec($con))->header('Content-Type', 'image/png');
    }
    public function success()
    {
        $header = $this->retailHeader(translate('Success'),false,false,true);
        // remove cart products after the successfull payment
        setcookie('cart', '',time()+31536000,'/');
        $footer = $this->footer();
        return view('success')->with(compact('header','footer'));
    }
    public function failed()
    {
        $header = $this->retailHeader(translate('Failed'),false,false,true);
        $footer = $this->footer();
        return view('failed')->with(compact('header','footer'));
    }

    public function getLocality(){
        $options = array(
            'options' => array(
                'min_range' => 100000
            )
        );
        $pin = filter_var($_POST['pincode'], FILTER_VALIDATE_INT, $options);
        if($pin != ''){
            $data = null;
            $data = DB::select("select * from pincode where pincode=".$pin);
            if(count($data)>0){
                return $data[0]->city;
            }else{
                return 'Pincode Not Available';
            }
        }else{
            return 'Invalid Pincode';
        }

    }

    public function checkPincode(Request $request, $pincode){
        $cat = $request->category;
        //dd($cat);
        $record = DB::table('shipping')->whereRaw('FIND_IN_SET(?,postcodes)',[$pincode])->whereRaw('FIND_IN_SET(?,category)',[$cat])->get();
        $recordx = DB::table('pincode')
                    ->where('city','=',$pincode)
                    ->get();

//        ->join('shipping','pincode.id', '=', 'shipping.id')
//            DB::select("select * from shipping where postcodes LIKE '%".$pincode."%' AND FIND_IN_SET('".$cat."',category)");

        //dd($record);
        if(count($record) || count($recordx)){
            return 'success';
        }else{
            return 'error';
        }
    }


    public function advices($cat = false)
    {

        $header = $this->retailHeader(false,false,false,true);
        $style = $this->style;
        if($cat) {
            $posts = Blog::where('slug',$cat)->orderBy('time','DESC')->get();
            $dimg = AdviceCategory::where('slug', $cat)->first();
        }else {
            $posts = Blog::orderByDesc('time')->get();
        }
        $footer = $this->footer();

        $cats = AdviceCategory::where('parent','=','0')->get();
        $data['tp'] = url("/assets/");

        $btotal = Blog::where('category',$cat)->get();
        $link = Link::where('page', 'Advices')->first();
        return view('advices')->with(compact('header','style','link','footer','dimg','btotal','data','posts','cats'));
    }

    public function allAdvices($cat = false)
    {
        $header = $this->retailHeader(false,false,false,true);
        $style = $this->style;
        $posts = DB::select("SELECT * FROM blog ORDER BY time DESC");
        $footer = $this->footer();

        $cats = DB::select("SELECT * FROM advices_category WHERE parent = '0'");
        $data['tp'] = url("/assets/");
        $dimg = DB::table('advices_category')->orderBy('id','DESC')->first();
        $btotal = DB::select("SELECT * FROM blog WHERE category = '".$cat."'");
        $link = DB::table('link')->where('page', 'Advices')->first();
        return view('all-advices')->with(compact('header','style','link','footer','dimg','btotal','data','posts','cats'));
    }

    public function advicelike(){
        $user_id = customer('id');
        $image_id = $_POST['advice_id'];
        $advices = DB::insert("INSERT INTO advices_like (user_id, advice_id) VALUES ('".$user_id."','".$image_id."')");
    }
    public function advicesave(){
        $user_id = customer('id');
        $image_id = $_POST['advice_id'];
        $advices = DB::insert("INSERT INTO advices_save (user_id, advice_id) VALUES ('".$user_id."','".$image_id."')");
    }
    public function emailSend(){
        $advice_id = $_POST['advice_id'];
        $email = $_POST['email'];

        $post = DB::select("SELECT * FROM blog WHERE id = '".$advice_id."'")[0];

        $to = $email;
        $subject = "Advice of Space Door sent to you by your friend";
        $a = "Title ";
        $b = "Short Description ";
        $c = "Content ";

        $title = $post->title;
        $short_desc = $post->short_des;
        $content = $post->content;


        $message = $a.$title.$b.$short_desc.$c.$content;
        $message="
            <html>
            <body>
            <table>
              <tr><td>".$title."</td></tr>
              <tr><td>".$short_desc."</td></tr>
              <tr><td>".$content."</td></tr>
          </table>
            </body>
            </html>
            ";

        $headers = "From: info@specedoor.com\r\n";
        $headers .= "Content-type: text/html\r\n";

        $sendmail = mail($to,$subject,$message,$headers);


        if ($sendmail) {
            return back();
        }
        else {
            return back();
        }
    }


    public function sellerservices(){
        $id = Customer('id');
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Account'),false,true);
        $servicewishlist = DB::select("SELECT * FROM service_wishlist WHERE user_id = ".$id." ORDER BY id DESC ");
        $my_product = DB::select("SELECT id FROM products WHERE added_by = ".$id." ORDER BY id DESC ");
        $orders = DB::select("SELECT * FROM services_answer WHERE user_id = ".$id." ORDER BY id DESC ");
        $categories = DB::select("SELECT * FROM category WHERE parent = 0 ORDER BY id DESC");
        $units = DB::select("SELECT * FROM units ORDER BY name ASC");
        $all_product = DB::select("SELECT * FROM products");
        $sid = customer('id');
        $balance = DB::select("SELECT * FROM multiseller_payments WHERE `seller_id` = '$sid' ORDER BY id ASC");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        return view('seller_services')->with(compact('header','orders','cities','states','dimg','pros','countries','categories','balance','units','servicewishlist','my_product','msg','tp','all_product','insert_check','this_variants','footer'))->render();
    }


    public function sellerPlan()
    {
        $header = $this->retailHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM layout_order WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $planwishlist = DB::select("SELECT * FROM layout_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $imgins = DB::select("SELECT * FROM image_inspiration WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        return view('seller_design')->with(compact('header','orders','planwishlist','dimg','imgins','pros','footer','cities','states','countries'))->render();
    }






    // abhijeet code start
    public function selleraccount(Request $request)
    {
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailretailHeader(translate('Account'),false,true);
        if(isset($_POST['submit'])){
            $name = $_POST['name'];
            $mobile = $_POST['mobile'];
            $alternate_mobile = $_POST['alternate_mobile'];
            $email = $_POST['email'];
            $id = $_POST['id'];


            if (request()->file('header_image')) {
                // Upload the downloadable file to product downloads directory
                $name1 = request()->file('header_image')->getClientOriginalName();
                //dd($name);
                $file = md5(time()).'1.'.request()->file('header_image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('header_image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);

                DB::update("UPDATE customers SET header_image = '".$file."' WHERE id = '".$id."'");
            }

            if (request()->file('header_image_1')) {

                $name2 = request()->file('header_image_1')->getClientOriginalName();
                $file1 = md5(time()).'2.'.request()->file('header_image_1')->getClientOriginalExtension();
                $path1 = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('header_image_1')->move($path1,$file1);
                $img = Image::make($path1.$file1)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path1.$file1);

                DB::update("UPDATE customers SET header_image_1 = '".$file1."' WHERE id = '".$id."'");
            }

            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name3 = request()->file('image')->getClientOriginalName();
                $file2 = md5(time()).'3.'.request()->file('image')->getClientOriginalExtension();
                $path2 = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('image')->move($path2,$file2);
                $img = Image::make($path2.$file2)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path2.$file2);

                DB::update("UPDATE customers SET image = '".$file2."' WHERE id = '".$id."'");
            }

            DB::update("UPDATE customers SET name = '$name',mobile = '$mobile',alternate_mobile = '$alternate_mobile',
                email = '$email' WHERE id = '".$id."'");
        }
        //$orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");

        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $categories = DB::select("SELECT * FROM category ORDER BY id ASC");
        $orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        return view('seller_account')->with(compact('header','orders','categories','dimg','footer','tp','cities','states','countries'))->render();
    }


    public function sellerproduct(Request $request)
    {
        //dd($request);
        $msg='';
        $id = Customer('id');
        $insert_check =0;
        $this_variants = '';
        if(isset($_POST['add_old_sku'])){
            $data['sku'] = $_POST['sku'];
            $sku = $_POST['sku'];
            $data['sale'] = $_POST['sale'];
            $data['min_qty'] = $_POST['min'];
            $data['quantity'] = (int)$_POST['q'];
            $product_detail = DB::select("SELECT * FROM products where sku ='$sku'")[0];

            $data['title'] = $product_detail->title;
            $data['text'] = $product_detail->text;
            $data['specification'] = $product_detail->specification;
            $data['price'] = $product_detail->price;
            $data['tax'] = $product_detail->tax;
            $data['category'] = $product_detail->category;
            $data['images'] = $product_detail->images;
            $data['download'] = $product_detail->download;
            $data['fb_cat'] = $product_detail->fb_cat;
            $data['hsn'] = $product_detail->hsn;
            $data['weight'] = $product_detail->weight;
            $data['weight_unit'] = $product_detail->weight_unit;
            $data['options'] = $product_detail->options;
            $data['added_by'] = $id;
            $data['status'] = 'pending';
            $is =  Customer('institutional_status');
            if($is!=0) {
                $data['institutional_price'] = $_POST['insti_price'];
                $data['institutional_sale_price'] = $_POST['insti_sale'];
                $data['institutional_min_quantity'] = $_POST['insti_min'];
            }

            $product = DB::table('products')->insertGetId($data);
            $insert_check = $product;
            $sku = $data['sku'];
            $this_variants = DB::select("SELECT * FROM product_variants WHERE product_id in (SELECT id FROM products WHERE sku ='$sku' ) AND status = 'approve' ");


        }
        if(isset($_POST['update_old_sku'])){
            $sale  = $_POST['sale'];
            $min_qty = $_POST['min'];
            $q = $_POST['q'];
            $pid = $_POST['product_id'];
            DB::update("UPDATE products SET sale = '".$sale."' , min_qty ='".$min_qty."' , quantity = '".$q."'  WHERE id = '".$pid."'");
            $msg = "Record Update Succesfully!";
        }
        if(isset($_POST['add_new_sku'])){
            $sku = $_POST['sku'];
            $data['title'] = $_POST['title'];
            $data['text'] = $_POST['text'];
            $data['specification'] = $_POST['specification'];
            $data['price'] = $_POST['price'];
            $data['tax'] = $_POST['tax'];
            $data['category'] = (int)$_POST['category'];
            $category_id = (int)$_POST['category'];
            $category_detail = DB::select("SELECT parent,name FROM category where id = $category_id ")[0];
            $product_max_id = DB::select("SELECT max(id) as maxid FROM products")[0];
            $new_pid = $product_max_id->maxid +1;
            $pid=$category_detail->parent;
            $sku_code ='';
            $cat_name =  $category_detail->name;
            $sku_code = strtolower(substr($cat_name, 0, 3));
            if($pid==0){
                $sku_code = $sku_code;
            }else{
                while($pid !=0){
                    $category_detail = DB::select("SELECT parent,name FROM category where id = $pid")[0];
                    $sku_code = strtolower(substr($category_detail->name, 0, 3)).$sku_code;
                    $pid = $category_detail->parent;
                }
            }
            $sku_code = $sku_code.'-'.$new_pid;
            $data['sku'] = $sku_code;
            $data['quantity'] = (int)$_POST['q'];
            $data['images'] = '';
            $data['download'] = '';
            $data['fb_cat'] = '';
            $data['hsn'] = $_POST['hsn'];
            $data['min_qty'] = $_POST['min'];
            $data['sale'] = $_POST['sale'];
            $data['weight'] = $_POST['weight'];
            $data['weight_unit'] = $_POST['weight_unit'];
            $data['added_by'] = $id;
            $data['status'] = 'pending';
            $is =  Customer('institutional_status');
            if($is!=0) {
                $data['institutional_price'] = $_POST['insti_price'];
                $data['institutional_sale_price'] = $_POST['insti_sale'];
                $data['institutional_min_quantity'] = $_POST['insti_min'];
            }
            if(isset($_POST['fb_cat'])){
                $data['fb_cat'] = implode(',', $_POST['fb_cat']);
            }
            $options = array();
            if (isset($_POST['option_title'])){
                $choice_titles = $_POST['option_title'];
                $choice_types = $_POST['option_type'];
                $choice_no = $_POST['option_no'];
                if(count($choice_titles ) > 0){
                    foreach ($choice_titles as $i => $row) {
                        $choice_options = $_POST['option_set'.$choice_no[$i]];
                        $options[] = array(
                            'no' => $choice_no[$i],
                            'title' => $choice_titles[$i],
                            'name' => 'choice_'.$choice_no[$i],
                            'type' => $choice_types[$i],
                            'option' => $choice_options
                        );
                    }
                }
            }
            $data['options'] = json_encode($options);
            $product = DB::table('products')->insertGetId($data);

            if (request()->file('images')) {
                // Upload selected images to product assets directory
                $order = 0;
                $images = array();
                foreach (request()->file('images') as $file) {
                    $name = $file->getClientOriginalName();
                    if (in_array($file->getClientOriginalExtension(), array("jpg", "png", "gif", "bmp"))){
                        $images[] = $image = md5(time()).'-'.$order.'.'.$file->getClientOriginalExtension();
                        $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'products/';
                        $file->move($path,$image);
                        if($image) {
                            $img = Image::make($path. $image)->resize(800, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($path. $image);
                            $order++;
                        }
                    } else {

                    }
                }
                DB::update("UPDATE products SET images = '".implode(',',$images)."' WHERE id = '".$product."'");
            }
            if (request()->file('download')) {
                // Upload the downloadable file to product downloads directory
                $name = request()->file('download')->getClientOriginalName();
                $file = md5(time()).'.'.request()->file('download')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'downloads/';
                request()->file('download')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);
                DB::update("UPDATE products SET download = '".$file."' WHERE id = '".$product."'");
            }

        }
        if(isset($_POST['variant_add'])){
            $tot_variant = count($_POST['variant_id']);
            $product_id = $_POST['product_id'];
            for($i=0;$i<$tot_variant;$i++){
                $variant_id = $_POST['variant_id'][$i];
                $price = $_POST['price'][$i];
                $variant_name2 = DB::select("SELECT variant_title FROM `product_variants` WHERE id = '$variant_id'")[0];
                $variant_name = $variant_name2->variant_title;
                $variants = DB::insert("INSERT INTO product_variants (product_id,variant_title,price,display) VALUE ('$product_id','$variant_name','$price','1')");
            }
            $insert_check='';
        }

        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Account'),false,true);
        $collection = DB::select("SELECT * FROM product_wishlist WHERE user_id = ".$id." ORDER BY id DESC ");
        $my_product = DB::select("SELECT id FROM products WHERE added_by = ".$id." ORDER BY id DESC ");
		$myorders = DB::select("SELECT * FROM `orders` WHERE find_in_set('$id',seller_id)");
        $orders = DB::select("SELECT * FROM orders WHERE customer = ".$id." ORDER BY id DESC ");
        $categories = DB::select("SELECT * FROM category WHERE parent = 0 ORDER BY id DESC");
        $units = DB::select("SELECT * FROM units ORDER BY name ASC");
        $all_product = DB::select("SELECT * FROM products");
        $sid = customer('id');
        $balance = DB::select("SELECT * FROM multiseller_payments WHERE `seller_id` = '$sid' ORDER BY id ASC");
        $billwise = DB::table('multiseller_payments')->where('seller_id', '=', $sid)->groupBy('order_id')->get();
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");

        return view('seller_product')->with(compact('header','orders','categories','balance','dimg','pros','billwise','units','collection','my_product','myorders','msg','tp','all_product','insert_check','this_variants','footer'))->render();
    }

    public function viewDetail(){
        $html = '';
        $oid = $_POST['order_id'];
        $amt = $_POST['amount'];
        $balance = DB::select("SELECT * FROM multiseller_payments WHERE `order_id` = '$oid'");
        $html = '<div class="panel-heading">
                <h3 class="panel-title" style="text-align: left">Invoice No. '.$oid.'</h3>
                <h3 class="panel-title" style="text-align: right">Invoice Amount. '.$amt.'</h3>
            </div>
            <div class="table-responsive scroll-vertical">
                <table class="table table-striped table-bordered" id="datatable-editable1">
                    <thead>
                    <tr class="bg-blue">
                        <th>Received Amount</th>
                        <th>Received Date</th>
                    </tr>
                    </thead>
                    <tbody>';
                    foreach ($balance as $bill){
                        $html .='<tr>
                            <td>'.$bill->paid_amount.'</td>
                            <td>'.$bill->date_paid.'</td>
                        </tr>';
                    }
                    $html.='</tbody>
            </table>

        </div>';

        return $html;

    }


    public function GetProductDetail(){
        $sku = $_POST['sku'];
        $product = DB::select("SELECT c.name,p.* FROM `products` as p INNER JOIN category as c ON p.category = c.id INNER JOIN units as u ON p.weight_unit = u.id WHERE p.`sku` = '$sku'");
        if($product){
            return json_encode($product[0]);
        }else{
            return json_encode(array('error' => 'not_found'));
        }
    }
    public function MyProductDelete(){
        $pid = $_POST['pid'];
        $product = DB::delete("DELETE FROM products WHERE id = '".$pid."'");
    }
    public function MyProductEdit(){

     $pid = $_POST['pid'];
        $product = DB::select("SELECT c.name,p.* FROM `products` as p INNER JOIN category as c ON p.category = c.id INNER JOIN units as u ON p.weight_unit = u.id WHERE p.`id` = '$pid'");
            return json_encode($product[0]);
    }

    public function FindSku(){
        $pid = $_POST['pid'];
        $product = DB::select("SELECT sku from `products`  WHERE `id` = '$pid'")[0];
        echo $product->sku;

    }

    /*abhijeet code start*/
    public function ShippingStatusChange(){
        $oid = $_POST['oid'];
        $status = $_POST['status'];
        DB::insert("INSERT INTO shipping_status (po_id,status) VALUE ('$oid','$status')");

    }
    
    public function Tmtcalculator()
    {
        $header = $this->retailHeader(false,false,false,true);
        $style = $this->style;
        $tmtcalculator = DB::select("SELECT * FROM tmt_calculator GROUP by brand_id");

        $footer = $this->footer();
        $data['tp'] = url("/assets/");
        $dimg = DB::table('default_image')->where('page', 'Advices')->first();
        $link = DB::table('link')->where('page', 'Advices')->first();
        return view('tmtbarcalculator')->with(compact('header','style','link','footer','dimg','data','tmtcalculator','cats'));
    }

    public function AjexBrandChange()
    {
    $brand_id = $_POST['brand'];
    $tmtvariants = DB::select("SELECT * FROM tmt_calculator where brand_id='$brand_id'");
    $record = '<table class="table table-bordered tmt_calculator_table">
                                <thead>
                                <tr>
                                    <th class="border"></th>
                                    <th><input type="radio" name="checkme" class="kg checkme" value="kg" > Kgs</th>
                                    <th><input type="radio" name="checkme" class="rods checkme" value="rods" > No.of Rods</th>
                                    <th><input type="radio" name="checkme" class="bundle checkme" value="bundle" > No.of Bundles</th>
                                </tr>
                                </thead>
                                
                                <tbody>';
                            foreach ($tmtvariants as $tmtv){
                                $record .= '<tr class="first-row">
                                    <td class="border">'.$tmtv->variant_name.'</td>
                                    <td>
                                        <div class="input-box">
                                            <input type="text"  class="input-text kg kg_calculate" autocomplete="false" 
                                            onkeypress="return event.charCode >= 48 && event.charCode <= 57" >
                                            <span  class="kg_val">'.$tmtv->kg.'</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-box">
                                            <input type="text"  class="input-text rod rod_calculate" autocomplete="false" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  >
                                            <span  class="rod_val">'.$tmtv->rod.'</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-box">
                                            <input type="text"  class="input-text bundle bundle_calculate" autocomplete="false" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                            <span  class="bundle_val">'.$tmtv->bundle.'</span>
                                        </div>
                                    </td>
                                </tr>';
                            }
        $record .= '<tr class="total-border">
                                    <td>Total</td>
                                    <td><span id="kg-total">0</span><!-- <input type="text" id="txt3" /> --></td>
                                    <td><span id="rods-total">0</span></td>
                                    <td><span id="bundles-total">0</span></td>
                                </tr>
                                </tbody>
                            </table>';
    return $record;

    }

    /*abhijeet code end*/

    public function ProductVariant(Request $request){
        if($pid = $_GET['id']){
            if($request->add_variant=='Add Variant'){
                $variant_pid = $request->variant_pid;
                $variant_title = $request->variant_title;
                $variant_price = $request->price;
                $variant_display = $request->display;
                $variants = DB::insert("INSERT INTO product_variants (product_id,variant_title,price,display) VALUE ('$variant_pid','$variant_title','$variant_price','$variant_display')");
                $pid = $request->variant_pid;
            }
        }
        if(isset($_GET['delete'])){
            $product_id = $_GET['delete'];
            $query =  DB::delete("delete from product_variants where id = '$product_id'");
        }
        if(isset($_POST['edit_variant'])) {
            $var_id = $_POST['var_id'];
            $variant_title = $_POST['variant_title'];
            $price = $_POST['price'];
            $display = $_POST['display'];
             DB::update("UPDATE product_variants SET variant_title ='$variant_title', price = '$price', display  = '$display' WHERE id = '".$var_id."'");
        }
        $pid = $_GET['id'];
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Account'),false,true);
        $footer = $this->footer();
        $variants = DB::select("SELECT * FROM product_variants WHERE product_id = '$pid'");
        $products = DB::select("SELECT * FROM products WHERE id = '$pid'")[0];
        return view('add_variant')->with(compact('header','tp','variants','products','footer'))->render();

    }
    public function ProductDiscount(Request $request){
        if($pid = $_GET['id']){
            if($request->add_discount=='Add Discount'){
                $discount_pid = $request->discount_pid;
                $discount_quantity = $request->quantity;
                $discount_discount = $request->discount;
                $discount_discount_type = $request->discount_type;
                $discount_user_type = $request->user_type;
                $discount = DB::insert("INSERT INTO product_discount (product_id,quantity,discount,discount_type,user_type) VALUE ('$discount_pid','$discount_quantity','$discount_discount','$discount_discount_type','$discount_user_type')");
            }
        }
        if(isset($_GET['delete'])){
            $product_id = $_GET['delete'];
            $query =  DB::delete("delete from product_discount where id = '$product_id'");
        }
        if(isset($_POST['edit_discount'])){
            $dis_id = $_POST['dis_id'];
            $quantity = $_POST['quantity'];
            $discount = $_POST['discount'];
            $discount_type = $_POST['discount_type'];
             $user_type = $_POST['user_type'];
            DB::update("UPDATE product_discount SET quantity ='$quantity', discount = '$discount',user_type = '$user_type', discount_type  = '$discount_type' WHERE id = '".$dis_id."'");
        }
        $pid = $_GET['id'];
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Account'),false,true);
        $footer = $this->footer();
        $discounts = DB::select("SELECT * FROM product_discount WHERE product_id = '$pid'");
        $products = DB::select("SELECT * FROM products WHERE id = '$pid'")[0];
        return view('add_discount')->with(compact('header','tp','discounts','products','footer'))->render();
    }
    public function getDiscount(Request $request){
        $response = array();
        $response = DB::select("SELECT * FROM product_discount WHERE id = ".$_POST['discount_id'])[0];
        return json_encode($response);
    }
    public function getVariant(Request $request){
        $response = array();
        $response = DB::select("SELECT * FROM product_variants WHERE id = ".$_POST['variant_id'])[0];
        return json_encode($response);
    }
    public function getVariantAjax(Request $request){
        $response = array();
        if(!empty($_POST["keyword"])) {
            $query =DB::select("SELECT * FROM product_variants WHERE status='approve' and variant_title like '%" . $_POST["keyword"] . "%' ORDER BY id");
            if(!empty($query)) {
                ?>
                <ul id="variant-list">
                    <?php
                    foreach($query as $variant) {
                        //dd($variant);
                        ?>
                        <li onClick="selectCountry('<?php echo $variant->variant_title; ?>');"><?php echo $variant->variant_title; ?></li>
                    <?php } ?>
                </ul>
            <?php } }
    }
    public function AllSeller($sku){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Account'),false,true);
        $footer = $this->footer();
        $sellers =DB::select("SELECT c.*,p.* FROM `customers` as c INNER JOIN products as p on p.added_by = c.id  WHERE c.id in (SELECT p2.added_by FROM products as p2 WHERE p2.sku = '".$sku."') AND c.status ='1'  ORDER BY `p`.`sale` ASC , p.added_by ASC");
        return view('all_seller')->with(compact('header','tp','sellers','footer'));
    }

    // abhijeet code End
    
    public function contactUs(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Contact Us'),false,true);
        $footer = $this->footer();

        return view('contact-us')->with(compact('header','tp','footer'));
    }

    public function contactEmail(){
        $to = 'info@adroweb.com';
        $subject = "Advice of Space Door sent to you by your friend";
        $a = "Name ";
        $b = "Email ";
        $c = "Subject ";
        $d = "Message ";

        $name = $_POST['name'];
        $email = $_POST['email'];
        $sub  = $_POST['subject'];
        $msg = $_POST['message'];


        $message = $a.$name.$b.$email.$c.$sub.$d.$msg;
        $message="
            <html>
            <body>
            <table>
              <tr><td>".$a.":".$name."</td></tr>
              <tr><td>".$b.":".$email."</td></tr>
              <tr><td>".$c.":".$sub."</td></tr>
              <tr><td>".$d.":".$msg."</td></tr>
          </table>
            </body>
            </html>
            ";

        $headers = "From: info@specedoor.com\r\n";
        $headers .= "Content-type: text/html\r\n";

        $sendmail = mail($to,$subject,$message,$headers);


        if ($sendmail) {
            return back();
        }
        else {
            return back();
        }
    }

    public function payments(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Payments'),false,true);
        $footer = $this->footer();

        return view('payments')->with(compact('header','tp','footer'));
    }

    public function shipping(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Shipping'),false,true);
        $footer = $this->footer();

        return view('shipping')->with(compact('header','tp','footer'));
    }

    public function cancellation(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Cancellation & Return'),false,true);
        $footer = $this->footer();

        return view('cancellation')->with(compact('header','tp','footer'));
    }

    public function aboutUs(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('About Us'),false,true);
        $footer = $this->footer();

        return view('about-us')->with(compact('header','tp','footer'));
    }

    public function careers(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Careers'),false,true);
        $footer = $this->footer();

        return view('careers')->with(compact('header','tp','footer'));
    }

    public function stories(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Stories'),false,true);
        $footer = $this->footer();

        return view('stories')->with(compact('header','tp','footer'));
    }

    public function sell(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Sell with us'),false,true);
        $footer = $this->footer();

        return view('sell')->with(compact('header','tp','footer'));
    }

    public function onlineShopping(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Online Shopping'),false,true);
        $footer = $this->footer();

        return view('online-shopping')->with(compact('header','tp','footer'));
    }

    public function affiliateProgram(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Affiliate Program'),false,true);
        $footer = $this->footer();

        return view('affiliate-program')->with(compact('header','tp','footer'));
    }

    public function giftCard(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Gift Card'),false,true);
        $footer = $this->footer();

        return view('gift-card')->with(compact('header','tp','footer'));
    }

    public function subscription(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Subscription'),false,true);
        $footer = $this->footer();

        return view('subscription')->with(compact('header','tp','footer'));
    }

    public function sitemap(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Sitemap'),false,true);
        $footer = $this->footer();

        return view('sitemap')->with(compact('header','tp','footer'));
    }

    public function faqs(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('FAQ'),false,true);
        $footer = $this->footer();

        return view('faq')->with(compact('header','tp','footer'));
    }

    public function reportInfringement(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Report Infringement'),false,true);
        $footer = $this->footer();

        return view('report-infringement')->with(compact('header','tp','footer'));
    }

    public function termsConditions(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Terms and Conditions'),false,true);
        $footer = $this->footer();

        return view('terms-conditions')->with(compact('header','tp','footer'));
    }

    public function termsUse(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Terms of Use'),false,true);
        $footer = $this->footer();

        return view('terms-of-use')->with(compact('header','tp','footer'));
    }


    public function press(){
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->retailHeader(translate('Press'),false,true);
        $footer = $this->footer();

        return view('press')->with(compact('header','tp','footer'));
    }
    
    public function getFaq(Request $request){
        $html = '';
        $brands = $request->brands;
        //dd($brands);
        $faqs = array();
        if(customer('user_type') == 'institutional'){
            $query = DB::select("SELECT COUNT(*) as count FROM product_faq WHERE id < ".$_POST['id']." AND FIND_IN_SET (".$brands.", category) AND section = 'institutional' ORDER BY id DESC")[0]->count;
            if($query > 0) {
                $faqs = DB::select("SELECT * FROM product_faq WHERE FIND_IN_SET (".$brands.", category) AND id < ".$_POST['id']." AND section = 'institutional'  ORDER BY id DESC LIMIT 5");
        }else {
                $query = DB::select("SELECT COUNT(*) as count FROM product_faq WHERE id < " . $_POST['id'] . " AND FIND_IN_SET (" . $brands . ", category) AND section = 'retail'  ORDER BY id DESC")[0]->count;
                if ($query > 0) {
                    $faqs = DB::select("SELECT * FROM product_faq WHERE FIND_IN_SET (" . $brands . ", category) AND id < " . $_POST['id'] . " AND section = 'retail' ORDER BY id DESC LIMIT 5");
                }
            }

            foreach ($faqs as $faq) {
                $postID = $faq->id;
                $html = '<li>
                            <input type="checkbox" checked>
                            <i></i>
                            <h5>Q :  ' . $faq->question . '<br><small>

                                    by Admin on ';
                $html .= date('d M, Y', strtotime($faq->time)) . '</small></h5>
                            <p>A : ' . $faq->answer . '</p>
                        </li>
                        <div class="col-lg-12" style="padding-top: 10px; text-align: center;">
                        <div class="show_more_main" id="show_more_main' . $postID . '">
                <span id="' . $postID . '" class="show_more" title="Load more posts" style="cursor: pointer;">Show more</span>
                <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
            </div></div>';
            }
        }

        return $html;
    }



    public function getSerFaq(Request $request){
        $html = '';
        $brands = $request->brands;
        //dd($brands);
        $query = DB::select("SELECT COUNT(*) as count FROM faq WHERE id < ".$_POST['id']." AND service_id in (".$brands.") ORDER BY id DESC")[0]->count;
        if($query > 0) {
            $faqs = DB::select("SELECT * FROM faq WHERE FIND_IN_SET (".$brands.", service_id) AND id < ".$_POST['id']." ORDER BY id DESC LIMIT 5");
            foreach ($faqs as $faq) {
                $postID = $faq->id;
                $html = '<li>
                            <input type="checkbox" checked>
                            <i></i>
                            <h5>Q :  ' . $faq->question . '<br><small>';
                $u = $faq->user_id;
                $user = DB::select("SELECT * FROM customers WHERE id = '$u'")[0];

                                   $html .='by '.$user->name.' on ';
                $html .= date('d M, Y', strtotime($faq->time)) . '</small></h5>
                            <p>A : ' . $faq->answer . '</p>
                        </li>
                        <div class="col-lg-12" style="padding-top: 10px; text-align: center;">
                        <div class="show_more_main" id="show_more_main' . $postID . '">
                <span id="' . $postID . '" class="show_more" title="Load more posts" style="cursor: pointer;">Show more</span>
                <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
            </div></div>';
            }
        }

        return $html;
    }


    public function getSerRew(Request $request){
        $html = '';
        $brands = $request->service_id;
        //dd($brands);
        $query = DB::select("SELECT COUNT(*) as count FROM services_reviews WHERE id < ".$_POST['id']." AND services in (".$brands.") ORDER BY id DESC")[0]->count;
        if($query > 0) {
            $faqs = DB::select("SELECT * FROM services_reviews WHERE services in (" . $brands . ") AND id < ".$_POST['id']." ORDER BY id DESC LIMIT 1");
            $rewID = '';
            foreach($faqs as $review){
                $rewID = $review->id;
                $usid = $review->name;
                $uimg = DB::select("SELECT * FROM customers WHERE id = '".$usid."'")[0];
                $html .='<img class="review-image" src="assets/user_image/';
                    if($uimg->image != ''){ $html .= $uimg->image; } else { $html .='user.png'; }; $html .='">
                                        <div class="review-meta"><b>'.$uimg->name.'</b><br/>
                                        <span class="time">'.date('M d, Y',$review->time).'</span><br/></div>
                                        <div class="review">
                                            <div class="rating pull-right">';
                $rr = $review->rating; $i = 0; while($i<5){ $i++;?>
                    <i class="star<?=($i<=$review->rating) ? '-selected' : '';?>"></i>
                    <?php $rr--; }
                $html .= '</div>
                                        <div class="clearfix"></div>
                                        <p>'.nl2br($review->review).'</p></div>';
            }

        }

        return $html;
    }
    public function LikePlan(){
        $user_id = customer('id');
        $image_id = $_POST['plan_id'];
        $images = DB::insert("INSERT INTO plan_like (user_id, plan_id) VALUES ('".$user_id."','".$image_id."')");
    }
    
    
    //abhijeet code start
 
 public function institutionalaccount()
    {
        $tp = url("/themes/".$this->cfg->theme);
        $header = $this->institutionalHeader(translate('Account'),false,true);
        if(isset($_POST['submit'])){
            $name = $_POST['name'];
            $mobile = $_POST['mobile'];
            $alternate_mobile = $_POST['alternate_mobile'];
            $email = $_POST['email'];
            $company = $_POST['company'];
            $address_line_1 = $_POST['address_line_1'];
            $address_line_2 = $_POST['address_line_2'];
            $city = $_POST['city'];
            $state = $_POST['state'];
            $country = $_POST['country'];
            $postcode = $_POST['postcode'];

            if (request()->file('header_image')) {
                // Upload the downloadable file to product downloads directory
                $name1 = request()->file('header_image')->getClientOriginalName();
                //dd($name);
                $file = md5(time()).'1.'.request()->file('header_image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('header_image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);

                DB::update("UPDATE customers SET header_image = '".$file."' WHERE id = '".$_GET['edit']."'");
            }

            if (request()->file('header_image_1')) {

                $name2 = request()->file('header_image_1')->getClientOriginalName();
                $file1 = md5(time()).'2.'.request()->file('header_image_1')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('header_image_1')->move($path,$file1);
                $img = Image::make($path.$file1)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file1);

                DB::update("UPDATE customers SET header_image_1 = '".$file1."' WHERE id = '".$_GET['edit']."'");
            }

            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name3 = request()->file('image')->getClientOriginalName();
                $file2 = md5(time()).'3.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'user_image/';
                request()->file('image')->move($path,$file2);
                $img = Image::make($path.$file2)->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file2);

                DB::update("UPDATE customers SET image = '".$file2."' WHERE id = '".$_GET['edit']."'");
            }


            DB::update("UPDATE customers SET name = '$name',mobile = '$mobile',alternate_mobile = '$alternate_mobile',
                email = '$email',company = '$company',address_line_1 = '$address_line_1',address_line_2 = '$address_line_2',
                 city = '$city',state = '$state',country = '$country',postcode = '$postcode' WHERE id = '".$_GET['edit']."'");
        }
//        $orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");

        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");
        $porders = DB::select("SELECT * FROM layout_order WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $sorders = DB::select("SELECT * FROM services_answer WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $prowishlist = DB::select("SELECT * FROM product_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $planwishlist = DB::select("SELECT * FROM layout_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $servicewishlist = DB::select("SELECT * FROM service_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $imglike = DB::select("SELECT * FROM image_like WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $imgins = DB::select("SELECT * FROM image_inspiration WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        return view('institutional_account')->with(compact('header','orders','porders','sorders','imglike','imgins','dimg','servicewishlist','prowishlist','planwishlist','footer','tp','cities','states','countries'))->render();
    }
    public function institutional_myorders()
    {
        $header = $this->institutionalHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM orders WHERE customer = ".customer('id')." ORDER BY id DESC ");
        $footer = $this->footer();
        $prowishlist = DB::select("SELECT * FROM product_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        return view('institutional_myorders')->with(compact('header','orders','prowishlist','footer','dimg','pros','cities','states','countries'))->render();
    }


    public function institutional_userAdvices()
    {
        $header = $this->institutionalHeader(translate('Account'),false,true);
        $saved = DB::select("SELECT * FROM advices_save WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $footer = $this->footer();
        $liked = DB::select("SELECT * FROM advices_like WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        return view('institutional_useradvices')->with(compact('header','saved','liked','footer','dimg','cities','states','countries'))->render();
    }
    
    public function institutional_planOrders()
    {
        $header = $this->institutionalHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM layout_order WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $planwishlist = DB::select("SELECT * FROM layout_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $imgins = DB::select("SELECT * FROM image_inspiration WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $footer = $this->footer();
        return view('institutional_planorders')->with(compact('header','orders','planwishlist','imgins','dimg','pros','footer','cities','states','countries'))->render();
    }
    public function institutional_serviceOrders()
    {
        $header = $this->institutionalHeader(translate('Account'),false,true);
        $orders = DB::select("SELECT * FROM services_answer WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $servicewishlist = DB::select("SELECT * FROM service_wishlist WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $cities = DB::select("SELECT * FROM cities ORDER BY id ASC");
        $states = DB::select("SELECT * FROM states ORDER BY id ASC");
        $countries = DB::select("SELECT * FROM countries ORDER BY id ASC");
        $dimg = DB::table('default_image')->where('page', 'Account')->first();
        $pros = DB::select("SELECT * FROM user_project WHERE user_id = ".customer('id')." ORDER BY id DESC ");
        $footer = $this->footer();
        return view('institutional_serviceorders')->with(compact('header','orders','dimg','pros','servicewishlist','footer','cities','states','countries'))->render();
    }
    //abhijeet code end

    public function changeHub(Request $request){
        if(isset($_POST['hub'])){
            //Cookie::queue('aakar_hub', $_POST['hub'], 10080);
            setcookie('aakar_hub', $_POST['hub'], time()+60*60*24*365);
        }
        return back();
    }
   public function institutionalIndex()
    {
        $hub = 0;
        if(isset($_COOKIE['aakar_hub'])) {
            $hub = $_COOKIE['aakar_hub'];
        }
        $header = $this->institutionalHeader(false,false,false,true);
        $vtype=Session::get('aakar360.visitor_type');
        if($vtype == null){
            return redirect()->to('welcome');
        }elseif($vtype == 'individual'){
            return redirect()->to('');
        }
        $style = $this->style;
        if(preg_match("/(youtube.com)\/(watch)?(\?v=)?(\S+)?/", $this->style->media)){
            parse_str(parse_url($this->style->media, PHP_URL_QUERY),$video);
            $media = '<a target="_blank" href="'.$this->style->media.'"><img class="landing-video" src="https://i3.ytimg.com/vi/'.$video['v'].'/mqdefault.jpg"></a>';
        } else {
            // Show image
            $media = '<img class="landing-image" src="'.$this->style->media.'">';
        }
        $show_preferences = false;
        $preferences = null;
        if(customer('id') !== ''){
            if(isset($_POST['categories'])){
                if($_POST['categories'] != ''){
                    $preferences = Customer::find(customer('id'))->preferences()->where('type', 'category')->first();
                    if($preferences === null){
                        $pre = new CustomerCache();
                        $pre->user_id = customer('id');
                        $pre->type = 'category';
                        $pre->cache = $_POST['categories'];
                        $pre->save();
                        return redirect('institutional-index');
                    }else{
                        $preferences->cache = $_POST['categories'];
                        $preferences->save();
                    }
                }
            }
            $pre = Customer::find(customer('id'))->preferences()->where('type', 'category')->first();
            if($pre === null){
                $preferences = null;
                $show_preferences = true;
            }else{
                $preferences = explode(',', $pre->cache);
            }
        }else{
            if(isset($_POST['categories'])){
                setcookie('categories', $_POST['categories'], time() + (86400 * 30));
                return redirect('institutional-index');
            }
            if(isset($_COOKIE['categories'])){
                $preferences = explode(',', $_COOKIE['categories']);
            }else{
                $preferences = null;
            }
            if($preferences === null){
                $show_preferences = true;
            }

        }
        $categories = false;
        if($show_preferences){
            $categories = DB::table('category')->orderBy('name', 'ASC')->get();
        }
        $blocs1 = DB::select('SELECT * FROM blocs WHERE area = "home" ORDER BY o ASC');
        if(!empty($blocs1)) {
            $blocs = $blocs1;
        }else{
            $blocs = 'Data Not Available';
        }
        $banners1 = DB::select('Select * from banner where section="institutional_home" ORDER BY priority ASC');
        if(!empty($banners1)){
            $banners = $banners1;
        }else{
            $banners = false;
        }
        $posts1 = DB::select("SELECT * FROM blog ORDER BY time DESC LIMIT 8");
        if(!empty($posts1)){
            $posts = $posts1;
        }else{
            $posts = 'Data Not Available';
        }

        $categoryforhomebrand1 = DB::select("SELECT * FROM category WHERE popular_in_brands=1 ORDER BY popular_brands_priority ASC LIMIT 1");
        if(!empty($categoryforhomebrand1)){
            $categoryforhomebrand = $categoryforhomebrand1;
        }else{
            $categoryforhomebrand = 'Data Not Available';
        }
        $service_categories1 = DB::select("select * from services_category where parent = 0 order by name ASC");
        if(!empty($service_categories1)){
            $service_categories = $service_categories1;
        }else{
            $service_categories = 'Data Not Available';
        }
        $design_categories1 = DB::select("select * from design_category where parent = 0 order by name ASC");
        if (!empty($design_categories1)){
            $design_categories = $design_categories1;
        }else{
            $design_categories = 'Data Not Available';
        }
        $rem_pre = 0;
        if($preferences === null){
            $pcss = DB::table('category')->where('popular', 1)->orderBy('popular_priority', 'ASC')->skip(0)->take(5)->get();
        }else{
            $pcss = DB::table('category')->whereIn('id', $preferences)->orderBy('popular_priority', 'ASC')->skip(0)->take(5)->get();
            $re = count($pcss);
            $limit = 5 - $re;
            if($limit){
                $more = DB::table('category')->where('popular', 1)->orderBy('popular_priority', 'ASC')->skip(0)->take($limit)->get();
                $pcss = $pcss->merge($more);
            }else{
                $rem_pre = $re-5;
            }
        }

        $footer = $this->footer();
        $data['tp'] = url("/assets/");
		$categoryforhomebrand1 = DB::select("SELECT * FROM category WHERE popular_in_brands=1 ORDER BY popular_brands_priority ASC LIMIT 1");
        if(!empty($categoryforhomebrand1)){
            $categoryforhomebrand = $categoryforhomebrand1;
        }else{
            $categoryforhomebrand = 'Data Not Available';
        }
        $top_brands = DB::select("SELECT * FROM brand ORDER BY name ASC");
        $hubs = ManufacturingHubs::orderBy('title')->get();
        return view('institutional-index')->with(compact('header','style','media','blocs', 'top_brands', 'banners', 'posts','categoryforhomebrand','service_categories','design_categories', 'footer','data', 'preferences', 'show_preferences', 'pcss', 'categories', 'rem_pre', 'hub', 'hubs'));
    }

    public function getProducts(Request $request){
        $brands = $request->brands;
        $rem_pre = $request->rem_pre;
        $ret= array();
        $html = '';
        $cat_id = null;
        $nb= 0;
        $preferences = null;

        if(customer('id') !== ''){
            $pre = Customer::find(customer('id'))->preferences()->where('type', 'category')->first();
            if($pre === null){
                $preferences = null;
            }else{
                $preferences = explode(',', $pre->cache);
            }
        }else{
            if(isset($_POST['categories'])){
                setcookie('categories', $_POST['categories'], time() + (86400 * 30));
                //Cookie::queue('aakar360.institutional.categories', $_POST['categories'], 17280);
                return redirect('institutional-index');
            }
            if(isset($_COOKIE['categories'])){
                $preferences = explode(',', $_COOKIE['categories']);
            }else{
                $preferences = null;
            }

        }
        $query = Category::where('popular_category', '>', $brands)->where('popular','1')->whereNotIn('id',implode(',',$preferences))->orderBy('popular_priority','ASC')->get();
        if($rem_pre && $preferences !== null){
            $offset = count($preferences) - $rem_pre;
            array_splice($preferences, 0, $offset);
            if(count($preferences)){
                $query = Category::whereIn('id',implode(',',$preferences))->orderBy('popular_priority','ASC')->get();
            }
        }
        $categoryforhomebrand = DB::select($query.' LIMIT 1');
        if(count($categoryforhomebrand)){
            $topbrand = $categoryforhomebrand[0];
            foreach($categoryforhomebrand as $pc){
                $cat_id = $pc->id;
                $nb = $pc->popular_priority;
                $html = '<div class="row">
                    <div class="col-md-12 main-title">
                        <div class="line-separator">
                            <h4 class="title">'.$pc->name.'</h4>
                            <a class="view-all-category" href="'.url("products/".$pc->path).'">View All <i class="fa fa-caret-right"></i></a>
                        </div>
                        <div class="slide-6 theme-arrow product-m new">';
                            $pcp = Products::where('category',$pc->id)->orderBy('id','DESC')->take('6')->get();

                            foreach($pcp as $cp){
                                $html .='<div class="swiper-wrapper">
                                    <article class="swiper-slide">
                                        <div class="product-layout">
                                            <div class="product-thumb ">
                                                <div class="product-inner">
                                                    <div class="product-image">
                                                        <a href="product/'.path($cp->title, $cp->id).'">
                                                            <img src="'.url('/assets/products/'.image_order($cp->images)).'" alt="'.translate($cp->title).'" style="height: 138px;">
                                                        </a>
                                                        <div class="action-links">
                                                            <a href="javascript:void(0);" onclick="Shopify.addItem(14650596786287, 1); return false;" class="ajax-spin-cart">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </a>
                                                            <a class="wishlist action-btn btn-wishlist" href="" title="Wishlist">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
    
                                                        </div>
                                                    </div>
                                                    <!-- end of product-image -->
                                                    <div class="product-caption">
                                                        <h4 class="product-name text-ellipsis"> <a href="product/'.path($cp->title, $cp->id).'" title="'.translate($cp->title).'">'.translate($cp->title).'</a></h4>
                                                        <a href="product/'.path($cp->title, $cp->id).'" class="btn btn-primary btn-sm">View Detail</a>
                                                    </div>
                                                    <!-- end of product-caption -->
                                                </div>
                                                <!-- end of product-inner -->
                                            </div>
                                            <!-- end of product-thumb -->
                                        </div>
                                    </article>
                                </div>';

                            }
                        $html .='</div>
                    </div>
                </div>';
        }
        }
        $ret['brands'] = $brands;
        $ret['nb'] = $nb;
        if($rem_pre){
            $ret['rem_pre'] = (count($preferences) - 1) <= 0 ? 0 : (count($preferences) - 1);
        }else{
            $ret['rem_pre'] = 0;
        }
        $ret['html'] = $html;
        $ret['cat_id'] = $cat_id;
        $nextCat = $categoryforhomebrand = DB::select($query.' LIMIT 2');
        if(count($nextCat) == 2){
            $ret['collapse'] = false;
        }else{
            $ret['collapse'] = true;
        }
        return json_encode($ret);
    }
    public function getProduct(Request $request){
        $ret= array();
        $limit = $request->limit;
        $html = '';
        $nb = 0;
        $preferences = null;
        if(customer('id') !== ''){
            $pre = Customer::find(customer('id'))->preferences()->where('type', 'category')->first();
            if($pre === null){
                $preferences = null;
            }else{
                $preferences = explode(',', $pre->cache);
            }
        }else{
            if(isset($_COOKIE['categories'])){
                $preferences = explode(',', $_COOKIE['categories']);
            }else{
                $preferences = null;
            }
        }
        $rem_pre = 0;
        if($preferences === null){
            $categoryforhomebrand = Category::where('popular','1')->orderBy('popular_priority','ASC')->take($limit)->get();
        }else{
            $categoryforhomebrand = Category::whereIn('id',implode(',',$preferences))->orderBy('popular_priority','ASC')->take($limit)->get();
            $re = count($categoryforhomebrand);
            $limit = 5 - $re;
            if($limit){
                $more = Category::where('popular','1')->orderBy('popular_priority','ASC')->take($limit)->get();
                $categoryforhomebrand = $categoryforhomebrand->merge($more);
            }else{
                $rem_pre = $re-5;
            }

        }
        if(count($categoryforhomebrand)){
            $topbrand = $categoryforhomebrand[0];
            foreach($categoryforhomebrand as $pc){
                $cat_id = $pc->id;
                $nb = $pc->popular_priority;
                $pcp = DB::table('products')->where('category', $pc->id)->orderBy('id', 'DESC')->get();
                if(count($pcp) == 0){
                    continue;
                }
                $html .= '<div class="row">
                    <div class="col-md-12 main-title">
                        <div class="line-separator">
                            <h4 class="title">'.$pc->name.'</h4>
                            <a class="view-all-category" href="'.url("products/".$pc->path).'">View All <i class="fa fa-caret-right"></i></a>
                        </div>
                        <div class="slide-6 theme-arrow product-m new">';

                foreach($pcp as $cp){
                    $html .='<div class="swiper-wrapper">
                                    <article class="swiper-slide">
                                        <div class="product-layout">
                                            <div class="product-thumb ">
                                                <div class="product-inner">
                                                    <div class="product-image">
                                                        <a href="product/'.path($cp->title, $cp->id).'">
                                                            <img src="'.url('/assets/products/'.image_order($cp->images)).'" alt="'.translate($cp->title).'" style="height: 138px;">
                                                        </a>
                                                        <div class="action-links">
                                                            <a href="javascript:void(0);" onclick="Shopify.addItem(14650596786287, 1); return false;" class="ajax-spin-cart">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </a>
                                                            <a class="wishlist action-btn btn-wishlist" href="" title="Wishlist">
                                                                <i class="fa fa-heart"></i>
                                                            </a>
    
                                                        </div>
                                                    </div>
                                                    <!-- end of product-image -->
                                                    <div class="product-caption">
                                                        <h4 class="product-name text-ellipsis"> <a href="product/'.path($cp->title, $cp->id).'" title="'.translate($cp->title).'">'.translate($cp->title).'</a></h4>
                                                        <a href="product/'.path($cp->title, $cp->id).'" class="btn btn-primary btn-sm">View Detail</a>
                                                    </div>
                                                    <!-- end of product-caption -->
                                                </div>
                                                <!-- end of product-inner -->
                                            </div>
                                            <!-- end of product-thumb -->
                                        </div>
                                    </article>
                                </div>';

                }
                $html .='</div>
                    </div>
                </div>';
            }
        }
        $ret['brands'] = $nb;
        $ret['nb'] = $nb;
        $ret['html'] = $html;
        if($nb) {
            $nextCat = DB::select("SELECT * FROM category WHERE popular_priority > " . $nb . " AND popular = 1 ORDER BY popular_priority ASC LIMIT 1");
            if (count($nextCat)) {
                $ret['collapse'] = false;
            } else {
                $ret['collapse'] = true;
            }
        }else {
            $ret['collapse'] = false;
        }
        return json_encode($ret);
    }
    public function sendAsr(Request $request){
        if(isset($request->message)){
            $sr = new SpecialRequirements();
            $sr->message = $request->message;
            $sr->product_id = $request->pid;
            $sr->user_id = customer('id');
            $sr->save();
            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name1 = request()->file('image')->getClientOriginalName();
                //dd($name);
                $file = md5(time()).'1.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);

                $sr->file = $file;
                $sr->save();
            }
            return back()->with("success", ["Enquiry sent !"]);
        }
        return back()->withErrors('msg', ['Oops! Something went wrong. Try again later.']);
    }
    public function sendRfr(Request $request){
        if(isset($request->msg)){
            $sr = new RateRequests();
            $sr->message = $request->msg;
            $sr->product_id = $request->pid;
            $sr->user_id = customer('id');
            if(isset($request->variants)){
                $sr->variants = json_encode($request->variants);
            }
            $sr->save();
            if (request()->file('image')) {
                // Upload the downloadable file to product downloads directory
                $name1 = request()->file('image')->getClientOriginalName();
                //dd($name);
                $file = md5(time()).'1.'.request()->file('image')->getClientOriginalExtension();
                $path = base_path().DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
                request()->file('image')->move($path,$file);
                $img = Image::make($path.$file)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path.$file);

                $sr->file = $file;
                $sr->save();
            }
            return back()->with("successx", ["Request sent !"]);
        }
        return back()->withErrors('msgx', ['Oops! Something went wrong. Try again later.']);
    }

    public function requestQuotation(Request $request){
        if(isset($request->base_quote)){
               $base_quantity = $request->base_quantity;
               $base_location = $request->location;
               $user_id = customer('id');
			   $brands = '';
			   if(isset($request->brands_base)){
					$brands = implode(', ', $request->brands_base);
			   }
               $p_id = $request->product_id;
               $cat_id = $request->category_id;
               $payment_option = $request->payment_base;
               $method = 'bybasic';
            $variants = array();
            $variants[] = array(
                'v_id' => $p_id,
                'qty' => $base_quantity,
                'action' => '',
                'var_price' =>'',
            );
        $var = json_encode($variants);
            $id = DB::insert("INSERT INTO request_quotation (user_id,cat_id,brand_id,product_id,payment_option,method,location,variants) VALUES ('".$user_id."','".$cat_id."','".$brands."','".$p_id."','".$payment_option."','".$method."','".$base_location."','".$var."')");
            if($id){
                return back()->with("successx", ["Request sent !"]);
            }

        }
        return back()->with('msgx', ['Oops! Something went wrong. Try again later.']);
    }

}

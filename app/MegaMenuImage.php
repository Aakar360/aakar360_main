<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class MegaMenuImage extends Model
{

    protected $table = 'mega_menu_image';

}

<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class LeadCallLog extends Model
{

    protected $table = 'lead_call_logs';

}

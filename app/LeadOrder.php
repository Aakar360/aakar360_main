<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadOrder extends Model
{
    protected $table = 'lead_orders';
}

<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class DispatchPrograms extends Model
{

    protected $table = 'dispatch_programs';

}

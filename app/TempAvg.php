<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class TempAvg extends Model
{
    protected $table = 'temp_avg';
    protected $fillable = ['customer_id', 'median', 'average'];
}

<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class PipelineStage extends Model
{

    protected $table = 'pipeline_stages';

}

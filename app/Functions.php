<?php

/*
* 	Send mail with custom templates
* 	$template : E-mail template.
* 	$array : Variables for email template.
* 	$subject : E-mail Subject.
* 	$to : E-mail receiver.
*/

function mailing($template,$array,$subject,$to) {
    $cfg = DB::table('config')->where('id', '1')->first();
    $array['url'] = url('');
    $array['name'] = $cfg->name;
    $array['address'] = nl2br($cfg->address);
    $array['phone'] = $cfg->phone;
    // Get the template from the database
    $template = DB::table("templates")->where('code', $template)->first();
    $message = '';
    if($template !== null){
        $message = $template->template;
    }
    foreach ($array as $ind => $val) {
        $message = str_replace("{{$ind}}",$val,$message);
    }
    $message = preg_replace('/\{\{(.*?)\}\}/is','',$message);
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: '.$cfg->name.' <'.$cfg->email.'>'."\r\n";
    mail($to,$subject,$message,$headers);
    return true;
}
/*
* 	Generate url
* 	$str : The title.
* 	$id : Item ID.
*/
function path($str,$id = false) {
    $path = preg_replace("/[^a-zA-Z0-9\_|+ -]/", '', $str);
    $path = strtolower(trim($path, '-'));
    $path = preg_replace("/[\_|+ -]+/", '-', $path);
    if($id != false){
        $path = $id.'-'.$path;
    }
    return $path;
}

function getProduct($cid, $except = 0){
    $products = array();
    $products = DB::table('products')->where('category', $cid)->get()->toArray();
    if(count($products)>0){
        return $products[0];
    }else{
        return null;
    }
}
function getProducts($pids){
    $products = array();
    $products = DB::table('products')->whereIn('id',explode(',', $pids))->orderBy('title', 'ASC')->get();
    return $products;
}
function getQuestion($qid){
    $question = DB::table("questions")->where('id', $qid)->first();
    if($question !== null){
        return $question->question;
    }else{
        return null;
    }

}
function getQuestionType($qid){
    $question = DB::select("select * from questions where id =".$qid);
    if($question){
        return $question[0]->type;
    }else{
        return null;
    }
}
function getQuestionOptions($qid){
    return DB::select("select * from questions where id =".$qid)[0]->options;
}
function getServiceName($sid){
    return DB::select("select * from services where id =".$sid)[0]->title;
}
function getServiceCategoryName($sid){
    return DB::select("select * from services_category where id =".$sid)[0]->name;
}
function getAdvicesCategories($parent = 0){
    return DB::select('SELECT * FROM advices_category WHERE parent = '.$parent.' ORDER BY name ASC');
}
function getadvicesCategoryName($id){
    return DB::select('SELECT * FROM advices_category WHERE id = '.$id)[0]->name;
}
function getRelatedAdvices($cat_id, $adv_id){
    return DB::select('SELECT * FROM blog WHERE category = '.$cat_id.' AND id<>'.$adv_id.' ORDER BY title ASC LIMIT 10');
}
function getBalanceAmount($id){
    return DB::select('SELECT * FROM multiseller_payments WHERE order_id = '.$id.' ORDER BY id DESC')[0]->balance_amount;
}
function getCategoryName($cid){
    $category = array();
    $category = DB::select('SELECT * FROM category where id = '.$cid);
    if(count($category)>0){
        return $category[0]->name;
    }else{
        return null;
    }
}
function getCategoryAddPrice($cid, $uid = false){
    if($uid){
        $cat = false;
        do{
            $cate = DB::table('category')->where('id', $cid)->first();
            if($cate->parent == 0){
                $cat = true;
            }else{
                $cid = $cate->parent;
            }
        }while(!$cat);
        $user = DB::table('crm_customers')->where('id', $uid)->first();
        if($user->strict_mode){
            $cats = (array)json_decode($user->price);
            //dd($cid);
            if(isset($cats[$cate->id])){
                return $cats[$cate->id];
            }else{
                $cat = DB::table('customer_category_price')->where('customer_category', $user->customer_category)->where('product_category', $cate->id)->first();
                if($cat === null){
                    return 0;
                }else{
                    return $cat->price;
                }
            }
        }else{
            $cat = DB::table('customer_category_price')->where('customer_category', $user->customer_category)->where('product_category', $cate->id)->first();
            if($cat === null){
                $cats = (array)json_decode($user->price);
                if(isset($cats[$cate->id]))
                    return $cats[$cate->id];
                else
                    return 0;
            }else{
                return $cat->price;
            }
        }
    }else {
        return 0;
    }
}
function getPlanCategory($cid){
    $category = array();
    $category = DB::select('SELECT * FROM design_category where id = '.$cid);
    if(count($category)>0){
        return $category[0]->name;
    }else{
        return null;
    }
}
function getPlanPhotsCount($id){
    $photos = array();
    $photos = DB::select('SELECT * FROM photos where gallery_id = '.$id);
    return count($photos);
}
/*
* 	Get image by order
* 	$string : The string.
* 	$order : Image order.
*/
function image_order($string, $order = 0){
    return explode(',',$string)[$order];
}
/*
* 	Smart string cut
* 	$text : The string.
* 	$length : Length of the output.
* 	$end : String to be appended.
*/
function string_cut($text, $length = 100,$end = ''){
    mb_strlen($text);
    if (mb_strlen($text) > $length){
        $text = mb_substr($text, 0, $length);
        return $text.$end;
    } else {
        return $text;
    }
}
/*
*   Get user operation system
*/
function getOS() {
    global $_SERVER;
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $os_platform    =   "Unknown OS Platform";
    $os_array       =   array(
        '/windows nt 10/i'     =>  'Windows 10',
        '/windows nt 6.3/i'     =>  'Windows 8.1',
        '/windows nt 6.2/i'     =>  'Windows 8',
        '/windows nt 6.1/i'     =>  'Windows 7',
        '/windows nt 6.0/i'     =>  'Windows Vista',
        '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
        '/windows nt 5.1/i'     =>  'Windows XP',
        '/windows xp/i'         =>  'Windows XP',
        '/windows nt 5.0/i'     =>  'Windows 2000',
        '/windows me/i'         =>  'Windows ME',
        '/win98/i'              =>  'Windows 98',
        '/win95/i'              =>  'Windows 95',
        '/win16/i'              =>  'Windows 3.11',
        '/macintosh|mac os x/i' =>  'Mac OS X',
        '/mac_powerpc/i'        =>  'Mac OS 9',
        '/linux/i'              =>  'Linux',
        '/ubuntu/i'             =>  'Ubuntu',
        '/iphone/i'             =>  'iPhone',
        '/ipod/i'               =>  'iPod',
        '/ipad/i'               =>  'iPad',
        '/android/i'            =>  'Android',
        '/blackberry/i'         =>  'BlackBerry',
        '/webos/i'              =>  'Mobile'
    );
    foreach ($os_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }
    }
    return $os_platform;
}
/*
*   Get user browser
*/
function getBrowser() {
    global $_SERVER;
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $browser		=   "Unknown Browser";
    $browser_array  =   array(
        '/msie/i'	   =>  'Internet Explorer',
        '/firefox/i'	=>  'Firefox',
        '/safari/i'	 =>  'Safari',
        '/chrome/i'	 =>  'Chrome',
        '/edge/i'	   =>  'Edge',
        '/opera/i'	  =>  'Opera',
        '/netscape/i'   =>  'Netscape',
        '/maxthon/i'	=>  'Maxthon',
        '/konqueror/i'  =>  'Konqueror',
        '/mobile/i'	 =>  'Handheld Browser'
    );
    foreach ($browser_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $browser	=   $value;
        }
    }
    return $browser;
}
/*
*   Get user country
*/
function getCountry() {
    $country = false;
    if (!isset($_COOKIE['country'])) {
        //get country from api
        $ch = curl_init('http://api.ipstack.com/?access_key=b5b16c061364ece46e07e01e1f47ff4c');
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $json = '';
        if (($json = curl_exec($ch)) !== false) {
            // return country code
            $jdata = json_decode($json,true);
            if($jdata != null){
                $country = $jdata['country_code'];
                // Save for one month
                setcookie("country",$country,time()+2592000);
            }

        }
        else
        {
            // return false if api failed
            $country = false;
        }
        curl_close($ch);
    }
    else
    {
        $country = $_COOKIE['country'];
    }
    return $country;
}
/*
*   Get referrer
*/
function getReferrer() {
    global $_SERVER;
    $user_referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER']: '';
    $referrer = implode(array_slice(explode('/', preg_replace('/https?:\/\/(www\.)?/', '', $user_referrer)), 0, 1));
    return $referrer;
}
/*
* 	Time difference
* 	$old : The compared time.
* 	$level : Time level.
*/
function timegap($old,$level = 0) {
    $time = time();
    $dif = $time-$old;
    $names = array('second','minute','hour','day','week','month','year','decade');
    $length = array(1,60,3600,86400,604800,2630880,31570560,315705600);
    for($v = sizeof($length)-1; ($v >= 0)&&(($no = $dif/$length[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $time-($dif%$length[$v]);
    $no = floor($no); if($no <> 1) $names[$v] .='s'; $gap=sprintf("%d %s ",$no,$names[$v]);
    if(($level > 0)&&($v >= 1)&&(($time-$_tm) > 0)) $gap .= ' and '.timegap($_tm,--$level);
    return $gap;
}
/*
* 	Percentage change
* 	$old : The old figure.
* 	$new : The new figure.
*/
function p($old, $new) {
    if (($old != 0) && ($new != 0)) {
        $percentChange = (1 - $old / $new) * 100;
    }
    else {
        $percentChange = 0;
    }
    return number_format($percentChange);
}
/*
* 	Currency
* 	$dollar_price : price.
*/
function c($dollar_price, $decimal = 2, $admin = false) {
    if($dollar_price == -1){
        return '<i class=\"fa fa-rupee-sign\"></i>';
    }
    if(customer('user_type') === '' && !$admin) {
        $price = (!empty($dollar_price) ? $dollar_price : number_format('0.00', $decimal, '.', ''));
        return '<i class=\"fa fa-rupee-sign\"></i>'.number_format(round($price), $decimal, '.', '');
//        return "<span>Signin for Pri</span>";
    }
    if(customer('rfr') == 1 && !$admin) {
        return "<button class='btn btn-primary btn-sm request-rate'>Request For Rate</button>";
    }
    if(!is_numeric($dollar_price)){
        return $dollar_price;
    }
    if (!isset($_COOKIE['currency'])) {
        // Use default currency
        $currency = DB::select('SELECT code FROM currency ORDER BY `default` DESC LIMIT 1')[0]->code;
    } else {
        // Use user currency
        $currency = $_COOKIE['currency'];
    }
    $check = DB::select("SELECT COUNT(*) as count FROM currency WHERE code = '".escape($currency)."'")[0];
    if ($check->count > 0) {
        $rate = DB::select("SELECT rate FROM currency WHERE code = '".escape($currency)."' LIMIT 1")[0]->rate;
        $price = (!empty($dollar_price) ? $dollar_price*$rate : number_format('0.00', $decimal, '.', ''));
        return $currency.number_format(round($price), $decimal, '.', '');
    } else {
        $price = (!empty($dollar_price) ? $dollar_price : number_format('0.00', $decimal, '.', ''));
        return '<i class=\"fa fa-rupee-sign\"></i>'.number_format(round($price), $decimal, '.', '');
    }
}
/*
* 	Returns country name .
* 	$iso : country iso code.
*/
function country($iso) {
    $country = DB::select("SELECT nicename FROM country WHERE iso = '$iso' LIMIT 1")[0]->nicename;
    return $country;
}
function getAllCountries(){
    $countries = DB::select("SELECT * FROM countries ORDER BY name ASC");
    return $countries;
}
function getStates($cid){
    $states = DB::select("SELECT * FROM states WHERE country_id='".$cid."' ORDER BY name ASC");
    return $states;
}
function getCities($sid){
    $cities = \App\City::where('state_id',$sid)->where('is_available','1')->orderBy('name','ASC')->get();
    return $cities;
}
/*
* 	Order status
* 	$i : Status id.
*/
function status($i) {
    switch ($i){
        case 1:
            $s = "Pending";
            break;
        case 2:
            $s = "Shipped";
            break;
        case 3:
            $s = "Delivered";
            break;
        case 4:
            $s = "Cancelled";
        case 5:
            $s = "Processing";
        case 6:
            $s = "Viewed";
        case 7:
            $s = "Hold";
        case 8:
            $s = "Completed";
            break;
    }
    return $s;
}
/*
* 	PDO Escape
* 	$word : The word to be escaped.
*/
function escape($word){
    return substr(DB::connection()->getPdo()->quote($word),1,-1);
}
/*
* 	Translation
* 	$word : The word to translate.
*/
function translate($word){
    $cfg = DB::select('SELECT * FROM config WHERE id = 1')[0];
    // Set frontend language
    if (!isset($_COOKIE['lang'])) {
        // Use default language
        $lang = $cfg->lang;
    } else {
        // Use user language
        $lang = $_COOKIE['lang'];
    }
    if ($cfg->translations == 0)
    {
        // Desactivate translation
        return $word;
    }
    else
    {
        $word = escape($word);
        // Fetching for translation
        $wordc =  DB::select("SELECT COUNT(*) as count FROM translate WHERE word = '".$word."' AND lang = '".$lang."'")[0];
        if ($wordc->count > 0)
        {
            // Return translation
            $translation = DB::select("SELECT translation FROM translate WHERE word = '".$word."' AND lang = '".$lang."'")[0];
            return $translation->translation;
        }
        else
        {
            // Add translation to database and return word
            DB::insert("INSERT INTO translate (lang,word,translation) VALUE ('".$lang."','".$word."','".$word."')");
            return $word;
        }
    }
}
/*
* 	Customer
* 	$info : The information to be retrieved.
*/
function customer($info){
    if (session('customer') == '') {
        return '';
    }
    $customer = \App\Customer::where('sid',session('customer'))->get();
    if(count($customer)>0){
        $cust = $customer[0];
        if (isset($cust->$info)) {
            return $cust->$info;
        } else {
            return '';
        }
    }else{
        return '';
    }
}

function institutional($info){
    if (session('institutional') == '') {
        return '';
    }
    $customer = \App\Customer::where('sid',session('institutional'))->get();
    if(count($customer)>0){
        $cust = $customer[0];
        if (isset($cust->$info)) {
            return $cust->$info;
        } else {
            return '';
        }
    }else{
        return '';
    }
}

function getCityName($cid){
    if($cid){
        $city = DB::table('cities')->where('id', $cid)->first();
        if($city === null){
            return $cid;
        }
        return $city->name;
    }else {
        return '';
    }
}
function getStateName($cid){
    if($cid){
        $state = DB::table('states')->where('id', $cid)->first();
        if($state === null){
            return $cid;
        }
        return $state->name;
    }else {
        return 'NA';
    }
}
function getDistrictName($cid){
    if($cid){
        $state = DB::table('district')->where('id', $cid)->first();
        if($state === null){
            return $cid;
        }
        return $state->name;
    }else {
        return 'NA';
    }
}
function getLocalityName($cid){
    if($cid){
        $state = DB::table('locality')->where('id', $cid)->first();
        if($state === null){
            return $cid;
        }
        return $state->locality;
    }else {
        return 'NA';
    }
}
function getCountryName($cid){
    if($cid){
        $country = DB::table('countries')->where('id', $cid)->first();
        if($country === null){
            return $cid;
        }
        return $country->name;
    }else {
        return '';
    }
}

function getProductCategories($parent = 0){
    return DB::select('SELECT * FROM category WHERE parent = '.$parent.' ORDER BY name ASC');
}
function getDesignCategories($parent = 0){
    return DB::select('SELECT * FROM design_category WHERE parent = '.$parent.' ORDER BY name ASC');
}
function getPCategories($count = 0){
    if($count !=0)
        return DB::select('select * from category Where popular = 1 and is_view = 0 ORDER BY popular_priority ASC LIMIT '.$count);
    else
        return DB::select('select * from category Where popular = 1 and is_view = 0 ORDER BY popular_priority ASC');
}
function getSCatDisplay($count = 8){
    return DB::select('select * from services_category Where display = 1 ORDER BY name LIMIT '.$count);
}
function getSCatList($count = 8){
    return DB::select('select * from services_category Where list = 1 ORDER BY name LIMIT '.$count);
}
function getServiceCategories($parent = 0){
    return DB::select('SELECT * FROM services_category WHERE parent = '.$parent.' ORDER BY name ASC');
}
function getRelatedProducts($cat_id, $pro_id){
    return DB::table('products')->where('category', '=', $cat_id)->where('id', '<>', $pro_id)->orderBy('title', 'ASC')->skip(0)->take(10)->get();
}
function getRelatedProductsWithHub($cat_id, $pro_id, $hub_id){
    $product_ids = array_pluck(DB::table('products')->where('category', '=', $cat_id)->where('id', '<>', $pro_id)->orderBy('title', 'ASC')->get(), 'id');
    $products = DB::table('products');
    if (isset($hub_id)) {
        $ph_products = array();
        $ph_relations = DB::table('ph_relations')->get();
        foreach($ph_relations as $ph){
            $hub_ids = json_decode($ph->hubs);
            foreach($hub_ids as $hub){
                if($hub->hub == $hub_id){
                    $ph_products[] = $ph->products;
                }
            }
        }
        $products = $products->whereIn('id', $ph_products);
    }
    $products = $products->where('category', '=', $cat_id)->where('id', '<>', $pro_id)->orderBy('title', 'ASC')->get();
    return $products;
}
function getfeaturedDesign($id = 1){
    return DB::select('select * from home_page_design where id='.$id)[0];
}
function getLink($id = 1){
    $design = DB::select('select * from home_page_design where id='.$id);
    if(count($design) > 0){
        $design = DB::select('select * from home_page_design where id='.$id)[0];
    }else{
        return '';
    }
    $link = DB::select("select * from design_category where slug='$design->link'");
    if(count($link) > 0 ){
        $link = DB::select("select * from design_category where slug='$design->link'")[0];
    }else{
        return '';
    }
    $ret = '';
    if($link->layout != null){
        $ret = $link->layout;
    }else{
        $link = DB::select("select * from design_category where id='$link->parent'");
        if(count($link) > 0){
            $link = DB::select("select * from design_category where id='$link->parent'")[0];
        }else{
            return '';
        }
        $ret = $link->layout;
    }
    return url($ret.'/'.$design->link);
}
function getRatings($pid){
    $total_ratings = DB::select("SELECT COUNT(*) as count FROM reviews WHERE active = 1 AND product = ".$pid)[0]->count;
    $rating = 0;
    if ($total_ratings > 0){
        $rating_summ = DB::select("SELECT SUM(rating) as sum FROM reviews WHERE active = 1 AND product = ".$pid)[0]->sum;
        $rating = $rating_summ / $total_ratings;
    }
    $data['total_ratings'] = $total_ratings;
    $data['rating'] = $rating;
    return $data;
}

function makeSlugOf($title, $table, $hash = FALSE){
    $slug = str_slug($title);
    if(!$slug || $hash)
        $slug .=  ($hash) ? '-'.getHashCode() : getHashCode();
    $count = DB::table($table)->whereRaw("slug LIKE '%'")->count();
    return $count ? "{$slug}-{$count}" : $slug;
}
function getTimelineData($table, $id){
    return DB::table($table)->find($id);
}
function getCategoryNames($ids){
    $pids = explode(',', $ids);
    $pnames = array_pluck(DB::table('category')->whereIn('id', $pids)->get(), 'name');
    return implode(', ', $pnames);
}
function getBrandNames($ids){
    $pids = explode(',', $ids);
    $pnames = array_pluck(DB::table('brand')->whereIn('id', $pids)->get(), 'name');
    return implode(', ', $pnames);
}
function getProductName($id){
    $p = DB::table('products')->where('id', $id)->first();
    if($p === null){
        return '';
    }else{
        return $p->title;
    }
}
function getVariantName($id){
    $ids = explode('-', $id);
    if($ids[0] == 'product'){
        $p = DB::table('products')->where('id', $ids[1])->first();
        if($p === null){
            return 'NA';
        }else{
            return $p->title;
        }
    }else if($ids[0] == 'variant'){
        $p = DB::table('product_variants')->where('id', $ids[1])->first();
        if($p === null){
            return 'NA';
        }else{
            return $p->variant_title;
        }
    }else{
        $p = DB::table('product_variants')->where('id', $ids[0])->first();
        if($p === null){
            return 'NA';
        }else{
            return $p->variant_title;
        }
    }

}

function getVariant($id){
    $p = DB::table('product_variants')->where('id', $id)->first();
    if($p === null){
        return 'NA';
    }else{
        $size = $p->variant_title;
        $price = DB::table('size')->where('id', $size)->first();
        if($price === null){
            return 'NA';
        }else{
            return $price->name;
        }
    }
}

function getVname($id){
    $price = DB::table('size')->where('id', $id)->first();
    if($price === null){
        return 'NA';
    }else{
        return $price->name;
    }
}

function getProName($id){
    $p = DB::table('products')->where('id', $id)->first();
    if($p === null){
        return 'NA';
    }else{
        return $p->title;
    }
}

function getPartyName($id){
    $ids = explode('-', $id);
    if($ids[0] == 'party'){
        $p = DB::table('billing_parties')->where('id', $ids[1])->first();
        if($p === null){
            return 'NA';
        }else{
            return $p->company;
        }
    }else if($ids[0] == 'customer'){
        $p = DB::table('customers')->where('id', $ids[1])->first();
        if($p === null){
            return 'NA';
        }else{
            if($p->company == ''){
                $p->name;
            }else {
                $p->company;
            }
        }
    }else{
        $p = DB::table('billing_parties')->where('id', $ids[0])->first();
        if($p === null){
            return 'NA';
        }else{
            return $p->company;
        }
    }
}
function getUnitSymbol($id){
    $p = DB::table('units')->where('id', $id)->first();
    if($p === null){
        return 'NA';
    }else{
        return $p->name;
    }
}
function getVariants($pid){
    $variants = DB::table('product_variants')->where('product_id', $pid)->get();
    $product = DB::table('products')->where('id', $pid)->first();
    $ret = array();
    if(count($variants)){
        foreach($variants as $variant){
            $ret[] = array(
                'id' => 'variant-'.$variant->id,
                'title' => $variant->variant_title
            );
        }
    }else{
        $ret[] = array(
            'id' => 'product-'.$product->id,
            'title' => $product->title
        );
    }
    return $ret;
}
function checkRole($uid, $page_code){
    $user = DB::table('user')->where('u_id', $uid)->first();
    $permissions = DB::table('crm_user_role')->where('id', $user->role_id)->first();
    $permissions = explode(",",$permissions->permissions);
    if(in_array($page_code,$permissions)){
        return true;
    }
    return false;
}

function getLastCall($user_id){
    $calls = \App\CrmCall::where('crm_customer_id', $user_id)->orderBy('select_date', 'DESC')->pluck('select_date')->first();

    $d = array();
    if($calls !== null){
        $date1 = $calls;
        $diff = date_diff(date_create($date1), date_create(date('Y-m-d')));
        $d['diff'] = $diff->format("%a");
        $d['date'] = $date1;
        $records = \App\CrmCall::where('crm_customer_id', $user_id)->pluck('select_date');
        $diff = array();
        for ($i = 0; $i < count($records) - 1; $i++) {
            $to = \Carbon\Carbon::createFromFormat('Y-m-d', $records[$i]);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d', $records[$i + 1]);
            $diff1[] = $to->diffInDays($from);
        }
        $updateCalls                =   \App\CrmCustomer::find($user_id);
        $updateCalls->call_days     =   $d['diff'];
        $updateCalls->call_last_updated     =   $d['date'];
        $updateCalls->call_average  = average($diff1);
        $updateCalls->call_median   = median($diff1);
        $updateCalls->save();
    }else{

        $updateCalls                =   \App\CrmCustomer::find($user_id);
        $updateCalls->call_days     =   0;
        $updateCalls->call_last_updated     = '';
        $updateCalls->call_average  = 0.00;
        $updateCalls->call_median   = 0.00;
        $updateCalls->save();

    }
    return $d;
}
function getTotalCall($user_id){
    $calls = DB::table('crm_call')->where('crm_customer_id', $user_id)->orderBy('select_date', 'DESC')->get();
    $updateCalls                =   \App\CrmCustomer::find($user_id);
    $updateCalls->total_calls   =   $calls->count();
    $updateCalls->save();
    return $calls->count();
}
function getLastVisit($user_id){
    $calls = DB::table('crm_visits')->where('crm_customer_id', $user_id)->orderBy('select_date', 'DESC')->first();
    $d = array();
    if($calls !== null){
        $date1 = $calls->select_date;
        $diff = date_diff(date_create($date1), date_create(date('Y-m-d')));
        $d['diff'] = $diff->format("%a days");
        $d['date'] = $date1;
    }else{
        $d['diff'] = 'Never';
        $d['date'] = '';
    }
    return $d;
}
function dateDiffInDays($date1, $date2)
{
    $diff = strtotime($date2) - strtotime($date1);
    return abs(round($diff / 86400));
}

function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
        }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}
function median(array $arr)
{
    if (0 === count($arr)) {
        return null;
    }
    // sort the data
    $count = count($arr);
    asort($arr);
    // get the mid-point keys (1 or 2 of them)
    $mid  = floor(($count - 1) / 2);
    $keys = array_slice(array_keys($arr), $mid, (1 === $count % 2 ? 1 : 2));
    $sum  = 0;
    foreach ($keys as $key) {
        $sum += $arr[$key];
    }
    $med = $sum / count($keys);
    if($med == null){
        $med = 0;
    }
    return number_format($med, 2, '.', '');
}
function average(array $arr)
{
    if(count($arr)) {
        $average = array_sum($arr) / count($arr);
        return number_format($average, 2, '.', '');
    }else{
        return count($arr);
    }
}
function getInstitutionalPrice($product_id,$customer_id, $hub_id, $var_id = false, $q = true)
{
    $ph = \App\PhRelations::where('products', $product_id)->first();
    if ($ph === null) {
        return false;
    }
    $product = \App\Products::find($product_id);
    $var = null;
    $vp = 0;
    $pp =0;
    if ($var_id) {
        $var = \App\ProductVariant::where('id', $var_id)->first();
        if ($var) {
            $price = \App\MultipleSize::where('id',$var->price)->get();
            if ($price) {
                $vp = $price[0]->price;
            }
        }
    }
    $t = ($product->tax == '' ? 18.00 : floatval(number_format((int)$product->tax, 2, '.', '')));
    $data = json_decode($ph->hubs);
    $pids = array_pluck($data, 'hub');
    $validity = array_pluck($data, 'validity', 'hub');
    $selectUtype = null;
    if($customer_id && $customer_id !== ''){
        $selectUtype = \App\Customer::where('appid', $customer_id)->orWhere('sid', $customer_id)->orWhere('id',$customer_id)->first();
    }
    $vd_in_secs = 0;
    if ($selectUtype !== null) {
        $uid = $selectUtype->id;
        if ($selectUtype->user_type !== '') {
            if ($selectUtype->show_price) {
                $key = array_search($hub_id, $pids);
                $pdata = $data[$key];
                $pp = round(floatval(isset($pdata->pprice) ? $pdata->pprice : $pdata->price));
                if($selectUtype->institutional_price !== null) {
                    $productPrice = $pdata->price + $selectUtype->institutional_price;
                }else{
                    $productPrice = $pdata->price;
                }

                $custDiscount = \App\CustomerDiscount::where('customer_id', $uid)->first();
                if($custDiscount !== null){
                    $selDis = \App\CustomerDiscount::where('customer_id', $uid)->where('category',$product->category)->first();
                    if($selDis !== null){
                        if($selDis->discount_type == 0 && $selDis->action == 0){
                            $discal = ($pdata->price*floatval($selDis->discount))/100;
                            $productPrice = ($pdata->price+$discal);
                        }elseif($selDis->discount_type == 1 && $selDis->action == 1){
                            $productPrice = ($pdata->price - floatval($selDis->discount));
                        }elseif($selDis->discount_type == 1 && $selDis->action == 0){
                            $productPrice = ($pdata->price + floatval($selDis->discount));
                        }elseif($selDis->discount_type == 0 && $selDis->action == 1){
                            $discal = ($pdata->price*floatval($selDis->discount))/100;
                            $productPrice = ($pdata->price-$discal);
                        }
                    }
                }
                if ($product->is_variable && $selectUtype->user_type == "institutional") {
                    if (count($validity)) {
                        if (isset($validity[$hub_id])){
                            date_default_timezone_set("Asia/Kolkata");
                            $vd_in_secs = strtotime($validity[$hub_id]) - strtotime(date('Y-m-d H:i:s')); //Code to convert date in secs using date difference
                        }
                    }

                    if ($vd_in_secs) {
                        if (in_array($hub_id, $pids) && $vd_in_secs > 0) {
                            $pr = 0;
                            $var_p = 0;
                            if ($var === null) {
                                $pr = $productPrice + $pdata->loading;
                            } else {
                                $var_pr = \App\MultipleSize::where('id',$var->price)->first();
                                $var_p = $var_pr->price;
                                $pr = $productPrice + $pdata->loading + $var_p;
                            }

                            if($q){
                                $quotation = \App\RequestQuotation::where('product_id', $product_id)->where('status', '=', 1)->where(function ($q) {
                                    $q->where('validity', '>=', date('Y-m-d H:i:s'));
                                })->orderBy('created_at', 'desc')->first();
                                //dd($quotation);
                                if ($quotation !== null) {
                                    $variant = (array)json_decode($quotation->variants);
                                    if($quotation->method == 'bysize'){
                                        $bids = explode(',',$quotation->brand_id);
                                        foreach ($bids as $bi) {
                                            foreach ($variant->$bi as $va) {
                                                if ($var_id == $va->id) {
                                                    $var_p = $va->var_price;
                                                    $pr = $productPrice + $pdata->loading + $var_p;
                                                }
                                            }
                                        }
                                    }else{
                                        foreach ($variant as $va) {
                                            if ($var_id == $va->id) {
                                                $var_p = $va->var_price;
                                                $pr = $productPrice + $pdata->loading + $var_p;
                                            }
                                        }
                                    }
                                }
                            }
                            $tax = ($pr * $t) / 100;
                            $total = $pr + $tax;

                            $pp1 = '0';
                            if ($var_id) {
                                return array('timer' => $vd_in_secs, 'pprice' => $pp, 'price' => round($productPrice), 'loading' => round($pdata->loading), 'tax' => round($tax), 'tax_value' => round($t), 'var' => round($var_p), 'total' => round($total));
                            }
                            //Check
                            return array('timer' => $vd_in_secs, 'pprice' => $pp, 'price' => round($productPrice), 'loading' => round($pdata->loading), 'tax' => round($tax), 'tax_value' => round($t), 'total' => round($total));
                        } else {
                            return array('timer' => $vd_in_secs, 'pprice' => 'waiting for update', 'price' => 'waiting for update', 'loading' => 0, 'tax' => 0, 'var' => 0, 'total' => 'waiting for update');
                        }
                    } else {
                        return array('timer' => 0, 'pprice' => 'waiting for update', 'price' => 'waiting for update', 'loading' => 0, 'tax' => 0, 'var' => 0, 'total' => 'waiting for update');
                    }
                } else {
                    $pp4 = '0';
                    $total_retail = $productPrice + $pdata->loading;
                    if ($var_id) {
                        $var = DB::table('product_variants')->where('id', $var_id)->first();

                        if ($var !== null) {
                            $price = \App\MultipleSize::where('id',$var->price)->first();
                            if ($price) {
                                $pp4 = $price->price;
                                $total_retail = $total_retail + $pp4;
                                $tax = ($total_retail*$t)/100;
                                $total_retail += $tax;
                                return array('timer' => 0, 'pprice' => $pp, 'price' => round($productPrice), 'loading' => round($pdata->loading), 'tax' => round($tax), 'tax_value' => round($t), 'total' => round($total_retail), 'var' => round($pp4));
                            }else{
                                $tax = ($total_retail*$t)/100;
                                $total_retail += $tax;
                                return array('timer' => 0, 'pprice' => $pp, 'price' => round($productPrice), 'loading' => round($pdata->loading), 'tax' => round($tax), 'tax_value' => round($t), 'total' => round($total_retail), 'var' => 0);
                            }
                        }else{
                            $tax = ($total_retail*$t)/100;
                            $total_retail += $tax;
                            return array('timer' => 0, 'pprice' => $pp, 'price' => round($productPrice), 'loading' => round($pdata->loading), 'tax' => round($tax), 'tax_value' => round($t), 'total' => round($total_retail));
                        }
                    }else{
                        $tax = ($total_retail*$t)/100;
                        $total_retail += $tax;
                        return array('timer' => 0, 'pprice' => $pp, 'price' => round($productPrice), 'loading' => round($pdata->loading), 'tax' => round($tax), 'tax_value' => round($t), 'total' => round($total_retail));
                    }
                }
            } else {
                return array('pprice'=>'Request for Quote', 'price' => 'Request for Quote', 'loading' => 0, 'tax' => 0, 'var' => 0, 'total' => 'Request for Quote', 'size_diffrence' => 'Request for Quote');
            }
        }
    } else {
        return array('pprice' => 'login to view price', 'price' => 'login to view price', 'loading' => 0, 'tax' => 0, 'var' => 0, 'total' => 'login to view price', 'size_diffrence' => 'login to view price');
    }
}
function getInstitutionalPriceFancy($product_id,$customer_id,$hub_id, $var_id = false, $q = true){
    $ph = \App\PhRelations::where('products', $product_id)->first();
    if($ph === null){
        return 'NA';
    }
    $product = \App\Products::find($product_id);
    $var = null;
    $pp = 0;
    if($var_id){
        $var = DB::table('product_variants')->where('id', $var_id)->first();
        if($var !== null){
            $price = DB::select("SELECT * FROM multiple_size WHERE id = $var->price");
            //dd($price);

            if($price){
                $pp = $price[0]->price;
            }
        }
    }
    $selectUtype = \App\Customer::where('appid', $customer_id)->orWhere('sid',$customer_id)->first();
    $t = ($product->tax == '' ? 18 : (int)$product->tax);
    $pretail = 0;
    if($var === null) {
        $pretail =  $product->price + $product->loading;
    }else{
        $pretail = $pp + $product->loading + $var->price;
    }
    $tax_retail = ($pretail*(int)$t)/100;
    $total_retail = $pretail + $tax_retail;
    $data = json_decode($ph->hubs);
    $pids = array_pluck($data, 'hub');
    $validity = array_pluck($data, 'validity', 'hub');
    $vd_in_secs = 0;
    if($selectUtype!== null){
        if($selectUtype['user_type'] !== '') {
            if($selectUtype['show_price']){
                if($product->is_variable && $selectUtype['user_type']=="institutional"){
                    if(count($validity)){
                        if(isset($validity[$hub_id]))
                            $vd_in_secs = strtotime($validity[$hub_id]) - strtotime(date('Y-m-d H:i:s')); //Code to convert date in secs using date difference
                    }
                    if($vd_in_secs) {

                        if (in_array($hub_id, $pids) && $vd_in_secs > 0) {
                            $key = array_search($hub_id, $pids);
                            $pdata = $data[$key];
                            $pr = 0;
                            $var_p = 0;
                            if ($var === null) {
                                $pr = $pdata->price + $pdata->loading;
                            } else {
                                $pr = $pdata->price + $pdata->loading + $var->price;
                                $var_p = $var->price;
                            }
                            if($q){
                                $quotation = DB::table('request_quotation')->where('product_id', $product_id)->where('status', '=', 1)->where(function ($q) {
                                    $q->where('validity', '>=', date('Y-m-d H:i:s'));
                                })->orderBy('created_at', 'desc')->first();
                                //dd($quotation);
                                if ($quotation !== null) {
                                    $variant = (array)json_decode($quotation->variants);
                                    if($quotation->method == 'bysize'){
                                        $bids = explode(',',$quotation->brand_id);
                                        foreach ($bids as $bi) {
                                            foreach ($variant->$bi as $va) {
                                                if ($var_id == $va->id) {
                                                    $var_p = $va->var_price;
                                                    $pr = $pdata->price + $pdata->loading + $var_p;
                                                }
                                            }
                                        }
                                    }else{
                                        foreach ($variant as $va) {
                                            if ($var_id == $va->id) {
                                                $var_p = $va->var_price;
                                                $pr = $pdata->price + $pdata->loading + $var_p;
                                            }
                                        }
                                    }
                                }
                            }
                            //get price and action(+/-) from rfq
                            $price = $pdata->price;
                            $timer = $vd_in_secs;
                            $loading = $pdata->loading;
                            $tax = ($pr * $t) / 100;
                            $total = $pr + $tax;
                            return round($total);
                        } else {
                            return  'waiting for update';
                        }
                    }
                    else {
                        return  'waiting for update';
                    }
                }
                else{
                    return round($pretail);
                }
            }
            else{
                return 'login to view price';
            }
        }
    }
    else{
        return 'Login to view price';
    }
}

function getPriceFancy($product_id,$customer_id,$hub_id, $var_id = false){
    $ph = \App\PhRelations::where('products', $product_id)->first();
    if($ph === null){
        return 'NA';
    }
    $product = \App\Products::find($product_id);
    $var = null;
    if($var_id){
        $var = DB::table('product_variants')->where('id', $var_id)->first();
    }
    $selectUtype = \App\Customer::where('appid', $customer_id)->orWhere('sid',$customer_id)->first();
//    dd($selectUtype);
    $t = ($product->tax == '' ? 18 : $product->tax);
    $pretail = 0;
    if($var === null) {
        $pretail = $product->price + $product->loading;
    }else{
        $pretail = $product->price + $product->loading + $var->price;
    }
    $tax_retail = ($pretail*$t)/100;
    $total_retail = $pretail + $tax_retail;
    $data = json_decode($ph->hubs);
    $pids = array_pluck($data, 'hub');
    $validity = array_pluck($data, 'validity', 'hub');
    $vd_in_secs = 0;
    if($selectUtype!== null){
        if($selectUtype['user_type'] !== '') {
            if($selectUtype['show_price']){
                if($product->is_variable && $selectUtype['user_type']=="institutional"){
                    if(count($validity)){
                        if(isset($validity[$hub_id]))
                            $vd_in_secs = strtotime($validity[$hub_id]) - strtotime(date('Y-m-d H:i:s')); //Code to convert date in secs using date difference
                    }
                    if($vd_in_secs) {

                        if (in_array($hub_id, $pids) && $vd_in_secs > 0) {
                            $key = array_search($hub_id, $pids);
                            $pdata = $data[$key];
                            $pr = 0;
                            $var_p = 0;
                            if ($var === null) {
                                $pr = $pdata->price + $pdata->loading;
                            } else {
                                $pr = $pdata->price + $pdata->loading + $var->price;
                                $var_p = $var->price;
                            }
                            $quotation = DB::table('request_quotation')->where('product_id', $product_id)->where('status', '=', 1)->where(function ($q) {
                                $q->where('validity', '>=', date('Y-m-d H:i:s'));
                            })->orderBy('created_at', 'desc')->first();
                            if($quotation !==null){
                                $variant = (array)json_decode($quotation->variants);
                                foreach($variant as $va){
                                    if($var_id == $va->v_id){
                                        $var_p = $va->price;
                                        $pr = $pdata->price + $pdata->loading + $var_p;
                                    }
                                }
                            }
                            //get price and action(+/-) from rfq
                            $price = $pdata->price;
                            $timer = $vd_in_secs;
                            $loading = $pdata->loading;
                            $tax = ($pr * $t) / 100;
                            $total = $pr + $tax;
                            return round($total);
                        } else {
                            return  'waiting for update';
                        }
                    }
                    else {
                        return  'waiting for update';
                    }
                }
                else{
                    return round($pretail);
                }
            }
            else{
                return 'login to view price';
            }
        }
    }
    else{
        return 'Login to view price';
    }
}


function checkHubLinks($hub_id){
    $phs = \App\PhRelations::all();
    $ret = false;
    foreach($phs as $ph){
        $hubs = array();
        $hubs = array_pluck(json_decode($ph->hubs), 'hub');
        if(count($hubs)){
            $ret = true;
            break;
        }
    }
    return $ret;
}
function getCustomerData($user_id){
    //dd(trim($user_id));
    $customer = \App\Customer::find($user_id);
    if($customer){
        return 'Name: '.$customer->name.'<br>Email: '.$customer->email.'<br>Mobile: '.$customer->mobile.'<br>City: '.getCityName($customer->city);
    }
}

function getCreditCharge($user_id){
    $customer = DB::table('credit_charges')->where('id', $user_id)->first();
    if($customer){
        return 'Days: '.$customer->days.'<br>Amount: '.$customer->amount.' Rs.';
    }
}

function getIndividual($product_id,$var_id = false){
    $product = \App\Products::find($product_id);
    $var = null;
    $pp = 0;
    if($var_id){
        $var = DB::table('product_variants')->where('id', $var_id)->first();
        if($var) {
            $price = DB::select("SELECT * FROM multiple_size WHERE id = $var->price");
            if ($price) {
                $pp = $price[0]->price;
            }
        }
    }

    $t = ($product->tax == '' ? 18.00 : number_format(floatval($product->tax), 2, '.', ''));
    //$t = '18';
    //dd($t);
    $pr = 0;
    $pr_sale = 0;
    $var_p = 0;
    if($var === null) {
        $pr = $product->price + $product->loading;
        $pr_sale = $product->sale + $product->loading;
    }else{
        $pr = $product->price + $product->loading + $pp;
        $pr_sale = $product->sale + $product->loading + $pp;
        $var_p = $pp;
    }
    $tax = ($pr*$t)/100;
    $tax_sale = ($pr_sale*$t)/100;
    $total = $pr + $tax;
    $total_sale = $pr_sale + $tax_sale;
    return array('price'=>round($product->price), 'loading'=>round($product->loading), 'tax'=>round($product->tax),'var_price'=>round($var_p),'total'=>round($total),'total_sale'=>round($total_sale));
}
function crmCallMedian(){
    $where = '';
    $wherex = '';
    $results = \App\CrmCustomer::orderBy('id', 'ASC')->get();

    foreach ($results as $customer){
        $cid = $customer->id;
        if($wherex !== '') {
            $dates1 = array_pluck(DB::table('crm_call')->where('crm_customer_id', '=', $cid)->whereRaw($wherex)->orderBy('select_date', 'ASC')->get(), 'select_date');
        }else{
            $dates1 = array_pluck(DB::table('crm_call')->where('crm_customer_id', '=', $cid)->orderBy('select_date', 'ASC')->get(), 'select_date');
        }
        $dates2 = $dates1;
        $diff_array = array();
        for($i=0;$i<(count($dates1)-1);$i++){
            $diff_array[] = dateDiffInDays($dates1[$i], $dates2[$i+1]);
        }
        if(count($diff_array) == 0 && count($dates1) > 0){
            $diff_array[] = dateDiffInDays($dates1[0], date('Y-m-d h:i:s'));
        }else if(count($diff_array) == 0 && count($dates1) == 0){
            $c = \App\CrmCustomer::find($cid);
            if($c !== null) {
                $diff_array[] = dateDiffInDays($c->created_at, date('Y-m-d h:i:s'));
            }
        }
        $med = median($diff_array);
        $avg = average($diff_array);

        $data[] = array(
            'customer_id' => $cid,
            'median' => $med,
            'average' => $avg
        );
        $temp = \App\TempAvg::firstOrNew(array('customer_id' => $cid));
        $temp->median = $med;
        $temp->average = $avg;
        $temp->save();
    }
    function crmCallMedianByCustomer($cid){
        $dates1 = array_pluck(DB::table('crm_call')->where('crm_customer_id', '=', $cid)->orderBy('select_date', 'ASC')->get(), 'select_date');
        $dates2 = $dates1;
        $diff_array = array();
        for($i=0;$i<(count($dates1)-1);$i++){
            $diff_array[] = dateDiffInDays($dates1[$i], $dates2[$i+1]);
        }
        if(count($diff_array) == 0 && count($dates1) > 0){
            $diff_array[] = dateDiffInDays($dates1[0], date('Y-m-d h:i:s'));
        }else if(count($diff_array) == 0 && count($dates1) == 0){
            $c = \App\CrmCustomer::find($cid);
            if($c !== null) {
                $diff_array[] = dateDiffInDays($c->created_at, date('Y-m-d h:i:s'));
            }
        }
        $med = median($diff_array);
        $avg = average($diff_array);

        $data[] = array(
            'customer_id' => $cid,
            'median' => $med,
            'average' => $avg
        );
        $temp = \App\TempAvg::firstOrNew(array('customer_id' => $cid));
        $temp->median = $med;
        $temp->average = $avg;
        $temp->save();
    }


}

function getSize($id){
    if($id !== null) {
        $variants = DB::select("SELECT * FROM multiple_size where id=$id");
        if ($variants) {
            return $variants[0]->price;
        } else {
            return 0;
        }
    }else{
        return 0;
    }
}

function variantTitle($id){
    if($id !== null) {
        $variants = DB::select("SELECT * FROM size where id=$id");
        if($variants) {
            return $variants[0]->name;
        }else{
            return '';
        }
    }else{
        return '';
    }
}

function variantTitleWeight($id){
    if($id !== null) {
        $variants = DB::select("SELECT * FROM size where id=$id");
        if($variants) {
            return $variants[0]->wunit;
        }else{
            return '';
        }
    }else{
        return '';
    }
}


function getLoadingByHub($pid, $hub, $cust_type)
{
    if ($cust_type == 'individual') {
        return 175;
    } elseif ($cust_type == 'institutional') {
        $ph = \App\PhRelations::where('products', $pid)->first();
        if ($ph === null) {
            return 0;
        }
        $data = json_decode($ph->hubs);
        $pids = array_pluck($data, 'hub');
        /*$validity = array_pluck($data, 'validity', 'hub');
        $vd_in_secs = 0;
        if (count($validity)) {
            if (isset($validity[$hub]))
                $vd_in_secs = strtotime($validity[$hub]) - strtotime(date('Y-m-d H:i:s')); //Code to convert date in secs using date difference
        }
        if ($vd_in_secs) {*/

        if (in_array($hub, $pids)) {
            $key = array_search($hub, $pids);
            $pdata = $data[$key];
            if($pdata->loading == null){
                return 0;
            }
            return $pdata->loading;
        }
        return 0;
        /*}
        return 0;*/
    }
    return 0;
}
function getLastUpdatedDate($section){
    $date = null;
    if($section == 'calls'){
        $customer = \App\CrmCustomer::orderBy('call_last_updated', 'desc')->first();
        $date = \Carbon\Carbon::parse($customer->call_last_updated);
    }else{
        $customer = \App\CrmCustomer::orderBy('visit_last_updated', 'desc')->first();
        $date = \Carbon\Carbon::parse($customer->visit_last_updated);
    }
    if($date === null){
        return 'Never';
    }
    return $date->diffForHumans();
}
function closestNumber($n, $m, $floor=false)
{
    // find the quotient
    $q = (int) ($n / $m);

    // 1st possible closest number
    $n1 = $m * $q;

    // 2nd possible closest number
    $n2 = ($n * $m) > 0 ?
        ($m * ($q + 1)) : ($m * ($q - 1));

    // if true, then n1 is the
    // required closest number

    if($floor) {
        if (abs($n - $n1) < abs($n - $n2))
            return $n1;
        return $n2;
    }else{
        if (abs($n - $n1) < abs($n - $n2))
            return $n2;
        return $n1;
    }
    // else n2 is the required
    // closest number

}

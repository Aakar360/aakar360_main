<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class RequestForCall extends Model
{
    protected $table = 'request_for_call';
}

<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => 'e78dca5ac99f2397aac2deed7b546ccca88bf343',
    'name' => 'laravel/laravel',
  ),
  'versions' => 
  array (
    'ckeditor/ckeditor' => 
    array (
      'pretty_version' => '4.11.1',
      'version' => '4.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '70e6f8f01c3d7d8f713291984017a6005c4e737a',
    ),
    'cordoval/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'davedevelopment/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'defuse/php-encryption' => 
    array (
      'pretty_version' => 'v2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f407c43b953d571421e0020ba92082ed5fb7620',
    ),
    'dnoegel/php-xdg-base-dir' => 
    array (
      'pretty_version' => '0.1',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '265b8593498b997dc2d31e75b89f053b5cc9621a',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '13e3381b25847283a91948d04640543941309727',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '2.13.0',
      'version' => '2.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '67d56d3203b33db29834e6b2fcdbfdc50535d796',
    ),
    'doctrine/deprecations' => 
    array (
      'pretty_version' => 'v0.5.3',
      'version' => '0.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9504165960a1f83cc1480e2be1dd0a0478561314',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '90b2128806bfde671b6952ab8bea493942c1fdae',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e884e78f9f0eb1329e445619e04456e64d8051d',
    ),
    'dompdf/dompdf' => 
    array (
      'pretty_version' => 'v0.8.3',
      'version' => '0.8.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '75f13c700009be21a1965dc2c5b68a8708c22ba2',
    ),
    'erusev/parsedown' => 
    array (
      'pretty_version' => '1.7.1',
      'version' => '1.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '92e9c27ba0e74b8b028b111d1b6f956a15c01fc1',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v5.0.0',
      'version' => '5.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9984a4d3a32ae7673d6971ea00bae9d0a1abba0e',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'v1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f72816b43e74063c8b10357394b6bba8cb1c10de',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.3.3',
      'version' => '6.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '407b0cb880ace85c9b63c5f9551db498cb2d50ba',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59da6cf61d80060647ff4d3eb2c03a2bc694646',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.5.2',
      'version' => '1.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f83dded91781a01c63574e387eaa769be769115',
    ),
    'hamcrest/hamcrest-php' => 
    array (
      'pretty_version' => 'v1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b37020aa976fa52d3de9aa904aa2522dc518f79c',
    ),
    'illuminate/auth' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/broadcasting' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/bus' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/cache' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/config' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/console' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/container' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/cookie' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/database' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/encryption' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/events' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/exception' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/hashing' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/http' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/log' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/mail' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/notifications' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/pagination' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/pipeline' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/queue' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/redis' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/routing' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/session' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/support' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/translation' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/validation' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'illuminate/view' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'intervention/image' => 
    array (
      'pretty_version' => '2.4.2',
      'version' => '2.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e82d274f786e3d4b866a59b173f42e716f0783eb',
    ),
    'jakub-onderka/php-console-color' => 
    array (
      'pretty_version' => 'v0.2',
      'version' => '0.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd5deaecff52a0d61ccb613bb3804088da0307191',
    ),
    'jakub-onderka/php-console-highlighter' => 
    array (
      'pretty_version' => 'v0.4',
      'version' => '0.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f7a229a69d52506914b4bc61bfdb199d90c5547',
    ),
    'kodova/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'laravel/framework' => 
    array (
      'pretty_version' => 'v5.4.36',
      'version' => '5.4.36.0',
      'aliases' => 
      array (
      ),
      'reference' => '1062a22232071c3e8636487c86ec1ae75681bbf9',
    ),
    'laravel/laravel' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => 'e78dca5ac99f2397aac2deed7b546ccca88bf343',
    ),
    'laravel/passport' => 
    array (
      'pretty_version' => 'v4.0.3',
      'version' => '4.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0542f1f82edfbf857d0197c34a3d41f549aff30a',
    ),
    'laravel/tinker' => 
    array (
      'pretty_version' => 'v1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cafbf598a90acde68985660e79b2b03c5609a405',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a11ec5f4b4d75d1fcd04e133dede4c317aac9e18',
    ),
    'league/event' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2cc124cf9a3fab2bb4ff963307f60361ce4d119',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.0.50',
      'version' => '1.0.50.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dab4e7624efa543a943be978008f439c333f2249',
    ),
    'league/oauth2-server' => 
    array (
      'pretty_version' => '6.1.1',
      'version' => '6.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a0cabb573c7cd5ee01803daec992d6ee3677c4ae',
    ),
    'league/oauth2server' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'lncd/oauth2' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'mockery/mockery' => 
    array (
      'pretty_version' => '0.9.10',
      'version' => '0.9.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '4876fc0c7d9e5da49712554a35c94d84ed1e9ee5',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.24.0',
      'version' => '1.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bfc9ebb28f97e7a24c45bdc3f0ff482e47bb0266',
    ),
    'mtdowling/cron-expression' => 
    array (
      'pretty_version' => 'v1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9504fa9ea681b586028adaaa0877db4aecf32bad',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b8a3a99ba1f6a3952ac2747d989303cbd6b7a3e',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '1.36.2',
      'version' => '1.36.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd324b98bc30290f233dd0e75e6ce49f7ab2a6c9',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v3.1.5',
      'version' => '3.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb87e28e7d7b8d9a7fda231d37457c9210faf6ce',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v2.0.18',
      'version' => '2.0.18.0',
      'aliases' => 
      array (
      ),
      'reference' => '0a58ef6e3146256cc3dc7cc393927bcc7d1b72db',
    ),
    'phenx/php-font-lib' => 
    array (
      'pretty_version' => '0.5.1',
      'version' => '0.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '760148820110a1ae0936e5cc35851e25a938bc97',
    ),
    'phenx/php-svg-lib' => 
    array (
      'pretty_version' => 'v0.3.2',
      'version' => '0.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ccc46ef6340d4b8a4a68047e68d8501ea961442c',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '3.3.2',
      'version' => '3.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf329f6c1aadea3299f08ee804682b7c45b326a2',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '0.4.0',
      'version' => '0.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9c977708995954784726e25d0cd1dddf4e65b0f7',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '2.0.21',
      'version' => '2.0.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f1287e68b3f283339a9f98f67515dd619e5bf9d',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ba436b55987b4bf311cb7c6ba82aa528aac0a06',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '4.0.8',
      'version' => '4.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ef7b2f56815df854e66ceaee8ebe9393ae36a40d',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '1.4.5',
      'version' => '1.4.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '730b01bc3e867237eaac355e06a36b85dd93a8b4',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '1.0.9',
      'version' => '1.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '3dcf38ca72b158baf0bc245e9184d3fdffa9c46f',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '1.4.12',
      'version' => '1.4.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ce90ba27c42e4e44e6d8458241466380b51fa16',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '5.7.27',
      'version' => '5.7.27.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7803aeca3ccb99ad0a506fa80b64cd6a56bbc0c',
    ),
    'phpunit/phpunit-mock-objects' => 
    array (
      'pretty_version' => '3.4.4',
      'version' => '3.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a23b761686d50a560cc56233b9ecf49597cc9118',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6c001f1daafa3a3ac1d8ff69ee4db8e799a654dd',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.9.9',
      'version' => '0.9.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '9aaf29575bb8293206bb0420c1e1c87ff2ffa94e',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '2.0.5',
      'version' => '2.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '5601c8a83fbba7ef674a7369456d12f1e0d0eafa',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '3.8.0',
      'version' => '3.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd09ea80159c1929d75b3f9c60504d613aeb4a1e3',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '3.8.0',
      ),
    ),
    'sabberworm/php-css-parser' => 
    array (
      'pretty_version' => '8.1.0',
      'version' => '8.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '850cbbcbe7fbb155387a151ea562897a67e242ef',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '2b7424b55f5047b47ac6e5ccb20b2aea4011d9be',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f066a26a962dbe58ddea9f72a4e82874a3975a4',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5795ffe5dc5b02460c3e34222fee8cbe245d8fac',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce474bdd1a34744d7ac5d6aad3a46d48d9bac4c4',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bc37d50fea7d017d3d340f230811c9f1d7280af4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1311872ac850040a79c3c058bea3e22d0f09cbb7',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2c3ba150cbec723aa057506e73a8d33bdb286c9a',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce990bb21759f94aeafd30209e8cfcdfa8bc3f52',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v5.4.12',
      'version' => '5.4.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '181b89f18a90f8925ef805f950d47a7190e9b950',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '069bf3f0e8f871a2169a06e43d9f3f03f355e9be',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ca29297c29b64fb3a1a135e71cb25f67f9fdccf',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '667a26c4dd6bc75c67f06bc9bcd015bdecc7cbb8',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ed5be1663fa66623b3a7004d5d51a14c4045399b',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '7c0c627220308928e958a87c293108e5891cde1d',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '9a81d2330ea255ded06a69b4f7fb7804836e7a05',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc6bf17684b7120f7bf74fae85c9155506041002',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3d826245268269cd66f8326bd8bc066687b4a19',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c79c051f5b3a46be09205c73b80b346e4153e494',
    ),
    'symfony/polyfill-php70' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6b88000cdd431cd2e940caa2cb569201f3f84224',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '009f8dda80930e89e8344a4e310b08f9ff07dd2e',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'v1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a33352af16f78a5ff4f9d90811536abf210df12b',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '62f0b8d8cd2cd359c3caa5a9f5253a4a6d480646',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '81cfcd6935cb7505640153576c1f9155b2a179c1',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '2159335b452d929cbb9921fc4eb7d1bfed32d0be',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v3.4.22',
      'version' => '3.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba11776e9e6c15ad5759a07bffb15899bac75c2d',
    ),
    'tightenco/collect' => 
    array (
      'replaced' => 
      array (
        0 => 'v5.4.36',
      ),
    ),
    'tijsverkoyen/css-to-inline-styles' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ed4a2ea4e0902dac0489e6436ebcd5bbcae9757',
    ),
    'unisharp/laravel-filemanager' => 
    array (
      'pretty_version' => 'v1.9.2',
      'version' => '1.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aaf2764f26eb8dcc9356bea1e25010333c2e61f4',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2a7dcf7e3e02dc5e701004e51a6f304b713107d5',
    ),
    'vsmoraes/laravel-pdf' => 
    array (
      'pretty_version' => '2.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd68d422c39414d7ccead68d538f5368ff1711913',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '83e253c8e0be5b0257b881e1827274667c5c17a9',
    ),
    'yajra/laravel-datatables-oracle' => 
    array (
      'pretty_version' => 'v7.10.2',
      'version' => '7.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc849d04ac52b359495d661a004962a78c213194',
    ),
    'zendframework/zend-diactoros' => 
    array (
      'pretty_version' => '1.8.6',
      'version' => '1.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '20da13beba0dde8fb648be3cc19765732790f46e',
    ),
  ),
);

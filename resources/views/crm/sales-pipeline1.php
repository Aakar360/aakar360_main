<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />

    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    td:first-child, th:first-child {
        position:sticky;
        left:0;
        z-index:1;
        background-color:white;
    }
    td:nth-child(2),th:nth-child(2)  {
        position:sticky;
        left:40px;
        z-index:1;
        background-color:white;
    }
    .tableFixHead th {
        position: sticky;
        top: 0;
        background: #ffffff;
        z-index:2
    }
    th:first-child , th:nth-child(2) {
        z-index:3

    }
    .pipeline-box{
        box-shadow: 3px 1px 6px 2px #757574;
        position: relative;
        margin-bottom: 15px;
        background: linear-gradient(45deg, rgba(255, 255, 255, 0.02), rgba(204, 204, 204, 0.03)) !important;
        border-radius: 15px;
        transition: all 0.3s ease;
        overflow: hidden;
    }

</style>
<!-- END: Head-->
<?=$header;?>

<div class="row">
    <div class="col s12">
        <div  class="card card-tabs">
            <div class="card-content">
                <div class="row">
                    <div class="col s4 m4">
                        <button class="btn btn-primary center">Add New Sales Pipeline</button>
                    </div>
                </div>
                <div class="col s3 m3 pipeline-box clearfix" id="addMoreBox1">
                    <div class="col s12 m12">
                        <div class="row">
                            <div class="col s8 m8">
                                <h6 style="margin-top: 50px;">Pipeline Name</h6>
                            </div>
                            <div class="col s4 m4">
                                <h6 style="margin-top: 50px;">Action</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s8 m8">
                                <input type="text" name="name" class="form-control" id="name" value="">
                            </div>
                            <div class="col s4 m4" style="display: flex">
                                <button class="btn myblue waves-light" style="padding:0 10px;" onclick="editTable('');"><i class="material-icons">edit</i></button>
                                <button id="deletepipeline" class="btn myred waves-light" style="padding:0 10px;" onclick="deleteTable('');"><i class="material-icons">delete</i></button>
                                <button   class="btn myblue waves-light" style="padding:0 10px;" onclick="updateTable('');"><i class="material-icons right">save</i></button>
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col s10 m10">
                            <div class="form-group" id="occasionBox">
                                <input class="form-control"  type="text" name="new[0]" placeholder="New"/>
                                <div id="errorOccasion"></div>
                            </div>
                            <div class="form-group" id="occasionBox">
                                <input class="form-control"  type="text" name="followup[0]" placeholder="followup"/>
                                <div id="errorOccasion"></div>
                            </div>
                            <div class="form-group" id="occasionBox">
                                <input class="form-control"  type="text" name="underreview[0]" placeholder="underreview"/>
                                <div id="errorOccasion"></div>
                            </div> <div class="form-group" id="occasionBox">
                                <input class="form-control"  type="text" name="demo[0]" placeholder="demo"/>
                                <div id="errorOccasion"></div>
                            </div>
                            <div class="form-group" id="occasionBox">
                                <input class="form-control"  type="text" name="negotiation[0]" placeholder="negotiation"/>
                                <div id="errorOccasion"></div>
                            </div>
                            <div class="form-group" id="occasionBox">
                                <input class="form-control"  type="text" name="won[0]" placeholder="won"/>
                                <div id="errorOccasion"></div>
                            </div>
                            <div class="form-group" id="occasionBox">
                                <input class="form-control"  type="text" name="lost[0]" placeholder="lost"/>
                                <div id="errorOccasion"></div>
                            </div>
                        </div>
                        <div col s2 m2>
                            <div>
                                <button type="button"  onclick="removeBox(1)"  <i class="material-icons">delete</i></button>
                            </div><div>
                                <button type="button"  onclick="removeBox(1)"  <i class="material-icons">delete</i></button>
                            </div><div>
                                <button type="button"  onclick="removeBox(1)"  <i class="material-icons">delete</i></button>
                            </div>
                            <div>
                                <button type="button"  onclick="removeBox(1)"  <i class="material-icons">delete</i></button>
                            </div>
                            <div id="insertBefore"></div>
                            <div class="clearfix">
                            </div>
                            <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                Add More <i class="fa fa-plus"></i>
                            </button></br>
                            <input type="checkbox" id="default" name="default" value="default">
                            <label for="default">Mark As Default</label>
                        </div>
                    </div>
                    <!--/row-->
                </div>

                <div id="newPipeline" class="col s4 m4 sales_div pipeline-box" style="display: none;"></div>
            </div>
        </div>
    </div>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/bootstable.js" type="text/javascript"></script>
<script>
    var $insertBefore = $('#insertBefore');
    var $i = 0;
    // Date Picker
    jQuery('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });
    // Add More Inputs
    $('#plusButton').click(function(){

        $i = $i+1;
        var indexs = $i+1;
        $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="col-md-5"><div class="form-group "><input autocomplete="off" class="form-control date-picker'+$i+'" id="dateField'+indexs+'" name="date['+$i+']" data-date-format="dd/mm/yyyy" type="text" value="" placeholder="Date"/></div></div>' +
            '<div class="col-md-5 "style="margin-left:5px;"><div class="form-group"><input class="form-control " name="occasion['+$i+']" type="text" value="" placeholder="Occasion"/></div></div>' +
            '<div class="col-md-1"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></div>' + '</div>').insertBefore($insertBefore);

        // Recently Added date picker assign
        jQuery('#dateField'+indexs).datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
    });
    // Remove fields
    function removeBox(index){
        $('#addMoreBox'+index).remove();
    }



    $(".center").click(function () {
        $("#newPipeline").show();
        $.ajax({
            type: "POST",
            url: "add-default-sales-pipeline",
            data: '_token=<?=csrf_token()?>',
            beforeSend: function(){
                $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
            },
            success: function(data){
                $(".sales_div").html(data);
            }
        });
    });
    /*Edit Pipeline*/
    function editTable(id) {
        $("#makeEditable"+id).attr('contenteditable','true');
    }
    /*End Edit Pipeline*/

    /*Update Pipeline*/
    function updateTable(id) {
        $('#makeEditable'+id+'  tr').each(function() {
            var stages = $(this).find("td").html();
            jsonObj.push(stages);

        });
        var jsonstring = JSON.stringify(jsonObj)

        var name = $("#name"+id).val();
        $.ajax({
            type: "POST",
            url: "update-pipeline",
            data:'id='+id+'&_token=<?=csrf_token(); ?>'+'&name='+name+'&stages='+jsonstring,
            success: function(data){

            }
        });
    }
    /*End Update Pipeline*/
    /*Delete Pipeline*/
    function deleteTable(id) {
        $.ajax({
            type: "POST",
            url: "delete-pipeline-ajax",
            data:'id='+id+'&_token=<?=csrf_token(); ?>',
            success: function(data){
                $('#deletepipeline'+id).html('Deleted Successfully!!');
                window.location.reload();
            }
        });
    }
    /*End Delete Pipeline*/


</script>

</body>

</html>

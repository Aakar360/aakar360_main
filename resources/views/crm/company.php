<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <!--<link rel="stylesheet" type="text/css" href="<? //$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />



    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content">
                <?=$notices;?>
                <?php
                    if($errors->any()){
                        ?>
                        <div class="card-alert card red">
                            <div class = "card-content white-text" >
                                <p><?=$errors->first()?> </p>
                            </div>
                            <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                                <span aria-hidden = "true" >×</span>
                            </button> </div>
                        <?php
                    }
                ?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                    <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Company Master</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light" style="padding:0 5px;"  href="company?add">
                        <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                    </a>
                </div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive">
                            <table id="table" class=" display">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th>Contact</th>
                                    <th>Address</th>
                                    <th>GST No.</th>
                                    <th>Bank Acc Detail</th>
                                    <th>Logo</th>
                                    <th>Latter Head</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                            <tbody>
                            <?php

                            $i=1; foreach ($company as $com){ $cid = $com->id;?>
                                <tr>
                                    <td>
                                        <?=$i;?>
                                    </td>
                                    <td><?= $com->name?></td>
                                    <td><?= $com->contact?></td>
                                    <td><?= $com->address?></td>
                                    <td><?= $com->gstno?></td>
                                    <td><?= $com->acc_detail?></td>
                                    <td><a href="../assets/crm/images/company_logo/<?=$com->logo?>"><img src="../assets/crm/images/company_logo/<?=$com->logo?>" width="30px" height="30px"/></a></td>
                                    <td><a href="../assets/crm/images/visits/<?=$com->latter_head?>"><img src="../assets/crm/images/aaa/<?=$com->latter_head?>" width="30px" height="30px"/></a></td>
                                    <td>
                                        <button value="<?=$cid?>" class="btn myred waves-light cust_category_delete" style="padding:0 10px;"><i class="material-icons">delete</i></button><br>
                                        <a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="company?edit=<?=$cid?>"><i class="material-icons">edit</i></a>
                                    </td>
                                </tr>
                                <?php
                                $i++; }
                            ?>

                            </tbody>
                            </table>
                        </div>
                    </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Company </h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="company" class="col s12 m12" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name" required>
                                <label for="Full_Name">Name</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">person</i>
                                <input id="contact" type="text" class="validate" name="contact" required>
                                <label for="displayName">Contact No.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">person</i>
                                <input id="address" type="text" class="validate" name="address" required>
                                <label for="username">Address</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">person</i>
                                <input id="address" type="text" class="validate" name="gstno" required>
                                <label for="username">GST NO.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">person</i>
                                <input id="address" type="text" class="validate" name="acc_detail" required>
                                <label for="username">Account Detail</label>
                            </div>
                            <div class="input-field col s12 m3">
                                <div class="file-field">
                                    <div class="btn myblue waves-light left" style="padding:0 5px;">
                                        <span>Choose Logo</span>
                                        <input type="file" name="image" style="width: 100%" class="btn myblue" id="logo_image">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Upload your file">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col s12 m3">
                                <p id="logo_img_div">
                                    <img id="logo_image_show" src="#" height="200px" width="200px"  />
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <div class="file-field">
                                    <div class="btn myblue waves-light left" style="padding:0 5px;">
                                        <span>Choose Latter head</span>
                                        <input type="file" name="latter_head" style="width: 100%" class="btn myblue" id="latter_head">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Upload your file">
                                    </div>
                                </div>
                            </div>
                            <div class="input-field col s12 m3">
                                <p id="latter_head_div">
                                    <img id="latter_head_show" src="#" height="200px" width="200px"  />
                                </p>
                            </div>
                        </div>
                            <div class="input-field col s12 m12" style="text-align: center">
                                <button class="btn myblue waves-light add_submit" type="submit"  name="add">SAVE
                                    <i class="material-icons right">save</i>
                                </button>
                            </div>
                        </div>



                    </form>
                <?php } ?>

                <?php if(isset($_GET['edit'])){
                    $uid = $_GET['edit'];
                    $company = DB::select("SELECT * FROM `company` WHERE id = '$uid'")[0];
                    ?>
                            <h5 class="card-title">Edit Company Details</h5>
                            <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                                <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                                    <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                                </a>
                            </div>
                            <form method="post" action="company" class="col s12 m12" autocomplete="off">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden"  name="uid" value="<?php echo $uid; ?>">
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="Full_Name" type="text" class="validate" name="name" value="<?=$company->name?>" required>
                                        <label for="Full_Name">Name</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">person</i>
                                        <input id="contact" type="text" class="validate" name="contact" value="<?=$company->contact?>" required>
                                        <label for="displayName">Contact No.</label>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">person</i>

                                        <input id="address" type="text" class="validate" name="address" value="<?=$company->gstno?>" autocomplete="off" required>
                                        <label for="username">GST NO.</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">person</i>

                                        <input id="address" type="text" class="validate" name="address" value="<?=$company->acc_detail?>" autocomplete="off" required>
                                        <label for="username">Account Detail</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">person</i>

                                        <input id="address" type="file" class="validate" name="address" value="<?=$company->logo?>" autocomplete="off" required>
                                        <label for="username">Logo</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">person</i>

                                        <input id="address" type="text" class="validate" name="address" value="<?=$company->latter_head?>" autocomplete="off" required>
                                        <label for="username">latter Head</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">person</i>

                                        <input id="address" type="text" class="validate" name="address" value="<?=$company->address?>" autocomplete="off" required>
                                        <label for="username">Address</label>
                                    </div>
                                </div>
                                    <div class="input-field col s12 m12" style="text-align: center;margin-bottom: 20px">
                                        <button class="btn myblue waves-light edit_submit" type="submit" name="edit">SAVE
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                <?php } ?>
            </div>
        </div>
    </div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>
<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-company") ?>",
                type: 'GET',
                data: function (d) {

                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'contact', name: 'contact' },
                { data: 'address', name: 'address' },
                { data: 'gstno', name: 'gstno' },
                { data: 'acc_detail', name: 'acc_detail' },
                { data: 'logo', name: 'logo' },
                { data: 'latter_head', name: 'latter_head' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
</script>

<script>

    $(document).on('click','.cust_delete',function () {

        if (confirm("Are you sure?")) {
            var cid = $(this).val();
//            alert(cid);
//            var a = $(this);
            $.ajax({
                type: "POST",
                url: "company_delete",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
                success: function(data){

                    alert('Record Deleted Successfully');
                    location.reload();
                }
            });
        }
        else{

        }
        return false;
    });

    $('#logo_img_div').hide();
    function readURLcategory(input) {
        $('#logo_img_div').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#logo_image_show').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#logo_image").change(function(){
        readURLcategory(this);
    });

    $('#latter_head_div').hide();
    function readURLcategory(input) {
        $('#latter_head_div').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#latter_head_show').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#latter_head").change(function(){
        readURLcategory(this);
    });

</script>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <!--<link rel="stylesheet" type="text/css" href="<? //$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />



    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <?=$notices;?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>
                    <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Suppliers Master</h5>

                    <div class="row">
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
                            <div class="col s10 m10">
                            <label>Select Category</label>
                            <select class="browser-default category" id="category" name="category[]" multiple  tabindex="-1">

                                <?php
                                foreach ($customer_category as $cate) {
                                    $c_id = $cate->id;
                                    ?>
                                    <option value="<?= $c_id; ?>"><?= $cate->type ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            </div>
                        </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">flag</i></div>
                            <div class="col s10 m10">
                            <label>Select State</label>
                            <select class="browser-default states" id="states" name="states[]" multiple  tabindex="-1">
                                <?php
                                foreach ($states as $state) {
                                    $v_id = $state->id;
                                    ?>
                                    <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        </div>

                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
                            <div class="col s10 m10">
                            <label>Select District</label>
                            <select class="browser-default district" id="district" name="district[]" multiple  tabindex="-1">

                            </select>
                            </div>
                        </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">child_friendly</i></div>
                            <div class="col s10 m10">
                            <label>Product Category</label>
                            <select class="browser-default products" id="products" name="products[]" multiple  tabindex="-1">
                                <?php
                                foreach ($product_category as $p) {
                                    $v_id = $p->id;
                                    ?>
                                    <option value="<?=$v_id; ?>"><?= $p->name ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            </div>
                        </div>
                        <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                            <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px;" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                            </button>

                            <a class="btn myblue waves-light" style="padding:0 5px;"  href="suppliers?add">
                                <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                            </a>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive">
                            <table id="table" class="display">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Product Category</th>
                                    <th>Contact No.</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Supplier </h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="suppliers" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name">
                                <label for="Full_Name">Full Name</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">store</i>
                                <input id="Proprieter" type="tel" class="validate" name="proprieter_name">
                                <label for="Proprieter">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_mail</i>
                                <input id="Email" type="text" class="validate" name="email">
                                <label for="Email">Email</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_phone</i>
                                <input id="Contact" type="tel" class="validate valid_no" name="contact_no"">
                                <label for="Contact">Contact No.</label>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_headline</i>
                                <select name="customer_category" class="cc" required> customer_category
                                    <option selected disabled value="" >Select Category</option>
                                    <?php
                                    foreach ($customer_category as $category){
                                        $s_id = $category->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$category->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="customer_category">Category </label>

                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">navigation</i>
                                <input id="Address" type="tel" class="validate" name="postal_address">
                                <label for="Address">Postal Address</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">flag</i>
                                <select  class="state_list " name="state"  style="width: 100%" required>
                                    <?php
                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        ?>
                                        <option  <?php if(1 !=1) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        }?> value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="State">State</label>
                            </div>
                            <div class="input-field col s12 m6" >
                                <i class="material-icons prefix">adjust</i>
                                <label for="District">District</label>
                                <div class="col s12 m12" style="margin-left: 20px">
                                <select  class="browser-default district dist_list" name="district" style="width: 100%" required>
                                    <option selected value="">District</option>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m3" style="padding-top: 14px;">
                                <i class="material-icons prefix" style="padding-top: 14px;">mail</i>
                                <select name="msg_active">
                                    <option value="0"> Yes</option>
                                    <option value="1" selected> No</option>
                                </select>
                                <label for="msg_active" style="padding-top: 14px;">Message Active</label>
                            </div>
                            <div class="input-field col s12 m9">
                                <i class="material-icons prefix" style="margin-top: 10px">list</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                Select Category
                                <select  class="browser-default category" name="category[]" multiple tabindex="-1" style="width: 100% !important;">
                                    <?php foreach ($product_category as $category){
                                        ?>
                                        <option value="<?=$category->id?>"> <?=$category->name?> </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12" style="text-align: center;margin-bottom: 20px">
                        <button class="btn myblue waves-light add_submit" style="padding:0 5px;" type="submit" disabled name="add">SAVE
                            <i class="material-icons right">save</i>
                        </button>
                            </div>
                        </div>

                    </form>
                <?php } ?>
                <?php if(isset($_GET['edit'])){
                    $cid = $_GET['edit'];
                    $get_cust = DB::select("SELECT * FROM `crm_supplier` WHERE id = '$cid'")[0];
                    ?>
                    <h5>Edit Supplier </h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="suppliers" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <input type="hidden" class="ccid" name="cidd" value="<?php echo $cid; ?>">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name" value="<?=$get_cust->name;?>">
                                <label for="Full_Name">Full Name</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">store</i>
                                <input id="Proprieter" type="tel" class="validate" name="proprieter_name" value="<?=$get_cust->proprieter_name;?>">
                                <label for="Proprieter">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_mail</i>
                                <input id="Email" type="text" class="validate" name="email" value="<?=$get_cust->email;?>">
                                <label for="Email">Email</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_phone</i>
                                <input id="Contact" type="tel" class="validate valid_no" name="contact_no" value="<?=$get_cust->contact_no;?>">
                                <label for="Contact">Contact No.</label>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_headline</i>
                                <select name="customer_category" class="cc1"> customer_category
                                    <?php
                                    foreach ($customer_category as $category){
                                        $s_id = $category->id;
                                        $old_cid = $get_cust->customer_category;
                                        ?>
                                        <option <?=($s_id==$old_cid)?'selected':'';?>  value="<?=$s_id;?>"><?=$category->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="Category">Category</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">navigation</i>
                                <input id="Address" type="tel" class="validate" name="postal_address" value="<?=$get_cust->postal_address;?>">
                                <label for="Address">Postal Address</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">flag</i>

                                <select  class="state_list" name="state"  style="width: 100%" required>
                                    <?php
                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        $old_sid = $get_cust->state;
                                        ?>
                                        <option <?=($s_id==$old_sid)?'selected':'';?>  value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="State">State</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">adjust</i>
                                <select id="District2"  class="dist_list" name="district" style="width: 100%" required>

                                    <?php
                                    foreach ($selected_district as $sdist){
                                        $d_id = $sdist->did;
                                        $old_did = $get_cust->district;
                                        ?>
                                        <option <?=($d_id==$old_did)?'selected':'';?>  value="<?=$d_id;?>"><?=$sdist->d_name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="District2">District</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m3" style="padding-top: 14px;">
                                <i class="material-icons prefix" style="padding-top: 14px;">mail</i>
                                <?php $mactive = $get_cust->msg_active; ?>
                                <select name="msg_active">
                                    <option <?=($mactive==0)?'selected':'';?> value="0"> Yes</option>
                                    <option <?=($mactive==1)?'selected':'';?> value="1"> No</option>
                                </select>
                                <label for="State" style="padding-top: 14px;">Message Active</label>
                            </div>
                            <div class="input-field col s12 m9" >
                                <i id="seticon" class="material-icons prefix">child_friendly</i>
                                <div class="col s12 m12" style="margin-left: 24px">
                                Select Category
                                <select  class="browser-default category" name="category[]" multiple tabindex="-1" style="width: 100% !important;">
                                    <?php
                                    foreach ($product_category as $category){

                                        $cid = $category->id;
                                        $cid_array =explode(',',$get_cust->product_category);

                                        ?>
                                        <option <?php if(in_array($cid,$cid_array)){echo 'Selected';} ?> value="<?=$category->id?>"> <?=$category->name?> </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            </div>
        <div class="col s12 m12" style="text-align: center;margin: 10px">
                            <button class="btn myblue waves-light edit_submit" style="padding:0 5px;" type="submit" name="edit">SAVE
                                <i class="material-icons right">save</i>
                            </button>
        </div>
                        </div>

                    </form>
                <?php } ?>
            </div>
        </div>
    </div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>
<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-suppliers") ?>",
                type: 'GET',
                data: function (d) {
                    d.category = $('#category').val();
                    d.state = $('#states').val();
                    d.district = $('#district').val();
                    d.products = $('#products').val();
                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'category', name: 'customer_category' },
                { data: 'product_category', name: 'product_category' },
                { data: 'contact_no', name: 'contact_no' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            order: [[0, 'desc']],
            dom: 'lBfrtip',
            buttons: <?=$buttons;?>,
            fixedColumns: true,
            colReorder: true,
            exportOptions:{
                columns: ':visible'
            }
        });
    });
    $('#filterButton').click(function(){
        $('#table').DataTable().draw(true);
    });
</script>
<script>
    $(document).ready(function(){
        $('#table').attr('style', 'width: 100%');
        $('.user').select2();
        $('.category').select2();
        $('.states').select2();
        $('.district').select2();
        $('.products').select2();

        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $('.state_list').change(function () {
            var sid = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district",
                data:'sid='+sid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.dist_list').html(data);
                }
            });
        });
        $('.cc').change(function () {
            var cid = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-category_all_price",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.category_price').html(data);
                }
            });
        });
        $('.cc1').change(function () {
            var ctgid = $(this).val();
            var cid = $('.ccid').val();
            $.ajax({
                type: "POST",
                url: "get-category_all_price_cust",
                data:'custid='+cid+'&ctgid='+ctgid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.category_price').html(data);
                }
            });
        });
    })

    $('.valid_no').blur(function () {
        var a = $(this).val();
        var b = $(this).val().length;
        var c = a.indexOf(",");
        if(c==-1){
            if(b==10){
                $('.add_submit').prop("disabled", false);
                $('.edit_submit').prop("disabled", false);
            }
            else{
                alert("Please Enter 10 digit no");
                $('.add_submit').prop("disabled", true);
                $('.edit_submit').prop("disabled", true);
            }
        }else{
            var str = a;
            var str_array = str.split(',');
            for(var i = 0; i < str_array.length; i++) {
                str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                var tot = str_array[i].length;
                if(tot!=10){
                    alert("Please Enter 10 digit number before and after comma");
                    $('.add_submit').prop("disabled", true);
                    $('.edit_submit').prop("disabled", true);
                }
                else{
                    $('.add_submit').prop("disabled", false);
                    $('.edit_submit').prop("disabled", false);
                }
            }
        }
    });

    $("input[name=contact_no]").keypress(function (e) {

        if (/\d+|,/i.test(e.key) ){

            console.log("character accepted: " + e.key)
        } else {
            console.log("illegal character detected: "+ e.key)
            return false;
        }

    });


    $(document).on('click','.supp_delete',function () {
        if (confirm("Are you sure?")) {
            var cid = $(this).val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "supplier_delete",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html("Wait...");
                },
                success: function(data){
                    alert('Supplier Deleted Successfully');
                    location.reload();
                }
            });
        }
        else{

        }
        return false;
    });

</script>
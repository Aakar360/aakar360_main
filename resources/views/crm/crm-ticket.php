<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- END: Custom CSS-->

    <style>
        .modal-window {
            position: fixed;
            background-color: rgba(200, 200, 200, 0.75);
            top: -70px;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 999;
            opacity: 0;
            pointer-events: none;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
        }

        .modal-window:target {
            opacity: 1;
            pointer-events: auto;
        }

        .modal-window>div {
            position: relative;
            margin-top: 70px;
            padding: 2rem;
            background: #fff;
            color: #444;
        }

        .modal-window header {
            font-weight: bold;
        }

        .modal-close {
            color: #aaa;
            line-height: 50px;
            font-size: 80%;
            position: absolute;
            right: 0;
            text-align: center;
            top: 0;
            width: 70px;
            text-decoration: none;
        }

        .modal-close:hover {
            color: #000;
        }

        .modal-window h1 {
            font-size: 150%;
            margin: 0 0 15px;
        }
    </style>
</head>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <h5 class="card-title" style="padding: 5px; color: #0d1baa;"><?=$title; ?></h5>
                <?=$notices;?>

                <div class="row" style="margin-top:20px; ">
                    <form method="get" action="" id="filterForm">
                        <div class="col s12 m4 xl3"><i class="material-icons prefix">forum</i>
                            <div class="col s12 m12" style="margin-left: 20px;margin-top: -42px;">
                            <select id="type" name="type" style="margin-top: -44px;"> type
                                <option selected disabled value="">Select Type</option>
                                <option value="Question">Question</option>
                                <option value="Quotation">Quotation</option>
                                <option value="Problem">Problem</option>
                            </select>
                        </div>
                        </div>

                        <div class="col s12 m4 xl3"><i class="material-icons prefix">low_priority</i>
                            <div class="col s12 m12" style="margin-left: 20px;margin-top: -42px;">
                            <select id="priority" name="priority"> Priority
                            <option value="" selected disabled>Select Priority</option>
                            <option value="Low">Low</option>
                            <option value="Medium">Medium</option>
                            <option value="High">High</option>
                            <option value="Urgent">Urgent</option>
                        </select>
                            </div>
                        </div>

                        <div class="col s12 m4 xl3"><i class="material-icons prefix">thumbs_up_down</i>
                            <div class="col s12 m12" style="margin-left: 20px;margin-top: -42px;">
                            <select id="status" name="status"> Status
                            <option value="" selected disabled>Select Status</option>
                            <option value="Pending">Pending</option>
                            <option value="Resolved">Resolved</option>
                            <option value="Closed">Closed</option>
                        </select>
                            </div>
                        </div>

                        <div class="col s12 m4 xl3"><i class="material-icons prefix">list</i>
                            <div class="col s12 m12" style="margin-left: 20px;margin-top: -42px;">
                                <select id="category" name="category"> Category
                                    <option value="" selected disabled>Select Categoty</option>
                                    <?php
                                    foreach ($customer_type as $type){
                                        $s_id = $type->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col s12 m4 xl3"><i class="material-icons prefix">watch_later</i>
                            <div class="col s12 m12" style="margin-left: 20px;margin-top: -42px;">
                               <select id="my_date_type">
                                   <option value="1">Create Date</option>
                                   <option value="2">Due Date</option>
                               </select>
                            </div>
                        </div>

                            <div class="col s12 m4 xl3" id="create"><i class="material-icons prefix">date_range</i>
                                <div class="col s12 m12" style="margin-left: 20px;margin-top: -42px;">
                                    <label for="date_range">Create Date Range
                                        <input type="text" id="date_range" name="dates" style="margin-top: -11px;">
                                    </label>
                                    <input type="hidden" id="date_start" value=""/>
                                    <input type="hidden" id="date_end" value=""/>
                                </div>
                            </div>

                            <div class="col s12 m4 xl3" style="margin-left: 22px; display: none" id="due">
                                <div class="col s1 m1"style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">date_range</i></div>
                                <div class="col s11 m11" style="margin-top: -7px">
                                    <label for="date_range">Due Date Range
                                        <input type="text" id="date_range" name="dates">
                                    </label>
                                    <input type="hidden" id="date_start" value="<?=date("yyyy-mm-dd");?>"/>
                                    <input type="hidden" id="date_end" value="<?=date("yyyy-mm-dd");?>"/>
                                </div>
                            </div>

                        <div class="input-field col s12 m6">
                            <i class="material-icons prefix">transfer_within_a_station</i>

                            <select class="Status" name="Assign_to" id ="Assign_to"  tabindex="-1">
                                <option value="all"> All</option>
                                <?php foreach ($users as $user){
                                    ?>
                                    <option value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                    <?php
                                }
                                ?>
                            </select>
                            <!--input id="Assign_to" type="text" class="validate" name="assign_to"-->
                            <label for="Assign_to">Assign To </label>
                        </div>

                    </form>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 10px;" >
                        <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px;" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                        </button>
                    </div>

                </div>
                <div class="divider"></div>

                <div class="row" style="position:relative;">
                    <div class="col s12 table-responsive">
                        <table id="table" class="display">
                            <thead>
                                <th>Ticket ID</th>
                                <th>Customer</th>
                                <th>Category</th>
                                <th>Type</th>
                                <th>Priority</th>
                                <th>Description</th>
                                <th>Assign To</th>
                                <th>File</th>
                                <th>Create Date</th>
                                <th>Due Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>

<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/crm-ticket-data") ?>",
                type: 'GET',
                data: function (d) {
                    d.type = $('#type').val();
                    d.priority = $('#priority').val();
                    d.status = $('#status').val();
                    d.category = $('#category').val();
                    d.date_type = $('#my_date_type').val();
                    d.startd = $('#date_start').val();
                    d.endd = $('#date_end').val();
                    d.assign_to = $('#Assign_to').val();
                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'c_name', name: 'crm_customers.name' },
                { data: 'cc_type', name: 'customer_category.type' },
                { data: 't_type', name: 'crm_tickets.type'},
                { data: 'priority', name: 'priority' },
                { data: 'description', name: 'description'},
                { data: 'assign_to', name: 'assign_to'},
                { data: 'image', name: 'image', orderable: false,searchable: false },
                { data: 'create_date', name: 'create_date'},
                { data: 'due_date', name: 'due_date'},
                { data: 'status', name: 'status'},
                { data: 'action', name: 'action', orderable: false,searchable: false }
            ],
            order: [[0, 'desc']],
            dom: 'lBfrtip',
            buttons: <?=$buttons;?>,
            fixedColumns: true,
            colReorder: true,
            exportOptions:{
                columns: ':visible'
            }
        });

    });
    $('#filterButton').click(function(){
        $('#table').DataTable().draw(true);
    });
    $(document).ready(function(){
        $('.datepicker').datepicker();
    });

    $('input[name="dates"]').daterangepicker({
        opens: 'left',
        locale: {
            format: 'DD, MMM YYYY'
        }
    }, function(start, end, label) {
        $('#date_start').val(start.format('YYYY-MM-DD'));
        $('#date_end').val(end.format('YYYY-MM-DD'));
    });

    $('#my_date_type').change(function() {
        var opval = $(this).val();
        if(opval=="1"){
            document.getElementById('create').style.display = 'block';
            document.getElementById('due').style.display = 'none';
        }else{
            document.getElementById('due').style.display = 'block';
            document.getElementById('create').style.display = 'none';
        }
    });

</script>



<div id="open-modal2" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <center><img src="../assets/crm/images/tickets/" width="120px" height="120px"/></center>
    </div>
</div>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/animate-css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/chartist-js/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/chartist-js/chartist-plugin-tooltip.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/dashboard-modern.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/intro.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="https://kit.fontawesome.com/yourcode.js"></script>

    <!-- END: Custom CSS-->
    <style>
        @media screen and (orientation:landscape) {
            /* landscape-specific styles */
            .size{
                position: inherit;
                height: 56vh;
                width: 65vw;
            }
        }

        #toast-container {
            min-width: 10%;
            top: 15%;
            right: 50%;
            transform: translateX(50%) translateY(50%);
        }
    </style>

</head>
<?php echo $header?>
<!-- Main content -->
<body>




<div class="col s12">
    <div class="container">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <div class="col s12 m9">
                        <?php
                        if($errors->any()){ ?>
                            <div class="card-alert card red">
                                <div class="card-content white-text">
                                    <p><?=$errors->first(); ?></p>
                                </div>
                                <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                       <?php }

                        ?>
                    </div>
                    <div class="col s12 m3 right">
                        <i class="material-icons prefix" style="padding-top: 7px;">date_range</i>
                        <div class="col s12 m12" style="margin-left: 20px;margin-top: -62px;">
                            <label for="">Date Range
                                <input type="text" id="date_range1" name="dates1">
                            </label>
                            <form method="get" action="" id="dateForm1">
                                <input type="hidden" name="start1" id="date_start1" value=""/>
                                <input type="hidden" name="end1" id="date_end1" value=""/>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row" name="desktop">
                    <div class="col xl2" style="cursor: pointer; width: 12% !important;" onclick="showChart_new('raw_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow min-height-100 white-text animate fadeLeft">
                            <div class="padding-4" style="font-size:12px !important;">
                                <div class="col s12 m12">
                                    <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px">folder_open</i>
                                    <h4 class="white-text" style="font-size: 1rem;"><?=count($data['raw'])?></h4>
                                </div>
                                <p style="text-align: center">RAW</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m4 xl2"  style="cursor: pointer; width: 12% !important;" onclick="showChart_new('unreachable_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow min-height-100 white-text animate fadeLeft">
                            <div class="padding-4" style="font-size:12px !important;">
                                <div class="col s12 m12">
                                    <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px"><img style="height:24px" src="../assets/crm/images/icon/unreachable.png"></i>
                                    <h4 class="white-text" style="font-size: 1rem;"><?=count($data['unreachable'])?></h4>
                                </div>
                                <p style="text-align: center">UNREACHABLE</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m4 xl2"  style="cursor: pointer; width: 12% !important;" onclick="showChart_new('contacted_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow min-height-100 white-text animate fadeLeft">
                            <div class="padding-4" style="font-size:12px !important;">
                                <div class="col s12 m12">
                                    <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px">folder_shared</i>
                                    <h4 class="white-text" style="font-size: 1rem;"><?=count($data['contacted'])?></h4>
                                </div>
                                <p style="text-align: center">CONTACTED</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m4 xl2"  style="cursor: pointer; width: 12% !important;" onclick="showChart_new('interested_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow min-height-100 white-text animate fadeLeft">
                            <div class="padding-4" style="font-size:12px !important;">
                                <div class="col s12 m12">
                                    <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px">folder_special</i>
                                    <h4 class="white-text" style="font-size: 1rem;"><?=count($data['interested'])?></h4>
                                </div>
                                <p style="text-align: center">INTERESTED</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m4 xl2"  style="cursor: pointer; width: 12% !important;" onclick="showChart_new('converted_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow min-height-100 white-text animate fadeLeft">
                            <div class="padding-4" style="font-size:12px !important;">
                                <div class="col s12 m12">
                                    <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px"><img style="height:26px"  src="../assets/crm/images/icon/user.png"></i>
                                    <h4 class="white-text" style="font-size: 1rem;"><?=count($data['converted'])?></h4>
                                </div>
                                <p style="text-align: center">CONVERTED</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m4 xl2"  style="cursor: pointer; width: 12% !important;" onclick="showChart_new('unqualified_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow min-height-100 white-text animate fadeLeft">
                            <div class="padding-4" style="font-size:12px !important;">
                                <div class="col s12 m12" style="font-size:12px !important;">
                                    <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px"><img style="height:24px" src="../assets/crm/images/icon/user_cross.png"></i>
                                    <h4 class="white-text" style="font-size: 1rem;"><?=count($data['unqualified'])?></h4>
                                </div>
                                <p style="text-align: center">UNQUALIFIED</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m4 xl2"  style="cursor: pointer; width: 12% !important;" onclick="showChart_new('not_in_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow min-height-100 white-text animate fadeLeft">
                            <div class="padding-4" style="font-size:12px !important;">
                                <div class="col s12 m12">
                                    <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px"><img style="height: 24px;" src="../assets/crm/images/icon/icon12.png"> </i>
                                    <h4 class="white-text" style="font-size: 1rem;"><?=count($data['not_in'])?></h4>
                                </div>
                                <p style="text-align: center">NOT INTERESTED</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m4 xl2"  style="cursor: pointer; width: 12% !important;" onclick="showChart_new('varify_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow min-height-100 white-text animate fadeLeft">
                            <div class="padding-4" style="font-size:12px !important;">
                                <div class="col s12 m12">
                                    <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px"><img style="height:26px"  src="../assets/crm/images/icon/verified.png"></i>
                                    <h4 class="white-text" style="font-size: 1rem;"><?=count($data['varify'])?></h4>
                                </div>
                                <p style="text-align: center">VARIFIED</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" name="mobile" >
                    <div class="col s12 m2" style="cursor: pointer;" onclick="showChart_new('raw_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow white-text animate fadeLeft">
                                <div class="col s12 m12" style="padding-bottom: 5px">
                                    <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px">folder_open</i>
                                    <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width:auto;"><?=count($data['raw'])?></h4>
                                    <p style="text-align: right;margin-top: 23px">RAW</p>
                                </div>
                        </div>
                    </div>

                    <div class="col s12 m2"  style="cursor: pointer;" onclick="showChart_new('unreachable_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow white-text animate fadeLeft">
                            <div class="col s12 m12" style="padding-bottom: 5px">
                                <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px">folder_shared</i>
                                <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width:auto;"><?=count($data['un_reachable'])?></h4>
                                <p style="text-align: right;margin-top: 23px">UNREACHABLE</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m2"  style="cursor: pointer;" onclick="showChart_new('contacted_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow white-text animate fadeLeft">
                            <div class="col s12 m12" style="padding-bottom: 5px">
                                <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px">folder_shared</i>
                                <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width:auto;"><?=count($data['contacted'])?></h4>
                                <p style="text-align: right;margin-top: 23px">CONTACTED</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m2"  style="cursor: pointer;" onclick="showChart_new('interested_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow white-text animate fadeLeft">
                            <div class="col s12 m12" style="padding-bottom: 5px">
                                <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px">folder_special</i>
                                <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width:auto;"><?=count($data['interested'])?></h4>
                                <p style="text-align: right;margin-top: 23px">INTERESTED</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m2"  style="cursor: pointer;" onclick="showChart_new('converted_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow white-text animate fadeLeft">
                            <div class="col s12 m12" style="padding-bottom: 5px">
                                <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px"><img style="height:26px" src="../assets/crm/images/icon/user.png"></i>
                                <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width:auto;"><?=count($data['converted'])?></h4>
                                <p style="text-align: right;margin-top: 23px">CONVERTED</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m2"  style="cursor: pointer;" onclick="showChart_new('unqualified_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow white-text animate fadeLeft">
                            <div class="col s12 m12" style="padding-bottom: 5px">
                                <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px"><img style="height:24px " src="../assets/crm/images/icon/user_cross.png"></i>
                                <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width:auto;"><?=count($data['unqualified'])?></h4>
                                <p style="text-align: right;margin-top: 23px">UNQUALIFIED</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m2"  style="cursor: pointer;" onclick="showChart_new('not_in_data_chart');">
                        <div class="card gradient-45deg-purple-deep-purple gradient-shadow white-text animate fadeLeft">
                            <div class="col s12 m12" style="padding-bottom: 5px">
                                <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px"><img style="height: 24px;" src="../assets/crm/images/icon/icon12.png"></i>
                                <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width:auto;"><?=count($data['not_in'])?></h4>
                                <p style="text-align: right;margin-top: 23px">NOT INTERESTED</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12 m9 xl9 chart1 raw_data_chart" style="padding: 15px;">
                        <h4 class="card-title" style="margin-top: 20px">Raw Graph</h4>
                        <div class="chart-container size" name="graphsize">
                            <canvas id="raw_data_chart"></canvas>
                        </div>
                    </div>

                    <div class="col s12 m9 chart1 unreachable_data_chart" style="display: none;padding: 15px;">
                        <h4 class="card-title" style="margin-top: 20px">Unreachable Graph</h4>
                        <div class="chart-container" name="graphsize">
                            <canvas id="unreachable_data_chart"></canvas>
                        </div>
                    </div>
                    <div class="col s12 m9 chart1 contacted_data_chart" style="display: none;padding: 15px;">
                        <h4 class="card-title" style="margin-top: 20px">Contacted Graph</h4>
                        <div class="chart-container" name="graphsize">
                            <canvas id="contacted_data_chart"></canvas>
                        </div>
                    </div>

                    <div class="col s12 m9 chart1 interested_data_chart" style="display: none;padding: 15px;">
                        <h4 class="card-title" style="margin-top: 20px">Interested Graph</h4>
                        <div class="chart-container" name="graphsize">
                            <canvas id="interested_data_chart"></canvas>
                        </div>
                    </div>

                    <div class="col s12 m9 chart1 converted_data_chart" style="display: none;padding: 15px;">
                        <h4 class="card-title" style="margin-top: 20px">Converted Graph</h4>
                        <div class="chart-container" name="graphsize">
                            <canvas id="converted_data_chart"></canvas>
                        </div>
                    </div>

                    <div class="col s12 m9 chart1 unqualified_data_chart" style="display: none;padding: 15px;">
                        <h4 class="card-title" style="margin-top: 20px">Unqualified Graph</h4>
                        <div class="chart-container" name="graphsize">
                            <canvas id="unqualified_data_chart"></canvas>
                        </div>
                    </div>


                    <div class="col s12 m12 xl9 chart1 not_in_data_chart" style="display: none;padding: 15px;">
                        <h4 class="card-title" style="margin-top: 20px">Not Interested Graph</h4>
                        <div class="chart-container" name="graphsize">
                            <canvas id="not_in_data_chart"></canvas>
                        </div>
                    </div>

                    <div class="col s12 m12 xl9 chart1 varify_data_chart" style="display: none;padding: 15px;">
                        <h4 class="card-title" style="margin-top: 20px">Varified Graph</h4>
                        <div class="chart-container" name="graphsize">
                            <canvas id="varify_data_chart"></canvas>
                        </div>
                    </div>

                    <div class="col s12 m3 xl3 card-width" >
                        <div class="card border-radius-6" style="height: 300px">
                            <div class="card-content center-align">
                                <i class="material-icons amber-text small-ico-bg mb-5">check</i>
                                <h4 class="m-0"><b>21.5k</b></h4>
                                <p>Total Views</p>
                                <p>145%</p>
                                <p>Total visit</p>
                                <p class="green-text  mt-3"><i class="material-icons vertical-align-middle">arrow_drop_up</i>
                                    119.71%</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col s12">
    <div class="container">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                       <div class="col s12 m3 right">
                           <i class="material-icons prefix" style="padding-top: 7px;">date_range</i>
                           <div class="col s12 m12" style="margin-left: 20px;margin-top: -62px;">
                               <label for="date_range">Date Range
                                   <input type="text" id="date_range" name="dates">
                               </label>
                               <form method="get" action="" id="dateForm">
                                   <input type="hidden" name="start" id="date_start" value=""/>
                                   <input type="hidden" name="end" id="date_end" value=""/>
                               </form>
                           </div>
                       </div>
                    </div>
                    <div class="row" name="desktop">
                        <div class="col s12 m6 l6 xl3" style="cursor: pointer;" onclick="showChart('chart_msgs');">
                            <div class="card brown darken-2 gradient-shadow min-height-100 white-text animate fadeLeft">
                                <div class="padding-4">

                                    <div class="col s12 m12">
                                        <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px">email</i>
                                        <h4 class="white-text" style="font-size: 1rem;"><?=count($data['messages'])?></h4>
                                    </div>
                                    <p style="text-align: center">Messages</p>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3" style="cursor: pointer;" onclick="showChart('chart_calls');">
                            <div class="card brown darken-2 gradient-shadow min-height-100 white-text animate fadeLeft">
                                <div class="padding-4">
                                    <div class="col s12 m12">
                                        <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px">phone</i>
                                        <h4 class="white-text" style="font-size: 1rem;"><?=count($data['calls'])?></h4>
                                    </div>
                                    <p style="text-align: center">Calls</p>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3" style="cursor: pointer;" onclick="showChart('chart_visits');">
                            <div class="card brown darken-2 gradient-shadow min-height-100 white-text animate fadeRight">
                                <div class="padding-4">
                                    <div class="col s12 m12">
                                        <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px">transfer_within_a_station</i>
                                        <h4 class="white-text" style="font-size: 1rem;"><?=count($data['visits'])?></h4>
                                    </div>
                                    <p style="text-align: center">Visits</p>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3" style="cursor: pointer;" onclick="showChart('chart_promo');">
                            <div class="card brown darken-2 gradient-shadow min-height-100 white-text animate fadeRight">
                                <div class="padding-4">
                                    <div class="col s12 m12">
                                        <i class="material-icons background-round mt-3 f-left" style="margin-right: 18px">attach_money</i>
                                        <h4 class="white-text" style="font-size: 1rem;"><?=count($data['promotional'])?></h4>
                                    </div>
                                    <p style="text-align: center">Promotional</p>
                                </div>
                                </div>
                            </div>
                        </div>


                    <div class="row" name="mobile">
                        <div class="col s12 m6 l6 xl3" style="cursor: pointer;" onclick="showChart('chart_msgs');">
                            <div class="card brown darken-2 gradient-shadow white-text animate fadeLeft">
                                <div class="col s12 m12" style="padding-bottom: 5px">
                                    <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px">email</i>
                                    <h4 class="white-text" style="font-size: 1rem;margin-top: 26px; width: auto;"><?=count($data['messages'])?></h4>
                                    <p style="text-align: right;margin-top: 23px">Messages</p>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3" style="cursor: pointer;" onclick="showChart('chart_calls');">
                            <div class="card brown darken-2 gradient-shadow white-text animate fadeLeft">
                                <div class="col s12 m12" style="padding-bottom: 5px">
                                    <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px">phone</i>
                                    <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width: auto;"><?=count($data['calls'])?></h4>
                                    <p style="text-align: right;margin-top: 23px">Calls</p>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3" style="cursor: pointer;" onclick="showChart('chart_visits');">
                            <div class="card brown darken-2 gradient-shadow white-text animate fadeLeft">
                                <div class="col s12 m12" style="padding-bottom: 5px">
                                    <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px">transfer_within_a_station</i>
                                    <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width: auto;"><?=count($data['visits'])?></h4>
                                    <p style="text-align: right;margin-top: 23px">Visits</p>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3" style="cursor: pointer;" onclick="showChart('chart_promo');">
                            <div class="card brown darken-2 gradient-shadow white-text animate fadeLeft">
                                <div class="col s12 m12" style="padding-bottom: 5px">
                                    <i class="material-icons background-round mt-1 f-left" style="margin-right: 14px">attach_money</i>
                                    <h4 class="white-text" style="font-size: 1rem;margin-top: 26px;width: auto;"><?=count($data['promotional'])?></h4>
                                    <p style="text-align: right;margin-top: 23px">Promotional</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col s12 m9 charts chart_msgs" style="padding:15px;">
                            <h4 class="card-title" style="margin-top: 20px">Messages Graph</h4>
                            <div class="chart-container" name="graphsize">
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                        <div class="col s12 m9 charts chart_calls" style="display: none;padding: 15px;">
                            <h4 class="card-title" style="margin-top: 20px">Calls Graph</h4>
                            <div class="chart-container" name="graphsize">
                                <canvas id="myChart1"></canvas>
                            </div>
                        </div>
                        <div class="col s12 m9 charts chart_visits" style="display: none;padding: 15px;">
                            <h4 class="card-title" style="margin-top: 20px">Visits Graph</h4>
                            <div class="chart-container" name="graphsize">
                                <canvas id="myChart2"></canvas>
                            </div>
                        </div>
                        <div class="col s12 m9 charts chart_promo" style="display: none;padding: 15px;">
                            <h4 class="card-title" style="margin-top: 20px">Promotional Materials Graph</h4>
                            <div class="chart-container" name="graphsize">
                                <canvas id="myChart3"></canvas>
                            </div>
                        </div>
                        <div class="col s12 m3 xl3 l3 card-width" >
                            <div class="card border-radius-6" style="height: 300px">
                                <div class="card-content center-align">
                                    <i class="material-icons amber-text small-ico-bg mb-5">check</i>
                                    <h4 class="m-0"><b>21.5k</b></h4>
                                    <p>Total Views</p>
                                    <p>145%</p>
                                    <p>Total visit</p>
                                    <p>more data</p>
                                    <p class="green-text  mt-3"><i class="material-icons vertical-align-middle">arrow_drop_up</i>
                                        119.71%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>




<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/chartjs/chart.min.js" type="text/javascript"></script>

<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/dashboard-modern.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/form-elements.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


<script>


    options = {
        responsive: true,
        maintainAspectRatio: false,
        aspectRatio: 5,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    };

    //chart for first top buttons
    var ctx22 = document.getElementById('raw_data_chart').getContext('2d');
    var chart = new Chart(ctx22, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['raw_label']);?>,
            datasets: [{
                label: 'Raw',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['raw_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });

    var ctx23 = document.getElementById('contacted_data_chart').getContext('2d');
    var chart = new Chart(ctx23, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['contacted_label']);?>,
            datasets: [{
                label: 'Contacted',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['contacted_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });

    var ctx28 = document.getElementById('unreachable_data_chart').getContext('2d');
    var chart = new Chart(ctx28, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['unreachable_label']);?>,
            datasets: [{
                label: 'Unreachable',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['un_reachable']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });

    //Verify Graph
    var ctx29 = document.getElementById('varify_data_chart').getContext('2d');
    var chart = new Chart(ctx29, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['varify_label']);?>,
            datasets: [{
                label: 'Varified',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['varified']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });

    var ctx24 = document.getElementById('interested_data_chart').getContext('2d');
    var chart = new Chart(ctx24, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['interested_label']);?>,
            datasets: [{
                label: 'Interested',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['interested_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });

    var ctx25 = document.getElementById('converted_data_chart').getContext('2d');
    var chart = new Chart(ctx25, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['converted_label']);?>,
            datasets: [{
                label: 'Converted',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['converted_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });

    var ctx26 = document.getElementById('unqualified_data_chart').getContext('2d');
    var chart = new Chart(ctx26, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['unqualified_label']);?>,
            datasets: [{
                label: 'Unqualified',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['unqualified_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });

    var ctx27 = document.getElementById('not_in_data_chart').getContext('2d');
    var chart = new Chart(ctx27, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['not_in_label']);?>,
            datasets: [{
                label: 'Not Interested',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['not_in_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });

    function showChart_new(chart_id){
        $('.chart1').hide();
        $('.'+chart_id).show();
    }

    $(document).ready(function(){
        var startDate1 = moment('<?=$start_date1?>', 'DD-MM-YYYY HH:mm:ss').format('DD-MM-YYYY HH:mm:ss');
        var endDate1 = moment('<?=$end_date1?>', 'DD-MM-YYYY HH:mm:ss').format('DD-MM-YYYY HH:mm:ss');
        $('#date_range1').daterangepicker({
            useCurrent: false,
            opens: 'left',
            locale: {
                format: 'DD-MM-YYYY'
            },
            startDate: startDate1,
            endDate: endDate1
        },  function(start1, end1, label) {
            $('#date_start1').val(start1.format('DD-MM-YYYY'));
            $('#date_end1').val(end1.format('DD-MM-YYYY'));
            $('#dateForm1').submit();
        });
    });


    // chart for second top button
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['msg_label']);?>,
            datasets: [{
                label: 'Messages',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['msg_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });
    var ctx1 = document.getElementById('myChart1').getContext('2d');

    var chart1 = new Chart(ctx1, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['call_label']);?>,
            datasets: [{
                label: 'Calls',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['call_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });
    var ctx2 = document.getElementById('myChart2').getContext('2d');

    var chart2 = new Chart(ctx2, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['visit_label']);?>,
            datasets: [{
                label: 'Visits',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['visit_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });
    var ctx3 = document.getElementById('myChart3').getContext('2d');
    var chart3 = new Chart(ctx3, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: <?=json_encode($data['promo_label']);?>,
            datasets: [{
                label: 'Promotional',
                backgroundColor: 'rgba(255, 99, 132, 0.60)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?=json_encode($data['promo_dataset']);?>,
                borderWidth: 1
            }]
        },

        // Configuration options go here
        options: options
    });
    function showChart(chart_id){
        $('.charts').hide();
        $('.'+chart_id).show();
    }



    $(document).ready(function(){
        /*var startDate = moment(new Date(<=strtotime($start_date)?> * 1000), 'DD, MMM YYYY');
        var endDate = moment(new Date(<=strtotime($end_date)?> * 1000), 'DD, MMM YYYY');*/
        var startDate = moment('<?=$start_date?>', 'DD-MM-YYYY HH:mm:ss').format('DD-MM-YYYY HH:mm:ss');
        var endDate = moment('<?=$end_date?>', 'DD-MM-YYYY HH:mm:ss').format('DD-MM-YYYY HH:mm:ss');
        $('#date_range').daterangepicker({
            useCurrent: false,
            opens: 'left',
            locale: {
                format: 'DD-MM-YYYY'
            },
            startDate: startDate,
            endDate: endDate
        }, function(start, end, label) {
            $('#date_start').val(start.format('DD-MM-YYYY'));
            $('#date_end').val(end.format('DD-MM-YYYY'));
            $('#dateForm').submit();
        });
    });



    $(document).ready(function() {
        // This will fire when document is ready:
        $(window).resize(function() {
            // This will fire each time the window is resized:
            window.addEventListener("orientationchange", function() {
                // Announce the new orientation number
                var mobile = document.getElementsByName("graphsize");
                $(mobile).css({"position": "absolute", "height": "56vh", "width": "65vw"});
            }, false);

            if($(window).width() >= 480) {
                // if larger or equal
                // Listen for orientation changes
                var mobile = document.getElementsByName("graphsize");
                $(mobile).css({"position": "inherit", "height": "253px", "width": "65vw"});
            } else {
                var desktop = document.getElementsByName("graphsize");
                $(desktop).css({"position": "inherit", "height": "253px", "width": "85vw"});
            }
        }).resize(); // This will simulate a resize to trigger the initial run.
    });

    $(document).ready(function() {
        // This will fire when document is ready:
        $(window).resize(function() {
            // This will fire each time the window is resized:
            if($(window).width() >= 480) {
                // if larger or equal
                var tablemobile = document.getElementsByName("mobile");
                var tabledesktop = document.getElementsByName("desktop");
                $(tabledesktop).show();
                $(tablemobile).hide();

            } else {
                var tablemobile = document.getElementsByName("mobile");
                var tabledesktop = document.getElementsByName("desktop");
                // if smaller
                $(tablemobile).show();
                $(tabledesktop).hide();
            }
        }).resize(); // This will simulate a resize to trigger the initial run.
    });



</script>
</body>

</html>
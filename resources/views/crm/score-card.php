<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>

    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!--    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>-->
    <link href="<?= $tp1; ?>/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <!-- END: Custom CSS-->


</head>

<style type="text/css">

    .file-field input.file-path{
        height: 33px !important;
    }
    .file-field .btn, .file-field .btn-large, .file-field .btn-small{
        height: 33px !important;
        line-height: 33px !important;
    }

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }
    @media only screen and (max-width: 600px) {
        .modal-window>div {
            width: 340px;
            position: relative;
            margin: 10% auto;
            padding: 2rem;
            background: #fff;
            color: #444;
        }
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    .mygray{
        background-color: #aab2ae !important;
    }
    .myred{
        background-color: #eb6667 !important;
    }
    .mygreen{
        background-color: #4bbb50 !important;
    }
    .btn-arrow-right,
    .btn-arrow-left {
        position: relative;
        padding-left: 18px;
        padding-right: 18px;
    }
    .btn-arrow-right {
        padding-left: 36px;
    }
    .btn-arrow-left {
        padding-right: 36px;
    }
    .btn-arrow-right:before,
    .btn-arrow-right:after,
    .btn-arrow-left:before,
    .btn-arrow-left:after { /* make two squares (before and after), looking similar to the button */
        content:"";
        position: absolute;
        top: 7px; /* move it down because of rounded corners */
        width: 22px; /* same as height */
        height: 22px; /* button_outer_height / sqrt(2) */
        background: inherit; /* use parent background */
        border: inherit; /* use parent border */
        border-left-color: transparent; /* hide left border */
        border-bottom-color: transparent; /* hide bottom border */
        border-radius: 0px 4px 0px 0px; /* round arrow corner, the shorthand property doesn't accept "inherit" so it is set to 4px */
        -webkit-border-radius: 0px 4px 0px 0px;
        -moz-border-radius: 0px 4px 0px 0px;
    }
    .btn-arrow-right:before,
    .btn-arrow-right:after {
        transform: rotate(45deg); /* rotate right arrow squares 45 deg to point right */
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
    }
    .btn-arrow-left:before,
    .btn-arrow-left:after {
        transform: rotate(225deg); /* rotate left arrow squares 225 deg to point left */
        -webkit-transform: rotate(225deg);
        -moz-transform: rotate(225deg);
        -o-transform: rotate(225deg);
        -ms-transform: rotate(225deg);
    }
    .btn-arrow-right:before,
    .btn-arrow-left:before { /* align the "before" square to the left */
        left: -11px;
    }
    .btn-arrow-right:after,
    .btn-arrow-left:after { /* align the "after" square to the right */
        right: -11px;
    }
    .btn-arrow-right:after,
    .btn-arrow-left:before { /* bring arrow pointers to front */
        z-index: 1;
    }
    .btn-arrow-right:before,
    .btn-arrow-left:after { /* hide arrow tails background */
        background-color: white;
    }

    .visit {
        display: block !important;
    }
    /* #timeline
     }*/
    #timeline-content {
        margin-top: 50px
        height: 300px;
        text-align: center;
    }

    /* Timeline */

    .timeline1 {
        border-left: 4px solid #004ffc;
        border-bottom-right-radius: 4px;
        border-top-right-radius: 4px;
        background: rgba(255, 255, 255, 0.03);
        color: rgba(255, 255, 255, 0.8);
        font-family: 'Chivo', sans-serif;
        margin: 50px auto;
        letter-spacing: 0.5px;
        position: relative;
        line-height: 1.4em;
        font-size: 1.03em;
        padding: 50px;
        list-style: none;
        text-align: left;
        font-weight: 100;
        max-width: 95%;
    }
    .timeline1 .event {
        border-bottom: 1px dashed rgba(255, 255, 255, 0.1);
        position: relative;
    }
    .event:last-of-type {
        padding-bottom: 0;
        margin-bottom: 0;
        border: none;
    }

    .event :before,

    .event :after {
        position: absolute;
        display: block;
        top: 0;
    }

    &
    .timeline1 .event:before {
        left: -215.5px;
        color: rgba(255, 255, 255, 0.4);
        content: attr(data-date);
        text-align: right;
        font-weight: 100;
        font-size: 0.9em;
        min-width: 120px;
        font-family: 'Saira', sans-serif;
    }
    .timeline1 .event:after{
        box-shadow: 0 0 0 4px #004ffc;
        left: -9.85px;
        background: #313534;
        border-radius: 50%;
        height: 11px;
        width: 11px;
        content: "";
        top: 5px;
        position: absolute;
    }

</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs">
            <div class="card-content" style="min-height: 500px;">
                <div class="card-title">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <ul class="row tabs" >
                                <li class="tab col s4 m4 x12"><a class="p-0 <?php if(!empty($_GET['searchtype'])) {if($_GET['searchtype'] == ""){echo 'active';}}?>"  href="#Score">Calls/Leads/Orders</a></li>
                                <li class="tab col s4 m4 xl2"><a class="p-0 <?php if(!empty($_GET['searchtype'])) {if($_GET['searchtype'] == "no_order"){echo 'active';}}?>"  href="#NoOrder">No Order</a></li>
                                <li class="tab col s12 m12 xl2"><a class="p-0 <?php if(!empty($_GET['searchtype'])) {if($_GET['searchtype'] == "month_order"){echo 'active';}}?>"  href="#MonthOrder">Month Order</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="Score" class="col-lg-12 col-xs-12">
                        <div class="divider"></div>
                        <div class="visits_form">
                            <form method="get" action="" id="searchForm1">
                                <div class="col s12 m12">
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix" >Type</i>
                                        <div class="col s12 m12">
                                            <label> By Type</label>
                                            <select class="browser-default" name="type" id="type">
                                                <option <?php if(isset($_GET['type'])&& $_GET['type'] == "byuser"){echo 'selected';} ?>value="byuser">By User</option>
                                                <option <?php if(isset($_GET['type'])&& $_GET['type'] == "bytype"){echo 'selected';} ?> value="bytype">By Type</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix" >supervisor_account</i>
                                        <div class="col s12 m12">
                                            <label>BY Designation</label>
                                            <select class="browser-default designation" id="designation" name="desigantion[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                                <option value=""> Select Designation</option>
                                                <?php foreach ($designations as $designation){ ?>
                                                    <option <?php if(isset($_GET['desigantion'])&& in_array('des'.$designation->id, $_GET['desigantion'])){echo 'selected';} ?> value="<?='des'.$designation->id?>"> <?=$designation->name?> </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix" >date_range</i>
                                        <div class="col s12 m12">
                                            <label for="">Date Range
                                                <input type="text" id="date_range1" name="dates1">
                                            </label>
                                            <input type="hidden" name="start1" id="date_start1" value=""/>
                                            <input type="hidden" name="end1" id="date_end1" value=""/>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix">format_list_bulleted</i>
                                        <div class="col s12 m12">
                                            <label>Select Category</label>
                                            <select class="browser-default serachcategory" id="serachcategory" name="serachcategory[]" multiple  tabindex="-1">
                                                <?php $i=0;
                                                foreach ($customer_category as $cate) {
                                                    $c_id = $cate->id;
                                                    $selected = ''; ?>
                                                    <option <?php if(isset($_GET['serachcategory'])&& in_array($cate->id, $_GET['serachcategory'])){echo 'selected';} ?> value="<?= $c_id; ?>" <?=$selected?>><?= $cate->type ?></option>
                                                    <?php $i++; } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix">dns</i>
                                        <div class="col s12 m12">
                                            <label>Select Customer Class</label>
                                            <select class="browser-default lead_customer_class" id="lead_customer_class" name="lead_cust_class[]" multiple tabindex="-1">
                                                <?php foreach ($cust_classes as $cust_class) {
                                                    $lv_id = $cust_class->id; ?>
                                                    <option <?php if(isset($_GET['lead_cust_class'])&& in_array($lv_id, $_GET['lead_cust_class'])){echo 'selected';} ?> value="<?=$lv_id; ?>"><?= $cust_class->type ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <button type="submit"  id = "search" class="btn btn-primary" >Search</button>
                                    </div>
                                    <input type="text" style="display: none;" name="graphType" id = "text2" value="bytype">
                                    <input type="text" style="display: none;" name="graphType1" id = "text3" value="byuser">
                            </form>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 xl12 chart1 raw_data_chart">
                                <h4 class="card-title">Score Card</h4>
                                <div class="graph_container" name="graphsize">
                                    <canvas id="score_chart"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 xl12 typechart raw_data_chart">
                                <h4 class="card-title">Score Card</h4>
                                <div class="graph_container" name="graphsize">
                                    <canvas id="score_By_Type"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="NoOrder"  class="col-lg-12 col-xs-12" style="<?php if(!empty($_GET['searchtype'])) {if($_GET['searchtype'] == "no_order"){echo 'display:block';}else{echo 'display:none';}}?>">
                        <div class="divider"></div>
                        <pre></pre>
                        <div class="Call_form">
                            <form method="get" action="" id="commsearchForm1">
                                <div class="col s12 m12 x12">
                                    <div class="input-field col s4 m4">
                                        <i class="material-icons prefix"  >Type</i>
                                        <div class="col s12 m12">
                                            <label>BY Type</label>
                                            <select class="browser-default" name="type" id="type1">
                                                <option  <?php if(isset($_GET['type'])&& $_GET['type'] == "comwise"){echo 'selected';} ?> value="comwise">Communication Wise</option>
                                                <option <?php if(isset($_GET['type'])&& $_GET['type'] == "leadwise"){echo 'selected';} ?> value="leadwise">Lead Wise</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s4 m4">
                                        <i class="material-icons prefix" >supervisor_account</i>
                                        <div class="col s12 m12" style="margin-top: 8px">
                                            <label>BY Designation</label>
                                            <select class="browser-default designation" id="designation1" name="desigantion[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                                <option value=""> Select Designation</option>
                                                <?php if(!empty($designations)){ foreach ($designations as $designation){ ?>
                                                    <option <?php if(isset($_GET['desigantion'])&& in_array('des'.$designation->id, $_GET['desigantion'])){echo 'selected';} ?> value="<?='des'.$designation->id?>"> <?=$designation->name?> </option>
                                                <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix" >date_range</i>
                                        <label for="">Date Range
                                            <input type="text" id="date_range2" name="no_order_dates">
                                        </label>
                                        <input type="hidden" name="start2" id="date_start2" value=""/>
                                        <input type="hidden" name="end2" id="date_end2" value=""/>
                                    </div>
                                    <div class="col s4 m4" >
                                        <i class="material-icons prefix" >format_list_bulleted</i>
                                        <div class="col s12 m12">
                                            <label>Select Category</label>
                                            <select class="browser-default category" id="category" name="category[]" multiple  tabindex="-1">
                                                <?php $i=0;
                                                if(!empty($customer_category)){
                                                    foreach ($customer_category as $cate) {
                                                        $c_id = $cate->id;
                                                        $selected = '';
                                                        ?>
                                                        <option <?php if(isset($_GET['category'])&& in_array($c_id, $_GET['category'])){echo 'selected';} ?> value="<?= $c_id; ?>" <?=$selected?>><?= $cate->type ?></option>
                                                        <?php
                                                        $i++;
                                                    } }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix">dns</i>
                                        <div class="col s12 m12">
                                            <label>Select Customer Class</label>
                                            <select class="browser-default customer_class" id="customer_class" name="cust_class[]" multiple tabindex="-1">
                                                <?php if(!empty($cust_classes)){ foreach ($cust_classes as $cust_class) {
                                                    $v_id = $cust_class->id; ?>
                                                    <option <?php if(isset($_GET['cust_class'])&& in_array($v_id, $_GET['cust_class'])){echo 'selected';} ?> value="<?=$v_id; ?>"><?= $cust_class->type ?></option>
                                                <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix">dns</i>
                                        <div class="col s12 m12">
                                            <label>Select User</label>
                                            <select class="browser-default user_filter" id="user_filter" name="user_filter[]" multiple tabindex="-1">
                                                <?php if(!empty($userfilter)){ foreach ($userfilter as $usr) {
                                                    $u_id = $usr->u_id; ?>
                                                    <option <?php if(isset($_GET['user_filter'])&& in_array($u_id, $_GET['user_filter'])){echo 'selected';} ?> value="<?=$u_id; ?>"><?=$usr->u_name; ?></option>
                                                <?php } }?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col s2 m2">
                                        <button type="submit" class="btn btn-primary" >Search</button>
                                    </div>
                                    <input type="text" style="display: none;" name="searchtype" id = "searchtype" value="no_order">
                            </form>
                            <div class="row">
                                <div class="col s12 m12 xl12 commtypechart raw_data_chart">
                                    <h4 class="card-title">Score Card</h4>
                                    <div class="graph_container" name="graphsize">
                                        <canvas id="comm_score_By_Type"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 xl12 leadtypechart raw_data_chart">
                                    <h4 class="card-title">Score Card</h4>
                                    <div class="graph_container" name="graphsize">
                                        <canvas id="lead_score_By_Type"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="MonthOrder" class="col-lg-12 col-xs-12">
                        <div class="divider"></div>
                        <div class="visits_form">
                            <form method="get" action="" id="searchForm2">
                                <div class="col s12 m12">
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix">dns</i>
                                        <div class="col s12 m12">
                                            <label>Select User</label>
                                            <select class="browser-default user_filter" id="user" name="user[]" multiple tabindex="-1">
                                                <option value="">Select User</option>
                                                <?php if(!empty($userfilter)){ foreach ($userfilter as $usr) {
                                                    $ex_id = isset($_GET['user'])?$_GET['user']:'';
                                                    $u_id = $usr->u_id; ?>
                                                    <option <?php if(!empty($ex_id) && in_array($u_id,$ex_id)){ echo 'selected'; }?> value="<?=$u_id; ?>"><?=$usr->u_name; ?></option>
                                                <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix">flag</i>
                                        <div class="col s12 m12">
                                            <label>Select State</label>
                                            <select  class="browser-default states state_list" name="state[]" multiple tabindex="-1" style="width: 100%" >
                                                <option value="">Select State</option>
                                                <?php
                                                if(!empty($states)){
                                                    foreach ($states as $state){
                                                        ?>
                                                        <option <?php if(isset($_GET['state'])&& in_array($state->id,$_GET['state'])){echo 'selected';} ?> value="<?=$state->id;?>"><?=$state->name?></option>
                                                        <?php
                                                    } }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix">adjust</i>
                                        <div class="col s12 m12">
                                            <label>Select District</label>
                                            <select  class="browser-default district dist_list" name="district[]" multiple tabindex="-1" style="width: 100%" >

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix" style="margin-top: 10px">dns</i>
                                        <div class="input-field col s12 m12">
                                            <label>Verified</label>
                                            <select name="verified" id="verified" class="browser-default ptype" > type
                                                <option value="" >Select Option</option>
                                                <option <?php if(isset($_GET['verified']) && $_GET['verified'] == '1'){ echo 'selected'; }  ?> value="1">Verified</option>
                                                <option <?php if(isset($_GET['verified']) && $_GET['verified'] == '2'){ echo 'selected'; } ?> value="2">Unverified</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix">format_list_bulleted</i>
                                        <div class="col s12 m12">
                                            <label>Select Category</label>
                                            <select class="browser-default monthcategory" id="monthcategory" name="monthcategory[]" multiple  tabindex="-1">
                                                <?php $i=0;
                                                foreach ($customer_category as $cate) {
                                                    $c_id = $cate->id;
                                                    $selected = ''; ?>
                                                    <option <?php if(isset($_GET['monthcategory'])&& in_array($cate->id, $_GET['monthcategory'])){echo 'selected';} ?> value="<?= $c_id; ?>" <?=$selected?>><?= $cate->type ?></option>
                                                    <?php $i++; } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s4 m4">
                                        <i class="material-icons prefix">format_list_bulleted</i>
                                        <div class="col s12 m12">
                                            <label>Managed By</label>
                                            <select class="browser-default managedby" id="managedby" name="managedby[]" multiple  tabindex="-1">
                                                <?php $i=0;
                                                foreach ($customer_desgnation as $deg) {
                                                    $selected = ''; ?>
                                                    <option <?php if(isset($_GET['managedby'])&& in_array('des'.$deg->id, $_GET['managedby'])){echo 'selected';} ?> value="<?= 'des'.$deg->id; ?>" <?=$selected?>><?= $deg->name ?></option>
                                                    <?php $i++; } ?>
                                                    <option>--------------------------</option>
                                                    <?php foreach ($manageuser as $user){
                                                        ?>
                                                        <option <?php if(isset($_GET['managedby'])&& in_array($user->u_id, $_GET['managedby'])){echo "selected";} ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s6 m6">
                                        <i class="material-icons prefix" >date_range</i>
                                        <label for="">Start Month
                                            <input type="text" id="startmonth" name="startmonth" value="<?= isset($_GET['startmonth'])?$_GET['startmonth']:date('01-Y')?>">
                                        </label>
                                    </div>
                                    <div class="col s6 m6">
                                        <i class="material-icons prefix" >date_range</i>
                                        <label for="">End Month
                                            <input type="text" id="endmonth" name="endmonth" value="<?= isset($_GET['endmonth'])?$_GET['endmonth']:date('m-Y') ?>">
                                        </label>
                                    </div>
                                </div>
                                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                                    <button type="submit" class="btn myblue waves-effect waves-light" style="padding:0 5px; margin-top: 5px" id="search1" ><i class="material-icons right" style="margin-left:3px">search</i>Search
                                        </button>
                                   <!-- <a class="btn myblue waves-effect waves-light" style="padding:0 5px; margin-top: 5px; float: right" href="<?/*=url('crm/update-month-order-records');*/?>"><i class="material-icons right" style="margin-left:3px">refresh</i>Refresh Data
                                    </a>-->
                                </div>
                                <input type="text" style="display: none;" name="searchtype" id = "searchtype1" value="month_order">
                            </form>

                            <div class="divider"></div>
                            <h6>All Month Order</h6>
                            <div class="row">
                                <div class="col s12 table-responsive">
                                    <table id="month_table" class="display" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th>Sr.No.</th>
                                            <th>Name</th>
                                            <th>Managed By</th>
                                            <?php if(!empty($months)){
                                                foreach($months as $key=>$month) {
                                                    $nmonth = explode('-',$month);
                                                    $month_name = date("F", mktime(0, 0, 0, $nmonth[1], 10));
                                                    ?>
                                                    <th><?php echo $month_name; ?></th>
                                                <?php } } ?>
                                            <th>Grand Total</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--//Model-->
<div id="open-modal" class="modal-window">
    <div class="modal-dialog" style="width: 405px !important;padding-left: 60px;">
        <div class="modal-content">
            <div>
                <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
            </div>
            <div>
                <table border="0">
                    <div class="modal-body">
                        <div class="col-md-8 offset-md-2">
                            <div class="row">
                                <button type="button" id="left" onclick="left();" class="btn btn-light bttn1 mr-3"><i class="fa fa-arrow-circle-left"></i></button>
                                <button type="button" id="year" value="2020" class="btn btn-light yrbrn"></button>
                                <button type="button" id="right" onclick="right();" class="btn btn-light bttnl ml-3"><i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                            <div class="row pt-3">
                                <button type="button" id="1" value="Jan" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Jan</button>
                                <button type="button" id="2" value="Feb" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Feb</button>
                                <button type="button" id="3" value="Mar" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Mar</button>
                            </div>
                            <div class="row pt-3">
                                <button type="button" id="4" value="Apr" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Apr</button>
                                <button type="button" id="5" value="May" class="btn btn-light btnwd mr-3" onclick="month(this.id);">May</button>
                                <button type="button" id="6" value="Jun" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Jun</button>
                            </div>
                            <div class="row pt-3">
                                <button type="button" id="7" value="Jul" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Jul</button>
                                <button type="button" id="8" value="Aug" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Aug</button>
                                <button type="button" id="9" value="Sep" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Sep</button>
                            </div>
                            <div class="row pt-3">
                                <button type="button" id="10" value="Oct" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Oct</button>
                                <button type="button" id=11 value="Nov" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Nov</button>
                                <button type="button" id="12" value="Dec" class="btn btn-light btnwd mr-3" onclick="month(this.id);">Dec</button>
                            </div>
                            <div class="row pt-5 pl-5 ml-5">
                                <button type="button" id="submit" value="submit" class="btn btn-danger btnsub text-center" onclick="submit();">Submit</button>
                            </div>
                        </div>
                    </div>
                </table>
            </div>
        </div>
    </div>
</div>
<!--Model End-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/chartjs/chart.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/form-layouts.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>

<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="<?= $tp1; ?>/js/bootstrap-datetimepicker.min.js"></script>

</body>

</html>
<script type="text/javascript">
    $('#startmonth,#endmonth').datetimepicker({
        viewMode: 'years',
        icons:
            {
                next: 'fa fa-angle-right',
                previous: 'fa fa-angle-left'
            },
        format: 'MM-YYYY'
    });

    function checkInput(){
        $("#checkdate").val('1')
    }
</script>
<script>
    $('.typechart').hide();
    $('.leadtypechart').hide();
</script>
<?php
if(!empty($_GET['type']) && !empty($_GET['graphType'])) {
    if($_GET['type'] == $_GET['graphType']){ ?>
        <script>
            $('.typechart').show();
            $('.chart1').hide();
        </script>
    <?php }
}

if(!empty($_GET['type'])) {
    if($_GET['type'] == 'leadwise'){ ?>
        <script>
            $('.leadtypechart').show();
            $('.commtypechart').hide();
        </script>
        <?php

    }
}


?>
<script>

    $(function() {

        $('#month_table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: "<?=url("crm/get-month_order-data") ?>",
                type: 'GET',
                data: {startmonth:'<?=isset($_GET['startmonth']) ? $_GET['startmonth'] : date('01-Y') ?>',
                    endmonth:'<?=isset($_GET['endmonth']) ? $_GET['endmonth'] : date('m-Y') ?>',
                    state:'<?=isset($_GET['state']) ? implode(',',$_GET['state']) : false ?>',
                    district:'<?=isset($_GET['district']) ? implode(',',$_GET['district']) : false ?>',
                    user:'<?=isset($_GET['user']) ? implode(',',$_GET['user']) : false ?>',
                    verified:'<?=isset($_GET['verified']) ? $_GET['verified'] : false ?>',
                    category:'<?=isset($_GET['monthcategory']) ? implode(',',$_GET['monthcategory']) : false ?>',
                    managedby:'<?=isset($_GET['managedby']) ? implode(',',$_GET['managedby']) : false ?>'
                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name'},
                { data: 'managed_by', name: 'managed_by'},
                <?php if(!empty($months)){
                foreach($months as $key=>$month) {
                $nmonth = explode('-',$month);
                $colums = count($nmonth);
                $month_name = date("F", mktime(0, 0, 0, $nmonth[1], 10));
                ?>
                { data: '<?php echo $month_name; ?>', name: '<?php echo $month_name; ?>',searchable: false},
                <?php } } ?>
                { data: 'grand_total', name: 'grand_total',searchable: false},
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            order: [['<?= $totalmonths + 3; ?>', 'desc']],

        });
    });
    var ctx = document.getElementById("score_chart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: <?=json_encode($data['user_label']);?>,

            datasets: [{
                label: 'Call',
                data:  <?=json_encode($data['calls']);?>,
                backgroundColor: "rgba(57,73,171,0.50)",
                borderColor: 'rgb(57,73,171)',
                borderWidth: 1,

            },{
                label: 'Lead',
                data: <?=json_encode($data['leads']);?>,
                backgroundColor: "rgba(255,255,0,0.50)",
                borderColor: 'rgb(255,255,0)',
                borderWidth: 1,
            },{
                label: 'Order',
                data: <?=json_encode($data['orders']);?>,
                backgroundColor: "rgba(0,128,0,0.50)",
                borderColor: 'rgb(0,128,0)',
                borderWidth: 1,
            }]
        },

        options: {
            title:{
                display:true,
                text:"Score Card"
            },
            scales: {
                xAxes: [{
                    id: 'top-x-axis',
                    type: 'linear',
                    position: 'top',
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true,
                }]
            }
        }
    });

    var options = {
        scales: {
            xAxes: [{
                id: 'top-x-axis',
                type: 'linear',
                position: 'top',
                gridLines: {
                    offsetGridLines: true
                }
            }],
            yAxes: [{
                stacked: true,
                barPercentage: 0.4
            }],

        }
    };
    var ctx1 = document.getElementById("score_By_Type").getContext('2d');
    var myChart1 = new Chart(ctx1, {
        type: 'horizontalBar',
        data: {
            labels: <?=json_encode($data['typelebel']);?>,
            datasets: [{
                label: 'Call',
                data:  <?=json_encode($data['totalCalls']);?>,
                backgroundColor: "rgba(57,73,171,0.50)",
                borderColor: 'rgb(57,73,171)',
                borderWidth: 1,

            },{
                label: 'Lead',
                data: <?=json_encode($data['totalLead']);?>,
                backgroundColor: "rgba(255,255,0,0.50)",
                borderColor: 'rgb(255,255,0)',
                borderWidth: 1,
            },{
                label: 'Order',
                data: <?=json_encode($data['totalOrder']);?>,
                backgroundColor: "rgba(0,128,0,0.50)",
                borderColor: 'rgb(0,128,0)',
                borderWidth: 1,
            }]
        },

        options: {
            scales: {
                xAxes: [{
                    id: 'top-x-axis',
                    type: 'linear',
                    position: 'top'
                }]
            }
        }
    });

    var ctx3 = document.getElementById("lead_score_By_Type").getContext('2d');
    var myChart3 = new Chart(ctx3, {
        type: 'horizontalBar',
        data: {
            labels: <?=json_encode($data['reason_label']);?>,
            datasets: [{
                data: <?=json_encode($data['leadtotal']);?>,
                backgroundColor: "rgba(0,128,0,0.50)",
                borderColor: 'rgb(0,128,0)',
                borderWidth: 1,
            }]
        },

        options: options,
    });
    var ctx4 = document.getElementById("comm_score_By_Type").getContext('2d');
    var myChart4 = new Chart(ctx4, {
        type: 'horizontalBar',
        data: {
            labels: <?=json_encode($data['reason_label']);?>,
            datasets: [{
                data: <?=json_encode($data['comtotal']);?>,
                backgroundColor: "rgba(0,128,0,0.50)",
                borderColor: 'rgb(0,128,0)',
                borderWidth: 1,
            }]
        },

        options: options,
    });

    $(document).ready(function(){

        $('.designation').select2();
        $('.category').select2();
        $('.serachcategory').select2();
        $('.monthcategory').select2();
        $('.managedby').select2();
        $('.customer_class').select2();
        $('.lead_customer_class').select2();
        $('.user_filter').select2();
        $('.state_list').select2();
        $('.dist_list').select2();

        var designation = $('.designation').val();
        var type = $("#type").val();

        var startDate1 = moment('<?=$start_date1?>').format('DD-MM-YYYY HH:mm:ss');
        var endDate1 = moment('<?=$end_date1?>').format('DD-MM-YYYY HH:mm:ss');
        var startDate2 = moment('<?=$no_order_start_date?>').format('DD-MM-YYYY HH:mm:ss');
        var endDate2 = moment('<?=$no_order_end_date?>').format('DD-MM-YYYY HH:mm:ss');
        $('#date_start1').val(startDate1);
        $('#date_end1').val(endDate1);
        $('#date_start2').val(startDate2);
        $('#date_end2').val(endDate2);
        $('#date_range1').daterangepicker({
            useCurrent: true,
            opens: 'left',
            locale: {
                format: 'DD-MM-YYYY'
            },
            startDate: startDate1,
            endDate: endDate1
        },  function(start1, end1, label) {
            $('#date_start1').val(start1.format('DD-MM-YYYY'));
            $('#date_end1').val(end1.format('DD-MM-YYYY'));

        });
        $('#date_range2').daterangepicker({
            useCurrent: true,
            opens: 'left',
            locale: {
                format: 'DD-MM-YYYY'
            },
            startDate: startDate2,
            endDate: endDate2
        },  function(start2, end2, label) {
            $('#date_start2').val(start2.format('DD-MM-YYYY'));
            $('#date_end2').val(end2.format('DD-MM-YYYY'));

        });

        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".district").html(data).append('<option value="" disabled selected>Select District</option>');
                }
            });
        });

    });
</script>
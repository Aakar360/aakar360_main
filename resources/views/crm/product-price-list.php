<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" href="<?=$tp; ?>/css/login/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/animate-css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/chartist-js/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/chartist-js/chartist-plugin-tooltip.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/dashboard-modern.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/intro.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">


    <!-- END: Custom CSS-->
</head>
<style type="text/css">

    .mybtn{

        background-color:#0d5f8c;
        border: 0px;
        border-radius:4px;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        font-size:16px;
        padding:9px 11px;
        text-decoration:none;
    }
    td, th {
        display: table-cell;
         padding: 0px 0px;
        text-align: left;
        vertical-align: middle;
        border-radius: 2px;
    }
    input[type=text]
    {
        text-align: center;
        margin: 0px;
        height: 20px;
        width: 100%;
        border: none;
    }
    .table-scroll {
        position:relative;
        max-width:100%;
        overflow:hidden;
        border:1px solid #aab2ae;
    }
    .table-wrap {
        width:100%;
        overflow:auto;
    }
    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }
    .table-scroll th, .table-scroll td {
        border:1px solid #d8d3d8;
        background:#fff;
        text-align:center;
        overflow-wrap: break-word;
    }
    .table-scroll thead, .table-scroll tfoot {
        background:#f9f9f9;
    }
    .clone {
        position:absolute;
        top:0;
        left:0;
        pointer-events:none;
    }
    .clone th, .clone td {
        visibility:hidden
    }
    .clone td, .clone th {
        border: 1px solid black;
    }
    .clone tbody th {
        visibility:visible;
        color:red;
    }
    .clone .fixed-side {
        border:1px solid #aab2ae;
    //background:#eee;
        visibility:visible;
    }
    .clone thead, .clone tfoot{background:transparent;}
</style>
<!-- END: Head-->
<?=$header;?>
<div class="row brand_wise">
    <div class="col s12 ">
        <div class="card hoverable">
            <div class="card-content">
        <div class="col s12">
                <h5 class="card-title" style="padding:5px;color: #0d1baa;;">Price Entry</h5>

                <form method="post">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">

                    <div class="col s12 m12">
                        <div class="col s1 m1" style="text-align: center; margin-top: 20px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
                        <div class="col s10 m10">
                        <select class="browser-default category" name="category" tabindex="-1">
                            <option value="">Select Category</option>
                            <?php foreach ($category as $cate) {
                                ?>
                                <option <?php /*if ($cid==$cate->id) { echo 'selected'; } else { echo ''; } */?>
                                        value="<?= $cate->id; ?>"><?= $cate->name ?></option>
                                <?php
                            } ?>
                        </select>
                        <input type="hidden" name="cat_name" id="cat_name">
                        </div>
                    </div>

                    <div class="col s12 m12" style="text-align: center">
                        <button style="margin-top: 20px;" class="btn waves-effect waves-light myblue submit" type="submit" name="category_find">
                            <i class="material-icons right">search</i>Search
                        </button>
                    </div>
                </form>

            </div>


            <?=$notices;?>
                <?php if(!empty($pro1)){ ?>

                                <h4 class="card-title">Products of <?=$selected_category?>
                                </h4>
                                <div class="row ">
                                    <div id="table-container" class="col s12" >
                                        <div id="table-scroll" class="table-scroll">
                                            <div class="table-wrap">
                                        <table id="maintable" name="desktop" class="table-responsive-sm browser-default table-hover" >
                                            <thead style="background: #91ddb9;">
                                                <th class="">Sr.No.</th>
                                                <th class="">Product Title</th>
                                                <th>Purchase Price</th>
                                                <th class="">Sr.No.</th>
                                                <th class="">Product Title</th>
                                                <th>Purchase Price</th>
                                            </thead>
                                            <tbody class="content" id="pridata">
                                            <form action="product-price-list" method="post">
                                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                            <?php $i=1;  foreach ($pro1 as $variant) { if(($i % 2) != 0) { ?>
                                                <tr>
                                                    <td class=""><?= $i; ?></td>
                                                    <td><?= $variant->title ?></td>
                                                    <td>
                                                        <input type="hidden" name="pid[]" value="<?= $variant->id ?>">
                                                        <input class="browser-default" type="text" name="new_price[]"
                                                               value="<?= $variant->purchase_price ?>">
                                                        <!--<button type="button" name="addprice" class="btn waves-effect waves-light right aprice" value="<?/*=$variant->id; */ ?>"> Add</button>-->
                                                    </td>

                                                <?php }if(($i % 2) == 0) {?>

                                                    <td class=""><?=$i; ?></td>
                                                    <td><?= $variant->title ?></td>
                                                    <td>
                                                        <input type="hidden" name="pid[]" value="<?= $variant->id ?>">
                                                        <input class="browser-default" type="text" name="new_price[]"
                                                               value="<?= $variant->purchase_price ?>">
                                                        <!--<button type="button" name="addprice" class="btn waves-effect waves-light right aprice" value="<?/*=$variant->id; */ ?>"> Add</button>-->
                                                    </td>
                                                </tr>

                                                <?php }
                                                $i++; }
                                            ?>
                                            <tr>
                                                <td colspan="10">
                                                    <button class="btn btn-margin-sms waves-effect waves-light myblue submit" type="submit" name="price_save">SAVE<i class="material-icons ">save</i>
                                                    </button>
                                                </td>
                                            </tr>
                                            </form>
                                            </tbody>
                                        </table>

                                                <table id="maintable" name="mobile" class="table-responsive-sm browser-default table-hover" >
                                                    <thead style="background: #91ddb9;">
                                                    <th class="">Sr.No.</th>
                                                    <th class="">Product Title</th>
                                                    <th>Purchase Price</th>
                                                    </thead>
                                                    <tbody class="content" id="pridata">
                                                    <form action="product-price-list" method="post">
                                                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                                        <?php $i=1;  foreach ($pro1 as $variant) {?>

                                                            <td class=""><?=$i; ?></td>
                                                            <td><?= $variant->title ?></td>
                                                            <td>
                                                                <input type="hidden" name="pid[]" value="<?= $variant->id ?>">
                                                                <input class="browser-default" type="text" name="new_price[]"
                                                                       value="<?= $variant->purchase_price ?>">
                                                                <!--<button type="button" name="addprice" class="btn waves-effect waves-light right aprice" value="<?/*=$variant->id; */ ?>"> Add</button>-->
                                                            </td>
                                                            </tr>

                                                        <?php
                                                            $i++; }
                                                        ?>
                                                        <tr>
                                                            <td colspan="3">
                                                                <button class="btn btn-margin-sms waves-effect waves-light myblue submit" type="submit" name="price_save">SAVE<i class="material-icons ">save</i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    </form>
                                                    </tbody>
                                                </table>
                                                <div id="bottom_anchor"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>

        </div>



<!-- END: Footer-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/chartjs/chart.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/chartist-js/chartist.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/chartist-js/chartist-plugin-tooltip.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/chartist-js/chartist-plugin-fill-donut.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/dashboard-modern.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/form-elements.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function() {
        // This will fire when document is ready:
        $(window).resize(function() {
            // This will fire each time the window is resized:
            if($(window).width() >= 480) {
                // if larger or equal
                var tablemobile = document.getElementsByName("mobile");
                var tabledesktop = document.getElementsByName("desktop");
                $(tabledesktop).show();
                $(tablemobile).hide();

            } else {
                var tablemobile = document.getElementsByName("mobile");
                var tabledesktop = document.getElementsByName("desktop");
                // if smaller
                $(tablemobile).show();
                $(tabledesktop).hide();
            }
        }).resize(); // This will simulate a resize to trigger the initial run.
    });

    $(document).ready(function(){

        /*$('.aprice').click(function() {
            var proid = $(this).val();
            var new_price = $(this).closest("td").find('.inprice').val();
            $.ajax({
                type: "POST",
                url: "add-product-price",
                data:'_token=<csrf_token();?>&product_id='+proid+'&new_price='+new_price,
                success: function(data){
                    alert("Updated Successfully");
                }
            });
        });*/


        function moveScroll(){
            var scroll = $(window).scrollTop();
            var anchor_top = $("#maintable").offset().top;
            var anchor_bottom = $("#bottom_anchor").offset().top;
            if (scroll>anchor_top && scroll<anchor_bottom) {
                clone_table = $("#clone");
                if(clone_table.length == 0){
                    clone_table = $("#maintable").clone();
                    clone_table.attr('id', 'clone');
                    clone_table.css({position:'fixed',
                        'pointer-events': 'none',
                        top:65});
                    clone_table.width($("#maintable").width());
                    $("#table-container").append(clone_table);
                    $("#clone").css({visibility:'hidden'});
                    $("#clone thead").css({visibility:'visible'});
                }
            } else {
                $("#clone").remove();
            }
        }
        $(window).scroll(moveScroll);

        jQuery("#maintable").clone(true).appendTo('#table-scroll').addClass('clone');
    });
</script>

</body>

</html>
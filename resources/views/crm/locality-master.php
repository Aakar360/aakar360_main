<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <!--<link rel="stylesheet" type="text/css" href="<? //$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />



    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <?=$notices;?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Locality Master</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light" style="padding:0 5px;"  href="locality-master?add">
                        <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                    </a>
                </div>
                <div class="row" style="position:relative;">
                    <div class="col s12 table-responsive">
                        <table id="table" class="display">
                            <thead>
                            <tr role="row">
                                <th>Sr.No.</th>
                                <th>District</th>
                                <th>Blocks</th>
                                <th>Locality</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                                
                    </div>
                </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Area / Locality
                        <a class="btn myblue waves-light right" href="locality-master" >
                            <i class="material-icons right">add_circle_outline</i>Go Back
                        </a>
                    </h5>
                    <form method="post" action="locality-master" class="col s12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m4">

                                <i class="material-icons prefix" style="margin-top: 10px">location_searching</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <label for="district">Select District</label>
                                    <select  class="browser-default" name="district" required tabindex="-1" style="width: 100% !important;" id="district">
                                        <?php foreach ($districts as $district){
                                            ?>
                                            <option value="<?=$district->id?>"> <?=$district->name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="input-field col s12 m4">

                                <i class="material-icons prefix" style="margin-top: 10px">location_searching</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <label for="district">Select Block</label>
                                    <select  class="browser-default" name="block" required tabindex="-1" style="width: 100% !important;" id="block">
                                        <?php foreach ($blocks as $block){
                                            ?>
                                            <option value="<?=$block->id?>"> <?=$block->block?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="input-field col s12 m4" style="margin-top: 27px">
                                <i class="material-icons prefix">location_on</i>
                                <input id="locality" type="text" class="validate" name="locality">
                                <label for="locality">Area / Locality Name</label>
                            </div>
                        </div>
                        <button class="btn myblue waves-light right add_submit" type="submit" name="add">Add
                            <i class="material-icons right">save</i>
                        </button>

                    </form>
                <?php } ?>
                <?php if(isset($_GET['edit'])){
                    $lid = $_GET['edit'];
                    $locality = DB::table("locality")->where('id', $lid)->first();
                   // dd($get_dist);
                    ?>

                    <h5>Edit Area / Locality</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="locality-master" >
                            <i class="material-icons right">home</i>Home
                        </a>
                    </div>
                    <form method="post" action="locality-master" class="col s12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <input type="hidden" name="lid" value="<?php echo $lid; ?>">
                        <div class="row">
                            <div class="input-field col s12 m4">

                                <i class="material-icons prefix" style="margin-top: 10px">location_searching</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <label for="district">Select District</label>
                                    <select  class="browser-default" name="district" required tabindex="-1" style="width: 100% !important;" id="district">
                                        <?php foreach ($districts as $district){
                                            ?>
                                            <option value="<?=$district->id?>" <?=($district->id == $locality->district_id) ? 'selected' : ''; ?>> <?=$district->name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="input-field col s12 m4">

                                <i class="material-icons prefix" style="margin-top: 10px">location_searching</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                    <label for="district">Select Block</label>
                                    <select  class="browser-default" name="block" required tabindex="-1" style="width: 100% !important;" id="block">
                                        <?php foreach ($blocks as $block){
                                            ?>
                                            <option value="<?=$block->id?>" <?=($block->id == $locality->block_id) ? 'selected' : ''; ?>> <?=$block->block?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="input-field col s12 m4" style="margin-top: 27px">
                                <i class="material-icons prefix">location_on</i>
                                <input id="locality" type="text" class="validate" name="locality" value="<?=$locality->locality?>">
                                <label for="locality">Area / Locality Name</label>
                            </div>

                        </div>
                        <button class="btn myblue waves-light right add_submit" type="submit" name="edit">Update
                            <i class="material-icons right">save</i>
                        </button>

                    </form>
                <?php } ?>
            </div>
        </div>
    </div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>
<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-locality") ?>",
                type: 'GET',
                data: function (d) {

                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'district_name', name: 'district.name' },
                { data: 'block', name: 'block_id' },
                { data: 'locality', name: 'locality' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            order: [[0, 'desc']],
            dom: 'lBfrtip',
            buttons: <?=$buttons;?>,
            fixedColumns: true,
            colReorder: true,
            exportOptions:{
                columns: ':visible'
            }
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('#table').attr('style', 'width: 100%');
        $('#district').select2();
        $('.state_list').change(function () {
            var sid = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district",
                data:'sid='+sid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.dist_list').html(data);
                }
            });
        });
    })

    $('.valid_no').blur(function () {
        var a = $(this).val();
        var b = $(this).val().length;
        var c = a.indexOf(",");
       if(c==-1){
           if(b==10){
               $('.add_submit').prop("disabled", false);
               $('.edit_submit').prop("disabled", false);
           }
           else{
               alert("Please Enter 10 digit no");
               $('.add_submit').prop("disabled", true);
               $('.edit_submit').prop("disabled", true);
           }
       }else{
           var str = a;
           var str_array = str.split(',');
           for(var i = 0; i < str_array.length; i++) {
               // Trim the excess whitespace.
               str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
               // Add additional code here, such as:
               var tot = str_array[i].length;
               if(tot!=10){
                   alert("Please Enter 10 digit number before and after comma");
                   $('.add_submit').prop("disabled", true);
                   $('.edit_submit').prop("disabled", true);
               }
               else{
                   $('.add_submit').prop("disabled", false);
                   $('.edit_submit').prop("disabled", false);
               }
           }
       }
    });

    $("input[name=contact_no]").keypress(function (e) {

        if (/\d+|,/i.test(e.key) ){

            console.log("character accepted: " + e.key)
        } else {
            console.log("illegal character detected: "+ e.key)
            return false;
        }

    });


    $(document).on('click','.locality_delete',function () {
        if (confirm("Are you sure?")) {
            var cid = $(this).val();
            $.ajax({
                type: "POST",
                url: "locality_delete",
                data:'lid='+cid+'&_token=<?=csrf_token(); ?>&_method=delete',
                success: function(data){
                    alert('Record Deleted Successfully');
                    location.reload();
                }
            });
        }
        else{

        }
        return false;
    });

</script>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- END: Custom CSS-->
    <style>
        .modal-window {
            position: fixed;
            background-color: rgba(200, 200, 200, 0.75);
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 999;
            opacity: 0;
            pointer-events: none;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
        }

        .modal-window:target {
            opacity: 1;
            pointer-events: auto;
        }

        .modal-window>div {
            position: relative;
            padding: 2px 0;
            background: #fff;
            color: #444;
            max-width: 500px;
            margin: 0px auto;
        }

        .modal-window header {
            font-weight: bold;
        }

        .modal-close {
            color: #aaa;
            line-height: 30px;
            font-size: 80%;
            position: absolute;
            right: 0;
            text-align: center;
            top: 0;
            width: 70px;
            text-decoration: none;
        }

        .modal-close:hover {
            color: #000;
        }

        .modal-window h1 {
            font-size: 100%;
            margin: 5px;
        }
        #map{
            height: calc(100vh - 100px);
            width: 100%;
        }
    </style>
</head>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <h5 class="card-title" style="padding: 5px; color: #0d1baa;"><?=$title; ?></h5>
                <?=$notices;?>
               <center> <button name="new_add" id="show" onclick="showElement()" class="btn myblue waves-light">ADD NEW</button></center>
               <center> <button name="new_add" id="hide" onclick="hideElement()" style="display: none" class="btn myblue waves-light">ADD NEW</button></center>

                <div class="row" id="mysection" style="display: none">
                    <form method="post" action="" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">

                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">person</i>
                        <input id="name" type="text" name="name">
                        <label for="">Name</label>
                    </div>

                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">person</i>
                        <input id="company_name" type="text" name="company_name">
                        <label for="">Company Name</label>
                    </div>

                    <div class="input-field col s12 m4">
                        <i class="material-icons prefix">navigation</i>
                        <input id="location" type="text" name="location">
                        <label for="">Location</label>
                    </div>
                    <div class="col s12 m2">
                        <center><a href="#open-modal3" class="btn myblue waves-light" onclick="setinmap()" style="padding: 0 5px; width: fit-content;">Set in Map
                                <i class="material-icons right">location_on</i>
                            </a></center>
                        <input type="hidden" id="lat1" name="lat"/>
                        <input type="hidden" id="long1" name="lng"/>
                    </div>
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">list</i>
                        <input id="description" type="text" name="description">
                        <label for="">Description</label>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 m6">
                            <!--<input type="file" name="image" style="width: 100%" class="btn myblue" />-->
                            <div class="file-field">
                                <div class="btn myblue waves-light left" style="padding:0 5px;">
                                    <span>Choose file</span>
                                    <input type="file" name="image[]" style="padding:0 5px;" class="btn myblue" id="quick_image" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" placeholder="Upload your file">
                                </div>
                            </div>
                        </div>

                        <div class="input-field col s12 m3">
                            <p id="quick_img_div">
                                <img id="quick_image_show" src="#" height="200px" width="200px"  />
                            </p>
                        </div>
                        <div class="input-field col s12 m3" style="text-align: center">
                            <button class="btn myblue waves-light " style="padding:0 5px;" type="submit" name="add_quick_data">Save
                                <i class="material-icons right">save</i>
                            </button>
                        </div>
                    </div>
                    </form>

                </div>
                <div class="row" style="position:relative;">
                    <div class="col s12 table-responsive" ><!--1x 11661593 6YDgrn-->
                        <table id="table" class="responsive display">
                            <thead>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Company Name</th>
                            <th>Description</th>
                            <th>location</th>
                            <th>Image</th>
                            <th>Created By</th>
                            <th>Action</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="MyLatLng" value=""/>
    <?php
    $lat = '';
    $lng = '';
    if(isset($_GET['edit'])) {
        $lat = $get_cust->lat;
        $lng = $get_cust->lng;
    }
    ?>
    <input type="hidden" id="lat" value="<?=$lat;?>"/>
    <input type="hidden" id="long" value="<?=$lng;?>"/>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
</body>

</html>
<script type="text/javascript">
    function zoomin(){
        var myImg = document.getElementById("sky");
        var currWidth = myImg.clientWidth;
        if(currWidth == 500){
            alert("Maximum zoom-in level reached.");
        } else{
            myImg.style.width = (currWidth + 50) + "px";
        }
    }
    function zoomout(){
        var myImg = document.getElementById("sky");
        var currWidth = myImg.clientWidth;
        if(currWidth == 50){
            alert("Maximum zoom-out level reached.");
        } else{
            myImg.style.width = (currWidth - 50) + "px";
        }
    }
</script>
<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-quick_report-data") ?>",
                type: 'GET',
                data: function (d) {
                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'company_name', name: 'company_name' },
                { data: 'discription', name: 'discription', searchable: false },
                { data: 'location', name: 'location' },
                { data: 'image', name: 'image', searchable: false },
                { data: 'created_by', name: 'created_by', searchable: false },
                { data: 'action', name: 'action', orderable: false,searchable: false }
            ],
            order: [[0, 'desc']],
            dom: 'lBfrtip',
            buttons: <?=$buttons;?>,
            fixedColumns: true,
            colReorder: true,
            exportOptions:{
                columns: ':visible'
            }
        });
        $('#table').DataTable().draw(true);
    });

    $(document).on('click','#quickdelete',function () {
        if (confirm("Are you sure?")) {
            var pid = $(this).val();
            $.ajax({
                type: "POST",
                url: "quick-report-delete",
                data:'pid='+pid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    alert('Record Deleted Successfully');
                    $('#table').DataTable().ajax.reload();
                }
            });
        }
        else{
        }
        return false;
    });

</script>
<script type="text/javascript">
    function showElement() {
        element = document.querySelector('#mysection');
        element1 = document.querySelector('#show');
        element2 = document.querySelector('#hide');
        element.style.display = 'block';
        element1.style.display = 'none';
        element2.style.display = 'block';
    }

    function hideElement() {
        element = document.querySelector('#mysection');
        element1 = document.querySelector('#show');
        element2 = document.querySelector('#hide');
        element.style.display = 'none';
        element1.style.display = 'block';
        element2.style.display = 'none';
    }

    $('#quick_img_div').hide();
    function readURLTicket(input) {
        $('#quick_img_div').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#quick_image_show').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#quick_image").change(function(){
        readURLTicket(this);
    });
    var map, marker, infoWindow, title, contentString, directionsService, directionsDisplay, pos;

    function initMap() {
        directionsService = new google.maps.DirectionsService();
        directionsDisplay = new google.maps.DirectionsRenderer();
        var lat = 59.909144;
        var lng = 10.7436936;
        <?php if(isset($_GET['edit'])){ ?>
        lat = <?=empty($get_cust->lat) ?  59.909144 : $get_cust->lat; ?>;
        lng = <?=empty($get_cust->lng) ?  10.7436936 : $get_cust->lng; ?>;
        <?php } ?>
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: {
                lat: lat,
                lng: lng
            }
        });
        $('#reason').change(function() {
            var opval = $(this).val();
            if(opval=="5" || opval=="6"){
                document.getElementById('reason_field').style.visibility = 'visible';
            }else{
                document.getElementById('reason_field').style.visibility = 'hidden';
                $('#reason_value').val("NA");
            }
        });

        //CUSTOM MARKER ICON
        var image = {
            url: "<?=url('assets/css/images')?>/map-marker.png",
            scaledSize: new google.maps.Size(32, 32)
        };
        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            icon: image,
            animation: google.maps.Animation.DROP,
            position: {
                lat: lat,
                lng: lng
            }
        });

        //marker.addListener('click', toggleBounce);
        //END CUSTOM MARKER ICON
        <?php if(isset($_GET['edit'])){ ?>
        contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading"><?=$get_cust->name?></h1>'+
            '<div id="bodyContent">'+
            '<p><?=$get_cust->postal_address?></p>'+
            '</div>'+
            '</div>';
        title = '<?=$get_cust->name?>';

        // GET POSITION

        <?php }else{ ?>
        contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<div id="bodyContent">'+
            '<p>This is your current location. Hold and drag to the site location</p>'+
            '</div>'+
            '</div>';
        title = 'Your Current Location';
        // Try HTML5 geolocation.
        <?php
        }?>
        infoWindow = new google.maps.InfoWindow({
            content: contentString
        });
        <?php
        $currentLocation = true;
        if(isset($_GET['edit'])){
        if($get_cust->lat != '' && $get_cust->lng != ''){
        $currentLocation = false;
        ?>
        document.getElementById('lat').value = <?=$get_cust->lat?>;
        document.getElementById('long').value = <?=$get_cust->lng?>;
        <?php
        }
        }

        ?>
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                <?php if($currentLocation){ ?>
                marker.setPosition(pos);
                marker.setTitle(title);
                map.setCenter(pos);
                <?php } ?>
                document.getElementById('MyLatLng').value = pos.lat + ', '+ pos.lng;

            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
        //END GET POSITION
        google.maps.event.addListener(marker, 'mouseup', function(event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('long').value = event.latLng.lng();
            //alert(event.latLng.lat() + ", " + event.latLng.lng());
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
    }
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }
    var im = "<?=url('assets/css/images')?>/bluecircle.png";
    function locate(){
        navigator.geolocation.getCurrentPosition(initialize,fail);
    }

    function initialize(position) {
        var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var userMarker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: im
        });
        map.setCenter(myLatLng);
    }

    function fail(){
        alert('navigator.geolocation failed, may not be supported');
    }
    function saveLocation(){
        document.getElementById('lat1').value = document.getElementById('lat').value;
        document.getElementById('long1').value = document.getElementById('long').value;

    }
    function setinmap(){
        var pos = {
            lat: parseFloat(document.getElementById('lat1').value),
            lng: parseFloat(document.getElementById('long1').value)
        };
//        alert(pos);
        var pos1 = {
            lat: false,
            lng: false
        };
        <?php if(isset($_GET['edit'])){ ?>
        pos1 = {
            lat: parseFloat(<?=$get_cust->lat?>),
            lng: parseFloat(<?=$get_cust->lng?>)
        };
        <?php } ?>
        if(pos.lat && pos.lat) {
            marker.setPosition(pos);
            map.setCenter(pos);
        }else if(pos1.lat && pos1.lat){
            marker.setPosition(pos1);
            map.setCenter(pos1);
        }else{
            navigator.geolocation.getCurrentPosition(function(position){
                var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                marker.setPosition(myLatLng);
                map.setCenter(myLatLng);
            });
        }
    }
</script>
<div id="open-modal3" class="modal-window">
    <div style="margin-top: 13px;">
        <a href="#modal-close" title="Close" class="modal-close" style="color: #000;font-size: 13px;">close &times;</a>
        <h1>Set Location</h1>
        <div >
            <div id="map"></div>
        </div>
        <?php if(!$currentLocation){ ?>
            <div style="text-align: center; margin-top: 5px;margin-bottom: 5px;">
                <!--<a class="btn myblue" href="javascript:void(0);" onclick="calcRoute()">Get Direction</a>-->
                <a style="padding: 7px;padding-top: unset;height: 31px;" class="btn myblue" href="javascript:void(0);" onclick="navigate()">Navigate</a>
                <a style="padding: 7px;padding-top: unset;height: 31px;" class="btn myblue" href="javascript:void(0);" onclick="locate()">My Location</a>
                <a style="padding: 7px;padding-top: unset;height: 31px;" class="btn myblue" href="#modal-close" onclick="saveLocation()">Save</a>
            </div>
        <?php }else{ ?>
            <div style="text-align: center; margin-top: 5px; margin-bottom: 5px;">
                <!--<a class="btn myblue" href="javascript:void(0);" onclick="calcRoute()">Get Direction</a>-->
                <!--<a class="btn myblue" href="javascript:void(0);" onclick="navigate()">Navigate</a>-->
                <a class="btn myblue" href="javascript:void(0);" onclick="locate()">My Location</a>
                <a class="btn myblue" href="#modal-close" onclick="saveLocation()">Save</a>
            </div>
        <?php } ?>
    </div>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAB5amm0dLJi65cbZSQSGM3dZn4ctnO22Q&callback=initMap"></script>
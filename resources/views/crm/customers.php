<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <!--<link rel="stylesheet" type="text/css" href="<? //$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />


<style>
    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        position: relative;
        padding: 2px 0;
        background: #fff;
        color: #444;
        max-width: 500px;
        margin: 0px auto;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 30px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 100%;
        margin: 5px;
    }
    #map{
        height: calc(100vh - 100px);
        width: 100%;
    }

</style>
    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 8px 8px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <?=$notices;?>
                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>
                    <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Customer Master</h5>
                    <div class="row">
                            <div class="col s12 m6 xl3">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
                                <div class="col s10 m10">
                                <label>Select Category</label>
                                <select class="browser-default category" id="category" name="category[]" multiple  tabindex="-1">

                                    <?php
                                    foreach ($customer_category as $cate) {
                                        $c_id = $cate->id;
                                        ?>
                                        <option value="<?= $c_id; ?>"><?= $cate->type ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">format_list_bulleted</i></div>
                            <div class="col s10 m10">
                                <label>Select Acc Manager</label>
                                <select class="browser-default user" name="user[]" id ="user" multiple tabindex="-1" style="width: 100% !important;" > type
                                    <?php foreach ($users as $user){
                                        ?>
                                        <option value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                            <div class="col s12 m6 xl3">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">flag</i></div>
                                <div class="col s10 m10">
                                <label>Select State</label>
                                <select class="browser-default states" id="states" name="states[]" multiple  tabindex="-1">
                                    <?php
                                    foreach ($states as $state) {
                                        $v_id = $state->id;
                                        ?>
                                        <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="col s12 m6 xl3">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
                                    <div class="col s10 m10">
                                <label>Select District</label>
                                <select class="browser-default district" id="district" name="district[]" multiple  tabindex="-1">

                                </select>
                                    </div>
                            </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
                            <div class="col s10 m10">
                                <label>Select Block</label>
                                <select class="browser-default block" id="block" name="block[]" multiple  tabindex="-1">

                                </select>
                            </div>
                        </div>
                            <div class="col s12 m6 xl3">
                                <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
                                <div class="col s10 m10">
                                    <label>Select Area / Locality</label>
                                    <select class="browser-default locality" id="locality" name="locality[]" multiple  tabindex="-1">

                                    </select>
                                </div>
                            </div>
                        <div class="col s12 m4 xl3" id="create"><i class="material-icons prefix">date_range</i>
                            <div class="col s12 m12" style="margin-left: 20px;margin-top: -42px;">
                                <label for="date_range">Create Date Range
                                    <input type="text" id="date_range" name="dates" style="margin-top: -11px;">
                                </label>
                                <input type="hidden" id="date_start" value="<?=$std_date?>"/>
                                <input type="hidden" id="date_end" value="<?=$end_date?>"/>
                            </div>
                        </div>
                            <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                                <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px;" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                                </button>

                                <a class="btn myblue waves-light" style="padding:0 5px;"  href="customers?add">
                                    <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                                </a>

                            </div>

                        </div>
                    <div class="divider"></div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive">
                            <table id="table" class="display">
                                <thead>
                                    <tr role="row">
                                        <th>#</th>
                                        <th>Name</th>
                                        <th style="display:none">Sec Name</th>
                                        <th>Firm Name</th>
                                        <th>Category</th>
                                        <th>Contact No.</th>
                                        <th style="display:none">Sec. Contact No.</th>
                                        <th>Acc Manager</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <?php } ?>
                <?php
                $lat = null;
                $lng = null;
                if(isset($_GET['add'])){
                    $name ='';
                    $company_name='';
                    $contact ='';
                    $description ='';
                    $location='';
                    $p  = DB::table('crm_master_quick_entry')->where('id',isset($_GET['add']) ? $_GET['add'] : 0)->first();
                        if($p===null){
                            $name ='';
                            $company_name='';
                            $contact ='';
                            $description ='';
                            $location='';
                        }else {
                            $name = $p->name;
                            $company_name = $p->company_name;
                            $contact = $p->contact;
                            $description = $p->discription;
                            $location = $p->location;
                            $lat = $p->lat;
                            $lng = $p->lng;
                        }
                    ?>
                    <h6 class="card-title" style=" color: #0d1baa;">Add New Customer</h6>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="customers" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                </div>
                    <form method="post" action="customers" class="col s12 m12" id="addForm">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <input type="hidden" name="quick" value="<?=(isset($_GET['add']) ? $_GET['add'] : 0); ?>">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" value="<?=$name?>" class="validate" name="name">
                                <label for="Full_Name">Full Name</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">store</i>
                                <input id="Proprieter" type="text" class="validate" value="<?=$company_name?>" name="proprieter_name">
                                <label for="Proprieter">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_mail</i>
                                <input id="Email" type="text" class="validate" name="email">
                                <label for="Email">Email</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_phone</i>
                                <input id="Contact" type="text" class="validate valid_no" name="contact_no" value="<?=$contact;?>">
                                <label for="Contact">Contact No.</label>
                            </div>
                        </div>
                       <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_mail</i>
                                <select  class="form-control" name="desg"  style="width: 100%" >
                                    <option>Select Designation</option>
                                    <?php if(!empty($crm_contax)) { foreach ($crm_contax as $des){
                                        ?>
                                        <option value="<?=$des->id ?>"> <?=$des->name?> </option>
                                        <?php
                                    } }
                                    ?>
                                </select>
                                <label for="desc">Contact Designation</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_phone</i>
                                <input id="landline" type="tel" class="validate" name="landline" value="" >
                                <label for="landline ">LandLine No.</label>
                            </div>
                            <div id="insertBefore"></div>
                            <div class="clearfix">
                            </div>
                            <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                Add More <i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <div class="row">
                            <input id="CID" type="hidden" class="validate" name="cid">
                            <div class="input-field col s12 m6" style="margin-top: 0px;">
                                    <i class="material-icons prefix" >person_add</i>
                                <div class="col s12 m12" style="margin-left: 23px">
                                    <?php if(!checkRole($user->u_id,"sel_usr")){
                                        ?>
                                        <label>U Can't Access This Section</label>
                                   <?php } else {?>
                                    <label>Managed By & Above</label>
                                    <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                        <?php foreach ($designation as $des){
                                            ?>
                                            <option value="group<?='des'.$des->id?>"> <?=$des->name?> </option>
                                            <?php
                                        }
                                        ?>
                                        <option>--------------------------------</option>
                                        <?php foreach ($users as $user){
                                            ?>
                                            <option value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_headline</i>
                                <select name="customer_category" class="cc" required> customer_category
                                    <option selected disabled value="" >Select Category</option>
                                    <?php
                                    foreach ($customer_category as $category){
                                        $s_id = $category->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$category->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="customer_category">Category </label>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_quilt</i>
                                <select name="type">
                                    <option value="" selected>Select Type</option>
                                    <?php
                                    foreach ($customer_type as $type){
                                        $s_id = $type->id;
                                        ?>
                                        <option  value="<?=$s_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="type">Type </label>


                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">flag</i>
                                <select  class="state_list " name="state"  style="width: 100%" required>
                                    <option value="" selected disabled>Select State</option>
                                    <?php
                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        ?>
                                        <option  <?php if(1 !=1) {
                                            echo "selected";
                                        } else {
                                            echo '';
                                        }?> value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="State">State</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6" >
                                    <i class="material-icons prefix">adjust</i>
                                <div class="col s12 m12" style="margin-left: 23px">
                                    <label>District</label>
                                <select  class="browser-default district dist_list" name="district" style="width: 100%" required>
                                    <option selected value="">District</option>
                                </select>
                                </div>
                            </div>
                            <div class="input-field col s12 m6" >
                                <i class="material-icons prefix">adjust</i>
                                <div class="col s12 m12" style="margin-left: 23px">
                                    <label>Block</label>
                                    <select  class="browser-default block block_list" name="block" id="block" style="width: 100%">
                                        <option selected value="">Block</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col s12 m4" >
                                <i class="material-icons prefix">location_on</i>
                                <div class="col s12 m12" style="margin-left: 23px">
                                    <label>Locality</label>
                                    <select  class="browser-default locality local_list" id="locality" name="locality" style="width: 100%">
                                        <option selected value="">Select Locality</option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-field col s12 m2">
                                <center><a href="#open-modal2" class="btn myblue waves-light" style="padding: 0 5px; width: fit-content;">Add New
                                        <i class="material-icons right">add_circle_outline</i>
                                    </a></center>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">navigation</i>
                                <input id="Address" type="text" class="validate" value="<?=$location?>" name="postal_address">
                                <label for="Address">Postal Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">Lat</i>
                                <input type="text" id="lat1" name="lat" value="<?=$lat?>"/>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">Lang</i>
                                <input type="text" id="long1" name="lng" value="<?=$lng?>"/>
                            </div>
                            <div class="col s12 m2">
                                <center><a href="#open-modal3" class="btn myblue waves-light" onclick="setinmap()" style="padding: 0 5px; width: fit-content;">Set in Map
                                        <i class="material-icons right">location_on</i>
                                    </a></center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">mail</i>
                                <select name="msg_active">
                                    <option value="0"> Yes</option>
                                    <option value="1"> No</option>
                                </select>
                                <label for="msg_active">Message Active</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">recent_actors</i>
                                <select name="msg_class" id="reason" required>
                                    <option selected disabled value="" >Select Class</option>

                                    <?php
                                    foreach ($customer_class as $class){
                                        $s_id = $class->id;
                                        ?>
                                        <option value="<?=$s_id;?>"><?=$class->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="msg_class">Customer Class</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <div class="col s12 m12" style="margin-left: 23px">
                                <label for="Full_Name">Payment Days</label>
                                <select class="browser-default payment_days" name="payment_days" tabindex="-1">
                                    <option value="">Select payment days</option>
                                    <option value="advance_payment">Advance payment</option>
                                    <?php
                                    for ($i=1; $i<=200; $i++)
                                    {
                                        ?>
                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="input-field col s12 m6" style="margin-top: 0px;">
                                <i class="material-icons prefix" >person_add</i>
                                <div class="col s12 m12" style="margin-left: 23px">
                                    <label>Select Association</label>
                                    <select class="browser-default association" name="association[]" multiple tabindex="-1">
                                        <option value="">Select Association</option>
                                        <?php
                                        foreach ($association as $type){
                                            $s_id = $type->id;
                                            ?>
                                            <option  value="<?=$s_id;?>"><?=$type->name?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="input-field col s12 m12" id="reason_field" style="visibility: hidden">
                                <i class="material-icons prefix">reply</i>
                                <input type="text" name="reason_value" id="reason_value" value = "NA" required>
                                <label for="Reason">Reason</label>
                            </div>
                            <div class="row category_price"></div>
                        </div>
                        <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                            <input type="hidden" id="goto" name="goto" value="0"/>
                            <button class="btn myblue waves-light add_submit" style="padding:0 5px;" type="submit" name="add">Add
                                <i class="material-icons right">save</i>
                            </button>
                            <button class="btn myblue waves-light add_submit " style="padding:0 5px; margin-top: 10px;margin-bottom: 10px" type="button" name="add_profile" id="add_profile">Add & Goto Profile
                                <i class="material-icons right">account_circle</i>
                            </button>
                        </div>
                    </form>
                <?php }?>
                <?php if(isset($_GET['edit'])){
                    $cid = $_GET['edit'];
                    $get_cust = \App\CrmCustomer::where('id',$cid)->first();
					$permission = checkRole($luser->u_id,"verify_cust");
					//dd($luser);
                    ?>
                    <h5>Edit Customer <?php if($permission && $get_cust->verified == 0){ echo '<a href="verify-customer?edit='.$_GET['edit'].'&action=1" class="btn myblue waves-light"><i class="material-icons left" style="margin-right: 5px">check</i> Verify</a>'; } if($get_cust->verified == 1){ echo '<span style="font-size: 16px;color: blue"><i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i> Verified</span>';if($permission) { echo '&nbsp;<a href="verify-customer?edit='.$_GET['edit'].'&action=0" class="btn myblue waves-light">Un-Verify</a>';} }?></h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="customers" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="customers" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <input type="hidden" class="ccid" name="cidd" value="<?php echo $cid; ?>">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name" value="<?=$get_cust->name;?>">
                                <label for="Full_Name">Full Name</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">store</i>
                                <input id="Proprieter" type="text" class="validate" name="proprieter_name" value="<?=$get_cust->proprieter_name;?>">
                                <label for="Proprieter">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_mail</i>
                                <input id="Email" type="text" class="validate" name="email" value="<?=$get_cust->email;?>">
                                <label for="Email">Email</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_phone</i>
                                <input id="Contact" type="tel" class="validate valid_no" name="contact_no" value="<?=$get_cust->contact_no;?>">
                                <label for="Contact">Contact No.</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_mail</i>
                                <select  class="state_list" name="desg"  style="width: 100%" >
                                    <option>Select Contact Designation</option>
                                    <?php if(!empty($crm_contax)) { foreach ($crm_contax as $des){
                                        ?>
                                        <option <?php if($get_cust->designation == $des->id){echo 'selected';}?> value="<?=$des->id?>"> <?=$des->name?> </option>
                                        <?php
                                    } }
                                    ?>
                                </select>
                                <label for="desg">Contact Designation</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">contact_phone</i>
                                <input id="landline" type="tel" class="validate" name="landline" value="<?=$get_cust->landline;?>">
                                <label for="landline">LandLine No.</label>
                            </div>
                        </div>
                            <div class="clearfix"> </div><br>
                           <?php  if(!empty($get_cust->sec_names)){
                               $names =unserialize($get_cust->sec_names);
                               $emails = unserialize($get_cust->sec_emails);
                               $contacts = unserialize($get_cust->sec_contact);
                               $desgs =unserialize($get_cust->sec_desc);
                               $landlines =unserialize($get_cust->sec_landline);
                               foreach ($names as $key=>$sec_name){ ?>
                               <div id="addMoreBox<?= $key; ?>" class="clearfix">
                               <div class="row">
                                    <div class="input-field col s6 m6">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input autocomplete="off" class="validate" id="names<?= $key; ?>" name="names[<?= $key; ?>]" type="text" value="<?= $sec_name;?>" placeholder="Full Name"/>
                                        <label for="names">Full Name</label>
                                    </div>
                                    <div class="input-field col s6 m6">
                                        <i class="material-icons prefix">contact_mail</i>
                                        <input autocomplete="off" class="validate" id="emails<?= $key; ?>" name="emails[<?= $key; ?>]" type="email" <?php if(!empty($emails[$key])){ ?> value="<?= $emails[$key]; }?>" placeholder="Email"/>
                                        <label for="emails">Email</label>
                                    </div>
                               </div>
                               <div class="row">
                                   <div class="input-field col s6 m6">
                                       <i class="material-icons prefix">contact_phone</i>
                                       <input autocomplete="off" class="validate valid_no" id="contacts<?= $key; ?>" name="contacts[<?= $key; ?>]" type="text" <?php if(!empty($contacts[$key])){ ?> value="<?= $contacts[$key]; }?>" placeholder="Contact No."/>
                                       <label for="contacts">Contact No.</label>
                                   </div>
                                   <div class="input-field col s6 m6">
                                       <i class="material-icons prefix">contact_phones</i>
                                       <input autocomplete="off" class="validate" id="landlines<?= $key; ?>" name="landlines[<?= $key; ?>]" type="tel" <?php if(!empty($landlines[$key])){ ?> value="<?= $landlines[$key]; } ?>" placeholder="Landline No" />
                                       <label for="landlines">Landline</label>
                                   </div>
                               </div>
                               <div class="row">
                                   <div class="input-field col s6 m6">
                                       <i class="material-icons prefix">contact_mail</i>
                                       <select class="getDesignation" id="desc<?= $key; ?>" name="descs[<?= $key; ?>]" style="margin-left: 25px">
                                           <option>Select Contact Designation</option>
                                           <?php foreach ($crm_contax as $desc): ?>
                                               <option <?php if(!empty($desgs[$key]) && $desgs[$key] == $desc->id){ echo 'selected';}?> value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>
                                           <?php endforeach; ?>
                                       </select>
                                       <label for="descs">Contact Designation</label>
                                   </div>
                                   <div class="input-field col s6 m6">
                                       <center>
                                           <button type="button" onclick="removeBox(<?= $key; ?>)" class="btn btn-sm btn-danger">Remove<i class="fa fa-times" style="margin-left:10px"></i></button>
                                       </center>
                                   </div>
                               </div>
                               </div>
                               <div class="clearfix"></div><br><br>
                           <?php  } }?>
                            <div id="insertBefore"></div>
                            <div class="clearfix"></div>
                            <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                Add More <i class="fa fa-plus"></i>
                            </button>

                        <div class="row">
                                <input id="CID" type="hidden" class="validate" name="cid">
                                <div class="input-field col s12 m6" style="margin-top: 0px;">
                                    <i class="material-icons prefix" style="margin-top: 10px">person_add</i>
                            <div class="col s12 m12" style="margin-left: 20px">
                                    Select User
                                    <select class="browser-default user" name="user[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                        <?php
                                        $u_id =  $get_cust->assign_users;
                                        $uid_array = explode(',',$u_id);
                                        foreach ($users as $user){
                                            ?>
                                            <option <?=(in_array($user->u_id,$uid_array))?'selected':''; ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                            </div>
                                </div>

                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_headline</i>
                                <select name="customer_category" class="cc1"> customer_category
                                    <?php
                                    foreach ($customer_category as $category){
                                        $s_id = $category->id;
                                        $old_cid = $get_cust->customer_category;
                                        ?>
                                        <option <?=($s_id==$old_cid)?'selected':'';?>  value="<?=$s_id;?>"><?=$category->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="Category">Category</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">view_quilt</i>
                                <select name="type" class="validate">
                                    <option value="">Select Type</option>
                                    <?php
                                    foreach ($customer_type as $type){
                                        $t_id = $type->id;
                                        $old_tid = $get_cust->type;
                                        ?>
                                        <option <?=($t_id==$old_tid)?'selected':'';?>   value="<?=$t_id;?>"><?=$type->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>

                                <label for="Type">Type</label>
                            </div>
<!--                        <div class="row">-->
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">flag</i>
                                <select  class="state_list" name="state"  style="width: 100%" required>
                                    <?php

                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        $old_sid = $get_cust->state;
                                        ?>
                                        <option <?=($s_id==$old_sid)?'selected':'';?>  value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label for="State">State</label>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12 m6" >

                                <i class="material-icons prefix">adjust</i>

                                <div class="col s12 m12" style="margin-left: 23px">
                                    District
                                    <select  class="browser-default district dist_list" name="district" id="district" style="width: 100%" required>
                                        <?php
                                        foreach ($selected_district as $sdist){
                                            $d_id = $sdist->did;
                                            $old_did = $get_cust->district;
                                            ?>
                                            <option <?=($d_id==$old_did)?'selected':'';?>  value="<?=$d_id;?>"><?=$sdist->d_name?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>

                            </div>

<!--                        </div>-->

                            <div class="input-field col s12 m6" >
                                <i class="material-icons prefix">adjust</i>
                                <div class="col s12 m12" style="margin-left: 23px">
                                    Block
                                    <select  class="browser-default block block_list" name="block" id="block" style="width: 100%">
                                        <?php
                                        foreach ($selected_block as $sblock){
                                            $d_id = $sblock->bid;
                                            $old_did = $get_cust->blocks;
                                            ?>
                                            <option <?=($d_id==$old_did)?'selected':'';?>  value="<?=$d_id;?>"><?=$sblock->b_name?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="row">
							<div class="input-field col s12 m4" >
                                <i class="material-icons prefix">location_on</i>
                                <div class="col s12 m12" style="margin-left: 23px">
                                    Locality
                                    <select  class="browser-default locality local_list" id="locality" name="locality" style="width: 100%">
                                        <option value="">Select Locality</option>
                                        <?php
                                        foreach ($localities as $slocal){

                                            $d_id = $slocal->id;
                                            $old_did = $get_cust->locality;
                                            ?>
                                            <option <?=($d_id==$old_did)?'selected':'';?>  value="<?=$d_id;?>"><?=$slocal->locality?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="input-field col s12 m2">
                                <center><a href="#open-modal2" class="btn myblue waves-light" style="padding: 0 5px; width: fit-content;">Add New
                                    <i class="material-icons right">add_circle_outline</i>
                                </a></center>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">navigation</i>
                                <input id="Address" type="text" class="validate" name="postal_address" value="<?=$get_cust->postal_address;?>">
                                <label for="Address">Postal Address</label>
                            </div>
						</div>
                        <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">Lat</i>
                                    <input type="text" value="<?=$get_cust->lat?>" id="lat1" name="lat"  />
                                    <label for="Address">Latitude</label>
                                </div>
                                <div class="input-field col s12 m4">
                                    <i class="material-icons prefix">Lang</i>
                                    <input type="text" value="<?=$get_cust->lng?>" id="long1" name="lng"  />
                                    <label for="Address">Longitude</label>
                                </div>
                                <div class="col s12 m2">
                                    <center><a href="#open-modal3" class="btn myblue waves-light" onclick="setinmap()" style="padding: 0 5px; width: fit-content;">Set in Map
                                            <i class="material-icons right">location_on</i>
                                        </a></center>
                                </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">mail</i>
                                <?php $mactive = $get_cust->msg_active; ?>
                                <select name="msg_active">
                                    <option <?=($mactive==0)?'selected':'';?> value="0"> Yes</option>
                                    <option <?=($mactive==1)?'selected':'';?> value="1"> No</option>
                                </select>
                                <label for="State">Message Active</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">dns</i>
                                <select name="msg_class" id="reason">
                                    <?php
                                    foreach ($customer_class as $class){
                                        $m_id = $class->id;
                                        $get_mid = $get_cust->class;
                                        ?>
                                        <option <?=($m_id==$get_mid)?'selected':'';?> value="<?=$m_id;?>"><?=$class->type?></option>
                                        <?php
                                    }
                                    ?>
                                </select>

                                <label for="msg_class">Customer Class</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                <label for="Full_Name">Payment Days</label>
                                <select class="browser-default payment_days" name="payment_days" tabindex="-1">
                                    <option value="">Select payment days</option>
                                    <option value="advance_payment">Advance payment</option>
                                    <?php
                                    for ($i=1; $i<=200; $i++)
                                    {
                                        ?>
                                        <option <?=isset($get_cust->payment_days)?'selected':''; ?> value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix" >person_add</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                    Association
                                <select class="browser-default association" name="association[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                    <option value="">Select Association</option>
                                    <?php
                                    $u_id =  $get_cust->crm_association;
                                    $uid_array = explode(',',$u_id);
                                    foreach ($association as $assoc){
                                        ?>
                                        <option <?=(in_array($assoc->id,$uid_array))?'selected':''; ?> value="<?=$assoc->id?>"> <?=$assoc->name?> </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="input-field col s12 m12" id="reason_field" style="visibility: hidden">
                            <i class="material-icons prefix">reply</i>
                            <input type="text" name="reason_value" id="reason_value" value="<?=$get_cust->unqualified_reason;?>">
                            <label for="Reason">Reason</label>
                        </div>
                        <div class="col s12 m12" style="text-align: center">
                            <button class="btn myblue waves-light edit_submit" style="padding: 0 5px" type="submit" name="edit">EDIT
                                <i class="material-icons right">save</i>
                            </button>
                        </div>

                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
<input type="hidden" id="MyLatLng" value=""/>
    <?php
    if(isset($_GET['edit'])) {
        $lat = $get_cust->lat;
        $lng = $get_cust->lng;
    }
    ?>
<input type="hidden" id="lat" value="<?=$lat;?>"/>
<input type="hidden" id="long" value="<?=$lng;?>"/>
</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>
<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: "<?=url("crm/get-customers") ?>",
                type: 'GET',
                data: function (d) {
                    d.category = $('#category').val();
                    d.state = $('#states').val();
                    d.district = $('#district').val();
                    d.locality = $('#locality').val();
                    d.block = $('#block').val();
                    d.ptype = $('#ptype').val();
                    d.user = $('#user').val();
                    d.pstype = $('#pstype').val();
                    d.startd = $('#date_start').val();
                    d.endd = $('#date_end').val();
                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name'},
                { data: 'sec_names', name: 'sec_names',visible:false},
                { data: 'proprieter_name', name: 'proprieter_name' },
                { data: 'category', name: 'customer_category' },
                { data: 'contact_no', name: 'contact_no' },
                { data: 'sec_contact', name: 'sec_contact',visible:false },
                { data: 'assign_users', name: 'assign_users' },
                { data: 'created_at', name: 'created_at' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            order: [[0, 'desc']],
            dom: 'lBfrtip',
            buttons: <?=$buttons;?>,
            fixedColumns: true,
            colReorder: true,
            exportOptions:{
                columns: ':visible'
            }
        });
    });

    $('#filterButton').click(function(){
        $('#table').DataTable().draw(true);
    });
</script>
<script>
    $(document).ready(function(){
        $('#table').attr('style', 'width: 100%');
        $('.user').select2();
        $('.category').select2();
        $('.states').select2();
        $('.district').select2();
        $('.locality').select2();
        $('.block').select2();
        $('.type').select2();
        $('.pstype').select2();
        $('.association').select2();
        $('.payment_days').select2();
        $('.datepicker').datepicker();
        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        var startDate = moment('<?=$std_date?>', 'YYYY-MM-DD HH:mm:ss').format('DD, MMM YYYY HH:mm:ss');
        var endDate = moment('<?=$end_date?>', 'YYYY-MM-DD HH:mm:ss').format('DD, MMM YYYY HH:mm:ss');

        $('input[name="dates"]').daterangepicker({
            opens: 'left',
            locale: {
                format: 'DD, MMM YYYY'
            },
            startDate: startDate,
            endDate: endDate
        }, function(start, end, label2) {
            $('#date_start').val(start.format('YY-MM-DD'));
            $('#date_end').val(end.format('YY-MM-DD'));
        });
        $('#my_date_type').change(function() {
            var opval = $(this).val();
            if(opval=="1"){
                document.getElementById('create').style.display = 'block';
                document.getElementById('due').style.display = 'none';
            }else{
                document.getElementById('due').style.display = 'block';
                document.getElementById('create').style.display = 'none';
            }
        });
        $('.state_list').change(function () {
            var sid = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district",
                data:'sid='+sid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.dist_list').html(data);
                }
            });
        });

        $('.ptype').change(function () {
            var val = $(this).val();

            $.ajax({
                type: "POST",
                url: "get-subtype",
                data:'tid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.pstype').html(data);
                }
            });
        });
        $('.district').change(function () {

            var did = $(this).val();

            $.ajax({
                type: "POST",
                url: "get-block",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){

                    $('#block').html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $('.block').change(function () {
            var did = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-locality",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $('#locality').html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $('.cc').change(function () {
            var cid = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-category_all_price",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.category_price').html(data);
                }
            });
        });
        $('.cc1').change(function () {
            var ctgid = $(this).val();
            var cid = $('.ccid').val();
            $.ajax({
                type: "POST",
                url: "get-category_all_price_cust",
                data:'custid='+cid+'&ctgid='+ctgid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.category_price').html(data);
                }
            });
        });
        $('#localitySave').on('click', function(){
            var formData = $('form#localityForm').serialize();
            var btn = $(this);
            $.ajax({
                url: 'save-locality',
                type: 'post',
                data: formData,
                beforeSend: function(){
                    btn.html('Saving...');
                },
                success: function(data){
                    if(data == 'success') {
                        alert('Locality Saved.');
                    }else{
                        alert('Invalid Data.');
                    }
                    btn.html('Save');
                }
            })
        })
    })

    function validateContact(index){
        var a = $(index).val();
        var b = $(index).val().length;
        var c = a.indexOf(",");
        if(c==-1){
            if(b==10){
                $('.add_submit').prop("disabled", false);
                $('.edit_submit').prop("disabled", false);
            }
            else{
                alert("Please Enter 10 digit no");
                $('.add_submit').prop("disabled", true);
                $('.edit_submit').prop("disabled", true);
            }
        }else{
            var str = a;
            var str_array = str.split(',');
            for(var i = 0; i < str_array.length; i++) {
                str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                var tot = str_array[i].length;
                if(tot!=10){
                    alert("Please Enter 10 digit number before and after comma");
                    $('.add_submit').prop("disabled", true);
                    $('.edit_submit').prop("disabled", true);
                }
                else{
                    $('.add_submit').prop("disabled", true);
                    $('.edit_submit').prop("disabled", true);
                }
            }
        }
    }
    $('.valid_no').blur(function () {
        var a = $(this).val();
        var b = $(this).val().length;
        var c = a.indexOf(",");
        if(c==-1){
            if(b==10){
                $('.add_submit').prop("disabled", false);
                $('.edit_submit').prop("disabled", false);
            }
            else{
                alert("Please Enter 10 digit no");
                $('.add_submit').prop("disabled", true);
                $('.edit_submit').prop("disabled", true);
            }
        }else{
            var str = a;
            var str_array = str.split(',');
            for(var i = 0; i < str_array.length; i++) {
                str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                var tot = str_array[i].length;
                if(tot!=10){
                    alert("Please Enter 10 digit number before and after comma");
                    $('.add_submit').prop("disabled", true);
                    $('.edit_submit').prop("disabled", true);
                }
                else{
                    $('.add_submit').prop("disabled", false);
                    $('.edit_submit').prop("disabled", false);
                }
            }
        }
    });

    $("input[name=contact_no]").keypress(function (e) {

        if (/\d+|,/i.test(e.key) ){

            console.log("character accepted: " + e.key)
        } else {
            console.log("illegal character detected: "+ e.key)
            return false;
        }

    });

    $('#add_profile').on('click', function(){
        $('#goto').val('1');
        $('#addForm button[type=submit]').trigger('click');
    });
    $(document).on('click','.cust_delete',function () {
        if (confirm("Are you sure?")) {
            var cid = $(this).val();
            $.ajax({
                type: "POST",
                url: "customer_delete",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    alert('Record Deleted Successfully');
                    location.reload();
                }
            });
        }
        else{

        }
        return false;
    });
    var map, marker, infoWindow, title, contentString, directionsService, directionsDisplay, pos;

    function initMap() {
        directionsService = new google.maps.DirectionsService();
        directionsDisplay = new google.maps.DirectionsRenderer();
		var lat = 59.909144;
		var lng = 10.7436936;
		<?php if(isset($_GET['edit'])){ ?>
			lat = <?=empty($get_cust->lat) ?  59.909144 : $get_cust->lat; ?>;
			lng = <?=empty($get_cust->lng) ?  10.7436936 : $get_cust->lng; ?>;
		<?php } ?>
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: {
                lat: lat,
                lng: lng
            }
        });
    $('#reason').change(function() {
        var opval = $(this).val();
        if(opval=="5" || opval=="6"){
            document.getElementById('reason_field').style.visibility = 'visible';
        }else{
            document.getElementById('reason_field').style.visibility = 'hidden';
            $('#reason_value').val("NA");
        }
    });

        //CUSTOM MARKER ICON
        var image = {
            url: "<?=url('assets/css/images')?>/map-marker.png",
            scaledSize: new google.maps.Size(32, 32)
        };
        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            icon: image,
            animation: google.maps.Animation.DROP,
            position: {
                lat: lat,
                lng: lng
            }
        });

        //marker.addListener('click', toggleBounce);
        //END CUSTOM MARKER ICON
		<?php if(isset($_GET['edit'])){ ?>
		contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading"><?=$get_cust->name?></h1>'+
            '<div id="bodyContent">'+
            '<p><?=$get_cust->postal_address?></p>'+
            '</div>'+
            '</div>';
		title = '<?=$get_cust->name?>';

        // GET POSITION

		<?php }else{ ?>
		contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<div id="bodyContent">'+
            '<p>This is your current location. Hold and drag to the site location</p>'+
            '</div>'+
            '</div>';
		title = 'Your Current Location';
        // Try HTML5 geolocation.
		<?php
		}?>
		infoWindow = new google.maps.InfoWindow({
          content: contentString
        });
		<?php
		$currentLocation = true;
		if(isset($_GET['edit'])){
			if($get_cust->lat != '' && $get_cust->lng != ''){
				$currentLocation = false;
                ?>
        document.getElementById('lat').value = <?=$get_cust->lat?>;
        document.getElementById('long').value = <?=$get_cust->lng?>;
        <?php
			}
		}

		?>
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                <?php if($currentLocation){ ?>
                marker.setPosition(pos);
                marker.setTitle(title);
                map.setCenter(pos);
                <?php } ?>
				document.getElementById('MyLatLng').value = pos.lat + ', '+ pos.lng;

            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }
        //END GET POSITION
        google.maps.event.addListener(marker, 'mouseup', function(event) {
            document.getElementById('lat').value = event.latLng.lat();
            document.getElementById('long').value = event.latLng.lng();
            //alert(event.latLng.lat() + ", " + event.latLng.lng());
        });
		marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
    }

    function calcRoute() {
        directionsDisplay.setMap(map);
        var start = document.getElementById('MyLatLng').value;
        var end = document.getElementById('lat').value + ', ' + document.getElementById('long').value;
        var request = {
            origin: start,
            destination: end,
            travelMode: 'DRIVING'
        };
        directionsService.route(request, function(result, status) {
            if (status == 'OK') {
                directionsDisplay.setDirections(result);
            }
        });
    }
    function navigate(){
        var url = 'http://maps.google.com/maps?navigate=yes&q=loc:';
        var end = document.getElementById('lat').value + ',' + document.getElementById('long').value;
        window.location.href = url + end;
    }
    //BOUNCE WHEN MARKER IS PRESSED
    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
    //END BOUNCE WHEN MARKER IS PRESSED

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }
    var im = "<?=url('assets/css/images')?>/bluecircle.png";
    function locate(){
        navigator.geolocation.getCurrentPosition(initialize,fail);
    }

    function initialize(position) {
        var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var userMarker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: im
        });
        map.setCenter(myLatLng);
        $("#latlong").val(myLatLng);

    }

    function fail(){
        alert('navigator.geolocation failed, may not be supported');
    }
    function saveLocation(){
        document.getElementById('lat1').value = document.getElementById('lat').value;
        document.getElementById('long1').value = document.getElementById('long').value;

    }
    function setinmap(){
        var pos = {
            lat: parseFloat(document.getElementById('lat1').value),
            lng: parseFloat(document.getElementById('long1').value)
        };
        var pos1 = {
            lat: false,
            lng: false
        };
        <?php if(isset($_GET['edit'])){ ?>
        pos1 = {
            lat: parseFloat(<?=$get_cust->lat?>),
            lng: parseFloat(<?=$get_cust->lng?>)
        };
        <?php } ?>
        if(pos.lat && pos.lng) {
            marker.setPosition(pos);
            map.setCenter(pos);
        }else if(pos1.lat && pos1.lng){
            marker.setPosition(pos1);
            map.setCenter(pos1);
        }else{
            navigator.geolocation.getCurrentPosition(function(position){
                var myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                marker.setPosition(myLatLng);
                map.setCenter(myLatLng);
            });
        }
    }
</script>
<script>

    var $insertBefore = $('#insertBefore');
    var $i = 0;
    // Add More Inputs
    $('#plusButton').click(function(){

        $i = $i+1;
        var indexs = $i+1;
        //append Options
        $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="row"><div class="input-field col s6 m6"><i class="material-icons prefix">account_circle</i><input autocomplete="off" class="validate" id="names'+indexs+'" name="names['+$i+']" type="text" value="" placeholder="Full Name"/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i><input autocomplete="off" class="validate" id="emails'+indexs+'" name="emails['+$i+']" type="email" value="" placeholder="Email"/></div></div>' +
            '<div class="row"><div class="input-field col s6 m6"><i class="material-icons prefix">contact_phone</i><input autocomplete="off" class="validate valid_no" id="contacts'+indexs+'" name="contacts['+$i+']" type="text" value="" onblur="validateContact(contacts'+indexs+')" placeholder="Contact No."/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_phones</i><input autocomplete="off" class="validate" id="landlines'+indexs+'" name="landlines['+$i+']" type="tel" value="" placeholder="Landline No" /></div></div>' +
            '<div class="row"><div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i>' +
            '<select class="getDesignation" id="desc'+indexs+'" name="descs['+$i+']" style="margin-left: 25px;display:block" >' +
            '<option>Select Contact Designation</option>' +
            '<?php foreach ($crm_contax as $desc): ?>' +
            '<option value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>'+
            '<?php endforeach; ?>' +
            '</select></div>' +
            '<div class="input-field col s6 m6"><center><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger">Remove<i class="fa fa-times" style="margin-left:10px"></i></button></center></div></div>' + '<div class="clearfix">\n' +
            '</div><br><br></div>').insertBefore($insertBefore);
        });
    // Remove fields
    function removeBox(index){
        $('#addMoreBox'+index).remove();
    }

</script>

<div id="open-modal2" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Add Area / Locality</h1>
        <div>
            <table border="0">
                <form method="post" id="localityForm">

                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>District</td>
                        <td ><select name="district_pop" class="browser-default district" tabindex="-1">
                                <?php
                                foreach ($districts as $district){
                                    $s_id = $district->id;
                                    ?>
                                    <option  value="<?=$s_id;?>"><?=$district->name?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Locality Name</td>
                        <td ><input type="text" id="locality_pop" name="locality_pop" class="form-control" value=""></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><a href="#modal-close" type="button" id="localitySave" class="btn myblue">Save</a></td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>
<div id="open-modal3" class="modal-window">
    <div style="margin-top: 13px;">
        <a href="#modal-close" title="Close" class="modal-close" style="color: #000;font-size: 13px;">close &times;</a>
        <h1>Set Location</h1>
        <div >
            <div id="map"></div>
        </div>
        <?php if(!$currentLocation){ ?>
        <div style="text-align: center; margin-top: 5px;margin-bottom: 5px;">e
            <!--<a class="btn myblue" href="javascript:void(0);" onclick="calcRoute()">Get Direction</a>-->
            <a style="padding: 7px;padding-top: unset;height: 31px;" class="btn myblue" href="javascript:void(0);" onclick="navigate()">Navigate</a>
            <a style="padding: 7px;padding-top: unset;height: 31px;" class="btn myblue" href="javascript:void(0);" onclick="locate()">My Location</a>
            <a style="padding: 7px;padding-top: unset;height: 31px;" class="btn myblue" href="#modal-close" onclick="saveLocation()">Save</a>
        </div>
        <?php }else{ ?>
            <div style="text-align: center; margin-top: 5px; margin-bottom: 5px;">
                <!--<a class="btn myblue" href="javascript:void(0);" onclick="calcRoute()">Get Direction</a>-->
                <!--<a class="btn myblue" href="javascript:void(0);" onclick="navigate()">Navigate</a>-->
                <a class="btn myblue" href="javascript:void(0);" onclick="locate()">My Location</a>
                <a class="btn myblue" href="#modal-close" onclick="saveLocation()">Save</a>
            </div>
        <?php } ?>
    </div>
</div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAB5amm0dLJi65cbZSQSGM3dZn4ctnO22Q&callback=initMap"></script>
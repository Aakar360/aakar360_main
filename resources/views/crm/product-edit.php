<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    td:first-child, th:first-child {
        position:sticky;
        left:0;
        z-index:1;
        background-color:white;
    }
    td:nth-child(2),th:nth-child(2)  {
        position:sticky;
        left:40px;
        z-index:1;
        background-color:white;
    }
    .tableFixHead th {
        position: sticky;
        top: 0;
        background: #ffffff;
        z-index:2
    }
    th:first-child , th:nth-child(2) {
        z-index:3

    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs">
            <div class="card-content" style="min-height: 500px;">
                <?=$notices;?>
<h6>Edit Product :: <?php echo  $product->id; ?></h6>
                            <div class="row">
                                <div id="Project">
                                    <center>
                                        <a class="btn myblue waves-light " style="padding:0 5px;" href="customer-profile?cid=<?=isset($_GET['cid']) ? $_GET['cid'] : 'customer-data'?>" >
                                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                                        </a>
                                    </center>
                                    <div class="project_form">
                                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                                <input type="hidden" name="product_id" value="<?php echo  $product->id; ?>">
                                                <input type="hidden" name="crm_customer_id" value="<?php echo  $product->crm_customer_id; ?>">

                                                <div class="row">
<?php// dd($products);?>
                                                    <div class="input-field col s12 m6" style="margin-top: 0px;">
                                                      Category
                                                        <select class="browser-default type1 category" name="category[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                                            <?php $project_ids =  $product->product_id;
                                                            $pid_array = explode(',',$project_ids);
                                                            foreach ($products as $pro){

                                                                ?>
                                                                <option <?=(in_array($pro->id,$pid_array))?'selected':''; ?> value="<?=$pro->id?>"><?=$pro->name?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="input-field col s12 m6" style="margin-top: 0px;">

                                                        <i class="material-icons prefix" style="margin-top: 10px">dns</i>
                                                        <div class="col s12 m12" style="margin-left: 20px">
                                                            Price Group
                                                            <select  class="browser-default price_group" name="price_group[]" multiple tabindex="-1" style="width: 100% !important;">
                                                                <option value="">Select Price Group</option>
                                                                <?php
                                                                $selected = explode(',', $product->price_groups);
                                                                    foreach($price_groups as $price_group){
                                                                        ?>
                                                                        <option <?=(in_array($price_group->id, $selected)) ? 'selected' : ''; ?> value="<?=$price_group->id?>"><?=$price_group->name.' - '.$price_group->group_name?></option>
                                                                    <?php
                                                                    }
                                                                
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12 m6">
                                                        Select Brand
                                                        <select  class="browser-default brand" name="brand[]" multiple tabindex="-1" style="width: 100% !important;">
                                                            <?php
                                                            $brand_ids =  $product->brands;
                                                            $bid_array = explode(',',$brand_ids);
                                                            foreach ($brand as $bnd){
                                                                ?>
                                                                <option <?=(in_array($bnd->id,$bid_array))?'selected':''; ?> value="<?=$bnd->id?>"> <?=$bnd->name?> </option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>

                                                    </div>
                                                    <div class="input-field col s12 m6">
                                                        Select Supplier
                                                        <select  class="browser-default supplier" name="supplier[]" multiple tabindex="-1" style="width: 100% !important;">

                                                            <?php
                                                             $sup_id =  $product->supplier_id;
                                                            $sid_array = explode(',',$sup_id);
                                                            foreach ($suppliers as $sup){
                                                                ?>
                                                                <option <?=(in_array($sup->id,$sid_array))?'selected':''; ?> value="<?=$sup->id?>"> <?=$sup->name?> </option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12 m12">
                                                        <i class="material-icons prefix">view_headline</i>
                                                        <input id="Description4" type="tel" class="validate" name="description" value="<?=$product->description; ?>">
                                                        <label for="Description4">Description</label>
                                                    </div>
                                                </div>

                                                <div class="row" style=" height: 200px;">

                                                    <div class="input-field col s12 m12">
                                                       <center><button class="btn myblue waves-light " type="submit" name="edit_product">Save
                                                            <i class="material-icons right">save</i>
                                                        </button></center>
                                                    </div>
                                                </div>

                                            </form>
                                    </div>
                                </div>
                            </div>
            </div>
        </div>
    </div>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

</body>

</html>
<script>
    $(document).ready(function(){
        $('.type1').select2();
        $('.variants').select2();
        $('.brand').select2();
        $('.supplier').select2();
        $('.category').select2();
        $('.price_group').select2();
        $(".category").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-price-groups-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".price_group").html(data);
                }
            });
        });

        $(".category").change(function(){
            var val = $(this).val();
            var cname = $(".category option:selected").html();

            $.ajax({
                type: "POST",
                url: "get-report-option",
                data:'cid='+val+'&cname='+cname+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data){
                    $(".report_div").html(data);
                    $("#model_report_type").val(data);
                }
            });
        });
        var get_view_type = $('#get_report_view').val();
        if(get_view_type=='brand_wise'){
            $('.brand_wise').show();
        }
        if(get_view_type=='price_wise'){
            $('.price_wise').show();
        }




    });
</script>




<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!--For Spreadsheet-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/Spreadsheet/styles.css">
    <!-- END: Custom CSS-->
    <style>
        table, td, th, tr{
            height: 0rem !important;
            font-size: 0.8rem !important;
        }
        .spreadsheet .select2-container--default .select2-selection--multiple .select2-selection__rendered{
            max-height: 100%;
        }
        .spreadsheet .select2-container--default.select2-container--focus .select2-selection--multiple{
            border: 1px solid gainsboro;
        }
        .spreadsheet .select2-container{
            height: 100%;
        }
        .spreadsheet td{
            max-width: 200px;
            padding: 1px;
        }
        .spreadsheet .select2-container--default .select2-selection--multiple{
            border:0px;
        }
    </style>
</head>

<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" >
                <?=$notices;?>
            <h5 class="card-title">Edit Tranportor Details </h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <?php
                $permission = checkRole($user->u_id,"tran_veri");

                ?>
                <h5><?=$transportor->name." [ ".$transportor->company_name." ] ";?>
                    <?php if($permission && $transportor->verified == 0)
                    { echo '<a href="verify-tronsportor?edit='.$_GET['cid'].'&action=1" class="btn myblue waves-light"><i class="material-icons left" style="margin-right: 5px">check</i> Verify</a>'; } if($transportor->verified == 1){ echo '<span style="font-size: 16px;color: blue"><i class="material-icons" style="font-size: 16px;color: blue;">verified_user</i> Verified</span>';if($permission) { echo '&nbsp;<a href="verify-customer?edit='.$_GET['cid'].'&action=0" class="btn myblue waves-light">Un-Verify</a>';} }?></h5>
                    <div class="col s12 m12 l12">
                        <ul class="row tabs" >
                            <li class="tab col s5 m5 xl1"><a class="active p-0" href="#Detail">Detail</a></li>
                            <li class="tab col s5 m5 xl1"><a class="p-0" href="#Root">Root</a></li>
                            <li class="tab col s5 m5 xl1"><a class="p-0" href="#Issue">Issue</a></li>
                        </ul>
                    </div>
                <div id="Detail" class="col-lg-12 col-xs-12">
                    <div class="divider"></div>
                    <div class="visits_form">
                     <form method="post" action="transportor_order-edit" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                         <input type="hidden" name="tid" value="<?php echo  $_GET['cid']; ?>">
                         <?php $editnames = explode(',',$transportor->name);
                         $editconts = explode(',',$transportor->contact_no);
                         $editlandline = explode(',',$transportor->landline);
                         $editcondes = explode(',',$transportor->contact_designation);
                         ?>
                        <div class="row">
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="Full_Name" type="text" class="validate" name="name" <?php if(!empty($editnames[0])){ ?> value="<?= $editnames[0]; } ?>">
                                <label for="Full_Name">Name</label>
                            </div>
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="Company_name" type="text" class="validate" name="cname" value="<?=$transportor->company_name?>">
                                <label for="Company_name">Company Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s6 m6">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;"> <i class="material-icons prefix">flag</i></div>
                                <div class="col s11 m11">
                                    <label>Select State</label>
                                <select class="browser-default states" id="states" name="states" style="width: 510px;">
                                    <?php if(!empty($states)){
                                        foreach ($states as $state) {
                                            $v_id = $state->id;
                                            ?>
                                            <option <?php if($transportor->state == $v_id){echo "selected";} ?> value="<?=$v_id; ?>"><?= $state->name ?></option>
                                            <?php
                                        } }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="col s6 m6">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;">
                                    <i class="material-icons prefix">adjust</i>
                                </div>
                                <div class="col s11 m11">
                                    <label>Select District</label>
                                    <select class="browser-default district" id="district" name="district" style="width: 510px;">
                                        <?php $master_district = DB::table('district')->where('state_id', $transportor->state)->first();
                                        if(!empty($master_district)){ ?>
                                            <option value="<?= $master_district->id?>"><?= $master_district->name?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="Company_Address" type="text" class="validate" name="caddress" value="<?=$transportor->company_address?>">
                                <label for="Company_Address">Company Address</label>
                            </div>
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="contact_number" type="text" class="contact_number" name="contact_number" <?php if(!empty($editconts[0])) { ?> value="<?= $editconts[0]; } ?>">
                                <label for="contact_number">Contact Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <select id="Designation" class="desc" name="designation">
                                    <?php foreach ($crm_contax as $desc): ?>' +
                                        <option <?php if($desc->id == 1){ echo  'selected'; } ?> value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="Company_Address">Contact Designation</label>
                            </div>
                            <div class="input-field col s6 m6">
                                <i class="material-icons prefix">list</i>
                                <input id="landline" type="text" class="landline" name="landline" <?php if(!empty($editlandline[0])){?> value="<?= $editlandline[0]; } ?>">
                                <label for="landline">Landline Number</label>
                            </div>
                        </div>

                         <?php $getCust = Session::get('customers');?>
                        <div class="row">
                            <div class="col s6 m6">
                                <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;"> <i class="material-icons prefix">list</i></div>
                                <div class="col s11 m11">
                                    <label>Refered By</label>
                                    <select class="browser-default listcust" id="listcust" name="listcust[]" multiple  tabindex="-1" style="width: 516px;">
                                       <!-- <?php /*if(!empty($customers)){
                                            foreach (array_chunk($customers, 10) as $customer) {
                                                foreach($customer as $cust){
                                                $v_id = $cust->id;
                                                */?>
                                                <option <?php /*if($transportor->refered_by == $v_id){echo "selected";} */?> value="<?/*=$v_id;*/?>"><?/*= $cust->name; */?></option>
                                                --><?php
/*                                          } } }
                                        */?>
                                    </select>
                                </div>
                            </div>
                        </div>
                         <!--More Options-->
                         <?php if(!empty($transportor->sec_names)){
                             $sec_names = unserialize($transportor->sec_names);
                             foreach ($sec_names as $key=>$edname){ ?>
                                 <div class="input-field col s6 m6">
                                     <i class="material-icons prefix">account_circle</i>
                                     <input autocomplete="off" class="validate" id="names'+indexs+'" name="names['+$i+']" type="text" value="<?= $edname?>" placeholder="Full Name"/>
                                 </div>
                                 <div class="input-field col s6 m6">
                                     <i class="material-icons prefix">contact_mail</i>
                                     <input autocomplete="off" class="validate" id="emails'+indexs+'" name="emails['+$i+']" type="email" value="<?= $transportor->email?>" placeholder="Email"/>
                                 </div>
                                 <div class="input-field col s6 m6">
                                     <i class="material-icons prefix">contact_phone</i>
                                     <input autocomplete="off" class="validate valid_no" id="contacts'+indexs+'" name="contacts['+$i+']" type="text" <?php if(!empty($editconts[$key])) { ?> value="<?= $editconts[$key]; }?>" onblur="validateContact(contacts'+indexs+')" placeholder="Contact No."/>
                                 </div>
                                 <div class="input-field col s6 m6">
                                     <i class="material-icons prefix">contact_phones</i>
                                     <input autocomplete="off" class="validate" id="landlines'+indexs+'" name="landlines['+$i+']" type="tel" <?php if(!empty($editlandline[$key])){ ?> value="<?= $editlandline[$key]?>" <?php } ?> placeholder="Landline No" />
                                 </div>
                                 <div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i>
                                     <select class="browser-default getDesignation" id="desc'+indexs+'" name="descs['+$i+']" style="margin-left: 25px">
                                         <option>Select Designation</option>
                                         <?php foreach ($crm_contax as $desc): ?>
                                             <option <?php if(!empty($editcondes[$key]) && $editcondes[$key] == $desc->id){ echo "selected"; }?> value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>
                                         <?php endforeach; ?>
                                     </select></div>
                                 <div class="input-field col s6 m6">
                                     <center><button type="button" onclick="removemoreBox('+indexs+')" class="btn btn-sm btn-danger">Remove<i class="fa fa-times" style="margin-left:10px"></i>
                                         </button></center></div><div class="clearfix">
                                 </div></br></br>
                             <?php }}?>
                         <!--End More Options-->
                        <div class="clearfix">

                        </div><br><br>
                        <div id="insertmoreBefore"></div>
                        <div class="clearfix"></div>
                        <button type="button" id="moreButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                            Add More <i class="fa fa-plus"></i>
                        </button>
                         <div class="col s12 m12 " style="text-align:center;margin-top: 20px;margin-bottom: 20px;" >
                             <button class="btn myblue waves-light add_submit"  style="padding:0 5px;" type="submit" name="add">SAVE
                                 <i class="material-icons right">save</i>
                             </button>
                         </div>
                    </form>
                    </div>
                </div>
                <div id="Root" class="col-lg-12 col-xs-12" style="display: none">
                    <div class="divider"></div>
                    <div class="Message_form">
                        <form method="post" action="transportor_order-edit?<?php echo  $_GET['cid']; ?>" class="col s12 m12">
                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                            <input type="hidden" name="tid" value="<?php echo  $_GET['cid']; ?>">
                            <!--SpreadSheet Start-->
                            <section class="spreadsheet">
                                <table class="spreadsheet__table"
                                       id="table-main">
                                    <thead>
                                        <th style="border-bottom: hidden"></th>
                                        <th colspan="2"  style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">Root Start</th>
                                        <th colspan="2"  style="text-align: center">Root End</th>
                                    </thead>
                                    <thead>
                                        <th style="width: 10%;text-align: center;">S.No.</th>
                                        <th  style="text-align: center">State</th>
                                        <th  style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">District</th>
                                         <th  style="text-align: center">State</th>
                                        <th  style="text-align: center;">District</th>
                                    </thead>
                                    <tbody class="spreadsheet__table--body"id="table-body">
                                  <?php   if(empty($transportor->ststate)) {
                                      ?>
                                      <input type="hidden" id="tatalrow"   value="1" >
                                    <tr>
                                        <td data-id="1" style="text-align: center">1</td>
                                        <td>
                                            <select class="browser-default states" id="ststates" name="ststates[]" multiple  tabindex="-1" style="width:100% !important;">
                                                <?php if(!empty($states)){
                                                    foreach ($states as $state) {
                                                        $v_id = $state->id;
                                                        ?>
                                                        <option  value="<?=$v_id; ?>"><?= $state->name ?></option>
                                                        <?php
                                                    } }
                                                ?>
                                            </select>
                                        </td>

                                        <td style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">
                                            <select class="browser-default stdistrict" id="stdistrict" name="stdistrict[]" multiple  tabindex="-1" style="width:100% !important;">
                                                <?php if(!empty($districts)){foreach($districts as $singledata) {?>
                                                    <option   value="<?= $singledata->id ?>"><?= $singledata->name; ?></option>
                                                <?php } } ?>
                                            </select></td>
                                        <td><select class="browser-default states" id="edstates" name="edstates[]" multiple  tabindex="-1" style="width:100% !important;">
                                                <?php if(!empty($states)){
                                                    foreach ($states as $state) {
                                                        $v_id = $state->id;
                                                        ?>
                                                        <option  value="<?=$v_id; ?>"><?= $state->name ?></option>
                                                        <?php
                                                    } }
                                                ?>
                                            </select></td>
                                        <td><select class="browser-default eddistrict" id="eddistrict" name="eddistrict[]" multiple  tabindex="-1" style="width:100% !important;">
                                                <?php if(!empty($districts)){foreach($districts as $singledata) {?>
                                                    <option  value="<?= $singledata->id ?>"><?= $singledata->name; ?></option>
                                                <?php } } ?>
                                            </select></td>
                                    </tr>
                                  <?php } ?>
                                        <?php if(!empty($transportor->ststate)) {
                                            $i = 0;
                                            $tatalrow = 1;
                                            $tranststate = !empty($transportor->ststate)?unserialize($transportor->ststate):false;
                                            $tatalrow = count($tranststate);
                                            $totalfields = !empty($tatalrow)?$tatalrow:1;
                                            $tranedstate = !empty($transportor->edstate)?unserialize($transportor->edstate):false;
                                            $transtdistrict = !empty($transportor->stdistrict)?unserialize($transportor->stdistrict):false;
                                            $traneddistrict = !empty($transportor->eddistrict)?unserialize($transportor->eddistrict):false;?>
                                            <input type="hidden" id="tatalrow"   value="<?= $tatalrow;?>" >
                                            <?php foreach ($tranststate as $key=>$stdata){  $i ++; ?>

                                            <tr>
                                                <td data-id="<?=$i;?>" style="text-align: center"><?=$i;?></td>
                                                <td>
                                                    <select class="browser-default states" id="ststates<?=$key+1;?>" name="ststates[<?=$key+1;?>][]" multiple  tabindex="-1" style="width:100% !important;">
                                                        <?php if(!empty($states)){
                                                            foreach ($states as $state) {
                                                                $v_id = $state->id;

                                                                ?>
                                                                <option <?php  if((is_array($stdata) && in_array($v_id,$stdata)) || (!is_array($stdata) && $v_id == $stdata)){ echo  'selected'; } ?> value="<?=$v_id; ?>"><?= $state->name ?></option>
                                                                <?php
                                                            } }
                                                        ?>
                                                    </select>
                                                </td>

                                                <td style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">
                                                    <select class="browser-default stdistrict" id="stdistrict<?=$key+1;?>" name="stdistrict[<?=$key+1;?>][]" multiple  tabindex="-1" style="width:100% !important;">
                                                        <?php if(!empty($districts)){foreach($districts as $singledata) {?>
                                                            <option  <?php if((is_array($transtdistrict[$key]) && in_array($singledata->id,$transtdistrict[$key])) || (!is_array($transtdistrict[$key]) && $singledata->id == $transtdistrict[$key])){ echo  'selected'; } ?> value="<?= $singledata->id ?>"><?= $singledata->name; ?></option>
                                                        <?php } } ?>
                                                    </select></td>
                                                <td><select class="browser-default states" id="edstates<?=$key+1;?>" name="edstates[<?=$key+1;?>][]" multiple  tabindex="-1" style="width:100% !important;">
                                                        <?php if(!empty($states)){
                                                            foreach ($states as $state) {
                                                                $v_id = $state->id;
                                                                ?>
                                                                <option <?php if((is_array($tranedstate[$key]) && in_array($v_id,$tranedstate[$key])) || (!is_array($tranedstate[$key]) && $v_id == $tranedstate[$key])){ echo  'selected'; } ?> value="<?=$v_id; ?>"><?= $state->name ?></option>
                                                                <?php
                                                            } }
                                                        ?>
                                                    </select></td>
                                                <td><select class="browser-default eddistrict" id="eddistrict<?=$key+1;?>" name="eddistrict[<?=$key+1;?>][]" multiple  tabindex="-1" style="width:100% !important;">
                                                        <?php if(!empty($districts)){foreach($districts as $singledata) {?>
                                                            <option  <?php if((is_array($traneddistrict[$key]) && in_array($singledata->id,$traneddistrict[$key])) || (!is_array($traneddistrict[$key]) && $singledata->id==$traneddistrict[$key])){ echo  'selected'; } ?> value="<?= $singledata->id ?>"><?= $singledata->name; ?></option>
                                                        <?php } } ?>
                                                    </select></td>
                                            </tr>
                                        <?php  }  }?>
                                            <tr id="insertBefore"></tr>
                                    </tbody>
                                </table>
                            </section>
                            <button type="button" id="plusButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                                Add More <i class="fa fa-plus"></i>
                            </button>
                            <div class="col s12 m12 " style="text-align:center;margin-top: 20px;margin-bottom: 20px;" >
                                <button class="btn myblue waves-light add_submit"  style="padding:0 5px;" type="submit" name="addroot">SAVE
                                    <i class="material-icons right">save</i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div id="Issue" class="col-lg-12 col-xs-12" style="display: none">
                    <div class="divider"></div>
                    <div class="notes_form">
                        <form method="post" action="transportor_order-edit" enctype="multipart/form-data" class="col s12">
                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                            <input type="hidden" name="crm_cust_id" value="<?php echo  $transportor->id; ?>">

                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <i class="material-icons prefix">watch_later</i>
                                    <input type="date" class="datepicker" id="dob1" name="select_date" >
                                    <label for="dob1">Date</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m12">
                                    <i class="material-icons prefix">view_headline</i>
                                    <input id="Description3" type="text" class="validate" name="description" required>
                                    <label for="Description3">Description</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <div class="file-field">
                                        <div class="btn myblue waves-light left" style="padding:0 5px;">
                                            <span>Choose file</span>
                                            <input type="file" name="image" style="width: 100%" class="btn myblue" id="notes_image">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload your file">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-field col s12 m3">
                                    <p id="notes_img_div">
                                        <img id="notes_image_show" src="#" height="200px" width="200px"  />
                                    </p>
                                </div>

                                <div class="input-field col s12 m3" style="text-align: center">
                                    <button class="btn myblue waves-light" style="padding:0 5px;" type="submit" name="add_issue">Save
                                        <i class="material-icons right">save</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="divider"></div>
                    <h6>All Issue</h6>
                    <div class="row">
                        <div class="col s12 table-responsive">
                            <table id="notes-table" class="display" style="width: 100%;">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>File</th>
                                    <th>edited_by</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

    </div>


</div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!--For Spreadsheet-->
<script src="<?=$tp; ?>/Spreadsheet/main.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js" integrity="sha512-jNDtFf7qgU0eH/+Z42FG4fw3w7DM/9zbgNPe3wfJlCylVDTT3IgKW5r92Vy9IHa6U50vyMz5gRByIu4YIXFtaQ==" crossorigin="anonymous"></script>
</body>

</html>
<script>
    var $insertBefore = $('#insertBefore');
    var i = Number($('#tatalrow').val());
    // Add More Inputs
    $('#plusButton').click(function(){
        i = Number(i+1);
        var indexs = i+1;
        //append Options
        $('<tr id="addMoreBox'+indexs+'" class="clearfix"><td style="text-align: center">'+i+'</td><td>' +
            '<select class="browser-default ststates" id="ststates'+indexs+'"  name="ststates['+i+'][]" multiple tabindex="-1" style="width:100% !important;"><option value="">Select State</option>' +
            '<?php foreach($states as $state):?>' +
            '<option value="<?=$state->id; ?>"><?= $state->name; ?></option>' +
            '<?php endforeach;?>' +
            '</select></td>' +
            '<td style="text-align: center;border-right-style: solid;border-right-width: 0.5rem; border-right-color: rgb(0 0 0 / 49%);">' +
            '<select class="browser-default stdistrict" id="stdistrict'+indexs+'"  name="stdistrict['+i+'][]" multiple tabindex="-1" style="width:100% !important;"></select></td>' +
            '<td><select class="browser-default edstates" id="edstates'+indexs+'"  name="edstates['+i+'][]" multiple tabindex="-1" style="width:100% !important;"><option value="">Select State</option>' +
            '<?php foreach($states as $state):?>' +
            '<option value="<?=$state->id; ?>"><?= $state->name; ?></option>' +
            '<?php endforeach;?>' +
            '</select></td>' +
            '<td><select class="browser-default eddistrict" id="eddistrict'+indexs+'"  name="eddistrict['+i+'][]" multiple tabindex="-1" style="width:100% !important;"></select></td></tr>').insertBefore($insertBefore);

        $('#ststates'+indexs).select2()
        $('#edstates'+indexs).select2()
        $('#stdistrict'+indexs).select2()
        $('#eddistrict'+indexs).select2()

        $("#ststates"+indexs).change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $("#stdistrict"+indexs).html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $("#edstates"+indexs).change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $("#eddistrict"+indexs).html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });

    });
    // Remove fields
    function removeBox(index){
        $('#addMoreBox'+index).remove();
    }


</script>
<script>

    var $insertmoreBefore = $('#insertmoreBefore');
    var $i = 0;
    // Add More Inputs
    $('#moreButton').click(function(){

        $i = $i+1;
        var indexs = $i+1;
        //append Options
        $('<div id="addMore'+indexs+'" class="clearfix"> ' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">account_circle</i><input autocomplete="off" class="validate" id="names'+indexs+'" name="names['+$i+']" type="text" value="" placeholder="Full Name"/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i><input autocomplete="off" class="validate" id="emails'+indexs+'" name="emails['+$i+']" type="email" value="" placeholder="Email"/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_phone</i><input autocomplete="off" class="validate valid_no" id="contacts'+indexs+'" name="contacts['+$i+']" type="text" value="" onblur="validateContact(contacts'+indexs+')" placeholder="Contact No."/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_phones</i><input autocomplete="off" class="validate" id="landlines'+indexs+'" name="landlines['+$i+']" type="tel" value="" placeholder="Landline No" pattern="[0-9]{4}-[0-9]{7}"/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i>' +
            '<select class="browser-default getDesignation" id="desc'+indexs+'" name="descs['+$i+']" style="margin-left: 25px">' +
            '<option>Select Designation</option>' +
            '<?php foreach ($crm_contax as $desc): ?>' +
            '<option value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>'+
            '<?php endforeach; ?>' +
            '</select></div>' +
            '<div class="input-field col s6 m6"><center><button type="button" onclick="removemoreBox('+indexs+')" class="btn btn-sm btn-danger">Remove<i class="fa fa-times" style="margin-left:10px"></i></button></center></div>' + '<div class="clearfix">\n' +
            '</div></br></br></div>').insertBefore($insertmoreBefore);
    });
    // Remove fields
    function removemoreBox(index){
        $('#addMore'+index).remove();
    }



</script>
<script>
    $(document).ready(function(){
        $('.user').select2();
        $('.pcat').select2();
        $('.states').select2();
        $('#ststates').select2();
        $('.edstates').select2();
        $('.stdistrict').select2();
        $('.eddistrict').select2();
        $('.district').select2();
        $('.listcust').select2();
        $('.listcust').lazyLoad();

        $('.pcat').change(function () {
            var pcid = $(this).val();
            var ccid = $('.ccid').val();
            $.ajax({
                type: "POST",
                url: "get-price",
                data:'pcid='+pcid+'&ccid='+ccid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.price').focus();
                    $('.price').val(data);

                }
            });
        });
        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $("#ststates").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".stdistrict").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $("#edstates").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".eddistrict").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });

    });

    $(document).on('click','.cust_transportor_delete',function () {

        if (confirm("Are you sure?")) {
            var cid = $(this).val();
//            alert(cid);
//            var a = $(this);
            $.ajax({
                type: "POST",
                url: "cust_transportor_delete",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
                success: function(data){

                    alert(data);
                    location.reload();
                }
            });
        }
        else{

        }
    });

    $(function() {
        ntable = $('#notes-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-issue-data") ?>",
                type: 'GET',
                data: {cid:'<?=isset($_GET['cid']) ? $_GET['cid'] : ''?>'}
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'select_date', name: 'select_date' },
                { data: 'description', name: 'description'},
                { data: 'image', name: 'image', orderable: false, searchable: false },
                { data: 'edited_by', name: 'edited_by', searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });

    $(document).on('click', '.idelete', function () {
        var pid = $(this).val();
        var tid = $(".cust_id").val();
        var a = $(this);

        $.ajax({
            type: "POST",
            url: "trans-issue-delete",
            data:'pid='+pid+'&tid='+tid+'&_token=<?=csrf_token(); ?>',
            beforeSend: function(){
                $(a).html('Wait...');
            },
            success: function(data){
                alert(data);
                if(ntable){
                    $('#notes-table').DataTable().ajax.reload();
                }else{
                    location.reload();
                }
            }
        });
    });

</script>
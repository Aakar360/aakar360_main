<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />



    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" >
                <?=$notices;?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                    <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Association Master</h5>
                        <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                            <a class="btn myblue waves-light" style="padding:0 5px;"  href="association-master?add">
                                <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                            </a>
                        </div>
                            <div class="row" style="position:relative;">
                                <div class="col s12 table-responsive">
                                    <table id="page-length-option" class="display">
                                        <thead>
                                        <tr role="row">
                                            <th>Sr.No.</th>
                                            <th>Association name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                            foreach ($association as $assoc){ $cid = $assoc->id;?>
                                            <tr>
                                                <td>
                                                    <?=$assoc->id;?>
                                                </td>
                                                <td><?= $assoc->name?></td>
                                                <td>
                                                    <button value="<?=$cid?>" class="btn myred waves-light cust_category_delete" style="padding:0 10px;"><i class="material-icons">delete</i></button>
                                                    <a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="association-master?edit=<?=$cid?>"><i class="material-icons">edit</i></a>
                                                </td>
                                            </tr>
                                            <?php
                                             }
                                        ?>

                                        </tbody>
                                    </table>

                    </div>
                </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Association </h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="association-master" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m10">
                                <i class="material-icons prefix">list</i>
                                <input id="Full_Name" type="text" class="validate" name="name">
                                <label for="Full_Name">Association Name</label>
                            </div>
                            <div class="col s12 m12 " style="text-align:center;margin-top: 20px;margin-bottom: 20px;" >
                                <button class="btn myblue waves-light add_submit"  style="padding:0 5px;" type="submit" name="add">SAVE
                                    <i class="material-icons right">save</i>
                                </button>
                            </div>
                         </div>
                    </form>
                <?php } ?>

                <?php if(isset($_GET['edit'])){
                $cid = $_GET['edit'];
                $get_association = DB::select("SELECT * FROM `crm_association` WHERE id = '$cid'")[0];
                ?>
                <h5 class="card-title">Edit association Details </h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form method="post" action="association-master" class="col s12 m12">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <input type="hidden" class="ccid" name="cidd" value="<?php echo $cid; ?>">
                    <div class="row">
                    <div class="input-field col s12 m10">
                        <i class="material-icons prefix">list</i>
                        <input id="Full_Name" type="text" class="validate" value="<?php echo $get_association->name; ?>" name="name">
                        <label for="Full_Name">Association Name</label>
                    </div>
                        <div class="input-field col s12 m3" style="text-align: center;margin-bottom: 20px">
                            <button class="btn myblue waves-light edit_submit" type="submit" name="edit">SAVE
                                <i class="material-icons right">save</i>
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>

        <?php } ?>

    </div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
</body>

</html>
<script>
    $(document).ready(function(){
        $('.pcat').select2();

        $('.pcat').change(function () {
            var pcid = $(this).val();
            var ccid = $('.ccid').val();
            $.ajax({
                type: "POST",
                url: "get-price",
                data:'pcid='+pcid+'&ccid='+ccid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.price').focus();
                    $('.price').val(data);

                }
            });
        });
    });


    $(document).on('click','.cust_category_delete',function () {

        if (confirm("Are you sure?")) {
            var cid = $(this).val();
//            alert(cid);
//            var a = $(this);
            $.ajax({
                type: "POST",
                url: "association_delete",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
                success: function(data){

                    alert(data);
                    location.reload();
                }
            });
        }
        else{

        }
    });

</script>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />

    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" >
                <?=$notices;?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                    <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Transportor Master</h5>
                        <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                            <a class="btn myblue waves-light" style="padding:0 5px;"  href="transportor-master?add">
                                <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                            </a>
                        </div>
                            <div class="row" style="position:relative;">
                                <div class="col s12 table-responsive">
                                    <table id="page-length-option" class="display">
                                        <thead>
                                        <tr role="row">
                                            <th>Sr.No.</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                            $i=1; foreach ($transportors as $transportor){ $cid = $transportor->id;?>
                                            <tr>
                                                <td>
                                                    <?=$i;?>
                                                </td>

                                                <td><?= $transportor->name?></td>
                                                <td>
                                                    <button value="<?=$cid?>" class="btn myred waves-light cust_transportor_delete" style="padding:0 10px;"><i class="material-icons">delete</i></button>
                                                    <a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="transportor-master?edit=<?=$cid?>"><i class="material-icons">edit</i></a>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++; }
                                        ?>

                                        </tbody>
                                    </table>

                    </div>
                </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Transportor</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <?=$notices;?>
                <form method="post" action="transportor-master" class="col s12 m12" id="addForm">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <div class="row">
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="Full_Name" type="text" class="validate" name="name">
                            <label for="Full_Name">Name</label>
                        </div>
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="Company_name" type="text" class="validate" name="cname">
                            <label for="Company_name">Company Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6 m6">
                            <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;"> <i class="material-icons prefix">flag</i></div>
                            <div class="col s11 m11">
                                <label>Select State</label>
                                <select class="browser-default states" id="states" name="states" style="width: 510px;">
                                    <?php if(!empty($states)){
                                        foreach ($states as $state) {
                                            $v_id = $state->id;
                                            ?>
                                            <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                            <?php
                                        } }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col s6 m6">
                            <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;">
                                <i class="material-icons prefix">adjust</i>
                            </div>
                            <div class="col s11 m11">
                                <label>Select District</label>
                                <select class="browser-default district" id="district" name="district" style="width: 510px;">

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="Company_Address" type="text" class="validate" name="caddress">
                            <label for="Company_Address">Company Address</label>
                        </div>
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="contact_number" type="text" class="contact_number valid" name="contact_number">
                            <label for="contact_number">Contact Number</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <select id="Designation" class="desc" name="designation">
                                <?php foreach ($crm_contax as $desc): ?>' +
                                    <option <?php if($desc->id == 1){ echo  'selected'; } ?> value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label for="Company_Address">Contact Designation</label>
                        </div>
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="landline" type="text" class="landline" name="landline">
                            <label for="landline">Landline Number</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6 m6">
                            <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;"> <i class="material-icons prefix">list</i></div>
                            <div class="col s11 m11">
                                <label>Refered By</label>
                                <input class="browser-default listcust" id="listcust" name="listcust[]" multiple style="width: 516px;"/>
                                <!--<select class="browser-default listcust" id="listcust" name="listcust[]" multiple  tabindex="-1" style="width: 516px;">
                                    <?php /*if(!empty($customers)){
                                            foreach ($customers as $customer) {
                                                $v_id = $customer->id;
                                                */?>
                                                <option value="<?/*=$v_id; */?>"><?/*= $customer->name; */?></option>
                                               <?php
/*                                                } }
                                               */?>
                                </select>-->
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">

                    </div><br><br>
                    <div id="insertmoreBefore"></div>
                    <div class="clearfix">

                    </div>
                    <button type="button" id="moreButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                        Add More <i class="fa fa-plus"></i>
                    </button>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <input type="hidden" id="goto" name="goto" value="0"/>
                        <button class="btn myblue waves-light add_submit" style="padding:0 5px;" type="submit"  name="add">Add
                            <i class="material-icons right">save</i>
                        </button>
                        <button class="btn myblue waves-light add_submit " style="padding:0 5px; margin-top: 10px;margin-bottom: 10px" type="button"  name="add_profile" id="add_profile">Add & Goto Profile
                            <i class="material-icons right">account_circle</i>
                        </button>
                    </div>
                </form>

                <?php } ?>

                <?php if(isset($_GET['edit'])){
                $cid = $_GET['edit'];
                $get_transportor = \App\Transportor::where('id',$cid)->first();
                $editnames = !empty($get_transportor->sec_names)?unserialize($get_transportor->sec_names):false;
                $editconts = !empty($get_transportor->sec_contacts)?unserialize($get_transportor->sec_contacts):false;
                $editlandline = !empty($get_transportor->sec_landline)?unserialize($get_transportor->sec_landline):false;
                $editcondes = !empty($get_transportor->sec_designation)?unserialize($get_transportor->sec_designation):false;
                ?>
                <h5 class="card-title">Edit Tranportor Details </h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form method="post" action="transportor-master" class="col s12 m12">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <input type="hidden" class="ccid" name="cid" value="<?php echo $cid; ?>">
                    <div class="row">
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="Full_Name" type="text" class="validate" name="name" <?php if(!empty($get_transportor->name)){  ?> value="<?= $get_transportor->name; } ?>">
                            <label for="Full_Name">Name</label>
                        </div>
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="Company_name" type="text" class="validate" name="cname" value="<?= $get_transportor->company_name; ?>">
                            <label for="Company_name">Company Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6 m6">
                            <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;"> <i class="material-icons prefix">flag</i></div>
                            <div class="col s11 m11">
                                <label>Select State</label>
                                <select class="browser-default states" id="states" name="states" style="width: 510px;">
                                    <?php if(!empty($states)){
                                        foreach ($states as $state) {
                                            $v_id = $state->id;
                                            ?>
                                            <option <?php if($get_transportor->state == $v_id){ echo "selected"; } ?> value="<?=$v_id; ?>"><?= $state->name ?></option>
                                            <?php
                                        } }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col s6 m6">
                            <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;">
                                <i class="material-icons prefix">adjust</i>
                            </div>
                            <div class="col s11 m11">
                                <label>Select District</label>
                                <select class="browser-default district" id="district" name="district" style="width: 510px;">
                                    <?php if(!empty($districts)) { foreach ($districts as $dists){ ?>
                                        <option <?php if($get_transportor->district == $dists->id){ echo "selected";}?> value="<?= $dists->id; ?>"><?= $dists->name; ?></option>
                                    <?php }} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="Company_Address" type="text" class="validate" name="caddress" value="<?= $get_transportor->company_address; ?>">
                            <label for="Company_Address">Company Address</label>
                        </div>
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="contact_number" type="text" class="contact_number valid" name="contact_number" <?php if(!empty($get_transportor->contact_no)) { ?> value="<?= $get_transportor->contact_no; } ?>">
                            <label for="contact_number">Contact Number</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <select id="Designation" class="desc" name="designation">
                                <?php foreach ($crm_contax as $desc): ?>' +
                                    <option <?php if($desc->id == 1){ echo  'selected'; } ?> value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <label for="Company_Address">Contact Designation</label>
                        </div>
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">list</i>
                            <input id="landline" type="text" class="landline" name="landline" <?php if(!empty($get_transportor->landline)){?>value="<?= $get_transportor->landline; } ?>">
                            <label for="landline">Landline Number</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6 m6">
                            <div class="col s1 m1" style="text-align: center;margin-top: 30px;margin-left: -15px;"> <i class="material-icons prefix">list</i></div>
                            <div class="col s11 m11">
                                <label>Refered By</label>
                                <input class="browser-default listcust" id="listcust" name="listcust[]" multiple style="width: 516px;"/>

                            </div>
                        </div>
                    </div>
                    <!--More Options-->
                    <?php if(!empty($get_transportor->sec_names)){
                    foreach ($editnames as $key=>$edname){  ?>
                        <div class="input-field col s6 m6">
                            <i class="material-icons prefix">account_circle</i>
                            <input autocomplete="off" class="validate" id="names'+indexs+'" name="names[<?= $key ?>]" type="text" value="<?= $edname?>" placeholder="Full Name"/>
                        </div>
                        <div class="input-field col s6 m6">
                        <i class="material-icons prefix">contact_mail</i>
                        <input autocomplete="off" class="validate" id="emails'+indexs+'" name="emails[<?= $key ?>]" type="email" value="<?= $get_transportor->email?>" placeholder="Email"/>
                        </div>
                        <div class="input-field col s6 m6">
                        <i class="material-icons prefix">contact_phone</i>
                        <input autocomplete="off" class="validate valid_no" id="contacts'+indexs+'" name="contacts[<?= $key ?>]" type="text" <?php if(!empty($editconts[$key])) { ?> value="<?= $editconts[$key]; }?>" onblur="validateContact(contacts'+indexs+')" placeholder="Contact No."/>
                        </div>
                        <div class="input-field col s6 m6">
                        <i class="material-icons prefix">contact_phones</i>
                        <input autocomplete="off" class="validate" id="landlines'+indexs+'" name="landlines[<?= $key ?>]" type="tel" <?php if(!empty($editlandline[$key])){ ?> value="<?= $editlandline[$key]?>" <?php } ?> placeholder="Landline No" />
                        </div>
                        <div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i>
                        <select class="browser-default getDesignation" id="desc'+indexs+'" name="descs[<?= $key ?>]" style="margin-left: 25px">
                        <option>Select Designation</option>
                        <?php foreach ($crm_contax as $desc): ?>
                        <option <?php if(!empty($editcondes[$key]) && $editcondes[$key] == $desc->id){ echo "selected"; }?> value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>
                        <?php endforeach; ?>
                        </select></div>
                        <div class="input-field col s6 m6">
                        <center><button type="button" onclick="removemoreBox('+indexs+')" class="btn btn-sm btn-danger">Remove<i class="fa fa-times" style="margin-left:10px"></i>
                        </button></center></div><div class="clearfix">
                        </div></br></br>
                    <?php }}?>
                    <!--End More Options-->
                    <div class="clearfix">

                    </div><br><br>
                    <div id="insertmoreBefore"></div>
                    <div class="clearfix">

                    </div>
                    <button type="button" id="moreButton" class="btn btn-sm btn-info" style="margin-bottom: 20px">
                        Add More <i class="fa fa-plus"></i>
                    </button>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <input type="hidden" id="goto" name="goto" value="0"/>
                        <button class="btn myblue waves-light add_submit" style="padding:0 5px;" type="submit"  name="edit">Edit
                            <i class="material-icons right">save</i>
                        </button>

                    </div>
                </form>

            </div>
        </div>
    </div>

</div>

        <?php } ?>

    </div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
</body>

</html>
<script>

    var $insertBefore = $('#insertBefore');
    var $i = 0;
    // Add More Inputs
    $('#plusButton').click(function(){

        $i = $i+1;
        var indexs = $i+1;
        //append Options
        $('<div id="addMoreBox'+indexs+'" class="clearfix"> ' +
            '<div class="input-field col s6 m6"><h6 style="text-align: center">Start Point</h6><div class="col s6 m6">' +
            '<div class="col s1 m1" style="text-align: center; margin-top: 30px;">' +
            '<i class="material-icons prefix">flag</i></div><label>Select State</label>' +
            '<select class="browser-default st_states" id="st_states'+indexs+'"  name="st_states['+$i+']" multiple tabindex="-1"><option value="">Select State</option>' +
            '<?php foreach($states as $state):?>' +
            '<option value="<?=$state->id; ?>"><?= $state->name; ?></option>' +
            '<?php endforeach;?>' +
            '</select></div>' +
            '<div class="col s6 m6">' +
            '<div class="col s1 m1" style="text-align: center; margin-top: 30px;"><i class="material-icons prefix">adjust</i></div> <label>Select District</label>' +
            '<select class="browser-default" id="st_district'+indexs+'"  name="st_district['+$i+']" multiple tabindex="-1"></select></div></div>' +
            '<div class="input-field col s6 m6"><h6 style="text-align: center">End Point</h6><div class="col s6 m6">' +
        '<div class="col s1 m1" style="text-align: center; margin-top: 30px;">' +
        '<i class="material-icons prefix">flag</i></div><label>Select State</label>' +
        '<select class="browser-default ed_states" id="ed_states'+indexs+'"  name="ed_states['+$i+']" multiple tabindex="-1"><option value="">Select State</option>' +
        '<?php foreach($states as $state):?>' +
        '<option value="<?=$state->id; ?>"><?= $state->name; ?></option>' +
        '<?php endforeach;?>' +
        '</select></div>' +
        '<div class="col s6 m6">' +
        '<div class="col s1 m1" style="text-align: center; margin-top: 30px;"><i class="material-icons prefix">adjust</i></div> <label>Select District</label>' +
        '<select class="browser-default ed_district" id="ed_district'+indexs+'"  name="ed_district['+$i+']" multiple tabindex="-1"></select></div></div>' +
            '<div class="input-field col s6 m6"><button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger">Remove<i class="fa fa-times"></i></button></div></div><div class="clearfix"></div></br></br>').insertBefore($insertBefore);

        $('#st_states'+indexs).select2()
        $('#ed_states'+indexs).select2()
        $('#st_district'+indexs).select2()
        $('#ed_district'+indexs).select2()
    });
    // Remove fields
    function removeBox(index){
        $('#addMoreBox'+index).remove();
    }


</script>
<script>

    var $insertmoreBefore = $('#insertmoreBefore');
    var $i = 0;
    // Add More Inputs
    $('#moreButton').click(function(){

        $i = $i+1;
        var indexs = $i+1;
        //append Options
        $('<div id="addMore'+indexs+'" class="clearfix"> ' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">account_circle</i><input autocomplete="off" class="validate" id="names'+indexs+'" name="names['+$i+']" type="text" value="" placeholder="Full Name"/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i><input autocomplete="off" class="validate" id="emails'+indexs+'" name="emails['+$i+']" type="email" value="" placeholder="Email"/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_phone</i><input autocomplete="off" class="validate valid_no" id="contacts'+indexs+'" name="contacts['+$i+']" type="text" value="" onblur="validateContact(contacts'+indexs+')" placeholder="Contact No."/></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_phones</i><input autocomplete="off" class="validate" id="landlines'+indexs+'" name="landlines['+$i+']" type="tel" value="" placeholder="Landline No" /></div>' +
            '<div class="input-field col s6 m6"><i class="material-icons prefix">contact_mail</i>' +
            '<select class="browser-default getDesignation" id="desc'+indexs+'" name="descs['+$i+']" style="margin-left: 25px">' +
            '<option>Select Designation</option>' +
            '<?php foreach ($crm_contax as $desc): ?>' +
            '<option value="<?php echo $desc->id; ?>" ><?php echo $desc->name; ?></option>'+
            '<?php endforeach; ?>' +
            '</select></div>' +
            '<div class="input-field col s6 m6"><center><button type="button" onclick="removemoreBox('+indexs+')" class="btn btn-sm btn-danger">Remove<i class="fa fa-times" style="margin-left:10px"></i></button></center></div>' + '<div class="clearfix">\n' +
            '</div></br></br></div>').insertBefore($insertmoreBefore);
    });
    // Remove fields
    function removemoreBox(index){
        $('#addMore'+index).remove();
    }

</script>
<script>
    $(document).ready(function(){
        $('.user').select2();
        $('.pcat').select2();
        $('.states').select2();
        $('.edstates').select2();
        $('.eddistrict').select2();
        $('.district').select2();

        $('.pcat').change(function () {
            var pcid = $(this).val();
            var ccid = $('.ccid').val();
            $.ajax({
                type: "POST",
                url: "get-price",
                data:'pcid='+pcid+'&ccid='+ccid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.price').focus();
                    $('.price').val(data);

                }
            });
        });
        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $(".edstates").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".eddistrict").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });

    });
    $(document).on('click','.cust_transportor_delete',function () {

        if (confirm("Are you sure?")) {
            var cid = $(this).val();
//            alert(cid);
//            var a = $(this);
            $.ajax({
                type: "POST",
                url: "cust_transportor_delete",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
                success: function(data){

                    alert(data);
                    location.reload();
                }
            });
        }
        else{

        }
    });

    $('#add_profile').on('click', function(){
        $('#goto').val('1');
        $('#addForm button[type=submit]').trigger('click');
    });

    $(document).on('keyup', '.listcust', function () {
        var key = $(this).val();
        $.ajax({
            type: "POST",
            url: "get-refered-by",
            data:'key='+key+'&_token=<?=csrf_token(); ?>',
            success: function(data){
                var stringObj = JSON.stringify(data);
                $('.listcust').val(stringObj);

            }
        });
    });
</script>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />



    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">

        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" >
                <?=$notices;?>
                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                    <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Project Sub Type</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light" style="padding:0 5px;"  href="project-subtype-master?add">
                            <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                        </a>
                    </div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive">
                                    <table id="table" class="responsive display">
                                        <thead>
                                        <tr role="row">
                                            <th>Sr.No.</th>
                                            <th>Type</th>
                                            <th>Sub Type</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>

                    </div>
                </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Category</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="project-subtype-master" class="col s12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m3">
                                <i class="material-icons prefix" style="margin-top: 10px">list</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                <p>Project Type</p>
                                <select  class="browser-default ptype" name="ptype" tabindex="-1">
                                    <?php foreach ($project_type as $ptype){
                                        ?>
                                        <option value="<?=$ptype->id?>"> <?=$ptype->project_type?> </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="input-field col s12 m6">
                                <p>&nbsp;</p>
                                <i class="material-icons prefix">dns</i>
                                <input id="Full_Name" type="text" class="validate" name="subtype">
                                <label for="Full_Name">Sub Type Name</label>
                            </div>
                            <div class="input-field col s12 m3" style="text-align: center">
                                <button class="btn myblue waves-light add_submit" type="submit" name="add">Save
                                    <i class="material-icons right">save</i>
                                </button>
                            </div>
                        </div>
                    </form>
                <?php } ?>

            <?php if(isset($_GET['edit'])){
                $cid = $_GET['edit'];
                $get_subtype = DB::select("SELECT * FROM `crm_project_subtype` INNER JOIN crm_project_type ON crm_project_subtype.project_type_id = crm_project_type.id WHERE crm_project_subtype.id = '$cid'")[0];
                ?>
                <h5 class="card-title">Edit Category Details</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                <form method="post" action="project-subtype-master" class="col s12 m12">
                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <input type="hidden" class="ccid" name="cid" value="<?php echo $cid; ?>">
                    <div class="row">
                        <p>Project Type</p>
                        <div class="input-field col s12 m3">
                            <i class="material-icons prefix">list</i>
                            <input id="population" type="text" class="validate" disabled   value="<?=$get_subtype->project_type?>">
                            <input  type="hidden" name="ptype"  value="<?=$get_subtype->id?>">
                            <label for="population">Type</label>
                        </div>

                        <div class="input-field  col s12 m6">
                            <i class="material-icons prefix">dns</i>
                            <input id="Full_Name1" type="text" class="validate" name="subtype" value="<?=$get_subtype->project_subtype?>">
                            <label for="Full_Name1">Sub Type Name</label>

                        </div>


                        <div class="input-field col s12 m3" style="text-align: center">
                            <button class="btn myblue waves-light edit_submit" type="submit" name="edit">Edit
                                <i class="material-icons right">save</i>
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
            <?php } ?>
    </div>
</div>
</div>
</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>
<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-projects-subtype") ?>",
                type: 'GET',
                data: function (d) {

                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'project_type', name: 'crm_project_type.project_type' },
                { data: 'project_subtype', name: 'project_subtype' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('#table').attr('style', 'width: 100%');
        $('.pcat').select2();

        $('.pcat').change(function () {
            var pcid = $(this).val();
            var ccid = $('.ccid').val();
            $.ajax({
                type: "POST",
                url: "get-price",
                data:'pcid='+pcid+'&ccid='+ccid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.price').focus();
                    $('.price').val(data);

                }
            });
        });
    });




    $(document).on('click','.project_subtype_delete',function () {
        if (confirm("Are you sure?")) {
            var cid = $(this).val();

            $.ajax({
                type: "POST",
                url: "project_subtype_delete",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',

                success: function(data){
                    alert(data);
                    location.reload();
                }
            });
        }
        else{

        }
    });

</script>
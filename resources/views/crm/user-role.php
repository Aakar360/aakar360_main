<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />

    <style>
        [type='checkbox'].filled-in:checked + span:not(.lever):after {
            border: 2px solid #3949ab;
            background-color: #3949ab;
        }

        #sel_all, #dsel_all{
            font-weight: bold;
        }
        .sel_all, .dsel_all:hover{
            font-weight: bold;
            cursor: pointer;
        }
    </style>



    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" >

                <?=$notices;?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                    <h5 class="card-title"style="padding: 5px; color: #0d1baa;">Role Master</h5>

                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light" style="padding:0 5px;"  href="user-role?add">
                            <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                        </a>
                    </div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive">
                            <table id="page-length-option" class="responsive display">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $i=1; foreach ($roles as $role){ $rid = $role->id;?>
                                    <tr>
                                        <td>
                                            <?=$i;?>
                                        </td>
                                        <td><?= $role->role?></td>
                                        <td>
                                            <a class="btn myblue waves-light" style="padding:0 10px; margin-left: 10px" href="user-role?edit=<?=$rid?>"><i class="material-icons">edit</i></a>
                                            <!--<button value="<?/*=$uid*/?>" class="cust_role_delete"><i class="material-icons right">delete</i></button>-->
                                        </td>
                                    </tr>
                                    <?php
                                    $i++; }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New Role</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="user-role" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m12">
                                <i class="material-icons prefix">supervisor_account</i>
                                <input id="role" type="text" class="validate" name="role">
                                <label for="role">Role</label>
                            </div>
                        </div>
                        <!--Below Check Boxes for Pages-->
                        <div class="row">
                            <div class="input-field col s12 m12">
                                <label>Permissions: (<a class="sel_all">Select All</a> / <a class="dsel_all">Deselect All</a>)</label>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="camp_sms" /><span>Campaign SMS</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="quot"  /><span>Quotation</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="pric_ent"  /><span>Price Entry</span></label></p>
                                     </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="ar_data"  /><span>Area Data</span></label></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="cst_data"  /><span>Customer Data</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="conv_cst"  /><span>Converted Customers</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="vst_rep"  /><span>Visit Report</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="tkt_rep"  /><span>Ticket Report</span></label></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="bpen_dw" /><span>Brand Penetration (DW)</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="bpen_sw"  /><span>Brand Penetration (SW)</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="scomp_dw"  /><span>Supplier Competition (DW)</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="scomp_sw"  /><span>Supplier Competition (SW)</span></label></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_cst" /><span>Customer Master</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_sup"  /><span>Supplier Master</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_usr"  /><span>User Master</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in " name="permission[]" value="mas_qe_rep" /><span>Quick Entry Report</span></label></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_rol" /><span>Role Master</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_dis"  /><span>District Master</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_block" /><span>Block Master</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_area"  /><span>Area/ Locality Master</span></label></p>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_cat"  /><span>Category Master</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_pgroup"  /><span>Price Groups Master</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_proj_stype" /><span>Project Sub Type</span></label></p>
                                    </div>

                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="export" /><span>Data Export</span></label></p>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="cv_freq" /><span>Call/Visit Frequency</span></label></p>
                                    </div>
									<div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="verify_cust" /><span>Verify Customer</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_des" /><span>Designation Master</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="sel_usr" /><span>Managed By & Above</span></label></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="no_order" /><span>No Order Reason</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="un_reach" /><span>Unreachable Report</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_scr" /><span>Score</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_cont" /><span>Contax Master</span></label></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="tran_mas" /><span>Transportor Master</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="tran_repo" /><span>Transportor Report</span></label></p>
                                    </div>
                                    <div class="col m3">
                                        <p><label><input type="checkbox" class="filled-in" name="permission[]" value="tran_veri" /><span>Transportor Verify</span></label></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Check Boxes Selection End-->
                        <div class="row">
                            <div class="input-field col s12 m12" style="text-align:center;margin-bottom: 20px">
                                <button class="btn myblue waves-light add_submit" style="padding:0 5px;" type="submit"  name="add">SAVE
                                    <i class="material-icons right" style="margin-left:3px">save</i>
                                </button>
                            </div>
                        </div>
                    </form>
                <?php } ?>

                <?php if(isset($_GET['edit'])){
                    $uid = $_GET['edit'];
                    $Role = DB::select("SELECT * FROM `crm_user_role` WHERE id = '$uid'")[0];
                    ?>
                            <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Edit Role Details </h5>
                                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                                    <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                                    </a>
                                </div>
                            <form method="post" action="user-role" class="col s12 m12">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden"  name="uid" value="<?php echo $uid; ?>">
                                <div class="row">
                                    <div class="input-field col s12 m12">
                                        <i class="material-icons prefix">supervisor_account</i>
                                        <input id="role1" type="text" class="validate" name="role" value="<?=$Role->role?>">
                                        <label for="role1">Name</label>
                                    </div>
                                </div>

                                <?php
                                    $permissions = explode(",",$Role->permissions);
                                ?>

                                <!--Below Check Boxes for Pages-->
                                <div class="row">
                                    <div class="input-field col s12 m12">
                                        <label>Permissions: (<a class="sel_all">Select All</a> / <a class="dsel_all">Deselect All</a>)</label>
                                        <br>
                                        <br>
                                        <div class="row">
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="camp_sms" <?php if(in_array( "camp_sms" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Campaign SMS</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="quot"  <?php if(in_array( "quot" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Quotation</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="pric_ent" <?php if(in_array( "pric_ent" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Price Entry</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="ar_data" <?php if(in_array( "ar_data" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Area Data</span></label></p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="cst_data" <?php if(in_array( "cst_data" ,$permissions)){ echo 'checked="checked"'; } ?>  /><span>Customer Data</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="conv_cst" <?php if(in_array( "conv_cst" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Converted Customers</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="vst_rep" <?php if(in_array( "vst_rep" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Visit Report</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="tkt_rep" <?php if(in_array( "tkt_rep" ,$permissions)){ echo 'checked="checked"'; } ?>  /><span>Ticket Report</span></label></p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="bpen_dw" <?php if(in_array( "bpen_dw" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Brand Penetration (DW)</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="bpen_sw" <?php if(in_array( "bpen_sw" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Brand Penetration (SW)</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="scomp_dw" <?php if(in_array( "scomp_dw" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Supplier Competition (DW)</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="scomp_sw" <?php if(in_array( "scomp_sw" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Supplier Competition (SW)</span></label></p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_cst" <?php if(in_array( "mas_cst" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Customer Master</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_sup" <?php if(in_array( "mas_sup" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Supplier Master</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_usr" <?php if(in_array( "mas_usr" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>User Master</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in " name="permission[]" value="mas_qe_rep" <?php if(in_array( "mas_qe_rep" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Quick Entry Report</span></label></p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_rol" <?php if(in_array( "mas_rol" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Role Master</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_dis" <?php if(in_array( "mas_dis" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>District Master</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_block" <?php if(in_array( "mas_block" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Block Master</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_area" <?php if(in_array( "mas_area" ,$permissions)){ echo 'checked="checked"'; } ?>  /><span>Area/ Locality Master</span></label></p>
                                            </div>


                                        </div>

                                        <div class="row">
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_cat" <?php if(in_array( "mas_cat" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Category Master</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_pgroup" <?php if(in_array( "mas_pgroup" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Price Groups Master</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_proj_stype" <?php if(in_array( "mas_proj_stype" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Project Sub Type</span></label></p>
                                            </div>

                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="export" <?php if(in_array( "export" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Data Export</span></label></p>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="cv_freq" <?php if(in_array( "cv_freq" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Call/Visit Frequency</span></label></p>
                                            </div>
											<div class="col m3">
												<p><label><input type="checkbox" class="filled-in" name="permission[]" value="verify_cust" <?php if(in_array( "verify_cust" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Verify Customer</span></label></p>
											</div>
                                            <div class="col m3">
												<p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_des" <?php if(in_array( "mas_des" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Designation Master</span></label></p>
											</div>
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="sel_usr" <?php if(in_array( "sel_usr" ,$permissions)){ echo 'checked="checked"'; } ?> /><span>Managed By & Above</span></label></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="no_order" <?php if(in_array( "no_order" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>No Order Reason</span></label></p>
                                            </div>
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="un_reach" <?php if(in_array( "un_reach" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Unreachable Report</span></label></p>
                                            </div>
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_scr" <?php if(in_array( "mas_scr" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Score</span></label></p>
                                            </div>
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="mas_cont" <?php if(in_array( "mas_cont" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Contax Master</span></label></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="tran_mas" <?php if(in_array( "tran_mas" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Transportor Master</span></label></p>
                                            </div>
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="tran_repo" <?php if(in_array( "tran_repo" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Transportor Report</span></label></p>
                                            </div>
                                            <div class="col m3">
                                                <p><label><input type="checkbox" class="filled-in" name="permission[]" value="tran_veri" <?php if(in_array( "tran_veri" ,$permissions)){ echo 'checked="checked"'; } ?>/><span>Transportor Verify</span></label></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Check Boxes Selection End-->

                                <div class="row">
                                    <div class="input-field col s12 m12" style="text-align:center;margin-bottom: 20px">
                                        <button class="btn myblue waves-light edit_submit" style="padding:0 5px;" type="submit" name="edit">SAVE
                                            <i class="material-icons right" style="margin-left:3px">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                <?php } ?>
            </div>

        </div>

       </div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
</body>

</html>
<script>
    $(document).ready(function(){
        $('.pcat').select2();

        $('.pcat').change(function () {
            var pcid = $(this).val();
            var ccid = $('.ccid').val();
            $.ajax({
                type: "POST",
                url: "get-price",
                data:'pcid='+pcid+'&ccid='+ccid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.price').focus();
                    $('.price').val(data);

                }
            });
        });
    });


    $(document).on('click','.cust_category_delete',function () {
        if (confirm("Are you sure?")) {
            var cid = $(this).val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "customer_category_delete",
                data:'cid='+cid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    location.reload();
                }
            });
        }
        else{

        }
    });

    $(document).on('click','.sel_all',function(){
        $(".filled-in").prop("checked",true);
    });

    $(document).on('click','.dsel_all',function(){
        $(".filled-in").prop("checked",false);
    });

</script>
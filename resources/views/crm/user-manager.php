<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <!--<link rel="stylesheet" type="text/css" href="<? //$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />



    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content">
                <?=$notices;?>
                <?php
                    if($errors->any()){
                        ?>
                        <div class="card-alert card red">
                            <div class = "card-content white-text" >
                                <p><?=$errors->first()?> </p>
                            </div>
                            <button type = "button" class = "close white-text" data-dismiss = "alert" aria-label = "Close">
                                <span aria-hidden = "true" >×</span>
                            </button> </div>
                        <?php
                    }
                ?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                    <h5 class="card-title" style="padding: 5px; color: #0d1baa;">User Master</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light" style="padding:0 5px;"  href="user-manager?add">
                        <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                    </a>
                </div>
                    <div class="row" style="position:relative;">
                        <div class="col s12 table-responsive">
                            <table id="table" class=" display">
                                <thead>
                                <tr role="row">
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th>Display Name</th>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add New User </h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="user-manager" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="Full_Name" type="text" class="validate" name="name" required>
                                <label for="Full_Name">Name</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">person</i>
                                <input id="displayName" type="text" class="validate" name="display_name" required>
                                <label for="displayName">Display Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">person</i>
                                <input id="username" type="text" class="validate" name="username" required>
                                <label for="username">Username</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <i class="material-icons prefix">security</i>
                                <input id="password" type="password" class="validate" name="password" required>
                                <label for="password">Password</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6 m6" style="margin-top: 0px;">

                                <i class="material-icons prefix" style="margin-top: 10px">supervisor_account</i>
                                <div class="col s12 m12" style="margin-left: 20px">
                                    Select Role
                                    <select class="browser-default role" name="role" tabindex="-1" style="width: 100% !important;" required>
                                        <?php foreach ($roles as $role){
                                            ?>
                                            <option value="<?=$role->id?>"> <?=$role->role?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>
                            <div class="input-field col s6 m6" style="margin-top: 0px;">

                                <i class="material-icons prefix" style="margin-top: 10px">supervisor_account</i>
                                <div class="col s12 m12" style="margin-left: 23px">
                                    <label>Select Designation</label>
                                    <select class="browser-default" name="desigantion"  tabindex="-1" style="width: 100% !important;" > type
                                        <option> Select Designation</option>
                                        <?php foreach ($designations as $designation){
                                            ?>
                                            <option value="<?='des'.$designation->id?>"> <?=$designation->name?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>


                            <div class="input-field col s12 m12" style="text-align: center">
                                <button class="btn myblue waves-light add_submit" type="submit"  name="add">SAVE
                                    <i class="material-icons right">save</i>
                                </button>
                            </div>
                        </div>



                    </form>
                <?php } ?>

                <?php if(isset($_GET['edit'])){

                    $uid = $_GET['edit'];
                    $users = DB::select("SELECT * FROM `user` WHERE u_id = '$uid'")[0];

                    ?>
                            <h5 class="card-title">Edit User Details</h5>
                            <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                                <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                                    <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                                </a>
                            </div>
                            <form method="post" action="user-manager" class="col s12 m12" autocomplete="off">
                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                <input type="hidden"  name="uid" value="<?php echo $uid; ?>">
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="Full_Name" type="text" class="validate" name="name" value="<?=$users->u_name?>" required>
                                        <label for="Full_Name">Name</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">person</i>
                                        <input id="displayName" type="text" class="validate" name="display_name" value="<?=$users->display_name?>" required>
                                        <label for="displayName">Display Name</label>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">person</i>

                                        <input id="username" type="text" class="validate" name="username" value="<?=$users->u_email?>" autocomplete="off" required>
                                        <label for="username">Username</label>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        <i class="material-icons prefix">security</i>
                                        <input id="password" type="password" class="validate" name="password" value="" autocomplete="off">
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6 m6" style="margin-top: 0px;">
                                        <i class="material-icons prefix" style="margin-top: 10px">supervisor_account</i>
                                        <div class="col s12 m12" style="margin-left: 20px">
                                            Select Role
                                            <select class="browser-default role" name="role" tabindex="-1" style="width: 100% !important;" required>
                                                <?php
                                                $r_id =  $users->role_id;
                                                $rid_array = explode(',',$r_id);
                                                foreach ($roles as $role){
                                                    ?>
                                                    <option <?=(in_array($role->id,$rid_array))?'selected':''; ?> value="<?=$role->id?>"> <?=$role->role?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s6 m6" style="margin-top: 0px;">

                                        <i class="material-icons prefix" style="margin-top: 10px">supervisor_account</i>
                                        <div class="col s12 m12" style="margin-left: 23px">
                                            <label>Select Designation</label>

                                            <select class="browser-default" name="desigantion"  tabindex="-1" style="width: 100% !important;" > type
                                                <option> Select Designation</option>
                                                <?php foreach ($designations as $designation){

                                                    ?>
                                                    <option <?php if($users->desigantion_id == 'des'.$designation->id){ echo "selected"; } ?> value="<?='des'.$designation->id?>"> <?=$designation->name?> </option>
                                                    <?php
                                                }
                                                ?>   
                                            </select>
                                        </div>

                                    </div>
                                    <div class="input-field col s12 m12" style="text-align: center;margin-bottom: 20px">
                                        <button class="btn myblue waves-light edit_submit" type="submit" name="edit">SAVE
                                            <i class="material-icons right">save</i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                <?php } ?>
            </div>
        </div>
    </div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>
<script>
    $('.user').select2();

    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-users") ?>",
                type: 'GET',
                data: function (d) {

                }
            },
            columns: [
                { data: 'u_id', name: 'u_id' },
                { data: 'u_name', name: 'u_name' },
                { data: 'display_name', name: 'display_name' },
                { data: 'u_email', name: 'u_email' },
                { data: 'role_id', name: 'role_id' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('#table').attr('style', 'width: 100%');
        $('.pcat').select2();
        $('.role').select2();

        $('.pcat').change(function () {
            var pcid = $(this).val();
            var ccid = $('.ccid').val();
            $.ajax({
                type: "POST",
                url: "get-price",
                data:'pcid='+pcid+'&ccid='+ccid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('.price').focus();
                    $('.price').val(data);

                }
            });
        });
    });


    $(document).on('click','.cust_manage_delete',function () {
        if (confirm("Are you sure?")) {
            var pid = $(this).val();
            var a = $(this);
            $.ajax({
                type: "POST",
                url: "customer_user_delete",
                data:'pid='+pid+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(a).html('Wait...');
                },
                success: function(data){
                    alert(data);
                    location.reload();
                }
            });
        }
        else{

        }
    });

</script>
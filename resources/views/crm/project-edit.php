<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    td:first-child, th:first-child {
        position:sticky;
        left:0;
        z-index:1;
        background-color:white;
    }
    td:nth-child(2),th:nth-child(2)  {
        position:sticky;
        left:40px;
        z-index:1;
        background-color:white;
    }
    .tableFixHead th {
        position: sticky;
        top: 0;
        background: #ffffff;
        z-index:2
    }
    th:first-child , th:nth-child(2) {
        z-index:3

    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs">
            <div class="card-content" style="min-height: 500px;">
                <?=$notices;?>
<h6>Edit Project :: <?php echo  $projects->id; ?></h6>
                            <div class="row">
                                <div id="Project">
                                    <center>
                                        <a class="btn myblue waves-light " style="padding:0 5px;" href="customer-profile?cid=<?=isset($_GET['cid']) ? $_GET['cid'] : 'customer-data'?>" >
                                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                                        </a>
                                    </center>
                                    <div class="project_form">

                                            <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12">
                                                <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                                <input type="hidden" name="project_id" value="<?php echo  $projects->id; ?>">
                                                <input type="hidden" name="crm_customer_id" value="<?php echo  $projects->crm_customer_id; ?>">

                                                <div class="row">
                                                    <div class="input-field col s12 m6">
                                                        <i class="material-icons prefix">account_circle</i>
                                                        <input type="text"  id="pname" name="pname" value="<?=$projects->project_name; ?>">
                                                        <label for="pname">Project Name</label>
                                                    </div>
                                                    <div class="input-field col s12 m3">
                                                        <i class="material-icons prefix">view_headline</i>
                                                        <select name="ptype" class="ptype" > type
                                                            <option value="" disabled selected>Select Project Type</option>
                                                            <option <?=($projects->project_type=='1')?'selected':''; ?> value="1" >Private</option>
                                                            <option <?=($projects->project_type=='2')?'selected':''; ?> value="2">Government</option>
                                                        </select>
                                                        <label for="ptype">Project Type</label>
                                                    </div>
                                                    <div class="input-field col s12 m3" style="margin-top: 0px;">
                                                        Project Sub Type
                                                        <select class="browser-default type1" name="pstype[]" multiple tabindex="-1" style="width: 100% !important;" > type
                                                            <?php $project_ids =  $projects->project_subtype;
                                                            $pid_array = explode(',',$project_ids);
                                                            foreach ($project_subtype as $subtype){

                                                                ?>
                                                                <option <?=(in_array($subtype->id,$pid_array))?'selected':''; ?> value="<?=$subtype->id?>"><?=$subtype->project_subtype?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12 m6">
                                                        <i class="material-icons prefix">view_headline</i>
                                                        <select name="pstatus" class="type"> status
                                                            <option value="Ongoing">Ongoing</option>
                                                            <option value="Start">Start</option>
                                                            <option value="Completed">Completed</option>
                                                        </select>
                                                        <label for="status">Status</label>
                                                    </div>
                                                    <div class="input-field col s12 m6">
                                                        <i class="material-icons prefix">view_headline</i>
                                                        <input type="text"  id="sqft" name="sqft" value="<?=$projects->sqft; ?>">
                                                        <label for="sqft">Total Sq.Feet</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12 m6">
                                                        <i class="material-icons prefix">flag</i>
                                                        <div class="col s12 m12" style="margin-left: 23px">
                                                            <select  class="browser-default states state_list" name="state" style="width: 100%" required>
                                                                <option value="">Select State</option>
                                                                <?php
                                                                foreach ($states as $state){
                                                                    ?>
                                                                    <option <?=($projects->state == $state->id) ? 'selected' : '' ?> value="<?=$state->id;?>"><?=$state->name?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s12 m6" >
                                                        <i class="material-icons prefix">adjust</i>
                                                        <div class="col s12 m12" style="margin-left: 23px">
                                                            <select  class="browser-default district dist_list" id="district" name="district" style="width: 100%" required>
                                                                <option value="">Select District</option>
                                                                <?php
                                                                    if($districtsx){
                                                                        foreach ($districtsx as $dist){
                                                                            ?>
                                                                            <option <?=($projects->district == $dist->id) ? 'selected' : '' ?> value="<?=$dist->id?>"><?=$dist->name?></option>
                                                                <?php
                                                                        }
                                                                    }

                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s12 m6" >
                                                        <i class="material-icons prefix">adjust</i>
                                                        <div class="col s12 m12" style="margin-left: 23px">
                                                            <select  class="browser-default block" name="block" style="width: 100%" required>
                                                                <option value="">Select District</option>
                                                                <?php
                                                                if($blocksx){
                                                                    foreach ($blocksx as $block){
                                                                        ?>
                                                                        <option <?=($projects->block == $block->id) ? 'selected' : '' ?> value="<?=$block->id?>"><?=$block->block?></option>
                                                                        <?php
                                                                    }
                                                                }

                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s12 m4" >
                                                        <i class="material-icons prefix">location_on</i>
                                                        <div class="col s12 m12" style="margin-left: 23px">
                                                            <select  class="browser-default locality local_list" id="locality" name="locality" style="width: 100%">
                                                                <option value="">Select Locality</option>\
                                                                <?php
                                                                if($localityx){
                                                                    foreach ($localityx as $local){
                                                                        ?>
                                                                        <option <?=($projects->locality == $local->id) ? 'selected' : '' ?> value="<?=$local->id?>"><?=$local->locality?></option>
                                                                        <?php
                                                                    }
                                                                }

                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="input-field col s12 m2">
                                                        <a href="#open-modal3" class="btn myblue waves-light" style="padding: 0 5px; width: fit-content;">Add New
                                                            <i class="material-icons right">add_circle_outline</i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12 m6">
                                                        <i class="material-icons prefix">account_circle</i>
                                                        <input type="text"  id="paddress" name="paddress" value="<?=$projects->project_address; ?>">
                                                        <label for="paddress">Project Address</label>
                                                    </div>
                                                    <div class="input-field col s12 m6">
                                                        <i class="material-icons prefix">account_circle</i>
                                                        <input type="text"  id="cpname" name="cpname" value="<?=$projects->person_name; ?>">
                                                        <label for="cpname">Contact Person Name</label>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="input-field col s12 m6">
                                                        <i class="material-icons prefix">account_circle</i>
                                                        <input type="text"  id="cpnumber" name="cpnumber" value="<?=$projects->person_phone; ?>">
                                                        <label for="cpnumber">Contact Person Number</label>
                                                    </div>
                                                    <div class="input-field col s12 m6">
                                                        <i class="material-icons prefix">account_circle</i>
                                                        <input type="text"  id="cpdesignation" name="cpdesignation"  value="<?=$projects->person_designation; ?>">
                                                        <label for="cpdesignation">Contact Person Designation</label>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="input-field col s12 m12">
                                                        <i class="material-icons prefix">view_headline</i>
                                                        <input id="Description4" type="tel" class="validate" name="description" value="<?=$projects->description; ?>">
                                                        <label for="Description4">Description</label>
                                                    </div>

                                                </div>

                                                <div class="row" style=" height: 200px;">
                                                    <div class="input-field col s12 m6">
                                                        <div class="file-field">
                                                            <div class="btn myblue waves-light left">
                                                                <span>Choose file</span>
                                                                <input type="file" name="image" style="width: 100%" class="btn myblue" id="project_image">
                                                            </div>
                                                            <div class="file-path-wrapper">
                                                                <input class="file-path validate" type="text" placeholder="Upload your file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="input-field col s12 m3">
                                                        <p id="project_img_div">
                                                            <img id="project_image_show" src="#" height="200px" width="200px"  />
                                                        </p>
                                                    </div>
                                                    <div class="input-field col s12 m3">
                                                        <center><button class="btn myblue waves-light " type="submit" name="edit_project">Save
                                                            <i class="material-icons right">save</i>
                                                        </button></center>
                                                    </div>
                                                </div>

                                            </form>
                                    </div>

                                </div>
                            </div>

            </div>
        </div>
    </div>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

</body>

</html>
<script>
    $(document).ready(function(){
        $('.type1').select2();
        $('.variants').select2();

        $('.states').select2();
        $('.district').select2();
        $('.block').select2();
        $('.locality').select2();
        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $(".district").html(data);
                }
            });
        });
        $('.block').change(function () {
            var did = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-locality",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',

                success: function(data){
                    $('#locality').html(data);
                }
            });
        });

        $('.district').change(function () {
            var did = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-block",
                data:'did='+did+'&_token=<?=csrf_token(); ?>',

                success: function(data){
                    $('#district').html(data);
                }
            });
        });
        $(".category").change(function(){
            var val = $(this).val();
            var cname = $(".category option:selected").html();

            $.ajax({
                type: "POST",
                url: "get-report-option",
                data:'cid='+val+'&cname='+cname+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data){
                    $(".report_div").html(data);
                    $("#model_report_type").val(data);
                }
            });
        });
        var get_view_type = $('#get_report_view').val();
        if(get_view_type=='brand_wise'){
            $('.brand_wise').show();
        }
        if(get_view_type=='price_wise'){
            $('.price_wise').show();
        }

        $('#localitySave').on('click', function(){
            var formData = $('form#localityForm').serialize();
            var btn = $(this);
            $.ajax({
                url: 'save-locality',
                type: 'post',
                data: formData,
                beforeSend: function(){
                    btn.html('Saving...');
                },
                success: function(data){
                    if(data == 'success') {
                        alert('Locality Saved.');
                    }else{
                        alert('Invalid Data.');
                    }
                    btn.html('Save');
                }
            });
        });


    });
</script>



<div id="open-modal3" class="modal-window">
    <div>
        <a href="#modal-close" title="Close" class="modal-close">close &times;</a>
        <h1>Add Area / Locality</h1>
        <div>
            <table border="0">
                <form method="post" id="localityForm">

                    <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                    <tr>
                        <td>District</td>
                        <td ><select name="district_pop" class="browser-default district" tabindex="-1">
                                <?php
                                foreach ($districts as $district){
                                    $s_id = $district->id;
                                    ?>
                                    <option  value="<?=$s_id;?>"><?=$district->name?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Locality Name</td>
                        <td ><input type="text" id="locality_pop" name="locality_pop" class="form-control" value=""></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="button" id="localitySave" class="btn myblue">Save</td>
                    </tr>
                </form>

            </table>

        </div>
    </div>
</div>
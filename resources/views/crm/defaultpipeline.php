<!-- Styles -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<!-- Scripts -->
<script src="<?=url('/assets/crm') ?>/js/jquery-3.5.1.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<!-- default pipeline -->
<div class="col s12 m12">
    <div class="row">
        <div class="col s8 m8">
            <h6 style="margin-top: 50px;">Pipeline Name</h6>
        </div>
        <div class="col s4 m4">
            <h6 style="margin-top: 50px;">Action</h6>
        </div>
    </div>
    <div class="row">
        <div class="col s8 m8">
            <input type="text" name="name" class="form-control" id="name" value="Default Pipeline">
        </div>
        <div class="col s4 m4" style="display: flex">
            <button class="btn myblue waves-light" style="padding:0 10px;" onclick="editTable();"><i class="material-icons">edit</i></button>
            <button id="deletepipeline" class="btn myred waves-light" style="padding:0 10px;" onclick="deleteTable();"><i class="material-icons">delete</i></button>
            <button   class="btn myblue waves-light" style="padding:0 10px;" onclick="insertTable();"><i class="material-icons right">save</i></button>
        </div>
    </div>

</div>
<div class="col s12 m12">
    <div class="row">
        <table class="table">
            <tbody>
            <tr class="success">
                <td >new</td>
                <td ></td>
            </tr>
            </tbody>
        </table>
        <table class="table" id="addDefaulttable">
            <tbody>
            <tr class="info">
                <td>Follow-Up</td>
                <td ><button id="deletepipeline" class="btn myred waves-light" style="padding:0 10px;" ><i class="material-icons">delete</i></button></td>
            </tr>
            <tr class="info">
                <td>Under Review</td>
                <td ><button id="deletepipeline" class="btn myred waves-light" style="padding:0 10px;" ><i class="material-icons">delete</i></button></td>
            </tr>
            <tr class="info">
                <td>Demo</td>
                <td ><button id="deletepipeline" class="btn myred waves-light" style="padding:0 10px;"><i class="material-icons">delete</i></button></td>
            </tr>
            <tr class="info">
                <td>Negotiation</td>
                <td ><button id="deletepipeline" class="btn myred waves-light" style="padding:0 10px;"><i class="material-icons">delete</i></button></td>
            </tr>

            </tbody>
        </table>
        <table class="table">
            <tbody>
            <tr class="success">
                <td id="won">Won</td>
                <td ></td>
            </tr>
            <tr class="success">
                <td id="lost">Lost</td>
                <td ></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<span>
    <button id="newadd" onclick="addMoreTable()">Add Deal Stage</button></span></br>
<input type="checkbox" id="addDefaultbtn" name="addDefault" value="">
<label for="addDefault">Marked as Default</label>
<!--default pipeline End-->
<script src="<?=url('/assets/crm')?>/js/bootstable.js" type="text/javascript"></script>
<script>

    function editTable() {
        $("#addDefaulttable").attr('contenteditable','true');
    }

    function insertTable() {
        jsonObj = [];
        $('#addDefaulttable  tr').each(function() {
            var stages = $(this).find("td").html();
            jsonObj.push(stages);

        });
        var jsonstring = JSON.stringify(jsonObj)
        var name = $("#name").val();
        $.ajax({
            type: "POST",
            url: "insert-pipeline",
            data:'_token=<?=csrf_token(); ?>'+'&name='+name+'&stages='+jsonstring,
            success: function(data){
                window.location.reload();
            }
        });
    }

    var $insertBefore = $('#insertBefore');
    var $i = 0;

    // Add More Inputs
    function addMoreTable() {

        $i = $i+1;
        markup = "<tr id='row"+$i+"'><td contenteditable>This is row "
            + $i + "</td><td><button type='button' onclick='removeBox($i)' class='btn btn-sm btn-danger'><i class='fa fa-times'></i></button></td></tr>";
        tableBody = $("#addDefaulttable");
        tableBody.append(markup);

    }


    // Remove fields
    function removeBox(index){
        $('#row'+index).remove();;
    }


</script>

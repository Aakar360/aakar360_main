<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    td:first-child, th:first-child {
        position:relative;
        left:0;
        z-index:1;
    }
    td:nth-child(2),th:nth-child(2)  {
        left:0px;
        z-index:1;
    }
    .tableFixHead th {
        position: relative;
        top: 0;
        z-index:2
        width: 1%;
    }
    th:first-child , th:nth-child(2) {
        z-index:3

    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div class="container">
            <div class="section section-data-tables">

                <div class="row" style="
    height: 125px;
">
                    <div class="col s12 m12 l12">
                        <div id="button-trigger" class="card card card-default scrollspy">
                            <div class="card-content" style="padding: 10px !important;">
                                <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Marketing Area Data</h5>
                                <div class="row">
                                    <form method="get">
                                        <div class="col s12 m12 xl4 ">
                                            <label>Select Category</label>
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">format_list_bulleted</i></div>
                                            <div class="col s10 m10">
                                            <select class="browser-default category" name="category" tabindex="-1">

                                                <?php $selected_category1 = explode(',',$selected_category);
                                                foreach ($category as $cate) {
                                                    $c_id = $cate->id;
                                                    $selected = '';
                                                    if(isset($_GET['category'])){
                                                        if($cate->id == $_GET['category']){
                                                            $selected = 'selected="selected"';
                                                        }
                                                    }
                                                    ?>
                                                    <option <?php if (in_array($c_id, $selected_category1)) { echo 'selected'; } else { echo ''; } ?>
                                                            value="<?= $c_id; ?>" <?=$selected?>><?= $cate->name ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="col s12 m12 xl4">
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">flag</i></div>
                                            <div class="col s10 m10">
                                            <label>Select State</label>
                                            <select class="browser-default states" id="states" name="state[]" multiple tabindex="-1">
                                                <?php
                                                foreach ($statesx as $variant) {
                                                    $v_id = $variant->id;
                                                    $selected = '';
                                                    if(isset($_GET['state'])){
                                                        if(in_array($variant->id, $_GET['state'])){
                                                            $selected = 'selected="selected"';
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?= $v_id; ?>" <?=$selected?>><?= $variant->name ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            </div>
                                        </div>

                                        <div class="col s12 m12 xl4">
                                            <div class="col s1 m1" style="text-align: center; margin-top: 30px"> <i class="material-icons prefix">adjust</i></div>
                                            <div class="col s10 m10">
                                            <label>Select District</label>
                                            <select class="browser-default district" id="district" name="district[]" multiple tabindex="-1">
                                                <?php if($districtsx){
                                                    foreach ($districtsx as $variant) {
                                                    $v_id = $variant->id;
                                                    $selected = '';
                                                    if(isset($_GET['district'])){
                                                        if(in_array($variant->id, $_GET['district'])){
                                                            $selected = 'selected="selected"';
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?= $v_id; ?>" <?=$selected?>><?= $variant->name ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                <?php }else{ ?>
                                                <option>Select State First</option>
                                                <?php } ?>
                                            </select>
                                            </div>
                                        </div>
                                        <!--div class="col s12 m2 report_div">
                                            <label>Select Report Type</label>
                                            <select class="browser-default" name="report_type" id="get_report_view">
                                                <option value="">Select Report Type</option>
                                                <option value="tabular_report">Tabular Report</option>
                                                <option value="price_wise">Price Wise</option>
                                            </select>
                                        </div-->
                                        <div class="col s12 m12" style="text-align: center">
                                            <button style="margin-top: 20px; padding: 0 5px" class="btn myblue waves-effect waves-light submit" type="submit">
                                                <i class="material-icons right">search</i>Search
                                            </button>
                                        </div>
                                    </form>
                                </div>

                <?php if($report_data){ ?>
                                    <h5 class="" style="font-size: 14px">Area Data Report (PCC - <?=$pcc;?>)</h5>
                                <div class="row" style="position:relative;">
                                    <div class="col s12 table-responsive">
                                            <div class="tableFixHead">
                                                <table class="table striped table-striped price_type_report" >
                                                    <!--<table id="page-length-option" class="display">-->
                                                    <thead>
                                                        <th>State</th>
                                                        <th>District</th>
                                                        <th>Population</th>
                                                        <th>Total Consumption (in Tons)</th>
                                                        <th>Contacted Customers</th>
                                                        <th>Interested Customers</th>
                                                        <th>Converted Customers</th>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sid = null;
                                                        $t_p = 0;
                                                        $t_c = 0;
                                                        $t_con= 0;
                                                        $t_int = 0;
                                                        $t_cus = 0;
                                                        $rend = true;
                                                        foreach($report_data as $rd){?>
                                                            <?php
                                                            if(is_null($sid)){
                                                                $sid[] = $rd['state'];
                                                            }elseif(!in_array($rd['state'], $sid)){?>
                                                                <tr class="color2">
                                                                    <td colspan="2" style="text-align: center; font-size: 18px">
                                                                        <b>Total</b>
                                                                    </td>
                                                                    <td><b><?=number_format($t_p, 0, '.', ',')?></b></td>
                                                                    <td><b><?=number_format($t_c, 0, '.', ',')?></b></td>
                                                                    <td><b><?=number_format($t_con, 0, '.', ',')?></b></td>
                                                                    <td><b><?=number_format($t_int, 0, '.', ',')?></b></td>
                                                                    <td><b><?=number_format($t_cus, 0, '.', ',')?></b></td>
                                                                </tr>
                                                                <?php
                                                                $sid[] = $rd['state'];
                                                                $t_p = 0;
                                                                $t_c = 0;
                                                                $t_con= 0;
                                                                $t_int = 0;
                                                                $t_cus=0;
                                                            }
                                                            $t_p = $t_p + $rd['population'];
                                                            $t_c = $t_c + $rd['consumption'];
                                                            $t_con = $t_con + $rd['contacted'];
                                                            $t_int = $t_int + $rd['interested'];
                                                            $t_cus = $t_cus + $rd['converted'];
                                                            ?>

                                                            <tr>
                                                                <td><?=$rd['state']?></td>
                                                                <td><?=$rd['district']?></td>
                                                                <td><?=number_format($rd['population'], 0, '.', ',')?></td>
                                                                <td><?=number_format($rd['consumption'], 0, '.', ',')?></td>
                                                                <td><?=number_format($rd['contacted'], 0, '.', ',')?></td>
                                                                <td><?=number_format($rd['interested'], 0, '.', ',')?></td>
                                                                <td><?=number_format($rd['converted'], 0, '.', ',')?></td>
                                                            </tr>


                                                        <?php }
                                                        if($rend){?>
                                                            <tr class="color2">
                                                                <td colspan="2">
                                                                    <b>Total</b>
                                                                </td>
                                                                <td><b><?=number_format($t_p, 0, '.', ',')?></b></td>
                                                                <td><b><?=number_format($t_c, 0, '.', ',')?></b></td>
                                                                <td><b><?=number_format($t_con, 0, '.', ',')?></b></td>
                                                                <td><b><?=number_format($t_int, 0, '.', ',')?></b></td>
                                                                <td><b><?=number_format($t_cus, 0, '.', ',')?></b></td>
                                                            </tr>
                                                        <?php }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php  } ?>

            <!-- DataTables example -->

            </div>
        </div>
    </div>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>

</body>

</html>
<script>
    $(document).ready(function(){
        $('.category').select2();
        $('.variants').select2();
        $('.price_wise').hide();
        $('.tabular_report').hide();

        $('.states').select2();
        $('.district').select2();

        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });

        /*$(".category").change(function(){
            var val = $(this).val();
            var cname = $(".category option:selected").html();

            $.ajax({
                type: "POST",
                url: "get-report-option",
                data:'cid='+val+'&cname='+cname+'&_token=',
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data){
                    var get_view_type = $('#get_report_view').val();
                    $(".report_div").html(data);
                    $("#model_report_type").val(data);
                }
            });
        });*/
        $(document).on('change','#get_report_view', function () {
            var get_view_type = $('#get_report_view').val();
            if(get_view_type=='tabular_report'){
                $('.tabular_report').show();
                $('.price_wise').hide();
                $('.brand_wise').hide();
            }
            if(get_view_type=='price_wise'){
                $('.price_wise').show();
                $('.brand_wise').hide();
                $('.tabular_report').hide();
            }
            if(get_view_type=='brand_wise'){
                $('.price_wise').hide();
                $('.brand_wise').show();
                $('.tabular_report').hide();
            }
        });





    });
</script>




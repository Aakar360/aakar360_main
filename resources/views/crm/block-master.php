<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title><?=$title; ?></title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=$tp; ?>/vendors/noUiSlider/nouislider.min.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">

    <!--<link rel="stylesheet" type="text/css" href="<? //$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />



    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->
<?=$header;?>
<div class="row">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 500px;">
                <?=$notices;?>

                <?php if(!isset($_GET['add']) && !isset($_GET['edit'])) {?>

                <h5 class="card-title" style="padding: 5px; color: #0d1baa;">Block Master</h5>
                    <div class="col s12 m6 xl3">
                        <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">flag</i></div>
                        <div class="col s10 m10">
                            <label>Select State</label>
                            <select class="browser-default states" id="states" name="states[]" multiple  tabindex="-1">
                                <?php
                                foreach ($states as $state) {
                                    $v_id = $state->id;
                                    ?>
                                    <option value="<?=$v_id; ?>"><?= $state->name ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col s12 m6 xl3"> <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
                        <div class="col s10 m10">
                            <label>Select District</label>
                            <select class="browser-default district" id="district" name="district[]" multiple  tabindex="-1">

                            </select>
                        </div>
                    </div>
                    <div class="col s12 m6 xl3"> <div class="col s1 m1" style="text-align: center; margin-top: 30px;"> <i class="material-icons prefix">adjust</i></div>
                        <div class="col s10 m10">
                            <label>Select Block</label>
                            <select class="browser-default block" id="block" name="block[]" multiple  tabindex="-1">

                            </select>
                        </div>
                    </div>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light" style="padding:0 5px;"  href="block-master?add">
                        <i class="material-icons right" style="margin-left:3px">add_circle_outline</i>Add New
                    </a>
                    <button class="btn myblue waves-effect waves-light submit" style="padding:0 5px;" id="filterButton"><i class="material-icons right" style="margin-left:3px">search</i>Search
                    </button>
                </div>
                <div class="row" style="position:relative;">
                    <div class="col s12 table-responsive">
                        <table id="table" class="display">
                            <thead>
                            <tr role="row">
                                <th>Sr.No.</th>
                                <th>State</th>
                                <th>District</th>
                                <th>Block</th>
                                <th>Population</th>
                                <th>Area</th>
                                <th>Density</th>
                                <th>Lat</th>
                                <th>Lng</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                        </table>
                                
                    </div>
                </div>
                <?php } ?>
                <?php if(isset($_GET['add'])){ ?>
                    <h5>Add Block</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="block-master" class="col s12 m12">
                        <?=csrf_field();?>
                        <div class="row">
                            <div class="input-field col s12 m2">
                                <i class="material-icons prefix">stars</i>
                                <input id="Full_Name" type="text" class="validate" name="country" disabled value="INDIA">
                                <label for="Full_Name">Country</label>
                            </div>
                            <div class="input-field col s12 m3">
                                <select id="State" class="state_list select2" name="state" style=" width: 100%" required>
                                    <option value="">Select State</option>
                                    <?php
                                    foreach ($states as $state){
                                        ?>
                                        <option value="<?=$state->id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="input-field col s12 m3">
                                <select id="District" class="district_list select2" name="district" style=" width: 100%" data-placeholder="Select State First" required>
                                    <option value="">Select State First</option>
                                </select>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">list</i>
                                <input id="Block" type="text" class="validate" name="block" value="">
                                <label for="Block">Block</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">av_timer</i>
                                <input id="population" type="text" class="validate" name="population"  value="">
                                <label for="population">Population</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">aspect_ratio</i>
                                <input id="area" type="text" class="validate" name="area"  value="">
                                <label for="area">Area</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">blur_on</i>
                                <input id="density" type="text" class="validate" name="density" value="">

                                <label for="density">Density</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">adjust</i>
                                <input id="lat" type="text" class="validate" name="lat" value="">

                                <label for="District">Lat</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">adjust</i>
                                <input id="lng" type="text" class="validate" name="lng" value="">

                                <label for="District">Lng</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                                <button class="btn myblue waves-light edit_submit"  style="padding:0 5px;" type="submit" name="add">SAVE
                                    <i class="material-icons right">save</i>
                                </button>
                            </div>
                        </div>
                    </form>
                <?php } ?>
                <?php if(isset($_GET['edit'])){
                    $did = $_GET['edit'];
                    $get_block = DB::select("SELECT * FROM `blocks` WHERE id = '$did'")[0];
                    $get_dist = DB::select("SELECT * FROM `district` WHERE id = '$get_block->district_id'")[0];
                   // dd($get_dist);
                    ?>

                    <h5>Edit Block</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="javascript:history.go(-1)" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form method="post" action="block-master" class="col s12 m12">
                        <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                        <input type="hidden" name="didd" value="<?php echo $did; ?>">
                        <div class="row">
                            <div class="input-field col s12 m2">
                                <i class="material-icons prefix">stars</i>
                                <input id="Full_Name" type="text" class="validate" name="name" disabled value="INDIA">
                                <label for="Full_Name">Country</label>
                            </div>

                            <div class="input-field col s12 m3">
                                <select id="State" class="state_list select2" name="state" disabled style="width: 100%" required>
                                    <?php
                                    foreach ($states as $state){
                                        $s_id = $state->id;
                                        $old_sid = $get_dist->state_id;
                                        ?>
                                        <option <?=($s_id==$old_sid)?'selected':'';?>  value="<?=$s_id;?>"><?=$state->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="input-field col s12 m3">
                                <select id="District"  class="district_list select2" name="district" disabled style="width: 100%" required>
                                    <?php
                                    foreach ($districts as $district){
                                        $s_id = $district->id;
                                        $old_sid = $get_block->district_id;
                                        ?>
                                        <option <?=($s_id==$old_sid)?'selected':'';?>  value="<?=$s_id;?>"><?=$district->name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">adjust</i>
                                <input id="Block" type="text" class="validate" name="block" value="<?=$get_block->block;?>">
                                <label for="Block">Block</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">av_timer</i>
                                <input id="population" type="text" class="validate" name="population"  value="<?=$get_block->population;?>">
                                <label for="population">Population</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">aspect_ratio</i>
                                <input id="area" type="text" class="validate" name="area"  value="<?=$get_block->area;?>">
                                <label for="area">Area</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">blur_on</i>
                                <input id="density" type="text" class="validate" name="density" value="<?=$get_block->density;?>">

                                <label for="density">Density</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">adjust</i>
                                <input id="lat" type="text" class="validate" name="lat" value="<?=$get_block->lat;?>">

                                <label for="District">Lat</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <i class="material-icons prefix">adjust</i>
                                <input id="lng" type="text" class="validate" name="lng" value="<?=$get_block->lng;?>">

                                <label for="District">Lng</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                                <button class="btn myblue waves-light edit_submit"  style="padding:0 5px;" type="submit" name="edit">SAVE
                                    <i class="material-icons right">save</i>
                                </button>
                            </div>
                        </div>
                    </form>
                <?php } ?>
            </div>
        </div>
    </div>

</div>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
</body>

</html>
<script>
    var table = null;
    $(function() {
        table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "<?=url("crm/get-blocks") ?>",
                type: 'GET',
                data: function (d) {
                    d.state = $('#states').val();
                    d.district = $('#district').val();
                    d.block = $('#block').val();
                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'state_name', name: 'states.name' },
                { data: 'district_name', name: 'district.name' },
                { data: 'block', name: 'block' },
                { data: 'population', name: 'population' },
                { data: 'area', name: 'area' },
                { data: 'density', name: 'density' },
                { data: 'lat', name: 'lat' },
                { data: 'lng', name: 'lng' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            order: [[0, 'asc']],
            dom: 'lBfrtip',
            buttons: <?=$buttons;?>,
            fixedColumns: true,
            colReorder: true,
            exportOptions:{
                columns: ':visible'
            }
        });

        $('#filterButton').click(function(){
            $('#table').DataTable().draw(true);

        });
    });
</script>
<script>
    $(document).ready(function(){
        $('.select2').select2();
        $('#table').attr('style', 'width: 100%');
        $('.states').select2();
        $('.district').select2();
        $('.block').select2();
        $('.state_list').change(function () {
            var sid = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district",
                data:'sid='+sid+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    var option = '<option value="">Select District</option>';
                    $('.district_list').html(option+data);
                }
            });
        });

        $(".states").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-district-ajax",
                data:'cid='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".district").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });
        $(".district").change(function(){
            var val = $(this).val();
            $.ajax({
                type: "POST",
                url: "get-block",
                data:'did='+val+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $(".submit").html("Wait...");
                },
                success: function(data){
                    $(".block").html(data);
                    $(".submit").html("Search <i class='material-icons '>search</i>");
                }
            });
        });

    })

    $('.valid_no').blur(function () {
        var a = $(this).val();
        var b = $(this).val().length;
        var c = a.indexOf(",");
       if(c==-1){
           if(b==10){
               $('.add_submit').prop("disabled", false);
               $('.edit_submit').prop("disabled", false);
           }
           else{
               alert("Please Enter 10 digit no");
               $('.add_submit').prop("disabled", true);
               $('.edit_submit').prop("disabled", true);
           }
       }else{
           var str = a;
           var str_array = str.split(',');
           for(var i = 0; i < str_array.length; i++) {
               // Trim the excess whitespace.
               str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
               // Add additional code here, such as:
               var tot = str_array[i].length;
               if(tot!=10){
                   alert("Please Enter 10 digit number before and after comma");
                   $('.add_submit').prop("disabled", true);
                   $('.edit_submit').prop("disabled", true);
               }
               else{
                   $('.add_submit').prop("disabled", false);
                   $('.edit_submit').prop("disabled", false);
               }
           }
       }
    });

    $("input[name=contact_no]").keypress(function (e) {

        if (/\d+|,/i.test(e.key) ){

            console.log("character accepted: " + e.key)
        } else {
            console.log("illegal character detected: "+ e.key)
            return false;
        }

    });


    $(document).on('click','.bdelete',function () {
        if (confirm("Are you sure?")) {
            var cid = $(this).val();
            $.ajax({
                type: "POST",
                url: "block_delete",
                data:'id='+cid+'&_token=<?=csrf_token(); ?>&_method=delete',
                success: function(data){
                    alert('Record Deleted Successfully');
                    table.ajax.reload();
                }
            });
        }
        else{

        }
        return false;
    });

</script>
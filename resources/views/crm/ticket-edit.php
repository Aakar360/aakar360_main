<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="ThemeSelect">
    <title>
        <?=$title; ?>
    </title>
    <link rel="apple-touch-icon" href="<?=$tp; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/css/custom/custom.css">
    <link href="<?=$tp; ?>/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
    table th, td{
        border:1px solid #d8d3d8;
        padding-left: 15px;
    }
    .tableFixHead {
        overflow: auto;
        height: 560px;
        width:100%
    }

    td:first-child, th:first-child {
        position:sticky;
        left:0;
        z-index:1;
        background-color:white;
    }
    td:nth-child(2),th:nth-child(2)  {
        position:sticky;
        left:40px;
        z-index:1;
        background-color:white;
    }
    .tableFixHead th {
        position: sticky;
        top: 0;
        background: #ffffff;
        z-index:2
    }
    th:first-child , th:nth-child(2) {
        z-index:3

    }
</style>
<!-- END: Head-->
<?=$header;?>

<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs">
            <div class="card-content" style="min-height: 500px;">
                <?=$notices;?>
<h6>Edit Ticket :: <?php echo  $tickets->id; ?></h6>
                            <div class="row">
                                <center>
                                    <?php if(isset($_GET['return'])){ ?>
                                        <a class="btn myblue waves-light " style="padding:0 5px;" href="<?=$_GET['return']; ?>" >
                                    <?php }else{ ?>
                                    <a class="btn myblue waves-light " style="padding:0 5px;" href="customer-profile?cid=<?=isset($_GET['cid']) ? $_GET['cid'] : 'customer-data'?>" >
                                    <?php } ?>
                                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                                    </a>
                                </center>
                                <div class="ticket_form">
                                        <form method="post" action="customer-profile" enctype="multipart/form-data" class="col s12 m12">
                                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                                            <input type="hidden" name="ticket_id" value="<?php echo  $tickets->id; ?>">
                                            <input type="hidden" name="crm_customer_id" value="<?php echo  $tickets->crm_customer_id; ?>">

                                            <!--<div class="row">
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">email</i>
                                                    <input id="email" type="tel" class="validate" name="email" value="<?/*=$tickets->email*/?>">
                                                    <label for="email">Contact</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">email</i>
                                                    <input id="cc" type="tel" class="validate" name="cc" value="<?/*=$tickets->cc*/?>">
                                                    <label for="cc"> CC</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">view_headline</i>
                                                    <input id="subject" type="tel" class="validate" name="subject" value="<?/*=$tickets->subject*/?>">
                                                    <label for="subject">Subject</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input id="phone" type="tel" class="validate" name="phone" value="<?/*=$tickets->phone*/?>">
                                                    <label for="phone"> Phone Number</label>
                                                </div>

                                            </div>-->
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">forum</i>
                                                    <select  name="type" class="type"> type
                                                        <option <?=($tickets->type=='Question')?'selected':''; ?> value="Question">Question</option>
                                                        <option <?=($tickets->type=='Quotation')?'selected':''; ?> value="Quotation">Quotation</option>
                                                        <option<?=($tickets->type=='Problem')?'selected':''; ?> value="Problem">Problem</option>
                                                    </select>
                                                    <label for="type">Type</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">low_priority</i>
                                                    <select name="priority" class="Priority"> Priority
                                                        <option <?=($tickets->priority=='Low')?'selected':''; ?> value="Low">Low</option>
                                                        <option <?=($tickets->priority=='Medium')?'selected':''; ?> value="Medium">Medium</option>
                                                        <option <?=($tickets->priority=='High')?'selected':''; ?> value="High">High</option>
                                                        <option <?=($tickets->priority=='Urgent')?'selected':''; ?> value="Urgent">Urgent</option>
                                                    </select>
                                                    <label for="priority">Priority</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">thumbs_up_down</i>
                                                    <select name="status" class="Status"> Status
                                                        <option <?=($tickets->status=='Pending')?'selected':''; ?> value="Pending">Pending</option>
                                                        <option <?=($tickets->status=='Resolved')?'selected':''; ?> value="Resolved">Resolved</option>
                                                        <option <?=($tickets->status=='Closed')?'selected':''; ?> value="Closed">Closed</option>
                                                    </select>
                                                    <label for="status">Status</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">transfer_within_a_station</i>
                                                    <select class="validate" name="assign_to" id ="Assign_to"  tabindex="-1">
                                                        <?php foreach ($users as $user){
                                                            ?>
                                                            <option <?=($tickets->assign_to == $user->u_id)?'selected':''; ?> value="<?=$user->u_id?>"> <?=$user->u_name?> </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <!--input id="Assign_to" type="tel" class="validate" name="assign_to" value="<--?=$tickets->assign_to?>"-->
                                                    <label for="Assign_to">Assign To</label>
                                                </div>
                                                <div class="col s12 m6 xl3">
                                                    <div class="col s1 m1"style="text-align: center;margin-top: 30px;margin-left: -32px;"> <i class="material-icons prefix">person</i></div>
                                                    <div class="col s11 m11" style="margin-top: -7px">
                                                        <label>Select Account Manager</label>
                                                        <select class="browser-default acc_manager" id="acc_manager" name="acc_manager" tabindex="-1">
                                                            <option value="<?=$tickets->acc_manager?>"><?=$tickets->acc_manager?></option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">view_headline</i>
                                                    <input id="Description" type="tel" class="validate" name="description" value="<?=$tickets->description?>">
                                                    <label for="Description">Description</label>
                                                </div>
                                                
 
                                                <div class="input-field col s12 m6">
                                                    <i class="material-icons prefix">watch_later</i>
                                                    <input type="text" class="datepicker" id="ticket_due_date" name="ticket_due_date" value="<?=$tickets->due_date?>">
                                                    <input type="text" style="display: none" id="due_date" value="<?=$tickets->due_date?>">
                                                    <label for="ticket_due_date">Due Date</label>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <!--<div class="input-field col s6">
                                                    <i class="material-icons prefix">view_headline</i>
                                                    <input id="Tag" type="tel" class="validate" name="tag" value="<?/*=$tickets->tag*/?>">
                                                    <label for="Tag">Tag</label>
                                                </div>-->
                                                <div class="input-field col s12 m6">
                                                    <div class="file-field">
                                                        <div class="btn myblue waves-light left">
                                                            <span>Choose file</span>
                                                            <input type="file" name="image" style="width: 100%" class="btn myblue" id="notes_image">
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                            <input class="file-path validate" type="text" placeholder="Upload your file">
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                   <center> <button class="btn myblue waves-light right " type="submit" name="edit_ticket">Save
                                                        <i class="material-icons right">save</i>
                                                    </button></center>
                                                </div>
                                            </div>

                                        </form>
                                    </fieldset>
                                </div>
                            </div>

            </div>
        </div>
    </div>
</div>


<!-- BEGIN VENDOR JS-->
<script src="<?=$tp; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/js/select2.min.js"></script>
<script src="<?=$tp; ?>/js/scripts/ui-alerts.js" type="text/javascript"></script>

</body>

</html>
<script>
    $(document).ready(function(){
        $('.datepicker').datepicker({ format: 'yyyy-mm-dd' });
    });

    $(".datepicker").change(function() {
        var date1 = $(due_date).val();
        var date2 = $(ticket_due_date).val();
        var date3 = Date.parse(date2) - Date.parse(date1);
        alert("due date extended for "+date3/1000/60/60/24+" days");
    });

    $(document).ready(function(){
        $('.type1').select2();
        $('.variants').select2();


        $(".category").change(function(){
            var val = $(this).val();
            var cname = $(".category option:selected").html();

            $.ajax({
                type: "POST",
                url: "get-report-option",
                data:'cid='+val+'&cname='+cname+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data){
                    $(".report_div").html(data);
                    $("#model_report_type").val(data);
                }
            });
        });
        var get_view_type = $('#get_report_view').val();
        if(get_view_type=='brand_wise'){
            $('.brand_wise').show();
        }
        if(get_view_type=='price_wise'){
            $('.price_wise').show();
        }




    });
</script>




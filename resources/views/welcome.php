<!DOCTYPE html>
<html>
    <head>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <style>

            .main-container{
                background: #eeeeee;
                width: 100%;
                height: 100vh;
            }
            .split1{
                height: inherit;
                background-image: url('assets/images/split1-image.jpg');
                background-repeat: no-repeat;
                background-size: cover;
                position: relative;
                text-align: center;
                width: 50%;
                float: left;
            }
            .split2{
                background: #ffffff;
                height:inherit;
                float: right;
                background-image: url('assets/images/split2-image.jpg');
                background-repeat: no-repeat;
                background-size: cover;
                position: relative;
                text-align: center;
                width: 50%;
            }
            .split-overlay{
                position: absolute;
                height: inherit;
                width: 100%;
                left: 0;
            }
            .split1 .split-overlay{
                background: rgba(0, 52, 129, 0.75);
            }
            .split2 .split-overlay{
                background: rgba(255,255,255,0.75);
            }
            .brand-logo{
                position: absolute;
                right: 15px;
                padding: 15px 0;
            }
            .brand-logo img{
                max-width: 230px;
                width: 100%;
            }
            .split1 h1{
                color: #FFFFFF;
                text-align: center;
                padding-top: 25%;
                margin-bottom: 15px;
                font-size: 70px;
            }
            .split1 h2{
                color: #ffffff;
                font-size: 50px;
                margin-bottom: 30px;
                margin-top: 0;
            }
            .split2 h1{
                color: rgb(0, 52, 129);
                text-align: center;
                padding-top: 25%;
                margin-bottom: 15px;
                font-size: 70px;
            }
            .split2 h2{
                color: rgb(0, 52, 129);
                font-size: 50px;
                margin-bottom: 30px;
                margin-top: 0;
            }
            .ins-btn{
                border-radius: 0;
            }
            @media screen and (max-device-width:767px), screen and (max-width:767px) {
                .split1 {
                    width: 100%;
                    height: 50vh;
                }
                .split2 {
                    width: 100%;
                    height: 50vh;
                }
                .brand-logo{
                    position: absolute;
                    right: 28%;
                    padding: 45px 0;
                }
                .brand-logo img{
                    max-width: 400px;
                }
                .ins-btn{
                    font-size: 34px;
                }
                .split2 h1{
                    padding-top: 30%;
                }
            }
        </style>
    </head>
    <body>
        <div class="main-container">


            <div class="col-md-6 split2">
                <div class="split-overlay"></div>
                <div class="col-md-12">
                    <h1>Individual</h1>
                    <h2><span>Buyer</span></h2>
                    <a href="<?=url('r/index');?>" class="btn btn-primary ins-btn">Shop Now</a>
                </div>
            </div>
            <div class="col-md-6 split1">
                <div class="split-overlay"></div>
                <div class="col-md-12">
                    <h1>Institutional</h1>
                    <h2><span>Buyer</span></h2>
                    <a href="<?=url('i/index');?>" class="btn btn-primary ins-btn">Shop Now</a>
                </div>
            </div>
            <a class="brand-logo" rel="home" href="<?=url('')?>"><img class="" src="<?=$data['cfg']->logo ?>"></a>
        </div>
    </body>
    <footer>
        <script
                src="https://code.jquery.com/jquery-2.2.4.min.js"
                integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </footer>
</html>
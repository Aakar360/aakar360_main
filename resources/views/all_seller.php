<?php echo $header?>

    <!-- Inner page heading end -->

    <!-- Property start from here -->
    <section class="at-property-sec at-property-right-sidebar" style="padding: 0;">
        <div class="container">
            <div class="content product-page">
                <div class="col-md-8">




                </div>
                <div class="col-md-4">

                </div>
                <div class="clearfix"></div>


                    <div class="table-responsive" style="margin-top: 20px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr class="bg-blue">
                                <th>Product Price</th>
                                <th>Product Quantity</th>
                                <th>Seller</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php  foreach ($sellers as $seller){
                                ?>
                                    <tr>
                                        <td><?=$seller->sale?></td>
                                        <td><?=$seller->min_qty?></td>
                                        <td><?=$seller->name?></td>
                                        <td><button class="btn btn-xs btn-primary seller_id" value="<?=$seller->id;?>">View Info</button></td>
                                    </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

            </div>
        </div>
    </section>
<script type="text/javascript">
    $(document).ready(function(){
       
        $('.seller_id').click(function (){
        var sid_url = $(this).val();
        var pre = 'product/';
        if (sid_url) {
        window.open( pre+sid_url, 'newwindow');
        //window.location = pre+sid_url;
        }
        return false;
        });
    });
</script>

<?php echo $footer?>
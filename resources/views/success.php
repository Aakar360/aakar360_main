<?php echo $header?>
<style>
	.bg,.cover{
	background:#69DD95;
	}
</style>
<div class="container landing-cover">
    <div class="jumbotron text-xs-center">
        <h1 class="display-3">Thank You!</h1>
        <p class="lead"><strong>Your order has been placed successfully </strong>Please check your email for further instructions.</p>
        <hr>
        <p>
            Having trouble? <a href="#">Contact us</a>
        </p>
        <p class="lead">
            <a class="btn btn-primary btn-sm" href="<?=url('')?>" role="button">Continue to homepage</a>
        </p>
    </div>
	<!--div class="not-found bg">
		<h1><?=translate("Success");?></h1>
		<h5><?=translate("Your order has been placed successfully");?></h5>
	</div-->
</div>
<?php echo $footer?>
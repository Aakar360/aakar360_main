<?php echo $header?>


    <section class="pdtopbtm-50">
        <div class="row">
            <div class="col-md-10 col-lg-offset-1 col-lg-10">
                <div class="grid">
                    <?php
                    foreach($pro as $serwish){
                        if($serwish->service_id == ''){
                            $ser = DB::select('SELECT * FROM services_category Where id = '.$serwish->sub_service_id)[0];

                            echo '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <a href="sub_service/'.path($ser->name,$ser->id).'" data-title="'.translate($ser->name).'">
                                                <div class="at-property-item at-col-default-mar" id="'.$ser->id.'">
                                                    <div class="at-property-img">
                                                        <img src="'.url('/assets/services/'.image_order($ser->image)).'" style="width: 360px; height: 200px;" alt="">
                                                    </div>
                                                    <div class="at-property-location" style="min-height: 150px;">
                                                        <h4><a href="sub_service/'.path($ser->name,$ser->id).'" data-title="'.translate($ser->name).'">'.translate($ser->name).'</a></h4>
                                                        <p>'.$ser->description.'</p>
                                                    </div>
                                                </div></a>
                                            </div>';
                        } else {

                            $sera = DB::select('SELECT * FROM services Where id = '.$serwish->service_id)[0];
                            echo '	<div class="col-md-3 col-lg-3 col-sm-2 col-xs-1">
                                        <a onClick="getQuestions(' . $sera->id . ')" href="#" data-title="' . translate($sera->title) . '">
                                            <div class="at-property-item at-col-default-mar" id="' . $sera->id . '">
                                                <div class="at-property-img">
                                                    <img src="' . url('/assets/services/' . image_order($sera->images)) . '" style="width: 360px; height: 200px;" alt="">
                                                </div>
                                                
                                                <div class="at-property-location" style="min-height: 150px;">
                                                    <h5><a href="#" data-title="' . translate($sera->title) . '">' . translate($sera->title) . '</a></h5>
                                                    <b>Service Starts at ' . c($sera->price) . '</b>
                                                    <p>' . substr($sera->description, 0, 50) . '...</p>';

                            echo '</div>
        
                                            </div>
                                        </a>
                                    </div>';
                        }
                    }


                    ?>
                </div>
            </div>
        </div>


    </section>


<?php echo $footer?>
<script>
    jQuery(window).on('load', function(){
        var $ = jQuery;
        var $container = $('.grid');
        $container.masonry({
            columnWidth:10,
            gutterWidth: 15,
            itemSelector: '.grid-item'
        });
    });
</script>

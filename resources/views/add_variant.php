<?php
echo $header;
?>
<style type="text/css">
    /*.frmSearch {border: 1px solid #a8d4b1;background-color: #c6f7d0;margin: 2px 0px;padding:40px;border-radius:4px;}*/
    #variant-list{float:left;list-style:none;margin-top:-3px;padding:0;width:393px;position: absolute;}
    #variant-list li{padding: 10px; background: #003481; border-bottom: #ffffff 1px solid; color: white}
    #variant-list li:hover{background: #4f4bec;cursor: pointer;}
    #search-box{padding: 10px;border: #003481 1px solid;border-radius:4px;}
</style>
<?php if(isset($_GET['id'])){ ?>
    <div class="row">
    <div class="col-lg-offset-2 col-lg-8" >
        <form autocomplete="off" action="product-variant?id=<?=$_GET['id'];?>" method="post" class="form-horizontal" id="modal-form" enctype="multipart/form-data" style="max-width: 100%;padding: 0;">

            <h2>Add Variant </h2>
            <div class="panel panel-default">
                <div class="panel-heading"> Add Variant  of Product <span style="color: green; font-weight: 800;text-transform: uppercase;"> <?=$products->title?></span></div>
                <div class="panel-body">
                    <form autocomplete="off" action="add_variants?add='.$_GET['add_variants'].'" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
                        <?= csrf_field(); ?>
                        <fieldset>
                            <div class="form-group col-md-6" style="padding-left: 30px;padding-right: 30px;">
                                <label class="control-label">Variant Title</label>
                                <input name="variant_pid" type="hidden" class="form-control" required="required" value="<?=$_GET['id'];?>"/>
                                <!--<input name="variant_title" type="text" class="form-control" required="required" id="search-box" placeholder="Variant Name"/>
                                <div id="suggesstion-box"></div>-->
                                <div class="frmSearch">
                                    <input name="variant_title" type="text" class="form-control" required="required" id="search-box" placeholder="Variant Name" autocomplete="nope"/>

                                </div>
                                <div id="suggesstion-box"></div>
                            </div>
                            <div class="form-group col-md-2" style="padding-left: 30px;padding-right: 30px;">
                                <label class="control-label">Price</label>
                                <input name="price" type="text" class="form-control" required="required"/>
                            </div>
                            <div class="form-group col-md-2">
                                <label class="control-label">Display</label><br>
                                <input name="display" type="radio" id="yes" value="1" /><label for="yes">Yes</label> &nbsp; <input name="display" type="radio" id="no" value="0" /><label for="no">No</label>
                            </div>
                            <input name="add_variant" type="submit" value="Add Variant" class="btn btn-primary col-md-2" style="padding-left: 30px;padding-right: 30px;margin-top: 20px;" onClick="PopUp()" />
                        </fieldset>
                    </form>
                    <div class="table-responsive">
                        <b>Added Variants</b>
                        <table class="table table-striped table-bordered" id="datatable-editable1">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Variant Title</th>
                                <th>Price</th>
                                <th>Display</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;

                            foreach ($variants as $var){
                                $dis = 'No';
                                if($var->display){
                                    $dis = 'Yes';
                                }
                                echo'<tr>
								<td>'.$sr.'</td>
								<td>'.$var->variant_title.'</td>
								<td>'.c($var->price).'</td>
								<td>'.$dis.'</td>
								<td>';?>
									<a href="product-variant?id=<?=$_GET['id'];?>&delete=<?=$var->id;?>"
									onClick="return confirm('Do you really want to delete ?');"><i class="icon-trash"></i></a>
                                <a href="javascript:void(0);" onClick="editVariant(<?=$var->id;?>)" id ="product_discount"><i class="icon-pencil"></i></a>
								</td>
							  </tr>
							 <?php
                                $sr++;}
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="row">

    </div>
<?php } ?>
<script type="text/javascript">
    $(function() {
//----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
//----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });

    function editDiscount1(){
        $('#edit_discount').click();
    }

    function editVariant(vid){
        $('#edit_discount').click();
        $.ajax({
            url: 'get-variant',
            type: 'post',
            data: 'variant_id='+vid+'&_token=<?=csrf_token(); ?>',
            success: function(datax){
                var data = JSON.parse(datax);
                $('#modal-form input[name=var_id]').val(data.id);
                $('#modal-form input[name=variant_title]').val(data.variant_title);
                $('#modal-form input[name=price]').val(data.price);
                if(data.display == 0){
                    $('#modal-form .display').html('<div class="form-group col-md-12">'
                        +'<label class="control-label">Display</label><br>'
                        +'<input name="display" type="radio" id="yes" value="1" /><label for="yes">Yes</label> &nbsp; <input name="display" type="radio" id="no" value="0" checked="checked"/><label for="no">No</label>'
                        +'</div>');
                }else{
                    $('#modal-form .display').html('<div class="form-group col-md-12">'
                        +'<label class="control-label">Display</label><br>'
                        +'<input name="display" type="radio" id="yes" value="1" checked="checked"/><label for="yes">Yes</label> &nbsp; <input name="display" type="radio" id="no" value="0" checked="checked"/><label for="no">No</label>'
                        +'</div>');
                }
                $('#edit-button').trigger('click');
            }
        });
    }

    $(document).ready(function(){
        $("#search-box").keyup(function(){
            var val = $(this).val();
            var sval = val.trim();
            $.ajax({
                type: "POST",
                url: "get-variant-ajax",
                data:'keyword='+sval+'&_token=<?=csrf_token(); ?>',
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(assets/LoaderIcon.gif) no-repeat 165px");
                },
                success: function(data){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#search-box").css("background","#FFF");
                }
            });
        });
    });

    function selectCountry(val) {
        $("#search-box").val(val);
        $("#suggesstion-box").hide();
    }

    $(document).ready(function(){
        setTimeout(function(){
            PopUp();
        },5000); // 5000 to load it after 5 seconds from page load
    });

</script>
<?php
echo $footer;
?>
<?php if(isset($_GET['id'])){ ?>

    <!-- Edit Discount  Popup Start -->
    <a class="btn btn-primary hidden" id="edit_discount" data-popup-open="popup-sku" href="#">Get Started !</a>
    <div class="popup" id="popup-sku" data-popup="popup-sku">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="row">
                <form action="product-variant?id=<?=$_GET['id'];?>" method="post" class="form-horizontal" id="modal-form" enctype="multipart/form-data" style="max-width: 100%;padding: 0;">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="var_id" value=""/>
                    <fieldset>
                        <div class="form-group col-md-12" style="padding-left: 30px;padding-right: 30px;">
                            <label class="control-label">Variant Title</label>
                            <input name="variant_title" type="text" class="form-control" required="required"/>
                        </div>
                        <div class="form-group col-md-12" style="padding-left: 30px;padding-right: 30px;">
                            <label class="control-label">Price</label>
                            <input name="price" type="text" class="form-control" required="required"/>
                        </div>
                        <div class="display"></div>
                        <center><input name="edit_variant" type="submit" value="Edit Variant" class="btn btn-primary"/><center>
                    </fieldset>
                </form>

            </div>

            <a class="popup-close" data-popup-close="popup-sku" href="#">x</a>
        </div>
    </div>
    <!-- Edit Discount Popup End -->
<?php } ?>


<?php echo $header?>

<section id="at-inner-title-sec" class="pdbtm-165" style="background-image: url('<?=url('/assets/services/'.$images[0])?>')">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8" style="padding-top: 100px;">
                <div class="inner-border">
                    <div class="at-inner-title-box text-center" style="padding: 40px 65px;margin:5px;border: none;background: rgba(255, 255, 255, 0.7);">
                        <h1 style="text-transform: none;"><?=translate($service->title)?></h1>
                        <!--p style="text-transform: none;font-size: 20px;">Let our accomplished team of experts submit concepts for your space <b>— it's easy!</b></p-->
                    </div>
                </div>
            </div>
            <div class="request-box" style="background: #ffffff;">
                <p><?=nl2br(translate($service->description))?></p>
                <a class="btn btn-primary btn-sm" data-popup-open="popup-1" href="#">Get Started !</a>
                <P>
                    <?php if(customer('id') !== ''){
                        $user_id = customer('id');

                        $img = DB::select("SELECT * FROM service_wishlist WHERE user_id = '".$user_id."' AND service_id = '".$service->id."'");

                        if(!empty($img)){
                            echo '<button class="btn btn-primary btn-sm" id="likebutton1" style="width: 90%; position: absolute; bottom: 40px; left: 0px; margin: 15px; right: 0;">
                        <span class="tickgreen">✔</span> Added to Project</button>';
                        } else{
                            echo '<button class="btn btn-primary likebutton1 btn-sm" id="likebutton1" style="width: 90%; position: absolute; bottom: 40px; left: 0px; margin: 15px; right: 0;"><i class="fa fa-heart"></i> Add to Project</button>';

                        }} else {
                        echo '<button class="btn btn-primary btn-sm" onClick="getModel()" style="width: 90%; position: absolute; bottom: 40px; left: 0px; margin: 15px; right: 0;"><i class="fa fa-heart"></i> Add to Project</button>';
                    }?>
                </P>
            </div>
        </div>
    </div>

</section>


<section class="how-it-works-sec padding" style="background: transparent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="box-heading" style="padding-left: 10px;">
                    <div class="at-sec-title">
                        <h2 style="font-size: 15px;">How it <span>Works</span> </h2>
                        <div class="at-heading-under-line" style="margin: auto;">
                            <div class="at-heading-inside-line" ></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tabs tabs-style-underline">
            <nav>
                <ul>
                    <li><a href="#section-underline-1" class="icon icon-idcard"><span>Pick your designer</span></a></li>
                    <li><a href="#section-underline-2" class="icon icon-personalize"><span>Personalize your design</span></a></li>
                    <li><a href="#section-underline-3" class="icon icon-ideas"><span>Collaborate on ideas</span></a></li>
                    <li><a href="#section-underline-4" class="icon icon-room"><span>Visualize your room</span></a></li>
                    <li><a href="#section-underline-5" class="icon icon-shop"><span>Shop with ease</span></a></li>
                </ul>
            </nav>
            <div class="content-wrap">
                <section id="section-underline-1">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Pick your designer</h3>
                            <p>Specedoor interior designers are vetted professionals and real people. Take our style survey to get matched with your perfect designer based on your style, or explore 100+ designers on your own.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/pick-your-designer.jpg');?>" alt="Pick your designer" style="margin-right:-27px;"/></div>
                    </div>
                </section>
                <section id="section-underline-2">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Personalize your design</h3>
                            <p>We work with your budget, style, and unique space. We'll even incorporate any of your existing pieces into the room design. After you book your designer, fill out your room profile to tell us exactly what you are looking for.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/personalize-your-design.jpg');?>" alt="Personalize your design" style="margin-right:-27px;"/></div>
                    </div>
                </section>
                <section id="section-underline-3">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Collaborate on ideas</h3>
                            <p>After 2 business days, your designer will come back to you with an initial set of ideas based on your vision. Give feedback along the way to refine a concept for your room. Your happiness is 100% guaranteed.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/collaborate-on-ideas.jpg');?>" alt="Collaborate on ideas" style="margin-right:-27px;"/></div>
                    </div>
                </section>
                <section id="section-underline-4">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Visualize your room</h3>
                            <p>It's hard to imagine the perfect room! Share a floor plan and room dimensions, and we'll create a visualization of your room and a floor plan recommendation so you can envision the final result.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/visualize-your-room.jpg');?>" alt="Visualize your room" style="margin-right:-27px;"/></div>
                    </div>
                </section>
                <section id="section-underline-5">
                    <div class="row">
                        <div class="col-md-4 details">
                            <h3>Shop with ease</h3>
                            <p>Shop your curated list of products, sourced from over 150 sellers, all in one place. Your ordering concierge will purchase all your pieces and keep a close eye on your orders to ensure everything gets to you, with no hassle.</p>
                        </div>
                        <div class="col-md-8"><img src="<?=url('/assets/images/shop-with-ease.jpg');?>" alt="Shop with ease" style="margin-right:-27px;"/></div>
                    </div>
                </section>
            </div><!-- /content -->
        </div><!-- /tabs -->
    </div>
</section>
<!-- Inner page heading end -->

<?php
if($service->products !== '') {?>
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-lg-12">
                <div class="box-heading" style="padding-left: 10px;">
                    <div class="at-sec-title">
                        <h2 style="font-size: 15px;">Related <span>Products</span> </h2>
                        <div class="at-heading-under-line" style="margin: auto;">
                            <div class="at-heading-inside-line" ></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div id="owl-product" class="owl-carousel">
            <?php
            $product_id = '';
            $pro = explode(',',$service->products);
            foreach ($pro as $pr) {
            $product = DB::table('products')->where('id', $pr)->first();?>
            <div class="" style="padding: 10px;">
                <div class="at-property-item at-col-default-mar" id="<?=$product->id ; ?>">
                    <div class="at-property-img">
                        <a href="product/<?=path($product->title, $product->id); ?>" data-title="<?=translate($product->title); ?>">
                            <img src="<?=url('/assets/products/' . image_order($product->images)); ?>" style="width: 100%; height: 150px;" alt="">
                        </a>
                    </div>
                    <div class="at-property-location">
                        <p>
                            <a href="product/<?=path($product->title, $product->id) ; ?>" data-title="<?=translate($product->title); ?>"><?=translate($product->title); ?>'</a>
                            <br><span class="price"><?=c($product->price); ?></span>
                        <div class="rating">
                            <?php
                            $rates = getRatings($product->id);
                            $tr = $rates['rating'];
                            $i = 0;
                            while ($i < 5) {
                                $i++;
                                echo '<i class="star' . (($i <= $rates["rating"]) ? "-selected" : "") . '"></i>';
                                $tr--;
                            }
                            echo '('.$rates["total_ratings"].')
                        </div>
                        </p>
                        <div class="cart-btn-custom" style="padding-top: 10px;">
                            <button class="bg">Add to Cart</button>
                            <div style="margin-top: -20px;">&nbsp;</div>
                                    ';
                            if (customer('id') !== '') {
                                $user_id = customer('id');
                                $product_id = $product->id;
                                $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '" . $user_id . "' AND product_id = '" . $product_id . "'");
                                if (!empty($img)) {
                                    echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                } else {
                                    echo '<input type="hidden" name="pid1" class="pid1" value="' . $product_id . '">
                                    <button class="bg likebutton">
                                        <i class="fa fa-heart"></i> Add to Project
                                    </button>';
                                }
                            } else {
                                echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                            }
                            echo '
                        </div>
                    </div>
                        </div>
                    </div>';
                            } ?>
                        </div>

                    </div></div>
                <?php }
                ?>

                <!--section class="at-property-sec at-property-right-sidebar" style="background-color: white;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="at-sec-title">
                    <h3 style="text-transform: none;">Reviews</h3>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding: 30px 0;">
            <div class="col-md-12 text-center">
                <?php if(customer('id') !== ''){ ?>
                    <button class="btn btn-primary" type="button" data-popup-open="popup-review" value="Ask a Question">Add Your Review</button>
                <?php } else { ?>
                    <button class="btn btn-primary" onClick="getModel()" type="button" value="Ask a Question">Add Your Review</button>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul id="rload">
                    <p>
                        <input type="hidden" name="service_id" value="<?=$service->id; ?>"/>
                    <?php
                $count = 0;
                $rewID = '';
                foreach($reviews as $review){
                    $rewID = $review->id;
                    $usid = $review->name;
                    $uimg = DB::select("SELECT * FROM customers WHERE id = '".$usid."'")[0];
                    echo '<img class="review-image" src="assets/user_image/'; if($uimg->image != ''){ echo $uimg->image; } else { echo 'user.png'; }; echo'">
                                        <div class="review-meta"><b>'.$uimg->name.'</b><br/>
                                        <span class="time">'.date('M d, Y',$review->time).'</span><br/></div>
                                        <div class="review">
                                            <div class="rating pull-right">';
                    $rr = $review->rating; $i = 0; while($i<5){ $i++;?>
                            <i class="star<?=($i<=$review->rating) ? '-selected' : '';?>"></i>
                            <?php $rr--; }
                    echo '</div>
                                        <div class="clearfix"></div>
                                        <p>'.nl2br($review->review).'</p></div>';
                    $count++;
                }
                if($count > 0){
                    echo '<hr/>';
                }
                ?>
                    </p>
                </ul>
                <div class="col-lg-12" style="padding-top: 10px; text-align: center;">
                    <div class="show_more_main" id="load_more_main<?php echo $rewID; ?>">
                        <span id="<?php echo $rewID; ?>" class="load_more" title="Load more posts" style="cursor: pointer;">Show more</span>
                        <span class="loding_t" style="display: none;"><span class="loding_text">Loading...</span></span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section-->



                <section class="at-property-sec at-property-right-sidebar" id="faq" >
                    <div class="container" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16); padding: 30px;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="box-heading" style="padding-left: 10px;">
                                    <div class="at-sec-title">
                                        <h2 style="font-size: 15px;">Frequently <span>Asked</span> Questions</h2>
                                        <div class="at-heading-under-line" style="margin: auto;">
                                            <div class="at-heading-inside-line" ></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding: 30px 0;">
                            <div class="col-md-12 text-center">
                                <?php if(customer('id') !== ''){ ?>
                                    <button class="btn btn-primary" type="button" data-popup-open="popup-question" value="Ask a Question">Ask a Question</button>
                                <?php } else { ?>
                                    <button class="btn btn-primary" onClick="getModel()" type="button" value="Ask a Question">Ask a Question</button>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul id="fload">
                                    <?php $postID = '';
                                    foreach($faqs as $faq){
                                        $postID = $faq->id; ?>
                                        <input type="hidden" name="category" value="<?=$service->id; ?>"/>
                                        <li>
                                            <input type="checkbox" checked>
                                            <i></i>
                                            <h5>Q : <?php echo $faq->question; ?><br><small>
                                                    <?php $u = $faq->user_id;
                                                    $user = DB::select("SELECT * FROM customers WHERE id = '$u'")[0];
                                                    ?>
                                                    by <?php echo $user->name; ?> on <?php $d = $faq->created; echo date('d M, Y', strtotime($d)); ?></small></h5>
                                            <p>A : <?php echo $faq->answer; ?></p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="col-lg-12" style="padding-top: 10px; text-align: center;">
                                <div class="show_more_main" id="show_more_main<?php echo $postID; ?>">
                                    <span id="<?php echo $postID; ?>" class="show_more" title="Load more posts" style="cursor: pointer;">Show more</span>
                                    <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- Property start from here -->

                <section style="padding-top: 30px; padding-bottom: 30px">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="at-sec-title">
									<h3 style="text-transform: none;">Why Choose <span>Us</span></h3>
									<div class="at-heading-under-line">
										<div class="at-heading-inside-line"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="why_sec">
							<div class="faq_dtls apk">
								<div class="inr_wrk_dtls">
									<div class="container">
										<div class="row">
											<div class="agent-carousel-2 slider" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
												<div class="col-md-3 col-sm-3 ptb-30">
													<div class="apk_block">
														<div class="apk_block_img">
															<img src="<?php echo url('assets/images/ico1.png'); ?>" style="width: 80px;height: auto;margin: auto;"/>
														</div>
														<span>Value <br>Pricing</span>
													</div>
												</div>
												<div class="col-md-3 col-sm-3 ptb-30">
													<div class="apk_block">
														<div class="apk_block_img">
															<img src="<?php echo url('assets/images/ico2.png'); ?>" style="width: 80px;height: auto;margin: auto;"/>
														</div>
														<span>Tailor Made <br> Designs</span>
													</div>
												</div>
												<div class="col-md-3 col-sm-3 ptb-30">
													<div class="apk_block">
														<div class="apk_block_img">
															<i class="icofont icofont-safety" style="color: #888a8b; font-size: 80px;"></i>
														</div>
														<span>1 Year <br>Warranty</span>
													</div>
												</div>
												<div class="col-md-3 col-sm-3 ptb-30">
													<div class="apk_block no-brd">
														<div class="apk_block_img">
															<img src="<?php echo url('assets/images/ico3.png'); ?>" style="width: 80px;height: auto;margin: auto;"/>
														</div>
														<span>Dedicated project<br>Managers</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

                <?php if(count($ladvices) > 0) {?>
				<section class="at-property-sec at-property-right-sidebar " style="background-color: #f4f4f4;">

					<div class="container" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16);padding: 30px;">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<div class="col-lg-8 col-md-8 mb-20">
									<div class="row">
										<div class="col-lg-12 col-md-8">
											<div class="box-heading" style="padding-left: 10px;">
												<div class="at-sec-title">
													<h2 style="font-size: 15px;">Latest <span>Advices</span> </h2>
													<div class="at-heading-under-line" style="margin: auto;">
														<div class="at-heading-inside-line" ></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div style="border-top: 2px solid #f4f4f4; padding-right: 10px; padding-left: 10px;">&nbsp;</div>
									<div class="vc_controls scroll-up" data-scroll="vc_cont1"><i class="fa fa-chevron-up"></i></div>
									<div class="verticleCarouselContainer" id="vcc1">
										<div class="adviceList" id="vc_cont1">
											<?php foreach ($ladvices as $ladvice){ ?>
											<article style="padding-top: 10px; padding-bottom: 10px;" class="row vcx_item">
												<div class="col-lg-4 col-md-4">
													<div class="home-featured-image">
														<a href="blog/<?php echo path($ladvice->title,$ladvice->id); ?>">
															<img src="<?php echo url('/assets/blog/'.$ladvice->images); ?>" class="alignleft" style="">
														</a>
													</div>

												</div>
												<div class="col-lg-8 col-md-8" itemprop="text">
													<span class="entry-categories">
														<?php echo getadvicesCategoryName($ladvice->category); ?>
													</span>
													<h5 class="entry-title" itemprop="headline">
														<a class="entry-title-link" rel="bookmark"><?=$ladvice->title; ?></a>
													</h5>
													<p class="entry-meta"><time class="entry-time" itemprop="datePublished" datetime="<?=$ladvice->date; ?>"><?=$ladvice->date; ?></time></p>
													<?=$ladvice->short_des; ?>
													<div class="col-lg-12 col-md-12" style="width:100%;margin-top: 15px;color:#003481">
														<?php
														$total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$ladvice->id)[0]->count;
														$total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$ladvice->id)[0]->count;
														$like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$ladvice->id)[0]->count;
														$rating = 0;
														if ($total_ratings > 0){
															$rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$ladvice->id)[0]->sum;
															$rating = $rating_summ / $total_ratings;
														}
														$reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$ladvice->id." AND active = 1 ORDER BY time DESC");
														$faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$ladvice->id." AND status = 'Approved' ORDER BY id DESC");
														$rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$ladvice->id."'")[0];
														$total_rating =  $rating->total_rating;
														$total_user = $rating->total_user;
														if($total_rating==0){
															$avg_rating = 0;
														}
														else{
															$avg_rating = round($total_rating/$total_user,1);
														}
														?>
														<div class="col-xs-12 text-center">
															<p style="width: 25%; float: left; bottom: 10px;"> <i class="fa fa-eye"></i> <?=$ladvice->visits; ?> Visits</p>
															<p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
															<p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
															<p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
														</div>
													</div>
												</div>
												
												<div class="col-lg-12 col-md-12 list-full">

													<div class="post-footer-container">
														<div class="post-footer-line post-footer-line-3">
															<div class="post-sharing-icons">
																<a target="_blank" href="https://www.facebook.com/sharer.php" title="Share on Facebook!" rel="noopener">
																	<i class="fa fa-post-footer fa-facebook"></i>
																</a>
																<a target="_blank" href="https://twitter.com/home/" title="Tweet this!" rel="noopener">
																	<i class="fa fa-post-footer fa-twitter"></i>
																</a>
																<a target="_blank" href="https://plus.google.com/share" title="Post this on Google Plus!" rel="noopener">
																	<i class="fa fa-post-footer fa-google-plus"></i>
																</a>
																<a href="mailto:" title="Send this article to a friend!">
																	<i class="fa fa-post-footer fa-envelope"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</article>
											<?php } ?>
										</div>
									</div>
									<div class="vc_controls scroll-down" data-scroll="vc_cont1"><i class="fa fa-chevron-down"></i></div>
								</div>
								<div class="col-lg-4 col-md-4 mb-20 border-left">
									<div class="row">
										<div class="col-lg-12 col-md-12">
											<div class="box-heading" style="padding-left: 10px;">
												<div class="at-sec-title">
													<h2 style="font-size: 15px;">Trending <span>Advices</span> </h2>
													<div class="at-heading-under-line" style="margin: auto;">
														<div class="at-heading-inside-line" ></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div style="border-top: 2px solid #f4f4f4; padding-right: 10px; padding-left: 10px;">&nbsp;</div>
									<div class="vc_controls scroll-up" data-scroll="vc_cont2"><i class="fa fa-chevron-up"></i></div>
										<div class="verticleCarouselContainer" id="vcc2">
											<div class="adviceList" id="vc_cont2">
												<?php foreach ($tadvices as $tadvice){ ?>
												<div class="home-featured-image vc_item vcx_item">
													<a href="blog/<?php echo path($tadvice->title,$tadvice->id); ?>">
														<img src="<?php echo url('/assets/blog/'.$tadvice->images); ?>" class="alignleft" sizes="(max-width: 600px) 100vw, 600px">
													</a>
													<h5 class="entry-title" itemprop="headline" style="padding: 5px; border: 4px solid #f4f4f4; ">
														<a class="entry-title-link" rel="bookmark"><?=$tadvice->title; ?></a>
													</h5>
												</div>
												<?php } ?>
											</div>
										</div>
									<div class="vc_controls scroll-down" data-scroll="vc_cont2"><i class="fa fa-chevron-down"></i></div>	

								</div>

							</div>
						</div>
					</div>
				</section>
				<?php } ?>



                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                            </div>
                            <div class="modal-body">
                                ...
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>



                <link rel="stylesheet" href="<?=$tp;?>/assets/flexslider.css" type="text/css">
                <script src="<?=$tp;?>/assets/jquery.flexslider.js"></script>
                <script src="<?=$tp;?>/assets/jquery.zoom.js"></script>
                <style>
                    .zoomImg {
                        background: white;
                    }
                </style>
                <script>

                    $(function() {
//----- OPEN
                        $('[data-popup-open]').on('click', function(e) {
                            var targeted_popup_class = jQuery(this).attr('data-popup-open');
                            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                            e.preventDefault();
                        });
//----- CLOSE
                        $('[data-popup-close]').on('click', function(e) {
                            var targeted_popup_class = jQuery(this).attr('data-popup-close');
                            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                            e.preventDefault();
                        });
                    });
                    
                    $('.likebutton').click( function () {
                        var ele =$(this);
                        var proid = $(this).prevAll('input').val();
                        $.ajax({
                            type: "POST",
                            url: "product-wishlist",
                            data:'_token=<?=csrf_token();?>&product_id='+proid,
                            success: function(data){
                                //alert('hello');
                                $('#open-project-modal1').click();
                                $('#projectName').html(data);
                                ele.html('<span class="tickgreen">✔</span> Added to Project');
                                ele.unbind();
                            }
                        });
                    });

                    function addProject(val) {
                        var ele =$(this);
                        var proid = $('#ghanta').data('id');
                        var project_id = val;
                        $.ajax({
                            type: "POST",
                            url: "add-product-project",
                            data:'_token=<?=csrf_token();?>&product_id='+proid+'&project_id='+project_id,
                            success: function(data){

                                $('#pclose').click();
                                ele.unbind();
                            }
                        });
                    }

					function addSerProject(val) {
						var ele =$(this);
						var project_id = val;
						$.ajax({
							type: "POST",
							url: "add-ser-project",
							data:'_token=<?=csrf_token();?>&service_id=<?=$service->id; ?>&project_id='+project_id,
							success: function(data){
								$('#pclose').click();
								ele.unbind();
							}
						});
					}


                    $(document).ready(function(){
                        $(document).on('click','.show_more',function(){
                            var brands = $('input[name=category]').val();
                            var ID = $(this).attr('id');
                            $('.show_more').hide();
                            $('.loding').show();
                            $.ajax({
                                type:'POST',
                                url:'get-ser-faq',
                                data:'brands='+brands+'&_token=<?=csrf_token();?>&id='+ID,
                                success:function(html){
                                    $('#show_more_main'+ID).remove();
                                    $('#fload').append(html);
                                }
                            });
                        });
                    });

                    $(document).ready(function(){
                        $(document).on('click','.load_more',function(){
                            var service = $('input[name=service_id]').val();
                            var id = $(this).attr('id');
                            $('.load_more').hide();
                            $('.loding_t').show();
                            $.ajax({
                                type:'POST',
                                url:'get-ser-rew',
                                data:'service_id='+service+'&_token=<?=csrf_token();?>&id='+id,
                                success:function(html){
                                    $('#load_more_main'+id).remove();
                                    $('#rload').append(html);
                                }
                            });
                        });
                    });


                    $('#star-rating').rating();
                    $('#carousel').flexslider({
                        animation: "slide",
                        controlNav: false,
                        animationLoop: false,
                        slideshow: false,
                        itemWidth: 210,
                        itemMargin: 5,
                        minItems: 4,
                        maxItems: 6,
                        asNavFor: '#slider'
                    });

                    $('#slider').flexslider({
                        animation: "slide",
                        controlNav: false,
                        animationLoop: false,
                        slideshow: false,
                        sync: "#carousel",
                        touch: true,
                        keyboard: true,
                        smoothHeight: true,
                    });
                    $(document).ready(function(){
                        $('.zoom').zoom({magnify: 3});

                    });

                    function getModel(){
                        $('#open-login').click();
                    }

                    $('.likebutton1').click( function () {
                        var elel =$(this);
                        $.ajax({
                            type: "POST",
                            url: "service-wishlist",
                            data:'_token=<?=csrf_token();?>',
                            success: function(data){
                                //alert('hello');
                                $('#open-project-modal1').click();
                                $('#projectName').html(data);
                                elel.html('<span class="tickgreen">✔</span> Added to Project');
                                elel.unbind();
                            }
                        });
                    });

                    function getcreate(){
                        event.preventDefault();
                        $('#project-modal2').click();
                    }

                    $(document).ready(function () {

                        var navListItems = $('div.setup-panel div a'),
                            allWells = $('.setup-content'),
                            allNextBtn = $('.nextBtn');
                        allPrevBtn = $('.prevBtn');

                        allWells.hide();
                        var ajax_valid = true;
                        navListItems.click(function (e) {
                            e.preventDefault();
                            var $target = $($(this).attr('href')),
                                $item = $(this);

                            if (!$item.hasClass('disabled')) {
                                navListItems.removeClass('btn-success').addClass('btn-default');
                                $item.addClass('btn-success');
                                //allWells.hide('slide', {direction: 'left'}, 1000);
                                //allWells.hide();
                                allWells.hide();
                                $target.show('slide', {direction: 'right'}, 1000);

                                //$target.find('input:eq(0)').focus();

                            }
                        });

                        allNextBtn.click(function () {
                            var curStep = $(this).closest(".setup-content"),
                                curStepBtn = curStep.attr("id"),
                                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                                isValid = true;
                            if(ajax_valid){
                                $(".form-group").removeClass("has-error");
                                for (var i = 0; i < curInputs.length; i++) {
                                    if (!curInputs[i].validity.valid) {
                                        isValid = false;
                                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                                    }
                                }
                            }else{
                                isValid = false;
                            }

                            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
                        });
                        allPrevBtn.click(function () {
                            var curStep = $(this).closest(".setup-content"),
                                curStepBtn = curStep.attr("id"),
                                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
                                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                                isValid = true;
                            allWells.hide();
                            curStep.prev().show('slide', {direction: 'left'}, 1000);
                        });

                        $('div.setup-panel div a.btn-success').trigger('click');
                        $('.pincode').on('keyup', function(){
                            var csrf_token = '<?=csrf_token();?>';
                            var pin = $(this).val();
                            $.ajax({
                                url: 'getlocality',
                                type: 'post',
                                async:false,
                                data: 'pincode='+pin+'&_token='+csrf_token,
                                success:function(data){
                                    if(data !== null){
                                        $('.location').html(data);
                                        if(data == 'Pincode Not Available' || data == 'Invalid Pincode'){
                                            $(".form-group").addClass("has-error");
                                            ajax_valid = false;
                                        }else{
                                            $(".form-group").removeClass("has-error");
                                            ajax_valid = true;
                                        }
                                    }else{
                                        $('.location').html('Pincode');
                                        $(".form-group").addClass("has-error");
                                        ajax_valid = false;
                                    }
                                },
                                error: function(){
                                    $('.location').html('Pincode Not Available');
                                    $(".form-group").addClass("has-error");
                                    ajax_valid = false;
                                }
                            })
                        });
                        $('.select2').select2({
                            placeholder: "Please Select Option...",
                            allowClear: true,
                            width: '100%'
                        });
                    });

                    function getQuestions(service_id){
                        event.preventDefault();
                        $('#open-service-modal').click();
                        $('.popup #data').html('<img class="loader-image" src="assets/images/que-loader.gif"/><p class="loader-label">Loading content...</p>');
                        $.ajax({
                            url: 'get-service-questions',
                            type: 'post',
                            data: 'service_id='+service_id+'&_token=<?=csrf_token(); ?>',
                            success: function(data){
                                $('.popup #data').html(data);
                                reloadFormWizard();
                            },
                            error: function(){

                            }
                        });
                    }

                    function reloadFormWizard(){
                        var navListItems = $('#data div.setup-panel div a'),
                            allWells = $('#data .setup-content'),
                            allNextBtn = $('#data .nextBtn');
                        allPrevBtn = $('#data .prevBtn');
                        allWells.hide();
                        var ajax_valid = true;
                        navListItems.click(function (e) {
                            e.preventDefault();
                            var $target = $($(this).attr('href')),
                                $item = $(this);
                            if (!$item.hasClass('disabled')) {
                                navListItems.removeClass('btn-success').addClass('btn-default');
                                $item.addClass('btn-success');
                                //allWells.hide('slide', {direction: 'left'}, 1000);
                                //allWells.hide();
                                allWells.hide();
                                $target.show('slide', {direction: 'right'}, 1000);

                                //$target.find('input:eq(0)').focus();

                            }
                        });

                        allNextBtn.click(function () {
                            var curStep = $(this).closest(".setup-content"),
                                curStepBtn = curStep.attr("id"),
                                nextStepWizard = $('#data div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                                isValid = true;
                            if(ajax_valid){
                                $("#data .form-group").removeClass("has-error");
                                for (var i = 0; i < curInputs.length; i++) {

                                    if (!curInputs[i].validity.valid) {
                                        isValid = false;
                                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                                    }
                                }
                            }else{
                                isValid = false;
                            }

                            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
                        });
                        allPrevBtn.click(function () {
                            var curStep = $(this).closest(".setup-content"),
                                curStepBtn = curStep.attr("id"),
                                nextStepWizard = $('#data div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
                                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                                isValid = true;
                            allWells.hide();
                            curStep.prev().show('slide', {direction: 'left'}, 1000);
                        });

                        $('#data div.setup-panel div a.btn-success').trigger('click');
                        $('#data .pincode').on('keyup', function(){
                            var csrf_token = '<?=csrf_token();?>';
                            var pin = $(this).val();
                            $.ajax({
                                url: 'getlocality',
                                type: 'post',
                                async:false,
                                data: 'pincode='+pin+'&_token='+csrf_token,
                                success:function(data){
                                    if(data !== null){
                                        $('#data .location').html(data);
                                        if(data == 'Pincode Not Available' || data == 'Invalid Pincode'){
                                            $("#data .form-group").addClass("has-error");
                                            ajax_valid = false;
                                        }else{
                                            $("#data .form-group").removeClass("has-error");
                                            ajax_valid = true;
                                        }
                                    }else{
                                        $('#data .location').html('Pincode');
                                        $("#data .form-group").addClass("has-error");
                                        ajax_valid = false;
                                    }
                                },
                                error: function(){
                                    $('#data .location').html('Pincode Not Available');
                                    $("#data .form-group").addClass("has-error");
                                    ajax_valid = false;
                                }
                            })
                        });
                        $('#data .select2').select2({
                            placeholder: "Please Select Option...",
                            allowClear: true,
                            width: '100%'
                        });
                    }	$(document).ready(function(){		(function() {            [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {                new CBPFWTabs( el );            });        })();	});
                </script>

                <?php echo $footer?>

                <!-- Ask a Question Popup Start -->
                <div class="popup" id="popup-question" data-popup="popup-question">
                    <div class="popup-inner">
                        <div class="col-md-12">
                            <h2 style="text-transform: none;" class="text-center">Ask a Question</h2>
                            <form id="contact_form" action="<?php echo url('faq'); ?>" method="post">
                                <?=csrf_field() ?>
                                <input type="hidden" name="ser_id" value="<?php echo $service->id;?>">
                                <div class="col-md-12 col-sm-12 text-center">
                                    <textarea class="form-control" name="question" rows="5" placeholder="Write question here" required="required"></textarea>
                                    <br>
                                    <button class="btn btn-primary" type="submit">SUBMIT</button>
                                </div>
                            </form>
                        </div>
                        <a class="popup-close" data-popup-close="popup-question" href="#">x</a>
                    </div>
                </div>
                <!-- Ask a Question Popup End -->

                <!-- Ask a Question Popup Start -->
                <div class="popup" id="popup-review" data-popup="popup-review">
                    <div class="popup-inner">
                        <div class="col-md-12">
                            <h5><?=translate('Add a review')?> :</h5>
                            <?php if(customer('id') !== ''){?>
                                <fieldset>
                                    <div class="col-md-12">
                                        <input name="name" readonly value="<?=customer('id')?>" class="form-control" type="hidden">
                                        <input name="service" value="<?=$service->id?>" class="form-control" type="hidden">

                                        <div class="form-group col-md-12">
                                            <label class="control-label"><?=translate('Rating')?></label>
                                            <div id="star-rating">
                                                <input type="radio" name="rating" class="rating" value="1" />
                                                <input type="radio" name="rating" class="rating" value="2" />
                                                <input type="radio" name="rating" class="rating" value="3" />
                                                <input type="radio" name="rating" class="rating" value="4" />
                                                <input type="radio" name="rating" class="rating" value="5" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label"><?=translate('Review')?></label>
                                            <textarea name="review" type="text" rows="5" class="form-control"></textarea>
                                        </div>
                                        <button type="submit" name="submit" class="btn btn-primary" ><?=translate('submit')?></button>
                                    </div>
                                </fieldset>
                            <?php } else { ?>
                                <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                    <div class="col-md-12 text-center">
                                        <button class="btn btn-primary" onClick="getModel()" type="button" value="Login">Login</button>
                                    </div>
                                </div>
                            <?php } ?>
                            </form>
                        </div>
                        <a class="popup-close" data-popup-close="popup-review" href="#">x</a>
                    </div>
                </div>
                <!-- Ask a Question Popup End -->

                <!-- Services Popup Start -->
                <a class="btn btn-primary hidden" id="open-login" data-popup-open="popup-login" href="#">Get Started !</a>
                <div class="popup" id="popup-login" data-popup="popup-login">
                    <div class="popup-inner" style="padding-top: 120px;">
                        <div class="col-md-6 account" style="border: none; box-shadow: none;">
                            <?php
                            if (session('error') != ''){
                                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                            }
                            ?>
                            <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                                <?=csrf_field() ?>
                                <input type="hidden" name="ser_id" value="<?php echo $service->id;?>">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="control-label"><?=translate('E-mail') ?></label>
                                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"><?=translate('Password') ?></label>
                                        <input name="password" type="password" class="form-control"  />
                                    </div>
                                    <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                                </fieldset>
                            </form>
                            <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
                        </div>
                        <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
                    </div>
                </div>
                <!-- Services Popup End -->
                <a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
                <div class="popup" id="popup-project2" data-popup="popup-project2">
                    <div class="popup-inner" style="padding-top: 120px;">
                        <div class="col-md-10 account" style="border: none; box-shadow: none;">
                            <?php
                            if (session('error') != ''){
                                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                            }
                            ?>
                            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                                    <tbody>
                                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                                    <tr>
                                        <td>Project Title</td>
                                        <td>:</td>
                                        <td><input type="text" name="title" required class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td>Project Image</td>
                                        <td>:</td>
                                        <td><input type="file" name="image" required class="form-control"></td>
                                    </tr>
                                    <?php echo csrf_field(); ?>
                                    <tr>
                                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
                    </div>
                </div>

                <button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
                <div class="popup" id="popup-project" data-popup="popup-project">
                    <div class="popup-inner" style="padding-top: 120px;">
                        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
                            <?=csrf_field() ?>
                            <label class="control-label">Select Your Project</label>
                            <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
                            <div id="projectName">

                            </div>
                        </div>
                        <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
                    </div>
                </div>

                <!-- Category Popup Start -->
                <div class="popup" data-popup="popup-1">
                    <div class="popup-inner">
                        <?php if($questions) {  ?>
                            <div class="stepwizard">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step col-xs-3">
                                        <a href="#step-0" type="button" class="btn btn-success btn-circle">0</a>
                                    </div>
                                    <?php
                                    $sr = 1;
                                    foreach($questions as $question){ ?>
                                        <div class="stepwizard-step col-xs-3">
                                            <a href="#step-<?=$sr;?>" type="button" class="btn btn-default btn-circle"><?=$sr; ?></a>
                                        </div>
                                        <?php $sr++; } ?>
                                </div>
                            </div>
                            <form role="form">
                                <div class="panel panel-primary setup-content" id="step-0">
                                    <div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
                                        <div class="form-group">
                                            <h2 class="text-center" style="text-transform: none;">What is the location of your Project?</h2>
                                            <hr style="position: relative; max-width: 100px; margin:40px auto;">
                                            <center><i class="fa fa-map-marker" style="font-size:48px;"></i><br><label class="location">Pincode</label><br><input maxlength="6" minlength="6" type="text"  required="required" class="form-control pincode" style="max-width: 200px;margin:20px 0;" placeholder="Enter Location" /></center>
                                        </div>
                                        <div style="padding: 20px;text-align:center">
                                            <hr>
                                            <button class="btn btn-primary prevBtn" disabled type="button">Previous</button>
                                            <button class="btn btn-primary nextBtn" type="button">Next</button>
                                        </div>
                                    </div>
                                </div>
                                <?php $sr = 1;
                                foreach($questions as $question){ ?>
                                    <div class="panel panel-primary setup-content" id="step-<?=$sr; ?>">
                                        <div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
                                            <div class="form-group">
                                                <h2 class="text-center" style="text-transform: none;"><?php echo getQuestion($question->question_id); ?></h2>
                                                <hr style="position: relative; max-width: 100px; margin:40px auto;">
                                                <?php
                                                $type = getQuestionType($question->question_id);
                                                if($type == 'text'){
                                                    ?>
                                                    <center><i class="fa fa-map-marker" style="font-size:48px;"></i><br><label class="location">Pincode</label><br><input maxlength="6" minlength="6" type="text"  required="required" class="form-control pincode" style="max-width: 200px;margin:20px 0;" placeholder="Enter Location" /></center>
                                                <?php }else if($type == 'single'){
                                                    echo '<div style="position:relative; max-width: 350px;margin:auto;">';
                                                    $options = json_decode(getQuestionOptions($question->question_id));
                                                    $i = 0;
                                                    foreach($options as $option){
                                                        ?>
                                                        <input type="radio" name="answer[]" value="<?=$option;?>" style="margin-right:10px;" id="<?=$sr.$i;?>"></input> <label class="label-hover" for="<?=$sr.$i;?>" style="cursor: pointer;""><?=$option; ?></label><br>
                                                        <?php $i++; } echo '</div>'; }
                                                else if($type == 'multiple'){
                                                    echo '<div style="position:relative; max-width: 350px;margin:auto;">';
                                                    $options = json_decode(getQuestionOptions($question->question_id));
                                                    $i = 0;
                                                    foreach($options as $option){
                                                        ?>
                                                        <input type="checkbox" name="answer[]" value="<?=$option;?>" style="margin-right:10px;" id="<?=$sr.$i;?>"></input> <label class="label-hover" for="<?=$sr.$i;?>" style="cursor: pointer;""><?=$option; ?></label><br>
                                                        <?php $i++; } echo '</div>'; }
                                                else if($type == 'textarea'){
                                                    echo '<div style="position:relative;width: 60%;margin:auto;">';
                                                    ?>
                                                    <textarea name="answer[]" style="margin-right:10px;" rows="7" class="form-control" placeholder="Enter a description..."></textarea>
                                                    <?php echo '</div>'; }
                                                else if($type == 'dropdown'){
                                                    echo '<div style="position:relative; max-width: 350px;margin:auto;">
													<select class="form-control select2" name="answer[]">';
                                                    $options = json_decode(getQuestionOptions($question->question_id));
                                                    $i = 0;
                                                    foreach($options as $option){
                                                        ?>
                                                        <option value="<?=$option;?>"><?=$option; ?></option>
                                                        <?php $i++; } echo '</select></div>'; } ?>
                                            </div>
                                            <div style="padding: 20px;text-align:center">
                                                <hr>
                                                <button class="btn btn-primary prevBtn" <?=($sr == 0) ? 'disabled' : ''; ?> type="button">Previous</button>
                                                <?php if($sr != count($questions)){ ?>
                                                    <button class="btn btn-primary nextBtn" type="button">Next</button>
                                                <?php }else{ ?>
                                                    <button class="btn btn-primary" type="submit">Finish</button>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </div>
                                    <?php $sr++; } ?>
                            </form>
                        <?php }else{
                            echo 'Ooops...! Something Went Wrong. Please try again later';
                        } ?>
                        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
                    </div>
                </div>
                <!-- Category Popup End -->
<?php echo $header?>

<div class="profile-root bg-white ng-scope">
    <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
        <div class="clearfix wrapper-1140 two-bgs">
            <?php if(customer('header_image') != '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') != '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-6" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image'); ?>" class="cover">
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } ?>
        </div>



        <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
            <img itemprop="image" class="profile-icon" src="assets/user_image/<?php if(customer('image') != ''){ echo Customer('image'); } else { echo 'user.png'; } ?>">
            <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                <a itemprop="url" class="url">
                    <?php echo Customer('name'); ?>
                </a>
                <p style="text-align: center;"><a href="seller_account" class="btn">My Profile</a></p>
            </h1>
            <div class="sans-light font-15 line-height-md hidden-xs">
                <p style="text-align: justify; padding: 30px;"><?php echo customer('about_professional'); ?></p>
            </div>
            <div class="sans-light font-15 line-height-md hidden-xs" style="padding-bottom: 0px; position: absolute; width: 100%; bottom: 0;">
                <p style="text-align: center;"><a href="seller_product" class="btn btn-primary col-lg-2">Products</a>
                    <a href="seller_services" class="btn col-lg-2">Services</a>
                    <a href="seller_design" class="btn col-lg-2">Design</a>
                    <a href="seller_advices" class="btn col-lg-2">Advices</a>
                    <a href="my-projects"  class="btn col-lg-2">My Projects</a>
                </p>
            </div>
        </div>

        <div class="container">
            <div class="col-md-12" id="result">
                <br>
                <?php if($msg!='')echo'<alert class=" col-md-12 alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$msg.'</alert>'; ?>
                <br>
            </div>
        </div>
        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">

            <div class="about-designer clearfix">

                <div class="text-left">
                    <section class="at-property-sec" style="padding-top: 0px;">
                        <div class="container" style="margin-left: -15px; width: 1140px;">
                            <ul class="nav nav-pills" style="background-color: white;" id="myTab">
                                <li class="active"><a data-toggle="tab"  href="#product">Product Orders</a></li>
                                <li><a data-toggle="tab"  href="#product-order">My Orders</a></li>
                                <li><a data-toggle="tab"  href="#product-added_by_me">My Products</a></li>
                                <li><a data-toggle="tab"  href="#product-add">Add New Product</a></li>
                                <li><a data-toggle="tab"  href="#transactions">Total Transactions</a></li>
                                <li><a data-toggle="tab"  href="#bill_wise">Bill Wise Transactions</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="product" class="tab-pane fade in active">
                                    <br>
                                    <?php foreach($orders as $pro) {
                                        $products_data = $pro->products;
                                        $products = json_decode($products_data, true);
                                        if (count($products) > 0) {
                                            $ids = "";
                                            foreach ($products as $data) {
                                                $ids = $data['id'];
                                                $ids = rtrim($ids, ',');
                                                $order_products = DB::select("SELECT * FROM products WHERE id IN (".$ids.") ORDER  BY id DESC ")[0];
                                                echo '<div class="col-md-3 col-sm-3">
                                                <div class="at-property-item at-col-default-mar" id="'.$order_products->id . '">
                                                    <div class="at-property-img">
                                                    <a href="' . url('invoice/'.$pro->id).'" data-title="'.translate($order_products->title).'">
                                                        <img src="'.url('/assets/products/'.image_order($order_products->images)).'" style="width: 100%; height: 150px;" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="at-property-location" style="min-height: 15px;">
                                                        <p>
                                                            <a href="'.url('invoice/'.$pro->id).'" data-title="'.translate($order_products->title).'">'.translate($order_products->title).'</a>
                                                            <br><span class="price">'.c($order_products->price).'</span>
                                                            <div class="rating">';


                                                $rates = getRatings($order_products->id);
                                                $tr = $rates['rating'];
                                                $i = 0;
                                                while ($i < 5) {
                                                    $i++;
                                                    echo '<i class="star'.(($i <= $rates["rating"]) ? "-selected" : "").'"></i>';
                                                    $tr--;
                                                }
                                                echo '('.$rates["total_ratings"].')
                                                            </div>
                                                        </p>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div>';
                                            }
                                        }
                                    }
                                    ?>
                                </div>


                                <div id="product-order" class="tab-pane fade">
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="datatable-editable">
                                            <thead>
                                            <tr class="bg-blue">
                                                <th>Sr. No.</th>
                                                <th>PO No.</th>
                                                <th>Product Name</th>
                                                <th>Product Variant</th>
                                                <th>Order Status</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $sr = 1;
                                            $sid = customer('id');
                                            foreach($myorders as $myodr) {
                                                $prod_detail = $myodr->products;
                                                $ord_id = $myodr->id;
                                                $prod_res = json_decode($prod_detail);
                                                foreach ($prod_res as $key => $value) {

                                                    ?>
                                                    <tr>
                                                        <td><?= $sr; ?></td>
                                                        <td><?= $myodr->po_no; ?></td>
                                                        <td><?= $value->title; ?></td>
                                                        <td><?= $value->variants; ?></td>
                                                        <td><?= $myodr->order_status; ?></td>
                                                        <td>
                                                            <input type="hidden" value="<?=$ord_id?>" class="my_order_id">
                                                            <select name="my_order_status" class="form-control my_order_status"  >
                                                                <option value="" selected disabled>Select Status</option>
                                                                <option value="Processing">Processing</option>
                                                                <option value="Shipped">Shipped</option>
                                                                <option value="Delivered">Delivered</option>
                                                                <option value="Cancelled">Cancelled</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <?php $sr++;
                                                }
                                            }

                                            ?>
                                            </tbody>
                                        </table>
                                        <?php
                                        /* $sid = customer('id');
                                        foreach($myorders as $myodr){
                                            $prod_q = DB::select("SELECT * FROM `orders` WHERE id = '$myodr->id'")[0];
                                            $prod_detail =  $prod_q->products;
                                            $prod_res = json_decode($prod_detail);
                                               foreach ( $prod_res as $key => $value){
                                                 $product_id = $value->id;
                                                   $check_prod = DB::select("SELECT * FROM `products` WHERE id = '$product_id' and added_by = '$sid'");
                                                   if(!empty($check_prod)){

                                                       $pro = DB::select('SELECT * FROM products Where id = '.$product_id)[0];
                                                       $cust_name = DB::select('SELECT customers.name FROM `orders` INNER JOIN customers ON orders.customer = customers.id WHERE orders.id = '.$myodr->id)[0];
                                                       echo '<div class="col-md-3 col-sm-3">
                                                <div class="at-property-item at-col-default-mar" id="'.$pro->id.'">
                                                    <div class="at-property-img">
                                                    <a href="product/'.path($pro->title,$pro->id).'" data-title="<'.translate($pro->title).'">
                                                        <img src="'.url('/assets/products/'.image_order($pro->images)).'" style="width: 100%; height: 150px;" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="at-property-location" style="min-height: 15px;">
                                                        <p>
                                                        <span>'.translate($cust_name->name).'</span>


                                                            <div class="cart-btn-custom">
                                                                        <button class="bg">View Details</button>
                                                                    </div>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>';

                                                   }
                                                   else{

                                                   }
                                               }


                                        }*/
                                        ?>

                                    </div>
                                </div>
								
                                <div id="product-added_by_me" class="tab-pane fade">
                                    <br>
                                    <?php
                                    foreach($my_product as $prowish){
                                        $pro = DB::select('SELECT * FROM products Where id = '.$prowish->id)[0];
                                        $all_price = DB::select("SELECT *,(SELECT name FROM customers where id = products.added_by) as seller FROM `products` WHERE status = 'approve' and  sku ='$pro->sku'");
                                       ?>

                                         <div class="a1 hidden" >
                                              <div class="popover-heading">All Sele Price <span style="float:right;cursor:pointer;" class="fa fa-times" data-toggle="popover"></span></div>

                                              <div class="popover-body">
                                                <table style="width:100%" border="1px">
                                                  <tr>
                                                    <th>Saller</th>
                                                    <th>Price</th>
                                                  </tr>

                                                      <?php
                                                            foreach($all_price as $saleprice){
                                                                ?>
                                                                <tr>
                                                                    <td><?=$saleprice->seller;?> </td>
                                                                    <td><?=$saleprice->sale;?> </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                      ?>

                                                </table>
                                              </div>
                                            </div>

                                    <?php
                                        //dd($all_price);
                                        if($pro->status=='pending' || $pro->status=='reject') $color ='red';else $color = 'green';
                                        echo '<div class="col-md-3 col-sm-3">
                                            <div class="at-property-item at-col-default-mar" id="'.$pro->id.'">
                                                <div class="at-property-img">
                                                <a href="product/'.path($pro->title,$pro->id).'" data-title="<'.translate($pro->title).'">
                                                    <img src="'.url('/assets/products/'.image_order($pro->images)).'" style="width: 100%; height: 150px;" alt="">
                                                    </a>
                                                </div>
                                                <div class="at-property-location" style="min-height: 15px;">
                                                    <p>
                                                        <a href="product/'.path($pro->title,$pro->id).'" data-title="'.translate($pro->title).'">'.translate($pro->title).'</a>
                                                        <br><span style="color:green;" class="price" >MRP   '.c($pro->price).'</span>
                                                        <span class="price pull-right" style="color:green;"> Sale '.c($pro->sale).'
                                                            <div class="popover-container" style="float:right;">
                                                                <a class="popover-dw"
                                                                   data-popover="true">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                                <div class="popover-content" style="display:none">
                                                                    <table style="width:100%" border="1px">
                                                                        <tr>
                                                                            <th style="padding: 5px;">Saller</th>
                                                                            <th style="padding: 5px;">Price</th>
                                                                        </tr>';

                                                                        foreach($all_price as $saleprice){
                                                                            echo '<tr>
                                                                                <td style="padding: 5px;"> '.$saleprice->seller.' </td>
                                                                                <td style="padding: 5px; width: 70px;"><i class="fa fa-inr"></i> '.$saleprice->sale.' </td>
                                                                            </tr>';

                                                                        }

                                                                   echo ' </table>
                    
                                                                </div>
                                                            </div>
                                                        </span>
                                                        
                                                        <div >
                                                            Status <span><b style="color:'.$color.'"> '.$pro->status.' </b></span>
                                                        </div>
                                                        <div class="row" style="margin-bottom: 5px;">
                                                            <div class="cart-btn-custom">
                                                                <div class="col-md-6">
                                                                    <button style="width: 100px" class="btn btn-info edit_my_product" value="'.$pro->id.'"><i class="fa fa-pencil" style="color:white;"></i> Edit </button> 
                                                                </div>
                                                                <div class="col-md-6">
                                                                '; ?>
                                                                    <button onClick="return confirm('Do you really want to delete ? ')" style="width: 100px" class="btn btn-info delete_my_product" value="<?=$pro->id;?>" ><i class="fa fa-trash" style="color:white;" "></i> Delete</button>
                                    <?php echo '
                                                                </div>
                                                             </div>
                                                        </div>
                                                        
                                                        <div class="row">
                                                                <div class="cart-btn-custom">
                                                                <div class="col-md-6">
                                                                 <a target="_blank" href="product-variant?id='.$pro->id.'">   <button style="width: 100px" class="btn btn-info add_varient" value="'.$pro->id.'"><i class="fa fa-plus" style="color:white;"></i> Varient </button> </a>
                                                                </div>
                                                                <div class="col-md-6">
                                                                <a target="_blank" href="product-discount?id='.$pro->id.'">   <button style="width: 100px" class="btn btn-info add_discount" value="'.$pro->id.'"><i class="fa fa-gift" style="color:white;"></i> Discount </button> </a>
                                                                    
                                                                </div>
                                                             </div>
                                                        </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>';
                                    }
                                    ?>
                                </div>
                                <div id="product-add" class="tab-pane fade">
                                    <br>
                                    <div class="col-md-12">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Add New Product</h3>
                                            </div>
                                            <div class="panel panel-info">
                                                <div class="panel-body">
                                                    <div class="col-md-4">
                                                        Enter SKU Code
                                                        [<b><button type="button" class="btn btn-info btn-xs" onClick="getModel()">Find SKU Code</button></a></b>]
                                                        <input type="text" id="sku" name="sku" placeholder="Enter SKU">
                                                    </div>
                                                    <div class="col-md-4"><br>
                                                        <input type="button" id="check_product" class="btn btn-default" value="Go"> &#09; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; OR
                                                    </div>
                                                    <div class="col-md-4"><br>
                                                        <input type="button" class="btn btn-default" id="add_new_product_button" value="Add New Product">
                                                        <input name="product_id" type ='hidden' id="insert_check" value="<?php if(isset($insert_check));echo $insert_check; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body" id="add_product_tab_old_sku">
                                               <?php  echo '<form action="" id="add_product_form" method="post" enctype="multipart/form-data" class="form-horizontal single">
                                                '.csrf_field().'
                                                <fieldset style="padding: 2.35em 9.625em 0.75em;">
                                                     <input id="product_id_edit_detail" name="product_id" type="hidden"  class="form-control"/>
                                                    <div class="form-group">
                                                        <label class="control-label">sku Code</label>
                                                        <input id="pro_sku" name="sku" type="text"  class="form-control"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Product name</label>
                                                        <input id="pro_name" name="title" type="text"  class="form-control" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Product category</label>
                                                        <select id="pro_category" name="category" class="form-control" >';
                                                        foreach ($categories as $category){
                                                            echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                                            $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
                                                            foreach ($childs as $child){
                                                                echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
                                                                $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id." ORDER BY id DESC");
                                                                foreach ($subchilds as $subchild){
                                                                    echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                                                }
                                                            }
                                                        }
                                                        echo '</select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">HSN Code</label>
                                                        <input id="pro_hsn" name="hsn" type="text" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Market price</label>
                                                        <input name="price" id="pro_price" type="text" class="form-control" required />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Sale price</label>
                                                        <input name="sale" type="text" id="pro_sale" class="form-control" required />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Minimum Order Quantity</label>
                                                        <input name="min" type="text" id="pro_min" class="form-control" required value="0"/>
                                                    </div>';
                                                        $is =  Customer('institutional_status');
                                                           if($is!=0){
                                                               echo '<div id="institutional_form" style="outline: 1px solid !important;">
                                                                
                                                                   <h4>Fill Detail for Institutional</h4>
                                                                        <label class="control-label">Market price</label>
                                                                        <input name="insti_price" type="text" class="form-control" required />
                                                                    
                                                                        <label class="control-label">Sale price</label>
                                                                        <input name="insti_sale" type="text" class="form-control" required />
                                                                    
                                                                        <label class="control-label">Minimum Order Quantity</label>
                                                                        <input name="insti_min" type="text" class="form-control" required value="0"/>
                                                                    </div> ';
                                                           }
                                                    echo '<div class="form-group">
                                                        <label class="control-label">Available quantity</label>
                                                        <input name="q" type="text" id="pro_q" class="form-control" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Tax (%)</label>
                                                        <input name="tax" type="text" id="pro_tax" class="form-control" required />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Weight</label>
                                                        <input name="weight" type="text" id="pro_weight" class="form-control" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Weight Unit</label>
                                                        <select id="pro_weight_unit" name="weight_unit" class="form-control">
                                                            <option>Select Weight Unit</option>';
                                                            foreach ($units as $unit){
                                                                echo '<option value="'.$unit->id.'">'.$unit->symbol.'</option>';
                                                            }
                                                        echo '</select>
                                                    </div>
                                                    <input id="old_sku_button" name="add_old_sku" type="submit" value="Add product" class="btn btn-primary" />
                                                </fieldset>
                                            </form>'; ?>
                                        </div>
                                        <!--Add product if Already sku end-->

                                        <!--Add product if NEW sku start-->
                                        <div class="panel-body" id="add_product_tab_new_sku">
                                            <?php  echo '<form action="" id="add_product_form_new" method="post" enctype="multipart/form-data" class="form-horizontal single">
                                            '.csrf_field().'
                                                <fieldset style="padding: 2.35em 9.625em 0.75em;">
                                                    <div class="form-group">
                                                        <!--<label class="control-label">Enter sku Code</label>-->
                                                        <input type="hidden" id="sku" name="sku" class="form-control" value=""  placeholder="Enter sku" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Product name</label>
                                                        <input  name="title" type="text"  class="form-control" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Product category</label>
                                                        <select  name="category" class="form-control" >';
                                                        foreach ($categories as $category){
                                                            echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                                            $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
                                                            foreach ($childs as $child){
                                                                echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
                                                                $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id." ORDER BY id DESC");
                                                                foreach ($subchilds as $subchild){
                                                                    echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                                                }
                                                            }
                                                        }
                                                        echo '</select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Bought Together Category(s)</label><br>
                                                        <select name="fb_cat[]" multiple class="form-control select2">';
                                                        foreach ($categories as $category){
                                                            echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                                            $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
                                                            foreach ($childs as $child){
                                                                echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
                                                                $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id." ORDER BY id DESC");
                                                                foreach ($subchilds as $subchild){
                                                                    echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                                                }
                                                            }
                                                        }
                                                        echo '</select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Product description</label>
                                                        <textarea name="text" type="text" id="editor1" class="form-control" required></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Product specification</label>
                                                        <textarea name="specification" id="editor" type="text" class="form-control" required></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">HSN Code</label>
                                                        <input name="hsn" type="text" class="form-control" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Market price</label>
                                                        <input name="price" type="text" class="form-control" required />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Sale price</label>
                                                        <input name="sale" type="text" class="form-control" required />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Minimum Order Quantity</label>
                                                        <input name="min" type="text" class="form-control" required value="0"/>
                                                    </div>';
                                                    $is =  Customer('institutional_status');
                                                    if($is!=0){
                                                        echo '<div id="institutional_form" style="outline: 1px solid !important;">
                                                    
                                                       <h4>Fill Detail for Institutional</h4>
                                                            <label class="control-label">Market price</label>
                                                            <input name="insti_price" type="text" class="form-control" required />
                                                        
                                                            <label class="control-label">Sale price</label>
                                                            <input name="insti_sale" type="text" class="form-control" required />
                                                        
                                                            <label class="control-label">Minimum Order Quantity</label>
                                                            <input name="insti_min" type="text" class="form-control" required value="0"/>
                                                        
                                                    
                                                        </div> ';
                                                    }

                                                    echo '<div class="form-group">
                                                        <label class="control-label">Tax (%)</label>
                                                        <input name="tax" type="text" class="form-control" required />
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Available quantity</label>
                                                        <input name="q" type="text" class="form-control" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Weight</label>
                                                        <input name="weight" type="text" class="form-control" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Weight Unit</label>
                                                        <select name="weight_unit" class="form-control">
                                                            <option>Select Weight Unit</option>';
                                                            foreach ($units as $unit){
                                                                echo '<option value="'.$unit->id.'">'.$unit->symbol.'</option>';
                                                            }
                                                        echo '</select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Product images</label>
                                                        <input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"  required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Download ( digital product )</label>
                                                        <input type="file" class="form-control" name="download"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Customer options</label>
                                                        <input type="hidden" id="option_count"/>
                                                        <div id="add_option" class="button pull-right">Add field</div>
                                                    </div>
                                                    <div class="col-lg-12" id="options"></div>
                                                    <input name="add_new_sku" type="submit" value="Add product" class="btn btn-primary" />
                                                </fieldset>
                                            </form>'; ?>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div id="transactions" class="tab-pane fade in">
                                    <br>
                                    <!--div class="row">
                                        <div class="col-lg-12">
                                            <table class="table-responsive" style="width: 100%;">
                                                <tr>
                                                    <td>Filter by Date</td>
                                                    <td>From Date</td>
                                                    <td><input type="text" name="from_date" class="form-control" id="datepicker" placeholder="From Date"> </td>
                                                    <td style="padding-left: 5px;">To Date</td>
                                                    <td><input type="text" name="to_date" id="datepicker1" class="form-control" placeholder="To Date"></td>
                                                    <td><input type="submit" name="filter" class="btn btn-primary" value="Filter"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div-->
                                    <hr>
                                    <div class="table-responsive scroll-vertical" id="filterData"></div>
                                    <div class="table-responsive scroll-vertical" id="nonFilter">
                                        <table class="table table-striped table-bordered" id="datatable-editable">
                                            <thead>
                                            <tr class="bg-blue">
                                                <th>Date</th>
                                                <th>Invoice No.</th>
                                                <th>Payment Type</th>
                                                <th>Remark</th>
                                                <th>Narration</th>
                                                <th>Dr</th>
                                                <th>Cr</th>
                                                <th>Running Balance</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $balamount = 0;
                                            $oid = '';
                                            $tsale = 0;
                                            $pamount = 0;
                                            $bamount = 0;
                                            foreach ($balance as $mpay){
                                                echo'<tr>
                                                    <td>'.$mpay->date_paid.'</td>
                                                    <td>';
                                                    if($mpay->paid_amount == 0 ) {
                                                        echo $mpay->order_id;
                                                    } else { echo ''; }
                                                    echo ' </td>
                                                    <td>'.$mpay->payment_type.'</td>
                                                    
                                                    <td>';
                                                if($mpay->amount == 0 ){
                                                    if($mpay->cr_id != ''){
                                                        echo 'Cr. Note No.'.$mpay->cr_id;
                                                    }
                                                    elseif ($mpay->dr_id != ''){
                                                        echo 'Dr. Note No.'.$mpay->dr_id;
                                                    }
                                                    else {
                                                        echo 'Invoice No.' . $mpay->order_id;
                                                    }
                                                }else { echo ''; }
                                                echo '</td>




                                                <td>'.$mpay->remark.'</td>
                                                    <td>';
                                                if($mpay->paid_amount != 0 ) {
                                                    echo '<i class="fa fa-inr"></i> ' . $mpay->paid_amount ;
                                                }else { echo ''; }
                                                echo '</td>
                                                    <td>';
                                                if($mpay->amount != 0 ){
                                                    echo '<i class="fa fa-inr"></i> '.$mpay->amount;
                                                }else { echo ''; }
                                                echo '</td>
                                                    <td><i class="fa fa-inr"></i> '.$mpay->balance_amount.'</td>
                                                </tr>';

                                                $pamount = $pamount + $mpay->paid_amount;
                                                $oid = $mpay->order_id;
                                            }
                                            $selid = customer('id');
                                            $saleamount = DB::select("SELECT t2.amount from (
SELECT mp.order_id,mp.amount FROM multiseller_payments as mp INNER JOIN (
SELECT order_id FROM multiseller_payments WHERE seller_id = '$selid' GROUP BY order_id ) as t1 on mp.order_id = t1.order_id ) as t2 GROUP by t2.order_id");
                                            foreach($saleamount as $sale){
                                                $tsale = $tsale + $sale->amount;
                                            }

                                            $balamt = DB::select("SELECT * FROM multiseller_payments WHERE seller_id = '$selid' ORDER BY id DESC ");
                                            if($balamt !== ''){
                                                $balamount = DB::select("SELECT * FROM multiseller_payments WHERE seller_id = '$selid' ORDER BY id DESC ")[0];
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4" style="text-align: right;">Total Sales : <i class="fa fa-inr"></i> <?=$tsale; ?></div>
                                            <div class="col-lg-4" style="text-align: center;">Received Amount : <i class="fa fa-inr"></i> <?=$pamount; ?></div>
                                            <div class="col-lg-4" style="text-align: left;">Balance Amount : <i class="fa fa-inr"></i> <?php if($balamount == ''){ echo ''; } else { $balamount->balance_amount;} ?></div>
                                        </div>
                                    </div>
                                </div>


                                <div id="bill_wise" class="tab-pane fade in">
                                    <hr>
                                    <div class="table-responsive scroll-vertical">
                                        <table class="table table-striped table-bordered" id="datatable-editable1">
                                            <thead>
                                            <tr class="bg-blue">
                                                <th>Invoice No.</th>
                                                <th>Invoice Amount</th>
                                                <th>Balance Amount</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($billwise as $bill){
                                                echo'<tr>
                                                    <td><input type="hidden" id="oid" value="'.$bill->order_id.'">'.$bill->order_id.'</td>
                                                    <td><input type="hidden" id="amt" value="'.$bill->amount.'">'.$bill->amount.'</td>
                                                    <td>'.getBalanceAmount($bill->order_id).'</td>
                                                    <td><button class="btn btn-primary" onClick="viewDetail()">View Detail</button> </td>
                                                </tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <style>
        .button {
            background: gainsboro;
            padding: 5px 20px;
            cursor: pointer;
            border-radius: 30px;
        }
        .mini-button {
            cursor: pointer;
            border-radius: 30px;
            background: transparent;
            padding: 5px 0px;
            display: block;
        }
        .select2-container {
            width:100% !important;
        }



    </style>
    <script>
        $( document ).ready(function() {

            $('#add_product_tab_old_sku').hide();
            $('#add_product_tab_new_sku').hide();

            $('#sku_div').hide();

            function activaTab(tab){
                $('#myTab a[href="#' + tab + '"]').tab('show');
            };

            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if(activeTab){
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }

            $('#add_new_product_button').click(function () {
                $('#add_product_tab_old_sku').hide();
                $('#add_product_tab_new_sku').show();
            });

            $('.edit_my_product').on('click', function () {
                $('#add_product_tab_old_sku').show();
                activaTab('product-add');
                var pid = $(this).val();
                $.ajax({
                    url: 'my-product-edit',
                    type: 'post',
                    data: 'pid='+pid+'&_token=<?=csrf_token(); ?>',
                    success: function(datax){
                        $('#add_product_tab_new_sku').hide();
                        var data = JSON.parse(datax);
                        $('#product_id_edit_detail').attr({'value': data.id,'readonly':true});
                        $('#pro_sku').attr({'value': data.sku,'readonly':true});
                        $('#pro_name').attr({'value': data.title,'disabled':'disabled'});
                        $('#pro_category option[value='+ data.category +']').attr({'selected':'selected'});
                        $('#pro_category').attr({'disabled':'disabled'});
                        $('#old_editor1').attr({'value' : data.text,'disabled':'disabled'});
                        $('#old_editor').attr({'value': data.specification,'disabled':'disabled'});
                        $('#pro_hsn').attr({'value': data.hsn,'disabled':'disabled'});
                        $('#pro_price').attr({'value': data.price,'disabled':'disabled'});
                        $('#pro_min').attr({'value': data.min_qty});
                        $('#pro_sale').attr({'value': data.sale});
                        $('#pro_tax').attr({'value': data.tax,'disabled':'disabled'});
                        $('#pro_q').attr({'value': data.quantity});
                        $('#pro_weight').attr({'value': data.weight,'disabled':'disabled'});
                        $('#pro_weight_unit option[value='+ data.weight_unit +']').attr({'selected':'selected'});
                        $('#pro_weight_unit').attr({'disabled':'disabled'});
                        $('#old_sku_button').attr({'value':'Update','name':'update_old_sku'});
                    }
                });
            });

            $('.delete_my_product').click(function () {
                var pid = $(this).val();
                $.ajax({
                    url: 'my-product-delete',
                    type: 'post',

                    data: 'pid='+pid+'&_token=<?=csrf_token(); ?>',
                    success: function(datax){
                        $("#result").html('Record Delete Succesfully! Please wait while..');
                        $("#result").addClass("alert alert-success offset4 span4");
                        window.location.reload();
                    }
                });
            });
            CKEDITOR.replace( 'editor' );
            CKEDITOR.replace( 'editor1' );

                $("[data-toggle=popover]").popover({
                    html: true,
                    content: function() {
                        var content = $(this).attr("data-popover-content");
                        return $(content).children(".popover-body").html();
                    },
                    title: function() {
                        var title = $(this).attr("data-popover-content");
                        return $(title).children(".popover-heading").html();
                    }
                });

            $('body').popover({
                selector: '[data-popover]',
                trigger: 'click ',
                placement: 'left',
                html: true,
                delay: {show: 50, hide: 400},
                content: function(ele) {
                    console.log(ele,this);
                    return $(this).next(".popover-content").html();
                }
            });

            $('.select2').select2();
            $('[data-toggle="popover"]').popover();

        });

        function option_count(type){
            var count = $('#option_count').val();
            if(type == 'add'){
                count++;
            }
            if(type == 'reduce'){
                count--;
            }
            $('#option_count').val(count);
        }

        $("#add_option").click(function(){
            option_count('add');
            var co = $('#option_count').val();
            $("#options").append(''
                +'<div class="form-group" data-no="'+co+'">'
                +'    <div class="col-sm-6">'
                +'        <input type="text" name="option_title[]" class="form-control"  placeholder="Title">'
                +'    </div>'
                +'    <div class="col-sm-5">'
                +'        <select class="form-control option_type" name="option_type[]" >'
                +'            <option value="text">Text input</option>'
                +'            <option value="select">Dropdown - single select</option>'
                +'            <option value="multi_select">Checkbox - multi select</option>'
                +'            <option value="radio">Radio</option>'
                +'        </select>'
                +'        <div class="options">'
                +'          <input type="hidden" name="option_set'+co+'[]" value="none" >'

                +'        </div>'
                +'    </div>'
                +'    <input type="hidden" name="option_no[]" value="'+co+'" >'

                +'    <div class="col-sm-1">'
                +'        <span class="remove mini-button"><i class="icon-close"></i></span>'
                +'    </div>'
                +'</div>'
            );
        });

        $("#options").on('change','.option_type',function(){
            var co = $(this).closest('.form-group').data('no');
            if($(this).val() !== 'text'){
                $(this).closest('div').find(".options").html('<div class="pull-right button add_option">Add option</div>');
            } else {
                $(this).closest('div').find(".options").html('<input type="hidden" name="option_set'+co+'[]" value="none" ><input type="hidden" name="option_set'+co+'[]" value="none" >');
            }
        });

        $("#options").on('click','.add_option',function(){
            var co = $(this).closest('.form-group').data('no');
            $(this).closest('.options').prepend(''
                +'    <div style="margin: 10px -15px;">'
                +'        <div class="col-sm-5">'
                +'          <input type="text" name="option_set'+co+'[]" class="form-control required"  placeholder="Option">'

                +'        </div>'
                +'        <div class="col-sm-5">'
                +'          <input type="text" name="option_set'+co+'[]" class="form-control required"  placeholder="Price">'

                +'        </div>'
                +'        <div class="col-sm-1">'
                +'          <span class="remove-option mini-button"><i class="icon-close"></i></span>'
                +'        </div>'
                +'        <div class="clearfix"></div>'
                +'    </div>'
            );
        });

        $('body').on('click', '.remove', function(){
            $(this).parent().parent().remove();
        });

        $('body').on('click', '.remove-option', function(){
            var co = $(this).closest('.form-group').data('no');
            $(this).parent().parent().remove();
            if($(this).parent().parent().parent().html() == ''){
                $(this).parent().parent().parent().html(''
                    +'   <input type="hidden" name="option_set'+co+'[]" value="none" >'

                );
            }
        });
        $('#check_product').click(function () {
            var sku = $('#sku').val();
            $.ajax({
                url: 'get-product-detail',
                type: 'post',

                data: 'sku='+sku+'&_token=<?=csrf_token(); ?>',
                success: function(datax){
                    $('#add_product_tab_new_sku').hide();
                    var data = JSON.parse(datax);
                    if(data.error){
                        alert("Not Found");
                    }else {
                        $('#add_product_tab_old_sku').show();
                        $('#pro_sku').attr({'value': data.sku,'readonly':true});
                        $('#pro_name').attr({'value': data.title,'disabled':'disabled'});
                        $('#pro_category option[value='+ data.category +']').attr({'selected':'selected'});
                        $('#pro_category').attr({'disabled':'disabled'});
                        /*$('#old_editor1').attr({'value': data.text,'disabled':'disabled'});
                        $('#old_editor').attr({'value': data.specification,'disabled':'disabled'});*/
                        $('#pro_hsn').attr({'value': data.hsn,'disabled':'disabled'});
                        $('#pro_price').attr({'value': data.price,'disabled':'disabled'});
                        $('#pro_min').attr({'value': ''});
                        $('#pro_sale').attr({'value': ''});
                        $('#pro_tax').attr({'value': data.tax,'disabled':'disabled'});
                        $('#pro_q').attr({'value': ''});
                        $('#pro_weight').attr({'value': data.weight,'disabled':'disabled'});
                        $('#pro_weight_unit option[value='+ data.weight_unit +']').attr({'selected':'selected'});
                        $('#pro_weight_unit').attr({'disabled':'disabled'});
                    }
                }
            });
        });
        function editVariant(vid){
            $.ajax({
                url: 'get-variant',
                type: 'post',
                data: 'id='+vid+'&_token=<?=csrf_token(); ?>',
                success: function(datax){
                    alert(datax);
                    var data = JSON.parse(datax);
                    $('#modal-form input[name=var_id]').val(data.id);
                    $('#modal-form input[name=variant_title]').val(data.variant_title);
                    $('#modal-form input[name=price]').val(data.price);
                    if(data.display == 0){
                        $('#modal-form .display').html('<div class="form-group col-md-12">'
                            +'<label class="control-label">Display</label><br>'
                            +'<input name="display" type="radio" id="yes" value="1" /><label for="yes">Yes</label> &nbsp; <input name="display" type="radio" id="no" value="0" checked="checked"/><label for="no">No</label>'
                            +'</div>');
                    }else{
                        $('#modal-form .display').html('<div class="form-group col-md-12">'
                            +'<label class="control-label">Display</label><br>'
                            +'<input name="display" type="radio" id="yes" value="1" checked="checked"/><label for="yes">Yes</label> &nbsp; <input name="display" type="radio" id="no" value="0" checked="checked"/><label for="no">No</label>'
                            +'</div>');
                    }
                    $('#edit-button').trigger('click');
                }
            });
        }

        function editDiscount(vid){
            $.ajax({
                url: 'get-discount',
                type: 'post',
                data: 'id='+vid+'&_token=<?=csrf_token(); ?>',
                success: function(datax){
                    var data = JSON.parse(datax);
                    $('#modal-form input[name=dis_id]').val(data.id);
                    $('#modal-form input[name=quantity]').val(data.quantity);
                    $('#modal-form input[name=discount]').val(data.discount);
                    $('#modal-form input[name=discount_type]').val(data.discount_type);
                    if(data.discount_type == '%'){
                        $('#modal-form .discount_type').html('<label class="control-label">Discount Type</label>'
                            +'<select name="discount_type" class="form-control" required="required">'
                            +'	<option value=""> Please Select Discount Type </option>'
                            +'	<option value="%" selected="selected"> % </option>'
                            +'	<option value="Flat"> Flat </option>'
                            +'</select>');
                    }else{
                        $('#modal-form .discount_type').html('<label class="control-label">Discount Type</label>'
                            +'<select name="discount_type" class="form-control" required="required">'
                            +'	<option value=""> Please Select Discount Type </option>'
                            +'	<option value="%"> % </option>'
                            +'	<option value="Flat" selected="selected"> Flat </option>'
                            +'</select>');
                    }
                    $('#edit-dis-button').trigger('click');
                }
            });
        }


        function viewDetail(){
            var oid = $('#oid').val();
            var amount = $('#amt').val();
            $.ajax({
                url: 'view-detail',
                type: 'post',
                data: 'order_id='+oid+'&amount='+amount+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    event.preventDefault();
                    $('#view_detail').click();
                    $("#viewdetaildata").html(data);
                }
            });
        }

    </script>
    <?php echo $footer?>

    <script>
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });
        function getprofile(){
            event.preventDefault();
            $('#personal-modal').click();
        }

        function getModel(){
            event.preventDefault();
            $('#find_sku').click();
        }


        $(document).on('click','#find_sku_btn', function() {
           var pid = $('#find_for_sku').val();
            $.ajax({
                url: 'find-sku',
                type: 'post',

                data: 'pid='+pid+'&_token=<?=csrf_token(); ?>',
                success: function(datax){
                    $('#sku_div').show();
                    $('#get_sku').val(datax);
                }
            });
        });


        function copyFunction() {
            var copyText = document.getElementById("get_sku");
            copyText.select();
            document.execCommand("copy");
            $("#copy_alert").html("Copied SKU Code: " + copyText.value);
            $("#copy_alert").addClass("alert alert-success col-md-6 col-sm-offset-2");
            $('#sku').val(copyText.value);

        }

        var insert_value_check = $('#insert_check').val();
        $(window).on('load',function(){
            if(insert_value_check!=0) {
                $('#find_variants').click();
            }
        });

        $(document).ready(function () {
            $("#check_all").click(function () {
                $(".variant_check").prop('checked', $(this).prop('checked'));
            });
        });

        function getcreate(){
            event.preventDefault();
            $('#project-modal').click();
        }

        function showProject(val) {
            document.getElementById("myForm").submit();
        }
    </script>

    <button class="btn btn-primary hidden" id="open-pro-modal1" data-popup-open="popup-pro">Get Started !</button>
    <div class="popup" id="popup-pro" data-popup="popup-pro">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-lg-12 account" style="border: none; box-shadow: none;">
                <?=csrf_field() ?>
                <div class="grid" id="projectName">

                </div>
            </div>
            <a class="popup-close" id="pclose" data-popup-close="popup-pro" href="#">x</a>
        </div>
    </div>

    <a class="btn btn-primary hidden" id="project-modal" data-popup-open="popup-project" href="#">Get Started !</a>
    <div class="popup" id="popup-project" data-popup="popup-project">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                        <tbody>
                        <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                        <tr>
                            <td>Project Title</td>
                            <td>:</td>
                            <td><input type="text" name="title" required class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Project Image</td>
                            <td>:</td>
                            <td><input type="file" name="image" required class="form-control"></td>
                        </tr>
                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-project" href="#">x</a>
        </div>
    </div>

    <!-- Variants Popup Start -->
    <a class="btn btn-primary hidden" id="view_detail" data-popup-open="popup-view-detail" href="#">Get Started !</a>
    <div class="popup" id="popup-view-detail" data-popup="popup-view-detail">
        <div class="popup-inner" style="padding-top: 120px;">
            <div id="viewdetaildata"></div>
            <a class="popup-close" data-popup-close="popup-view-detail" href="#">x</a>
        </div>
    </div>
    <!-- Variants Popup End -->



    <!-- Variants Popup Start -->
    <a class="btn btn-primary hidden" id="find_variants" data-popup-open="popup-variants" href="#">Get Started !</a>
    <div class="popup" id="popup-variants" data-popup="popup-variants">
        <div class="popup-inner" style="padding-top: 120px;">
            <form action="seller_product" method="post">
                <div class="col-md-2">
                    <?php echo csrf_field(); ?>
                    <input name="product_id" type ='hidden' value="<?php if(isset($insert_check));echo $insert_check; ?>">
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="control-label">Select Variants </label><br>
                        <input type="checkbox" value="all" id="check_all"> Check All <br>
                        <?php  if(!empty($this_variants)){ foreach ($this_variants as $variants){
                            ?>
                            <div class="col-md-8">
                                <input type="checkbox" name="variant_id[]" value="<?=$variants->id;?>" class="variant_check">
                                <span><?=$variants->variant_title;?></span>
                            </div>
                            <div class="col-md-4" style="padding-bottom: 5px;">
                                <input type="text" class="form-control" name="price[]" placeholder="Enter Price">
                            </div>
                            <br>
                        <?php } }?>
                    </div>
                </div>
                <div class="col-md-2"><br>
                    <button name="variant_add" type="submit" class="btn btn-lg btn-info">Add</button>
                </div>
            </form>
            <a class="popup-close" data-popup-close="popup-variants" href="#">x</a>
        </div>
    </div>
    <!-- Variants Popup End -->

    <!-- Services Popup Start -->
    <a class="btn btn-primary hidden" id="find_sku" data-popup-open="popup-login" href="#">Get Started !</a>
    <div class="popup" id="popup-login" data-popup="popup-login">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="row">
                <div id="copy_alert"></div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">Select Product </label>
                    <select id="find_for_sku" name="find_for_sku" class="form-control select2" >
                     <?php   foreach ($all_product as $product){
                        ?> <option value="<?=$product->id;?>"><?=$product->title;?></option>
                        <?php } ?>
                    </select>
                    <div id="sku_div">
                        <b>SKU Code is</b>
                        <br>
                        <div class="col-md-8">
                            <input type="text" value="" id="get_sku" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <button onclick="copyFunction()" class="btn btn-sm btn-info" class="popup-close" data-popup-close="popup-login">Copy SKU</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3"><br>
                <button type="button" class="btn btn-info" id="find_sku_btn" >Find</button>
            </div>
            <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
        </div>
    </div>
    <!-- Services Popup End -->



    <a class="btn btn-primary hidden" id="personal-modal" data-popup-open="popup-personal" href="#">Get Started !</a>
    <div class="popup" id="popup-personal" data-popup="popup-personal">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('account'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                        <tbody>
                        <tr>
                            <td>Name (*)</td>
                            <td>:</td>
                            <td><input type="text" name="name" required class="form-control" value="<?php echo Customer('name'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Mobile (*)</td>
                            <td>:</td>
                            <td><input type="text" name="mobile" required class="form-control" value="<?php echo Customer('mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Alternate Mobile</td>
                            <td>:</td>
                            <td><input type="text" name="alternate_mobile" class="form-control" value="<?php echo Customer('alternate_mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Email (*)</td>
                            <td>:</td>
                            <td><input type="text" name="email" required class="form-control" value="<?php echo Customer('email'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Profile Image</td>
                            <td>:</td>
                            <td><input type="file" name="image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 1</td>
                            <td>:</td>
                            <td><input type="file" name="header_image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 2</td>
                            <td>:</td>
                            <td><input type="file" name="header_image_1" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td>Company (*)</td>
                            <td>:</td>
                            <td><input type="text" name="company" required class="form-control" value="<?php echo Customer('company'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 1 (*)</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_1" required class="form-control" value="<?php echo Customer('address_line_1'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 2</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_2" class="form-control" value="<?php echo Customer('address_line_2'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Country (*)</td>
                            <td>:</td>
                            <td>
                                <select name="country" id="country_shipping" required class="form-control" onChange="getStates(this.value, 'state_shipping')">
                                    <option value="<?php Customer('country'); ?>"><?php echo getCountryName(Customer('country')); ?></option>
                                    <option value="">Select country</option>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>State (*)</td>
                            <td>:</td>
                            <td>
                                <select name="state" class="form-control" required id="state_shipping" onChange="getCities(this.value, 'city_shipping')">
                                    <option value="<?php Customer('state'); ?>"><?php echo getStateName(Customer('state')); ?></option>
                                    <option value="">Please select region, state or province</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>City (*)</td>
                            <td>:</td>
                            <td>
                                <select name="city" class="form-control" required id="city_shipping">
                                    <option value="<?php Customer('city'); ?>"><?php echo getCityName(Customer('city')); ?></option>
                                    <option value="">Please select city or locality</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Postcode (*)</td>
                            <td>:</td>
                            <td><input type="text" name="postcode" required class="form-control" value="<?php echo Customer('postcode'); ?>"></td>
                        </tr>
                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-personal" href="#">x</a>
        </div>
    </div>
    <script>
        /*abhijeet code start*/
        $(document).ready(function(){
            $('.my_order_status').change(function () {
                var s = $(this).val();
                var oid = $(this).closest('tr').find('.my_order_id').val();
                $.ajax({
                    url: 'shipping-status-change',
                    type: 'post',

                    data: 'oid='+oid+'&status='+s+'&_token=<?=csrf_token(); ?>',
                    success: function(datax){
                        alert("Status Update Successfully");
                    }
                });
            });
        })
        /*abhijeet code end*/
    </script>
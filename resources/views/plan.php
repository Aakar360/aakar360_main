<?php echo $header?>
    <section class="at-property-sec at-property-right-sidebar" style="padding: 0;">
       <div class="container">
            <div class="content product-page">
                <div class="col-md-8" style="padding: 0;">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner text-center">
							<?php for($i=0; $i<count($images); $i++){
								if($i == 0){?>
								<div class="item active" style="padding: 2px;">
									<img src="<?=url('/assets/layout_plans/'.$images[$i])?>" style="width:100%;" />
                                    <div class="image-actions">
                                        <?php if(customer('id') !== ''){
                                            $user_id = customer('id');
                                            $image_id = $layout->id;
                                            $img = DB::select("SELECT * FROM plan_like WHERE user_id = '".$user_id."' AND plan_id = '".$image_id."'");
                                            if(!empty($img)){
                                                ?>
                                                <button class="btn btn-primary" id="likeplan"><span class="tickgreen">✔</span> Liked</button>
                                            <?php } else{?>
                                                <button class="btn btn-primary" id="likeplan" onclick="likePro(this.value);"><i class="fa fa-heart"></i> Like</button>
                                            <?php }
                                            } else { ?>
                                            <button class="btn btn-primary" onClick="getModel()" ><i class="fa fa-heart"></i> Like</button>
                                        <?php } ?>
                                    </div>
								</div>
							<?php }  else {?>
								<div class="item" style="padding: 2px;">
									<img src="<?=url('/assets/layout_plans/'.$images[$i])?>" style="width:100%;" />
                                    <div class="image-actions">
                                        <?php if(customer('id') !== ''){
                                            $user_id = customer('id');
                                            $image_id = $layout->id;
                                            $img = DB::select("SELECT * FROM plan_like WHERE user_id = '".$user_id."' AND plan_id = '".$image_id."'");
                                            if(!empty($img)){
                                                ?>
                                                <button class="btn btn-primary" id="likeplan"><span class="tickgreen">✔</span> Liked</button>
                                            <?php } else{?>
                                                <button class="btn btn-primary" id="likeplan" onclick="likePro(this.value);"><i class="fa fa-heart"></i> Like</button>
                                            <?php } ?>
                                        <?php  } else { ?>
                                            <button class="btn btn-primary" onClick="getModel()" ><i class="fa fa-heart"></i> Like</button>
                                        <?php } ?>
                                    </div>
								</div>
							<?php }} ?>
						</div>
						<!-- End Carousel Inner -->
						<ul class="pro-image-slider" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
							<?php for($i=0; $i<count($images); $i++){
							if($i == 0){?>
								<li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active" >
									<a href="#"><img src="<?=url('/assets/layout_plans/thumbs/'.$images[$i])?>"/></a>
								</li>
							<?php } else {?>
								<li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>">
									<a href="#"><img src="<?=url('/assets/layout_plans/thumbs/'.$images[$i])?>"/></a>
								</li>
							<?php }} ?>
						</ul>
					</div>
					<div class="icon-holder">
                        <?php $amm = json_decode($layout->interior_ammenities);
                        $i = 1;
                        //dd($amm);
                        foreach($amm as $key=>$value){
                            $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                            ?>
                            <div class="icons mrgntop"><b><?=$a->name.' : '.$value; ?></div>
                        <?php } ?>
					</div>
				</div>
				<div class="col-md-4" >
					<h3><?=translate($layout->title)?></h3>
					<form class="form" style="margin-top: 30px;">
                        <?php if($layout->p1_name != '' && $layout->p2_name != '' && $layout->p3_name != ''){ ?>
						<div class="option">
							<h6>Select Plan Sets :</h6>
							<select name="plan_type" class="form-control select2">
                                <?php if($layout->p1_name != ''){?>
								<option value="1"><?=$layout->p1_name; ?> (<?=c($layout->p1_price);?>)</option>
                                <?php } if($layout->p2_name != ''){?>
								<option value="2"><?=$layout->p2_name; ?> (<?=c($layout->p2_price);?>)</option>
                                <?php } if($layout->p3_name != ''){?>
								<option value="3"><?=$layout->p3_name; ?> (<?=c($layout->p3_price);?>)</option>
                                <?php } ?>
							</select>
						</div>
                        <?php }else{ ?>
                            <p class="final-price" style="text-align: left;"><label class="custom-control-label">Price : </label> <?=c($layout->price);?></p>
                        <?php } ?>
                        <?php if($addons){ ?>
						<div class="option" style="margin-top: 30px;">
							<h6>Select Adons :</h6>
							<select name="adons" multiple class="form-control select2">
                                <?php foreach($addons as $addon){
                                    $id = $addon->layout_addon_id;
                                    $add = DB::select("SELECT * FROM layout_addon WHERE id = '".$id."'")[0];
                                ?>
								    <option value="<?php echo $add->id; ?>"><?php echo $add->name; ?> (<?=c($add->price);?>)</option>
                                <?php } ?>
							</select>
						</div>
                        <?php } ?>
						<div class="option" style="margin-top: 30px;">
							<button class="btn btn-primary checkout" type="button" style="width: 100%;"><i class="fa fa-cart-plus"></i> Add to Cart</button>
						</div>
						<div class="option" style="margin-top: 15px;">
                            <?php if(customer('id') !== ''){ ?>
                                <button class="btn btn-primary" data-popup-open="popup-1" type="button" style="width: 100%;"><i class="fa fa-pencil"></i> Modify Plan</button>
                            <?php } else { ?>
                                <button class="btn btn-primary" onClick="getModel()" type="button"  style="width: 100%;"><i class="fa fa-pencil"></i> Modify Plan</button>
                            <?php } ?>

						</div>
                        <div class="option" style="margin-top: 15px;">
                        <?php if(customer('id') !== ''){
                            $user_id = customer('id');
                            $layout_id = $layout->id;

                            $img = DB::select("SELECT * FROM layout_wishlist WHERE user_id = '".$user_id."' AND layout_id = '".$layout_id."'");

                            if(!empty($img)){
                                ?>
                                <button class="btn btn-primary" id="likebutton" style="width: 100%;"><span class="tickgreen">✔</span> Added to Project</button>
                            <?php } else{?>
                                <a class="btn btn-primary likebutton" id="likebutton" style="width: 100%;"><i class="fa fa-heart"></i> Add to Project</a>

                            <?php }} else { ?>
                            <button class="btn btn-primary" onClick="getModel()" style="width: 100%;"><i class="fa fa-heart"></i> Add to Project</button>
                        <?php } ?>
                        </div>
					</form>
				</div>
				<div class="clearfix"></div>
            </div>


            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color: #ffffff;"><h4 class="panel-title">
                            Description
                        </h4></a>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p><?=$layout->description?></p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="color: #ffffff;"><h4 class="panel-title">
                            Specification
                        </h4></a>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p><?=$layout->specification?></p>
                        </div>
                    </div>
                </div>
				<div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" style="color: #ffffff;"><h4 class="panel-title">
                            Deisgn Views
                        </h4></a>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>
								
							<?php 
								$dv = json_decode($layout->drawing_views);
								if(count($dv) > 0 ){
									echo '<h4>'.$dv[0].'</h4>';
									echo '<button class="btn btn-sm btn-primary pull-right" style="margin: 3px;" data-flip="h0" onClick="flipH(0)"><i class="fa fa-exchange"></i> Flip Horizontal</button>';
									echo '<button class="btn btn-sm btn-primary pull-right" style="margin: 3px;" data-flip="v0" onClick="flipV(0)"><i class="fa fa-exchange fa-rotate-90"></i> Flip Vertical</button>';
									$fl = explode(',',$layout->file);
									if(count($fl) > 0)
										echo '<center><img id="img0" src="assets/layout_plans/'.$fl[0].'" style="width: 60%;"/> </center>';?>
									<br>
									<br>
									<?php
									if(count($dv) > 1){
										echo '<h4>'.$dv[1].'</h4>';
										echo '<button class="btn btn-sm btn-primary pull-right" style="margin: 3px;" data-flip="h1" onClick="flipH(1)"><i class="fa fa-exchange"></i> Flip Horizontal</button>';
										echo '<button class="btn btn-sm btn-primary pull-right" style="margin: 3px;" data-flip="v1" onClick="flipV(1)"><i class="fa fa-exchange fa-rotate-90"></i> Flip Vertical</button>';
										if(count($fl) > 1)
											echo '<center><img id="img1" src="assets/layout_plans/'.$fl[1].'" style="width: 60%;"/></center> ';
									}
								}
								?></p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" style="color: #ffffff;"><h4 class="panel-title">
                            Ratings & Reviews
                        </h4></a>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">

                            <div class="feedback-container">
                                <div class="col-lg-12">
                                    <div class="col-lg-4">
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style="margin-bottom: 0px; font-family: Sans-Serif; font-style: bold;">Avarage rating <?=$avg_rating;?></h5>
                                                <div class="rating" style="padding-bottom: 10px;">
                                                    <?php
                                                    $a=$avg_rating;
                                                    $b = explode('.',$a);
                                                    $first = $b[0];
                                                    $rr = $avg_rating;
                                                    $i = 0;
                                                    while($i<5){ $i++;?>
                                                        <i class="star<?=($i<=$avg_rating) ? '-selected' : '';?>"></i>
                                                        <?php $rr--; }
                                                    echo '</div>';
                                                    ?>
                                                    <h5 style=" font-family: Sans-Serif; font-style: bold;">Total rating <?=$total_user;?></h5>
                                            </li>
                                        </ul>
                                        <hr>
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style=" font-family: Sans-Serif; font-style: bold;">Total comment <?=$total_reviews; ?></h5>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-8 reviews-graph">
                                        <ul>
                                            <li class="float-left">
                                                <?php
                                                if($rating5 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">5 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating5->rating5_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating5->rating5_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating5->rating5_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating4 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">4 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating4->rating4_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating4->rating4_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating4->rating4_user;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating3 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">3 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating3->rating3_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating3->rating3_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating3->rating3_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating2 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">2 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating2->rating2_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating2->rating2_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating2->rating2_user;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating1 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">1 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating1->rating1_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating1->rating1_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating1->rating1_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12" style="padding-bottom: 10px;">
                                <form action="<?php echo url('review');?>" method="post" class="form-horizontal single">
                                    <?=csrf_field();?>
                                    <h5 style="float: left; width: 100%; margin-top: 10px; border-top: 1px solid #000000; padding-top: 20px;"><?=translate('Add a review')?> :</h5>
                                    <div id="response" style="float: left; width: 100%"></div>
                                    <?php if(customer('id') !== ''){?>
                                        <fieldset style="float: left; width: 100%;">
                                            <div class="col-md-12">
                                                <input name="name" readonly value="<?=customer('id')?>" class="form-control" type="hidden">
                                                <input name="product" value="<?=$layout->id?>" class="form-control" type="hidden">

                                                <div class="form-group col-md-12">
                                                    <label class="control-label"><?=translate('Rating')?></label>
                                                    <div id="star-rating">
                                                        <input type="radio" name="rating" class="rating" value="1" />
                                                        <input type="radio" name="rating" class="rating" value="2" />
                                                        <input type="radio" name="rating" class="rating" value="3" />
                                                        <input type="radio" name="rating" class="rating" value="4" />
                                                        <input type="radio" name="rating" class="rating" value="5" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label"><?=translate('Review')?></label>
                                                    <textarea name="review" type="text" rows="5" class="form-control"></textarea>
                                                </div>
                                                <button data-product="<?=$layout->id?>" name="submit" class="btn btn-primary" ><?=translate('submit')?></button>
                                            </div>
                                        </fieldset>
                                    <?php } else { ?>
                                        <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                            <div class="col-md-12 text-center">
                                                <button class="btn btn-primary" onClick="getModel()" type="button" value="Login">Login</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </form>
                                <br><br><br>
                                <p>
                                    <?php
                                    $count = 0;
                                    foreach($reviews as $review){
                                        $usid = $review->name;
                                        $uimg = DB::select("SELECT * FROM customers WHERE id = '".$usid."'")[0];
                                        echo '<img class="review-image" src="assets/user_image/'; if($uimg->image != ''){ echo $uimg->image; } else { echo 'user.png'; }; echo'">
											<div class="review-meta"><b>'.$uimg->name.'</b><br/>
											<span class="time">'.date('M d, Y',$review->time).'</span><br/></div>
											<div class="review">
												<div class="rating pull-right">';
                                        $rr = $review->rating; $i = 0; while($i<5){ $i++;?>
                                            <i class="star<?=($i<=$review->rating) ? '-selected' : '';?>"></i>
                                            <?php $rr--; }
                                        echo '</div>
											<div class="clearfix"></div>
											<p>'.nl2br($review->review).'</p></div>';
                                        $count++;
                                    }
                                    if($count > 0){
                                        echo '<hr/>';
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" style="color: #ffffff;"><h4 class="panel-title">
                                FAQ
                            </h4></a>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <section class="at-property-sec at-property-right-sidebar" id="faq">
                                <div class="">
                                    <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                        <div class="col-md-12 text-center">
                                            <?php if(customer('id') !== ''){ ?>
                                                <button class="btn btn-primary" type="button" data-popup-open="popup-question" value="Ask a Question">Ask a Question</button>
                                            <?php } else { ?>
                                                <button class="btn btn-primary" onClick="getModel()" type="button" value="Ask a Question">Ask a Question</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul>
                                                <?php foreach($faqs as $faq){?>
                                                    <li>
                                                        <input type="checkbox" checked>
                                                        <i></i>
                                                        <h5>Q : <?php echo $faq->question; ?><br><small>
                                                                <?php $u = $faq->user_id;
                                                                $user = DB::select("SELECT * FROM customers WHERE id = '$u'")[0];
                                                                ?>
                                                                by <?php echo $user->name; ?> on <?php $d = $faq->time; echo date('d M, Y', strtotime($d)); ?></small></h5>
                                                        <p>A : <?php echo $faq->answer; ?></p>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </section>

                            <!-- Ask a Question Popup Start -->
                            <div class="popup" id="popup-question" data-popup="popup-question">
                                <div class="popup-inner">
                                    <div class="col-md-12">
                                        <h2 style="text-transform: none;" class="text-center">Ask a Question</h2>
                                        <form id="contact_form" action="<?php echo url('layout-faq');?>" method="post">
                                            <?=csrf_field() ?>
                                            <input type="hidden" name="layout_id" value="<?=$layout->id;?>" />
                                            <div class="col-md-12 col-sm-12 text-center">
                                                <textarea class="form-control" name="question" rows="5" placeholder="Write question here" required="required"></textarea>
                                                <br>
                                                <button class="btn btn-primary" type="submit">SUBMIT</button>
                                            </div>
                                        </form>
                                    </div>
                                    <a class="popup-close" data-popup-close="popup-question" href="#">x</a>
                                </div>
                            </div>
                            <!-- Ask a Question Popup End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Inner page heading end -->
<?php if($layout->p1_name != '' && $layout->p2_name != '' && $layout->p3_name != ''){ ?>
	<section class="at-property-sec">
		<div class="container">
			<div class="row">
                <div class="col-lg-12">
                    <div class="box-heading" style="padding-left: 10px;">
                        <div class="at-sec-title">
                            <h2 style="font-size: 15px;">Plan <span>Details</span> </h2>
                            <div class="at-heading-under-line" style="margin: auto;">
                                <div class="at-heading-inside-line" ></div>
                            </div>
                            <p class="sub-title">Specedoor is the best way to design your home. We work within any budget, big or small. You can start from scratch or work with a designer using your existing furniture pieces.</p>
                        </div>
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="agent-carousel-4" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>
                    <?php if($layout->p1_name != ''){?>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricingTable">
                            <div class="pricingTable-header">
                                <h3 class="title"><?=$layout->p1_name; ?></h3>
                                <div class="price-value">
                                    <div class="value">
                                        <span class="amount"><?=c($layout->p1_price, 0);?></span>
                                        <span class="month"><?=$layout->p1_duration; ?></span>
                                    </div>
                                </div>
                            </div>
                            <ul class="pricing-content">
                                <?=$layout->p1_description; ?>
                            </ul>
                            <a href="javascript:void(0);" class="btn btn-primary" style="position: absolute;bottom: 30px;left: 45px;right: 45px;" data-id="1" data-rate="<?=$layout->p1_price; ?>"><i class="fa fa-dot-circle-o"></i> Select</a>
                        </div>
                    </div>
                    <?php } if($layout->p2_name != ''){?>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricingTable">
                            <div class="pricingTable-header">
                                <h3 class="title"><?=$layout->p2_name; ?></h3>
                                <div class="price-value">
                                    <div class="value">
                                        <span class="amount"><?=c($layout->p2_price, 0);?></span>
                                        <span class="month"><?=$layout->p2_duration; ?></span>
                                    </div>
                                </div>
                            </div>
                            <ul class="pricing-content">
                                <?=$layout->p2_description; ?>

                            </ul>
                            <a href="javascript:void(0);" class="btn btn-primary" style="position: absolute;bottom: 30px;left: 45px;right: 45px;" data-id="2" data-rate="<?=$layout->p2_price; ?>"><i class="fa fa-dot-circle-o"></i> Select</a>
                        </div>
                    </div>
                    <?php } if($layout->p3_name != ''){?>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricingTable">
                            <div class="pricingTable-header">
                                <h3 class="title"><?=$layout->p3_name; ?></h3>
                                <div class="price-value">
                                    <div class="value">
                                        <span class="amount"><?=c($layout->p3_price, 0);?></span>
                                        <span class="month"><?=$layout->p3_duration; ?></span>
                                    </div>
                                </div>
                            </div>
                            <ul class="pricing-content">
                                <?=$layout->p3_description; ?>
                            </ul>
                            <a href="javascript:void(0);" class="btn btn-primary" style="position: absolute;bottom: 30px;left: 45px;right: 45px;" data-id="3" data-rate="<?=$layout->p3_price; ?>"><i class="fa fa-dot-circle-o"></i> Select</a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
			</div>
		</div>
	</section>
<?php } ?>
	<section class="at-property-sec">
		<div class="container">
            <?php if($addons){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="at-sec-title">
                            <h2 style="font-size: 15px;">Available <span>Adons</span></h2>
                            <div class="at-heading-under-line">
                                <div class="at-heading-inside-line"></div>
                            </div>
                            <p class="sub-title">Select one or more if you need additional work to be done in this package.</p>
                        </div>
                    </div>
                </div>
                <div class="row adons">
                    <?php foreach($addons as $addon){
                    $id = $addon->layout_addon_id;
                    $add = DB::select("SELECT * FROM layout_addon WHERE id = '".$id."'")[0];
                    ?>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="adons-inner" data-id="<?php echo $add->id; ?>" data-price="<?=$add->price;?>"><img src="<?=url('/assets/addon/'.$add->image)?>"/><br><h6><?php echo $add->name; ?></h6><h6><?=c($add->price);?></h6></div>
                    </div>
                    <?php } ?>
                </div>
            <?php } ?>
			<div class="row" style="margin-top: 30px;" >
				<div class="col-md-12 text-center">
					<h5 id="total" style="text-transform: none;"></h5>
					<button class="btn btn-primary checkout" name="checkout">Add To Cart</button>
				</div>
			</div>
            <form name="checkForm" method="post" action="add-plan-in-session">
                <?=csrf_field(); ?>
                <input type="hidden" name="layout_id" value="<?=$layout->id;?>" />
                <input type="hidden" name="adons" value=""/>
                <input type="hidden" name="package" value=""/>
                <input type="hidden" name="package_price" value="<?=$layout->price;?>"/>
            </form>
		</div>
	</section>
	<section class="at-property-sec">
	<div class="container">
		<main>
			<input type="hidden" value="<?=$layout->slug; ?>" name="design_slug"/>
			
		<?php echo csrf_field(); ?>
		  <input id="tab1" type="radio" name="tabs" checked onclick="getRelatedPlans()">
		  <label for="tab1" >Related Plans</label>
			
		  <input id="tab2" type="radio" name="tabs" onclick="getRecentViewed();">
		  <label for="tab2">Recently Viewed Designs</label>
			<section id="content1">
				<div class="row">
					<div class="col-md-12" id="data1">
						<div class="grid">
							<?php foreach($related as $rel){  ?>
									
								<div class="grid-item">
									<div class="tiles">
										<a href="<?php echo url('/layout_plan/'.$rel->slug); ?>">
											<img src="<?php echo url('/assets/layout_plans/'.image_order($rel->images)); ?>"/>
											<div class="icon-holder">
                                                <?php $amm = json_decode($rel->interior_ammenities);
                                                $i = 1;
                                                //dd($amm);
                                                foreach($amm as $key=>$value){
                                                    $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                                                    //dd($a);
                                                    if($i <= 3){
                                                        if($i%3 == 0){ ?>
                                                            <div class="icons mrgntop borderRight0"><b><?=$a->name.' : '.$value; ?></b></div>
                                                        <?php }else{ ?>
                                                            <div class="icons mrgntop"><b><?=$a->name.' : '.$value; ?></b></div>
                                                        <?php }
                                                    }else{
                                                        if($i%3 == 0){ ?>
                                                            <div class="icons mrgnbot borderRight0"><b><?=$a->name .' : '.$value; ?></b></div>
                                                        <?php }else{ ?>
                                                            <div class="icons mrgnbot"><b><?=$a->name .' : '.$value; ?></b></div>
                                                        <?php } } ?>
                                                    <?php $i++; } ?>
											</div>
										</a>
										
									</div>
								</div>
								<?php } ?>
						</div>
					</div>
				</div>
			</section>
			<section id="content2">
				<div class="row">
					<div class="col-md-12" id="data2">
					</div>
				</div>
			</section>

		</main>
		</div>
	</section>
    <?php if(!empty($link)){?>
        <div class="container">
            <div class="blockquote blockquote--style-1">
                <div class="row inner-div">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <img src="<?=url('/assets/products/'.image_order($link->image))?>" style="width:50%;height:auto;"/>
                        </div>
                        <div class="col-md-7" style="padding-top: 25px;">
                            <h3><?=$link->content; ?></h3>
                        </div>
                        <div class="col-md-2" style="padding:15px 0 0 0">
                            <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                GET STARTED
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

<?php echo $footer?>

<script src="<?=$tp;?>/assets/jquery.flexslider.js"></script>
<script src="<?=$tp;?>/assets/jquery.zoom.js"></script>

<script>
jQuery(window).on('load', function(){ 
	var $ = jQuery;
	var $container = $('.grid');
    $container.masonry({
          columnWidth:10, 
          gutterWidth: 15,
          itemSelector: '.grid-item'
    });
});
(function() {

	[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
		new CBPFWTabs( el );
	});

})();
$(function() {
//----- OPEN
$('[data-popup-open]').on('click', function(e) {
var targeted_popup_class = jQuery(this).attr('data-popup-open');
$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
e.preventDefault();
});
//----- CLOSE
$('[data-popup-close]').on('click', function(e) {
var targeted_popup_class = jQuery(this).attr('data-popup-close');
$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
e.preventDefault();
});
});
$( document ).ready(function() {
	
	$('#star-rating').rating();
	$('#carousel').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: true,
		slideshow: true,
		itemWidth: 210,
		itemMargin: 5,
		minItems: 4,
		maxItems: 6,
		asNavFor: '#slider'
	});
	
	$('#slider').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: true,
		slideshow: true,
		sync: "#carousel",
		touch: true,
		keyboard: true,
		smoothHeight: true, 
	});
	$('.select2').select2();
	var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');
        allPrevBtn = $('.prevBtn');

    allWells.hide();
var ajax_valid = true;
    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            //allWells.hide('slide', {direction: 'left'}, 1000);
			//allWells.hide();
			allWells.hide();
            $target.show('slide', {direction: 'right'}, 1000);
			
            //$target.find('input:eq(0)').focus();
			
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
            isValid = true;
		if(ajax_valid){
			$(".form-group").removeClass("has-error");
			for (var i = 0; i < curInputs.length; i++) {
				if (!curInputs[i].validity.valid) {
					isValid = false;
					$(curInputs[i]).closest(".form-group").addClass("has-error");
				}
			}
		}else{
			isValid = false;
		}

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });
	allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
            isValid = true;
			allWells.hide();
			curStep.prev().show('slide', {direction: 'left'}, 1000);
    });

    $('div.setup-panel div a.btn-success').trigger('click');

    var packprice = 0.00;
    $('a.btn').on('click', function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var price = $(this).attr('data-rate');
        $('div.adons-inner.selected').each(function(){
            price = parseFloat(price) + parseFloat($(this).attr('data-price'));
        });
        packprice = $(this).attr('data-rate');
        $('input[name=package]').val(id);
        $("select[name=plan_type]").val(id).trigger("change");

        $('a.btn').each(function(){
            $(this).html('Select').removeClass('selected');
        });
        $(this).addClass('selected');
        $(this).html('<span class="tickgreen">✔</span> Selected');
        $('form[name=checkForm]').attr('action', 'add-plan-in-session');
        $('#total').html("Total : <?php echo c(-1); ?> "+price);
    });
	$('.adons-inner').on('click', function(){
		if($(this).hasClass('selected')){
			$(this).removeClass('selected');
		}else{
			$(this).addClass('selected');
		}
		var price = 0.00;
        var adons = [];
        $('div.adons-inner.selected').each(function(){
            adons.push($(this).attr('data-id'));
            price = price + parseFloat($(this).attr('data-price'));
        });
        $("select[name=adons]").val(adons).trigger("change");
        $('input[name=adons]').val(adons.join(','));

        //alert(packprice);

        if(price == 0.00) {
            price = packprice;
        } else {
            price = parseFloat(packprice) + parseFloat(price);
        }
        $('#total').html("Total : <?php echo c(-1); ?> "+price);
        $('input[name=package-price]').val(price);
	});

    //$('#total').html("Total : <?php echo c(-1); ?> "+packprice);

    $('button.checkout').on('click', function(){
        $('form[name=checkForm]').submit();
    });
	$('select[name=adons]').on('change', function(){
		if($(this).val() == null){
            $('.adons-inner').removeClass('selected').removeClass('new');
            $('input[name=adons]').val('');
            var price = 0.00;
            $('#total').html("Total : <?php echo c(-1); ?> "+price);
            $('input[name=package-price]').val(price);
		}else{
			var datax = $(this).val();
			var data = datax.toString().split(',');
			$.each(data, function(key, value){
				var ele = $('.adons-inner[data-id='+value+']');
				ele.addClass('new');
				if(!ele.hasClass('selected')){
					ele.click();
				}
			});

            $('.adons-inner.selected').each(function(){
                if(!$(this).hasClass('new')){
                    $(this).removeClass('selected');
                    var price = 0.00;
                    var adons = [];
                    $('div.adons-inner.selected').each(function(){
                        adons.push($(this).attr('data-id'));
                    });
                    $('input[name=adons]').val(adons.join(','));

                    //alert(packprice);

                    $('div.adons-inner.selected').each(function(){
                        price = price + parseFloat($(this).attr('data-price'));
                    });
                    if(price == 0.00) {
                        price = packprice;
                    } else {
                        price = parseFloat(packprice) + parseFloat(price);
                    }
                    $('#total').html("Total : <?php echo c(-1); ?> "+price);
                    $('input[name=package-price]').val(price);
                }
                $(this).removeClass('new');
            });
		}
	});
});
function flipH(id){
	$('#img'+id).removeClass('flippedV');
	$('button[data-flip=v'+id+']').removeClass('active');
	if($('#img'+id).hasClass('flippedH')){
		$('#img'+id).removeClass('flippedH');
		$('button[data-flip=h'+id+']').removeClass('active');
	}else{
		$('#img'+id).addClass('flippedH');
		$('button[data-flip=h'+id+']').addClass('active');
	}
}
function flipV(id){
	$('#img'+id).removeClass('flippedH');
	$('button[data-flip=h'+id+']').removeClass('active');
	if($('#img'+id).hasClass('flippedV')){
		$('#img'+id).removeClass('flippedV');
		$('button[data-flip=v'+id+']').removeClass('active');
	}else{
		$('#img'+id).addClass('flippedV');
		$('button[data-flip=v'+id+']').addClass('active');
	}
}
function getModel(){
    event.preventDefault();
    $('#open-service-modal1').click();
}
</script>
<script>
    $('.likebutton').click( function () {
        var elel =$(this);
        $.ajax({
            type: "POST",
            url: "layout-wishlist",
            data:'_token=<?=csrf_token();?>',
            success: function(data){
                //alert('hello');
                $('#open-project-modal1').click();
                $('#projectName').html(data);
                elel.html('<span class="tickgreen">✔</span> Added to Project');
                elel.unbind();
            }
        });
    });

    function likePro() {
        $.ajax({
            type: "POST",
            url: "like-plan",
            data:'_token=<?=csrf_token();?>&plan_id=<?=$layout->id; ?>',
            success: function(data){
                $('#likeplan').html('<span class="tickgreen">✔</span> Liked');
            }
        });
    }

    function addLayProject(val) {
        var ele =$(this);
        var project_id = val;
        $.ajax({
            type: "POST",
            url: "add-lay-project",
            data:'_token=<?=csrf_token();?>&layout_id=<?=$layout->id; ?>&project_id='+project_id,
            success: function(data){
                $('#pclose').click();
                ele.unbind();
            }
        });
    }

    function getcreate(){
        event.preventDefault();
        $('#project-modal2').click();
    }
</script>

<button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <?=csrf_field() ?>
            <label class="control-label">Select Your Project</label>
            <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
            <div id="projectName">

            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>

<a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
<div class="popup" id="popup-project2" data-popup="popup-project2">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
    </div>
</div>

<!-- Services Popup Start -->
<a class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login" href="#">Get Started !</a>
<div class="popup" id="popup-login" data-popup="popup-login">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-6 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                <?=csrf_field() ?>
                <input type="hidden" name="ser_id" value="<?php echo $layout->id;?>">
                <fieldset>
                    <div class="form-group">
                        <label class="control-label"><?=translate('E-mail') ?></label>
                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?=translate('Password') ?></label>
                        <input name="password" type="password" class="form-control"  />
                    </div>
                    <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                </fieldset>
            </form>
            <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
        </div>
        <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
    </div>
</div>
<!-- Services Popup End -->


<!-- Category Popup Start -->
    <div class="popup" data-popup="popup-1">
        <div class="popup-inner">
            <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
	            <?php if($questions) {  ?>
				    <div class="stepwizard">
					    <div class="stepwizard-row setup-panel">
						    <div class="stepwizard-step col-xs-3">
							    <a href="#step-0" type="button" class="btn btn-success btn-circle">0</a>
						    </div>
					        <?php
					        $sr = 1;
					        foreach($questions as $question){ ?>
						    <div class="stepwizard-step col-xs-3">
							    <a href="#step-<?=$sr;?>" type="button" class="btn btn-default btn-circle"><?=$sr; ?></a>
						    </div>
					        <?php $sr++; } ?>
					    </div>
				    </div>
				    <form role="form" action="<?php echo url("update-plan-answer"); ?>" method="post">
                        <?=csrf_field() ?>
				        <div class="panel panel-primary setup-content" id="step-0">
                            <input type="hidden" name="plan_id" value="<?php echo $layout->id; ?>">
						    <div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
							    <div class="form-group">
								    <h6 style="margin: 0;">1. Tell Us About Your Modifications	</h6>
								    <p>Send us a description of the changes you want to make, using the form below. We will send you a confirmation email letting you know we are working on a quote.</p>
								    <br>
								    <h6 style="margin: 0;">2. Review Your No-Obligation Quote</h6>
    								<p>One of our designers will review your request and provide a custom quote within 3 business days. The quote will include the price for the modifications, plus the time frame needed to complete the changes.</p>
	    							<br>
		    						<h6 style="margin: 0;">3. Submit Payment & Hire A Designer</h6>
			    					<p>Once you approve the quote and submit payment, your designer will get to work!</p>
				    				<br>
					    			<h6 style="margin: 0;">4. Receive Your Modified Plans!</h6>
						    		<p>Custom modifications typically take 3-4 weeks, but can vary depending on the volume and complexity of the changes. The exact time frame to complete your plans will be specified in the quote.</p>
							    </div>
							    <div style="padding: 20px;text-align:center">
								<hr>
								    <button class="btn btn-primary prevBtn" disabled type="button">Previous</button>
								    <button class="btn btn-primary nextBtn" type="button">Next</button>
							    </div>
						    </div>
					    </div>
    				    <?php $sr = 1;
	    				    foreach($questions as $question){ ?>
                        <div class="panel panel-primary setup-content" id="step-<?=$sr; ?>">
	                        <div class="panel-body" style="padding:40px 20px 10px 20px;min-height: 350px;">
							    <div class="form-group">
								    <h2 class="text-center" style="text-transform: none;"><?php echo getQuestion($question->question_id); ?></h2>
								    <hr style="position: relative; max-width: 100px; margin:40px auto;">
								    <?php
									$type = getQuestionType($question->question_id); 
									if($type == 'text'){
								    ?>
								    <center><i class="fa fa-map-marker" style="font-size:48px;"></i><br><label class="location">Pincode</label><br>
                                        <input maxlength="6" minlength="6" type="text"  name="answer[pincode]" required="required" class="form-control pincode" style="max-width: 200px;margin:20px 0;" placeholder="Enter Location" /></center>
									<?php }else if($type == 'single'){ 
										echo '<div style="position:relative; max-width: 350px;margin:auto;">';
										$options = json_decode(getQuestionOptions($question->question_id));
										$i = 0;
										foreach($options as $option){
									?>
									<input type="radio" name="answer['<?=$question->question_id; ?>']" value="<?=$option;?>" style="margin-right:10px;" id="<?=$sr.$i;?>"></input> <label class="label-hover" for="<?=$sr.$i;?>" style="cursor: pointer;""><?=$option; ?></label><br>
									    <?php $i++; } echo '</div>'; }
										else if($type == 'multiple'){ 
										echo '<div style="position:relative; max-width: 350px;margin:auto;">';
										$options = json_decode(getQuestionOptions($question->question_id));
										$i = 0;
										foreach($options as $option){
									?>
									<input type="checkbox" name="answer['<?=$question->question_id; ?>']" value="<?=$option;?>" style="margin-right:10px;" id="<?=$sr.$i;?>"></input> <label class="label-hover" for="<?=$sr.$i;?>" style="cursor: pointer;""><?=$option; ?></label><br>
									    <?php $i++; } echo '</div>'; }
										else if($type == 'textarea'){ 
										echo '<div style="position:relative;width: 60%;margin:auto;">';
									?>
									<textarea name="answer['<?=$question->question_id; ?>']" style="margin-right:10px;" rows="7" class="form-control" placeholder="Enter a description..."></textarea>
									<?php echo '</div>'; }
										else if($type == 'dropdown'){ 
										echo '<div style="position:relative; max-width: 350px;margin:auto;">
													<select class="form-control select2" name="answer[]">';
										$options = json_decode(getQuestionOptions($question->question_id));
										$i = 0;
										foreach($options as $option){
									?>
									<option value="<?=$option;?>"><?=$option; ?></option>
									
										<?php $i++; } echo '</select></div>'; } ?>
							    </div>
							    <div style="padding: 20px;text-align:center">
								<hr>
								    <button class="btn btn-primary prevBtn" <?=($sr == 0) ? 'disabled' : ''; ?> type="button">Previous</button>
								    <?php if($sr != count($questions)){ ?>
									<button class="btn btn-primary nextBtn" type="button">Next</button>
								    <?php }else{ ?>
									<button class="btn btn-primary" type="submit">Finish</button>
								    <?php } ?>
								</div>
						    </div>
						</div>
					    <?php $sr++; } ?>
					</form>
                <?php }else{
	            echo 'Ooops...! Something Went Wrong. Please try again later';
                } ?>
            </div>
        </div>
<!-- Category Popup End -->


<?php echo $header?>
    <section class="pdtopbtm-50">
		<div class="row">
			<button class="btn btn-default filter-button" style="margin-top: 10px;margin-bottom: 10px;"><i class="fa fa-filter"></i> Filter</button>
			<aside class="sidebar-shop col-md-3 order-md-first">
			<div class="filter-div-close"></div>
				<div class="sidebar-wrapper at-categories" style="padding: 10px;">
					<div class="widget">
						<h3 class="widget-title">
							<a data-toggle="collapse" href="#widget-body-c" role="button" aria-expanded="true" aria-controls="widget-body-c" class="cat_accord">Categories</a>
						</h3>
						<div class="show collapse" id="widget-body-c">
							<div class="widget-body" style="height: 250px; overflow-y: auto;">
								<ul class="cat-list">
									<?php foreach($cats as $cat){
										echo '<li><a href="./'.$cat->layout.'/'.$cat->slug.'">'.translate($cat->name).'</a></li>';
										$childs = DB::select("SELECT * FROM design_category WHERE parent = ".$cat->id." ORDER BY id DESC");
										foreach ($childs as $child){
											echo '<li><a href="./'.$child->layout.'/'.$child->slug.'">- '.$child->name.'</a></li>';
										}
									}
									?>
								</ul>
							</div><!-- End .widget-body -->
						</div><!-- End .collapse -->
					</div><!-- End .widget -->
				</div>
			</aside>
			<div class="filter-wrapper"></div>
			<div class="col-md-9 col-lg-9">
				<div class="grid">
				<?php foreach($galleries as $gallery){ ?>
					<div class="grid-item">
						<div class="tiles">
							<a href="photo-gallery/<?=$gallery->slug; ?>">
								<img src="assets/images/gallery/<?=$gallery->image; ?>"/>
								<div class="icon-holder">
									<div class="icons1 mrgntop"><?=substr(translate($gallery->title), 0, 20); if(strlen($gallery->title)>20) echo '...';?></div>
									<div class="icons2 mrgntop borderRight0"><?=getPlanPhotsCount($gallery->id); ?> Photos</div>
								</div>
							</a>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>

		
    </section>
    <!-- Inner page heading end -->


<?php echo $footer?>
<script>
jQuery(window).on('load', function(){ 
	var $ = jQuery;
	var $container = $('.grid');
    $container.masonry({
          columnWidth:10, 
          gutterWidth: 15,
          itemSelector: '.grid-item'
    });
});
$(document).ready(function(){
	$(document).on('click', '.filter-button', function(){
		$('aside.sidebar-shop').addClass('open');
	});
	$(document).on('click', '.filter-div-close', function(){
		$('aside.sidebar-shop').removeClass('open');
	});
});
</script>
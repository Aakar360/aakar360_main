<!DOCTYPE html>
<html lang="en">
<?php $type = $data['type'];
$img = $data['img'];

?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$data['title'] ?></title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content='<?=$data['desc'] ?>' name='description'/>
    <meta content='<?=$data['keywords'] ?>' name='keywords'/>
    <base href="<?=url('') ?>/" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/iofrm-theme3.css">
    <style>
        .medium-password {
            color: #b7d60a;
        }

        .weak-password {
            color: #ce1d14;

        }

        .strong-password {
            color: #12CC1A;
        }
    </style>
</head>
<body>
<div class="form-body">
    <div class="row">
        <div class="img-holder" style="background-image: url('<?php echo url('/assets/register/'.$img);?> '); background-repeat: no-repeat">
            <div class="bg"></div>
            <div class="info-holder">

            </div>
        </div>
        <div class="form-holder">
            <div class="form-content">

                <div class="form-items">
                    <div class="website-logo">
                        <a href="<?=url('')?>">
                            <div class="logo">
                                <img class="logo-size" src="<?=$data['cfg']->logo ?>" alt="">
                            </div>
                        </a>
                    </div>
                    <h3><?=$data['rtitle'];?> <?=translate(' Register')?></h3>
                    <p>Access to the most powerfull tool in the entire design and web industry.</p>
                    <?php
                    $urldata = '<script>alert("Register Successfully !"); window.location.href("'.url("/r/index").'");</srcipt>';
                    if (isset($data['error'])){
                        echo '<div class="alert alert-warning">' . translate($data['error']) . '</div>';
                    }
                    ?>
                    <form action="" method="post" class="form-horizontal single">
                        <fieldset >
                            <?=csrf_field(); ?>
                            <div class="form-group">
                                <label class="control-label"><?=($type=='institutional')?'Institutional':'';?><?=translate(' Name') ?> (*)</label>
                                <input name="name" type="text" value="<?=isset($_POST['name']) ? $_POST['name'] : '' ?>" class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?=translate('E-mail') ?> (*)</label>
                                <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?=translate('Phone') ?> (*)</label>
                                <input name="mobile" type="text" class="form-control" required />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?=translate('Password') ?> (*)</label>
                                <input name="password" type="password"  id="password" class="form-control" onkeyup="checkPasswordStrength();" required/>
                                <span id="password-strength-status"></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?=translate('Confirm Password') ?> (*)</label>
                                <input name="confirm_password" type="password" id="confirm_password" onkeyup="checkPasswordMatch();" class="form-control" required />
                                <span id="CheckPasswordMatch"></span>
                            </div>
                            <div class="form-group">
                                <!-- <label class="control-label"><?=translate('User Type') ?> (*)</label>
                                <select name="user_type" class="form-control" required>
                                    <option value="">Please Select User Type</option>
                                    <option value="customer">Customer</option>
                                    <option value="seller">Seller</option>
                                    <option value="professional">Professional</option>
                                </select> -->
                                <input name="user_type" type="hidden" value="<?=$type;?>">
                            </div>

                            <?php if($type=='institutional'){
                                ?>

                                <div class="form-group">
                                    <label class="control-label"><?=translate('Address') ?> (*)</label>
                                    <input name="address_line_1" type="text" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?=translate('GSTN') ?> (*)</label>
                                    <input name="gst" type="text" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?=translate('Person Name') ?> (*)</label>
                                    <input name="cont_person_name" type="text" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?=translate('Person Contact') ?> (*)</label>
                                    <input name="cont_person_phone" type="text" class="form-control" required />
                                </div>
                                <?php
                            } ?>
                            <div class="form-button">
                            <button name="register" type="submit" value="" class="btn btn-primary" ><?=translate('Register') ?></button>
                            </div>
                        </fieldset>
                    </form>
                    <!--div class="other-links">
                        <span>Or login with</span>
                            <a href="#"><i class="fab fa-facebook-f"></i></a><a href="#"><i class="fab fa-google"></i></a><a href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=$data['tp'] ?>/assets/jquery.min.js"></script>
<script src="<?=$data['tp'] ?>/assets/popper.min.js"></script>
<script src="<?=$data['tp'] ?>/assets/bootstrap.min.js"></script>
<script src="<?=$data['tp'] ?>/assets/main.js"></script>
</body>

</html>
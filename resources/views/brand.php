<?php echo $header?>
<section >
    <div class="container">
        <div class="row" id="at-inner-title-sec" <?=($bran->banner_image !== '' ? 'style="background-image: url(\'assets/products/'.$bran->banner_image.'\')"' : ''); ?>>
            <div class="col-md-6 col-sm-6 col-md-offset-3" style="padding: 30px 0; margin-top: 45px; vertical-align: middle;">
                <div class="inner-border">
                    <div class="at-inner-title-box text-center" style="vertical-align: middle; padding: 25px;margin:5px;border: none;background: rgba(255, 255, 255, 0.7);">
                        <h3><?=(isset($bran->name) ? translate($bran->name) : translate('Products'));?></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <!-- Inner page heading end -->

    <!-- Property start from here -->
    <section class="at-property-sec at-property-right-sidebar">
        <div class="container">
            <div class="row">
                <aside class="sidebar-shop col-lg-3 order-lg-first">
                    <div class="sidebar-wrapper">

                        <div class="widget">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-body-c" role="button" aria-expanded="true" aria-controls="widget-body-c">Categories</a>
                            </h3>

                            <div class="show collapse in" id="widget-body-c">
                                <div class="widget-body">
                                    <ul class="cat-list" style="height: 250px; overflow-y: auto;">
                                        <?php foreach($cats as $cat){
                                            echo '<li><a href="products/'.$cat->path.'">'.translate($cat->name).'</a></li>';
                                            $childs = DB::select("SELECT * FROM category WHERE parent = ".$cat->id." ORDER BY id DESC");
                                            foreach ($childs as $child){
                                                echo '<li><a href="products/'.$child->path.'">- '.$child->name.'</a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->

                        <div class="widget">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-body-p" role="button" aria-expanded="true" aria-controls="widget-body-p">Price</a>
                            </h3>

                            <div class="show collapse in" id="widget-body-p">
                                <div class="widget-body">
                                    <div class="form-price">
                                        <div class="clearfix"></div>
                                        <b class="pull-left price"><?=$price['min'] ?></b>
                                        <b class="pull-right price"><?=$price['max']; ?></b>
                                        <input name="min" id="min" type="hidden">
                                        <input name="max" id="max" type="hidden">
                                        <div id="price"></div>
                                    </div>
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->

                        <div class="widget">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-body-b" role="button" aria-expanded="true" aria-controls="widget-body-b">Brand</a>
                            </h3>

                            <div class="show collapse in" id="widget-body-b">
                                <div class="widget-body">
                                    <ul class="cat-list" style="height: 100px; overflow-y: auto;">
                                        <?php
                                        foreach($brands as $brand){
                                            echo '<li><a href="brand/'.$brand->path.'">'.translate($brand->name).'</a></li>';
                                        }
                                        ?>
                                    </ul>

                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->



                        <?php
                        if(!empty($filters)) { ?>
                            <?php foreach ($filters as $fil) {
                                $fil_option = DB::table('filter_options')->where('filter_id', $fil->id)->get();
                                if (!empty($fil_option)) {
                                    ?>
                                    <div class="widget">
                                        <h3 class="widget-title">
                                            <a data-toggle="collapse" href="#widget-body-<?= $fil->id; ?>" role="button"
                                               aria-expanded="true"
                                               aria-controls="widget-body-<?= $fil->id; ?>"><?php echo $fil->name; ?></a>
                                        </h3>
                                        <div class="show collapse in" id="widget-body-<?= $fil->id; ?>">
                                            <div class="widget-body">
                                                <?php if ($fil->name == 'Colour') { ?>
                                                    <ul class="config-swatch-list">
                                                        <?php foreach($fil_option as $fo){ ?>
                                                            <li>
                                                                <a href="#" style="background-color: <?php echo $fo->name; ?>;"> </a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                <?php } elseif($fil->name == 'Size') { ?>
                                                    <ul class="config-size-list">
                                                        <?php foreach($fil_option as $fo){ ?>
                                                            <li>
                                                                <a href="#" class="a-text"><?php echo $fo->name; ?></a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                <?php } else { ?>
                                                    <ul>
                                                        <?php foreach($fil_option as $fo){ ?>
                                                            <li>
                                                                <a href="#"><?php echo $fo->name; ?></a>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                <?php } ?>
                                            </div><!-- End .widget-body -->
                                        </div><!-- End .collapse -->
                                    </div><!-- End .widget -->
                                <?php }
                            }
                        }?>
                        <div class="widget widget-featured">
                            <h3 class="widget-title">Featured Proucts</h3>

                            <div class="widget-body">
                                <div class="owl-carousel widget-featured-products" id="featured-products">
                                    <div class="featured-col">
                                        <div class="pro product-sm">
                                            <figure class="product-image-container">
                                                <a href="product.html" class="product-image">
                                                    <img src="assets/images/product-10.jpg" alt="product">
                                                </a>
                                            </figure>
                                            <div class="product-details">
                                                <h2 class="product-title">
                                                    <a href="product.html">Papell Gown</a>
                                                </h2>
                                                <div class="ratings-container">
                                                    <div class="product-ratings">
                                                        <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                                                    </div><!-- End .product-ratings -->
                                                </div><!-- End .product-container -->
                                                <div class="price-b">
                                                    <span class="old-price">$50.00</span>
                                                </div><!-- End .price-box -->
                                            </div><!-- End .product-details -->
                                        </div><!-- End .product -->

                                        <div class="pro product-sm">
                                            <figure class="product-image-container">
                                                <a href="product.html" class="product-image">
                                                    <img src="assets/images/product-11.jpg" alt="product">
                                                </a>
                                            </figure>
                                            <div class="product-details">
                                                <h2 class="product-title">
                                                    <a href="product.html">Masrinna Ankle</a>
                                                </h2>
                                                <div class="ratings-container">
                                                    <div class="product-ratings">
                                                        <span class="ratings" style="width:20%"></span><!-- End .ratings -->
                                                    </div><!-- End .product-ratings -->
                                                </div><!-- End .product-container -->
                                                <div class="price-b">
                                                    <span class="old-price">$60.00</span>
                                                </div><!-- End .price-box -->
                                            </div><!-- End .product-details -->
                                        </div><!-- End .product -->

                                        <div class="pro product-sm">
                                            <figure class="product-image-container">
                                                <a href="product.html" class="product-image">
                                                    <img src="assets/images/product-12.jpg" alt="product">
                                                </a>
                                            </figure>
                                            <div class="product-details">
                                                <h2 class="product-title">
                                                    <a href="product.html">Crown Vintage</a>
                                                </h2>
                                                <div class="ratings-container">
                                                    <div class="product-ratings">
                                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->
                                                    </div><!-- End .product-ratings -->
                                                </div><!-- End .product-container -->
                                                <div class="price-b">
                                                    <span class="old-price">$50.00</span>
                                                </div><!-- End .price-box -->
                                            </div><!-- End .product-details -->
                                        </div><!-- End .product -->
                                    </div><!-- End .featured-col -->

                                    <div class="featured-col">
                                        <div class="pro product-sm">
                                            <figure class="product-image-container">
                                                <a href="product.html" class="product-image">
                                                    <img src="assets/images/product-4.jpg" alt="product">
                                                </a>
                                            </figure>
                                            <div class="product-details">
                                                <h2 class="product-title">
                                                    <a href="product.html">Training Shoes</a>
                                                </h2>
                                                <div class="ratings-container">
                                                    <div class="product-ratings">
                                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->
                                                    </div><!-- End .product-ratings -->
                                                </div><!-- End .product-container -->
                                                <div class="price-b">
                                                    <span class="old-price">$50.00</span>
                                                </div><!-- End .price-box -->
                                            </div><!-- End .product-details -->
                                        </div><!-- End .product -->

                                        <div class="pro product-sm">
                                            <figure class="product-image-container">
                                                <a href="product.html" class="product-image">
                                                    <img src="assets/images/product-5.jpg" alt="product">
                                                </a>
                                            </figure>
                                            <div class="product-details">
                                                <h2 class="product-title">
                                                    <a href="product.html">EyeGlasses</a>
                                                </h2>
                                                <div class="ratings-container">
                                                    <div class="product-ratings">
                                                        <span class="ratings" style="width:60%"></span><!-- End .ratings -->
                                                    </div><!-- End .product-ratings -->
                                                </div><!-- End .product-container -->
                                                <div class="price-b">
                                                    <span class="old-price">$29.00</span>
                                                </div><!-- End .price-box -->
                                            </div><!-- End .product-details -->
                                        </div><!-- End .product -->

                                        <div class="pro product-sm">
                                            <figure class="product-image-container">
                                                <a href="product.html" class="product-image">
                                                    <img src="assets/images/product-3.jpg" alt="product">
                                                </a>
                                            </figure>
                                            <div class="product-details">
                                                <h2 class="product-title">
                                                    <a href="product.html">Woman Highheels</a>
                                                </h2>
                                                <div class="ratings-container">
                                                    <div class="product-ratings">
                                                        <span class="ratings" style="width:20%"></span><!-- End .ratings -->
                                                    </div><!-- End .product-ratings -->
                                                </div><!-- End .product-container -->
                                                <div class="price-b">
                                                    <span class="old-price">$40.00</span>
                                                </div><!-- End .price-box -->
                                            </div><!-- End .product-details -->
                                        </div><!-- End .product -->
                                    </div><!-- End .featured-col -->
                                </div><!-- End .widget-featured-slider -->
                            </div><!-- End .widget-body -->
                        </div><!-- End .widget -->

                    </div><!-- End .sidebar-wrapper -->
                </aside>
                <div class="col-md-9">

                    <div class="row" style="padding: 30px 0;">
                        <?php
                        $product_id = '';
                        foreach($products as $product){
                            $productI = $product->id;
							echo '<div class="col-md-4 col-sm-4">
								<div class="at-property-item at-col-default-mar" id="'.$product->id.'">
									<div class="at-property-img">
									<a href="product/'.path($product->title,$product->id).'" data-title="<'.translate($product->title).'">
										<img src="'.url('/assets/products/'.image_order($product->images)).'" style="width: 100%; height: 150px;" alt="">
										</a>
									</div>
									<div class="at-property-location">
										<p>
											<a href="product/'.path($product->title,$product->id).'" data-title="'.translate($product->title).'">'.translate($product->title).'</a>
											<br><span class="price">'.c($product->price).'</span>
											<div class="rating">';
												$rates = getRatings($product->id);
												$tr = $rates['rating']; $i = 0; while($i<5){ $i++;
													echo '<i class="star'.(($i<=$rates["rating"]) ? "-selected" : "").'"></i>';
													 $tr--; }
												echo '('.$rates["total_ratings"].')
											</div>
										</p>
										<div class="cart-btn-custom" style="padding-top: 10px;">
											<button class="bg">Add to Cart</button>
											<div style="margin-top: -20px;">&nbsp;</div>
											';
										if(customer('id') !== ''){
                                        $user_id = customer('id');
                                        $product_id = $product->id;

                                        $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");

                                        if(!empty($img)){

                                            echo '<button style="display:none;" class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                        } else{
                                            echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                            <button style="display:none;" class="bg likebutton" id="likebutton"><i class="fa fa-heart"></i> Add to Project</button>';

                                        }} else {
                                        echo '<button style="display:none;" class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                    }
									echo '
										</div>
										</div>
								</div>
							</div>';
						}

			?>

                    </div>
					
					<?php if(count($suggested) > 0){ ?>
					<div class="row">
						<div class="box clearfix box-with-products">
                                               <!-- Carousel nav -->
						 <a class="next" href="#myCarousel0" id="myCarousel0_next"><span></span></a>
						 <a class="prev" href="#myCarousel0" id="myCarousel0_prev"><span></span></a>
							
						 <div class="box-heading">Suggested Products</div>
						 <div class="strip-line"></div>

						 <div class="box-content products" style="margin: 20px 0;">
							  <div class="box-product">
								   <div id="myCarousel0" class="owl-carousel">
										<?php 
										
										foreach($suggested as $suggest){ 
											
										?>
										<div class="item">
											 <div class="at-property-item at-col-default-mar" id="<?=$suggest->id;?>">
												<div class="at-property-img">
													<a href="product/<?=path($suggest->title,$suggest->id)?>" data-title="<?=translate($suggest->title)?>">
														<img src="<?=url('/assets/products/'.image_order($suggest->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
													</a>
													<div class=""></div>
												</div>
												<div class="at-property-location">
													<p>
														<a href="product/<?=path($suggest->title,$suggest->id)?>" data-title="<?=translate($suggest->title)?>"><?=translate($suggest->title)?></a>
														<br><span class="price"><?=c($suggest->price)?></span>
														<div class="rating">
															
															<?php
															$rates = getRatings($suggest->id);
															$tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
																<i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
																<?php $tr--; }?>
															(<?=$rates['total_ratings']?>)
														</div>
													</p>
													<div class="cart-btn-custom" style="padding-top: 10px;">
                                                        <button class="bg">Add to Cart</button>
                                                        <div style="margin-top: -20px;">&nbsp;</div>
                                                        <?php
                                                        if(customer('id') !== ''){
                                                        $user_id = customer('id');
                                                        $product_id = $suggest->id;

                                                        $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");

                                                        if(!empty($img)){

                                                        echo '<button style="display:none;" class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                        } else{
                                                        echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                                <button style="display:none;" class="bg likebutton1" id="likebutton" ><i class="fa fa-heart"></i> Add to Project</button>';

                                                        }} else {
                                                        echo '<button style="display:none;" class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                        }
                                                        ?>
													</div>
												</div>
											</div>
										</div>
										<?php } ?>
								   </div>
							  </div>
						 </div>
					</div>
					</div>
					<?php } ?>

                </div>
            </div>

        </div>
    </section>
<section>
    <div class="container">
        <?php if(count($top_brands) > 0){ ?>
        <div class="row" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16); padding: 30px; margin-top: -40px">
            <div class="row" id="brands">
                <div class="col-sm-12" style="margin-bottom: -10px;">
                    <div class="brand-panel">
                        <div class="brand-panel-title text-center">
                            <h5 class="">Top Brands</h5>
                        </div>
                        <div class="col-md-12 brand-panel-items" style="padding: 15px;">
                            <section class="bregular slider">
                                <?php
                                foreach($top_brands as $tb){
                                    ?>
                                    <div style="text-align: center; padding: 5px;">
                                        <div class="hover-effect" style="width: 100%; height: 100%;border: 1px solid #ddd;padding: 3px;">
                                            <div style="width: 100%; height: 100%;border: 1px solid #555;">
                                                <a href="<?=url('brand/'.$tb->path); ?>" >
                                                    <img src="assets/brand/<?php echo $tb->image; ?>" style="width: 100%; max-width:100%; height: 100px;">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if(!empty($link)){?>
            <div class="row">
                <div class="blockquote blockquote--style-1">
                    <div class="row inner-div">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <img src="<?=url('/assets/products/'.image_order($link->image))?>" style="width:50%;height:auto;"/>
                            </div>
                            <div class="col-md-7" style="padding-top: 25px;">
                                <h3><?=$link->content; ?></h3>
                            </div>
                            <div class="col-md-2" style="padding:15px 0 0 0">
                                <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                    GET STARTED
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
<?php if(count($faqs) > 0){ ?>
<section class="at-property-sec at-property-right-sidebar " id="faq">

    <div class="container" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16);padding: 30px;">
        <div class="box-heading" style="padding-left: 10px;">
            <div class="at-sec-title">
                <h2 style="font-size: 15px;">Frequently <span>Asked</span> Questions</h2>
                <div class="at-heading-under-line" style="margin: auto;">
                    <div class="at-heading-inside-line" ></div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-right: 10px; padding-left: 10px;">
            <div class="col-md-12">
                <input type="hidden" name="category" value="<?=$categ->id; ?>"/>
                <ul id="fload">
                    <?php
                    $postID = '';
                    foreach($faqs as $faq){
                        $postID = $faq->id;
                        ?>
                        <li>
                            <input type="checkbox" checked>
                            <i></i>
                            <h5>Q : <?php echo $faq->question; ?><br><small>

                                    by Admin on <?php $d = $faq->time; echo date('d M, Y', strtotime($d)); ?></small></h5>
                            <p>A : <?php echo $faq->answer; ?></p>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-lg-12" style="padding-top: 10px; text-align: center;">
                <div class="show_more_main" id="show_more_main<?php echo $postID; ?>">
                    <span id="<?php echo $postID; ?>" class="show_more" title="Load more posts" style="cursor: pointer;">Show more</span>
                    <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- Start Client -->
<?php } ?>

<section class="section " id="client" style="margin-top: -40px;">
    <div class="container bg-client" style="box-shadow: 0 3px 6px rgba(0,0,0,.16); padding: 10px;">
        <div class="row" style="display:none;">
            <div class="col-lg-12">
                <div class="box-heading" style="padding-left: 10px;">
                    <div class="at-sec-title">
                        <h2 style="font-size: 15px;">What <span>Clients</span> Says?</h2>
                        <div class="at-heading-under-line" style="margin: auto;">
                            <div class="at-heading-inside-line" ></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12">
                <div id="owl-demo" class="owl-carousel">
                    <?php
                    $cid[] = $categ->id;
                    //Kd codes
                    $c = array();
                    $cont = true;
                    $temp = $cid;
                    while($cont){
                        $c = array_pluck(DB::table('category')->whereIn('parent', $temp)->get(), 'id');
                        if(count($c)){
                            $cid = array_merge($cid, $c);
                            $temp = $c;
                        }else{
                            $cont = false;
                        }
                    }
                    $pro1 = DB::table('products')->whereIn('category', $cid)->get();


                    //Kd codes END
                    //dd($cid);
                    foreach ($pro1 as $pd){

                        $rpidx = DB::select("SELECT * FROM reviews WHERE (product = ".$pd->id." AND active = '1') AND (rating = '4' OR rating = '5') ORDER BY rating DESC");
                        foreach ($rpidx as $rpid_val){
                            $pid = $rpid_val->product;

                            $pro = DB::table('products')->where('id', '=', $pid)->first();
                        ?>
                    <div class="text-center testi_boxes mt-3">
                        <div class="bus_testi_icon text-custom">
                            <i class="fa fa-quote-left"></i>
                        </div>
                        <div class="mt-3">

                            <div class="mt-4">
                                <img src="http://localhost/sellerkit/assets/products/6c055265459572b2126c42ecabb6d98e.jpg" style="width: 125px; height: 125px; padding: 4px; line-height: 1.42857143; border: 3px solid #d7d3d3; border-radius: 4px; box-shadow: 0 0 20px #a5a5a5; ">
                            </div>
                            <div class="testi_icon_center text-custom">
                                <i class="fa fa-quote-left"></i>
                            </div>
                            <p class="text-custom mb-0"><?php echo $pro->title; ?>
                            <br>
                            <div class="review" style="padding-left: 47%;">
                                <div class="rating" style="text-align: center;">
                                    <?php $rr = $rpid_val->rating; $i = 0; while($i<5){ $i++;?>
                                        <i class="star<?=($i<=$rpid_val->rating) ? '-selected' : '';?>"></i>
                                        <?php $rr--; }?>
                                </div>
                            </div>
                            </p>

                            <p class="client_review text-muted font-italic text-center">
                                " <?php echo $rpid_val->review; ?>"

                            </p>
                            <?php $cust = DB::table('customers')->where('id', '=', $rpid_val->name)->first(); ?>
                            <div class="testi_img mt-4">
                                <img src="<?php if($cust->image !== ''){ echo url('assets/user_image/'.$cust->image); } else { echo url('assets/user_image/user.png'); }; ?>" alt="" class="img-fluid rounded-circle mx-auto d-block img-thumbnail">
                            </div>

                            <p class="client_name text-center mb-0 mt-3 font-weight-bold"><?=$cust->name; ?></p>
                        </div>
                    </div>
                    <?php
                    } }?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Client -->
<?php if(count($ladvices) > 0) {?>
<section class="at-property-sec at-property-right-sidebar " style="background-color: #f4f4f4;">

    <div class="container" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16);padding: 30px;">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box-heading" style="padding-left: 10px;">
                                <div class="at-sec-title">
                                    <h2 style="font-size: 15px;">Latest <span>Advices</span> </h2>
                                    <div class="at-heading-under-line" style="margin: auto;">
                                        <div class="at-heading-inside-line" ></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="border-top: 2px solid #f4f4f4; padding-right: 10px; padding-left: 10px;">&nbsp;</div>
					<div class="vc_controls scroll-up" data-scroll="vc_cont1"><i class="fa fa-chevron-up"></i></div>
					<div class="verticleCarouselContainer">
						<div class="adviceList" id="vc_cont1">
							<?php foreach ($ladvices as $ladvice){ ?>
							<article style="padding-top: 10px; padding-bottom: 10px;" class="row">
								<div class="col-lg-4">
									<div class="home-featured-image">
										<a href="blog/<?php echo path($ladvice->title,$ladvice->id); ?>">
											<img src="<?php echo url('/assets/blog/'.$ladvice->images); ?>" class="alignleft" style="">
										</a>
									</div>

								</div>
								<div class="col-lg-8" itemprop="text">
									<span class="entry-categories">
										<?php echo getadvicesCategoryName($ladvice->category); ?>
									</span>
									<h5 class="entry-title" itemprop="headline">
										<a class="entry-title-link" rel="bookmark"><?=$ladvice->title; ?></a>
									</h5>
									<p class="entry-meta"><time class="entry-time" itemprop="datePublished" datetime="<?=$ladvice->date; ?>"><?=$ladvice->date; ?></time></p>
									<?=$ladvice->short_des; ?>
									<div class="col-lg-12" style="width:100%;margin-top: 15px;color:#003481">
										<?php
										$total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$ladvice->id)[0]->count;
										$total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$ladvice->id)[0]->count;
										$like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$ladvice->id)[0]->count;
										$rating = 0;
										if ($total_ratings > 0){
											$rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$ladvice->id)[0]->sum;
											$rating = $rating_summ / $total_ratings;
										}
										$reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$ladvice->id." AND active = 1 ORDER BY time DESC");
										$faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$ladvice->id." AND status = 'Approved' ORDER BY id DESC");
										$rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$ladvice->id."'")[0];
										$total_rating =  $rating->total_rating;
										$total_user = $rating->total_user;
										if($total_rating==0){
											$avg_rating = 0;
										}
										else{
											$avg_rating = round($total_rating/$total_user,1);
										}
										?>
										<div class="col-xs-12 text-center">
											<p style="width: 25%; float: left; bottom: 10px;"> <i class="fa fa-eye"></i> <?=$ladvice->visits; ?> Visits</p>
											<p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
											<p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
											<p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
										</div>
									</div>
								</div>
								
								<div class="col-lg-12 list-full">

									<div class="post-footer-container">
										<div class="post-footer-line post-footer-line-3">
											<div class="post-sharing-icons">
												<a target="_blank" href="https://www.facebook.com/sharer.php" title="Share on Facebook!" rel="noopener">
													<i class="fa fa-post-footer fa-facebook"></i>
												</a>
												<a target="_blank" href="https://twitter.com/home/" title="Tweet this!" rel="noopener">
													<i class="fa fa-post-footer fa-twitter"></i>
												</a>
												<a target="_blank" href="https://plus.google.com/share" title="Post this on Google Plus!" rel="noopener">
													<i class="fa fa-post-footer fa-google-plus"></i>
												</a>
												<a href="mailto:" title="Send this article to a friend!">
													<i class="fa fa-post-footer fa-envelope"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</article>
							<?php } ?>
						</div>
					</div>
					<div class="vc_controls scroll-down" data-scroll="vc_cont1"><i class="fa fa-chevron-down"></i></div>
                </div>
                <div class="col-lg-4" style="border-left: 2px solid #f4f4f4;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box-heading" style="padding-left: 10px;">
                                <div class="at-sec-title">
                                    <h2 style="font-size: 15px;">Tranding <span>Advices</span> </h2>
                                    <div class="at-heading-under-line" style="margin: auto;">
                                        <div class="at-heading-inside-line" ></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="border-top: 2px solid #f4f4f4; padding-right: 10px; padding-left: 10px;">&nbsp;</div>
					<div class="vc_controls scroll-up" data-scroll="vc_cont2"><i class="fa fa-chevron-up"></i></div>
						<div class="verticleCarouselContainer">
							<div class="adviceList" id="vc_cont2">
								<?php foreach ($tadvices as $tadvice){ ?>
								<div class="home-featured-image vc_item">
									<a href="blog/<?php echo path($tadvice->title,$tadvice->id); ?>">
										<img src="<?php echo url('/assets/blog/'.$tadvice->images); ?>" class="alignleft" sizes="(max-width: 600px) 100vw, 600px">
									</a>
									<h5 class="entry-title" itemprop="headline" style="padding: 5px; border: 4px solid #f4f4f4; ">
										<a class="entry-title-link" rel="bookmark"><?=$tadvice->title; ?></a>
									</h5>
								</div>
								<?php } ?>
							</div>
						</div>
					<div class="vc_controls scroll-down" data-scroll="vc_cont2"><i class="fa fa-chevron-down"></i></div>	

				</div>

            </div>
        </div>
    </div>
</section>
<?php } ?>



    <button class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login">Get Started !</button>
    <div class="popup" id="popup-login" data-popup="popup-login">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-6 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                    <?=csrf_field() ?>
                    <fieldset>
                        <div class="form-group">
                            <label class="control-label"><?=translate('E-mail') ?></label>
                            <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?=translate('Password') ?></label>
                            <input name="password" type="password" class="form-control"  />
                        </div>
                        <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                    </fieldset>
                </form>
                <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
            </div>
            <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
        </div>
    </div>
<script>
		
		$(document).ready(function(){
			var owl0 = $(".box #myCarousel0");
		
			$("#myCarousel0_next").on("click", function() {
				event.preventDefault();
				owl.trigger('owl.next');
				return false;
			  })
			$("#myCarousel0_prev").on("click", function() {
				event.preventDefault();
				owl.trigger('owl.prev');
				return false;
			});
			  
			owl0.owlCarousel({
				 slideSpeed : 400,
				 itemsCustom : [
				   [0, 1],
				   [480, 2],
				   [700, 3],
				   [1255, 3]
				 ]
			});
			

		});
</script>
    <script>

        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });
        function getModel(){
            //alert('hello');
            $('#open-service-modal1').click();
        }

        function addProject(val) {
            var ele =$(this);
            var proid = $('#ghanta').data('id');
            var project_id = val;
            $.ajax({
                type: "POST",
                url: "add-product-project",
                data:'_token=<?=csrf_token();?>&product_id='+proid+'&project_id='+project_id,
                success: function(data){

                    $('#pclose').click();
                    ele.unbind();
                }
            });
        }

        $('.likebutton').click( function () {
            var ele =$(this);
            var proid = $(this).prevAll('input').val();
            $.ajax({
                type: "POST",
                url: "product-wishlist",
                data:'_token=<?=csrf_token();?>&product_id='+proid,
                success: function(data){
                    //alert('hello');
                    $('#open-project-modal1').click();
                    $('#projectName').html(data);
                    ele.html('<span class="tickgreen">✔</span> Added to Project');
                    ele.unbind();
                }
            });
        });

        $('.likebutton1').click( function () {
            var ele =$(this);
            var proid = $(this).prevAll('input').val();
            $.ajax({
                type: "POST",
                url: "product-wishlist",
                data:'_token=<?=csrf_token();?>&product_id='+proid,
                success: function(data){
                    //alert('hello');
                    $('#open-project-modal1').click();
                    $('#projectName').html(data);
                    ele.html('<span class="tickgreen">✔</span> Added to Project');
                    ele.unbind();
                }
            });
        });


        function getcreate(){
            event.preventDefault();
            $('#project-modal2').click();
        }
    </script>
<?php echo $footer?>
<button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <?=csrf_field() ?>
            <label class="control-label">Select Your Project</label>
            <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
            <div id="projectName">

            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>
<a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
<div class="popup" id="popup-project2" data-popup="popup-project2">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
    </div>
</div>

<?php echo $header?>

<div class="profile-root bg-color ng-scope">
    <div class="clearfix buffer-top text-center wrapper-1400 bg-color" style="padding-bottom: 20px;">
        <div class="clearfix wrapper-1140 two-bgs">
            <?php if(customer('header_image') != '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12" style="padding: 0;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-12" style="padding: 0;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12" style="padding: 0;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') != '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-6" style=" padding-left: 0px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image'); ?>" class="cover">
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs" style=" padding-left: 2px; padding-right: 0px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope">
            <img itemprop="image" class="profile-icon" src="assets/user_image/<?php if(customer('image') != ''){ echo Customer('image'); } else { echo 'user.png'; } ?>">
            <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                <a itemprop="url" class="url">
                    <?php echo Customer('name'); ?>
                </a>
                <p style="text-align: center;"><a href="#" onClick="getprofile()" class="btn">My Profile</a></p>
            </h1>
            <div class="sans-light font-15 line-height-md">
                <p style="text-align: justify; padding: 30px;">Vanessa&nbsp;has a Bachelor’s Degree in Architecture and a Minor in Interior Design from the University of Oklahoma. Her architecture education has given her a strong passion for incorporating clean lines, unexpected color palettes, and simple but meaningful details into the spaces she designs.</p>
            </div>
            <div class="sans-light font-15 line-height-md" style="padding-bottom: 0px; position: absolute; width: 100%; bottom: 0;">
                <ul class="u-menu" style="text-align: center;">
                    <li><a href="r/myorders"  class="btn">Products</a></li>
                    <li><a href="r/serviceorders"  class="btn btn-primary">Services</a></li>
                    <li><a href="r/planorders"  class="btn">Design</a></li>
                    <li><a href="r/useradvices"  class="btn">Advices</a></li>
                    <li><a href="r/my-projects"  class="btn">My Projects</a></li>
                </ul>
            </div>
        </div>

        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left">
                    <section class="at-property-sec" style="padding-top: 0px;">
                        <div class="container" style="margin-left: -15px; width: 1140px;">
                            <ul class="nav nav-pills" style="background-color: white;">
                                <li class="active"><a href="#service" data-toggle="tab" >Service Orders</a></li>
                            </ul>

                            <div class="tab-content">



                                <div id="service" class="tab-pane fade in active">
                                    <br>
                                    <?php foreach ($orders as $order){
                                        $service = DB::select("SELECT * FROM services WHERE id = '".$order->service_id."'")[0];
                                        //dd($service);

                                        echo '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                <a href="'. url('serviceinvoice/'.$order->id).'" data-title="'.translate($service->title).'">
                                                    <div class="at-property-item at-col-default-mar" id="'.$service->id.'">
                                                        <div class="at-property-img">
                                                            <img src="'.url('/assets/services/'.image_order($service->images)).'" style="width: 360px; height: 200px;" alt="">
                                                        </div>
                                                        <div class="at-property-location" style="min-height: 150px;">
                                                            <h4><a href="'. url('serviceinvoice/'.$order->id).'" data-title="'.translate($service->title).'">'.translate($service->title).'</a></h4>
                                                            <p>'.$service->description.'</p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>';
                                        ?>
                                    <?php } if (count($orders) == 0) {?>
                                        <center>
                                        <table>
                                            <tr >
                                                <td>
                                                    <i class="icon-basket"></i>
                                                    <?=translate('You have not made any previous orders!')?>

                                                </td>

                                            </tr>
                                        </table>
                                        </center>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <?php echo $footer?>

    <script>
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });
        function getprofile(){
            event.preventDefault();
            $('#personal-modal').click();
        }

        function getcreate(){
            event.preventDefault();
            $('#project-modal').click();
        }

        function showProject(val) {
            document.getElementById("myForm").submit();
        }
    </script>

    <button class="btn btn-primary hidden" id="open-pro-modal1" data-popup-open="popup-pro">Get Started !</button>
    <div class="popup" id="popup-pro" data-popup="popup-pro">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-lg-12 account" style="border: none; box-shadow: none;">
                <?=csrf_field() ?>
                <div class="grid" id="projectName">

                </div>
            </div>
            <a class="popup-close" id="pclose" data-popup-close="popup-pro" href="#">x</a>
        </div>
    </div>

    <a class="btn btn-primary hidden" id="project-modal" data-popup-open="popup-project" href="#">Get Started !</a>
    <div class="popup" id="popup-project" data-popup="popup-project">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                        <tbody>
                        <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                        <tr>
                            <td>Project Title</td>
                            <td>:</td>
                            <td><input type="text" name="title" required class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Project Image</td>
                            <td>:</td>
                            <td><input type="file" name="image" required class="form-control"></td>
                        </tr>
                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-project" href="#">x</a>
        </div>
    </div>

    <a class="btn btn-primary hidden" id="personal-modal" data-popup-open="popup-personal" href="#">Get Started !</a>
    <div class="popup" id="popup-personal" data-popup="popup-personal">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-12 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('account'.'?edit='.Customer('id')); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style=" margin: auto;">
                        <tbody>
                        <tr>
                            <td>Name <span class="red-star">*</span></td>
                            <td><input type="text" required name="name" class="form-control" value="<?php echo Customer('name'); ?>"></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>Mobile <span class="red-star">*</span></td>
                            <td><input type="text" name="mobile" required class="form-control" value="<?php echo Customer('mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td colspan="7">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Alternate Mobile</td>
                            <td><input type="text" name="alternate_mobile" class="form-control" value="<?php echo Customer('alternate_mobile'); ?>"></td>
                            <td>&nbsp;</td>
                            <td>Email <span class="red-star">*</span></td>
                            <td><input type="text" name="email" required class="form-control" value="<?php echo Customer('email'); ?>"></td>
                        </tr>
                        <tr>
                            <td colspan="7">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Address Line 1 <span class="red-star">*</span></td>
                            <td><input type="text" name="address_line_1" required class="form-control" value="<?php echo Customer('address_line_1'); ?>"></td>
                            <td>&nbsp;</td>
                            <td>Company</td>
                            <td><input type="text" name="company" class="form-control" value="<?php echo Customer('company'); ?>"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Address Line 2</td>
                            <td><input type="text" name="address_line_2" class="form-control" value="<?php echo Customer('address_line_2'); ?>"></td>
                            <td>&nbsp;</td>
                            <td>Country <span class="red-star">*</span></td>
                            <td>
                                <select name="country" required id="country_shipping" class="form-control" onChange="getStates(this.value, 'state_shipping')">
                                    <option value="<?php Customer('country'); ?>"><?php echo getCountryName(Customer('country')); ?></option>
                                    <option value="">Select country</option>
                                    <?php foreach($countries as $country){
                                        echo '<option value="'.$country->id.'">'.$country->name.'</option>';
                                    }?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>State <span class="red-star">*</span></td>
                            <td>
                                <select name="state" required class="form-control" id="state_shipping" onChange="getCities(this.value, 'city_shipping')">
                                    <option value="<?php Customer('state'); ?>"><?php echo getStateName(Customer('state')); ?></option>
                                    <option value="">Please select region, state or province</option>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>City <span class="red-star">*</span></td>
                            <td>
                                <select name="city" required class="form-control" id="city_shipping">
                                    <option value="<?php Customer('city'); ?>"><?php echo getCityName(Customer('city')); ?></option>
                                    <option value="">Please select city or locality</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Postcode <span class="red-star">*</span></td>
                            <td><input type="text" name="postcode" required class="form-control" value="<?php echo Customer('postcode'); ?>"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img id="output" src="assets/user_image/<?php echo Customer('image'); ?>"  style="border:0px solid #FFF;max-width:150px;max-height:150px;float: right; padding-right: 20px;" alt="Your Image Here"/>
                                <script>
                                    var loadFile = function(event) {
                                        var output = document.getElementById('output');
                                        output.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </td>
                            <td>Profile Image</td>
                            <td><input type="file" name="image" onchange="loadFile(event)" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img id="output1" src="assets/user_image/<?php echo Customer('header_image'); ?>"  style="border:0px solid #FFF; max-width:150px;max-height:150px;float: right; padding-right: 20px;" alt="Your Image Here"/>
                                <script>
                                    var loadFile1 = function(event) {
                                        var output1 = document.getElementById('output1');
                                        output1.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </td>
                            <td>Banner Image 1</td>
                            <td><input type="file" name="header_image" onchange="loadFile1(event)" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img id="output2" src="assets/user_image/<?php echo Customer('header_image_1'); ?>"  style="border:0px solid #FFF;max-width:150px;max-height:150px;float: right; padding-right: 20px;" alt="Your Image Here"/>
                                <script>
                                    var loadFile2 = function(event) {
                                        var output2 = document.getElementById('output2');
                                        output2.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </td>
                            <td>Banner Image 2</td>
                            <td><input type="file" name="header_image_1" onchange="loadFile2(event)" class="form-control" ></td>
                        </tr>

                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="5"><input type="submit" name="edit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-personal" href="#">x</a>
        </div>
    </div>
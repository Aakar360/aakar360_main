<?php echo $header?>
    <div class="profile-root bg-white ng-scope">
        <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
            <div class="clearfix wrapper-1140 two-bgs">
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/0e94f355317086ae791f78e1e851d4fb.jpg" class="cover">
                    </div>
                </div>
            </div>
            <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
                <img itemprop="image" class="profile-icon" src="assets/invoice.png">
                <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                    <a itemprop="url" class="url">
                        <?=translate('Invoice')?>
                    </a>
                </h1>
            </div>
        </div>
        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left" style="padding-top: 50px;">
                    <div class="sdb-tabl-com sdb-pro-table">
                        <div class="udb-sec udb-prof">
                            <div class="col-md-8">
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-user"></i> Customer details
                                    </div>
                                    <div class="item"><h6><b>Order ID</b> : #<?=$order->id?></h6></div>
                                    <div class="item"><h6><b>Order time</b> : <?=$order->created; ?></h6></div>
                                    <?php

                                    echo '<div class="item order"><h6><b>Shipping Address</b> : </h6>
                                        <p>';
                                    echo '<b>Name : </b> '.customer('name').'<br>';
                                    echo '<b>Company : </b> '.customer('company').'<br>';
                                    echo '<b>Address Line 1 : </b> '.customer('address_line_1').'<br>';
                                    echo '<b>Address Line 2 : </b> '.customer('address_line_2').'<br>';
                                    echo '<b>Country : </b> '.getCountryName(customer('country')).'<br>';
                                    echo '<b>State : </b> '.getStateName(customer('state')).'<br>';
                                    echo '<b>City : </b> '.getCityName(customer('city')).'<br>';
                                    echo '<b>Pincode : </b> '.customer('postcode').'<br>';
                                    echo '<b>Contact No. : </b> '.customer('mobile').'<br>';
                                    echo '<b>Alternate Contact No. : </b> '.customer('alternate_contact').'<br>';
                                    echo '</p>
                                        </div>';
                                    echo   '<br/>';
                                    ?>
                                </div>
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-basket"></i> Service
                                    </div>
                                    <?php $user_id = session('user_id');
                                    $service_id = $order->service_id;
                                    echo '<div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                              <tr>
                                                <th>Service Name</th>
                                                <th class="text-right">Price</th>
                                                <th class="text-right">Tax(%)</th>
                                                <th class="text-right">Tax Amount</th>
                                                <th class="text-right">Amount</th>
                                              </tr>
                                        </thead>';
                                    $serv = DB::select("SELECT * FROM services WHERE id = '".$service_id."'")[0];

                                    $price = $serv->price;
                                    $tax = 0;
                                    $taxamt= 0;
                                    if($serv->tax !== null) {
                                        $rate = $serv->price;
                                        $tax = $serv->tax;
                                        $taxa = 1 + ($tax / 100);
                                        $price = $price/$taxa;
                                        $tprice = $serv->price;
                                        $taxamt = $rate - $price;
                                    } else { $price = $serv->price; $tprice = $serv->price; }
                                    echo '<tr>
                                                <td>'.$serv->title.'</p></td>
                                                <td class="text-right">'.c(number_format($price, 2, '.', '')).'</td>
                                                <td class="text-right">'.$tax.'</td>
                                                <td class="text-right">'.c(number_format($taxamt, 2, '.', '')).'</td>
                                                <td class="text-right">'.$tprice.'</td>
                                            </tr>
                                        </table></div>';
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-credit-card"></i> Payment details
                                    </div>
                                    <?php $payment_data = json_decode($order->payment_method,true);
                                    $payment_data = json_decode($order->payment_method,true);
                                    echo '<div class="alert alert-warning" style="margin-bottom: 0;">Order unpaid</div>';
                                    if ($payment_data['method'] == 'paypal') {
                                        echo '<div class="item order"><h6><b>Payment method : </b>PayPal</h6></div>
                                        <div class="item order"><h6><b>Payment status : </b>'.$payment_data['payment_status'].'</h6></div>
                                        <div class="item order"><h6><b>Transcation ID : </b>'.$payment_data['txn_id'].'</h6></div>
                                        <div class="item order"><h6><b>Receiver e-mail : </b>'.$payment_data['receiver_email'].'</h6></div>
                                        <div class="item order"><h6><b>Payer e-mail : </b>'.$payment_data['payer_email'].'</h6></div>
                                        <div class="item order"><h6><b>Payment amount : </b>'.$payment_data['payment_amount'].'</h6></div>
                                        <div class="item order"><h6><b>Payment currency : </b>'.$payment_data['payment_currency'].'</h6></div>';
                                    } elseif ($payment_data['method'] == 'stripe') {
                                        echo '<div class="item order"><h6><b>Payment method : </b>Stripe</h6></div>
                                        <div class="item order"><h6><b>Charge ID : </b>'.$payment_data['charge_id'].'</h6></div>
                                        <div class="item order"><h6><b>Balance transaction : </b>'.$payment_data['balance_transaction'].'</h6></div>
                                        <div class="item order"><h6><b>Card ID : </b>'.$payment_data['card'].'</h6></div>
                                        <div class="item order"><h6><b>Card brand : </b>'.$payment_data['card_brand'].'</h6></div>
                                        <div class="item order"><h6><b>Last 4 : </b>'.$payment_data['last4'].'</h6></div>
                                        <div class="item order"><h6><b>Expiry month : </b>'.$payment_data['exp_month'].'</h6></div>
                                        <div class="item order"><h6><b>Expiry year : </b>'.$payment_data['exp_year'].'</h6></div>
                                        <div class="item order"><h6><b>Fingerprint : </b>'.$payment_data['fingerprint'].'</h6></div>';
                                    } elseif ($payment_data['method'] == 'cash') {
                                        echo '<div class="item order"><h6><b>Payment method : </b>Cash on delivery</h6></div>';
                                    } elseif ($payment_data['method'] == 'bank') {
                                        echo '<div class="item order"><h6><b>Payment method : </b>Bank Transfer</h6></div>';
                                    }?>
                                </div>
                                <div class="list">
                                    <div class="title">
                                        <i class="icon-badge"></i>Order status
                                    </div>
                                    <div class="item text-center"><br/><h6><?=$order->status;?></h6><br/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo $footer?>
<?php echo $header?>

<div class="profile-root bg-white ng-scope">
    <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
        <div class="clearfix wrapper-1140 two-bgs">
            <?php if(customer('header_image') != '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') != '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-6" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image'); ?>" class="cover">
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs" style=" padding-left: 2px; padding-right: 2px;" >
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
            <img itemprop="image" class="profile-icon" src="assets/user_image/<?php if(customer('image') != ''){ echo Customer('image'); } else { echo 'user.png'; } ?>">
            <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                <a itemprop="url" class="url">
                    <?php echo Customer('name'); ?>
                </a>
                <p style="text-align: center;"><a href="seller_account" class="btn">My Profile</a></p>
            </h1>
            <div class="sans-light font-15 line-height-md hidden-xs">
                <p style="text-align: justify; padding: 30px;"><?php echo customer('business_description'); ?></p>
            </div>

            <div class="sans-light font-15 line-height-md hidden-xs" style="padding-bottom: 0px; position: absolute; width: 100%; bottom: 0;">
                <p style="text-align: center;">
                    <a href="seller_product" class="btn col-lg-3">Products</a>
                    <a href="seller_services" class="btn col-lg-3">Services</a>
                    <a href="seller_design" class="btn col-lg-3">Design</a>
                    <a href="seller_advices" class="btn col-lg-3">Advices</a> </p>
            </div>

        </div>

        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left">
                    <section class="at-property-sec" style="padding-top: 0px;">
                        <div class="container" style="margin-left: -15px; width: 1140px;">
                            <div class="tab-content">
                                <div id="account" class="tab-pane fade in active" style="padding: 15px;">
                                    <div class="box">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="box-part text-center" style="min-height: 330px;">
                                                    <i class="fa fa-user fa-3x" aria-hidden="true"></i>
                                                    <div class="title">
                                                        <h4>Personal Details</h4>
                                                    </div>
                                                    <div class="text">
                                                        <span>You need to provide your Personal information</span>
                                                    </div>
                                                    <?php if(customer('mobile') == '' && customer('alternate_mobile') == ''){ ?>
                                                        <a href="#" onClick="getPersonal()">Add Details</a>
                                                    <?php } else{ ?>
                                                        <a href="#" onClick="getPersonal()">View Details</a>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="box-part text-center" style="min-height: 330px;">
                                                    <i class="fa fa-book fa-3x" aria-hidden="true"></i>
                                                    <div class="title">
                                                        <h4>Business Details</h4>
                                                    </div>
                                                    <div class="text">
                                                        <span>You need to provide your GSTIN, TAN and other business information</span>
                                                    </div>
                                                    <?php if(customer('company') == ''
                                                        && customer('cont_person_name') == ''
                                                        && customer('cont_person_email') == ''
                                                        && customer('cont_person_phone') == ''
                                                        && customer('cont_person_designation') == ''
                                                        && customer('gst') == ''
                                                        && customer('pan') == ''
                                                        && customer('address_line_1') == ''
                                                        && customer('address_line_2') == ''
                                                        && customer('country') == ''
                                                        && customer('state') == ''
                                                        && customer('city') == ''
                                                        && customer('postcode') == ''
                                                    ){ ?>
                                                        <a href="#" onClick="getbusiness()">Add Details</a>
                                                    <?php } else {?>
                                                        <a href="#" onClick="getbusiness()">View Details</a>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="box-part text-center" style="min-height: 330px;">
                                                    <i class="fa fa-university fa-3x" aria-hidden="true"></i>
                                                    <div class="title">
                                                        <h4>Bank Details</h4>
                                                    </div>
                                                    <div class="text">
                                                        <span>We need your bank account details to verify your account</span>
                                                    </div>
                                                    <?php if(customer('holder_name') == ''
                                                        && customer('ac_no') == ''
                                                        && customer('bank_name') == ''
                                                        && customer('branch_name') == ''
                                                        && customer('ifsc') == ''
                                                    ){ ?>
                                                        <a href="#" onClick="getBank()">Add Details</a>
                                                    <?php } else {?>
                                                        <a href="#" onClick="getBank()">View Details</a>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <div class="box-part text-center" style="min-height: 330px;">
                                                    <i class="fa fa-shopping-bag fa-3x" aria-hidden="true"></i>
                                                    <div class="title">
                                                        <h4>Store Details</h4>
                                                    </div>
                                                    <div class="text">
                                                        <span>We need your Store details to show in your profile</span>
                                                    </div>
                                                    <?php if(customer('display_name') == ''
                                                        && customer('business_description') == ''
                                                    ){ ?>
                                                        <a href="#" onClick="getStore()">Add Details</a>
                                                    <?php } else {?>
                                                        <a href="#" onClick="getStore()">View Details</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <?php echo $footer?>

    <script>
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });
        function getPersonal(){
            event.preventDefault();
            $('#personal-modal').click();
        }
        function getbusiness(){
            event.preventDefault();
            $('#Business-modal').click();
        }
        function getBank(){
            event.preventDefault();
            $('#Bank-modal').click();
        }
        function getStore(){
            event.preventDefault();
            $('#Store-modal').click();
        }

    </script>


    <a class="btn btn-primary hidden" id="personal-modal" data-popup-open="popup-personal" href="#">Get Started !</a>
    <div class="popup" id="popup-personal" data-popup="popup-personal">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('seller_account'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <?=csrf_field() ?>
                    <table class="responsive-table bordered" style="margin: auto;">
                        <input type="hidden" name="id" value="<?php echo customer('id'); ?>">
                        <tbody>
                        <tr>
                            <td>Name <span class="red-star">*</span></td>
                            <td><input type="text" name="name" required class="form-control" value="<?php echo Customer('name'); ?>"></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>Mobile <span class="red-star">*</span></td>
                            <td><input type="text" name="mobile" required class="form-control" value="<?php echo Customer('mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Alternate Mobile</td>
                            <td><input type="text" name="alternate_mobile" class="form-control" value="<?php echo Customer('alternate_mobile'); ?>"></td>
                            <td>&nbsp;</td>
                            <td>Eamil <span class="red-star">*</span></td>
                            <td><input type="text" name="email" required class="form-control" value="<?php echo Customer('email'); ?>"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img id="output" src="assets/user_image/<?php echo Customer('image'); ?>"  style="border:0px solid #FFF;max-width:150px;max-height:150px;float: right; padding-right: 20px;" alt="Your Image Here"/>
                                <script>
                                    var loadFile = function(event) {
                                        var output = document.getElementById('output');
                                        output.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </td>
                            <td>Profile Image</td>
                            <td><input type="file" name="image" onchange="loadFile(event)" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img id="output1" src="assets/user_image/<?php echo Customer('header_image'); ?>"  style="border:0px solid #FFF; max-width:150px;max-height:150px;float: right; padding-right: 20px;" alt="Your Image Here"/>
                                <script>
                                    var loadFile1 = function(event) {
                                        var output1 = document.getElementById('output1');
                                        output1.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </td>
                            <td>Banner Image 1</td>
                            <td><input type="file" name="header_image" onchange="loadFile1(event)" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img id="output2" src="assets/user_image/<?php echo Customer('header_image_1'); ?>"  style="border:0px solid #FFF;max-width:150px;max-height:150px;float: right; padding-right: 20px;" alt="Your Image Here"/>
                                <script>
                                    var loadFile2 = function(event) {
                                        var output2 = document.getElementById('output2');
                                        output2.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </td>
                            <td>Banner Image 2</td>
                            <td><input type="file" name="header_image_1" onchange="loadFile2(event)" class="form-control" ></td>
                        </tr>

                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-personal" href="#">x</a>
        </div>
    </div>

    <a class="btn btn-primary hidden" id="Business-modal" data-popup-open="popup-business" href="#">Get Started !</a>
    <div class="popup" id="popup-business" data-popup="popup-business">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-12 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('seller_business'); ?>" method="post" class="form-horizontal single">
                    <table class="responsive-table bordered" style=" margin: auto;">
                        <tbody>
                            <?=csrf_field() ?>
                            <input type="hidden" name="id" value="<?php echo customer('id'); ?>">
                            <tr>
                                <td>Company Name <span class="red-star">*</span></td>
                                <td><input type="text" name="company" required class="form-control" value="<?php echo Customer('company'); ?>"></td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>Category Name <span class="red-star">*</span></td>
                                <td>
                                    <select name="category[]" multiple required class="form-control select2" style="width: 300px;">
                                        <?php foreach($categories as $category){
                                            $selected = '';
                                            $cat = Customer('category');
                                            $cato = explode(',', $cat);
                                            foreach($cato as $c){
                                                if($category->id == $c){
                                                    $selected = 'selected';
                                                }
                                            }
                                            ?>

                                            <option value="<?php echo $category->id; ?>" <?=$selected; ?>><?php echo $category->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Contact Person Name <span class="red-star">*</span></td>
                                <td><input type="text" name="cont_person_name" required class="form-control" value="<?php echo Customer('company'); ?>"></td>
                                <td>&nbsp;</td>
                                <td>Contact Person Email <span class="red-star">*</span></td>
                                <td><input type="text" name="cont_person_email" required class="form-control" value="<?php echo Customer('cont_person_email'); ?>"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Contact Person Phone <span class="red-star">*</span></td>
                                <td><input type="text" name="cont_person_phone" required class="form-control" value="<?php echo Customer('cont_person_phone'); ?>"></td>
                                <td>&nbsp;</td>
                                <td>Contact Person Designation <span class="red-star">*</span></td>
                                <td><input type="text" name="cont_person_designation" required class="form-control" value="<?php echo Customer('cont_person_designation'); ?>"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td>GST No. <span class="red-star">*</span></td>
                                <td><input type="text" name="gst" required class="form-control" value="<?php echo Customer('gst'); ?>"></td>
                                <td>&nbsp;</td>
                                <td>PAN No. <span class="red-star">*</span></td>
                                <td><input type="text" name="pan" required class="form-control" value="<?php echo Customer('pan'); ?>"></td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td>Address Line 1 <span class="red-star">*</span></td>
                                <td><input type="text" name="address_line_1" required class="form-control" value="<?php echo Customer('address_line_1'); ?>"></td>
                                <td>&nbsp;</td>
                                <td>Address Line 2</td>
                                <td><input type="text" name="address_line_2" class="form-control" value="<?php echo Customer('address_line_2'); ?>"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Country <span class="red-star">*</span></td>
                                <td>
                                    <select name="country" required id="country_shipping" class="form-control" onChange="getStates(this.value, 'state_shipping')">
                                        <?php
                                        foreach($countries as $country){
                                            $selected = '';
                                            $cat = Customer('country');
                                            if($cat == $country->id){
                                                $selected = 'selected';
                                            }
                                            echo '<option value="'.$country->id.'" '.$selected.'>'.$country->name.'</option>';
                                        }?>
                                    </select>
                                </td>
                                <td>&nbsp;</td>
                                <td>State <span class="red-star">*</span></td>
                                <td>
                                    <select name="state" required class="form-control" id="state_shipping" onChange="getCities(this.value, 'city_shipping')">
                                        <option value="<?php echo Customer('state'); ?>"><?php echo getStateName(Customer('state')); ?></option>
                                        <option value="">Please select region, state or province</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>City <span class="red-star">*</span></td>
                                <td>
                                    <select name="city" required class="form-control" id="city_shipping">
                                        <option value="<?php echo Customer('city'); ?>"><?php echo getCityName(Customer('city')); ?></option>
                                        <option value="">Please select city or locality</option>
                                    </select>
                                </td>
                                <td>&nbsp;</td>
                                <td>Postcode <span class="red-star">*</span></td>
                                <td><input type="text" name="postcode" class="form-control" required value="<?php echo Customer('postcode'); ?>"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="5"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                            </tr>
                        </tbody>
                    </table>
                </form>

            </div>
            <a class="popup-close" data-popup-close="popup-business" href="#">x</a>
        </div>
    </div>

    <a class="btn btn-primary hidden" id="Bank-modal" data-popup-open="popup-bank" href="#">Get Started !</a>
    <div class="popup" id="popup-bank" data-popup="popup-bank">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-6 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('seller_bank'); ?>" method="post" class="form-horizontal single">
                    <?=csrf_field() ?>
                    <input type="hidden" name="id" value="<?php echo customer('id'); ?>">
                    <fieldset>
                        <div class="form-group">
                            <label class="control-label">Account Holder Name <span class="red-star">*</span></label>
                            <input name="holder_name" type="text" required value="<?=isset($_POST['holder_name']) ? $_POST['holder_name'] : '' ?>" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Account No. <span class="red-star">*</span></label>
                            <input name="ac_no" type="text" class="form-control" required  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Bank Name <span class="red-star">*</span></label>
                            <input name="bank_name" type="text" class="form-control" required  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Branch Name <span class="red-star">*</span></label>
                            <input name="branch_name" type="text" required class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">IFSC <span class="red-star">*</span></label>
                            <input name="ifsc" type="text" required class="form-control"  />
                        </div>
                        <div>
                            <input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg">
                        </div>
                    </fieldset>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-bank" href="#">x</a>
        </div>
    </div>




    <a class="btn btn-primary hidden" id="Store-modal" data-popup-open="popup-store" href="#">Get Started !</a>
    <div class="popup" id="popup-store" data-popup="popup-store">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-6 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('seller_store'); ?>" method="post" class="form-horizontal single">
                    <?=csrf_field() ?>
                    <input type="hidden" name="id" value="<?php echo customer('id'); ?>">
                    <fieldset>
                        <div class="form-group">
                            <label class="control-label">Display Name <span class="red-star">*</span></label>
                            <input name="display_name" type="text" required value="<?=isset($_POST['display_name']) ? $_POST['display_name'] : '' ?>" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Business Description <span class="red-star">*</span></label>
                            <textarea name="business_description" class="form-control" required  ></textarea>
                        </div>

                        <div>
                            <input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg">
                        </div>
                    </fieldset>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-store" href="#">x</a>
        </div>
    </div>
    <script>$('.select2').select2();</script>

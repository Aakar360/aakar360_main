<?php echo $header?>
    <section id="at-inner-title-sec" class="pdbtm-165">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12" style="padding-top: 145px;">
					<div class="inner-border">
						<div class="at-inner-title-box text-center" style="padding: 40px 65px;margin:5px;border: none;background: rgba(255, 255, 255, 0.7);">
							<h1 style="text-transform: none;">Get a designer space you'll love</h1>
							<p style="text-transform: none;font-size: 20px;">Let our accomplished team of experts submit concepts for your space <b>— it's easy!</b></p>
						</div>
					</div>
                </div>
            </div>
        </div>
		
    </section>
    <!-- Inner page heading end -->

    <!-- Property start from here -->
    <section class="at-property-sec at-property-right-sidebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <?php
						$margin = 0;
						if(count($services_categories) < 4){
								$margin = number_format(((4-count($services_categories))*25)/2, 2, '.', '');
								echo '<div style="width:'.$margin.'%;margin: 0;float: left;">&nbsp;</div>';
							}
                        foreach($services_categories as $service){
							
                            echo '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a href="sub_service/'.path($service->name,$service->id).'" data-title="'.translate($service->name).'"><div class="at-property-item at-col-default-mar" id="'.$service->id.'">
                                <div class="at-property-img">
                                    <img src="'.url('/assets/services/'.image_order($service->image)).'" style="width: 360px; height: 200px;" alt="">
                                </div>
                                <div class="at-property-location">
                                    <h4><a href="sub_service/'.path($service->name,$service->id).'" data-title="'.translate($service->name).'">'.translate($service->name).'</a></h4>
                                    <p>'.$service->description.'</p>
                                </div>
                            </div></a>
                        </div>';
                        }
                        ?>
                   
                </div>
            </div>

        </div>
    </section>
    <?php if($how){ ?>
	<section class="how-it-works-sec padding">
		<div class="row">
			<div class="col-md-12">
				<div class="at-sec-title">
					<h2 >How It <span>Works</span></h2>
					<div class="at-heading-under-line">
						<div class="at-heading-inside-line"></div>
					</div>
					<p class="sub-title">Specedoor is the best way to design your home. We work within any budget, big or small. You can start from scratch or work with a designer using your existing furniture pieces.</p>
				</div>
			</div>
		</div>
		<div class="tabs tabs-style-underline">
			<nav>
				<ul>
                    <?php foreach($how as $h){ ?>
					    <li><a href="#section-underline-<?=$h->id; ?>" class="<?=$h->tab_icon; ?>"><span><?=$h->tab_title; ?></span></a></li>
                    <?php } ?>
				</ul>
			</nav>
			<div class="content-wrap">
				<?php foreach($how as $h){ ?>
                <section id="section-underline-<?=$h->id; ?>">
					<div class="row">
						<div class="col-md-4 details">
							<h3><?=$h->content_title; ?></h3>
							<p><?=$h->content; ?></p>
						</div>
						<div class="col-md-8"><img src="<?=url('/assets/how_it_works/'.$h->image);?>" alt="<?=$h->content_title; ?>" style="margin-right:-27px;"/></div>
					</div>
				</section>
                <?php } ?>

			</div><!-- /content -->
		</div><!-- /tabs -->
	</section>
<?php } ?>
<?php if(!empty($icat)){ ?>
	<section class="padding">
		<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="at-sec-title">
					<h2 style="text-transform: none;">Our designers create impeccable spaces every single day</h2>
					<div class="at-heading-under-line">
						<div class="at-heading-inside-line"></div>
					</div>
					<p class="design-sec">
                        <?php
                        $i = 0;
                        $default_cat = 0;
                        $incat = '';
                        $dcat = '';
						$ncat = null;
                        foreach($icat as $ic){
							if($i == 0){
								$dcat = DB::select("SELECT * FROM design_category WHERE id = '".$ic->gallery_category_id."'")[0];
								$incat = $ic->gallery_category_id;
								$ncat = DB::select("SELECT * FROM services_category_image WHERE gallery_category_id = '".$incat."'")[0];
								$default_cat = $ic->gallery_category_id;
								?>

								<a href="javascript:void(0);" class="designs-link first active" data-id="<?=$ic->gallery_category_id; ?>" data-link="<?=$ic->service_category_id; ?>"><?php echo $dcat->name; ?></a>
						<?php }  else {
								$ncat = DB::select("SELECT * FROM services_category_image WHERE gallery_category_id = '".$incat."'")[0];
								$dcat = DB::select("SELECT * FROM design_category WHERE id = '".$ic->gallery_category_id."'")[0];?>
								<a href="javascript:void(0);" class="designs-link" data-id="<?=$ic->gallery_category_id; ?>" data-link="<?=$ic->service_category_id; ?>"><?php echo $dcat->name; ?></a>
						<?php }
                        $i++;
                        } ?>
					</p>
				</div>
			</div>
		</div>
		<?php if(!empty($ncat)){ ?>
            <div class="row imagearea" id="imgarea">
				<div class="col-xs-12 col-sm-12 col-md-5">
                    <?php $image_1 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_1."'")[0]; ?>
					<div class="col-md-12 col-xs-12 col-sm-12 sample-1">
                        <a id="im1" href="photo-gallery/<?php echo $image_1->slug; ?>">
                            <div class="imageframe-half">
                                <img class="imageframe-img" src="assets/images/gallery/<?php echo $image_1->image; ?>"/>
                                <div class="image-overlay"></div>
                                <div class="image-loading"></div>
                            </div>
                        </a>
					</div>
                    <?php $image_2 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_2."'")[0]; ?>
					<div class="col-xs-12 col-sm-12 col-md-6 sample-2">
                        <a id="im2" href="photo-gallery/<?php echo $image_2->slug; ?>">
                            <div class="imageframe-half">
                                <img class="imageframe-img" src="assets/images/gallery/<?php echo $image_2->image; ?>"/>
                                <div class="image-overlay"></div>
                                <div class="image-loading"></div>
                            </div>
                        </a>
					</div>
                    <?php $image_3 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_3."'")[0]; ?>
					<div class="col-xs-12 col-sm-12 col-md-6 sample-3">
                        <a id="im3" href="photo-gallery/<?php echo $image_3->slug; ?>">
                            <div class="imageframe-half">
                                <img class="imageframe-img" src="assets/images/gallery/<?php echo $image_3->image; ?>"/>
                                <div class="image-overlay"></div>
                                <div class="image-loading"></div>
                            </div>
                        </a>
					</div>
				</div>
                <?php $image_4 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_4."'")[0]; ?>
				<div class="col-xs-12 col-sm-12 col-md-5 sample-4">
                    <a id="im4" href="photo-gallery/<?php echo $image_4->slug; ?>">
                        <div class="imageframe-full">
                            <img class="imageframe-img" src="assets/images/gallery/<?php echo $image_4->image; ?>"/>
                            <div class="image-overlay"></div>
                            <div class="image-loading"></div>
                        </div>
                    </a>
				</div>
                <?php $image_5 = DB::select("SELECT * FROM photo_gallery WHERE id = '".$ncat->image_5."'")[0]; ?>
				<div class="col-xs-12 col-sm-12 col-md-2 sample-5">
                    <a id="im5" href="photo-gallery/<?php echo $image_5->slug; ?>">
                        <div class="imageframe-full">
                            <img class="imageframe-img" src="assets/images/gallery/<?php echo $image_5->image; ?>"/>
                            <div class="image-overlay"></div>
                            <div class="image-loading"></div>
                        </div>
                    </a>
				</div>
			</div>
		<?php } ?>
		</div>
	</section>
<?php } ?>
<?php if(!empty($link)){?>
    <div class="container">
        <div class="blockquote blockquote--style-1">
            <div class="row inner-div">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <img src="<?=url('/assets/products/'.image_order($link->image))?>" style="width:50%;height:auto;"/>
                    </div>
                    <div class="col-md-7" style="padding-top: 25px;">
                        <h3><?=$link->content; ?></h3>
                    </div>
                    <div class="col-md-2" style="padding:15px 0 0 0">
                        <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                            GET STARTED
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
			
			


<?php echo $footer?>
<script>
	(function() {

		[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
			new CBPFWTabs( el );
		});

	})();
	var current = "<?=isset($default_cat) ? $default_cat : 0 ; ?>";
	var path = 'assets/images/gallery/';
	var hr = 'photo-gallery/';
	$('.designs-link').on('click', function(){
		$('.designs-link').removeClass('active');
		$(this).addClass('active');
		var getDataOf = $(this).attr('data-link');
		var gallery_cat = $(this).attr('data-id');
		if(current != gallery_cat){
			current = gallery_cat;
			$('.imagearea .image-overlay').show();
			$('.imagearea .image-loading').show();
            $.ajax({
                type: "POST",
                url: "getCatImage",
                data:'_token=<?=csrf_token();?>&service_category_id='+getDataOf+'&gallery_category_id='+gallery_cat,
                success: function(data){
                    var images = JSON.parse(data);
                    var i = 1;
                    $.each(images, function(key,value){
                        $('.sample-'+i+' img').attr('src', path+value.img);
                        $('.sample-'+i+' a').attr('href', hr+value.slug);
                        i = parseInt(i) + 1;
                    });
                    /*$('.sample-1 img').attr('src', path+images.image1);
                    $('.sample-2 img').attr('src', path+images.image2);
                    $('.sample-3 img').attr('src', path+images.image3);
                    $('.sample-4 img').attr('src', path+images.image4);
                    $('.sample-5 img').attr('src', path+images.image5);

                    $('.sample-1 a').attr('href', hr.slug);
                    $('.sample-2 a').attr('href', hr.slug);
                    $('.sample-3 a').attr('href', hr.slug);
                    $('.sample-4 a').attr('href', hr.slug);
                    $('.sample-5 a').attr('href', hr.slug);*/
                }
            });
			setTimeout(function(){ 
				$('.imagearea .image-overlay').hide();
				$('.imagearea .image-loading').hide();
			}, 1000);
			
		}
	});

</script>
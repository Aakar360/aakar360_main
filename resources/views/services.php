<?php echo $header?>
    <section id="at-inner-title-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6" style="padding-top: 120px;">
                    <div class="at-inner-title-box">
                        <h2><?=(isset($services_category->name) ? translate($services_category->name) : translate('Services'));?></h2>
                        <p><a href="<?=url('')?>">Home</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <a href="#"><?=(isset($services_category->name) ? translate($services_category->name) : translate('Services'));?></a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <img src="assets/images/title.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- Inner page heading end -->

    <!-- Property start from here -->
    <section class="at-property-sec at-property-right-sidebar">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="at-sidebar">
                        <div class="at-sidebar-search at-sidebar-mar">
                            <form id="search" method="get">
                                <div class="input-group">
                                    <input placeholder="<?=translate('Search keyword')?>" value="<?=isset($_GET['search'])?$_GET['search']:''?>" class="form-control" name="search" type="text">
                                    <span class="input-group-btn">
                                  <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                  </span>
                                </div>
                            </form>
                        </div>
                        <div class="at-categories clearfix">
                            <h3 class="at-sedebar-title">categories</h3>
                            <ul>
                                <?php foreach($serv as $cat){
                                    echo '<li><a href="services?search='.translate($cat->name).'">'.translate($cat->name).'</a></li>';
                                    $childs = DB::select("SELECT * FROM services_category WHERE parent = ".$cat->id." ORDER BY id DESC");
                                    foreach ($childs as $child){
                                        echo '<li><a href="services?search='.translate($child->name).'">- '.$child->name.'</a></li>';
                                    }
                                }
                                ?>
                                <!--li><a href="#">new building</a> <span class="pull-right">(10)</span>
                                </li>
                                <li><a href="#">modern design</a> <span class="pull-right">(08)</span>
                                </li>
                                <li><a href="#">best design</a> <span class="pull-right">(29)</span>
                                </li>
                                <li><a href="#">popular design</a> <span class="pull-right">(33)</span>
                                </li>
                                <li><a href="#">strong building</a> <span class="pull-right">(23)</span>
                                </li>
                                <li><a href="#">old design</a> <span class="pull-right">(22)</span>
                                </li>
                                <li><a href="#">popular design</a> <span class="pull-right">(29)</span>
                                </li>
                                <li><a href="#">best design</a> <span class="pull-right">(11)</span>
                                </li-->
                            </ul>
                        </div>

                        <div class="at-sidebar-tags">
                            <a href="#">Responsive</a>
                            <a href="#">Web Design</a>
                            <a href="#">Best</a>
                            <a href="#">Modern Design</a>
                            <a href="#">Popular</a>
                            <a href="#">Servar</a>
                            <a href="#">Javascript</a>
                            <a href="#">Jquery</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <?php
                        foreach($services as $service){
                            echo '<div class="col-md-4 col-sm-4">
                            <div class="at-property-item at-col-default-mar" id="'.$service->id.'">
                                <div class="at-property-img">
                                    <img src="'.url('/assets/services/'.image_order($service->image)).'" style="width: 100%; height: 160px;" alt="">
                                    <div class="at-property-overlayer"></div>
                                    <a class="btn btn-default at-property-btn" href="services_category/'.path($service->name,$service->id).'" data-title="'.translate($service->name).'" role="button">View Details</a>
                                    
                                </div>
                                
                                <div class="at-property-location">
                                    <h4><a href="services_category/'.path($service->name,$service->id).'" data-title="'.translate($service->name).'">'.translate($service->name).'</a></h4>
                                </div>
                            </div>
                        </div>';
                        }
                        ?>

                    </div>
                </div>
            </div>

        </div>
    </section>


<?php echo $footer?>
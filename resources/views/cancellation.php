<?php echo $header?>
    <section class="at-contact-sec" style="background: white;">
        <div class="container">
            <div class="row">
                <div id="cancel" class="faqs-section">
                    <h2>Cancellations and Modifications</h2>
                    <div class="myXYZ-link">
                        <span>You can cancel your orders under MyXYZ</span>
                        <span class="links">
                            <a href="" class="link-button">Cancel item</a>
                        </span>
                    </div>
                    <div class="queries">
                        <p class="query"><a>What is XYZ's Cancellation Policy?</a></p>
                        <div class="answer" style="display: block;">
                            <div>
                                <p>You can cancel an order until it has not been packed in our Warehouse on App/Website/M-site. This includes items purchased on sale also. Any amount paid will be credited into the same payment mode using which the payment was made</p>
                            </div>
                        </div>
                        <p class="query"><a>Can I modify the shipping address of my order after it has been placed?</a></p>
                        <div class="answer" style="display: block;">
                            <div>
                                <p>Yes, You can modify the shipping address of your order before we have processed (packed) it, by updating it under 'change address' option which is available under ‘My order’ section of App/Website/M-site</p>
                            </div>
                        </div>
                        <p class="query"><a>How do I cancel my Order?</a></p>
                        <div class="answer" style="display: block;">
                            <div>
                                <p>Tap on “My Orders” section under the main menu of your App/Website/M-site and then select the item or order you want to cancel</p>
                            </div>
                        </div>
                        <p class="query"><a>I just cancelled my order. When will I receive my refund?</a></p>
                        <div class="answer" style="display: block;">
                            <div>
                                <p>If you had selected Cash on Delivery, there is no amount to be refunded because you haven't paid for your order. For payments made via Credit Card, Debit Card, Net Banking, or Wallet you will receive refund into the source account within 7-10 days from the time of order cancellation.If payment was made by redeeming PhonePe wallet balance then, refund will be initiated into Phonepe wallet within a day of your order cancellation, which can be later transferred into your bank account, by contacting PhonePe customer support team.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div id="returns" class="faqs-section">
                    <h2>Returns and Exchange</h2>
                    <div class="myXYZ-link">
                        <span>You can return/exchange your orders in MyXYZ</span>
                        <span class="links">
                            <a href="" class="link-button">Exchange item</a>
                            <a href="" class="link-button">Return item</a>
                        </span></div>
                    <div class="queries">
                        <p data-link="[&quot;return-exchange-policy&quot;,&quot;Return-exchange-policy&quot;]" class="query"><a>What is XYZ's Return and Exchange Policy? How does it work?</a></p>
                        <div class="answer">
                            <div>
                                <p>XYZ's returns and exchange policy gives you an option to return or exchange items purchased on XYZ for any reason within the specified return/exchange period (check product details page for the same). We only ask that you don't use the product and preserve its original condition, tags, and packaging. You are welcome to try on a product but please take adequate measure to preserve its condition. There are two ways to return the product to us:</p>
                                <ul>
                                    <li>Pick up: In most locations, we offer a free pick up service. You will see a pickup option when you submit a return request.</li>
                                    <li>Self-Ship: If we don't offer a pick up at your location. In such cases, we will credit the shipping costs in the form of XYZ credits provided the product meets the return policy and you have shared scan copy of the courier receipt with us.</li>
                                </ul>
                                <p>During Pick Up, our delivery agent may do a quality check on the return. For a successful pick up, we will initiate a doorstep refund for the return amount into the refund mode selected at the time of initiating the return request. This doorstep refund may not be applicable for some pin codes in which case refund will be initiated after the product has been received at our warehouse and has passed a quality check. If the picked up product does not pass the quality check, we shall ship it back to you.
                                    If you choose to exchange the item for reason of mismatch of size or receipt of a defective item, you will be provided with a replacement of the item, free of cost. However all exchanges are subject to stock availability and subject to your address being serviceable for an exchange. If you choose to exchange an item, our delivery representative will deliver the new item to you and simultaneously pick up the original item from you. Please note that we are only able to offer size exchanges. If you wish to exchange your item for an alternative product, we suggest that you return it to obtain refund and purchase the new item separately.
                                </p>
                                <p> The following EXCEPTIONS and RULES apply to this Policy: </p>
                                <ul>
                                    <li>
                                        <p>Swarovski, Precious Jewelry, Cosmetics, Rayban  Sunglasses, Socks, Deodorants, Perfumes, Briefs, Shapewear Bottoms, any Lingerie Set that includes a Brief, Swimwear, Mittens, Wrist-Bands <strong>cannot be exchanged or returned</strong>.</p>
                                    </li>
                                    <li>
                                        <p>Some products like fine jewellery, watches and selected products which are susceptible to damage can only be returned/exchanged for a limited number of days. Certain products like sherwanis can only be exchanged not returned. Please read the Product Detail Page to see the number of days upto which a product can be returned/exchanged, post-delivery. </p>
                                    </li>
                                    <li>
                                        <p>Due to the intimate nature, we handle returns for certain Innerwear, Sleepwear and Lingerie items differently. Only self-ship return is allowed for such items, hence pickup facility will not be available.Also, these items cannot be exchanged.</p>
                                    </li>
                                    <li>
                                        <p>All items to be returned or exchanged must be unused and in their original condition with all original tags and packaging intact (for e.g. shoes must be packed in the original shoe box). </p>
                                    </li>
                                    <li>Under Exchange Policy, only size exchanges are allowed. Items can be exchanged for a similar size or a different size. Exchanges are allowed only if your address is serviceable for an exchange by our Logistics team. </li>
                                    <li>In case you have purchased an item which has a free gift/offer associated with it and you wish to return the main item, then you will have to return the free product as well</li>
                                    <li>XYZ will not be liable for the products returned by mistake. In circumstances where an extra or a different product is returned by mistake, XYZ would not be accountable for misplacement or replacement of the product and is not responsible for its delivery back to the User </li>
                                    <li>If you self-ship your returns, kindly pack the items securely to prevent any loss or damage during transit. For all self-shipped returns, we recommend you use a reliable courier service. </li>
                                    <li>If you self-ship your returns, your shipping costs would be reimbursed subject to your return having met our Returns and Exchange Policy and the image of the courier receipt is shared by you and validated by us. For self ship returns the refund for returned products will only be initiated if they pass through a quality check conducted at the warehouse. If the quality check fails the product will be reshipped back to you.</li>
                                </ul>
                            </div>
                        </div>
                        <p data-link="to-return-a-product" class="query"><a>To return a product to XYZ, please follow these steps:</a></p>
                        <div class="answer">
                            <div>
                                <p>You can return products purchased from XYZ within the specified return/exchange period(check product details page for the same), except for our non-returnable products and high-value products which can only be returned for limited number of days.</p>
                                <ul>
                                    <li>
                                        <p>Create a 'Return Request' under “My Orders” section of App/Website/M-site. Follow the screens that come up after tapping on the 'Return’ button. Please make a note of the Return ID that we generate at the end of the process. Keep the item ready for pick up or ship it to us basis on the return mode </p>
                                    </li>
                                    <li>
                                        <p>We offer pick-up facility in selected locations basis our courier serviceability. </p>
                                    </li>
                                    <li>If reverse pick-up option is not available at your location you can self-ship the product to us </li>
                                </ul>
                                <p><strong>Pick-up</strong>: If you select to schedule a pick-up, please place the product in a packet and the product must be unused, unwashed and all the tags are intact.Keep the packet ready and open to expedite the return pickup. Our staff may initially examine the product at the time of pickup and a further quality check of the product will be conducted at our Returns Desk.  </p>
                                <p><strong>Self-ship</strong>: In case your area pincode is not eligible for “Pick-up” mode then, please self-ship the product to our Returns desk.Please pack the product and ensure product is unused, unwashed and all the tags are intact.Also, please mention Order No and Return id on a piece of paper and place it in the packet. Kindly address the package to the address of the Returns desk closest to you. We have listed out the addresses of the Returns desk in another section on this page.</p>
                                <p>We will send you a confirmation email as soon as we receive the shipment at our warehouse. At any time, you can track the status of your return request on App/Website/Msite. </p>
                                <p><strong>NOTE: NO PRODUCTS SHALL BE ACCEPTED IF THE WARRANTY CARD IS MISSING WHILE RETURN OR EXCHANGE OF THE PRODUCTS.</strong></p>
                            </div>
                        </div>
                        <p data-link="place-an-exchange-request" class="query"><a>How do I place an exchange request on XYZ?</a></p>
                        <div class="answer">
                            <div>
                                <p>If you would like to exchange products purchased from XYZ, please follow below mentioned steps:</p>
                                <ul>
                                    <li>
                                        <p>You can create exchange for products purchased from XYZ within the specified return/exchange period (check product details page for the same) under “My Orders” section of App/Website/M-site. If your address is serviceable for exchange you will be able to proceed and generate an exchange id. Please note down your exchange id for future reference </p>
                                    </li>
                                    <li>
                                        <p>Place the product in a packet but do not seal it. Please ensure product is unused, unwashed with all the tags intact. </p>
                                    </li>
                                    <li>Hand over the original product to our delivery staff and receive the exchange item from him. Please ensure that you have the original item available with you at the same address which has been selected for delivery of the exchange item.</li>
                                    <li>At any time, you can track the status of your exchange requests under “My Orders” of App/Website/M-site.</li>
                                </ul>
                            </div>
                        </div>
                        <p class="query"><a>What is No Questions Asked Returns?</a></p>
                        <div class="answer">
                            <div>
                                <p>Once you create a return via App / Desktop as per the returns policy, XYZ will ensure a quick, easy and seamless returns experience for you. Our delivery agents may perform a simple quality check at your doorstep and upon acceptance of the return, your refund will be initiated immediately.</p>
                            </div>
                        </div>
                        <p class="query"><a>Why has my return been put on hold despite No Questions Asked Returns Policy?</a></p>
                        <div class="answer">
                            <div>
                                <p>At the time of creating a return, customers are requested to confirm (via a check box click) that the product being returned is unused with original tags intact. At the time of pickup, if the above conditions are not found to be met, your return may be put on hold pending further clarification with our Contact Center.</p>
                            </div>
                        </div>
                        <p class="query"><a>What is Instant Refunds?</a></p>
                        <div class="answer">
                            <div>
                                <p>Upon successful pickup of the return product at your doorstep, XYZ will instantly initiate the refund to your source account or chosen method of refund. Instant Refunds is not available in a few select pin codes and for all self ship returns.</p>
                            </div>
                        </div>
                        <p class="query"><a>Why have I not received my Refund despite Instant Refunds policy?</a></p>
                        <div class="answer">
                            <div>
                                <p>For refunds taken into PhonePe &amp; Bank Account (via IMPS), your refund will reflect in your account within a day once it has been initiated at the doorstep. For refunds to source account (that is Credit Card, Debit Card and Netbanking), your refund may take 7-10 days to reflect in your account depending upon your bank partner.</p>
                            </div>
                        </div>
                        <p data-link="how-long-to-receive-return-refund" class="query"><a>How long would it take me to receive the refund of the returned product?</a></p>
                        <div class="answer">
                            <div>
                                <p>After the refund has been initiated by XYZ as per the Returns Policy, the refund amount is expected to reflect in the customer account as per the following timelines:</p>
                                <ul>
                                    <li>NEFT - 1 to 3 days post refund initiation  </li>
                                    <li>XYZ Credit - Instant </li>
                                    <li>Online Refund – 7 to 10 days post refund initiation, depending on your bank partner </li>
                                    <li>PhonePe wallet – 1 day post refund initiation.</li>
                                </ul>
                                <p>Please note, XYZ initiates the refund upon successful return pick up, or after the returned item has reached us and quality check is successful. Therefore, the refund initiation time may vary by time taken by the courier partner to deliver the return to a XYZ warehouse. In case of any refund discrepancies, XYZ may at its sole discretion, request you to share with us a screenshot of your bank statement.</p>
                            </div>
                        </div>
                        <p data-link="return-multiple-product-from-single-order" class="query"><a>How do I return multiple products from a single order?</a></p>
                        <div class="answer">
                            <div>
                                <p>If you are returning multiple products from a single order then, you will receive a separate Return ID via e-mail for each item. If you are self-shipping the products, you can ship all the products in a single shipment. Please mention the Return IDs for all the products on a single sheet of paper and place it inside the packet.If mode of return is Pickup, our courier person will pickup the products from the pickup address.</p>
                            </div>
                        </div>
                        <p data-link="product-return-location-serviceability" class="query"><a>Does XYZ pick up the product I want to return from my location?</a></p>
                        <div class="answer">
                            <div>
                                <p>Currently, we pick up products only from selected PIN Codes. If your area pincode is serviceable, you will be able to select the pickup option when you create a Return Request on App/Website/M-site.</p>
                                <ul>
                                    <li>We will pick up the return within 4 - 7 days from the request placement date.</li>
                                    <li>Please keep the return shipment ready.</li>
                                </ul>
                            </div>
                        </div>
                        <p data-link="return-self-ship-explained" class="query"><a>How can I Self-Ship the product to XYZ?</a></p>
                        <div class="answer">
                            <div>
                                <p>If your area pin code is not serviceable for pickup, you will need to self-ship the return item via any reliable courier partner. Please ensure to place a sheet of paper with the details of Order ID and Return ID for each item included in the package. For all self-shipped returns, you will be duly reimbursed the shipping costs. Therefore, please ensure that scan copy of courier bill/receipt is shared via Contact us option available on App/Website/M-site. The courier bill/receipt should satisfy the following conditions for successful processing: </p>
                                <ul>
                                    <li>It should capture the weight of the return package. </li>
                                    <li>Residential/office address, destination address, shipment date, amount and other details should be mentioned.</li>
                                    <li>The information on the receipt should NOT be edited/over-written.</li>
                                    <li>
                                        <p>The courier charge (amount mentioned on the receipt) should not overshoot the Sender-Destination-Service combination and shall be cross checked with the courier company.</p>
                                        <p>This is subject to your returns being inspected and successfully processed upon receipt at our end. </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <p data-link="return-self-ship-addresses" class="query"><a>Where should I self-ship the Returns?</a></p>
                        <div class="answer">
                            <div>
                                <p>You can send the return to any one of the following returns processing facilities depending on your location. Kindly do not send it to on any other address as the package would not be acceptable then. Please address the return shipments to the seller you purchased the products from, details of which can be found on your order invoice.</p>
                                <p>**South </p>
                                <p> InstaKart Services Pvt. Ltd,
                                    A/C Seller Name:
                                    Survey Numbers 231, 232 and 233,
                                    Soukya Road, Samethanahalli Village,
                                    Anugondanahalli Hobli, Hoskote Taluk,
                                    Bangalore - 560067,
                                </p>
                                <p> (Professional courier are not accepted at Bangalore return desk) </p>
                                <p>**North </p>
                                <p> InstaKart Services Pvt. Ltd,
                                    A/C Seller Name:
                                    Dev Niwas, K-2 block, Khasra No-819,
                                    behind Maruti Service Centre, Mahipalpur, New Delhi - 110037
                                </p>
                                <p>**West </p>
                                <p> InstaKart Services Pvt. Ltd,
                                    Avon Eng Compound
                                    Makwana Road
                                    Marol Naka, near Indiana building, behind Marol metro station
                                    , Andheri East Mumbai 400059
                                </p>
                                <p>**East </p>
                                <p> InstaKart Services Pvt. Ltd,
                                    A/C Seller Name:
                                    379, S N Roy Road,
                                    , Landmark : Piyashi Cinema,
                                    , Kolkata - 700038,
                                </p>
                                <p> (DTDC courier are not accepted at Kolkata return desk)</p>
                            </div>
                        </div>
                        <p class="query"><a>Why has my return request been declined?</a></p>
                        <div class="answer">
                            <div>
                                <p>This may have happened, if the item you returned is used, damaged or original tags are missing. For more details, please call our customer care.</p>
                            </div>
                        </div>
                        <p class="query"><a>Why did the pick up of my product fail?</a></p>
                        <div class="answer">
                            <div>
                                <p>We make three attempts to pick up the item, if the item is not picked up in the third attempt, Pickup request will be marked as failed. You can initiate a new return request, if item meets the return criteria and is within the specified return/exchange period (check product details page for the same). For more details, please call our customer care.</p>
                            </div>
                        </div>
                        <p class="query"><a>Why is my returned product re-shipped?</a></p>
                        <div class="answer">
                            <div>
                                <p>This may have happened, if the item you have returned is used, damaged or original tags or MRP tags are missing. For more details, please call our customer care.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php echo $footer?>
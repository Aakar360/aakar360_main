<?php echo $header?>
    <section class="at-contact-sec" style="background: white;">
        <div class="container">
            <div class="row">
                <div class="t-0M7P _3olgyV _2doH3V">
                    <div class="_1VV5Cf _1QHAXj">
                        <div class="_1SFos- " style="transform: scaleX(1);"></div>
                    </div>
                    <div class="_3e7xtJ">
                        <div>
                            <div class="bhgxx2 col-12-12">
                                <div class="_1StnW-">
                                    <div>
                                        <h2 id="payments">Payments</h2>
                                        <h2>How do I pay for a XYZ purchase?</h2>
                                        <p>XYZ offers you multiple payment methods. Whatever your online mode of payment, you can rest assured that XYZ's trusted payment gateway partners use secure encryption technology to keep your transaction details confidential at all times.</p>
                                        <p>You may use Internet Banking, Gift Card, Cash on Delivery and Wallet to make your purchase.</p>
                                        <p>XYZ also accepts payments made using Visa, MasterCard, Maestro and American Express credit/debit cards in India and 21 other countries.</p>

                                        <h2>Are there any hidden charges (Octroi or Sales Tax) when I make a purchase on XYZ?</h2>
                                        <p>There are NO hidden charges when you make a purchase on XYZ. The prices listed for all the items are final and all-inclusive. The price you see on the product page is exactly what you pay.</p>
                                        <p>Delivery charges may be extra depending on the seller policy. Please check individual seller for the same. In case of seller WS Retail, the ₹50 delivery charge is waived off on orders worth ₹500 and over.</p>

                                        <h2>What is Cash on Delivery?</h2>
                                        <p>If you are not comfortable making an online payment on XYZ.com, you can opt for the Cash on Delivery (C-o-D) payment method instead. With C-o-D you can pay in cash at the time of actual delivery of the product at your doorstep, without requiring you to make any advance payment online.</p>
                                        <p>The maximum order value for a Cash on Delivery (C-o-D) payment is ₹50,000. It is strictly a cash-only payment method. Gift Cards or store credit cannot be used for C-o-D orders. Foreign currency cannot be used to make a C-o-D payment. Only Indian Rupees accepted.</p>

                                        <h2>How do I pay using a credit/debit card?</h2>
                                        <p>We accept payments made by credit/debit cards issued in India and 21 other countries.</p>
                                        <p><strong>Credit cards</strong></p>
                                        <p>We accept payments made using Visa, MasterCard and American Express credit cards.</p>
                                        <p>To pay using your credit card at checkout, you will need your card number, expiry date, three-digit CVV number (found on the backside of your card). After entering these details, you will be redirected to the bank's page for entering the online 3D Secure password.</p>
                                        <p><strong>Debit cards</strong></p>
                                        <p>We accept payments made using Visa, MasterCard and Maestro debit cards.</p>
                                        <p>To pay using your debit card at checkout, you will need your card number, expiry date (optional for Maestro cards), three-digit CVV number (optional for Maestro cards). You will then be redirected to your bank's secure page for entering your online password (issued by your bank) to complete the payment.</p>
                                        <p>Internationally issued credit/debit cards cannot be used for Flyte, Wallet and eGV payments/top-ups.</p>

                                        <h2>Is it safe to use my credit/debit card on XYZ?</h2>
                                        <p>Your online transaction on XYZ is secure with the highest levels of transaction security currently available on the Internet. XYZ uses 256-bit encryption technology to protect your card information while securely transmitting it to the respective banks for payment processing.</p>
                                        <p>All credit card and debit card payments on XYZ are processed through secure and trusted payment gateways managed by leading banks. Banks now use the 3D Secure password service for online transactions, providing an additional layer of security through identity verification.</p>

                                        <h2>What steps does XYZ take to prevent card fraud?</h2>
                                        <p>XYZ realizes the importance of a strong fraud detection and resolution capability. We and our online payments partners monitor transactions continuously for suspicious activity and flag potentially fraudulent transactions for manual verification by our team.</p>
                                        <p>In the rarest of rare cases, when our team is unable to rule out the possibility of fraud categorically, the transaction is kept on hold, and the customer is requested to provide identity documents. The ID documents help us ensure that the purchases were indeed made by a genuine card holder. We apologise for any inconvenience that may be caused to customers and request them to bear with us in the larger interest of ensuring a safe and secure environment for online transactions.</p>

                                        <h2>What is a 3D Secure password?</h2>
                                        <p>The 3D Secure password is implemented by VISA and MasterCard in partnership with card issuing banks under the "Verified by VISA" and "Mastercard SecureCode" services, respectively.</p>
                                        <p>The 3D Secure password adds an additional layer of security through identity verification for your online credit/debit card transactions. This password, which is created by you, is known only to you. This ensures that only you can use your card for online purchases.</p>

                                        <h2>How can I get the 3D Secure password for my credit/debit card?</h2>
                                        <p>You can register for the 3D Secure password for your credit/debit card by visiting your bank's website. The registration links for some of the banks have been provided below for easy reference:</p>
                                        <table border="1" cellpadding="0" cellspacing="0"  style="text-align: center; width: 100%">
                                            <tr>
                                                <td><a href="https://retail.onlinesbi.com/personal/secure_card_transactions.html">State Bank of India</a></td>
                                                <td><a href="http://www.icicibank.com/aboutus/article/ivr.html">ICICI Bank</a></td>
                                                <td><a href="https://netsafe.hdfcbank.com/ACSWeb/enrolljsp/Registration1.jsp">HDFC Bank</a></td>
                                                <td><a href="https://acs2.enstage-sas.com/ACSWeb/EnrollWeb/PNB/main/index.jsp">Punjab National Bank</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="https://secure.axisbank.com/ACSWeb/EnrollWeb/AxisBank/main/index.jsp">Axis Bank</a></td>
                                                <td><a href="https://corpbank.electracard.com/corpbank/enrollment/enroll_welcome.jsp">Corporation Bank</a></td>
                                                <td><a href="https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/AndhraBank/main/index.jsp">Andhra Bank</a></td>
                                                <td><a href="http://www.bankofbaroda.com/vbv.asp">Bank of Baroda</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="http://www.standardchartered.co.in/personal/credit-cards/en/3d_secure.html">Standard Chartered India</a></td>
                                                <td><a href="https://ubi.electracard.com/ubi/enrollment/WhatisVerifyByVisa.jsp">Union Bank of India</a></td>
                                                <td><a href="https://cbi.electracard.com/cbi/enrollment/enroll_welcome.jsp">Central Bank of India</a></td>
                                                <td><a href="https://www.online.citibank.co.in/products-services/online-services/online-security.htm">Citibank</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="https://www.deutschebank.co.in/direct_debit.html">Deutsche Bank</a></td>
                                                <td><a href="http://www.dhanbank.com/personal/vbv-faq.aspx">Dhanlakshmi Bank</a></td>
                                                <td><a href="https://secureonline.idbibank.com/ACSWeb/EnrollWeb/IDBIBank/main/index.jsp">IDBI Bank</a></td>
                                                <td><a href="https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/CanaraBank/main/termsandconditions.jsp">Canara Bank</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="http://www.indusind.com/Terms-and-Conditions-for-IBL-e-Secure-service.html">IndusInd Bank</a></td>
                                                <td><a href="https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/FederalBank/main/index.jsp">Federal Bank</a></td>
                                                <td><a href="https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/KotakBank/main/init.jsp?go=2">Kotak Mahindra Bank</a></td>
                                                <td><a href="https://cardsecurity.enstage.com/ACSWeb/EnrollWeb/KVB/main/vbv.jsp">Karur Vysya Bank</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="https://www.obcindia.co.in/obcnew/site/inner.aspx?status=R1&amp;menu_id=16">Oriental Bank of Commerce</a></td>
                                                <td><a href="https://www.southindianbank.com/content/viewContentLvl2.aspx?linkIdLvl2=16&amp;linkId=920">South Indian Bank</a></td>
                                                <td><a href="https://www.vijayabank.com/Card-Services/Verified-by-VISA">Vijaya Bank</a></td>
                                                <td>-</td>
                                            </tr>
                                        </table>

                                        <h2>Can I make a credit/debit card or Internet Banking payment on XYZ through my mobile?</h2>
                                        <p>Yes, you can make credit card payments through the XYZ mobile site and application. XYZ uses 256-bit encryption technology to protect your card information while securely transmitting it to the secure and trusted payment gateways managed by leading banks.</p>
                                        <h2>How does 'Instant Cashback' work?</h2>
                                        <p>The 'Cashback' offer is instant and exclusive to XYZ.com. You only pay the final price you see in your shopping cart.</p>
                                        <h2>How do I place a Cash on Delivery (C-o-D) order?</h2>
                                        <p>All items that have the "Cash on Delivery Available" icon are valid for order by Cash on Delivery.</p>
                                        <p>Add the item(s) to your cart and proceed to checkout. When prompted to choose a payment option, select "Pay By Cash on Delivery". Enter the CAPTCHA text as shown, for validation.</p>
                                        <p>Once verified and confirmed, your order will be processed for shipment in the time specified, from the date of confirmation. You will be required to make a cash-only payment to our courier partner at the time of delivery of your order to complete the payment.</p>
                                        <p>Terms &amp; Conditions:</p>
                                        <ul>
                                            <li>The maximum order value for C-o-D is ₹50,000</li>
                                            <li>Gift Cards or Store Credit cannot be used for C-o-D orders</li>
                                            <li>Cash-only payment at the time of delivery.</li>
                                        </ul>
                                        <h2>What is XYZ's credit card EMI option?</h2>
                                        <p>With XYZ's credit card EMI option, you can choose to pay in easy installments of 3, 6, 9, 12, 18*, or 24 months* with credit cards from the following banks:</p>
                                        <ul>
                                            <li>HDFC</li>
                                            <li>Citi</li>
                                            <li>ICICI</li>
                                            <li>Kotak</li>
                                            <li>Axis</li>
                                            <li>IndusInd</li>
                                            <li>SBI</li>
                                            <li>Standard Chartered</li>
                                            <li>HSBC</li>
                                        </ul>
                                        <p>*18 &amp; 24 months EMI options are available from select banks only. Please refer to the table below for more details:</p>
                                        <table border="1" cellpadding="0" cellspacing="0" style="text-align: center; width: 100%">
                                            <thead style="text-align: center;">
                                                <tr style="text-align: center;">
                                                    <th style="text-align: center"><strong>Banks</strong></th>
                                                    <th style="text-align: center;"><strong>Supports 18 &amp; 24 months tenure</strong></th>
                                                    <th style="text-align: center;"><strong>Minimum order value to avail 18 &amp; 24 months EMI options</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>HDFC</td>
                                                    <td>Yes</td>
                                                    <td>₹ 10,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Citi</td>
                                                    <td>Yes</td>
                                                    <td>₹ 10,000</td>
                                                </tr>
                                                <tr>
                                                    <td>ICICI</td>
                                                    <td>Yes</td>
                                                    <td>₹ 10,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Kotak</td>
                                                    <td>Yes</td>
                                                    <td>₹ 4,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Axis</td>
                                                    <td>Yes</td>
                                                    <td>₹ 4,000</td>
                                                </tr>
                                                <tr>
                                                    <td>IndusInd</td>
                                                    <td>Yes</td>
                                                    <td>₹ 4,000</td>
                                                </tr>
                                                <tr>
                                                    <td>SBI</td>
                                                    <td>No</td>
                                                    <td>NA</td>
                                                </tr>
                                                <tr>
                                                    <td>Standard Chartered</td>
                                                    <td>Yes</td>
                                                    <td>₹ 4,000</td>
                                                </tr>
                                                <tr>
                                                    <td>HSBC</td>
                                                    <td>No</td>
                                                    <td>NA</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p>There is NO processing fee charged for availing XYZ's EMI payment option. On return or exchange, interest charged by the bank till that time will not be refunded by XYZ.</p>
                                        <p>You may check with the respective bank/issuer on how a cancellation, refund or pre-closure could affect the EMI terms, and what interest charges would be levied on you for the same.</p>
                                        <p><strong>Example and Calculations</strong></p>
                                        <p>The table below shows a representative rendering of EMI plans for a Rs 20,000 purchase on XYZ paid using the EMI payment plan</p>
                                        <table border="1" cellpadding="0" cellspacing="0"  style="text-align: center; width: 100%">
                                            <thead>
                                            <tr>
                                                <th style="text-align: center"><strong>Tenure (months)</strong></th>
                                                <th style="text-align: center"><strong>Loan amount</strong></th>
                                                <th style="text-align: center"><strong>Monthly installment</strong></th>
                                                <th style="text-align: center"><strong>Bank interest rate</strong></th>
                                                <th style="text-align: center"><strong>Total effective price you pay</strong></th>
                                                <th style="text-align: center"><strong>Interest paid to Bank</strong></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>3</td>
                                                <td>Rs 20,000</td>
                                                <td>Rs 6,800.44</td>
                                                <td>12.00%</td>
                                                <td>Rs. 20,401.33</td>
                                                <td>Rs. 401.33</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>Rs 20,000</td>
                                                <td>Rs 3,450.97</td>
                                                <td>12.00%</td>
                                                <td>Rs 20,705.80</td>
                                                <td>Rs. 705.80</td>
                                            </tr>
                                            <tr>
                                                <td>9</td>
                                                <td>Rs 20,000</td>
                                                <td>Rs 2,344.32</td>
                                                <td>13.00%</td>
                                                <td>Rs 21,098.89</td>
                                                <td>Rs. 1,098.89</td>
                                            </tr>
                                            <tr>
                                                <td>12</td>
                                                <td>Rs 20,000</td>
                                                <td>Rs 1,786.35</td>
                                                <td>13.00%</td>
                                                <td>Rs 21,436.15</td>
                                                <td>Rs. 1,436.15</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <h2>How do I make a payment using XYZ's credit card EMI option?</h2>
                                        <p>Once you've added the desired items to your XYZ shopping cart, proceed with your order as usual by entering your address. When you're prompted to choose a payment mode for your order, select 'EMI' &amp; follow these simple steps:</p>
                                        <ol>
                                            <li>
                                                <p>Choose your credit-card issuing bank you wish to pay from</p>
                                            </li>
                                            <li>
                                                <p>Select the EMI plan of your preference</p>
                                            </li>
                                            <li>
                                                <p>Enter your credit card details</p>
                                            </li>
                                            <li>
                                                <p>Click 'Save and Pay'</p>
                                            </li>
                                        </ol>
                                        <p>Please note that the full amount will be charged on your card the day of the transaction.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php echo $footer?>
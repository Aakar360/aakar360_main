<?php echo $header?>
<div class="b-hero1">

    <figure <?php if(!empty($dimg)) { ?> style="background-image:url(&quot;<?php echo url('/assets/blog/'.$dimg->banner_image); ?>&quot;)"  <?php } ?> class="b-post__hero-image">
        <div data-w-id="403de785-9462-5110-ab3f-b13328bf2c04" class="b-post__hero-inner">
            <div class="b-container cc-read cc-hero">
                <div class="b-read__wrap cc-hero">
                    <h1 class="b-post__title"> <?php if(!empty($dimg)) { ?><?=translate($dimg->name); } ?></h1>
                    <p class="b-post__subhead"><?php if(!empty($dimg)) { ?><?=$dimg->short_description; } ?></p>
                </div>
            </div>
        </div>
    </figure>
</div>
<div class="row" style="text-align: center;">
    <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8" style="margin-top: -105px; z-index: 1">
        <div class="header-bottom">
            <div class="header-navigation">
                <ul class="navigation navigation--horizontal">
                    <li class="navigation-item navigation-design">
                        <a href="javascript:void(0)" title="Saved"><span>Saved</span><i class="fa fa-save"></i></a>
                    </li>
                    <li class="navigation-item navigation-architecture">
                        <?php $totvisit = 0;
                            foreach($btotal as $btv){
                                $totvisit = $totvisit + $btv->visits;
                            }
                        ?>
                        <a href="javascript:void(0)" title="Total Visits"><span><?=$totvisit; ?> Total Visits</span><i class="fa fa-eye"> <?=$totvisit; ?></i> </a>
                    </li>
                    <li class="navigation-item navigation-art">
                        <?php
                            $totrat = 0;
                            foreach ($btotal as $btr){
                                $total_revi = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$btr->id)[0]->count;
                                $totrat = $totrat + $total_revi;

                            }
                        ?>
                        <a href="javascript:void(0)" title="Liked"><span><?=$totrat; ?> Total Reviews</span><i class="fa fa-pencil-square-o"> <?=$totrat; ?></i></a>
                    </li>
                    <li class="navigation-item navigation-art">
                        <?php
                        $totli = 0;
                        foreach ($btotal as $btl){
                            $li = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$btl->id)[0]->count;
                            $totli = $totli + $li;

                        }
                        ?>
                        <a href="javascript:void(0)" title="Liked"><span><?=$totli; ?> Total Likes</span><i class="fa fa-thumbs-up"> <?=$totli; ?></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<section class="pdtopbtm-50">
    <div class="container">
        <div class="row">
            <button class="btn btn-default filter-button"><i class="fa fa-filter"></i> Filter</button>
            <aside class="sidebar-shop col-md-3 order-md-first">
                <div class="filter-div-close"></div>
                    <div class="sidebar-wrapper at-categories" style="padding: 10px;">
                        <div class="widget">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-body-c" role="button" aria-expanded="true" aria-controls="widget-body-c">Categories</a>
                            </h3>
                            <div class="show collapse in" id="widget-body-c">
                                <div class="widget-body">
                                    <ul class="cat-list">
                                        <?php foreach($cats as $cat){
                                            echo '<li><a href="advices/'.$cat->id.'">'.translate($cat->name).'</a></li>';
                                            $childs = DB::select("SELECT * FROM advices_category WHERE parent = ".$cat->id." ORDER BY id DESC");
                                            foreach ($childs as $child){
                                                echo '<li><a href="advices/'.$child->id.'">- '.$child->name.'</a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->
                    </div>
            </aside>

            <div class="col-md-9 col-lg-9" style="">
                <div class="row " style="padding-top: 30px;">
                    <div class="col-md-4">
                        <div class="featured-item feature-bg-box gray-bg text-center m-bot-0 inline-block radius-less">
                            <div class="icon">
                                <i class="icon-layers"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>Customizable Designs</h4>
                            </div>
                            <div class="desc1">
                                Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="featured-item feature-bg-box gray-bg text-center m-bot-0 inline-block radius-less">
                            <div class="icon">
                                <i class="fa fa-inr"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>Value Pricing</h4>
                            </div>
                            <div class="desc1">
                                Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="featured-item feature-bg-box gray-bg text-center m-bot-0 inline-block radius-less">
                            <div class="icon">
                                <i class="fa fa-check-circle-o"></i>
                            </div>
                            <div class="title text-uppercase">
                                <h4>Wider Selection</h4>
                            </div>
                            <div class="desc1">
                                Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.
                            </div>
                        </div>
                    </div>
                </div>

                <div id="works" class="package_page">
                    <div class="blocks" style="width: 100%">
                        <div class="works block col-lg-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="at-sec-title">
                                        <h2 style="font-size: 15px;">Featured <span>Advices</span></h2>
                                        <div class="at-heading-under-line">
                                            <div class="at-heading-inside-line"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--section class="call_action">
                                <div>
                                    <p>We regularly add fresh new images to our collection, he's some of the subject catagories we like the best:</p>
                                </div>
                            </section-->
                            <div class="home_categories col-lg-12">
                                <?php foreach ($featured as $layout){ ?>
                                    <a href="blog/<?php echo path($layout->title,$layout->id); ?>" class="grow" title="<?=$layout->title; ?>" style="background: url('<?php echo url('/assets/blog/'.$layout->images); ?>') center center;">
                                        <span><?=$layout->title; ?></span></a>
                                <?php } ?>
                                <div class="cleaner"></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /works -->
                <div class="MultiStoryModule Grid Grid--gutters" style="padding:0 10px 30px 10px; width: 100%; margin-top: -60px;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="at-sec-title">
                                <h2 style="font-size: 15px;">Best For <span>You</span></h2>
                                <div class="at-heading-under-line">
                                    <div class="at-heading-inside-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="agent-carousel-3 slider" data-slick='{"slidesToShow": 2, "slidesToScroll": 1}'>
                    <?php foreach ($best as $b) {?>
                        <div class="Grid-col MultiStoryModule-tile MultiStoryModule-tile-0" style="float: left; padding-right: 10px;">
                            <div class="TempoCategoryTile  u-focusTile">
                                <div class="TempoCategoryTile-tile valign-top">
                                    <div class="TempoCategoryTile-imgContainer">
                                        <div class="border-img" style="padding-bottom: 140.09%; height: 0px; width: 100%;">
                                            <img aria-hidden="true" tabindex="-1" src="assets/products/<?=$b->image; ?>" class="TempoCategoryTile-img" style="position: absolute; width: 100%; left: 0px; top: 0px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="MultiStoryModule-overlay" aria-hidden="true">
                                <p class="MultiStoryModule-overlayHeading"><?=$b->title; ?></p>
                                <p class="MultiStoryModule-overlayText"><?=$b->content; ?></p>
                                <div>&nbsp;</div>
                                <div class="text-center">
                                    <a href="<?=$b->link; ?>" class="btn btn-primary btn-sm text-center" style="width: 50%;">View More</a>
                                </div>
                            </div>
                            <a class="MultiStoryModule-overlayLink"></a>
                        </div>
                    <?php } ?>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="row" style="padding-top: 50px;">
                        <div class="col-md-12">
                            <div class="at-sec-title">
                                <h2 style="font-size: 15px;">Advices By <span>Category</span></h2>
                                <div class="at-heading-under-line">
                                    <div class="at-heading-inside-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid" style="padding-top: 10px;">
                        <?php foreach($posts as $gallery){ ?>
                            <div class="grid-item">
                                <div class="tiles">
                                    <a href="advices/<?php echo path($gallery->name,$gallery->id); ?>">
                                        <img src="<?php echo url('/assets/blog/'.$gallery->banner_image); ?>"/>
                                        <div class="icon-holder">
                                            <div class="icons1 mrgntop borderRight0" style="width: 100%;"><?=substr(translate($gallery->name), 0, 20); if(strlen($gallery->name)>20) echo '...';?></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                </div>
                <?php if(!empty($link)){?>
                    <div class="container">
                        <div class="blockquote blockquote--style-1">
                            <div class="row inner-div">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <img src="<?=url('/assets/products/'.image_order($link->image))?>" style="width:50%;height:auto;"/>
                                    </div>
                                    <div class="col-md-7" style="padding-top: 25px;">
                                        <h3><?=$link->content; ?></h3>
                                    </div>
                                    <div class="col-md-2" style="padding:15px 0 0 0">
                                        <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                            GET STARTED
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

    <?php echo $footer?>

    <script>
        $(document).ready(function(){
            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if(activeTab){
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }
        });
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });






    </script>



<script>
jQuery(window).on('load', function(){
var $ = jQuery;
var $container = $('.grid');
$container.masonry({
columnWidth:10,
gutterWidth: 15,
itemSelector: '.grid-item'
});
});
$(document).ready(function(){
    $(document).on('click', '.filter-button', function(){
        $('aside.sidebar-shop').addClass('open');
    });
    $(document).on('click', '.filter-div-close', function(){
        $('aside.sidebar-shop').removeClass('open');
    });
});
</script>
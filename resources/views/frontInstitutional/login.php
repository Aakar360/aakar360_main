<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$data['title'] ?></title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content='<?=$data['desc'] ?>' name='description'/>
    <meta content='<?=$data['keywords'] ?>' name='keywords'/>
    <base href="<?=url('') ?>/" />
    <link rel="shortcut icon" href="<?=url('') ?>/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/iofrm-theme3.css">
    <style>
        .medium-password {
            color: #b7d60a;
        }

        .weak-password {
            color: #ce1d14;

        }

        .strong-password {
            color: #12CC1A;
        }
    </style>
</head>
<body>
<div class="form-body">
    <div class="row">
        <div class="img-holder" style="background-image: url('<?=$data['tp'] ?>/assets/img2.jpg')">
            <div class="bg"></div>
            <div class="info-holder">
            </div>
        </div>
        <div class="form-holder">
            <div class="form-content" style="height:100vh;">
                <div class="form-items">
                    <div class="website-logo">
                        <a href="<?=url('')?>">
                            <div class="logo">
                                <img class="logo-size" src="<?=$data['cfg']->logo ?>" alt="">
                            </div>
                        </a>
                    </div>
                    <h3>Get more things done with Loggin platform.</h3>
                    <p>Access to the most powerfull tool in the entire design and web industry.</p>
                    <?php
                    if (isset($error) && !empty($error)){
                        echo '<div class="alert alert-warning">'.translate($error).'</div>';
                    }
                    ?>
                    <form action="" method="post">
                        <?=csrf_field() ?>
                        <input tabindex="1" data-dependency="first" name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control" placeholder="E-mail Address" required  />
                        <input tabindex="2" class="form-control" type="password" id="password" name="password" onkeyup="checkPasswordStrength();" placeholder="Password" required>
                        <span id="password-strength-status"></span>
                        <div class="form-button">
                            <button id="submit" name="login" type="submit" class="ibtn" tab-index="3">Login</button> <a href="<?=url('reset-password')?>">Forget password?</a>
                            OR <a href="register/institutional">Register with us!</a>
                        </div>
                    </form>
                    <!--div class="other-links">
                        <span>Or login with</span>
                            <a href="#"><i class="fab fa-facebook-f"></i></a><a href="#"><i class="fab fa-google"></i></a><a href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=$data['tp'] ?>/assets/jquery.min.js"></script>
<script src="<?=$data['tp'] ?>/assets/main.js"></script>
</body>

</html>
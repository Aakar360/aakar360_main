<?php echo $header?>
<?php
if($banners){ ?>
<section class="at-main-slider">
    <div class="flexslider">
        <ul class="slides">
            <?php foreach($banners as $banner){ ?>
            <li data-thumb="assets/banners/<?=$banner->image; ?>">
                <img src="assets/banners/<?=$banner->image; ?>" alt="">
                <?php if($banner->title != ''){ ?>
                <div class="inner-border">
                    <div class="at-inner-title-box text-center">
                        <h1 style="text-transform: none;"><?=$banner->title; ?></h1>
                        <p style="text-transform: none;"><?=$banner->short_description; ?></p>
                    </div>
                </div>
            <?php } ?>
            <!--p class="flex-caption">
				Get your dream home from <span>HOMY</span>
                </p-->
            </li>
            <?php } ?>
        </ul>
    </div>
</section>
<?php } ?>
<section style="padding-top: 40px; padding-bottom: 30px;" id="sign">
    <div class="container">
        <div class="row " >
            <div class="agent-carousel-2 mlr-15" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>
                <div class="at-agent-col pt-40">
                    <div class="featured-item feature-bg-box gray-bg text-center m-bot-0 inline-block radius-less">
                        <div class="icon">
                            <i class="icon-layers"></i>
                        </div>
                        <div class="title text-uppercase">
                            <h4>Customizable Designs</h4>
                        </div>
                        <div class="desc1">
                            Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.
                        </div>
                    </div>
                </div>
                <div class="at-agent-col pt-40">
                    <div class="featured-item feature-bg-box gray-bg text-center m-bot-0 inline-block radius-less">
                        <div class="icon">
                            <i class="fa fa-inr"></i>
                        </div>
                        <div class="title text-uppercase">
                            <h4>Value Pricing</h4>
                        </div>
                        <div class="desc1">
                            Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.
                        </div>
                    </div>
                </div>
                <div class="at-agent-col pt-40">
                    <div class="featured-item feature-bg-box gray-bg text-center m-bot-0 inline-block radius-less">
                        <div class="icon">
                            <i class="fa fa-check-circle-o"></i>
                        </div>
                        <div class="title text-uppercase">
                            <h4>Wider Selection</h4>
                        </div>
                        <div class="desc1">
                            Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="at-product-category" style="margin: 0; padding: 15px 0;border:none;">
    <div class="container" style="padding: 0 10px;">
        <div class="row">
            <div class="col-md-12">
                <div class="at-sec-title">
                    <h2 style="font-size: 15px;">Shop By <span>Popular </span>Category</h2>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php $pcs = getPCategories(); $i=1; ?>
        <div class="row">
            <div class="col-md-12">
                <div class="agent-carousel" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
                    <?php foreach($pcs as $pc){
                    if($i%2 !=0)
                        echo '<div class="at-agent-col">';
                    $parent = \App\Category::where('id', $pc->parent)->first();
                    $path = $pc->path;
                    if($parent !== null){
                        $path = $parent->path.'/'.$pc->path;
                    }
                    ?>
                    <a class="" href="<?=url('i/products/'.$path); ?>">
                        <div class="col-sm-12 pcs"><div class="col-sm-12 hover-effect-zoom" style="background-color: #ffffff;">
                                <center>
                                    <img src="assets/products/<?=$pc->image; ?>"/>
                                </center>
                                <h6 class="text-center">
                                    <?=$pc->name; ?>
                                    <i class="fa fa-angle-right" style="font-size: 14px;font-weight: 600;"></i>
                                </h6>
                            </div>
                        </div>
                    </a>
                    <?php if($i%2 == 0)
                        echo '</div>';
                    $i++;?>
                    <?php }
                    if($i%2 == 0)
                        echo '</div>';
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if(count($pcss)){ ?>
<section class="at-product-category" style="margin: 0; padding: 15px 0;border:none;">
    <div class="container" id="brandsx">
    <?php
    $postID = '';
    $nb = 1;
    foreach($pcss as $pc){?>
    <!-- Product layout two -->
        <?php $nb = $pc->popular_priority;
        $pcp = \App\Products::where('category', $pc->id)->orderBy('id', 'DESC')->get();
        if(count($pcp) == 0){
            continue;
        }

        ?>
        <div class="row">

            <div class="col-md-12 main-title">
                <div class="line-separator">
                    <h4 class="title"><?=$pc->name; ?></h4>
                    <a class="view-all-category" href="<?=url('i/products/'.$pc->path); ?>">View All <i class="fa fa-caret-right"></i></a>
                </div>
                <div class="slide-6 theme-arrow product-m">
                    <?php

                    $count = 0;
                    foreach($pcp as $cp){
                    if($count == 6){
                        break;
                    }
                    $customer_id = '';

                    if(institutional('id')!==''){
                        $customer_id = institutional('sid');
                    }
                    $cp_price = getInstitutionalPriceFancy($cp->id,$customer_id, $hub);

                    if($cp_price != 'NA'){
                    ?>
                    <div class="swiper-wrapper">
                        <article class="swiper-slide">
                            <div class="product-layout">
                                <div class="product-thumb ">
                                    <div class="product-inner">
                                        <div class="product-image">
                                            <a href="i/product/<?=path($cp->title, $cp->id); ?>">
                                                <img src="<?=url('/assets/products/'.image_order($cp->images)); ?>" alt="<?=translate($cp->title); ?>" style="height: 138px;">
                                            </a>
                                            <div class="action-links">
                                                <a href="javascript:void(0);" class="ajax-spin-cart">
                                                    <i class="fa fa-shopping-cart"></i>
                                                </a>
                                                <a class="wishlist action-btn btn-wishlist" href="" title="Wishlist">
                                                    <i class="fa fa-heart"></i>
                                                </a>

                                            </div>
                                        </div>
                                        <!-- end of product-image -->
                                        <div class="product-caption">
                                            <h4 class="product-name text-ellipsis"> <a href="i/product/<?=path($cp->title, $cp->id); ?>" title="<?=translate($cp->title); ?>"><?=translate($cp->title); ?></a></h4>
                                            <a href="product/<?=path($cp->title, $cp->id); ?>" class="btn btn-primary btn-sm">View Detail</a>
                                        <!--<p class="product-price">
                                                        <span class="price-new"><span class=money><?/*=(customer('user_type') == 'institutional' ? c($cp_price) : c($cp->price)); */?></span></span>
                                                    </p>-->
                                        </div>
                                        <!-- end of product-caption -->
                                    </div>
                                    <!-- end of product-inner -->
                                </div>
                                <!-- end of product-thumb -->
                            </div>
                        </article>
                    </div>

                    <?php $count++; } } ?>
                </div>
            </div>
        </div>

        <?php } ?>
        <input type="hidden" name="brands" class="band" value="<?=$nb; ?>"/>
        <input type="hidden" name="rem_pre" value="<?=$rem_pre; ?>"/>
    </div>
    <div class="container">
        <div id="brand-nav" class="col-md-12">
            <a href="javascript:void(0);" id="load-brand" class="load-brand" onClick="nextBrand()">Load More Products <i class="fa fa-caret-down"></i></a>
        </div>
    </div>
</section>
<?php } ?>
<section class="funfact-section slider_section">
    <div class="container" style="padding: 0;">
        <div class="row" style="width: 100%;margin: 0">
            <div class="col-md-12 pull-left pro-reward-container">
                <div>
                    <div class="pro-rewards-banner pro-rewards-banner-Desktop" data-qa-id="proRewardsBannerContainer" data-reactid="1839" style="margin-bottom: 0">
                    <!--<img src="<?/*=url('assets/css/images/line_graph.jpg'); */?>" class="left_image"/>-->
                        <div class="col-md-6 pull-right right_div">
                            <img src="<?=url('assets/css/images/combined.jpg'); ?>" class="right_image"/>
                        </div>
                        <div class="overlay"></div>
                        <div class="col-md-6 pull-left">
                            <div data-reactid="1840">
                                <h2 data-reactid="1841">Material Mentor Program</h2>
                                <p data-reactid="1842">An innovative program in which all puzzles for material procurement will be on us like When to buy?, At what rate to buy? etc. You have to tell us just when and what you need. So simple isn't it!</p>
                            </div>
                        </div>
                        <div class="col-md-6 pull-left reward-buttons">
                            <a class="bd-button bd-button--large" href="#">Join Now</a>
                            <a  class="bd-button bd-button--large" href="#" style="margin-left: 15px; display: none;">View More</a>
                        </div>
                    </div>
                    <div class="pro-rewards-items slide-7 mlr-15">
                        <div class="pro-reward-item">
                            <i class="fa fa-calendar"></i>
                            <p>Give us material planning when you need it.</p>
                        </div>
                        <div class="pro-reward-item">
                            <i class="fa fa-truck"></i>
                            <p>We will purchase on behalf of you.</p>
                        </div>
                        <div class="pro-reward-item">
                            <i class="fa fa-users"></i>
                            <p>Group buying for best purchase rate.</p>
                        </div>
                        <div class="pro-reward-item">
                            <i class="fa fa-line-chart"></i>
                            <p>Detail market analysis regarding material.</p>
                        </div>
                        <div class="pro-reward-item">
                            <i class="fa fa-inr"></i>
                            <p>Great saving on purchase.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- funfact&form  -->
<section class="funfact-section">
    <div class="container" >
        <div class="col-md-12 funfact" style="">
            <div class="col-md-6 fact-counter pt-30 pb-30" style="padding-top: 60px;">
                <div class="outer-box">
                    <div class="row counter-area">

                        <!--Column-->
                        <article class="column counter-column col-md-6">
                            <div class="item">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <i class="icon-layers"></i>
                                    </div>
                                    <div class="count-outer count-box">
                                        <p>Wider Selection</p>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <!--Column-->
                        <article class="column counter-column col-md-6">
                            <div class="item">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <i class="icon-wallet"></i>
                                    </div>
                                    <div class="count-outer count-box">
                                        <p>Best Price</p>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <!--Column-->
                        <article class="column counter-column col-md-6">
                            <div class="item">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <i class="icon-check"></i>
                                    </div>
                                    <div class="count-outer count-box">
                                        <p>Quality Product</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="column counter-column col-md-6">
                            <div class="item">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <i class="icon-user-following"></i>
                                    </div>
                                    <div class="count-outer count-box">
                                        <p>Satisfied Customer</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 fact-enquiry">
                <div class="consultation">
                    <div class="consultation-form">
                        <div class="sec-title" style="margin-bottom: 15px;">
                            <h3 style="margin-bottom: 0">Quick Quote</h3>
                            <small>Get quotation from our experts</small>
                        </div>
                        <!-- default form -->
                        <div class="default-form-area">
                            <form id="contact-form" name="contact_form" class="contact-form" action="" method="post">
                                <?=csrf_field(); ?>
                                <div class="row clearfix">
                                    <div class="col-md-12 col-sm-12 column">
                                        <div class="form-group">
                                            <input type="text" name="form_name" class="form-control" value="" placeholder="Name" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 column">
                                        <div class="form-group">
                                            <input type="email" name="form_email" class="form-control required email" value="" placeholder="Email" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 column">
                                        <div class="form-group">
                                            <input type="tel" minlength="10" maxlength="10" name="form_contact" class="form-control" value="" placeholder="Contact No." required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 column">
                                        <div class="form-group">
                                            <textarea name="form_message" class="form-control textarea required" placeholder="What are you looking for..." required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-section-btn">
                                    <div class="form-group">
                                        <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                        <button class="btn btn-primary" type="submit" data-loading-text="Please wait...">Submit Enquiry</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="funfact-section">
    <div class="container">
        <?php if(count($top_brands) > 0){ ?>
        <div class="row" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16); padding: 0px;margin: 10px 0;">
            <div class="col-lg-12" id="brands" style="padding: 0px;">
                <div class="col-sm-12" style="padding: 0px;">
                    <div class="brand-panel">
                        <div class="brand-panel-title text-center">
                            <h5 class="">Top Brands</h5>
                        </div>
                        <div class="col-md-12 brand-panel-items" style="padding: 15px;background: #ffffff;">
                            <div class="agent-carousel slider" data-slick='{"slidesToShow": 6, "slidesToScroll": 1}'>
                                <?php
                                foreach($top_brands as $tb){
                                ?>
                                <div style="text-align: center;">
                                    <div class="hover-effect" style="border: 1px solid #ddd;padding: 3px;">
                                        <div style="width: 100%; height: 100%;border: 1px solid #555;">
                                            <a href="<?=url('brand/'.$tb->path); ?>" >
                                                <img src="assets/brand/<?php echo $tb->image; ?>" style="width: 100%; max-width:100%; height: 100px;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>

<?php
foreach ($blocs as $bloc){
if (mb_substr($bloc->content, 0, 7) == 'widget:') {
echo $__env->make('widgets/'.mb_substr($bloc->content, 7, 255))->render();
} else {
echo $bloc->content;
}
}
echo $footer
?>

<script>

    var collapse = false;
    function nextBrand(){
        var brandsLoaded = $('.band').val();
        var rem_pre = $('input[name=rem_pre]').val();
        $.ajax({
            url: '<?=url("get-products"); ?>',
            type: 'post',
            data: 'brands='+brandsLoaded+'&_token=<?=csrf_token();?>&limit=1&rem_pre'+rem_pre,
            beforeSend: function(){
                if(!collapse){
                    $('#brand-nav').html('Loading...');
                }
            },
            success: function(data){
                var retData = JSON.parse(data);
                $('.band').val(retData.nb);
                $('input[name=rem_pre]').val(retData.rem_pre);
                $('#brandsx').append(retData.html);
                collapse = retData.collapse;
                $('.slide-6.new').slick({
                    dots: false,
                    arrows: true,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 6,
                                slidesToScroll: 1,
                                infinite: false,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 750,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 470,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        }

                    ]
                });
            },
            complete: function(){
                $('.slide-6').removeClass('new');
                if(!collapse){
                    $('#brand-nav').html('<a href="javascript:void(0);" id="unload-brand" onClick="collapseBrands()">Collapse Products <i class="fa fa-caret-up"></i></a> <a href="javascript:void(0);" id="load-brand" onClick="nextBrand()">Load More Products <i class="fa fa-caret-down"></i></a>');
                }else{
                    $('#brand-nav').html('<a href="javascript:void(0);" id="unload-brand" onClick="collapseBrands()">Collapse Products <i class="fa fa-caret-up"></i></a>');
                }
            }
        });
    }
    function collapseBrands(){
        $.ajax({
            url: '<?=url("get-product"); ?>',
            type: 'post',
            data: '_token=<?=csrf_token();?>&limit=5',
            beforeSend: function(){
                $('#brand-nav').html('Loading...');
            },
            success: function(data){
                var retData = JSON.parse(data);
                $('#brandsx').html('<input type="hidden" name="brands" class="band" value="'+retData.nb+'"/><input type="hidden" name="rem_pre"  value="'+retData.rem_pre+'"/>'+retData.html);
                collapse = false;
                $('.slide-6.new').slick({
                    dots: false,
                    arrows: true,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 6,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 750,
                            settings: {
                                slidesToShow: 4,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 470,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                margin:0
                            }
                        }

                    ]
                });
            },
            complete: function(){
                $('.slide-6').removeClass('new');
                if(!collapse){
                    $('#brand-nav').html('<a href="javascript:void(0);" id="load-brand" onClick="nextBrand()">Load More Products <i class="fa fa-caret-down"></i></a>');
                }else{
                    $('#brand-nav').html('<a href="javascript:void(0);" id="unload-brand" onClick="collapseBrands()">Collapse Products <i class="fa fa-caret-up"></i></a>');
                }
                $('html, body').animate({
                    scrollTop: $("#brandsx").offset().top-200
                }, 'slow');
            }
        });
    }
    $(function() {
//----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
//----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });


    $("#quotebutton").click(function() {
        $('html, body').animate({ scrollTop:$("#quote").offset().top}, 500);
    });
</script>
<script>
    /*$(function(){
        $("#scrollslider").scrollSlider();
    })*/
</script>

<!-- Service Category Popup Start -->
<div class="popup" data-popup="popup-1">
    <div class="popup-innerx">
        <?php if($service_categories == 'Data Not Available'){ ?>
        <div class="item">
            <p></p>
        </div>
        <?php } else { ?>
        <?php
        $ur = 'services_category/';
        foreach($service_categories as $pc){ ?>
        <div class="item">
            <a href="<?=url($ur.path($pc->name, $pc->id)); ?>" class="serviceImg" style="width: 100%;height: auto;margin:0;">
                <img src="<?=url('assets/services/'.$pc->image); ?>" style="width: 100%;height: 145px;"/>
                <span class="serviceImgtitle" style="font-size: 16px;"> <?=$pc->name; ?> </span>
                <span class="overlay"></span>
            </a>
        </div>
        <?php } }?>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
</div>
<div class="popup" data-popup="popup-2">
    <div class="popup-innerx">
        <?php if($design_categories == 'Data Not Available'){ ?>
        <div class="item">
            <p></p>
        </div>
        <?php } else { ?>
        <?php
        $ur = 'design-category-';
        foreach($design_categories as $pc){
        $ur='design-category-'.$pc->layout.'/';
        ?>
        <div class="item">
            <a href="<?=url($ur.$pc->slug); ?>" class="serviceImg" style="width: 100%;height: auto;margin:0;">
                <img src="<?=url('assets/design/'.$pc->image); ?>" style="width: 100%;height: 145px;;"/>
                <span class="serviceImgtitle" style="font-size: 16px;"> <?=$pc->name; ?> </span>
                <span class="overlay"></span>
            </a>
        </div>
        <?php } }?>
        <a class="popup-close" data-popup-close="popup-2" href="#">x</a>
    </div>
</div>
<!--Service Category Popup End -->
<?php
if($show_preferences){ ?>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form action="" method="post">
            <?=csrf_field()?>
            <input type="hidden" id="pre_cats" name="categories" value=""/>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">What type of product are you looking for?</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <?php if($categories){
                        foreach($categories as $category){
                        $ur = 'assets/products/';
                        ?>
                        <div class="col-md-4 proSuggestions" data-id="<?=$category->id?>">
                            <a href="javascript:void(0);" class="serviceImg" style="width: 100%;height: auto;margin:0;">
                                <img src="<?=url($ur.$category->image)?>" style="width: 100%;height: 112px;">
                                <span class="serviceImgtitle" style="font-size: 14px;"><?=$category->name?></span>
                                <span class="overlay"></span>
                            </a>
                            <span class="selector"></span>
                        </div>
                        <?php } } ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Done</button>
                </div>
            </div>
        </form>

    </div>
</div>


<script>
    function submitHub(){
        if($('input[name=hub]:checked').val()) {
            $('form#hubForm').submit();

        }else{
            alert('Please select rate type to proceed!')
        }
    }
    $('#myModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    var removeValue = function(list, value, separator) {
        separator = separator || ",";
        var values = list.split(separator);
        for(var i = 0 ; i < values.length ; i++) {
            if(values[i] == value) {
                values.splice(i, 1);
                return values.filter(function(v){return v!==''}).join(separator);
            }
        }
        return list;
    }
    $(document).on('click', '.proSuggestions', function(){
        var ele = $(this);
        ele.find('.selector').toggleClass('active');
        var data = $('#pre_cats').val();
        if(ele.find('.selector').hasClass('active')){
            var ar = data.split(',');
            ar.push(ele.data('id'));
            $('#pre_cats').val(ar.filter(function(v){return v!==''}).join(','));
        }else{
            $('#pre_cats').val(removeValue(data, ele.data('id'), ','));
        }
    })
</script>
<?php }else{ ?>
<?php if(empty($hub)){ ?>
<script>
    $(document).ready(function(){
        $('#rateModal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    });
</script>
<?php } } ?>
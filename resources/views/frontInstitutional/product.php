<?php echo $header?>
<section class="at-property-sec at-property-right-sidebar" style="padding: 0;">
    <div class="container" id="<?=$product->id;?>">
        <?php $pvalidity = -1;
        if($product->is_variable){
            date_default_timezone_set("Asia/Kolkata");
            $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
            if($pvalidity < 0){
                $pvalidity = 0;
            }
        } ?>
        <span class="countdown-timer" data-key="<?=$product->id;?>"><?=$pvalidity;?></span>
        <div class="content product-page pull-left" style="width: 100%;">
            <?php if(Session::has('successx')) {
                $msgs = Session::get('successx');
                foreach ($msgs as $msg) {
                    echo '<div class="alert alert-success auto-close">' . $msg . '</div>';
                }
            }
            if(Session::has('msgx')) {
                $msgs = Session::get('msgx');
                foreach ($msgs as $msg) {
                    echo '<div class="alert alert-danger auto-close">' . $msg . '</div>';
                }
            }
            ?>
            <div class="col-md-8">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner text-center">
                        <?php for($i=0; $i<count($images); $i++){
                            if($i == 0){?>
                                <div class="item active" style="padding: 2px;">
                                    <img src="<?=url('/assets/products/'.$images[$i])?>"/>
                                </div>
                            <?php }  else {?>
                                <div class="item" style="padding: 2px;">
                                    <img src="<?=url('/assets/products/'.$images[$i])?>"/>
                                </div>
                            <?php }} ?>
                    </div>
                    <?php if(count($images) > 1){ ?>
                        <ul class="pro-image-slider" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
                            <?php for($i=0; $i<count($images); $i++){
                                if($i == 0){?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active">
                                        <a href="#"><img src="<?=url('/assets/products/thumbs/'.$images[$i])?>"/></a>
                                    </li>
                                <?php } else {?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>">
                                        <a href="#"><img src="<?=url('/assets/products/thumbs/'.$images[$i])?>"/></a>
                                    </li>
                                <?php }} ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-4">
                <a href="<?=url('/products/'.$cat->path)?>" class=""><?=translate($cat->name)?></a>
                <h3 style="font-family: sans-serif; !important; font-size: 15px;"><?=translate($product->title)?></h3>
                <div class="share">
                    <b><?=translate('Share')?> </b>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?=url()->current()?>"><i class="icon-social-facebook"></i></a>
                    <a href="https://twitter.com/intent/tweet/?url=<?=url()->current()?>"><i class="icon-social-twitter"></i></a>
                </div>
                <?php if($product->rating_view != '0'){ ?>
                    <div class="rating">
                        <?php $tr = $rating; $i = 0; while($i<5){ $i++;?>
                            <i class="star<?=($i<=$rating) ? '-selected' : '';?>"></i>
                        <?php $tr--; }?>
                        <b> <?=$total_ratings.' '.translate('Reviews')?> </b>
                    </div>
                <?php } ?>
                <?php
                if(institutional('user_type') == 'institutional'){
                    $customer_id = institutional('id');
                    $pf = getInstitutionalPrice($product->id,$customer_id,$hub, false, false);
                    if(is_array($pf)) {
                        $display_price = $pf['price'];
                        $display_sale = $pf['price'];
                    }else{
                        $display_price = getInstitutionalPriceFancy($product->id,$customer_id, $hub);
                        $display_sale = getInstitutionalPriceFancy($product->id,$customer_id, $hub);
                    }
                }else{
                    $display_price = $product->price;
                    $display_sale = $product->sale;
                }
                $prc = 0.00; ?>
                <?php if($display_sale<$display_price){
                    $prc = $display_sale;
                    ?>
                    <?php if(count($variants) && institutional('rfr') == 0){  ?>
                        <h5 class="price" style="text-transform: none; font-size: 14px;">Starting at <strike ><?=c($display_price)?></strike> <?=c($display_sale)?></b> per unit</h5>
                    <?php }elseif(count($variants) && institutional('rfr') == 1){ ?>
                        <h5 class="price" style="text-transform: none; font-size: 14px;"><?=c($display_price)?></h5>
                    <?php } }else{
                    $prc = $display_price;
                    ?>
                    <?php if(count($variants) && institutional('rfr') == 0){  ?>
                        <h5 class="price" style="text-transform: none;">Starting at <b style="font-size: 20px;"><?=c($display_price)?></b> per unit</h5>
                    <?php }elseif(count($variants) && institutional('rfr') == 1){ ?>
                        <h5 class="price" style="text-transform: none; font-size: 14px;"><?=c($display_price)?></h5>
                    <?php } }  ?>
                <input type="hidden" id="min-qty" value="<?=$product->institutional_min_quantity;?>"/>
                <input type="hidden" id="page-url" value="<?php echo url("/i/rfq_booking"); ?>"/>
                <input type="hidden" id="_token" value="<?php echo csrf_token(); ?>"/>
                <?php
                if($product->institutional_min_quantity > 0){ ?>
                    <div class="alert alert-warning">
                        Minimum Order Quantity is <?=$product->institutional_min_quantity;?>
                    </div>
                <?php } ?>
                <div class="order">
                    <?php if ($product->quantity > 0) { ?>
                        <?php
                        $all_options = json_decode($product->options,true);
                        if(!empty($all_options)){
                            ?>
                            <form class="options" style="background:rgb(249, 250, 252)">
                                <?php
                                foreach($all_options as $i=>$row){
                                    $type = $row['type'];
                                    $name = $row['name'];
                                    $title = $row['title'];
                                    $option = $row['option'];
                                    ?>
                                    <div class="option">
                                        <h6><?php echo $title.' :';?></h6>
                                        <?php if($type == 'radio'){ ?>
                                            <div class="custom_radio">
                                                <?php $i=1;
                                                foreach ($option as $op) { ?>
                                                    <label for="<?php echo 'radio_'.$i; ?>" style="display: block;"><input type="radio" name="<?php echo $name;?>" value="<?php echo $op;?>" id="<?php echo 'radio_'.$i; ?>"><?php echo $op;?></label>
                                                <?php $i++; } ?>
                                            </div>
                                        <?php } else if($type == 'text'){ ?>
                                            <textarea class="form-control" rows="2" style="width:100%" name="<?php echo $name;?>"></textarea>
                                        <?php } else if($type == 'select'){ ?>
                                            <select name="<?php echo $name; ?>" class="form-control" type="text">
                                                <option value=""><?php echo translate('Choose one'); ?></option>
                                                <?php foreach ($option as $op) { ?>
                                                    <option value="<?php echo $op; ?>" ><?php echo $op; ?></option>
                                                <?php } ?>
                                            </select>
                                        <?php } else if($type == 'multi_select') {
                                            $j=1;
                                            foreach ($option as $op){ ?>
                                                <label for="<?php echo 'check_'.$j; ?>" style="display: block;">
                                                    <input type="checkbox" id="<?php echo 'check_'.$j; ?>" name="<?php echo $name;?>[]" value="<?php echo $op;?>">
                                                    <?php echo $op;?>
                                                </label>
                                            <?php $j++; }
                                        } ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </form>
                            <?php
                        }
                        ?>
                        <?php if(count($bulk_discounts)){
                            if(institutional('user_type') !== ''){
                                if(institutional('user_type') !== 'institutional') {
                                    ?>
                                    <div class="table-responsive" >
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr class="bg-blue">
                                                <th colspan="3" class="text-center text-success">Buy More Save More</th>
                                            </tr>
                                            <tr class="bg-blue">
                                                <th class="text-center">Quantity</th>
                                                <th class="text-center">Extra Discount<br>(per unit)</th>
                                                <th class="text-center">You Save</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($bulk_discounts as $bd){ $saving = 0.00; ?>
                                                <tr>
                                                    <td>
                                                        <?=$bd->quantity; ?> or more
                                                    </td>
                                                    <td class="text-right">
                                                        <?php
                                                        if($bd->discount_type == 'Flat'){
                                                            echo c($bd->discount);
                                                            $saving = $bd->quantity*$bd->discount;
                                                        }else{
                                                            $dis = number_format(($bd->discount*$prc)/100,2,'.', '');
                                                            echo c(($prc-$dis));
                                                            $saving = $bd->quantity*($prc-$dis);
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?=c($saving); ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } } } ?>
                        <?php if(count($variants) && institutional('user_type') !== 'institutional'){ ?>
                            <div class="alert alert-info">
                                Buy 5 Tonnes & above. Get special price ! Call Now on 06263956550
                            </div>
                    <?php } } else { ?>
                        <p>Quantity unavailable</p>
                    <?php } ?>
                </div>
                <div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if(institutional('credit_charges') != 0 || !empty(institutional('credit_charges'))){ ?>
                            <div style="padding-top: 15px; border: 1px #ddd solid; padding-left: 10px;">
                                <div class="form-group">
                                    <span >Select Credit Charges</span>
                                </div>
                                <?php foreach ($creditCharges as $credit){ ?>
                                    <div class="form-group">
                                        <input class="form-check-input" type="radio" name="credit" id="credit<?php echo $credit->id;?>" value="<?php echo $credit->id;?>">
                                        <label class="form-check-label" for="credit">
                                            <?php echo $credit->amount; ?> per <span class="setUnit"><?php echo getUnitSymbol($product->weight_unit);?></span> for <?php echo $credit->days; ?>
                                        </label>
                                    </div>
                                <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                </div>

            <div class="row">
                <div class="col-lg-12">
                    <?php if(count($variants)){ ?>
                        <div class="price-box" id="price-box">
                            <table class="price-table" style="margin-bottom: 15px;">
                                <form method="post" id="directForm">
                                    <input type="hidden" name="type" id="methodType" value="bybasic">
                                    <input type="hidden" name="price" id="displayPrice" value="<?php echo $display_price; ?>">
                                    <input type="hidden" name="unit" id="weightUnit" value="<?php echo getUnitSymbol($product->weight_unit); ?>">
                                    <input type="hidden" name="unit" id="without_variants" value="1">
                                    <tr>
                                        <td class="text-left">Unit Price</td>
                                        <?php if(institutional('user_type') == 'institutional') { ?>
                                        <td class="text-right"><b style="font-size: 18px;" class="dynamic" id="sale_price"><?=c($display_price)?></b></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="quantity-select" style="width: 45%;">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="col-lg-1">
                                                                    <a class="dec reasevar btn btn-sm btn-primary" onclick="decrement('<?php echo $product->id; ?>')">-</a>
                                                                </div>
                                                                <div class="col-lg-4" style="margin-left: -14px; margin-right: 9px">
                                                                    <input name="quantity" class="quantityvar pridvalue<?php echo $product->id; ?>" data-product="<?php echo $product->id; ?>" value="<?php echo $product->institutional_min_quantity; ?>" style="width: 100% !important;">
                                                                </div>
                                                                <div class="col-lg-1">
                                                                    <a class="inc reasevar btn btn-sm btn-primary" onclick="increment('<?php echo $product->id; ?>')">+</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </form>
                            </table>
                            <?php if(institutional('user_type') !== 'institutional') { ?>
                                <p class="final-price dynamic">
                                    <?= c($display_sale * ($product->institutional_min_quantity != 0 ? $product->institutional_min_quantity : 1)); ?>
                                </p>

                                <?php
                            }
                            if(count($bulk_discounts) || institutional('user_type') !== 'institutional'){
                                if(institutional('user_type') !== '' && $display_price != 0){ ?>
                                    <p class="final-discount dynamic">You save : <?=c(($display_price - $display_sale)*($product->institutional_min_quantity != 0 ? $product->institutional_min_quantity : 1)); ?> (<?=round((($display_price - $display_sale)/$display_price)*100,2);?>%)</p>
                                <?php } } ?>
                            <div id="error" style="display: none;"></div>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="text-center">
                                        <?php if(institutional('id') !== ''){
                                            $user_id = institutional('id');
                                            $product_id = $product->id;

                                            $img = \App\ProductWishlist::where('user_id' ,$user_id)->where('product_id',$product_id)->first();
                                            if(institutional('user_type') == 'institutional' && count($variants)){ ?>

                                                <button class="btn btn-primary book-now-direct" data-id="<?=$product->id?>" style="width: 100%;margin-bottom: 10px;"><i class="icon-basket"></i> <?=translate('book now')?></button>
                                                <?php
                                            } } ?>
                                        <!--if(!empty($img)){
                                         ?>--><!--
                                                <button class="btn btn-primary likebutton" id="likebutton" style="width: 100%;"><span class="tickgreen">✔</span> Added to Project</button>
                                            <?php /*} else{ */?>
                                                <input type="hidden" id="proid1" value="<?/*=$product_id;*/?>">
                                                <button class="btn btn-primary likebutton" id="likebutton" onclick="productWishlist(this.value);" style="width: 100%;"><i class="fa fa-heart"></i> Add to Project</button>
                                            <?php /*}} else { */?>
                                            <button class="btn btn-primary" onClick="getModel()" style="width: 100%; display: none;"><i class="fa fa-heart"></i> Add to Project</button>
                                        --><?php /*} */?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if(institutional('user_type') == 'institutional' && count($variants)){ ?>
                        <div class="col-md-4" id="productDiv">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <button style="width: 100%;" class="btn btn-primary pull-right" id="showtable" data-id="<?=$product->id?>"><i class="icon-eye"></i> <?=translate('Show Section')?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-md-4" id="productDiv">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button style="width: 100%;" class="btn btn-primary  pull-right" id="request_quotation" data-id="<?=$product->id?>"><i class="icon-notebook"></i> <?=translate('Request For Quotation')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" id="productDiv">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button style="width: 100%;" class="btn btn-primary  pull-right" id="counter_offer" data-id="<?=$product->id?>"><i class="icon-plus"></i> <?=translate('Counter Offer')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Rfq Booking-->
            <?php if(count($variants)){
                ?>
                <div class="table-responsive variantDiv col-md-12" id="variantDiv" style="margin-top: 20px; display: none;">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr class="bg-blue">
                            <th>Product Name</th>
                            <th>Price (<i class="fa fa-inr"></i> )</th>
                            <th>Unit</th>
                            <th style="min-width: 126px;">Qty</th>
                            <th class="text-right">Total Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <form id="variants-form">
                            <input type="hidden" id="type" name="type" value="bysize">
                            <?php
                            foreach ($variants as $key => $vart){
                                $customer_id ='';
                                if(institutional('id')){
                                    $customer_id = institutional('id');
                                }
                                $price = $product->price + $vart->price;
                                if ($product->sale < $product->price && $product->sale != 0) {
                                    $price = $product->sale + $vart->price;
                                }
                                $vartprice = c($product->price+getSize($vart->price));
                                $price = '';
                                if(is_array(getInstitutionalPrice($product->id,$customer_id,$hub,$vart->id,false))){
                                    $price = getInstitutionalPrice($product->id,$customer_id,$hub,$vart->id,false)['pprice'];
                                }else{
                                    $price = c(is_array(getInstitutionalPrice($product->id,$customer_id,$hub,$vart->id,false)));
                                }
                                echo'<tr>
                                    <td>'.variantTitle($vart->variant_title).'
                                    <input type="hidden" class="prlist" id="prlist'.$vart->id.'" name="prlist[]" value="'.$vart->id.'">
                                    <input type="hidden"  class="prlist" name="prlist[]" value="'.$vart->id.'">
                                    <input type="hidden"  class="rfqunit" name="rfqunit[]" value="'.$vart->unit.'">
                                    </td>';
                                echo '<td>
                                            <input type="hidden" class="mainprice'.$vart->id.'" id="mainprice'.$vart->id.'" value="'.$price.'">
                                        '.$price;

                                echo '</td>';
                                $col = 4;
                                echo '<td><input type="hidden" class="unit'.$vart->id.'" id="unit'.$key.'" name="unit[]" value="'.variantTitleWeight($vart->variant_title).'">'.variantTitleWeight($vart->variant_title).'</td>';
                                $dis = '';
                                if(institutional('rfr')){
                                    $dis = 'disabled';
                                }
                                $qt = $product->institutional_min_quantity != 0 ? $product->institutional_min_quantity : '1';
                                echo '<td style="min-width:126px;">
                                        <a class="btn-sm btn-primary" onclick="decrement('.$vart->id.')">-</a>
                                        <input name="quantity" class="quantityvar pridvalue'.$vart->id.'" onchange="getTotal(this.value,'.$key.','.$vart->id.');" id="quantityvar'.$vart->id.'" data-product="'.$product->id.'" >
                                        <input type="hidden" name="id" class="quantityvarid"  value="'.$vart->id.'" >
                                        <a class="btn btn-sm btn-primary" onclick="increment('.$vart->id.');">+</div>
                                           
                                    </td>
                                    <td class="text-right">
                                        <b class="tp" id="tp-'.$vart->id.'" data-price="0"><input type="hidden" class="tpv" value=""><span class="singlepr'.$vart->id.' price">0.00</span></b>
                                    </td>
                                </tr>';
                            }
                            echo '<tr>
                                <td colspan="'.$col.'" class="text-right">Total Basic Amount:</td>
                                <td class="text-right"><b id="gt">'.(institutional('rfr') == 0 ? c('0.00') : 'Rate not available').'</b></td>
                            </tr>';
//                            echo '<tr>
//                                <td colspan="'.$col.'" class="text-right">Total Market Price:</td>
//                                <td class="text-right"><strike id="mp" style="font-size: 14px; font-weight: 700;">' . (customer('rfr') == 0 ? c('0.00') : 'Rate not available') . '</strike></td>
//                            </tr>';
//                            echo '<tr>
//                                <td colspan="'.$col.'" class="text-right">Your Savings:</td>
//                                <td class="text-right"><b id="savings" style="color: green;">' . (customer('rfr') == 0 ? c('0.00') : 'Rate not available') . '</b></td>
//                            </tr>';
                            echo '<tr>
                                <td colspan="'.$col.'" class="text-right">Total Size Difference:</td>
                                <td class="text-right"><b id="difference">' . (institutional('rfr') == 0 ? c('0.00') : 'Rate not available') . '</b></td>
                            </tr>';
                            echo '<tr>
                                <td colspan="'.$col.'" class="text-right">Total Loading Amount:</td>
                                <td class="text-right"><b id="loading">' . (institutional('rfr') == 0 ? c('0.00') : 'Rate not available') . '</b></td>
                            </tr>';
                            echo '<tr>
                                <td colspan="'.$col.'" class="text-right">Total Tax Amount:</td>
                                <td class="text-right"><b id="tax">' . (institutional('rfr') == 0 ? c('0.00') : 'Rate not available') . '</b></td>
                            </tr>';
                            if(institutional('credit_charges') != 0){
                                echo '<tr>
                                    <td colspan="' . $col . '" class="text-right" id="creditText">Credit Charges:</td>
                                    <td class="text-right"><b id="credit">' . (institutional('rfr') == 0 ? c('0.00') : 'Rate not available') . '</b></td>
                                </tr>';
                            }
                            ?>
                        </form>
                        </tbody>
                    </table>
                    <div class="order">
                        <div class="col-md-8">
                            <div class="error" style="display:none;"></div>
                        </div>
                        <div class="col-md-4">
                            <?php //if(customer('rfr') == 0){ ?>
                                <!--div id="error" style="display: none;"></div>
                                <button style="width: 100%;" class="add-cart-variant btn btn-primary pull-right" id="added" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Add to cart')?><span style="display: none;" class ="addd">Added To Cart</span></button-->
                            <?php //}else{ ?>
                                <div id="error" style="display: none;"></div>
                                <button style="width: 100%;" class="book-now-button btn btn-primary pull-right" data-id="<?=$product->id?>"> <?=translate('Book Now')?></button>
                            <?php //} ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!--End Rfq Booking-->
        </div>

        <div class="clearfix"></div>
        <div class="panel-group" id="accordion">
            <?php
            if($product->description_view != '0') {
                if ($product->text != '') { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               style="color: #ffffff;"><h4 class="panel-title">
                                    Description
                                </h4></a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p><?= $product->text ?></p>
                            </div>
                        </div>
                    </div>
                <?php }
            }?>
            <?php
            if($product->specification_view != '0'){
                if($product->specification != '') { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="color: #ffffff;">
                                <h4 class="panel-title">
                                    Specification
                                </h4></a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p><?= $product->specification ?></p>
                            </div>
                        </div>
                    </div>
                    <?php
                }} ?>
            <?php if($product->rating_view != '0'){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" style="color: #ffffff;"><h4 class="panel-title">
                                Ratings & Reviews
                            </h4></a>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="feedback-container">
                                <div class="col-lg-12">
                                    <div class="col-lg-4">
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style="margin-bottom:0px; font-family: Sans-Serif; font-style:bold;">Avarage rating <?=$avg_rating;?></h5>
                                                <div class="rating" style="padding-bottom: 10px;">
                                                    <?php
                                                    $a=$avg_rating;
                                                    $b = explode('.',$a);
                                                    $first = $b[0];
                                                    $rr = $avg_rating;
                                                    $i = 0;
                                                    while($i<5){ $i++;?>
                                                        <i class="star<?=($i<=$avg_rating) ? '-selected' : '';?>"></i>
                                                        <?php $rr--; }
                                                    echo '</div>';
                                                    ?>
                                                    <h5 style=" font-family: Sans-Serif; font-style: bold;">Total rating <?=$total_user;?></h5>
                                            </li>
                                        </ul>
                                        <hr>
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style=" font-family: Sans-Serif; font-style: bold;">Total comment <?=$total_reviews; ?></h5>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-8 reviews-graph">
                                        <ul>
                                            <li class="float-left">
                                                <?php
                                                if($rating5){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">5 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">

                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating5; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating5; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating5;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating5;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating5;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating4 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">4 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating4; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating4; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating4;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating4;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating4;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating3 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">3 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating3; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating3; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating3;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating3;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating3;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating2 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">2 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating2; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating2; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating2;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating2;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating2;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating1 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">1 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating1; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating1;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating1;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating1;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12" style="padding-bottom: 10px;">
                                <form action="<?php echo url('review');?>" method="post" class="form-horizontal single">
                                    <?=csrf_field();?>

                                    <div id="response" style="float: left; width: 100%"></div>
                                    <?php if(institutional('id') !== ''){?>
                                        <h5 style="float: left; width: 100%; margin-top: 10px; border-top: 1px solid #000000; padding-top: 20px;"><?=translate('Add a review')?> :</h5>


                                        <fieldset style="float: left; width: 100%;">
                                            <div class="col-md-12">
                                                <input name="name" readonly value="<?=institutional('id')?>" class="form-control" type="hidden">
                                                <input name="product" value="<?=$product->id?>" class="form-control" type="hidden">

                                                <div class="form-group col-md-12">
                                                    <label class="control-label"><?=translate('Rating')?></label>
                                                    <div id="star-rating">
                                                        <input type="radio" name="rating" class="rating" value="1" />
                                                        <input type="radio" name="rating" class="rating" value="2" />
                                                        <input type="radio" name="rating" class="rating" value="3" />
                                                        <input type="radio" name="rating" class="rating" value="4" />
                                                        <input type="radio" name="rating" class="rating" value="5" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label"><?=translate('Review')?></label>
                                                    <textarea name="review" type="text" rows="5" class="form-control"></textarea>
                                                </div>
                                                <button data-product="<?=$product->id?>" name="submit" class="btn btn-primary" ><?=translate('submit')?></button>
                                            </div>
                                        </fieldset>
                                    <?php } else { ?>
                                        <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                            <div class="col-md-12 text-center my">
                                                <button class="btn btn-primary" onClick="getModel()" type="button" value="Login">Login To Review</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </form>
                                <br><br><br>
                                <p>
                                    <?php
                                    $count = 0;
                                    foreach($reviews as $review){
                                        $usid = $review->name;
                                        $uimg = \App\Customer::where('id',$usid)->first();
                                        echo '<img class="review-image" src="assets/user_image/'; if($uimg->image != ''){ echo $uimg->image; } else { echo 'user.png'; }; echo'">
											<div class="review-meta"><b>'.$uimg->name.'</b><br/>
											<span class="time">'.date('M d, Y',$review->time).'</span><br/></div>
											<div class="review">
												<div class="rating pull-right">';
                                        $rr = $review->rating; $i = 0; while($i<5){ $i++;?>
                                            <i class="star<?=($i<=$review->rating) ? '-selected' : '';?>"></i>
                                            <?php $rr--; }
                                        echo '</div>
											<div class="clearfix"></div>
											<p>'.nl2br($review->review).'</p></div>';
                                        $count++;
                                    }
                                    if($count > 0){
                                        echo '<hr/>';
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if($product->faq_view != '0'){?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" style="color: #ffffff;"><h4 class="panel-title">
                            FAQ
                        </h4></a>
                </div>
                <div id="collapseFour" class="panel-collapse collapse">
                    <div class="panel-body">
                        <section class="at-property-sec at-property-right-sidebar">
                            <div class="">
                                <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                    <div class="col-md-12 text-center">
                                        <?php if(institutional('id') !== ''){ ?>
                                            <button class="btn btn-primary" type="button" data-popup-open="popup-question" value="Ask a Question">Ask a Question</button>
                                        <?php } else { ?>
                                            <button class="btn btn-primary" onClick="getModel()" type="button" value="Ask a Question">Ask a Question</button>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul>
                                            <?php foreach($faqs as $faq){?>
                                                <li>
                                                    <!--<input type="checkbox" checked>
                                                    <i></i>-->
                                                    <h5>Q : <?php echo $faq->question; ?><br><small>
                                                            by Admin on <?php $d = $faq->time; echo date('d M, Y', strtotime($d)); ?></small></h5>
                                                    <p>A : <?php echo $faq->answer; ?></p>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!-- Ask a Question Popup Start -->
                        <div class="popup" id="popup-question" data-popup="popup-question">
                            <div class="popup-inner">
                                <div class="col-md-12">
                                    <h2 style="text-transform: none;" class="text-center">Ask a Question</h2>
                                    <form id="contact_form" action="<?php echo url('product-faq');?>" method="post">
                                        <?=csrf_field() ?>
                                        <input type="hidden" name="product_id" value="<?=$product->id;?>" />
                                        <div class="col-md-12 col-sm-12 text-center">
                                            <textarea class="form-control" name="question" rows="5" placeholder="Write question here" required="required"></textarea>
                                            <br>
                                            <button class="btn btn-primary" type="submit">SUBMIT</button>
                                        </div>
                                    </form>
                                </div>
                                <a class="popup-close" data-popup-close="popup-question" href="#">x</a>
                            </div>
                        </div>
                        <!-- Ask a Question Popup End -->

                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
</section>

<?php if($product->fb_cat !== '' && institutional('user_type') !== 'institutional'){
    $fb_cats = explode(',', $product->fb_cat);

    ?>
    <section>
        <div class="container">
            <div class="box clearfix box-with-products">
                <!-- Carousel nav -->
                <a class="next" href="#myCarousel0" id="myCarousel0_next"><span></span></a>
                <a class="prev" href="#myCarousel0" id="myCarousel0_prev"><span></span></a>

                <div class="box-heading">Frequently Bought Together</div>
                <div class="strip-line"></div>

                <div class="box-content products" style="margin: 20px 0;">
                    <div class="box-product">
                        <div id="myCarousel0" class="owl-carousel">
                            <?php
                            foreach($fb_cats as $fb_cat){
                                $pro = null;
                                $pro = getProduct($fb_cat, $product->id);
                                if($pro !== null){

                                    ?>
                                    <div class="item">
                                        <div class="at-property-item at-col-default-mar" id="<?=$pro->id;?>">
                                            <div class="at-property-img">
                                                <a href="product/<?=path($pro->title,$pro->id)?>" data-title="<?=translate($pro->title)?>">
                                                    <img src="<?=url('/assets/products/'.image_order($pro->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                                </a>
                                                <div class=""></div>
                                            </div>
                                            <div class="at-property-location">
                                                <div class="text-ellipsis">
                                                    <a href="product/<?=path($pro->title,$pro->id)?>" data-title="<?=translate($pro->title)?>"><?=translate($pro->title)?></a>
                                                </div>
                                                <span class="price"><?=(institutional('user_type') == 'institutional' ? c($pro->institutional_price) : c($pro->price))?></span>
                                                <div class="rating">

                                                    <?php
                                                    $rates = getRatings($pro->id);
                                                    $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                        <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                        <?php $tr--; }?>
                                                    (<?=$rates['total_ratings']?>)
                                                </div>
                                                </p>
                                                <div class="cart-btn-custom" style="padding-top: 10px;">
                                                    <button class="bg" data-redirect="product/<?=path($pro->title, $pro->id); ?>" data-title="<?=translate($pro->title) ?>"><i class="icon-basket"></i> Add to Cart</button>
                                                    <div style="margin-top: -20px;">&nbsp;</div>
                                                    <?php
                                                    if(institutional('id') !== ''){
                                                        $user_id = institutional('id');
                                                        $product_id = $product->id;

                                                        $img = \App\ProductWishlist::where('user_id',$user_id)->where('product_id',$product_id)->get();

                                                        if(!empty($img)){

                                                            echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                        } else{
                                                            echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                            <button class="bg likebutton1" id="likebutton"><i class="fa fa-heart"></i> Add to Project</button>';

                                                        }} else {
                                                        echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



<?php } if(!empty($related_products)){
    $customer_id ='';
    if(institutional('id')){
        $customer_id = institutional('sid');
    }
    $pc = false;
    foreach($related_products as $rp){
        $p = getInstitutionalPriceFancy($rp->id,$customer_id,$hub);
        if($p != "NA"){
            $pc = true;
            break;
        }
    }
    if($pc){
        ?>
        <section>
            <div class="container">
                <div class="box clearfix box-with-products">
                    <!-- Carousel nav -->
                    <a class="next" href="#myCarousel" id="myCarousel_next"><span></span></a>
                    <a class="prev" href="#myCarousel" id="myCarousel_prev"><span></span></a>

                    <div class="box-heading">Related products</div>
                    <div class="strip-line"></div>

                    <div class="box-content products" style="margin: 20px 0;">
                        <div class="box-product">
                            <div id="myCarousel" class="owl-carousel">
                                <?php
                                foreach($related_products as $rel){
                                    $customer_id ='';
                                    if(institutional('id')){
                                        $customer_id = institutional('sid');
                                    }
                                    $pr = getInstitutionalPriceFancy($rel->id,$customer_id,$hub);
                                    if($pr != "NA"){
                                        ?>
                                        <div class="item">
                                            <div class="at-property-item at-col-default-mar" id="<?=$rel->id;?>">
                                                <div class="at-property-img">
                                                    <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>">
                                                        <img src="<?=url('/assets/products/'.image_order($rel->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                                    </a>
                                                    <div class=""></div>
                                                </div>
                                                <div class="at-property-location">
                                                    <div class="text-ellipsis">
                                                        <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>"><?=translate($rel->title)?></a>
                                                    </div>
                                                    <span class="price"><?=(institutional('user_type') == 'institutional' ? c($rel->institutional_price) : c($rel->price))?></span>
                                                    <div class="rating">

                                                        <?php
                                                        $rates = getRatings($rel->id);
                                                        $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                            <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                            <?php $tr--; }?>
                                                        (<?=$rates['total_ratings']?>)
                                                    </div>
                                                    </p>
                                                    <div class="cart-btn-custom" style="padding-top: 10px;">
                                                        <button class="bg" data-redirect="product/<?=path($rel->title, $rel->id) ?>" data-title="<?=translate($rel->title) ?>"><i class="icon-basket"></i> Add to Cart</button>
                                                        <div style="margin-top: -20px;">&nbsp;</div>
                                                        <?php

                                                        if(institutional('id') !== '' || !empty(institutional('id'))){
                                                            $user_id = institutional('id');
                                                            $product_id = $product->id;

                                                            $img = \App\ProductWishlist::where('user_id',$user_id)->where('product_id',$product_id)->get();
                                                            if(!empty($img)){

                                                                echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                            } else{
                                                                echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                            <button class="bg likebutton2" id="likebutton"><i class="fa fa-heart"></i> Add to Project</button>';

                                                            }} else {
                                                            echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } }
if($recent_viewed){
    ?>
    <section>
        <div class="container">
            <div class="box clearfix box-with-products">
                <!-- Carousel nav -->
                <a class="next" href="#myCarousel1" id="myCarousel1_next"><span></span></a>
                <a class="prev" href="#myCarousel1" id="myCarousel1_prev"><span></span></a>

                <div class="box-heading">Recently Viewed</div>
                <div class="strip-line"></div>

                <div class="box-content products" style="margin: 20px 0;">
                    <div class="box-product">
                        <div id="myCarousel1" class="owl-carousel">
                            <?php

                            foreach($recent_viewed as $rel){
                                $customer_id ='';
                                if(institutional('id')){
                                    $customer_id = institutional('sid');
                                }
                                if(is_bool(getInstitutionalPrice($rel->id, $customer_id,$hub,false,false))) {
                                    $prv = c(getInstitutionalPrice($rel->id, $customer_id, $hub, false, false));
                                }else{
                                    $prv = c(getInstitutionalPrice($rel->id, $customer_id,$hub,false,false)['pprice']); }

                                ?>
                                <div class="item">
                                    <div class="at-property-item at-col-default-mar" id="<?=$rel->id;?>">
                                        <div class="at-property-img">
                                            <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>">
                                                <img src="<?=url('/assets/products/'.image_order($rel->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                            </a>
                                            <div class=""></div>
                                        </div>
                                        <div class="at-property-location">
                                            <div class="text-ellipsis">
                                                <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>"><?=translate($rel->title)?></a>
                                            </div>
                                            <span class="price"><?=(institutional('user_type') == 'institutional' ? c($prv) : c($rel->price))?></span>
                                            <div class="rating">

                                                <?php
                                                $rates = getRatings($rel->id);
                                                $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                    <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                    <?php $tr--; }?>
                                                (<?=$rates['total_ratings']?>)
                                            </div>
                                            </p>
                                            <div class="cart-btn-custom" style="padding-top: 10px;">
                                                <button class="bg" data-redirect="product/<?=path($rel->title, $rel->id) ?>" data-title="<?=translate($rel->title) ?>"><i class="icon-basket"></i> Add to Cart</button>
                                                <div style="margin-top: -20px;">&nbsp;</div>
                                                <?php
                                                if(institutional('id') !== ''){
                                                    $user_id = institutional('id');
                                                    $product_id = $product->id;
                                                    $img = \App\ProductWishlist::where('user_id',$user_id)->where('product_id',$product_id)->get();

                                                    if(!empty($img)){

                                                        echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                    } else{
                                                        echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                        <button class="bg likebutton3" id="likebutton" ><i class="fa fa-heart"></i> Add to Project</button>';

                                                    }} else {
                                                    echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php } ?>
<?php if(!empty($link)){?>
    <section>
        <div class="container">
            <div class="row">
                <div class="blockquote blockquote--style-1">
                    <div class="row inner-div">
                        <div class="col-md-12">
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-4">
                                <img src="<?=url('/assets/products/'.image_order($link->image))?>" class="cont-image"/>
                            </div>
                            <div class="col-md-7 col-lg-7 col-sm-7 col-xs-8" style="padding: 25px 0 0;">
                                <h3 class="cont-content"><?=$link->content; ?></h3>
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 text-center" style="padding:15px 0 0 0">
                                <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                    GET STARTED
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<button class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login">Get Started !</button>
<div class="popup" id="popup-login" data-popup="popup-login">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-6 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                <?=csrf_field() ?>
                <fieldset>
                    <div class="form-group">
                        <label class="control-label"><?=translate('E-mail') ?></label>
                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?=translate('Password') ?></label>
                        <input name="password" type="password" class="form-control"  />
                    </div>
                    <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                </fieldset>
            </form>
            <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
        </div>
        <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
    </div>
</div>

<!-- Services Popup End -->
<div id="asrModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h4 class="modal-title">Request Quotation</h4>
            </div>
            <div id="rfq-error" style="display: none;"></div>
            <div class="modal-body">
                <!--<button style="width: 50%;" class="byBaseModal btn btn-primary  pull-left active" data-id="<?/*=$product->id*/?>"><?/*=translate('Basic')*/?></button>
                <button style="width: 50%;" class="byVarModal btn btn-primary  pull-left" data-id="<?/*=$product->id*/?>"><?/*=translate('Size')*/?></button>-->
                <?php if(institutional('user_type') == 'institutional' && count($variants)){ ?>
                    <div class="col-md-12" id="productDiv">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button style="width: 100%;" class="btn btn-primary pull-right" id="showreqtable" data-id="<?=$product->id?>"><i class="icon-eye"></i> <?=translate('Show Section')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <br>

                <div id="baseForm" style="padding-left: 10px; padding-right: 10px;">
                    <?php
                    $basecustomer_id ='';
                    if(institutional('id')){
                        $basecustomer_id = institutional('id');
                    }?>
                        <input type="hidden" id="base_type" name="type" value="bybasic">
                        <input type="hidden" id="base_user_id" name="type" value="<?php echo $basecustomer_id;?>">
                        <input type="hidden" name="category_id" id="category_id" value="<?=$cat->id;?>"/>
                        <input type="hidden" name="hub_id" id="hub_id" value="<?=$hub;?>"/>
                        <?php foreach ($units as $unit){
                              if($product->weight_unit == $unit->id) {?>
                                  <input type="hidden" name="base_unit" id="base_unit" value="<?php echo $unit->symbol;?>"/>
                        <?php } }?>
                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group new">
                                <h6 style="font-family: sans-serif; !important; font-size: 20px; margin-top: 40px; padding-left: 10px;"><?=translate($product->title)?></h6>
                            </div>
                            <div class="form-group baseQuantity pull-right qun but qtycss" style="margin-top: -40px; padding-right: 10px; padding: 14px;">
                                <div class="col-lg-12">
                                    <div class="col-lg-1">
                                        <a class="dec reasevar btn btn-sm btn-primary" onclick="decrementrfqd('<?php echo $product->id; ?>')">-</a>
                                    </div>
                                    <div class="col-lg-4" style="margin-left: -14px; margin-right: 9px">
                                        <input name="quantity" class="quantityvar pridvaluerfqd<?php echo $product->id; ?>" data-product="<?php echo $product->id; ?>" value="<?php echo $product->institutional_min_quantity; ?>" style="width: 100% !important;">
                                    </div>
                                    <div class="col-lg-1" style="margin-left: -27px;">
                                        <a class="inc reasevar btn btn-sm btn-primary" onclick="incrementrfqd('<?php echo $product->id; ?>')">+</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label style="color:#000;" for="sel1">Please Select Product:</label>
                                <select name="brands_base[]" class="form-control select2 " id="brands_base" multiple>
                                    <?php foreach($related_products as $brand){ ?>
                                        <option value="<?=$brand->id?>"><?=$brand->title?></option>
                                    <?php } ?>
                                </select>

                                <br>
                                <label style="color:#000;" for="sel2">Please Select Payment Option::</label>
                                <select name="payment_base" class="form-control" id="payment_base">
                                    <option>Select Payment Option</option>
                                    <option>next day</option>
                                    <option>15 days</option>
                                    <option>30 days</option>
                                    <option>more than 30 days</option>
                                    <option>Advanced Payment</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label style="color:#000;" for="usr">Location(optional):</label>
                                <input type="text" class="form-control" id="location_base" name="location">
                            </div>
                            <center>
                                <input name="base_quote"  value="Request Quotation" data-id="<?=$product->id?>" class="btn btn-primary btn-sm request-quatation-direct"/>
                            </center>

                        </div>
                </div>
                <div id="sizeForm" style="display: none;padding-left: 10px; padding-right: 10px;">
                        <?php $col = 6;?>
                        <div class="row" style="margin-top: 10px;">
                            <div class="table-responsive  col-md-12" style="margin-top: 20px;">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr class="bg-blue">
                                        <th>Product Name</th>
                                        <th>Price (<i class="fa fa-inr"></i> )</th>
                                        <th>Unit</th>
                                        <th style="min-width: 126px;">Qty</th>
                                        <th class="text-right">Total Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <form id="variants-form">
                                        <input type="hidden" id="rfq-type" name="type" value="bysize">
                                        <input type="hidden" id="rfq-min-qty" value="<?=$product->institutional_min_quantity;?>"/>
                                        <input type="hidden" id="rfq-page-url" value="<?php echo url("/i/request-quotation"); ?>"/>
                                        <input type="hidden" id="_token" value="<?php echo csrf_token(); ?>"/>
                                        <input type="hidden" id="cat_id" name="category_id" value="<?php echo $product->category; ?>">
                                        <?php
                                        foreach ($variants as $key => $vart){
                                            $customer_id ='';
                                            if(institutional('id')){
                                                $customer_id = institutional('id');
                                            }
                                            $price = $product->price + $vart->price;
                                            if ($product->sale < $product->price && $product->sale != 0) {
                                                $price = $product->sale + $vart->price;
                                            }
                                            $vartprice = c($product->price+getSize($vart->price));
                                            $price = '';
                                            if(is_array(getInstitutionalPrice($product->id,$customer_id,$hub,$vart->id,false))){
                                                $price = getInstitutionalPrice($product->id,$customer_id,$hub,$vart->id,false)['pprice'];
                                            }else{
                                                $price = c(is_array(getInstitutionalPrice($product->id,$customer_id,$hub,$vart->id,false)));
                                            }
                                            echo'<tr>
                                    <td>'.variantTitle($vart->variant_title).'
                                    <input type="hidden" class="rfqprlist" id="rfqprlist" name="prlist[]" value="'.$vart->id.'">
                                     <input type="hidden" id="user_id" value="'.$customer_id.'"/>
                                    <input type="hidden"  class="rfqprlist" name="prlist[]" value="'.$vart->id.'">
                                    <input type="hidden"  class="rfqprlist" name="unit[]" value="'.$vart->unit.'">
                                    <input type="hidden"  class="rfqtitle"  name="title[]" value="'.variantTitle($vart->variant_title).'">
                                    </td>';
                                            echo '<td>
                                            <input type="hidden" class="rfqmainprice'.$vart->id.'" id="rfqmainprice'.$vart->id.'" value="'.$price.'">
                                        '.$price;

                                            echo '</td>';
                                            $col = 4;
                                            echo '<td><input type="hidden" class="rfqunit'.$vart->id.'" id="unit'.$key.'" name="rfqunits[]" value="'.variantTitleWeight($vart->variant_title).'">'.variantTitleWeight($vart->variant_title).'</td>';
                                            $dis = '';
                                            if(institutional('rfr')){
                                                $dis = 'disabled';
                                            }
                                            $qt = $product->institutional_min_quantity != 0 ? $product->institutional_min_quantity : '1';
                                            echo '<td style="min-width:126px;">
                                        <div class=" buttons_added_'.$key.' vaributton">
                                            <div class="form-group baseQuantity but qtycss">
                                                <div class="dec reasevar btn btn-sm btn-primary" onclick="decrementrfq('.$vart->id.')">-</div>
                                                <input name="quantity" class="quantityvar rfqpridvalue'.$vart->id.'" onchange="getRfqTotal(this.value,'.$key.','.$vart->id.');" id="quantityvar'.$vart->id.'" data-product="'.$product->id.'" >
                                                <input type="hidden" name="id" class="quantityvarid"  value="'.$vart->id.'" >
                                                <div class="inc reasevar btn btn-sm btn-primary" onclick="incrementrfq('.$vart->id.');">+</div>
                                            </div>
                                        </div>
                                        <!--button type="button" style="display: none" name="add-to-cart" class="single_add_to_cart_button_'.$key.' button alt" style="display: block !important">Add+</button-->   
                                    </td>
                                    <td class="text-right">
                                        <b class="rfqtp" id="rfqtp-'.$vart->id.'" data-price="0"><input type="hidden" class="rfqtpv" value=""><span class="rfqsinglepr'.$vart->id.' rfqprice">0.00</span></b>
                                    </td>
                                </tr>';
                                        }
                                        echo '<tr>
                                <td colspan="'.$col.'" class="text-right">Total Amount:</td>
                                <td class="text-right"><b id="rfqgt">'.(institutional('rfr') == 0 ? c('0.00') : 'Rate not available').'</b></td>
                            </tr>';
                                        ?>
                                    </form>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <label style="color:#000;" for="brand_id">Please Select Product:</label>
                                    <select name="brands_base[]" class="form-control select2" id="brand_id" multiple>
                                        <?php foreach($related_products as $brand){ ?>
                                            <option value="<?=$brand->id?>"><?=$brand->title?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label style="color:#000;" for="payment_base">Please Select Payment Option::</label>
                                    <select name="payment_base" class="form-control" id="payment_base">
                                        <option>Select Payment Option</option>
                                        <option>next day</option>
                                        <option>15 days</option>
                                        <option>30 days</option>
                                        <option>more than 30 days</option>
                                        <option>Advanced Payment</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label style="color:#000;" for="location">Location(optional):</label>
                                    <input type="text" class="form-control" id="location" name="location">
                                </div>
                                <center>
                                    <input name="bysize_quote"  value="Request Quotation"  data-id="<?=$product->id?>" class="btn btn-primary btn-sm request-quatation-button"/>
                                </center>
                            </div>
                        </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>

<div id="rfrModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h4 class="modal-title">Request For Rate</h4>
            </div>
            <div class="modal-body">
                <form action="<?=url('send-rfr')?>" method="post" id="rfrForm" enctype="multipart/form-data">
                    <?=csrf_field()?>
                    <fieldset>
                        <div class="form-group">
                            <label class="control-label">Product (*)</label>
                            <input type="hidden" name="pid" value="<?=$product->id?>"/>
                            <select name="product" class="form-control" required disabled>
                                <option value="<?=$product->id?>" selected><?=$product->title?></option>
                            </select>
                        </div>
                        <?php if(count($variants)){ ?>
                            <div class="form-group">
                                <label class="control-label">Variants (*)</label>
                                <select name="variants[]" class="form-control select2 variantsx" multiple required>
                                    <?php foreach($variants as $var){ ?>
                                        <option value="<?=$var->id?>"><?=getVname($var->variant_title)?></option>
                                    <?php } ?>
                                </select>
                                <div class="alert alert-danger error" style="display: none"></div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="control-label">Enquiry (*)</label>
                            <textarea name="msg" rows="5" class="form-control" required></textarea>
                            <div class="alert alert-danger error" style="display: none"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Attach File (Optional)</label>
                            <input type="file" class="form-control" name="image">
                        </div>

                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitRfr();">Send</button>
            </div>
        </div>
    </div>
</div>
<div id="counterModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h4 class="modal-title">Counter Offer</h4>
            </div>
            <div class="modal-body">
                <form action="<?=url('i/save-counter-offer')?>" method="post" id="counterForm" enctype="multipart/form-data">
                    <?=csrf_field()?>
                    <input type="hidden" name="product_id" value="<?php echo $product->id?>">
                    <input type="hidden" name="credit_charge_id" value="<?php echo institutional('credit_charges'); ?>">
                    <input type="hidden" name="hub_id" id="hubId" value="<?php echo $hub; ?>">
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo institutional('id'); ?>">
                    <fieldset>
                        <div class="form-group">
                            <label class="control-label"><?php echo $product->title?></label>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Price</label>
                            <input type="text" name="price" placeholder="Enter Your Price" required />
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-1">
                            <div class="form-group">
                            <label>Quantity</label>
                            <input type="text" name="counterqty"  size="5" required>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-1">
                            <div class="form-group">
                            <label>Unit</label>
                            <select name="unit" id="changeUnit">
                                <option value="">select unit</option>
                                <?php foreach ($units as $unit){ ?>
                                    <option  <?php if($product->weight_unit == $unit->id){echo 'selected';} ?> value="<?php echo $unit->id;?>"><?php echo $unit->symbol;?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                    <span style="color: red">* Please Enter Your Desired Price,Quantity and Unit</span>
                    </div>
                    <?php if(institutional('credit_charges') != 0){ ?>
                    <div style="padding-top: 15px; border: 1px #ddd solid; padding-left: 10px;">
                    <div class="form-group">
                    <span >Select Credit Charges</span>
                    </div>
                    <?php foreach ($creditCharges as $credit){ ?>
                    <div class="form-group">
                        <input class="form-check-input" type="radio" name="credit" id="credit<?php echo $credit->id;?>">
                        <label class="form-check-label" for="credit">
                            <?php echo $credit->amount; ?> per <span class="setUnit"><?php echo getUnitSymbol($product->weight_unit);?></span> for <?php echo $credit->days; ?>
                        </label>
                    </div>
                    <?php } } ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" onclick="submitCounter();">Save</button>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>

<?php foreach ($variants as $key => $value) { ?>

    <script type="text/javascript">
        $(document).ready(function ()
        {
            $(document).on("click",'.minus-<?= $key ?>',function ()
            {
                var qtyval = $('.qty-<?= $key ?>').val();
                if(qtyval >= 1)
                {
                    var newQTY = parseInt(qtyval) - parseInt(1);
                    if(newQTY > 1)
                    {
                        $('.single_add_to_cart_button_<?= $key ?>').hide();
                        $('.buttons_added_-<?= $key ?>').show();
                    }else if(newQTY == 0)
                    {
                        $('.single_add_to_cart_button_<?= $key ?>').show();
                        $('.buttons_added_-<?= $key ?>').hide();

                    }
                    $('.qty-<?= $key ?>').val(newQTY);
                }

            });

            $(document).on("click",'.plus-<?= $key ?>',function ()
            {
                var qtyval = $('.qty-<?= $key ?>').val();
                var newQTY = parseInt(qtyval) + parseInt(1);
                if(newQTY >= 1)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').hide();
                    $('.buttons_added-<?= $key ?>').show();
                }else if(newQTY == 0)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').show();
                    $('.buttons_added-<?= $key ?>').hide();

                }
                $('.qty-<?= $key ?>').val(newQTY);
            });

            $(document).on("click",'.single_add_to_cart_button_<?= $key ?>',function ()
            {
                var newQTY = 1;
                if(newQTY >= 1)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').hide();
                    $('.buttons_added_<?= $key ?>').show();
                }else if(newQTY == 0)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').show();
                    $('.buttons_added_<?= $key ?>').hide();

                }
                $('.qty-<?= $key ?>').val(newQTY);
            });
            $('.variantDiv').hide();
        });

    </script>
<?php } ?>
<button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <?=csrf_field() ?>
            <label class="control-label">Select Your Project</label>
            <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
            <div id="projectName">

            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>

<a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
<div class="popup" id="popup-project2" data-popup="popup-project2">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=institutional('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
    </div>
</div>
<!-- Modal -->
<!--Modal: Login / Register Form-->
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Modal cascading tabs-->
            <div class="modal-c-tabs">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
                    <li class="nav-item">
                        <a style="color: blue;" class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
                            By Basic</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
                            By Size</a>
                    </li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">
                    <!--Panel 7-->
                    <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

                        <!--Body-->
                        <div class="modal-body mb-1">

                            <a href="<?=url('/products/'.$cat->path)?>" class=""><?=translate($cat->name)?></a>
                            <h3 style="font-family: sans-serif; !important; font-size: 20px;"><?=translate($product->title)?></h3>
                            <div class="quantity-select productDiv" style="margin-top: 10px">
                                <div class="dec rease">-</div>
                                <input name="quantity" class="quantity productQty" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                                <div class="inc rease">+</div>
                            </div>
                            <select name="unit" class="form-control productDiv" style="float: right;max-width: 50%; margin-top: 5px;">
                                <option value="MT">MT</option>
                            </select>
                        </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1"style="text-align: center;margin-top: 30px;"> <i class="material-icons prefix">person</i></div>
                            <div class="col s10 m10">
                                <label>Select Account Manager</label>
                                <select class="browser-default acc_manager" id="acc_manager" name="acc_manager" tabindex="-1">
                                    <option value="all">All</option>

                                </select>
                            </div>
                        </div>
                        <div class="text-center mt-2">
                            <button class="btn btn-info">Log in <i class="fas fa-sign-in ml-1"></i></button>
                        </div>
                        <!--Footer-->
                        <div class="modal-footer">

                            <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                    <!--/.Panel 7-->

                    <!--Panel 8-->
                    <div class="tab-pane fade" id="panel8" role="tabpanel">

                        <!--Body-->
                        <div class="modal-body">
                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-envelope prefix"></i>
                                <input type="email" id="modalLRInput12" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput12">Your email</label>
                            </div>

                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-lock prefix"></i>
                                <input type="password" id="modalLRInput13" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput13">Your password</label>
                            </div>

                            <div class="md-form form-sm mb-4">
                                <i class="fas fa-lock prefix"></i>
                                <input type="password" id="modalLRInput14" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput14">Repeat password</label>
                            </div>

                            <div class="text-center form-sm mt-2">
                                <button class="btn btn-info">Sign up <i class="fas fa-sign-in ml-1"></i></button>
                            </div>

                        </div>
                        <!--Footer-->
                        <div class="modal-footer">
                            <div class="options text-right">
                                <p class="pt-1">Already have an account? <a href="#" class="blue-text">Log In</a></p>
                            </div>
                            <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!--/.Panel 8-->
                </div>

            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

<!--for project popup-->
<div id="myProjectModel" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form action="" method="post">
            <?=csrf_field()?>
            <input type="hidden" id="product_id" name="product_id" value="<?=$product->id;?>"/>
            <input type="hidden" id="project_id" name="project_id" value=""/>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">What type of Project are you looking for?</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <?php if($projects){
                            foreach($projects as $project){
                                $ur = 'assets/projects/';
                                ?>
                                <div class="col-md-4 proSuggestions" data-id="<?php echo $project->id?>">

                                    <a href="javascript:void(0);" class="serviceImg" style="width: 100%;height: auto;margin:0;">
                                        <img src="<?=url($ur.$project->image)?>" style="width: 100%;height: 112px;">
                                        <span class="serviceImgtitle" style="font-size: 14px;"><?=$project->title;?></span>
                                        <span class="overlay"></span>
                                    </a>
                                    <span class="selector"></span>
                                </div>
                            <?php } } ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Done</button>
                </div>
            </div>
        </form>

    </div>
</div>
<?php echo $footer?>
<script>
    $(document).ready(function(){
        $("#changeUnit").change(function () {
            var unit = $( "#changeUnit option:selected" ).text();
            $(".setUnit").html(unit);
        });
        $('form input').keydown(function (e) {
            if (e.keyCode == 13) {
                var inputs = $(this).parents("form").eq(0).find(":input");
                if (inputs[inputs.index(this) + 1] != null) {
                    inputs[inputs.index(this) + 1].focus();
                }
                e.preventDefault();
                return false;
            }
        });
        $('#bookBy').on('change', function(){
            var data = $(this).val();
            if(data == 'product'){
                $('.variantDiv').hide();
                $('.productDiv').show();
            }else{
                $('.variantDiv').show();
                $('.productDiv').hide();
            }
        });
        $('.byBase').on('click', function(){
            $(this).toggleClass('active');
            $('.byVar').toggleClass('active');
            $('.variantDiv').hide();
            $('.productDiv').show();
        });
//        $('#showtable').on('click', function(){
//            $(this).toggleClass('active');
//            $('#showtable').toggleClass('active');
//            $('.variantDiv').hide();
//            $('.price-box').show();
//        });
        $('#showtable').on('click', function(){
            var x = document.getElementById("variantDiv");
            var y = document.getElementById("price-box");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
            if (y.style.display === "none") {
                y.style.display = "block";
            } else {
                y.style.display = "none";
            }
        });
        $('#showreqtable').on('click', function(){
            var x = document.getElementById("baseForm");
            var y = document.getElementById("sizeForm");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
            if (y.style.display === "none") {
                y.style.display = "block";
            } else {
                y.style.display = "none";
            }
        });
        $('.byBaseModal').on('click', function(){
            $(this).toggleClass('active');
            $('.byVarModal').toggleClass('active');
            $('#baseForm').show();
            $('#sizeForm').hide();
//            alert('hello');
        });
        $('.byVarModal').on('click', function(){
            $(this).toggleClass('active');
            $('.byBaseModal').toggleClass('active');
            $('#sizeForm').show();
            $('#baseForm').hide();
//            alert('hello sir');
        });

        $('#request_quotation').on('click', function(){
//            $(this).toggleClass('active');
//            $('.byVar').toggleClass('active');
//            $('.variantDiv').hide();
//            $('.productDiv').show();
            $('#asrModal').modal('show');
        });
        $('#counter_offer').on('click', function(){
            $('#counterModal').modal('show');
        });

        $('.byVar').on('click', function(){
            $(this).toggleClass('active');
            $('.byBase').toggleClass('active');
            $('.variantDiv').show();
            $('.productDiv').hide();
            var $container = $("html,body");
            var $scrollTo = $('.variantDiv');

            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top  - 120, scrollLeft: 0},300);
        });
        // Function to update counters on all elements with class counter
        var doUpdate = function() {
            $('.countdown-timer').each(function() {
                var count = parseInt($(this).html());
                var pid = $(this).data('key');
                if (count > 0) {
                    $(this).html(count - 1);
                }else if(count == 0){
                    $('#'+pid+' .price').html('Waiting for update');
                    $('#'+pid+' .tooltipx').remove();
                    $('#'+pid+' .tp').html('Waiting for update');
                }
            });
        };

        // Schedule the update to happen once every second
        setInterval(doUpdate, 1000);
    });
</script>
<script>

    (function() {

        window.inputNumber = function(el) {

            var min = el.attr('min') || false;
            var max = el.attr('max') || false;

            var els = {};

            els.dec = el.prev();
            els.inc = el.next();

            el.each(function() {
                init($(this));
            });

            function init(el) {

                els.dec.on('click', decrement);
                els.inc.on('click', increment);

                function decrement() {
                    var value = el[0].value;
                    value--;
                    if(!min || value >= min) {
                        el[0].value = value;
                    }
                }

                function increment() {
                    var value = el[0].value;
                    value++;
                    if(!max || value <= max) {
                        el[0].value = value++;
                    }
                }
            }
        }
    })();

    inputNumber($('.input-number'));
    function decrement(prid) {

        var el = $(".pridvalue"+prid);
        var saleprice = $("#mainprice"+prid).val();
        console.log(saleprice);
        var totpr = '';
        var value = el.val();
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        value--;
        if(!min || value >= min) {
            el.val(value);
//            totpr = saleprice*value;
            totpr = '<input type="hidden" class="tpv" value="'+saleprice*value+'"><span class="singlepr'+prid+' price">'+saleprice*value+'</span>';
            $("#tp-"+prid).html("0.00");
            $("#tp-"+prid).html(totpr);
            caltotal();
        }
    }

    function increment(prid) {
        var el = $(".pridvalue"+prid);
        var saleprice = $("#mainprice"+prid).val();

        var totpr = '';
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        var value = el.val();
        value++;
        if(!max || value <= max) {
            el.val(value);
            totpr = '<input type="hidden" class="tpv" value="'+saleprice*value+'"><span class="singlepr'+prid+' price">'+saleprice*value+'</span>';
            console.log(totpr);
            $("#tp-"+prid).html("0.00");
            $("#tp-"+prid).html(totpr);
            caltotal();
        }
    }

    function decrementrfqd(prid) {

        var el = $(".pridvaluerfqd"+prid);
        var value = el.val();
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        value--;
        if(!min || value >= min) {
            el.val(value);
        }
    }

    function incrementrfqd(prid) {
        var el = $(".pridvaluerfqd"+prid);
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        var value = el.val();
        value++;
        if(!max || value <= max) {
            el.val(value);

        }
    }

    function getTotal(val,index,varid){
        var qty = parseInt(val);
        var prid = $('#prlist'+varid).val();
        var saleprice = $('#mainprice'+varid).val();
         var hdata = '<input type="hidden" class="tpv" value="'+qty*saleprice+'"><span class="singlepr'+varid+' price">'+qty*saleprice+'</span>';
        $("#tp-"+varid).html(hdata);
        var variants = 0.00;
        var mtotal = 0.00;
//        $(".tpv").each(function () {
//            variants += Number($(this).val());
//        });

//        $('#gt').html(variants);
        caltotal();
    }
    function getRfqTotal(val,index,varid){
        var qty = parseInt(val);
        var prid = $('#prlist'+varid).val();
        var saleprice = $('#rfqmainprice'+varid).val();
        var hdata = '<input type="hidden" class="rfqtpv" value="'+qty*saleprice+'"><span class="rfqsinglepr'+varid+' rfqprice">'+qty*saleprice+'</span>';
        $("#rfqtp-"+varid).html(hdata);
        var variants = 0.00;
        var mtotal = 0.00;
        $(".rfqtpv").each(function () {
            variants += Number($(this).val());
        });
        $('#rfqgt').html(variants);
        rfqcaltotal();
    }

    function  caltotal() {
        var variants = 0;
        var quantity = [];
        var variant_id = [];
        var product_id = '<?=$product->id;?>';
        var customer_id = '<?=institutional('id');?>';
        var hub_id = '<?=$hub;?>';
        var weight_unit = '<?=$product->weight_unit;?>';
        var credit_id = $("input[type='radio'][name='credit']:checked").val();
        $(".tpv").each(function () {
            var vrprce = parseInt($(this).val()) || 0;
                variants += vrprce;
        });
        $('#gt').html("0.00");
        $('#gt').html(variants);
//            alert(variants);
        $(".prlist").each(function () {
            if($(this).parent().parent().is(":visible")) {
                var prid = $(this).val();
                var quant = $(".pridvalue" + prid).val();
                if (quant) {
                    variant_id.push(prid);
                    quantity.push(quant);
                }
            }
        });
        if(quantity && variant_id){
            $.ajax({
                url: 'i/get_var_price_detail',
                type: 'post',
                data: 'quantity='+quantity+'&variant_id='+variant_id+'&product_id='+product_id+'&customer_id='+customer_id+'&hub_id='+hub_id+'&credit_id='+credit_id+'&weight_unit='+weight_unit+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('#difference').html(data.difference);
                    $('#loading').html(data.loading);
                    $('#tax').html(data.tax);
                    if(credit_id !== undefined) {
                        $('#credit').html(data.credit);
                        $('#creditText').html(data.creditText);
                    }
                }
            });
        }
    }
    function decrementrfq(prid) {

        var el = $(".rfqpridvalue"+prid);
        var saleprice = $(".rfqmainprice"+prid).val();
        alert(saleprice);
        var totpr = 0;
        var value = el.val();
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        value--;
        if(!max || value >= min) {
            el.val(value);
            totpr = '<input type="hidden" class="rfqtpv" value="'+saleprice*value+'"><span class="rfqsinglepr'+prid+' rfqprice">'+saleprice*value+'</span>';
            console.log(totpr);
            $("#tp-"+prid).html("0.00");
            $("#tp-"+prid).html(totpr);
            caltotal();
        }
    }

    function incrementrfq(prid) {
        var el = $(".rfqpridvalue"+prid);
        var saleprice = $(".rfqmainprice"+prid).val();
        var totpr = 0;
        var min = el.attr('min') || false;
        var max = el.attr('max') || false;
        var value = el.val();
        value++;
        if(!max || value >= max) {
            el.val(value);
            totpr = '<input type="hidden" class="rfqtpv" value="'+saleprice*value+'"><span class="rfqsinglepr'+prid+' rfqprice">'+saleprice*value+'</span>';
            $(".rfqsinglepr"+prid).html("0.00");
            $(".rfqsinglepr"+prid).html(totpr);
            rfqcaltotal();
        }
    }

    function  rfqcaltotal() {
        var variants = 0.00;
        $(".rfqtpv").each(function () {
            variants += Number($(this).val());
        });
        //alert(variants);
        $('#rfqgt').html("0.00");
        $('#rfqgt').html(variants);

        var mainvariants = 0.00;
        $(".rfqprlist").each(function () {
            var prid = $(this).val();
            var el = $(".rfqpridvalue"+prid).val();
            var singpr = $(".rfqmainprice"+prid).text();
            mainvariants += parseInt(singpr*el);
        });

    }

    function productWishlist(val){
        $('#myProjectModel').modal('show')
    }

    $(document).on('click', '.proSuggestions', function(){
        var ele = $(this);
        ele.find('.selector').toggleClass('active');
        var data = $('#project_id').val();
        if(ele.find('.selector').hasClass('active')){
            var ar = data.split(',');
            ar.push(ele.data('id'));
            $('#project_id').val(ar.filter(function(v){return v!==''}).join(','));
        }else{
            $('#project_id').val(removeValue(data, ele.data('id'), ','));
        }
    })
    function submitCounter(){
        $('form#counterForm').submit();
    }



</script>
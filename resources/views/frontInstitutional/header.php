<!DOCTYPE html>
<html>
    <head>
		<meta charset='utf-8'/>
		<title><?=$data['title'] ?></title>        
		<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<meta content='<?=$data['desc'] ?>' name='description'/>
		<meta content='<?=$data['keywords'] ?>' name='keywords'/>
		<base href="<?=url('') ?>/" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"> 
		<link rel="stylesheet" href="<?=$data['tp'] ?>/assets/select2.css">
		<link rel="stylesheet" href="<?=$data['tp'] ?>/assets/style.css">
		<script src="<?=$data['tp'] ?>/assets/plugins.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/tabs.css" />
		<link rel="stylesheet" type="text/css" href="<?=$data['tp'] ?>/assets/tabstyles.css" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
        <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css">
        <link rel="stylesheet" href="<?=$data['tp'];?>/assets/flexslider.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="<?=$data['tp'];?>/assets/dataTables.bootstrap4.css">
        <link rel="stylesheet" href="<?=$data['tp'];?>/assets/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?=$data['tp'];?>/assets/buttons.dataTables.min.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
		<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Coustard:900' rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css' />
        <!-- Main stylesheet  -->
        <link rel="stylesheet" type="text/css" href="assets/css/aos.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <!-- Responsive stylesheet  -->
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
		<script src="<?=$data['tp'] ?>/assets/modernizr.custom.js"></script>
        <script src="<?=$data['tp'] ?>/assets/jquery.dataTables.js"></script>
        <script src="<?=$data['tp'] ?>/assets/dataTables.bootstrap4.js"></script>
        <script src="<?=$data['tp'] ?>/assets/ckeditor/ckeditor.js"></script>
        <script src="<?=$data['tp'] ?>/slick/slick.min.js" type="text/javascript" charset="utf-8"></script>
		<?php if ($data['stripe']->active == 1) {?><script type="text/javascript" src="https://js.stripe.com/v2/" async></script><?php }?>
		<script>
			var sitename = '<?=translate($data['cfg']->name) ?>';
			var empty_cart = '<?=translate('Your cart is empty') ?>';
			var checkout = '<?=translate('Checkout') ?>';
			var continue_to_payment = '<?=translate('Continue') ?>';			
			<?php if ($data['stripe']->active == 1) {?>var stripe_key = '<?=json_decode($data['stripe']->options,true)['key']?>';<?php }?>
			<?=$data['style']->js ?>
		</script>
        <style>
            .visitor-type{
                position: fixed;
                right: 0;
                top: 0;
                padding: 5px 10px;
                background: #ffffff;
                z-index: 9999;
                font-size: 12px;
                text-transform: capitalize;
                line-height: 1;
            }
            .visitor-type a{
                margin-left: 15px;
                color: blue;
            }
            @media screen and (max-device-width:767px), screen and (max-width:767px) {
                .pd20{
                    padding-top: 20px;
                }
                .visitor-type{
                    left:0;
                    text-align: center;
                }
            }
        </style>
	</head>
	<body dir="ltr" <?php if ($data['page'] == true) {?>class="page"<?php }?>>
    <div class="visitor-type"><?=$data['vtype'];?> Buyer <a href="<?=url('welcome')?>">Change?</a></div>
		<?php if ($data['cfg']->floating_cart == 1 && institutional('user_type') !== 'institutional') {?>
		<button class="toggle-cart"><i class="icon-basket"></i></button>
		<div id="cart">
			<button class="toggle-cart"><i class="icon-close"></i></button>
			<div id="cart-header">
				Cart
				<button class="pull-right" onclick="$('#cart').toggle('300');"><i class="icon-close"></i></button>
			</div>
			<div id="cart-content">
				<div class="loading"></div>
			</div>
		</div>
		<?php }?>
		<?php if ($data['landing'] == true) {?><div id="wrap"><div class="page-warpper cover-header"><?php } else {?><div id="wrap"><div class="page"><?php }?>
		<style>
			<?php if ($data['bg'] != false) {?>
			.cover {background:linear-gradient(to bottom, rgba(0, 0, 0, 0.5),rgba(255, 255, 255, 0)),url("<?=$data['bg'] ?>") no-repeat 0%/cover scroll;}
			<?php } else {?>
			.cover {background:linear-gradient(to right, <?=$data['style']->background ?>) repeat scroll 0% 0%;}
			<?php }?>
			.bg, #navbar.collapse.in, #navbar.collapsing {
			background:linear-gradient(to right, <?=$data['style']->background ?>) repeat scroll 0% 0%;
			}
			.c {
			color: <?=$data['color'][0] ?>;
			}
			<?=$data['style']->css ?>
		</style>
        <section class="at-main-herader-sec fixed-header">
            <div class="at-header-topbar" style="background-color: #f4f4f4;">
                <div class="container">
                    <div class="row pd20">
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                            <a class="navbar-brand" rel="home" href="<?=url('')?>"><img class="" src="<?=$data['cfg']->logo ?>"></a>
                        </div>
                        <!--div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 at-full-wd480">
                            <p class="at-respo-right"><i class="icofont icofont-email"></i> <a href="#">info@yourmail.com</a>
                            </p>
                        </div-->
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 login-section pull-right">
                            <?php if(institutional('sid')) { ?>
                                <div class="dropdown" style="display:inline-block">
                                    <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">
                                        <?php $username = institutional('name'); $t = explode(' ', $username); echo $t[0]; ?>
                                        <span class="caret"></span>
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="<?=url('i/dashboard');?>" style="padding: 5px 10px;">Dashboard</a>
                                        <a class="dropdown-item" href="<?=url('i/change-password');?>" style="padding: 5px 10px;">Change Password</a>
                                        <a class="dropdown-item" href="<?=url('i/logout');?>" style="padding: 5px 10px;">Logout</a>
                                    </div>
                                </div>

                            <?php } else { ?>
								<div class="dropdown m-view" style="display:inline-block">
									<a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
										Hi! Guest.
										<span class="caret"></span>
									</a>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="i/login" style="padding: 10px;">Login</a><br>
										<a class="dropdown-item animation" href="register/institutional" style="padding: 10px;">Signup</a>
									</div>
								</div>
                                <div class="d-view">
                                    <ul class="" style="display: inline-flex;">
                                        <li class="dropdown-item" style="padding: 10px;"><a href="./login" >Login</a></li>

                                        <li class="dropdown-item" style="padding: 10px;">
                                            <?php
                                            //dd(url()->current());
                                            if(url()->current() == 'https://aakar360.com') {?>
                                                <a href="#sign" >Signup</a>
                                            <?php } else { ?>
                                                <a href="#" onclick="getRegModal()" >Signup</a>
                                        <?php } ?>
                                        </li>
                                    </ul>
                                </div>

                            <?php } ?>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
						</div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="at-search-box">
                                <div class="col-lg-12" style="padding:0;">
                                	<div id="custom-search-input">
										<div class="input-group col-md-12" style="border-radius: 50%;">
											<input type="text" id="search-input" class="form-control input-lg" placeholder="Search for photos, products and services..." style="border-radius: 50%;font-size: 13px;"/>
											<span class="input-group-btn">
												<button class="btn btn-info btn-lg" type="submit">
													<i class="fa fa-search" style="margin:0;"></i>
												</button>
											</span>
										</div>
									</div>
                                </div>
                            </div>
                            <div id="search-results" class="col-lg-10 col-md-10 col-sm-10" style="margin-left: 10px;z-index: 999">
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Header navbar start -->
            <div class="at-navbar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-default" style="background-color: transparent;">

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" data-hover="dropdown" data-animations="fadeInUp">

                                    <ul class="nav navbar-nav navbar-left" id="topNav" style="width: 100%;">
                                        <ul class="nav navbar-nav">
                                            <li class="dropdown mega-dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icofont icofont-box"></i>Products<span class="caret"></span></a>
                                                <ul class="dropdown-menu mega-dropdown-menu row">
                                                    <ul class="col-sm-9 main-menu">
                                                        <?php
                                                        $pcats = false;
                                                        $pcats = $data['p_cat'];
                                                        if($pcats){
                                                            foreach($pcats as $pcat){
                                                                ?>
                                                                <li class="col-sm-4">
                                                                    <ul>
                                                                        <li>
                                                                            <a class="dropdown-header" href="<?=url('i/products/'.$pcat->path);?>"><?php echo $pcat->name; ?></a>
                                                                        </li>
                                                                        <?php
                                                                        $pscats = false;
                                                                        $pscats = getProductCategories($pcat->id);
                                                                        if($pscats){
                                                                            foreach($pscats as $pscat){ ?>
                                                                                <li><a href="<?=url('i/products/'.$pcat->path);?>"><?=$pscat->name;?></a></li>
                                                                            <?php }
                                                                        } ?>
                                                                        <li class="divider"></li>
                                                                    </ul>
                                                                </li>
                                                            <?php }
                                                        } ?>
                                                    </ul>
                                                    <li class="col-sm-3">
                                                        <ul>
                                                            <div class="col-md-12">
                                                                <?php
                                                                $mega_product = false;
                                                                $mega_product = $data['mega_product'];
                                                                ?>
                                                                <?php if($mega_product == 'Data Not Available'){?>
                                                                    <img src="" style="width: 100%; height: auto;margin:20px 20px 20px 10px;"/>
                                                                <?php } else {?>
                                                                    <img src="assets/megamenuimage/<?php echo $mega_product->image; ?>" style="width: 100%; height: auto;margin:20px 20px 20px 10px;"/>
                                                                <?php } ?>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <ul class="nav navbar-nav">
                                            <li class="dropdown mega-dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icofont icofont-idea"></i>Advices<span class="caret"></span></a>
                                                <ul class="dropdown-menu mega-dropdown-menu row">
                                                    <?php if($data['cust_type'] == 'institutional'){?>
                                                    <ul class="col-sm-9 main-menu" style="display: none;">
                                                        <?php }  else{ ?>
                                                        <ul class="col-sm-9 main-menu">
                                                            <?php }?>
                                                            <?php
                                                            $adcats = false;
                                                            $adcats = $data['a_cat'];
                                                            if(!empty($adcats)){
                                                                foreach($adcats as $adcat){
                                                                    ?>
                                                                    <li class="col-sm-4">
                                                                        <ul>
                                                                            <li><a class="dropdown-header" href="./advices_cat/<?php echo path($adcat->name, $adcat->id); ?>"><?php echo $adcat->name; ?></a></li>
                                                                            <?php
                                                                            $adscats = false;
                                                                            $adscats = getAdvicesCategories($adcat->id);

                                                                            if($adscats){
                                                                                foreach($adscats as $adscat){ ?>
                                                                                    <li><a href="./advices/<?=$adscat->slug;?>"><?=$adscat->name;?></a></li>
                                                                                <?php } } ?>
                                                                            <li class="divider"></li>
                                                                        </ul>
                                                                    </li>
                                                                <?php } } ?>
                                                        </ul>
                                                        <?php if($data['cust_type'] == 'institutional'){?>
                                                            <center>
                                                                <h3 style="margin-top: 50px; color: #003481;">Coming Soon!</h3>
                                                            </center>
                                                        <?php }?>
                                                        <?php if($data['cust_type'] == 'institutional'){?>
                                                        <li class="col-sm-3 pull-right" style="margin-top: -70px; display:none;">
                                                            <?php }  else{ ?>
                                                        <li class="col-sm-3 pull-right">
                                                            <?php }?>
                                                            <ul>
                                                                <div class="col-md-12">
                                                                    <?php
                                                                    $mega_product = false;
                                                                    $mega_product = $data['mega_product'];
                                                                    ?>
                                                                    <img src="assets/megamenuimage/<?php echo $mega_product->image; ?>" style="width: 100%; height: auto;margin:20px 20px 20px 10px;"/>
                                                                </div>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                    </li>
                                                </ul>
                                        </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header navbar end -->
        </section>

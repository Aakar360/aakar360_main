
<?php echo $header;
if(institutional('id') !== ''){
    $customerId = institutional('id');
}else{
    $customerId = false;
}
?>
<section >
    <div class="container">
        <div class="row" id="at-inner-title-sec" <?=($categ->banner !== '' ? 'style="background-image: url(\'assets/products/'.$categ->banner.'\'); box-shadow: 0 3px 6px rgba(0,0,0,.16);"' : ''); ?>>
            <div class="col-md-6 col-sm-6 col-md-offset-3" style="padding: 30px 0; margin-top: 45px; vertical-align: middle;">
                <div class="inner-border">
                    <div class="at-inner-title-box text-center" style="vertical-align: middle; padding: 25px;margin:5px;border: none;background: rgba(255, 255, 255, 0.7);">
                        <h3><?=(isset($categ->name) ? translate($categ->name) : translate('Products'));?></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <section class="at-property-sec at-property-right-sidebar">
        <div class="container">
            <div class="row">
                <button class="btn btn-default filter-button"><i class="icon-basket"></i> Featured Products</button>
                <aside class="sidebar-shop col-md-3 order-md-first">
                    <div class="filter-div-close"></div>
                    <div class="sidebar-wrapper">
                        <div class="widget">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-body-c" role="button" aria-expanded="true" aria-controls="widget-body-c">Categories</a>
                            </h3>
                            <div class="show collapse in" id="widget-body-c">
                                <div class="widget-body">
                                    <ul class="cat-list" style="height: 250px; overflow-y: auto;">
                                        <?php foreach($cats as $cat){
                                            echo '<li><a href="i/products/'.$cat->path.'">'.translate($cat->name).'</a></li>';
                                            $childs = \App\Category::where('parent',$cat->id)->orderBy('id','DESC')->get();
                                            foreach ($childs as $child){
                                                echo '<li><a href="i/products/'.$child->path.'">- '.$child->name.'</a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->
                        <?php if($show_filter && $show_products ){ ?>
                            <form method="get" action="">
                                <div class="widget">
                                    <h3 class="widget-title">
                                        <a data-toggle="collapse" href="#widget-body-p" role="button" aria-expanded="true" aria-controls="widget-body-p">Price</a>
                                    </h3>
                                    <div class="show collapse in" id="widget-body-p">
                                        <div class="widget-body">
                                            <div class="form-price">
                                                <div class="clearfix"></div>
                                                <b class="pull-left price"><?=$price['set_min'] ?></b>
                                                <b class="pull-right price"><?=$price['set_max']; ?></b>
                                                <input name="min" id="min" type="hidden">
                                                <input name="max" id="max" type="hidden">
                                                <div id="price"></div>
                                            </div>
                                        </div><!-- End .widget-body -->
                                    </div><!-- End .collapse -->
                                </div><!-- End .widget -->
                                <div class="widget">
                                    <h3 class="widget-title">
                                        <a data-toggle="collapse" href="#widget-body-b" role="button" aria-expanded="true" aria-controls="widget-body-b">Brand</a>
                                    </h3>
                                    <div class="show collapse in" id="widget-body-b">
                                        <div class="widget-body">
                                            <ul class="cat-list" style="height: 100px; overflow-y: auto;">
                                                <?php
                                                foreach($brands as $brand){
                                                    $checked = '';
                                                    if(in_array($brand->id, $bids)){
                                                        $checked = 'checked';
                                                    }
                                                    echo '<li><input type="checkbox" name="brands['.$brand->id.']" id="br-'.$brand->id.'" '.$checked.'><label for="br-'.$brand->id.'">'.translate($brand->name).'</label></a></li>';
                                                }
                                                ?>
                                            </ul>
                                        </div><!-- End .widget-body -->
                                    </div><!-- End .collapse -->
                                </div><!-- End .widget -->
                                <?php
                                if(!empty($filters)) { ?>
                                    <?php foreach ($filters as $fil) {
                                        $fil_option = \App\FilterOption::where('filter_id', $fil->id)->get();
                                        if (!empty($fil_option)) {
                                            ?>
                                            <div class="widget">
                                                <h3 class="widget-title">
                                                    <a data-toggle="collapse" href="#widget-body-<?= $fil->id; ?>" role="button"
                                                       aria-expanded="true"
                                                       aria-controls="widget-body-<?= $fil->id; ?>"><?php echo $fil->name; ?></a>
                                                </h3>
                                                <div class="show collapse in" id="widget-body-<?= $fil->id; ?>">
                                                    <div class="widget-body">
                                                        <?php if ($fil->name == 'Colour') { ?>
                                                            <ul class="config-swatch-list">
                                                                <?php foreach($fil_option as $fo){ ?>
                                                                    <li>
                                                                        <a href="#" style="background-color: <?php echo $fo->name; ?>;"> </a>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } elseif($fil->name == 'Size') { ?>
                                                            <ul class="config-size-list">
                                                                <?php foreach($fil_option as $fo){ ?>
                                                                    <li>
                                                                        <a class="a-text" href="#"><?php echo $fo->name; ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } else { ?>
                                                            <ul>
                                                                <?php foreach($fil_option as $fo){ ?>
                                                                    <li>
                                                                        <a href="#"><?php echo $fo->name; ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </div><!-- End .widget-body -->
                                                </div><!-- End .collapse -->
                                            </div><!-- End .widget -->
                                        <?php }
                                    }
                                }?>
                                <div class="filter-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">Apply Filter</button>
                                </div>
                            </form>
                        <?php } ?>
                    </div><!-- End .sidebar-wrapper -->
                </aside>
                <div class="filter-wrapper"></div>

                <div class="col-md-9">
                    <?php if(count($suggested) > 0){?>
                        <div class="row" style="padding-left: 10px;">
                            <div class="box clearfix box-with-products" style="width: 800px; height:500px;">
                                <div class="strip-line"></div>
                                <div class="box-content products" style="margin:20px 15px 20px 20px;">
                                    <div class="box-product">

                                            <?php
                                            foreach($suggested as $suggest){

                                                $pvalidity = -1;
                                                if($suggest->is_variable){
                                                    date_default_timezone_set("Asia/Kolkata");
                                                    $pvalidity = strtotime($suggest->validity) - strtotime(date('Y-m-d H:i:s'));
                                                    if($pvalidity < 0){
                                                        $pvalidity = 0;
                                                    }
                                                }
                                                ?>
                                                <div class="col-md-4">
                                                    <div class="item">
                                                        <div class="at-property-item at-col-default-mar" id="<?=$suggest->id;?>">
                                                            <span class="countdown-timer" data-key="<?=$suggest->id;?>"><?=$pvalidity;?></span>
                                                            <div class="at-property-img">
                                                                <a href="i/product/<?=path($suggest->title,$suggest->id)?>" data-title="<?=translate($suggest->title)?>">
                                                                    <img src="<?=url('/assets/products/'.image_order($suggest->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                                                </a>
                                                                <div class=""></div>
                                                            </div>
                                                            <div class="at-property-location">
                                                                <div class="text-ellipsis">
                                                                    <a href="i/product/<?=path($suggest->title,$suggest->id)?>" data-title="<?=translate($suggest->title)?>"><?=translate($suggest->title)?></a>
                                                                </div>
                                                                <span class="price">
                                                                <?php
                                                                if(is_bool(getInstitutionalPrice($suggest->id, $customerId,$hub,false,false))) { ?>
                                                                    <?php echo c(getInstitutionalPrice($suggest->id, $customerId, $hub, false, false));
                                                                }else{?>
                                                                <?php echo c(getInstitutionalPrice($suggest->id, $customerId,$hub,false,false)['price']); }?>
                                                                </span>
                                                                <?php if(institutional('user_type') == 'institutional' && institutional('rfr') == 0){
                                                                    $pb = getInstitutionalPrice($suggest->id, $customerId, $hub,false,false);

                                                                    echo '<div class="tooltipx"><i class="fa fa-info-circle"></i>
                                                            <div class="tooltiptext">
                                                                <div class="tooltipContent">
                                                                    <span class="tooltipLeft">Basic</span>
                                                                    <span class="tooltipRight">'; if(!empty($pb)){ echo $pb['price'];} echo '</span>
                                                                </div>
                                                                <div class="tooltipContent">
                                                                    <span class="tooltipLeft">Loading</span>
                                                                    <span class="tooltipRight">'; if(!empty($pb)){ echo $pb['loading'];} echo '</span>
                                                                </div>
                                                                <div class="tooltipContent">
                                                                    <span class="tooltipLeft">Tax</span>
                                                                    <span class="tooltipRight">'; if(!empty($pb)){ echo $pb['tax'];} echo '</span>
                                                                </div>
                                                                <div class="tooltipContent">
                                                                    <span class="tooltipLeft">Total</span>
                                                                    <span class="tooltipRight">'; if(!empty($pb)){ echo $pb['total'];} echo '</span>
                                                                </div>
                                                            </div>
                                                        </div>';
                                                                } if($suggest->rating_view != '0'){?>
                                                                    <div class="rating">
                                                                        <?php
                                                                        $rates = getRatings($suggest->id);
                                                                        $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                                            <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                                            <?php $tr--; }?>
                                                                        (<?=$rates['total_ratings']?>)
                                                                    </div>
                                                                <?php }?>
                                                                <div class="cart-btn-custom" style="padding-top: 10px;">
                                                                    <button class="bg" data-redirect="i/product/<?=path($suggest->title, $suggest->id) ?>" data-title="<?=translate($suggest->title) ?>"><i class="icon-basket"></i> View Detail</button>
                                                                    <div style="margin-top: -20px;">&nbsp;</div>
                                                                    <?php
                                                                    if(institutional('id') !== ''){
                                                                        $user_id = institutional('id');
                                                                        $product_id = $suggest->id;
                                                                        $img = \App\ProductWishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();
                                                                        if(!empty($img)){
                                                                            echo '<button style="display:none;" class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                                        } else{
                                                                            echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
																            <button style="display:none;" class="bg likebutton"><i class="fa fa-heart"></i> Add to Project</button>';
                                                                        }
                                                                    } else {
                                                                        echo '<button style="display:none;" class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>

    <!-- End Client -->
<button class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login">Get Started !</button>
<div class="popup" id="popup-login" data-popup="popup-login">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-6 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                <?=csrf_field() ?>
                <fieldset>
                    <div class="form-group">
                        <label class="control-label"><?=translate('E-mail') ?></label>
                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?=translate('Password') ?></label>
                        <input name="password" type="password" class="form-control"  />
                    </div>
                    <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                </fieldset>
            </form>
            <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
        </div>
        <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
    </div>
</div>
<script>
    function submitHub(){
        if($('input[name=hub]:checked').val()) {
            $('form#hubForm').submit();
        }else{
            alert('Please select rate type to proceed!')
        }
    }
    $(document).ready(function(){
        var owl0 = $(".box #myCarousel0");
        $("#myCarousel0_next").on("click", function() {
            event.preventDefault();
            owl0.trigger('owl.next');
            return false;
        })
        $("#myCarousel0_prev").on("click", function() {
            event.preventDefault();
            owl0.trigger('owl.prev');
            return false;
        });
        owl0.owlCarousel({
            slideSpeed : 400,
            itemsCustom : [
                [0, 1],
                [360, 2],
                [700, 3],
                [1255, 3]
            ]
        });
    });
    function initVerticalCarousel(obj){
        var vc = $(obj);
        var heights = $(obj +" .vcx_item").map(function ()
        {
            return $(this).height();
        }).get();
        var maxHeight = Math.max.apply(null, heights) + 20;
        window.matchMedia('screen and (max-width: 991px)')
        {
            vc.attr('style', 'height:'+maxHeight+'px;');
        }
    }
    $(window).resize(function(){
        initVerticalCarousel('#vcc1');
        initVerticalCarousel('#vcc2');
    });
    $(document).ready(function(){
        var owl1 = $(".box #myCarousel1");

        $("#myCarousel1_next").on("click", function() {
            event.preventDefault();
            owl.trigger('owl.next');
            return false;
        })
        $("#myCarousel1_prev").on("click", function() {
            event.preventDefault();
            owl.trigger('owl.prev');
            return false;
        });
        owl1.owlCarousel({
            slideSpeed : 400,
            itemsCustom : [
                [0, 1],
                [480, 1],
                [700, 1],
                [1255, 1]
            ]
        });
        initVerticalCarousel('#vcc1');
        initVerticalCarousel('#vcc2');
    });
</script>
<script>
    $(function() {
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });
    function getModel(){
        $('#open-service-modal1').click();
    }
    function addProject(val) {
        var ele =$(this);
        var proid = $('#ghanta').data('id');
        var project_id = val;
        $.ajax({
            type: "POST",
            url: "add-product-project",
            data:'_token=<?=csrf_token();?>&product_id='+proid+'&project_id='+project_id,
            success: function(data){
                $('#pclose').click();
                ele.unbind();
            }
        });
    }
    $('.likebutton').click( function () {
        var ele =$(this);
        var proid = $(this).prevAll('input').val();
        $.ajax({
            type: "POST",
            url: "product-wishlist",
            data:'_token=<?=csrf_token();?>&product_id='+proid,
            success: function(data){
                //alert('hello');
                $('#open-project-modal1').click();
                $('#projectName').html(data);
                ele.html('<span class="tickgreen">✔</span> Added to Project');
                ele.unbind();
            }
        });
    });
    function addProject(val) {
        var ele =$(this);
        var proid = $('#ghanta').data('id');
        var project_id = val;
        $.ajax({
            type: "POST",
            url: "add-product-project",
            data:'_token=<?=csrf_token();?>&product_id='+proid+'&project_id='+project_id,
            success: function(data){

                $('#pclose').click();
                ele.unbind();
            }
        });
    }
    $('.likebutton1').click( function () {
        var ele =$(this);
        var proid = $(this).prevAll('input').val();
        $.ajax({
            type: "POST",
            url: "product-wishlist",
            data:'_token=<?=csrf_token();?>&product_id='+proid,
            success: function(data){
                //alert('hello');
                $('#open-project-modal1').click();
                $('#projectName').html(data);
                ele.html('<span class="tickgreen">✔</span> Added to Project');
                ele.unbind();
            }
        });
    });
    function getcreate(){
        event.preventDefault();
        $('#project-modal2').click();
    }
    $(document).ready(function(){
        $(document).on('click','.show_more',function(){
            var brands = $('input[name=category]').val();
            var ID = $(this).attr('id');
            $('.show_more').hide();
            $('.loding').show();
            $.ajax({
                type:'POST',
                url:'get-faq',
                data:'brands='+brands+'&_token=<?=csrf_token();?>&id='+ID,
                success:function(html){
                    $('#show_more_main'+ID).remove();
                    $('#fload').append(html);
                }
            });
        });
    });
    $(document).ready(function(){
        $(document).on('click','.brand_more',function(){
            var id = $(this).attr('id');
            $('.brand_more').hide();
            $('.loding_brand').show();
            $.ajax({
                type:'POST',
                url:'get-brand-load',
                data:'_token=<?=csrf_token();?>&id='+id,
                success:function(html){
                    $('#brand_more_main'+id).remove();
                    $('#bload').append(html);
                }
            });
        });
        $(document).on('click', '.filter-button', function(){
            $('aside.sidebar-shop').addClass('open');
        });
        $(document).on('click', '.filter-div-close', function(){
            $('aside.sidebar-shop').removeClass('open');
        });
        var doUpdate = function() {
            $('.countdown-timer').each(function() {
                var count = parseInt($(this).html());
                var pid = $(this).data('key');
                if (count > 0) {
                    $(this).html(count - 1);
                }else if(count == 0){
                    <?php if(institutional('user_type') == ''){ ?>
                    $('#'+pid+' .price').html('Login to view price');
                    <?php } else{ ?>
                    $('#'+pid+' .price').html('Waiting for update');
                    <?php } ?>
                    $('#'+pid+' .tooltipx').remove();
                    }
                });
            };
        setInterval(doUpdate, 1000);
    });
</script>
<script>
    var handlesSlider = document.getElementById('price');
    noUiSlider.create(handlesSlider, {
        start: [<?=$price['set_min']?>,<?=$price['set_max']?>],
        step: 1,
        connect: false,
        range: {'min':0,'max':<?=$price['max']?>},
    });
    var BudgetElement = [document.getElementById('min'),document.getElementById('max')];
    handlesSlider.noUiSlider.on('update', function(values, handle) {
        BudgetElement[0].textContent = values[0];
        BudgetElement[1].textContent = values[1];
        $("#min").val(values[0]);
        $(".pull-left.price").html(values[0]);
        $("#max").val(values[1]);
        $(".pull-right.price").html(values[1]);
    });
</script>
<?php echo $footer?>
<button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <?=csrf_field() ?>
            <label class="control-label">Select Your Project</label>
            <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
            <div id="projectName">
            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>
<a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
<div class="popup" id="popup-project2" data-popup="popup-project2">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=institutional('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
    </div>
</div>
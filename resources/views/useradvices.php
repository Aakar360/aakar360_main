<?php echo $header?>

<div class="profile-root bg-color ng-scope">
    <div class="clearfix buffer-top text-center wrapper-1400 bg-color" style="padding-bottom: 20px;">
        <div class="clearfix wrapper-1140 two-bgs">
            <?php if(customer('header_image') != '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12" style="padding: 0;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-12" style="padding: 0;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12" style="padding: 0;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') != '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-6" style=" padding-left: 0px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image'); ?>" class="cover">
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs" style=" padding-left: 2px; padding-right: 0px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope">
            <img itemprop="image" class="profile-icon" src="assets/user_image/<?php if(customer('image') != ''){ echo Customer('image'); } else { echo 'user.png'; } ?>">
            <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                <a itemprop="url" class="url">
                    <?php echo Customer('name'); ?>
                </a>
                <p style="text-align: center;"><a href="#" onClick="getprofile()" class="btn">My Profile</a></p>
            </h1>
            <div class="sans-light font-15 line-height-md">
                <p style="text-align: justify; padding: 30px;">Vanessa&nbsp;has a Bachelor’s Degree in Architecture and a Minor in Interior Design from the University of Oklahoma. Her architecture education has given her a strong passion for incorporating clean lines, unexpected color palettes, and simple but meaningful details into the spaces she designs.</p>
            </div>
            <div class="sans-light font-15 line-height-md" style="padding-bottom: 0px; position: absolute; width: 100%; bottom: 0;">
                <ul class="u-menu" style="text-align: center;">
                    <li><a href="r/myorders"  class="btn">Products</a></li>
                    <li><a href="r/serviceorders"  class="btn">Services</a></li>
                    <li><a href="r/planorders"  class="btn">Design</a></li>
                    <li><a href="r/useradvices"  class="btn btn-primary">Advices</a></li>
                    <li><a href="r/my-projects"  class="btn">My Projects</a></li>
                </ul>
            </div>
        </div>

        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left">
                    <section class="at-property-sec" style="padding-top: 0px;">
                        <div class="container">
                            <ul class="nav nav-pills" style="background-color: white;">
                                <li class="active"><a href="#liked" data-toggle="tab" >Liked Advices</a></li>
                                <li><a href="#saved" data-toggle="tab" >Saved Advices</a></li>

                            </ul>

                            <div class="tab-content" style="padding-top: 60px;">
                                <div id="liked" class="tab-pane fade in active">
                                    <br>
                                    <?php foreach ($liked as $pos){
                                        $post = DB::select("SELECT * FROM blog WHERE id = '".$pos->advice_id."'")[0];?>
                                        <div class="profile-overlay clearfix wrapper-11140 ng-isolate-scope">
                                            <div class="col-lg-12 advice-box" data-redirect="advice/<?=path($post->title,$post->id);?>">
                                                <div class="col-lg-6 col-md-6 advice-banner">
                                                    <img class="advice-img" itemprop="image" src="<?php echo url('/assets/blog/'.$post->images); ?>">
                                                </div>
                                                <div class="col-lg-6 col-md-6 advice-content">
                                                    <div class="advice-inner-content">
                                                        <h3><?=$post->title; ?>
                                                            <p>
                                                                <small class="pull-left"><i class="fa fa-bars"></i> <?php echo getadvicesCategoryName($post->category); ?></small>
                                                                <small class="pull-right"><i class="fa fa-calendar"></i> <?=$post->date; ?></small>
                                                            </p>
                                                        </h3>

                                                        <p><?=$post->short_des; ?></p>

                                                    </div>
                                                    <div class="advice-bottom-content">
                                                        <?php
                                                        $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->count;
                                                        $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$post->id)[0]->count;
                                                        $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$post->id)[0]->count;
                                                        $rating = 0;
                                                        if ($total_ratings > 0){
                                                            $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->sum;
                                                            $rating = $rating_summ / $total_ratings;
                                                        }
                                                        $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$post->id." AND active = 1 ORDER BY time DESC");
                                                        $faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$post->id." AND status = 'Approved' ORDER BY id DESC");
                                                        $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$post->id."'")[0];
                                                        $total_rating =  $rating->total_rating;
                                                        $total_user = $rating->total_user;
                                                        if($total_rating==0){
                                                            $avg_rating = 0;
                                                        }
                                                        else{
                                                            $avg_rating = round($total_rating/$total_user,1);
                                                        }
                                                        ?>

                                                        <p style="width: 25%; float: left; bottom: 10px;"> <i class="fa fa-eye"></i> <?=$post->visits; ?> Visits</p>
                                                        <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
                                                        <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
                                                        <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    <?php } ?>
                                    <?php if (count($liked) == 0) {?>
                                        <center>
                                        <table>
                                            <tr>
                                                <td>
                                                    <i class="icon-basket"></i>
                                                    <?=translate('You do not have any liked advices!')?>
                                                </td>
                                            </tr>
                                        </table>
                                        </center>
                                    <?php }?>
                                </div>

                                <div id="saved" class="tab-pane fade">
                                    <br>
                                    <?php foreach ($saved as $pos){
                                        $post = DB::select("SELECT * FROM blog WHERE id = '".$pos->advice_id."'")[0];?>
                                        <div class="profile-overlay clearfix wrapper-11140 ng-isolate-scope">
                                            <div class="col-lg-12 advice-box" data-redirect="advice/<?=path($post->title,$post->id);?>">
                                                <div class="col-lg-6 col-md-6 advice-banner">
                                                    <img class="advice-img" itemprop="image" src="<?php echo url('/assets/blog/'.$post->images); ?>">
                                                </div>
                                                <div class="col-lg-6 col-md-6 advice-content">
                                                    <div class="advice-inner-content">
                                                        <h3><?=$post->title; ?>
                                                            <p>
                                                                <small class="pull-left"><i class="fa fa-bars"></i> <?php echo getadvicesCategoryName($post->category); ?></small>
                                                                <small class="pull-right"><i class="fa fa-calendar"></i> <?=$post->date; ?></small>
                                                            </p>
                                                        </h3>

                                                        <p><?=$post->short_des; ?></p>

                                                    </div>
                                                    <div class="advice-bottom-content">
                                                        <?php
                                                        $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->count;
                                                        $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$post->id)[0]->count;
                                                        $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$post->id)[0]->count;
                                                        $rating = 0;
                                                        if ($total_ratings > 0){
                                                            $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->sum;
                                                            $rating = $rating_summ / $total_ratings;
                                                        }
                                                        $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$post->id." AND active = 1 ORDER BY time DESC");
                                                        $faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$post->id." AND status = 'Approved' ORDER BY id DESC");
                                                        $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$post->id."'")[0];
                                                        $total_rating =  $rating->total_rating;
                                                        $total_user = $rating->total_user;
                                                        if($total_rating==0){
                                                            $avg_rating = 0;
                                                        }
                                                        else{
                                                            $avg_rating = round($total_rating/$total_user,1);
                                                        }
                                                        ?>

                                                        <p style="width: 25%; float: left; bottom: 10px;"> <i class="fa fa-eye"></i> <?=$post->visits; ?> Visits</p>
                                                        <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
                                                        <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
                                                        <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    <?php } ?>
                                    <?php if (count($saved) == 0) {?>
                                        <center>
                                        <table>
                                            <tr>
                                                <td>
                                                    <i class="icon-basket"></i>
                                                    <?=translate('You do not have any saved advices!')?>
                                                </td>
                                            </tr>
                                        </table>
                                        </center>
                                    <?php }?>
                                </div>


                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <?php echo $footer?>

    <script>
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });
        function getprofile(){
            event.preventDefault();
            $('#personal-modal').click();
        }
    </script>

    <a class="btn btn-primary hidden" id="personal-modal" data-popup-open="popup-personal" href="#">Get Started !</a>
    <div class="popup" id="popup-personal" data-popup="popup-personal">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-12 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('account'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style=" margin: auto;">
                        <tbody>
                        <tr>
                            <td>Name <span class="red-star">*</span></td>
                            <td><input type="text" required name="name" class="form-control" value="<?php echo Customer('name'); ?>"></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>Mobile <span class="red-star">*</span></td>
                            <td><input type="text" name="mobile" required class="form-control" value="<?php echo Customer('mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td colspan="7">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Alternate Mobile</td>
                            <td><input type="text" name="alternate_mobile" class="form-control" value="<?php echo Customer('alternate_mobile'); ?>"></td>
                            <td>&nbsp;</td>
                            <td>Email <span class="red-star">*</span></td>
                            <td><input type="text" name="email" required class="form-control" value="<?php echo Customer('email'); ?>"></td>
                        </tr>
                        <tr>
                            <td colspan="7">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Address Line 1 <span class="red-star">*</span></td>
                            <td><input type="text" name="address_line_1" required class="form-control" value="<?php echo Customer('address_line_1'); ?>"></td>
                            <td>&nbsp;</td>
                            <td>Company</td>
                            <td><input type="text" name="company" class="form-control" value="<?php echo Customer('company'); ?>"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Address Line 2</td>
                            <td><input type="text" name="address_line_2" class="form-control" value="<?php echo Customer('address_line_2'); ?>"></td>
                            <td>&nbsp;</td>
                            <td>Country <span class="red-star">*</span></td>
                            <td>
                                <select name="country" required id="country_shipping" class="form-control" onChange="getStates(this.value, 'state_shipping')">
                                    <?php
                                    foreach($countries as $country){
                                        $selected = '';
                                        $cat = Customer('country');
                                        if($cat == $country->id){
                                            $selected = 'selected';
                                        }
                                        echo '<option value="'.$country->id.'" '.$selected.'>'.$country->name.'</option>';
                                    }?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>State <span class="red-star">*</span></td>
                            <td>
                                <select name="state" required class="form-control" id="state_shipping" onChange="getCities(this.value, 'city_shipping')">
                                    <option value="<?php echo Customer('state'); ?>"><?php echo getStateName(Customer('state')); ?></option>
                                    <option value="">Please select region, state or province</option>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>City <span class="red-star">*</span></td>
                            <td>
                                <select name="city" required class="form-control" id="city_shipping">
                                    <option value="<?php echo Customer('city'); ?>"><?php echo getCityName(Customer('city')); ?></option>
                                    <option value="">Please select city or locality</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Postcode <span class="red-star">*</span></td>
                            <td><input type="text" name="postcode" required class="form-control" value="<?php echo Customer('postcode'); ?>"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img id="output" src="assets/user_image/<?php echo Customer('image'); ?>"  style="border:0px solid #FFF;max-width:150px;max-height:150px;float: right; padding-right: 20px;" alt="Your Image Here"/>
                                <script>
                                    var loadFile = function(event) {
                                        var output = document.getElementById('output');
                                        output.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </td>
                            <td>Profile Image</td>
                            <td><input type="file" name="image" onchange="loadFile(event)" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img id="output1" src="assets/user_image/<?php echo Customer('header_image'); ?>"  style="border:0px solid #FFF; max-width:150px;max-height:150px;float: right; padding-right: 20px;" alt="Your Image Here"/>
                                <script>
                                    var loadFile1 = function(event) {
                                        var output1 = document.getElementById('output1');
                                        output1.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </td>
                            <td>Banner Image 1</td>
                            <td><input type="file" name="header_image" onchange="loadFile1(event)" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img id="output2" src="assets/user_image/<?php echo Customer('header_image_1'); ?>"  style="border:0px solid #FFF;max-width:150px;max-height:150px;float: right; padding-right: 20px;" alt="Your Image Here"/>
                                <script>
                                    var loadFile2 = function(event) {
                                        var output2 = document.getElementById('output2');
                                        output2.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </td>
                            <td>Banner Image 2</td>
                            <td><input type="file" name="header_image_1" onchange="loadFile2(event)" class="form-control" ></td>
                        </tr>

                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="id" value="<?php echo customer('id'); ?>">
                        <tr>
                            <td colspan="5"><input type="submit" name="edit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-personal" href="#">x</a>
        </div>
    </div>
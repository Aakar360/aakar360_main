<?php echo $header?>
<div class="b-hero1">
    <figure style="background-image:url(&quot;<?php echo url('/assets/blog/'.$post->banner_image); ?>&quot;)" class="b-post__hero-image">
        <div data-w-id="403de785-9462-5110-ab3f-b13328bf2c04" class="b-post__hero-inner">
            <div class="b-container cc-read cc-hero">
                <div class="b-read__wrap cc-hero">
                    <h1 class="b-post__title"> <?=translate($post->title) ?></h1>
                    <!--p class="b-post__subhead">Take a look at these 19 websites made in Webflow and handpicked from our internal Slack.</p-->
                    <div class="b-post__details-wrap">
                        <div class="b-post__details-wrap-cell w-inline-block">
                            <div class="b-post__hero-detail-text"><i class="fa fa-eye" style="color: #fff; font-size: 24px;"></i> <?=$post->visits; ?> <span class="hidden-xs">Visits</span></div>
                        </div>
                        <div class="b-post__details-wrap-cell">
                            <div class="b-post__hero-detail-text"><i class="fa fa-pencil-square-o" style="color: #fff; font-size: 24px;"></i> <?=$total_reviews; ?> <span class="hidden-xs">Reviews</span></div>
                        </div>
                        <div class="b-post__details-wrap-cell">
                            <div class="b-post__hero-detail-text"><i class="fa fa-star" style="color: #fff; font-size: 24px;"></i> <?=$avg_rating; ?> <span class="hidden-xs">Ratings</span></div>
                        </div>
                        <div class="b-post__details-wrap-cell cc-last w-inline-block">
                            <div class="b-post__hero-detail-text"><i class="fa fa-thumbs-up" style="color: #fff; font-size: 24px;"></i> <?=$like; ?> <span class="hidden-xs">Likes</span></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </figure>

</div>
<div class="row" style="text-align: center;">
    <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8" style="margin-top: -105px; z-index: 1">
        <div class="header-bottom">
            <div class="header-navigation">
                <ul class="navigation navigation--horizontal">
                    <li class="navigation-item navigation-design">
                    <?php if(customer('id') !== ''){
                        $user_id = customer('id');
                        $layout_id = $post->id;

                        $save = \App\AdviceSave::where('user_id',$user_id)->where('advice_id',$layout_id)->get();

                        if(!empty($save)){
                            ?>
                            <a href="javascript:void(0)" title="Saved"><span>Saved</span><i class="fa fa-save"></i></a>
                        <?php } else{?>
                            <a href="javascript:void(0)" id="savebutton" title="Save" onclick="adviceSave(this.value);"><span>Save</span><i class="fa fa-save"></i></a>
                        <?php }} else { ?>
                        <a href="javascript:void(0)" title="Save" onClick="getModel()"><span>Save</span><i class="fa fa-save"></i></a>
                    <?php } ?>
                    </li>
                    <li class="navigation-item navigation-architecture">
                        <a href="javascript:void(0)" title="Share" onClick="emailModel()"><span>Share</span><i class="fa fa-share-alt"></i> </a>
                    </li>

                    <li class="navigation-item navigation-art">
                        <?php if(customer('id') !== ''){
                            $user_id = customer('id');
                            $layout_id = $post->id;

                            $img = \App\AdviceLike::where('user_id',$user_id)->where('advice_id',$layout_id)->get();

                            if(!empty($img)){
                                ?>
                                <a href="javascript:void(0)" title="Liked"><span>Liked</span><i class="fa fa-thumbs-up"></i></a>
                            <?php } else{?>
                                <a href="javascript:void(0)" id="likebutton" onclick="adviceLike(this.value);" title="Like"><span>Like</span><i class="fa fa-heart"></i></a>
                            <?php }} else { ?>
                            <a href="javascript:void(0)" onClick="getModel()" title="Like"><span>Like</span><i class="fa fa-heart"></i></a>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </div>
        

    </div>

</div>
<section class="pdtopbtm-50">
    <div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="post-content">
                <?=nl2br(translate($post->content)) ?>
            </div>
        </div>

        <div class="col-md-12 col-lg-12" style="">

            <div class="clearfix"></div>


            <div class="panel-group" id="accordion" style="padding-top: 20px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" style="color: #ffffff;">
                            <h4 class="panel-title">
                                Ratings & Reviews
                            </h4>
                        </a>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">

                            <div class="feedback-container">
                                <div class="col-lg-12">
                                    <div class="col-lg-4">
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style="margin-bottom: 0px; font-family: Sans-Serif; font-style: bold;">Avarage rating <?=$avg_rating;?></h5>
                                                <div class="rating" style="padding-bottom: 10px;">
                                                    <?php
                                                    $a=$avg_rating;
                                                    $b = explode('.',$a);
                                                    $first = $b[0];
                                                    $rr = $avg_rating;
                                                    $i = 0;
                                                    while($i<5){ $i++;?>
                                                        <i class="star<?=($i<=$avg_rating) ? '-selected' : '';?>"></i>
                                                        <?php $rr--; }
                                                    echo '</div>';
                                                    ?>
                                                    <h5 style=" font-family: Sans-Serif; font-style: bold;">Total rating <?=$total_user;?></h5>
                                            </li>
                                        </ul>
                                        <hr>
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style=" font-family: Sans-Serif; font-style: bold;">Total comment <?=$total_reviews; ?></h5>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-8 reviews-graph">
                                        <ul>
                                            <li class="float-left">
                                                <?php
                                                if($rating5 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">5 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating5->rating5_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating5->rating5_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating5->rating5_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating4 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">4 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating4->rating4_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating4->rating4_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating4->rating4_user;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating3 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">3 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating3->rating3_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating3->rating3_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating3->rating3_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating2 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">2 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating2->rating2_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating2->rating2_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating2->rating2_user;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating1 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">1 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating1->rating1_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating1->rating1_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating1->rating1_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12" style="padding-bottom: 10px;">
                                <form action="<?php echo url('review');?>" method="post" class="form-horizontal single">
                                    <?=csrf_field();?>
                                    <h5 style="float: left; width: 100%; margin-top: 10px; border-top: 1px solid #000000; padding-top: 20px;"><?=translate('Add a review')?> :</h5>
                                    <div id="response" style="float: left; width: 100%"></div>
                                    <?php if(customer('id') !== ''){?>
                                        <fieldset style="float: left; width: 100%;">
                                            <div class="col-md-12">
                                                <input name="name" readonly value="<?=customer('id')?>" class="form-control" type="hidden">
                                                <input name="product" value="<?=$post->id?>" class="form-control" type="hidden">

                                                <div class="form-group col-md-12">
                                                    <label class="control-label"><?=translate('Rating')?></label>
                                                    <div id="star-rating">
                                                        <input type="radio" name="rating" class="rating" value="1" />
                                                        <input type="radio" name="rating" class="rating" value="2" />
                                                        <input type="radio" name="rating" class="rating" value="3" />
                                                        <input type="radio" name="rating" class="rating" value="4" />
                                                        <input type="radio" name="rating" class="rating" value="5" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label"><?=translate('Review')?></label>
                                                    <textarea name="review" type="text" rows="5" class="form-control"></textarea>
                                                </div>
                                                <button data-product="<?=$post->id?>" name="submit" class="btn btn-primary" ><?=translate('submit')?></button>
                                            </div>
                                        </fieldset>
                                    <?php } else { ?>
                                        <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                            <div class="col-md-12 text-center">
                                                <button class="btn btn-primary" onClick="getModel()" type="button" value="Login">Login</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </form>
                                <br><br><br>
                                <p>
                                    <?php
                                    $count = 0;
                                    foreach($reviews as $review){
                                        $usid = $review->name;
                                        $uimg = \App\Customer::where('id',$usid)->first();
                                        echo '<img class="review-image" src="assets/user_image/'; if($uimg->image != ''){ echo $uimg->image; } else { echo 'user.png'; }; echo'">
											<div class="review-meta"><b>'.$uimg->name.'</b><br/>
											<span class="time">'.date('M d, Y',$review->time).'</span><br/></div>
											<div class="review">
												<div class="rating pull-right">';
                                        $rr = $review->rating; $i = 0; while($i<5){ $i++;?>
                                            <i class="star<?=($i<=$review->rating) ? '-selected' : '';?>"></i>
                                            <?php $rr--; }
                                        echo '</div>
											<div class="clearfix"></div>
											<p>'.nl2br($review->review).'</p></div>';
                                        $count++;
                                    }
                                    if($count > 0){
                                        echo '<hr/>';
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" style="color: #ffffff;"><h4 class="panel-title">
                                FAQ
                            </h4></a>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <section class="at-property-sec at-property-right-sidebar">
                                <div class="">
                                    <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                        <div class="col-md-12 text-center">
                                            <?php if(customer('id') !== ''){ ?>
                                                <button class="btn btn-primary" type="button" data-popup-open="popup-question" value="Ask a Question">Ask a Question</button>
                                            <?php } else { ?>
                                                <button class="btn btn-primary" onClick="getModel()" type="button" value="Ask a Question">Ask a Question</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul>
                                                <?php foreach($faqs as $faq){?>
                                                    <li>
                                                        <input type="checkbox" checked>
                                                        <i></i>
                                                        <h5>Q : <?php echo $faq->question; ?><br><small>
                                                                <?php $u = $faq->user_id;
                                                                $user = \App\Customer::where('id',$u)->first();
                                                                ?>
                                                                by <?php echo $user->name; ?> on <?php $d = $faq->time; echo date('d M, Y', strtotime($d)); ?></small></h5>
                                                        <p>A : <?php echo $faq->answer; ?></p>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </section>

                            <!-- Ask a Question Popup Start -->
                            <div class="popup" id="popup-question" data-popup="popup-question">
                                <div class="popup-inner">
                                    <div class="col-md-12">
                                        <h2 style="text-transform: none;" class="text-center">Ask a Question</h2>
                                        <form id="contact_form" action="<?php echo url('advices-faq');?>" method="post">
                                            <?=csrf_field() ?>
                                            <input type="hidden" name="advice_id" value="<?=$post->id;?>" />
                                            <div class="col-md-12 col-sm-12 text-center">
                                                <textarea class="form-control" name="question" rows="5" placeholder="Write question here" required="required"></textarea>
                                                <br>
                                                <button class="btn btn-primary" type="submit">SUBMIT</button>
                                            </div>
                                        </form>
                                    </div>
                                    <a class="popup-close" data-popup-close="popup-question" href="#">x</a>
                                </div>
                            </div>
                            <!-- Ask a Question Popup End -->
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" style="background-color: #f4f4f4; border: 0; box-shadow: 0 0px 0px !important;">
                    <ul class="nav nav-pills" id="myTab">
                        <li class="active"><a href="#related" data-toggle="tab" >Related Advices</a></li>
                        <li ><a href="#recently" data-toggle="tab" >Recently Viewed</a></li>
                        <li class=""><a href="#plan" data-toggle="tab" >Auditorial Advices</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="related" class="tab-pane fade in active" style="background-color: #f4f4f4;">
                            <br>
                            <?php foreach ($related_advices as $postr){ ?>
                                <div class="profile-overlay clearfix wrapper-11140 ng-isolate-scope">
                                    <div class="col-lg-12 advice-box" data-redirect="advice/<?=path($postr->title,$postr->id);?>">
                                        <div class="col-lg-6 col-md-6 advice-banner">
                                            <img class="advice-img" itemprop="image" src="<?php echo url('/assets/blog/'.$postr->images); ?>">
                                        </div>
                                        <div class="col-lg-6 col-md-6 advice-content">
                                            <div class="advice-inner-content">
                                                <h3><?=$postr->title; ?>
                                                    <p>
                                                        <small class="pull-left"><i class="fa fa-bars"></i> <?php echo getadvicesCategoryName($postr->category); ?></small>
                                                        <small class="pull-right"><i class="fa fa-calendar"></i> <?=$postr->date; ?></small>
                                                    </p>
                                                </h3>

                                                <p><?=$postr->short_des; ?></p>

                                            </div>
                                            <div class="advice-bottom-content">
                                                <?php
                                                $total_ratings = \App\AdviceReview::where('active','=', '1')->where('advice_id',$postr->id)->count();
                                                $total_reviews = \App\AdviceReview::where('active','=', '1')->where('review','!=','')->where('advice_id',$postr->id)->count();
                                                $like = \App\AdviceLike::where('advice_id',$postr->id)->count();
                                                $rating = 0;
                                                if ($total_ratings > 0){
                                                    $rating_summ = \App\AdviceReview::where('active','=', '1')->where('advice_id',$postr->id)->sum('rating');
                                                    $rating = $rating_summ / $total_ratings;
                                                }
                                                $reviews = \App\AdviceReview::where('active','=', '1')->where('advice_id',$postr->id)->orderBy('time','DESC')->get();
                                                $faqs = \App\AdviceFaq::where('advice_id',$postr->id)->where('status',' =', 'Approved')->orderBy('id','DESC')->get();
                                                $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$postr->id."'")[0];
                                                $total_rating =  $rating->total_rating;
                                                $total_user = $rating->total_user;
                                                if($total_rating==0){
                                                    $avg_rating = 0;
                                                }
                                                else{
                                                    $avg_rating = round($total_rating/$total_user,1);
                                                }
                                                ?>

                                                <p style="width: 25%; float: left; bottom: 10px;margin: 0;"> <i class="fa fa-eye"></i> <?=$postr->visits; ?> Visits</p>
                                                <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
                                                <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
                                                <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if(!empty($recent_viewed)){ ?>
                            <div id="recently" class="tab-pane fade " style="background-color: #f4f4f4;">
                                <br>
                                <?php foreach ($recent_viewed as $poste){ ?>
                                    <div class="profile-overlay clearfix wrapper-11140 ng-isolate-scope">
                                        <div class="col-lg-12 advice-box" data-redirect="advice/<?=path($poste->title,$poste->id);?>">
                                            <div class="col-lg-6 col-md-6 advice-banner">
                                                <img class="advice-img" itemprop="image" src="<?php echo url('/assets/blog/'.$poste->images); ?>">
                                            </div>
                                            <div class="col-lg-6 col-md-6 advice-content">
                                                <div class="advice-inner-content">
                                                    <h3><?=$poste->title; ?>
                                                        <p>
                                                            <small class="pull-left"><i class="fa fa-bars"></i> <?php echo getadvicesCategoryName($poste->category); ?></small>
                                                            <small class="pull-right"><i class="fa fa-calendar"></i> <?=$poste->date; ?></small>
                                                        </p>
                                                    </h3>

                                                    <p><?=$poste->short_des; ?></p>

                                                </div>
                                                <div class="advice-bottom-content">
                                                    <?php
                                                    $total_ratings = \App\AdviceReview::where('active','=', '1')->where('advice_id',$poste->id)->count();
                                                    $total_reviews = \App\AdviceReview::where('active','=', '1')->where('review','!=','')->where('advice_id',$poste->id)->count();
                                                    $like = \App\AdviceLike::where('advice_id',$poste->id)->count();
                                                    $rating = 0;
                                                    if ($total_ratings > 0){
                                                        $rating_summ = \App\AdviceReview::where('active','=', '1')->where('advice_id',$poste->id)->sum('rating');
                                                        $rating = $rating_summ / $total_ratings;
                                                    }
                                                    $reviews = \App\AdviceReview::where('active','=', '1')->where('advice_id',$poste->id)->orderBy('time','DESC')->get();
                                                    $faqs = \App\AdviceFaq::where('advice_id',$poste->id)->where('status',' =', 'Approved')->orderBy('id','DESC')->get();
                                                    $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$poste->id."'")[0];
                                                    $total_rating =  $rating->total_rating;
                                                    $total_user = $rating->total_user;
                                                    if($total_rating==0){
                                                        $avg_rating = 0;
                                                    }
                                                    else{
                                                        $avg_rating = round($total_rating/$total_user,1);
                                                    }
                                                    ?>

                                                    <p style="width: 25%; float: left; bottom: 10px;margin: 0;"> <i class="fa fa-eye"></i> <?=$poste->visits; ?> Visits</p>
                                                    <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
                                                    <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
                                                    <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div id="plan" class="tab-pane fade" style="background-color: #f4f4f4;">
                            <br>
                            <?php foreach ($auditorials as $posto){ ?>
                                <div class="profile-overlay clearfix wrapper-11140 ng-isolate-scope">
                                    <div class="col-lg-12 advice-box" data-redirect="advice/<?=path($posto->title,$posto->id);?>">
                                        <div class="col-lg-6 col-md-6 advice-banner">
                                            <img class="advice-img" itemprop="image" src="<?php echo url('/assets/blog/'.$posto->images); ?>">
                                        </div>
                                        <div class="col-lg-6 col-md-6 advice-content">
                                            <div class="advice-inner-content">
                                                <h3><?=$posto->title; ?>
                                                    <p>
                                                        <small class="pull-left"><i class="fa fa-bars"></i> <?php echo getadvicesCategoryName($posto->category); ?></small>
                                                        <small class="pull-right"><i class="fa fa-calendar"></i> <?=$posto->date; ?></small>
                                                    </p>
                                                </h3>

                                                <p><?=$posto->short_des; ?></p>

                                            </div>
                                            <div class="advice-bottom-content">
                                                <?php
                                                $total_ratings = \App\AdviceReview::where('active','=', '1')->where('advice_id',$posto->id)->count();
                                                $total_reviews = \App\AdviceReview::where('active','=', '1')->where('review','!=','')->where('advice_id',$posto->id)->count();
                                                $like = \App\AdviceLike::where('advice_id',$posto->id)->count();
                                                $rating = 0;
                                                if ($total_ratings > 0){
                                                    $rating_summ = \App\AdviceReview::where('active','=', '1')->where('advice_id',$posto->id)->sum('rating');
                                                    $rating = $rating_summ / $total_ratings;
                                                }
                                                $reviews = \App\AdviceReview::where('active','=', '1')->where('advice_id',$posto->id)->orderBy('time','DESC')->get();
                                                $faqs = \App\AdviceFaq::where('advice_id',$posto->id)->where('status',' =', 'Approved')->orderBy('id','DESC')->get();
                                                $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$posto->id."'")[0];
                                                $total_rating =  $rating->total_rating;
                                                $total_user = $rating->total_user;
                                                if($total_rating==0){
                                                    $avg_rating = 0;
                                                }
                                                else{
                                                    $avg_rating = round($total_rating/$total_user,1);
                                                }
                                                ?>

                                                <p style="width: 25%; float: left; bottom: 10px;margin: 0;"> <i class="fa fa-eye"></i> <?=$posto->visits; ?> Visits</p>
                                                <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
                                                <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
                                                <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if(!empty($link)){?>
                    <div class="container">
                        <div class="blockquote blockquote--style-1">
                            <div class="row inner-div">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <img src="<?=url('/assets/products/'.image_order($link->image))?>" style="width:50%;height:auto;"/>
                                    </div>
                                    <div class="col-md-7" style="padding-top: 25px;">
                                        <h3><?=$link->content; ?></h3>
                                    </div>
                                    <div class="col-md-2" style="padding:15px 0 0 0">
                                        <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                            GET STARTED
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    </div>

</section>





<?php echo $footer?>


<a class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login" href="#">Get Started !</a>
<div class="popup" id="popup-login" data-popup="popup-login">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-6 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                <?=csrf_field() ?>
                <fieldset>
                    <div class="form-group">
                        <label class="control-label"><?=translate('E-mail') ?></label>
                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?=translate('Password') ?></label>
                        <input name="password" type="password" class="form-control"  />
                    </div>
                    <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                </fieldset>
            </form>
            <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
        </div>
        <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
    </div>
</div>
<!-- Services Popup End -->



<a class="btn btn-primary hidden" id="emailmodel" data-popup-open="popup-email" href="#">Get Started !</a>
<div class="popup" id="popup-email" data-popup="popup-email">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-6 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('email-send'); ?>" method="post" class="form-horizontal single">
                <?=csrf_field() ?>
                <fieldset>
                    <input type="hidden" name="advice_id" value="<?=$post->id; ?>">
                    <div class="form-group">
                        <label class="control-label"><?=translate('E-mail') ?></label>
                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                    </div>

                    <input name="login" type="submit" value="Send" class="btn btn-primary" />
                </fieldset>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-email" href="#">x</a>
    </div>
</div>
<!-- Services Popup End -->




<script src="<?=$tp;?>/assets/jquery.flexslider.js"></script>
<script src="<?=$tp;?>/assets/jquery.zoom.js"></script>

<script>
    jQuery(window).on('load', function(){
        var $ = jQuery;
        var $container = $('.grid');
        $container.masonry({
            columnWidth:10,
            gutterWidth: 15,
            itemSelector: '.grid-item'
        });
    });
    (function() {

        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
            new CBPFWTabs( el );
        });

    })();
    $(function() {
//----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
//----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });
    $( document ).ready(function() {

        $('#star-rating').rating();
        $('#carousel').flexslider({
            animation: "slide",
            controlNav: true,
            animationLoop: true,
            slideshow: true,
            itemWidth: 210,
            itemMargin: 5,
            minItems: 4,
            maxItems: 6,
            asNavFor: '#slider'
        });

        $('#slider').flexslider({
            animation: "slide",
            controlNav: true,
            animationLoop: true,
            slideshow: true,
            sync: "#carousel",
            touch: true,
            keyboard: true,
            smoothHeight: true,
        });
        $('.select2').select2();
        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
        allPrevBtn = $('.prevBtn');

        allWells.hide();
        var ajax_valid = true;
        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-success').addClass('btn-default');
                $item.addClass('btn-success');
                //allWells.hide('slide', {direction: 'left'}, 1000);
                //allWells.hide();
                allWells.hide();
                $target.show('slide', {direction: 'right'}, 1000);

                //$target.find('input:eq(0)').focus();

            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                isValid = true;
            if(ajax_valid){
                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }
            }else{
                isValid = false;
            }

            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
        });
        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url'],input[type='tel'],select,input[type='radio'],input[type='checkbox'],textarea"),
                isValid = true;
            allWells.hide();
            curStep.prev().show('slide', {direction: 'left'}, 1000);
        });

        $('div.setup-panel div a.btn-success').trigger('click');

        var packprice = 0.00;
        $('a.btn').on('click', function(e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            var price = $(this).attr('data-rate');
            $('div.adons-inner.selected').each(function(){
                price = parseFloat(price) + parseFloat($(this).attr('data-price'));
            });
            packprice = $(this).attr('data-rate');
            $('input[name=package]').val(id);
            $("select[name=plan_type]").val(id).trigger("change");

            $('a.btn').each(function(){
                $(this).html('Select').removeClass('selected');
            });
            $(this).addClass('selected');
            $(this).html('<span class="tickgreen">✔</span> Selected');
            $('form[name=checkForm]').attr('action', 'add-plan-in-session');
            $('#total').html("Total : <?php echo c(''); ?> "+price);
        });
        $('.adons-inner').on('click', function(){
            if($(this).hasClass('selected')){
                $(this).removeClass('selected');
            }else{
                $(this).addClass('selected');
            }
            var price = 0.00;
            var adons = [];
            $('div.adons-inner.selected').each(function(){
                adons.push($(this).attr('data-id'));
            });
            $("select[name=adons]").val(adons).trigger("change");
            $('input[name=adons]').val(adons.join(','));

            //alert(packprice);

            $('div.adons-inner.selected').each(function(){
                price = price + parseFloat($(this).attr('data-price'));
            });
            if(price == 0.00) {
                price = packprice;
            } else {
                price = parseFloat(packprice) + parseFloat(price);
            }
            $('#total').html("Total : <?php echo c(''); ?> "+price);
            $('input[name=package-price]').val(price);
        });

        //$('#total').html("Total : <?php echo c(''); ?> "+packprice);

        $('button[name=checkout]').on('click', function(){
            $('form[name=checkForm]').submit();
        });
    });
    function flipH(id){
        $('#img'+id).removeClass('flippedV');
        $('button[data-flip=v'+id+']').removeClass('active');
        if($('#img'+id).hasClass('flippedH')){
            $('#img'+id).removeClass('flippedH');
            $('button[data-flip=h'+id+']').removeClass('active');
        }else{
            $('#img'+id).addClass('flippedH');
            $('button[data-flip=h'+id+']').addClass('active');
        }
    }
    function flipV(id){
        $('#img'+id).removeClass('flippedH');
        $('button[data-flip=h'+id+']').removeClass('active');
        if($('#img'+id).hasClass('flippedV')){
            $('#img'+id).removeClass('flippedV');
            $('button[data-flip=v'+id+']').removeClass('active');
        }else{
            $('#img'+id).addClass('flippedV');
            $('button[data-flip=v'+id+']').addClass('active');
        }
    }
    function getModel(){
        event.preventDefault();
        $('#open-service-modal1').click();
    }

    function emailModel(){
        event.preventDefault();
        $('#emailmodel').click();
    }

</script>
<script>
    function adviceLike() {
        $.ajax({
            type: "POST",
            url: "advice-like",
            data:'_token=<?=csrf_token();?>&advice_id=<?=$post->id; ?>',
            success: function(data){
                $('#likebutton').html('<i class="fa fa-heart"></i> Liked');
            }
        });
    }
</script>

<script>
    function adviceSave() {
        $.ajax({
            type: "POST",
            url: "advice-save",
            data:'_token=<?=csrf_token();?>&advice_id=<?=$post->id; ?>',
            success: function(data){
                $('#savebutton').html('<i class="fa fa-save"></i> Saved');
            }
        });
    }
</script>
<?php echo $header?>


    <div class="profile-root bg-white ng-scope">
        <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
            <div class="clearfix wrapper-1140 two-bgs">
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?php if(!empty($dimg)){ $dimg->image; } ?>" class="cover">
                    </div>
                </div>
            </div>
            <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
                <img itemprop="image" class="profile-icon" src="assets/login.png">
                <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                    <a itemprop="url" class="url">
                        <?=translate('Change Password')?>
                    </a>
                </h1>
            </div>
        </div>
        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left" style="padding-top: 50px;">
                    <div class="account" style="width: 980px;">

                        <?php
                        if (isset($error)){
                            echo $error;
                        }
                        ?>
                        <form action="<?php echo url('change-password') ?>" method="post" class="form-horizontal single">
                            <?=csrf_field() ?>
                            <fieldset style="width: 33.33%;text-align: center; margin: auto;">
                                <div class="form-group">
                                    <label class="control-label">Old Password</label>
                                    <input name="oldpassword" type="password" class="form-control"  />
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?=translate('Password') ?></label>
                                    <input name="password" type="password" class="form-control"  />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Confirm Password</label>
                                    <input name="confirm_password" type="password" class="form-control"  />
                                </div>
                                <input name="login" type="submit" value="Submit" class="btn btn-primary" />
                            </fieldset>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>




<?php echo $footer?>

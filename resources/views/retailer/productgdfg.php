<?php echo $header?>

<!-- Inner page heading end -->

<!-- Property start from here -->
<section class="at-property-sec at-property-right-sidebar" style="padding: 0;">

    <div class="container" id="<?=$product->id;?>">
        <?php
        $pvalidity = -1;
        if($product->is_variable){
            date_default_timezone_set("Asia/Kolkata");

            $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
            if($pvalidity < 0){
                $pvalidity = 0;
            }
        }
        ?>
        <span class="countdown-timer" data-key="<?=$product->id;?>"><?=$pvalidity;?></span>
        <div class="content product-page pull-left">
            <?php if(Session::has('successx')) {
                $msgs = Session::get('successx');
                foreach ($msgs as $msg) {
                    echo '<div class="alert alert-success auto-close">' . $msg . '</div>';
                }

            }
            if(Session::has('msgx')) {
                $msgs = Session::get('msgx');
                foreach ($msgs as $msg) {
                    echo '<div class="alert alert-danger auto-close">' . $msg . '</div>';
                }
            }
            ?>
            <div class="col-md-8">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner text-center">
                        <?php for($i=0; $i<count($images); $i++){
                            if($i == 0){?>
                                <div class="item active" style="padding: 2px;">
                                    <img src="<?=url('/assets/products/'.$images[$i])?>"/>

                                </div>
                            <?php }  else {?>
                                <div class="item" style="padding: 2px;">
                                    <img src="<?=url('/assets/products/'.$images[$i])?>"/>
                                </div>
                            <?php }} ?>
                    </div>
                    <!-- End Carousel Inner -->
                    <?php if(count($images) > 1){ ?>
                        <ul class="pro-image-slider" data-slick='{"slidesToShow": 4, "slidesToScroll": 1}'>
                            <?php for($i=0; $i<count($images); $i++){
                                if($i == 0){?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active">
                                        <a href="#"><img src="<?=url('/assets/products/thumbs/'.$images[$i])?>"/></a>
                                    </li>
                                <?php } else {?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>">
                                        <a href="#"><img src="<?=url('/assets/products/thumbs/'.$images[$i])?>"/></a>
                                    </li>
                                <?php }} ?>
                        </ul>
                    <?php } ?>
                </div>
                <?php if(customer('user_type') != 'institutional' && $vtype != 'institutional'){ ?>
                    <div class="clearfix" style="width: 100%; margin-top: 10px;"></div>
                    <div class="col-md-12"><b>You are looking for <?php if(isset($_COOKIE['cities'])) {
                            echo $_COOKIE['cities'];
                            } ?> </b></div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <select name="select_city" class="form-control">
                                <option value="">Please select</option>
                                <?php foreach($cities as $city){ ?>
                                <option value="<?php echo $city->name; ?>"><?php echo $city->name; ?></option>
                                <?php } ?>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" style="padding: 9px 12px;">Save</button>
                            </span>
                        </div>
                    </div>
                <?php } ?>
                <?php if(!count($variants) && customer('user_type') != 'institutional' && $vtype != 'institutional'){ ?>
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            Buy 5 Tonnes & above. Get special price ! Call Now on 06263956550
                        </div>
                    </div>
                <?php } ?>


            </div>
            <div class="col-md-4">
                <a href="<?=url('/products/'.$cat->path)?>" class=""><?=translate($cat->name)?></a>
                <h3 style="font-family: sans-serif; !important; font-size: 15px;"><?=translate($product->title)?></h3>
                <?php if($product->rating_view != '0'){ ?>
                    <div class="rating">
                        <?php $tr = $rating; $i = 0; while($i<5){ $i++;?>
                            <i class="star<?=($i<=$rating) ? '-selected' : '';?>"></i>
                            <?php $tr--; }?>
                        <b> <?=$total_ratings.' '.translate('Reviews')?> </b>
                    </div>
                <?php }else{ ?>
                    <div class="rating"><i class="star"></i><i class="star"></i><i class="star"></i><i class="star"></i><i class="star"></i>&nbsp;&nbsp;(<b>0 <?=translate('Reviews');?></b>)</div>
                <?php } ?>
                <input type="hidden" id="min-qty" value="<?=$product->min_qty;?>"/>
                <?php
                if($product->min_qty > 0){ ?>
                    <div class="alert alert-warning">
                        Minimum Order Quantity is <?=$product->min_qty;?>
                    </div>
                <?php } ?>
                <?php if ($product->quantity > 0) { ?>
                    <div class="order">
                        <?php
                        $all_options = json_decode($product->options,true);
                        if(!empty($all_options)){
                            ?>
                            <form class="options" style="background:rgb(249, 250, 252)">
                                <?php
                                foreach($all_options as $i=>$row){
                                    $type = $row['type'];
                                    $name = $row['name'];
                                    $title = $row['title'];
                                    $option = $row['option'];
                                    ?>
                                    <div class="option">
                                        <h6><?php echo $title.' :';?></h6>
                                        <?php
                                        if($type == 'radio'){
                                            ?>
                                            <div class="custom_radio">
                                                <?php
                                                $i=1;
                                                foreach ($option as $op) {
                                                    ?>
                                                    <label for="<?php echo 'radio_'.$i; ?>" style="display: block;"><input type="radio" name="<?php echo $name;?>" value="<?php echo $op;?>" id="<?php echo 'radio_'.$i; ?>"><?php echo $op;?></label>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        } else if($type == 'text'){
                                            ?>
                                            <textarea class="form-control" rows="2" style="width:100%" name="<?php echo $name;?>"></textarea>
                                            <?php
                                        } else if($type == 'select'){
                                            ?>
                                            <select name="<?php echo $name; ?>" class="form-control" type="text">
                                                <option value=""><?php echo translate('Choose one'); ?></option>
                                                <?php
                                                foreach ($option as $op) {
                                                    ?>
                                                    <option value="<?php echo $op; ?>" ><?php echo $op; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <?php
                                        } else if($type == 'multi_select') {
                                            $j=1;
                                            foreach ($option as $op){
                                                ?>
                                                <label for="<?php echo 'check_'.$j; ?>" style="display: block;">
                                                    <input type="checkbox" id="<?php echo 'check_'.$j; ?>" name="<?php echo $name;?>[]" value="<?php echo $op;?>">
                                                    <?php echo $op;?>
                                                </label>
                                                <?php
                                                $j++;
                                            }
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </form>
                            <?php
                        }
                        ?>
                        <?php if(count($bulk_discounts)){
                            if(customer('user_type') !== ''){
                                if(customer('user_type') != 'institutional' && $vtype != 'institutional') {
                                    ?>
                                    <div class="table-responsive" >
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr class="bg-blue">
                                                <th colspan="3" class="text-center text-success">Buy More Save More</th>
                                            </tr>
                                            <tr class="bg-blue">
                                                <th class="text-center">Quantity</th>
                                                <th class="text-center">Extra Discount<br>(per unit)</th>
                                                <th class="text-center">You Save</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($bulk_discounts as $bd){ $saving = 0.00; ?>
                                                <tr>
                                                    <td>
                                                        <?=$bd->quantity; ?> or more
                                                    </td>
                                                    <td class="text-right">
                                                        <?php
                                                        if($bd->discount_type == 'Flat'){
                                                            echo c($bd->discount);
                                                            $saving = $bd->quantity*$bd->discount;
                                                        }else{
                                                            $dis = number_format(($bd->discount*$prc)/100,2,'.', '');
                                                            echo c(($prc-$dis));
                                                            $saving = $bd->quantity*($prc-$dis);
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?=c($saving); ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } } } ?>

                        <?php if(count($variants) && customer('user_type') != 'institutional' && $vtype != 'institutional'){ ?>
                            <div class="alert alert-info">
                                Buy 5 Tonnes & above. Get special price ! Call Now on 06263956550
                            </div>
                        <?php } ?>
                        <?php if(!count($variants)){ ?>
                            <div class="price-box">
                                <table class="price-table" style="margin-bottom: 15px;">
                                    <?php if(count($bulk_discounts) || customer('user_type') != 'institutional'){ ?>
                                        <tr>
                                            <td class="text-left">Unit Price</td>
                                            <td class="text-right"><strike style="font-size: 16px;"><?=c($display_price); ?></strike></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Unit Sale Price</td>
                                            <td class="text-right"><b style="font-size: 18px;" class="dynamic" id="sale_price"><?=c($display_sale); ?></b></td>
                                        </tr>
                                    <?php }else{ ?>
                                        <tr>
                                            <td class="text-left">Unit Price</td>
                                            <td class="text-right"><b style="font-size: 18px;" class="dynamic" id="sale_price"><?=c($product->institutional_price); ?></b></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                                <?php if(customer('user_type') != 'institutional' && $vtype != 'institutional') { ?>
                                    <p class="final-price dynamic">
                                        <?= c($display_sale * ($product->min_qty != 0 ? $product->min_qty : 1)); ?>
                                    </p>

                                    <?php
                                }
                                if(count($bulk_discounts) || customer('user_type') != 'institutional'){
                                    if(customer('user_type') !== '' && $display_price != 0){ ?>
                                        <p class="final-discount dynamic">You save : <?=c(($display_price - $display_sale)*($product->min_qty != 0 ? $product->min_qty : 1)); ?> (<?=round((($display_price - $display_sale)/$display_price)*100,2);?>%)</p>
                                    <?php } } ?>
                                <div id="error" style="display: none;"></div>
                                <div class="quantity-select">
                                    <div class="dec rease">-</div>
                                    <input name="quantity" class="quantity" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                                    <div class="inc rease">+</div>
                                </div>
                                <?php if(customer('user_type') !== ''){
                                    if(customer('user_type') != 'institutional' && $vtype != 'institutional'){ ?>
                                        <button class="add-cart btn btn-primary" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Add to cart')?></button>
                                    <?php } }else{ ?>
                                    <button class="btn btn-primary" data-id="<?=$product->id?>" data-redirect="<?=url('login')?>"><i class="icon-basket"></i> <?=translate('Add to cart')?></button>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <div class="order">
                        <p>Quantity unavailable</p>
                    </div>
                <?php } ?>


                <div class="option" style="margin-top: 15px;">
                    <?php if(customer('id') !== ''){
                        $user_id = customer('id');
                        $product_id = $product->id;

                        $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");
                        if(customer('user_type') == 'institutional' && count($variants) == 0){
                            if(customer('rfr') == 0){?>
                                <button class="btn btn-primary book_now" data-id="<?=$product->id?>" style="width: 100%;margin-bottom: 10px;"><i class="icon-basket"></i> <?=translate('book now')?></button>
                            <?php }else{ ?>
                                <button class="btn btn-primary request-rate" data-id="<?=$product->id?>" style="width: 100%;margin-bottom: 10px;"><i class="icon-inbox"></i> <?=translate('request for rate')?></button>
                            <?php }
                        }
                        if(!empty($img)){
                            ?>
                            <button class="btn btn-primary likebutton" id="likebutton" style="width: 100%;"><span class="tickgreen">✔</span> Added to Project</button>
                        <?php } else{?>
                            <button class="btn btn-primary likebutton" id="likebutton" onclick="productWishlist(this.value);" style="width: 100%; display: none;"><i class="fa fa-heart"></i> Add to Project</button>
                        <?php }} else { ?>
                        <button class="btn btn-primary" onClick="getModel()" style="width: 100%; display: none;"><i class="fa fa-heart"></i> Add to Project</button>
                    <?php } ?>
                </div>
                <div class="option" style="margin-top: 15px;">
                    <?php if(customer('id') !== ''){
                        $user_id = customer('id');
                        $product_id = $product->id;

                        $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");
                        if(customer('user_type') == 'institutional' && count($variants) == 0){
                            if(customer('rfr') == 0){?>
                                <button class="btn btn-primary book_now" data-id="<?=$product->id?>" style="width: 100%;margin-bottom: 10px;"><i class="icon-basket"></i> <?=translate('book now')?></button>
                            <?php }else{ ?>
                                <button class="btn btn-primary request-rate" data-id="<?=$product->id?>" style="width: 100%;margin-bottom: 10px;"><i class="icon-inbox"></i> <?=translate('request for rate')?></button>
                            <?php }
                        }
                        if(!empty($img)){
                            ?>
                            <button class="btn btn-primary likebutton" id="likebutton" style="width: 100%;"><span class="tickgreen">✔</span> Added to Project</button>
                        <?php } else{?>
                            <button class="btn btn-primary likebutton" id="likebutton" onclick="productWishlist(this.value);" style="width: 100%; display: none;"><i class="fa fa-heart"></i> Add to Project</button>
                        <?php }} else { ?>
                        <button class="btn btn-primary" onClick="getModel()" style="width: 100%; display: none;"><i class="fa fa-heart"></i> Add to Project</button>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-4" id="productDiv">
                <?php if(customer('user_type') !== ''){
                    if(customer('user_type') != 'institutional' && $vtype != 'institutional') {
                        if(!count($variants)){?>
                            <button style="width: 100%;" class="add-cart btn btn-primary  pull-right" id="noVar" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Add to cart')?></button>
                        <?php } }else{?>

                        <button style="width: 50%;" class="byBase btn btn-primary  pull-left active" data-id="<?=$product->id?>"><?=translate('Book By Basic')?></button>
                        <button style="width: 50%;" class="byVar btn btn-primary  pull-left" data-id="<?=$product->id?>"><?=translate('Book By Size')?></button>

                        <!--<select name="type" class="form-control" id="bookBy">
                            <option value="product">Book By Product</option>
                            <option value="variants">Book By Variants</option>
                        </select>-->
                        <div class="quantity-select productDiv" style="margin-top: 10px">
                            <div class="dec rease">-</div>
                            <input name="quantity" class="quantity productQty" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                            <div class="inc rease">+</div>
                        </div>
                        <select name="unit" class="form-control productDiv" style="float: right;max-width: 50%; margin-top: 5px;">
                            <?php foreach($wunits as $unit){ ?>
                                <option value="<?=$unit->id?>"><?=$unit->symbol?></option>
                            <?php } ?>
                        </select>
                    <?php }
                }?>

                <?php if(customer('user_type') !== ''){
                    if(customer('user_type') != 'institutional' && $vtype != 'institutional') {?>
                        <div class="error productDiv" ></div>
                        <button style="width: 100%;display:none;" class="add-cart btn btn-primary  pull-right productDiv" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Add to cart')?></button>
                    <?php }else{ ?>
                        <div class="error productDiv" style="display:none;"></div>
                        <?php if(customer('rfr') == 0){?>
                            <button class="btn btn-primary book_now productDiv" data-id="<?=$product->id?>" style="width: 100%;margin-bottom: 10px;margin-top: 15px;"><i class="icon-basket"></i> <?=translate('book now')?></button>
                        <?php }else{ ?>
                            <button class="btn btn-primary request-rate productDiv" data-id="<?=$product->id?>" style="width: 100%;margin-bottom: 10px;margin-top: 15px;"><i class="icon-inbox"></i> <?=translate('request for rate')?></button>
                        <?php }
                    }
                }else{
                    if(!count($variants)){ ?>
                        <div class="error productDiv" style="display:none;"></div>
                        <button style="width: 100%;" class="btn btn-primary  pull-right productDiv" data-redirect="<?=url('login')?>" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Add to cart')?></button>
                    <?php } } ?>
                <div class="text-center">
                    <button style="width: 100%;" class="btn btn-primary  pull-right" id="request_quotation" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Request For Quotation')?></button>
                </div>
            </div>


            <?php if(count($variants)){
                $display = '';
                if(customer('user_type') == 'institutional' || $vtype == 'institutional'){
                    $display = 'display:none;';
                }
                ?>

                <div class="table-responsive variantDiv col-md-12" style="margin-top: 20px;<?=$display?>">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr class="bg-blue">
                            <th>Product Name</th>
                            <th>Price</th>
                            <?php if(customer("user_type") !== "institutional" && $vtype != 'institutional'){ ?>
                                <th>Sale Price</th>
                            <?php } ?>
                            <th style="min-width: 126px;">Qty</th>
                            <th class="text-right">Total Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <form id="variants-form">
                            <?php
                            foreach ($variants as $key => $vart){
                                $customer_id ='';
                                if(customer('user_type') == 'institutional' || $vtype == 'institutional'){
                                    $customer_id = customer('sid');
                                    $price = $product->institutional_price+$vart->price;
                                }else {
                                    $price = $product->price + $vart->price;
                                    if ($product->sale < $product->price && $product->sale != 0)
                                        $price = $product->sale + $vart->price;
                                }
                                //$priceval = '';
                                //$price = DB::table('multiple_size')->where('id',$vart->price)->first();
                                //if($price){ $priceval = $price->price; }
                                echo'<tr>
											<td>'.variantTitle($vart->variant_title).'</td>';
                                echo '<td class="price">'.(customer('rfr') == 0 ? (customer('user_type') == 'institutional' ? c(getInstitutionalPriceFancy($product->id,$customer_id,$hub,$vart->id)) : c($product->price+getSize($vart->price))) : 'Rate not available');
                                if(customer('user_type') == 'institutional' && customer('rfr') == 0){
                                    $pb = getInstitutionalPrice($product->id,$customer_id,$hub,$vart->id);
                                    echo '<div class="tooltipx"><i class="fa fa-info-circle"></i>
                                              <div class="tooltiptext">
                                                <div class="tooltipContent">
                                                    <span class="tooltipLeft">Basic</span>
                                                    <span class="tooltipRight">'.c($pb['price']).'</span>
                                                </div>
                                                <div class="tooltipContent">
                                                    <span class="tooltipLeft">Size Diff.</span>
                                                    <span class="tooltipRight">'.c($pb['var']).'</span>
                                                </div>
                                                <div class="tooltipContent">
                                                    <span class="tooltipLeft">Loading</span>
                                                    <span class="tooltipRight">'.c($pb['loading']).'</span>
                                                </div>
                                                <div class="tooltipContent">
                                                    <span class="tooltipLeft">Tax</span>
                                                    <span class="tooltipRight">'.c($pb['tax']).'</span>
                                                </div>
                                                <div class="tooltipContent">
                                                    <span class="tooltipLeft">Total</span>
                                                    <span class="tooltipRight">'.c($pb['total']).'</span>
                                                </div>
                                              </div>
                                            </div>';
                                }
                                echo '</td>';
                                $col = 3;
                                if(customer("user_type") !== "institutional" && $vtype != 'institutional') {
                                    $col = 4;
                                    echo '<td class="price" id="sp-' . $vart->id . '">' . (customer('rfr') == 0 ? c($price) : 'Rate not available') . '</td>';
                                }
                                $dis = '';
                                if(customer('rfr')){
                                    $dis = 'disabled';
                                }
                                echo '<td style="min-width:126px;"><div class=" buttons_added_'.$key.' " style="display: none">
<div class="form-group baseQuantity but" style="  border: 1px solid #dadada !important; width: 100px !important;">
                                            <span class="input-number-decrement" style="color: rgb(0, 52, 129);
    font-weight: 600;" margin-left: 1em;>–</span><input class="input-number" style="border: none !important;
    width: 64px !important;
    text-align: center !important; color: rgb(0, 52, 129);
    font-weight: 600;" type="text" value="1" min="0" max="100"><span class="input-number-increment" style="color: rgb(0, 52, 129);
    font-weight: 600;">+</span>
                                        </div>

</div>

<button type="button" name="add-to-cart" value="1520" class="single_add_to_cart_button_'.$key.' button alt" style="display: block !important">Add+</button>   
</td>
                                            <td class="text-right">
                                                <b class="tp" id="tp-'.$vart->id.'" data-price="0">'.(customer('rfr') == 0 ? c('0.00') : 'Rate not available').'</b>
                                            </td>
                                        </tr>
                                        ';
                            }
                            echo '<tr>
                                        <td colspan="'.$col.'" class="text-right">Total '.(customer("user_type") == "institutional" ? "" : "Sale ").'Price:</td>
                                        <td class="text-right"><b id="gt">'.(customer('rfr') == 0 ? c('0.00') : 'Rate not available').'</b></td>
                                    </tr>';
                            if(customer('user_type') !== 'institutional' && $vtype != 'institutional') {
                                echo '<tr>
                                        <td colspan="'.$col.'" class="text-right">Total Market Price:</td>
                                        <td class="text-right"><strike id="mp" style="font-size: 14px; font-weight: 700;">' . (customer('rfr') == 0 ? c('0.00') : 'Rate not available') . '</strike></td>
                                    </tr>';
                                echo '<tr>
                                        <td colspan="'.$col.'" class="text-right">Your Savings:</td>
                                        <td class="text-right"><b id="savings" style="color: green;">' . (customer('rfr') == 0 ? c('0.00') : 'Rate not available') . '</b></td>
                                    </tr>';
                            }
                            ?>
                        </form>
                        </tbody>
                    </table>
                    <div class="order">
                        <div class="col-md-8">
                            <div class="error" style="display:none;"></div>
                        </div>
                        <div class="col-md-4">
                            <?php if(customer('user_type') !== ''){
                                if(customer('user_type') != 'institutional' && $vtype != 'institutional') {?>
                                    <?php if(customer('rfr') == 0){ ?>
                                        <button style="width: 100%;" class="add-cart btn btn-primary  pull-right" id="added" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Add to cart')?><span style="display: none;" class ="addd">Added To Cart</span></button>
                                    <?php }else{ ?>
                                        <button style="width: 100%;" class="request-rate btn btn-primary  pull-right" data-id="<?=$product->id?>"> <?=translate('Request for rate')?></button>
                                    <?php } }else{
                                    if(customer('rfr') == 0){ ?>
                                        <button class="btn btn-primary book_now" data-id="<?=$product->id?>" style="width: 100%;margin-bottom: 10px;margin-top: 15px;"><i class="icon-basket"></i> <?=translate('book now')?></button>
                                    <?php }else{ ?>
                                        <button class="btn btn-primary request-rate" data-id="<?=$product->id?>" style="width: 100%;margin-bottom: 10px;margin-top: 15px;"><?=translate('request for rate')?></button>
                                    <?php    }
                                }
                            }else{ ?>
                                <button style="width: 100%; display: none;" class="btn btn-primary  pull-right" data-redirect="<?=url('login')?>" data-id="<?=$product->id?>"><i class="icon-basket"></i> <?=translate('Add to cart')?></button>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            <?php } ?>
        </div>

        <div class="clearfix"></div>
        <div class="panel-group" id="accordion">
            <?php
            if($product->description_view != '0') {
                if ($product->text != '') { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               style="color: #ffffff;"><h4 class="panel-title">
                                    Description
                                </h4></a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p><?= $product->text ?></p>
                            </div>
                        </div>
                    </div>
                <?php }
            }?>
            <?php
            if($product->specification_view != '0'){
                if($product->specification != '') { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="color: #ffffff;">
                                <h4 class="panel-title">
                                    Specification
                                </h4></a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p><?= $product->specification ?></p>
                            </div>
                        </div>
                    </div>
                    <?php
                }} ?>
            <?php if($product->rating_view != '0'){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" style="color: #ffffff;"><h4 class="panel-title">
                                Ratings & Reviews
                            </h4></a>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="feedback-container">
                                <div class="col-lg-12">
                                    <div class="col-lg-4">
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style="margin-bottom: 0px; font-family: Sans-Serif; font-style: bold;">Avarage rating <?=$avg_rating;?></h5>
                                                <div class="rating" style="padding-bottom: 10px;">
                                                    <?php
                                                    $a=$avg_rating;
                                                    $b = explode('.',$a);
                                                    $first = $b[0];
                                                    $rr = $avg_rating;
                                                    $i = 0;
                                                    while($i<5){ $i++;?>
                                                        <i class="star<?=($i<=$avg_rating) ? '-selected' : '';?>"></i>
                                                        <?php $rr--; }
                                                    echo '</div>';
                                                    ?>
                                                    <h5 style=" font-family: Sans-Serif; font-style: bold;">Total rating <?=$total_user;?></h5>
                                            </li>
                                        </ul>
                                        <hr>
                                        <ul class="rating">
                                            <li class="float-left">
                                                <h5 style=" font-family: Sans-Serif; font-style: bold;">Total comment <?=$total_reviews; ?></h5>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-8 reviews-graph">
                                        <ul>
                                            <li class="float-left">
                                                <?php
                                                if($rating5 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">5 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating5->rating5_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating5->rating5_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating5->rating5_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating5->rating5_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating4 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">4 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating4->rating4_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating4->rating4_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating4->rating4_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating4->rating4_user;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating3 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">3 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating3->rating3_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating3->rating3_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating3->rating3_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating3->rating3_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                if($rating2 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">2 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating2->rating2_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating2->rating2_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating2->rating2_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating2->rating2_user;?> Reviews)</div>
                                                    </div>
                                                    <?php
                                                }
                                                if($rating1 != ''){
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 25px;">
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="text-align: right;">1 <i class="fa fa-star"></i></div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>"
                                                                     aria-valuemin="0" aria-valuemax="100" style="width:<?php $a = $rating1->rating1_user; echo  ($a== 0 ? '0' : ($a*100)/$total_user) ?>%">
                                                                    <span class="sr-only"><?=$rating1->rating1_user;?> Reviews</span>
                                                                </div>
                                                            </div>
                                                            <span class="mob-rev-count">(<?=$rating1->rating1_user;?> Reviews)</span>
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 rev-count">(<?=$rating1->rating1_user;?> Reviews)</div>
                                                    </div>

                                                    <?php
                                                }
                                                ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12" style="padding-bottom: 10px;">
                                <form action="<?php echo url('review');?>" method="post" class="form-horizontal single">
                                    <?=csrf_field();?>

                                    <div id="response" style="float: left; width: 100%"></div>
                                    <?php if(customer('id') !== ''){?>
                                        <h5 style="float: left; width: 100%; margin-top: 10px; border-top: 1px solid #000000; padding-top: 20px;"><?=translate('Add a review')?> :</h5>


                                        <fieldset style="float: left; width: 100%;">
                                            <div class="col-md-12">
                                                <input name="name" readonly value="<?=customer('id')?>" class="form-control" type="hidden">
                                                <input name="product" value="<?=$product->id?>" class="form-control" type="hidden">

                                                <div class="form-group col-md-12">
                                                    <label class="control-label"><?=translate('Rating')?></label>
                                                    <div id="star-rating">
                                                        <input type="radio" name="rating" class="rating" value="1" />
                                                        <input type="radio" name="rating" class="rating" value="2" />
                                                        <input type="radio" name="rating" class="rating" value="3" />
                                                        <input type="radio" name="rating" class="rating" value="4" />
                                                        <input type="radio" name="rating" class="rating" value="5" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label"><?=translate('Review')?></label>
                                                    <textarea name="review" type="text" rows="5" class="form-control"></textarea>
                                                </div>
                                                <button data-product="<?=$product->id?>" name="submit" class="btn btn-primary" ><?=translate('submit')?></button>
                                            </div>
                                        </fieldset>
                                    <?php } else { ?>
                                        <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                            <div class="col-md-12 text-center my">
                                                <button class="btn btn-primary" onClick="getModel()" type="button" value="Login">Login To Review</button>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </form>
                                <br><br><br>
                                <p>
                                    <?php
                                    $count = 0;
                                    foreach($reviews as $review){
                                        $usid = $review->name;
                                        $uimg = DB::select("SELECT * FROM customers WHERE id = '".$usid."'")[0];
                                        echo '<img class="review-image" src="assets/user_image/'; if($uimg->image != ''){ echo $uimg->image; } else { echo 'user.png'; }; echo'">
											<div class="review-meta"><b>'.$uimg->name.'</b><br/>
											<span class="time">'.date('M d, Y',$review->time).'</span><br/></div>
											<div class="review">
												<div class="rating pull-right">';
                                        $rr = $review->rating; $i = 0; while($i<5){ $i++;?>
                                            <i class="star<?=($i<=$review->rating) ? '-selected' : '';?>"></i>
                                            <?php $rr--; }
                                        echo '</div>
											<div class="clearfix"></div>
											<p>'.nl2br($review->review).'</p></div>';
                                        $count++;
                                    }
                                    if($count > 0){
                                        echo '<hr/>';
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if($product->faq_view != '0'){?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" style="color: #ffffff;"><h4 class="panel-title">
                            FAQ
                        </h4></a>
                </div>
                <div id="collapseFour" class="panel-collapse collapse">
                    <div class="panel-body">
                        <section class="at-property-sec at-property-right-sidebar">
                            <div class="">
                                <div class="row" style="padding: 15px 0; margin-top: -55px;">
                                    <div class="col-md-12 text-center">
                                        <?php if(customer('id') !== ''){ ?>
                                            <button class="btn btn-primary" type="button" data-popup-open="popup-question" value="Ask a Question">Ask a Question</button>
                                        <?php } else { ?>
                                            <button class="btn btn-primary" onClick="getModel()" type="button" value="Ask a Question">Ask a Question</button>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul>
                                            <?php foreach($faqs as $faq){?>
                                                <li>
                                                    <!--<input type="checkbox" checked>
                                                    <i></i>-->
                                                    <h5>Q : <?php echo $faq->question; ?><br><small>
                                                            by Admin on <?php $d = $faq->time; echo date('d M, Y', strtotime($d)); ?></small></h5>
                                                    <p>A : <?php echo $faq->answer; ?></p>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <!-- Ask a Question Popup Start -->
                        <div class="popup" id="popup-question" data-popup="popup-question">
                            <div class="popup-inner">
                                <div class="col-md-12">
                                    <h2 style="text-transform: none;" class="text-center">Ask a Question</h2>
                                    <form id="contact_form" action="<?php echo url('product-faq');?>" method="post">
                                        <?=csrf_field() ?>
                                        <input type="hidden" name="product_id" value="<?=$product->id;?>" />
                                        <div class="col-md-12 col-sm-12 text-center">
                                            <textarea class="form-control" name="question" rows="5" placeholder="Write question here" required="required"></textarea>
                                            <br>
                                            <button class="btn btn-primary" type="submit">SUBMIT</button>
                                        </div>
                                    </form>
                                </div>
                                <a class="popup-close" data-popup-close="popup-question" href="#">x</a>
                            </div>
                        </div>
                        <!-- Ask a Question Popup End -->
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
</section>
<?php if($product->fb_cat !== '' && customer('user_type') !== 'institutional' && $vtype != 'institutional'){
    $fb_cats = explode(',', $product->fb_cat);

    ?>
    <section>
        <div class="container">
            <div class="box clearfix box-with-products">
                <!-- Carousel nav -->
                <a class="next" href="#myCarousel0" id="myCarousel0_next"><span></span></a>
                <a class="prev" href="#myCarousel0" id="myCarousel0_prev"><span></span></a>

                <div class="box-heading">Frequently Bought Together</div>
                <div class="strip-line"></div>

                <div class="box-content products" style="margin: 20px 0;">
                    <div class="box-product">
                        <div id="myCarousel0" class="owl-carousel">
                            <?php
                            foreach($fb_cats as $fb_cat){
                                $pro = null;
                                $pro = getProduct($fb_cat, $product->id);
                                if($pro !== null){

                                    ?>
                                    <div class="item">
                                        <div class="at-property-item at-col-default-mar" id="<?=$pro->id;?>">
                                            <div class="at-property-img">
                                                <a href="product/<?=path($pro->title,$pro->id)?>" data-title="<?=translate($pro->title)?>">
                                                    <img src="<?=url('/assets/products/'.image_order($pro->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                                </a>
                                                <div class=""></div>
                                            </div>
                                            <div class="at-property-location">
                                                <div class="text-ellipsis">
                                                    <a href="product/<?=path($pro->title,$pro->id)?>" data-title="<?=translate($pro->title)?>"><?=translate($pro->title)?></a>
                                                </div>
                                                <span class="price"><?=(customer('user_type') == 'institutional' ? c($pro->institutional_price) : c($pro->price))?></span>
                                                <div class="rating">

                                                    <?php
                                                    $rates = getRatings($pro->id);
                                                    $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                        <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                        <?php $tr--; }?>
                                                    (<?=$rates['total_ratings']?>)
                                                </div>
                                                </p>
                                                <div class="cart-btn-custom" style="padding-top: 10px;">
                                                    <button class="bg" data-redirect="product/<?=path($pro->title, $pro->id); ?>" data-title="<?=translate($pro->title) ?>"><i class="icon-basket"></i> Add to Cart</button>
                                                    <div style="margin-top: -20px;">&nbsp;</div>
                                                    <?php
                                                    if(customer('id') !== ''){
                                                        $user_id = customer('id');
                                                        $product_id = $product->id;

                                                        $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");

                                                        if(!empty($img)){

                                                            echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                        } else{
                                                            echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                            <button class="bg likebutton1" id="likebutton"><i class="fa fa-heart"></i> Add to Project</button>';

                                                        }} else {
                                                        echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php } if(!empty($related_products)){
    $customer_id ='';
    if(customer('id')){
        $customer_id = customer('sid');
    }
    $pc = false;
    foreach($related_products as $rp){
        $p = getInstitutionalPriceFancy($rp->id,$customer_id,$hub);
        if($p != "NA"){
            $pc = true;
            break;
        }
    }
    if($pc){
        ?>
        <section>
            <div class="container">
                <div class="box clearfix box-with-products">
                    <!-- Carousel nav -->
                    <a class="next" href="#myCarousel" id="myCarousel_next"><span></span></a>
                    <a class="prev" href="#myCarousel" id="myCarousel_prev"><span></span></a>

                    <div class="box-heading">Related products</div>
                    <div class="strip-line"></div>

                    <div class="box-content products" style="margin: 20px 0;">
                        <div class="box-product">
                            <div id="myCarousel" class="owl-carousel">
                                <?php
                                foreach($related_products as $rel){
                                    $customer_id ='';
                                    if(customer('id')){
                                        $customer_id = customer('sid');
                                    }
                                    $pr = getInstitutionalPriceFancy($rel->id,$customer_id,$hub);
                                    if($pr != "NA"){
                                        ?>
                                        <div class="item">
                                            <div class="at-property-item at-col-default-mar" id="<?=$rel->id;?>">
                                                <div class="at-property-img">
                                                    <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>">
                                                        <img src="<?=url('/assets/products/'.image_order($rel->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                                    </a>
                                                    <div class=""></div>
                                                </div>
                                                <div class="at-property-location">
                                                    <div class="text-ellipsis">
                                                        <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>"><?=translate($rel->title)?></a>
                                                    </div>
                                                    <span class="price"><?=(customer('user_type') == 'institutional' ? c($rel->institutional_price) : c($rel->price))?></span>
                                                    <div class="rating">

                                                        <?php
                                                        $rates = getRatings($rel->id);
                                                        $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                            <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                            <?php $tr--; }?>
                                                        (<?=$rates['total_ratings']?>)
                                                    </div>
                                                    </p>
                                                    <div class="cart-btn-custom" style="padding-top: 10px;">
                                                        <button class="bg" data-redirect="product/<?=path($rel->title, $rel->id) ?>" data-title="<?=translate($rel->title) ?>"><i class="icon-basket"></i> Add to Cart</button>
                                                        <div style="margin-top: -20px;">&nbsp;</div>
                                                        <?php
                                                        if(customer('id') !== ''){
                                                            $user_id = customer('id');
                                                            $product_id = $product->id;

                                                            $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");

                                                            if(!empty($img)){

                                                                echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                            } else{
                                                                echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                            <button class="bg likebutton2" id="likebutton"><i class="fa fa-heart"></i> Add to Project</button>';

                                                            }} else {
                                                            echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } }
if($recent_viewed){
    ?>
    <section>
        <div class="container">
            <div class="box clearfix box-with-products">
                <!-- Carousel nav -->
                <a class="next" href="#myCarousel1" id="myCarousel1_next"><span></span></a>
                <a class="prev" href="#myCarousel1" id="myCarousel1_prev"><span></span></a>

                <div class="box-heading">Recently Viewed</div>
                <div class="strip-line"></div>

                <div class="box-content products" style="margin: 20px 0;">
                    <div class="box-product">
                        <div id="myCarousel1" class="owl-carousel">
                            <?php

                            foreach($recent_viewed as $rel){
                                $customer_id ='';
                                if(customer('id')){
                                    $customer_id = customer('sid');
                                }
                                $prv = getInstitutionalPriceFancy($rel->id,$customer_id,$hub);

                                ?>
                                <div class="item">
                                    <div class="at-property-item at-col-default-mar" id="<?=$rel->id;?>">
                                        <div class="at-property-img">
                                            <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>">
                                                <img src="<?=url('/assets/products/'.image_order($rel->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                            </a>
                                            <div class=""></div>
                                        </div>
                                        <div class="at-property-location">
                                            <div class="text-ellipsis">
                                                <a href="product/<?=path($rel->title,$rel->id)?>" data-title="<?=translate($rel->title)?>"><?=translate($rel->title)?></a>
                                            </div>
                                            <span class="price"><?=(customer('user_type') == 'institutional' ? c($prv) : c($rel->price))?></span>
                                            <div class="rating">

                                                <?php
                                                $rates = getRatings($rel->id);
                                                $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                    <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                    <?php $tr--; }?>
                                                (<?=$rates['total_ratings']?>)
                                            </div>
                                            </p>
                                            <div class="cart-btn-custom" style="padding-top: 10px;">
                                                <button class="bg" data-redirect="product/<?=path($rel->title, $rel->id) ?>" data-title="<?=translate($rel->title) ?>"><i class="icon-basket"></i> Add to Cart</button>
                                                <div style="margin-top: -20px;">&nbsp;</div>
                                                <?php
                                                if(customer('id') !== ''){
                                                    $user_id = customer('id');
                                                    $product_id = $product->id;

                                                    $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");

                                                    if(!empty($img)){

                                                        echo '<button class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                    } else{
                                                        echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
                                                        <button class="bg likebutton3" id="likebutton" ><i class="fa fa-heart"></i> Add to Project</button>';

                                                    }} else {
                                                    echo '<button class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php } ?>
<?php if(!empty($link)){?>
    <section>
        <div class="container">
            <div class="row">
                <div class="blockquote blockquote--style-1">
                    <div class="row inner-div">
                        <div class="col-md-12">
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-4">
                                <img src="<?=url('/assets/products/'.image_order($link->image))?>" class="cont-image"/>
                            </div>
                            <div class="col-md-7 col-lg-7 col-sm-7 col-xs-8" style="padding: 25px 0 0;">
                                <h3 class="cont-content"><?=$link->content; ?></h3>
                            </div>
                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 text-center" style="padding:15px 0 0 0">
                                <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                    GET STARTED
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<button class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login">Get Started !</button>
<div class="popup" id="popup-login" data-popup="popup-login">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-6 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                <?=csrf_field() ?>
                <fieldset>
                    <div class="form-group">
                        <label class="control-label"><?=translate('E-mail') ?></label>
                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?=translate('Password') ?></label>
                        <input name="password" type="password" class="form-control"  />
                    </div>
                    <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                </fieldset>
            </form>
            <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
        </div>
        <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
    </div>
</div>
<!-- Services Popup End -->
<div id="asrModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h4 class="modal-title">Request Quotation</h4>
            </div>
            <div class="modal-body">
                <button style="width: 50%;" class="byBaseModal btn btn-primary  pull-left active" data-id="<?=$product->id?>"><?=translate('Book By Basic')?></button>
                <button style="width: 50%;" class="byVarModal btn btn-primary  pull-left" data-id="<?=$product->id?>"><?=translate('Book By Size')?></button>
                <div id="baseForm" style="display: none; padding-left: 10px; padding-right: 10px;">
                    <form action="<?=url('request-quotation')?>" method="post" id="bybaseForm" enctype="multipart/form-data">
                        <?=csrf_field()?>
                        <input type="hidden" name="product_id" value="<?=$product->id;?>"/>
                        <input type="hidden" name="category_id" value="<?=$cat->id;?>"/>

                        <div class="row" style="margin-top: 10px;">
                            <div class="form-group new">
                                <h6 style="font-family: sans-serif; !important; font-size: 20px; margin-top: 40px; padding-left: 10px;"><?=translate($product->title)?></h6>
                            </div>
                            <div class="form-group baseQuantity pull-right qun" style="margin-top: -40px; padding-right: 10px; padding: 14px;">
                                <div class="dec rease">-</div>
                                <input name="base_quantity" class="quantity productQty" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                                <div class="inc rease">+</div>
                            </div>
                            <div class="form-group">
                                <label style="color:#000;" for="sel1">Please Select Product:</label>
                                <select name="brands_base[]" class="form-control select2" id="sel1" multiple>
                                    <?php foreach($related_products as $brand){ ?>
                                        <option value="<?=$brand->id?>"><?=$brand->title?></option>
                                    <?php } ?>
                                </select>

                                <br>
                                <label style="color:#000;" for="sel2">Please Select Payment Option::</label>
                                <select name="payment_base" class="form-control" id="sel2">
                                    <option>Select Payment Option</option>
                                    <option>next day</option>
                                    <option>15 days</option>
                                    <option>30 days</option>
                                    <option>more than 30 days</option>
                                    <option>Advanced Payment</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label style="color:#000;" for="usr">Location(optional):</label>
                                <input type="text" class="form-control" id="location" name="location">
                            </div>
                            <center>
                                <input name="base_quote" type="submit" value="Request Quotation"  class="btn btn-primary btn-sm"/>
                            </center>

                        </div>
                    </form>
                </div>

                <div id="sizeForm" style="display: none;padding-left: 10px; padding-right: 10px;">
                    <form action="<?=url('request-quotation')?>" method="post" id="bysizeForm" enctype="multipart/form-data">
                        <?=csrf_field()?>

                        <div class="row" style="margin-top: 10px;">
                            <table id="showGroupData" class="table table-striped table-bordered" style="background-color: white;">
                                <?php
                                foreach ($variants as $key => $vart){
                                    $customer_id ='';
                                    if(customer('user_type') == 'institutional'){
                                        $customer_id = customer('sid');
                                        $price = $product->institutional_price+$vart->price;
                                    }else {
                                        $price = $product->price + $vart->price;
                                        if ($product->sale < $product->price && $product->sale != 0)
                                            $price = $product->sale + $vart->price;
                                    }
                                    echo'<tr>
									<td>'.variantTitle($vart->variant_title).'</td>
									<td>
										<div class="form-group baseQuantity pull-right">
											<div class="dec rease">-</div>
											<input name="quantity" class="quantity productQty" data-product="'.$product->id.'" value="'.($product->min_qty != 0 ? $product->min_qty : '1').'" >
											<div class="inc rease">+</div>
										</div>
									</td>
								</tr>';
                                }

                                ?>
                                <!--<tr style=" border: 1px solid #000000;">
                                    <td>
                                        <label class="control-label">10 MM TMT BAR</label>
                                        <input type="hidden" name="variant_title[]" class="form-control"  style="background-color: white"> <br>
                                    </td>
                                    <td>
                                        <div class="form-group baseQuantity pull-right">
                                            <div class="dec rease">-</div>
                                            <input name="quantity" class="quantity productQty" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                                            <div class="inc rease">+</div>
                                        </div>

                                    </td>
                                </tr>
                                <tr style=" border: 1px solid #000000;">
                                    <td>
                                        <label class="control-label">6 MM TMT BAR</label>
                                        <input type="hidden" name="variant_title[]" class="form-control"  style="background-color: white"> <br>
                                    </td>
                                    <td>
                                        <div class="form-group baseQuantity pull-right">
                                            <div class="dec rease">-</div>
                                            <input name="quantity" class="quantity productQty" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                                            <div class="inc rease">+</div>
                                        </div>

                                    </td>
                                </tr>
                                <tr style=" border: 1px solid #000000;">
                                    <td>
                                        <label class="control-label">12 MM TMT BARe</label>
                                        <input type="hidden" name="variant_title[]" class="form-control"  style="background-color: white"> <br>
                                    </td>
                                    <td>
                                        <div class="form-group baseQuantity pull-right">
                                            <div class="dec rease">-</div>
                                            <input name="quantity" class="quantity productQty" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                                            <div class="inc rease">+</div>
                                        </div>

                                    </td>
                                </tr>
                                <tr style=" border: 1px solid #000000;">
                                    <td>
                                        <label class="control-label">8 MM TMT BAR</label>
                                        <input type="hidden" name="variant_title[]" class="form-control"  style="background-color: white"> <br>
                                    </td>
                                    <td>
                                        <div class="form-group baseQuantity pull-right">
                                            <div class="dec rease">-</div>
                                            <input name="quantity" class="quantity productQty" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                                            <div class="inc rease">+</div>
                                        </div>

                                    </td>
                                </tr>-->
                            </table>

                            <div class="form-group">
                                <label style="color:#000;" for="sel1">Please Select Product:</label>
                                <select name="brands_size[]" class="form-control select2" id="sel1" multiple>
                                    <?php foreach($related_products as $brand){ ?>
                                        <option value="<?=$brand->id?>"><?=$brand->title?></option>
                                    <?php } ?>
                                </select>
                                <label style="color:#000;" for="sel2">Please Select Payment Option::</label>
                                <select name="payment_size" class="form-control" id="sel2">
                                    <option>Select Payment Option</option>
                                    <option>next day</option>
                                    <option>15 days</option>
                                    <option>30 days</option>
                                    <option>more than 30 days</option>
                                    <option>Advanced Payment</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label style="color:#000;" for="usr">Location(optional):</label>
                                <input type="text" class="form-control" id="location" name="location">
                            </div>
                            <center>
                                <input name="size_quote" type="submit" value="Request Quotation"  class="btn btn-primary btn-sm"/>
                            </center>
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>

<div id="rfrModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h4 class="modal-title">Request For Rate</h4>
            </div>
            <div class="modal-body">
                <form action="<?=url('send-rfr')?>" method="post" id="rfrForm" enctype="multipart/form-data">
                    <?=csrf_field()?>
                    <fieldset>
                        <div class="form-group">
                            <label class="control-label">Product (*)</label>
                            <input type="hidden" name="pid" value="<?=$product->id?>"/>
                            <select name="product" class="form-control" required disabled>
                                <option value="<?=$product->id?>" selected><?=$product->title?></option>
                            </select>
                        </div>
                        <?php if(count($variants)){ ?>
                            <div class="form-group">
                                <label class="control-label">Variants (*)</label>
                                <select name="variants[]" class="form-control select2 variantsx" multiple required>
                                    <?php foreach($variants as $var){ ?>
                                        <option value="<?=$var->id?>"><?=getVname($var->variant_title)?></option>
                                    <?php } ?>
                                </select>
                                <div class="alert alert-danger error" style="display: none"></div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="control-label">Enquiry (*)</label>
                            <textarea name="msg" rows="5" class="form-control" required></textarea>
                            <div class="alert alert-danger error" style="display: none"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Attach File (Optional)</label>
                            <input type="file" class="form-control" name="image">
                        </div>

                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="submitRfr();">Send</button>
            </div>
        </div>

    </div>
</div>

<?php foreach ($variants as $key => $value) { ?>


    <script type="text/javascript">
        $(document).ready(function ()
        {
            $(document).on("click",'.minus-<?= $key ?>',function ()
            {
                var qtyval = $('.qty-<?= $key ?>').val();
                if(qtyval >= 1)
                {
                    var newQTY = parseInt(qtyval) - parseInt(1);
                    if(newQTY > 1)
                    {
                        $('.single_add_to_cart_button_<?= $key ?>').hide();
                        $('.buttons_added_-<?= $key ?>').show();
                    }else if(newQTY == 0)
                    {
                        $('.single_add_to_cart_button_<?= $key ?>').show();
                        $('.buttons_added_-<?= $key ?>').hide();

                    }
                    $('.qty-<?= $key ?>').val(newQTY);
                }

            });

            $(document).on("click",'.plus-<?= $key ?>',function ()
            {
                var qtyval = $('.qty-<?= $key ?>').val();
                var newQTY = parseInt(qtyval) + parseInt(1);
                if(newQTY >= 1)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').hide();
                    $('.buttons_added-<?= $key ?>').show();
                }else if(newQTY == 0)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').show();
                    $('.buttons_added-<?= $key ?>').hide();

                }
                $('.qty-<?= $key ?>').val(newQTY);
            });

            $(document).on("click",'.single_add_to_cart_button_<?= $key ?>',function ()
            {
                var newQTY = 1;
                if(newQTY >= 1)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').hide();
                    $('.buttons_added_<?= $key ?>').show();
                }else if(newQTY == 0)
                {
                    $('.single_add_to_cart_button_<?= $key ?>').show();
                    $('.buttons_added_<?= $key ?>').hide();

                }
                $('.qty-<?= $key ?>').val(newQTY);
            });

        });

    </script>
<?php } ?>
<button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <?=csrf_field() ?>
            <label class="control-label">Select Your Project</label>
            <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
            <div id="projectName">

            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>

<a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
<div class="popup" id="popup-project2" data-popup="popup-project2">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
    </div>
</div>
<?php echo $footer?>
<script>
    $(document).ready(function(){
        $('form input').keydown(function (e) {
            if (e.keyCode == 13) {
                var inputs = $(this).parents("form").eq(0).find(":input");
                if (inputs[inputs.index(this) + 1] != null) {
                    inputs[inputs.index(this) + 1].focus();
                }
                e.preventDefault();
                return false;
            }
        });
        $('#bookBy').on('change', function(){
            var data = $(this).val();
            if(data == 'product'){
                $('.variantDiv').hide();
                $('.productDiv').show();
            }else{
                $('.variantDiv').show();
                $('.productDiv').hide();
            }
        });
        $('.byBase').on('click', function(){
            $(this).toggleClass('active');
            $('.byVar').toggleClass('active');
            $('.variantDiv').hide();
            $('.productDiv').show();
        });
        $('.byBaseModal').on('click', function(){
            $(this).toggleClass('active');
            $('.byVarModal').toggleClass('active');
            $('#baseForm').show();
            $('#sizeForm').hide();
//            alert('hello');
        });
        $('.byVarModal').on('click', function(){
            $(this).toggleClass('active');
            $('.byBaseModal').toggleClass('active');
            $('#sizeForm').show();
            $('#baseForm').hide();
//            alert('hello sir');
        });

        $('#request_quotation').on('click', function(){
//            $(this).toggleClass('active');
//            $('.byVar').toggleClass('active');
//            $('.variantDiv').hide();
//            $('.productDiv').show();
            $('#asrModal').modal('show');
        });

        $('.byVar').on('click', function(){
            $(this).toggleClass('active');
            $('.byBase').toggleClass('active');
            $('.variantDiv').show();
            $('.productDiv').hide();
            var $container = $("html,body");
            var $scrollTo = $('.variantDiv');

            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top  - 120, scrollLeft: 0},300);
        });
        // Function to update counters on all elements with class counter
        <?php if(customer('user_type') != ''){ ?>
        var doUpdate = function() {
            $('.countdown-timer').each(function() {
                var count = parseInt($(this).html());
                var pid = $(this).data('key');
                if (count > 0) {
                    $(this).html(count - 1);
                }else if(count == 0){
                    $('#'+pid+' .price').html('Waiting for update');
                    $('#'+pid+' .tooltipx').remove();
                    $('#'+pid+' .tp').html('Waiting for update');
                }
            });
        };

        // Schedule the update to happen once every second
        setInterval(doUpdate, 1000);
        <?php } ?>
    });
</script>
<script>

    (function() {

        window.inputNumber = function(el) {

            var min = el.attr('min') || false;
            var max = el.attr('max') || false;

            var els = {};

            els.dec = el.prev();
            els.inc = el.next();

            el.each(function() {
                init($(this));
            });

            function init(el) {

                els.dec.on('click', decrement);
                els.inc.on('click', increment);

                function decrement() {
                    var value = el[0].value;
                    value--;
                    if(!min || value >= min) {
                        el[0].value = value;
                    }
                }

                function increment() {
                    var value = el[0].value;
                    value++;
                    if(!max || value <= max) {
                        el[0].value = value++;
                    }
                }
            }
        }
    })();

    inputNumber($('.input-number'));
</script>
<!-- Modal -->
<!--Modal: Login / Register Form-->
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Modal cascading tabs-->
            <div class="modal-c-tabs">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
                    <li class="nav-item">
                        <a style="color: blue;" class="nav-link active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-1"></i>
                            By Basic</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-1"></i>
                            By Size</a>
                    </li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">
                    <!--Panel 7-->
                    <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

                        <!--Body-->
                        <div class="modal-body mb-1">

                            <a href="<?=url('/products/'.$cat->path)?>" class=""><?=translate($cat->name)?></a>
                            <h3 style="font-family: sans-serif; !important; font-size: 20px;"><?=translate($product->title)?></h3>
                            <div class="quantity-select productDiv" style="margin-top: 10px">
                                <div class="dec rease">-</div>
                                <input name="quantity" class="quantity productQty" data-product="<?=$product->id; ?>" value="<?=$product->min_qty != 0 ? $product->min_qty : '1'; ?>" >
                                <div class="inc rease">+</div>
                            </div>
                            <select name="unit" class="form-control productDiv" style="float: right;max-width: 50%; margin-top: 5px;">
                                <option value="MT">MT</option>
                            </select>


                        </div>
                        <div class="col s12 m6 xl3">
                            <div class="col s1 m1"style="text-align: center;margin-top: 30px;"> <i class="material-icons prefix">person</i></div>
                            <div class="col s10 m10">
                                <label>Select Account Manager</label>
                                <select class="browser-default acc_manager" id="acc_manager" name="acc_manager" tabindex="-1">
                                    <option value="all">All</option>

                                </select>
                            </div>
                        </div>
                        <div class="text-center mt-2">
                            <button class="btn btn-info">Log in <i class="fas fa-sign-in ml-1"></i></button>
                        </div>
                        <!--Footer-->
                        <div class="modal-footer">

                            <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                    <!--/.Panel 7-->

                    <!--Panel 8-->
                    <div class="tab-pane fade" id="panel8" role="tabpanel">

                        <!--Body-->
                        <div class="modal-body">
                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-envelope prefix"></i>
                                <input type="email" id="modalLRInput12" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput12">Your email</label>
                            </div>

                            <div class="md-form form-sm mb-5">
                                <i class="fas fa-lock prefix"></i>
                                <input type="password" id="modalLRInput13" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput13">Your password</label>
                            </div>

                            <div class="md-form form-sm mb-4">
                                <i class="fas fa-lock prefix"></i>
                                <input type="password" id="modalLRInput14" class="form-control form-control-sm validate">
                                <label data-error="wrong" data-success="right" for="modalLRInput14">Repeat password</label>
                            </div>

                            <div class="text-center form-sm mt-2">
                                <button class="btn btn-info">Sign up <i class="fas fa-sign-in ml-1"></i></button>
                            </div>

                        </div>
                        <!--Footer-->
                        <div class="modal-footer">
                            <div class="options text-right">
                                <p class="pt-1">Already have an account? <a href="#" class="blue-text">Log In</a></p>
                            </div>
                            <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!--/.Panel 8-->
                </div>

            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Login / Register Form-->

<!--<div class="text-center">-->
<!--    <a href="" class="btn btn-default btn-rounded my-3" data-toggle="modal" data-target="#modalLRForm">Launch-->
<!--        Modal LogIn/Register</a>-->
<!--</div>-->
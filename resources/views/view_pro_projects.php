<?php echo $header?>


    <section class="pdtopbtm-50">
        <div class="row">
            <div class="col-md-10 col-lg-offset-1 col-lg-10">
                <div class="grid">
                    <?php
                    if(!empty($pro)){
                    foreach($pro as $prowish){
                        $pro = DB::select('SELECT * FROM products Where id = '.$prowish->product_id)[0];
                        echo '<div class="col-md-3 col-sm-3">
                                            <div class="at-property-item at-col-default-mar" id="'.$pro->id.'">
                                                <div class="at-property-img">
                                                <a href="product/'.path($pro->title,$pro->id).'" data-title="<'.translate($pro->title).'">
                                                    <img src="'.url('/assets/products/'.image_order($pro->images)).'" style="width: 100%; height: 150px;" alt="">
                                                    </a>
                                                </div>
                                                <div class="at-property-location" style="min-height: 15px;">
                                                    <p>
                                                        <a href="product/'.path($pro->title,$pro->id).'" data-title="'.translate($pro->title).'">'.translate($pro->title).'</a>
                                                        <br><span class="price">'.c($pro->price).'</span>
                                                        <div class="rating">';
                        $rates = getRatings($pro->id);
                        $tr = $rates['rating']; $i = 0; while($i<5){ $i++;
                            echo '<i class="star'.(($i<=$rates["rating"]) ? "-selected" : "").'"></i>';
                            $tr--; }
                        echo '('.$rates["total_ratings"].')
                                                        </div>
                                                        <div class="cart-btn-custom">
                                                                    <button class="bg">Add to Cart</button>
                                                                </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>';
                    }
                    } else {
                        echo '<div class="col-md-12">
                                            <h3 style="text-align: center; padding: 30px;">Product not available in project...</h3>
                                        </div>';
                    }
                    ?>
                </div>
            </div>
        </div>


    </section>


<?php echo $footer?>
<script>
    jQuery(window).on('load', function(){
        var $ = jQuery;
        var $container = $('.grid');
        $container.masonry({
            columnWidth:10,
            gutterWidth: 15,
            itemSelector: '.grid-item'
        });
    });
</script>


<?php echo $header;
if(customer('id') !== ''){
    $customerId = customer('id');
}else{
    $customerId = false;
}
?>
<section >
    <div class="container">
        <div class="row" id="at-inner-title-sec" <?=($categ->banner !== '' ? 'style="background-image: url(\'assets/products/'.$categ->banner.'\'); box-shadow: 0 3px 6px rgba(0,0,0,.16);"' : ''); ?>>
            <div class="col-md-6 col-sm-6 col-md-offset-3" style="padding: 30px 0; margin-top: 45px; vertical-align: middle;">
                <div class="inner-border">
                    <div class="at-inner-title-box text-center" style="vertical-align: middle; padding: 25px;margin:5px;border: none;background: rgba(255, 255, 255, 0.7);">
                        <h3><?=(isset($categ->name) ? translate($categ->name) : translate('Products'));?></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Inner page heading end -->

<!-- Property start from here -->
<?php
if(!empty($hub) || customer('user_type') !== 'institutional' || customer('user_type') == ''){ ?>
    <section class="at-property-sec at-property-right-sidebar">
        <div class="container">
            <div class="row">
                <button class="btn btn-default filter-button"><i class="icon-basket"></i> Featured Products</button>
                <aside class="sidebar-shop col-md-3 order-md-first">
                    <div class="filter-div-close"></div>
                    <div class="sidebar-wrapper">

                        <div class="widget">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-body-c" role="button" aria-expanded="true" aria-controls="widget-body-c">Categories</a>
                            </h3>

                            <div class="show collapse in" id="widget-body-c">
                                <div class="widget-body">
                                    <ul class="cat-list" style="height: 250px; overflow-y: auto;">
                                        <?php foreach($cats as $cat){
                                            echo '<li><a href="products/'.$cat->path.'">'.translate($cat->name).'</a></li>';
                                            $childs = DB::select("SELECT * FROM category WHERE parent = ".$cat->id." ORDER BY id DESC");
                                            foreach ($childs as $child){
                                                echo '<li><a href="products/'.$child->path.'">- '.$child->name.'</a></li>';
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->
                        <?php if($data['show_filter'] && $show_products ){ ?>
                            <form method="get" action="">
                                <div class="widget">
                                    <h3 class="widget-title">
                                        <a data-toggle="collapse" href="#widget-body-p" role="button" aria-expanded="true" aria-controls="widget-body-p">Price</a>
                                    </h3>

                                    <div class="show collapse in" id="widget-body-p">
                                        <div class="widget-body">
                                            <div class="form-price">
                                                <div class="clearfix"></div>
                                                <b class="pull-left price"><?=$price['set_min'] ?></b>
                                                <b class="pull-right price"><?=$price['set_max']; ?></b>
                                                <input name="min" id="min" type="hidden">
                                                <input name="max" id="max" type="hidden">
                                                <div id="price"></div>
                                            </div>
                                        </div><!-- End .widget-body -->
                                    </div><!-- End .collapse -->
                                </div><!-- End .widget -->

                                <div class="widget">
                                    <h3 class="widget-title">
                                        <a data-toggle="collapse" href="#widget-body-b" role="button" aria-expanded="true" aria-controls="widget-body-b">Brand</a>
                                    </h3>

                                    <div class="show collapse in" id="widget-body-b">
                                        <div class="widget-body">
                                            <ul class="cat-list" style="height: 100px; overflow-y: auto;">
                                                <?php
                                                foreach($brands as $brand){
                                                    $checked = '';
                                                    if(in_array($brand->id, $bids)){
                                                        $checked = 'checked';
                                                    }
                                                    echo '<li><input type="checkbox" name="brands['.$brand->id.']" id="br-'.$brand->id.'" '.$checked.'><label for="br-'.$brand->id.'">'.translate($brand->name).'</label></a></li>';
                                                }
                                                ?>
                                            </ul>

                                        </div><!-- End .widget-body -->
                                    </div><!-- End .collapse -->
                                </div><!-- End .widget -->



                                <?php

                                if(!empty($filters)) { ?>
                                    <?php foreach ($filters as $fil) {
                                        $fil_option = DB::table('filter_options')->where('filter_id', $fil->id)->get();
                                        if (!empty($fil_option)) {
                                            ?>
                                            <div class="widget">
                                                <h3 class="widget-title">
                                                    <a data-toggle="collapse" href="#widget-body-<?= $fil->id; ?>" role="button"
                                                       aria-expanded="true"
                                                       aria-controls="widget-body-<?= $fil->id; ?>"><?php echo $fil->name; ?></a>
                                                </h3>
                                                <div class="show collapse in" id="widget-body-<?= $fil->id; ?>">
                                                    <div class="widget-body">
                                                        <?php if ($fil->name == 'Colour') { ?>
                                                            <ul class="config-swatch-list">
                                                                <?php foreach($fil_option as $fo){ ?>
                                                                    <li>
                                                                        <a href="#" style="background-color: <?php echo $fo->name; ?>;"> </a>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } elseif($fil->name == 'Size') { ?>
                                                            <ul class="config-size-list">
                                                                <?php foreach($fil_option as $fo){ ?>
                                                                    <li>
                                                                        <a class="a-text" href="#"><?php echo $fo->name; ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } else { ?>
                                                            <ul>
                                                                <?php foreach($fil_option as $fo){ ?>
                                                                    <li>
                                                                        <a href="#"><?php echo $fo->name; ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </div><!-- End .widget-body -->
                                                </div><!-- End .collapse -->
                                            </div><!-- End .widget -->
                                        <?php }
                                    }
                                }?>
                                <div class="filter-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">Apply Filter</button>
                                </div>
                            </form>
                        <?php } ?>
                        <!--<div class="widget widget-featured">
                            <h3 class="widget-title">Featured Products</h3>

                            <div class="widget-body">

                            </div><!-- End .widget-body >
                        </div><!-- End .widget -->

                    </div><!-- End .sidebar-wrapper -->
                </aside>
                <div class="filter-wrapper"></div>
                <div class="col-md-9">
                    <div class="row">
                        <?php if(count($product_categories) > 0){?>
                            <div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="at-sec-title">
                                            <h2 style="font-size: 15px;">Shop By <span>Category</span></h2>
                                            <div class="at-heading-under-line" style="margin: auto;">
                                                <div class="at-heading-inside-line" ></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                foreach($product_categories as $service){
                                    echo '<div class="col-md-4 col-sm-4">
                                <a href="products/'.$categ->path.'/'.$service->path.'" data-title="<'.translate($service->name).'">
                                    <div class="at-property-item at-col-default-mar" id="'.$service->id.'">
                                        <div class="at-property-img">
                                            <img src="'.url('/assets/products/'.image_order($service->image)).'" style="width: 100%; height: 200px;" alt="">
                                        </div>
                                        <div class="at-property-location" style="border-top: 1px solid #ddd;background: rgb(0, 52, 129); min-height: 30px;">
                                            <h4 class="text-center" style="margin-top: -10px;margin-bottom: -10px;"><a href="products/'.$categ->path.'/'.$service->path.'" data-title="'.translate($service->name).'" style="color: #ffffff;">'.translate($service->name).'</a></h4>
                                        </div>
                                    </div>
                                </a>
                            </div>';
                                }
                                ?>
                            </div>
                        <?php } ?>
                        <!--<div class="col-lg-12">&nbsp;</div>-->
                        <?php if(count($best) > 0 && count($products)== 0){?>
                            <section class="at-property-sec at-property-right-sidebar" style="padding-bottom: 0; margin-bottom: 10px;">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="at-sec-title">
                                                <h2 style="font-size: 15px;">Choose The Best <span><?=(isset($categ->name) ? translate($categ->name) : translate('Products'));?></span></h2>
                                                <div class="at-heading-under-line" style="margin: auto;">
                                                    <div class="at-heading-inside-line" ></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="padding: 0px 40px;">
                                            <div class="regular" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>
                                                <?php foreach($best as $b){ ?>
                                                    <div class="at-agent-col">
                                                        <div class="at-col-default-mar" id="'.$service->id.'" style="margin: 0;">
                                                            <div class="at-property-img">
                                                                <img src="<?php echo url('/assets/products/' . image_order($b->image)); ?>" style="width: 100%; height: 125px;" alt="">
                                                            </div>
                                                            <div class="at-property-location" style="min-height: 255px; pa">
                                                                <div class="comparison-tile-content">
                                                                    <div class="row comparison-tile-bullets">
                                                                        <?php echo $b->content; ?>
                                                                        <div class="columns" style="text-align: center;">
                                                                            <a href="<?=$b->link; ?>" class="btn btn-primary btn-sm">Shop All</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        <?php } ?>
                        <?php
                        $product_id = '';

                        if($show_products) {
                            if (count($products) && customer('user_type') !== 'institutional' && customer('user_type') !== '') {

                                $pc = false;
                                foreach ($products as $product) {
                                    $p_price = getIndividual($product->id);
                                    if ($p_price != 'NA') {
                                        $pc = true;
                                        $pvalidity = -1;
                                        if($product->is_variable){
                                            date_default_timezone_set("Asia/Kolkata");

                                            $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
                                        }
                                        echo '<div class="padding-extension-left">';
                                        $productI = $product->id;
                                        echo '<div class="col-md-4 col-sm-4 col-xs-6 padding-reset">
                                    <div class="at-property-item at-col-half-mar" id="' . $product->id . '">
                                        <span class="countdown-timer" data-key="'.$product->id.'">'.$pvalidity.'</span>
                                        <div class="at-property-img">
                                            <a href="product/' . path($product->title, $product->id) . '" data-title="<' . strtoupper(translate($product->title)) . '">
                                                <img src="' . url('/assets/products/' . image_order($product->images)) . '" style="width: 100%; height: 150px;" alt="">
                                            </a>
                                        </div>
                                        <div class="at-property-location">
                                            <div class="text-ellipsis">
                                                <a href="product/' . path($product->title, $product->id) . '" data-title="' . strtoupper(translate($product->title)) . '">' . strtoupper(translate($product->title)) . '</a>
                                            </div>
                                                <span class="price">' . (customer('user_type') == 'institutional' ? c($p_price) : c($product->price)) . '</span>';
                                        if(customer('user_type') == 'institutional' && customer('rfr') == 0){
                                            $pb = getInstitutionalPrice($product->id, $customerId, $hub->id,false,false);
                                            echo '<div class="tooltipx"><i class="fa fa-info-circle"></i>
                                                      <div class="tooltiptext">
                                                        <div class="tooltipContent">
                                                            <span class="tooltipLeft">Basic</span>
                                                            <span class="tooltipRight">'.$pb['price'].'</span>
                                                        </div>
                                                        <div class="tooltipContent">
                                                            <span class="tooltipLeft">Loading</span>
                                                            <span class="tooltipRight">'.$pb['loading'].'</span>
                                                        </div>
                                                        <div class="tooltipContent">
                                                            <span class="tooltipLeft">Tax</span>
                                                            <span class="tooltipRight">'.$pb['tax'].'</span>
                                                        </div>
                                                        <div class="tooltipContent">
                                                            <span class="tooltipLeft">Total</span>
                                                            <span class="tooltipRight">'.$pb['total'].'</span>
                                                        </div>
                                                      </div>
                                                    </div>';
                                        }
                                        if($product->rating_view != '0') {
                                            echo '<div class="rating">';
                                            $rates = getRatings($product->id);
                                            $tr = $rates['rating'];
                                            $i = 0;
                                            while ($i < 5) {
                                                $i++;
                                                echo '<i class="star' . (($i <= $rates["rating"]) ? "-selected" : "") . '"></i>';
                                                $tr--;
                                            }
                                            echo '(' . $rates["total_ratings"] . ')
                                            </div>';
                                        }
                                        echo '<div class="cart-btn-custom" style="padding-top: 10px;">
                                                <button class="bg" data-redirect="product/' . path($product->title, $product->id) . '" data-title="' . translate($product->title) . '"><i class="icon-basket"></i> ' . (customer('user_type') == "institutional" ? "Book Now" : "Add to Cart") . '</button>
                                                <div style="margin-top: -20px;">&nbsp;</div>
                                                ';
                                        if (customer('id') !== '') {
                                            $user_id = customer('id');
                                            $product_id = $product->id;
                                            $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '" . $user_id . "' AND product_id = '" . $product_id . "'");
                                            if (!empty($img)) {
                                                echo '<button style="display:none;" class="bg" id="likebutton1"><span class="tickgreen">✔</span> Added to Project</button>';
                                            } else {
                                                echo '<input type="hidden" name="pid1" class="pid1" value="' . $product_id . '">
                                                    <button style="display:none;" class="bg likebutton1">
                                                        <i class="fa fa-heart"></i> Add to Project
                                                    </button>';
                                            }
                                        } else {
                                            echo '<button style="display:none;" class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                        }
                                        echo '
                                            </div>
                                            </div>
                                    </div>
                                </div>
                                </div>';
                                    }
                                }
                                if(!$pc){
                                    echo '<h5 class="text-center"></h5>';
                                }

                            }

                            else if (count($products) && customer('user_type') == 'institutional') {
                                $hub_detail = $hub;
                                echo '<section class="at-property-sec at-property-right-sidebar" style="padding-bottom: 0;padding-top: 0">';
                                if($hub_detail !== null) {
                                    echo '<p class="m-hub-para">Rate : <span>'.$hub_detail->title.'</span><a href="javascript:;" class="change-hub">Change?</a></p>';
                                }
                                $pc = false;

                                foreach ($products as $product) {

                                    $p_price = getInstitutionalPrice($product->id, $customerId, $hub->id,false,false);
                                        $pc = true;
                                        $pvalidity = -1;
                                        if($product->is_variable){
                                            date_default_timezone_set("Asia/Kolkata");

                                            $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
                                        }
                                        echo '<div class="padding-extension-left">';
                                        $productI = $product->id;
                                        echo '<div class="col-md-4 col-sm-4 col-xs-6 padding-reset">
                                    <div class="at-property-item at-col-half-mar" id="' . $product->id . '">
                                    <span class="countdown-timer" data-key="'.$product->id.'">'.$pvalidity.'</span>
                                        <div class="at-property-img">
                                            <a href="product/' . path($product->title, $product->id) . '" data-title="' . strtoupper(translate($product->title)) . '">
                                                <img src="' . url('/assets/products/' . image_order($product->images)) . '" style="width: 100%; height: 150px;" alt="">
                                            </a>
                                        </div>
                                        <div class="at-property-location">
                                            <div class="text-ellipsis">
                                                <a href="product/' . path($product->title, $product->id) . '" data-title="' . strtoupper(translate($product->title)) . '">' . strtoupper(translate($product->title)) . '</a>
                                            </div>
                                                <span class="price">' . (customer('user_type') == 'institutional' ? c($p_price['total']) : c($product->price)) . '</span>';
                                        if(customer('user_type') == 'institutional' && customer('rfr') == 0){
                                            $pb = $p_price;
                                            echo '<div class="tooltipx"><i class="fa fa-info-circle"></i>
                                                      <div class="tooltiptext">
                                                        <div class="tooltipContent">
                                                            <span class="tooltipLeft">Basic</span>
                                                            <span class="tooltipRight">'.$pb['price'].'</span>
                                                        </div>
                                                        <div class="tooltipContent">
                                                            <span class="tooltipLeft">Loading</span>
                                                            <span class="tooltipRight">'.$pb['loading'].'</span>
                                                        </div>
                                                        <div class="tooltipContent">
                                                            <span class="tooltipLeft">Tax</span>
                                                            <span class="tooltipRight">'.$pb['tax'].'</span>
                                                        </div>
                                                        <div class="tooltipContent">
                                                            <span class="tooltipLeft">Total</span>
                                                            <span class="tooltipRight">'.$pb['total'].'</span>
                                                        </div>
                                                      </div>
                                                    </div>';
                                        }

                                        if($product->rating_view != '0') {
                                            echo '<div class="rating">';
                                            $rates = getRatings($product->id);
                                            $tr = $rates['rating'];
                                            $i = 0;
                                            while ($i < 5) {
                                                $i++;
                                                echo '<i class="star' . (($i <= $rates["rating"]) ? "-selected" : "") . '"></i>';
                                                $tr--;
                                            }
                                            echo '(<i class="fa fa-user"></i> ' . $rates["total_ratings"] . ')
                                            </div>';
                                        }else{
                                            echo '<div class="rating"><i class="star"></i><i class="star"></i><i class="star"></i><i class="star"></i><i class="star"></i>&nbsp;&nbsp;(<i class="fa fa-user"></i> 0)</div>';
                                        }
                                        echo '<div class="cart-btn-custom" style="padding-top: 10px;">
                                                <button class="bg" data-redirect="product/' . path($product->title, $product->id) . '" data-title="<' . translate($product->title) . '"><i class="icon-basket"></i> ' . (customer('user_type') == "institutional" ? "Book Now" : "Add to Cart") . '</button>
                                                <div style="margin-top: -20px;">&nbsp;</div>
                                                ';
                                        if (customer('id') !== '') {
                                            $user_id = customer('id');
                                            $product_id = $product->id;
                                            $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '" . $user_id . "' AND product_id = '" . $product_id . "'");
                                            if (!empty($img)) {
                                                echo '<button style="display:none;" class="bg" id="likebutton1"><span class="tickgreen">✔</span> Added to Project</button>';
                                            } else {
                                                echo '<input type="hidden" name="pid1" class="pid1" value="' . $product_id . '">
                                                    <button style="display:none;" class="bg likebutton1">
                                                        <i class="fa fa-heart"></i> Add to Project
                                                    </button>';
                                            }
                                        } else {
                                            echo '<button style="display:none;" class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                        }
                                        echo '
                                            </div>
                                            </div>
                                    </div>
                                </div>
                                </div>';

                                }

                                if(!$pc){
                                    echo '<h5 class="text-center">No products found. Please change hub or filter and try again.</h5>';
                                }
                                echo '</section>';
                            } else if (customer('user_type') == '') {

                                $pc = false;
                                foreach ($products as $product) {
                                    $p_price = getIndividual($product->id);
                                    if ($p_price != 'NA') {
                                        $pc = true;
                                        $pvalidity = -1;
                                        if($product->is_variable){
                                            date_default_timezone_set("Asia/Kolkata");

                                            $pvalidity = strtotime($product->validity) - strtotime(date('Y-m-d H:i:s'));
                                            if($pvalidity < 0){
                                                $pvalidity = 0;
                                            }
                                        }
                                        echo '<div class="padding-extension-left">';
                                        $productI = $product->id;
                                        echo '<div class="col-md-4 col-sm-4 col-xs-6 padding-reset">
                                    <div class="at-property-item at-col-half-mar" id="' . $product->id . '">
                                        <span class="countdown-timer" data-key="'.$product->id.'">'.$pvalidity.'</span>
                                        <div class="at-property-img">
                                            <a href="product/' . path($product->title, $product->id) . '" data-title="' . strtoupper(translate($product->title)) . '">
                                                <img src="' . url('/assets/products/' . image_order($product->images)) . '" style="width: 100%; height: 150px;" alt="">
                                            </a>
                                        </div>
                                        <div class="at-property-location">
                                            <div class="text-ellipsis">
                                                <a href="product/' . path($product->title, $product->id) . '" data-title="' . strtoupper(translate($product->title)) . '">' . strtoupper(translate($product->title)) . '</a>
                                            </div>
                                                <span class="price">' . (customer('user_type') == 'institutional' ? c($p_price) : "Login to view price") . '</span>';
                                        if($product->rating_view != '0') {
                                            echo '<div class="rating">';
                                            $rates = getRatings($product->id);
                                            $tr = $rates['rating'];
                                            $i = 0;
                                            while ($i < 5) {
                                                $i++;
                                                echo '<i class="star' . (($i <= $rates["rating"]) ? "-selected" : "") . '"></i>';
                                                $tr--;
                                            }
                                            echo '(<i class="fa fa-user"></i> ' . $rates["total_ratings"] . ')
                                            </div>';
                                        }else{
                                            echo '<div class="rating"><i class="star"></i><i class="star"></i><i class="star"></i><i class="star"></i><i class="star"></i>&nbsp;&nbsp;(<i class="fa fa-user"></i> 0)</div>';
                                        }
                                        echo '<div class="cart-btn-custom" style="padding-top: 10px;">
                                                <button class="bg" data-redirect="product/' . path($product->title, $product->id) . '" data-title="<' . translate($product->title) . '"><i class="icon-basket"></i> ' . ($data['vtype'] == "institutional" ? "Book Now" : "Add to Cart") . '</button>
                                                <div style="margin-top: -20px;">&nbsp;</div>
                                                ';
                                        if (customer('id') == '') {
                                            $user_id = customer('id');
                                            $product_id = $product->id;
                                            $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '" . $user_id . "' AND product_id = '" . $product_id . "'");
                                            if (!empty($img)) {
                                                echo '<button style="display:none;" class="bg" id="likebutton1"><span class="tickgreen">✔</span> Added to Project</button>';
                                            } else {
                                                echo '<input type="hidden" name="pid1" class="pid1" value="' . $product_id . '">
                                                    <button style="display:none;" class="bg likebutton1">
                                                        <i class="fa fa-heart"></i> Add to Project
                                                    </button>';
                                            }
                                        } else {
                                            echo '<button style="display:none;" class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                        }
                                        echo '
                                            </div>
                                            </div>
                                    </div>
                                </div>
                                </div>';
                                    }
                                }
                                if(!$pc){
                                    echo '<h5 class="text-center"></h5>';
                                }

                            }

                        }

                        ?>

                    </div>

                    <?php if(count($suggested) > 0){ ?>
                        <div class="row" style="padding-left: 10px;">
                            <div class="box clearfix box-with-products" style="width: 800px; height:500px;">
                                <!-- Carousel nav -->
                                <a class="next" href="#myCarousel0" id="myCarousel0_next"><span></span></a>
                                <a class="prev" href="#myCarousel0" id="myCarousel0_prev"><span></span></a>
                                <div class="box-heading">Suggested Products</div>
                                <div class="strip-line"></div>
                                <div class="box-content products" style="margin:20px 15px 20px 20px;">
                                    <div class="box-product">
                                        <div id="myCarousel0" class="owl-carousel">
                                            <?php
                                            foreach($suggested as $suggest){
                                                $pvalidity = -1;
                                                if($suggest->is_variable){
                                                    date_default_timezone_set("Asia/Kolkata");

                                                    $pvalidity = strtotime($suggest->validity) - strtotime(date('Y-m-d H:i:s'));
                                                    if($pvalidity < 0){
                                                        $pvalidity = 0;
                                                    }
                                                }
                                                ?>
                                                <div class="item">
                                                    <div class="at-property-item at-col-default-mar" id="<?=$suggest->id;?>">
                                                        <span class="countdown-timer" data-key="<?=$suggest->id;?>"><?=$pvalidity;?></span>
                                                        <div class="at-property-img">
                                                            <a href="product/<?=path($suggest->title,$suggest->id)?>" data-title="<?=translate($suggest->title)?>">
                                                                <img src="<?=url('/assets/products/'.image_order($suggest->images))?>" style="width: 100%; height: 150px;background-color: #060606;" alt="">
                                                            </a>
                                                            <div class=""></div>
                                                        </div>
                                                        <div class="at-property-location">
                                                            <div class="text-ellipsis">
                                                                <a href="product/<?=path($suggest->title,$suggest->id)?>" data-title="<?=translate($suggest->title)?>"><?=translate($suggest->title)?></a>
                                                            </div>
                                                            <span class="price"><?=(customer('user_type') == 'institutional' ? c(getInstitutionalPriceFancy($suggest->id, $customerId, $hub->id,false,false)) : c($suggest->price))?></span>
                                                            <?php if(customer('user_type') == 'institutional' && customer('rfr') == 0){
                                                                $pb = getInstitutionalPrice($suggest->id, $customerId, $hub->id,false,false);
                                                                echo '<div class="tooltipx"><i class="fa fa-info-circle"></i>
                                                            <div class="tooltiptext">
                                                                <div class="tooltipContent">
                                                                    <span class="tooltipLeft">Basic</span>
                                                                    <span class="tooltipRight">'.$pb['price'].'</span>
                                                                </div>
                                                                <div class="tooltipContent">
                                                                    <span class="tooltipLeft">Loading</span>
                                                                    <span class="tooltipRight">'.$pb['loading'].'</span>
                                                                </div>
                                                                <div class="tooltipContent">
                                                                    <span class="tooltipLeft">Tax</span>
                                                                    <span class="tooltipRight">'.$pb['tax'].'</span>
                                                                </div>
                                                                <div class="tooltipContent">
                                                                    <span class="tooltipLeft">Total</span>
                                                                    <span class="tooltipRight">'.$pb['total'].'</span>
                                                                </div>
                                                            </div>
                                                        </div>';
                                                            } if($product->rating_view != '0'){?>
                                                                <div class="rating">
                                                                    <?php
                                                                    $rates = getRatings($suggest->id);
                                                                    $tr = $rates['rating']; $i = 0; while($i<5){ $i++;?>
                                                                        <i class="star<?=($i<=$rates['rating']) ? '-selected' : '';?>"></i>
                                                                        <?php $tr--; }?>
                                                                    (<?=$rates['total_ratings']?>)
                                                                </div>
                                                            <?php } ?>
                                                            <div class="cart-btn-custom" style="padding-top: 10px;">
                                                                <button class="bg" data-redirect="product/<?=path($suggest->title, $suggest->id) ?>" data-title="<?=translate($suggest->title) ?>"><i class="icon-basket"></i> <?=(customer('user_type') == "institutional" ? "Book Now" : "Add to Cart" )?></button>
                                                                <div style="margin-top: -20px;">&nbsp;</div>
                                                                <?php
                                                                if(customer('id') !== ''){
                                                                    $user_id = customer('id');
                                                                    $product_id = $suggest->id;

                                                                    $img = DB::select("SELECT * FROM product_wishlist WHERE user_id = '".$user_id."' AND product_id = '".$product_id."'");

                                                                    if(!empty($img)){

                                                                        echo '<button style="display:none;" class="bg" id="likebutton"><span class="tickgreen">✔</span> Added to Project</button>';
                                                                    } else{

                                                                        echo '<input type="hidden" name="pidd" class="pidd" value="'.$product_id.'">
																<button style="display:none;" class="bg likebutton"><i class="fa fa-heart"></i> Add to Project</button>';
                                                                    }
                                                                } else {
                                                                    echo '<button style="display:none;" class="bg" onClick="getModel()"><i class="fa fa-heart"></i> Add to Project</button>';
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <?php if(count($top_brands) > 0){ ?>
                <div class="row" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16); padding: 30px;">
                    <div class="row" id="brands">
                        <div class="col-sm-12" style="margin-bottom: -10px;">
                            <div class="brand-panel">
                                <div class="brand-panel-title text-center">
                                    <h5 class="">Top Brands</h5>
                                </div>
                                <div class="col-md-12 brand-panel-items" style="padding: 15px;background: #ffffff;">
                                    <div class="agent-carousel slider" data-slick='{"slidesToShow": 6, "slidesToScroll": 1}'>
                                        <?php
                                        foreach($top_brands as $tb){
                                            ?>
                                            <div style="text-align: center;">
                                                <div class="hover-effect" style="border: 1px solid #ddd;padding: 3px;">
                                                    <div style="width: 100%; height: 100%;border: 1px solid #555;">
                                                        <a href="<?=url('brand/'.$tb->path); ?>" >
                                                            <img src="assets/brand/<?php echo $tb->image; ?>" style="width: 100%; max-width:100%; height: 100px;">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if(!empty($link)){?>
                <div class="row">
                    <div class="blockquote blockquote--style-1">
                        <div class="row inner-div">
                            <div class="col-md-12">
                                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-4">
                                    <img src="<?=url('/assets/products/'.image_order($link->image))?>" class="cont-image"/>
                                </div>
                                <div class="col-md-7 col-lg-7 col-sm-7 col-xs-8" style="padding: 25px 0 0;">
                                    <h3 class="cont-content"><?=$link->content; ?></h3>
                                </div>
                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 text-center" style="padding:15px 0 0 0">
                                    <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                        GET STARTED
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
    <?php if(count($faqs) > 0){ ?>
        <section class="at-property-sec at-property-right-sidebar " id="faq">

            <div class="container" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16);padding: 30px;">
                <div class="box-heading" style="padding-left: 10px;">
                    <div class="at-sec-title">
                        <h2 style="font-size: 15px;">Frequently <span>Asked</span> Questions</h2>
                        <div class="at-heading-under-line" style="margin: auto;">
                            <div class="at-heading-inside-line" ></div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-right: 10px; padding-left: 10px;">
                    <div class="col-md-12">
                        <input type="hidden" name="category" value="<?=$categ->id; ?>"/>
                        <ul id="fload">
                            <?php
                            $postID = '';
                            foreach($faqs as $faq){
                                $postID = $faq->id;
                                ?>
                                <li>
                                    <input type="checkbox" checked>
                                    <i></i>
                                    <h5>Q : <?php echo $faq->question; ?><br><small>

                                            by Admin on <?php $d = $faq->time; echo date('d M, Y', strtotime($d)); ?></small></h5>
                                    <p>A : <?php echo $faq->answer; ?></p>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-lg-12" style="padding-top: 10px; text-align: center;">
                        <div class="show_more_main" id="show_more_main<?php echo $postID; ?>">
                            <span id="<?php echo $postID; ?>" class="show_more" title="Load more posts" style="cursor: pointer;">Show more</span>
                            <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- Start Client -->
    <?php } ?>

    <section class="section " id="client" style="margin-top: -40px;">
        <div class="container bg-client" style="box-shadow: 0 3px 6px rgba(0,0,0,.16); padding: 10px;">
            <!--        <div class="row">-->
            <!--            <div class="col-lg-12">-->
            <!--                <div class="box-heading" style="padding-left: 10px;">-->
            <!--                    <div class="at-sec-title">-->
            <!--                        <h2 style="font-size: 15px;">What <span>Clients</span> Says?</h2>-->
            <!--                        <div class="at-heading-under-line" style="margin: auto;">-->
            <!--                            <div class="at-heading-inside-line" ></div>-->
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--            </div>-->
            <!--        </div>-->
            <div class="row mt-5">
                <div class="col-lg-12">
                    <div id="owl-demo" class="owl-carousel">
                        <?php

                        $pro1 = $products;

                        foreach ($pro1 as $pd){

                            $rpidx = DB::select("SELECT * FROM reviews inner join customers on customers.id = reviews.name WHERE (product = ".$pd->id." AND  active = '1') AND (rating = '4' OR rating = '5') ".(customer('user_type') != '' ? 'AND (customers.user_type = \''.customer('user_type').'\')' : '')." ORDER BY rating DESC");
                            foreach ($rpidx as $rpid_val){
                                $pid = $rpid_val->product;
                                $pro = DB::table('products')->where('id', '=', $pid)->first();
                                ?>
                                <?php $cust = DB::table('customers')->where('id', '=', $rpid_val->name)->first(); ?>
                                <?php if($cust !== null) { ?>
                                    <div class="text-center testi_boxes mt-3">
                                        <div class="bus_testi_icon text-custom">
                                            <i class="fa fa-quote-left"></i>
                                        </div>
                                        <div class="mt-3">

                                            <div class="mt-4">
                                                <img src="http://localhost/sellerkit/assets/products/6c055265459572b2126c42ecabb6d98e.jpg"
                                                     style="width: 125px; height: 125px; padding: 4px; line-height: 1.42857143; border: 3px solid #d7d3d3; border-radius: 4px; box-shadow: 0 0 20px #a5a5a5; ">
                                            </div>
                                            <div class="testi_icon_center text-custom">
                                                <i class="fa fa-quote-left"></i>
                                            </div>
                                            <p class="text-custom mb-0"><?php echo $pro->title; ?>
                                                <br>
                                            <div class="review" style="padding-left: 47%;">
                                                <div class="rating" style="text-align: center;">
                                                    <?php $rr = $rpid_val->rating;
                                                    $i = 0;
                                                    while ($i < 5) {
                                                        $i++; ?>
                                                        <i class="star<?= ($i <= $rpid_val->rating) ? '-selected' : ''; ?>"></i>
                                                        <?php $rr--;
                                                    } ?>
                                                </div>
                                            </div>
                                            </p>

                                            <p class="client_review text-muted font-italic text-center">
                                                " <?php echo $rpid_val->review; ?>"

                                            </p>

                                            <div class="testi_img mt-4">
                                                <img src="<?php if ($cust->image !== '') {
                                                    echo url('assets/user_image/' . $cust->image);
                                                } else {
                                                    echo url('assets/user_image/user.png');
                                                }; ?>" alt=""
                                                     class="img-fluid rounded-circle mx-auto d-block img-thumbnail">
                                            </div>

                                            <p class="client_name text-center mb-0 mt-3 font-weight-bold"><?= $cust->name; ?></p>
                                        </div>
                                    </div>
                                    <?php
                                } } }?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Client -->
    <?php if(count($ladvices) > 0) {?>
        <section class="at-property-sec at-property-right-sidebar " style="background-color: #f4f4f4;">

            <div class="container" style="background-color: white; box-shadow: 0 3px 6px rgba(0,0,0,.16);padding: 30px;">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="col-lg-8 col-md-8 mb-20">
                            <div class="row">
                                <div class="col-lg-12 col-md-8">
                                    <div class="box-heading" style="padding-left: 10px;">
                                        <div class="at-sec-title">
                                            <h2 style="font-size: 15px;">Latest <span>Advices</span> </h2>
                                            <div class="at-heading-under-line" style="margin: auto;">
                                                <div class="at-heading-inside-line" ></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="border-top: 2px solid #f4f4f4; padding-right: 10px; padding-left: 10px;">&nbsp;</div>
                            <div class="vc_controls scroll-up" data-scroll="vc_cont1"><i class="fa fa-chevron-up"></i></div>
                            <div class="verticleCarouselContainer" id="vcc1">
                                <div class="adviceList" id="vc_cont1">
                                    <?php foreach ($ladvices as $ladvice){ ?>
                                        <article style="padding-top: 10px; padding-bottom: 10px;" class="row vcx_item">
                                            <div class="col-lg-4 col-md-4">
                                                <div class="home-featured-image">
                                                    <a href="blog/<?php echo path($ladvice->title,$ladvice->id); ?>">
                                                        <img src="<?php echo url('/assets/blog/'.$ladvice->images); ?>" class="alignleft" style="">
                                                    </a>
                                                </div>

                                            </div>
                                            <div class="col-lg-8 col-md-8" itemprop="text">
									<span class="entry-categories">
										<?php echo getadvicesCategoryName($ladvice->category); ?>
									</span>
                                                <h5 class="entry-title" itemprop="headline">
                                                    <a class="entry-title-link" rel="bookmark"><?=$ladvice->title; ?></a>
                                                </h5>
                                                <p class="entry-meta"><time class="entry-time" itemprop="datePublished" datetime="<?=$ladvice->date; ?>"><?=$ladvice->date; ?></time></p>
                                                <?=$ladvice->short_des; ?>
                                                <div class="col-lg-12 col-md-12" style="width:100%;margin-top: 15px;color:#003481">
                                                    <?php
                                                    $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$ladvice->id)[0]->count;
                                                    $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$ladvice->id)[0]->count;
                                                    $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$ladvice->id)[0]->count;
                                                    $rating = 0;
                                                    if ($total_ratings > 0){
                                                        $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$ladvice->id)[0]->sum;
                                                        $rating = $rating_summ / $total_ratings;
                                                    }
                                                    $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$ladvice->id." AND active = 1 ORDER BY time DESC");
                                                    $faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$ladvice->id." AND status = 'Approved' ORDER BY id DESC");
                                                    $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$ladvice->id."'")[0];
                                                    $total_rating =  $rating->total_rating;
                                                    $total_user = $rating->total_user;
                                                    if($total_rating==0){
                                                        $avg_rating = 0;
                                                    }
                                                    else{
                                                        $avg_rating = round($total_rating/$total_user,1);
                                                    }
                                                    ?>
                                                    <div class="col-xs-12 text-center">
                                                        <p style="width: 25%; float: left; bottom: 10px;"> <i class="fa fa-eye"></i> <?=$ladvice->visits; ?> Visits</p>
                                                        <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
                                                        <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
                                                        <p style="width: 25%; float: left; bottom: 10px;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12 list-full">

                                                <div class="post-footer-container">
                                                    <div class="post-footer-line post-footer-line-3">
                                                        <div class="post-sharing-icons">
                                                            <a target="_blank" href="https://www.facebook.com/sharer.php" title="Share on Facebook!" rel="noopener">
                                                                <i class="fa fa-post-footer fa-facebook"></i>
                                                            </a>
                                                            <a target="_blank" href="https://twitter.com/home/" title="Tweet this!" rel="noopener">
                                                                <i class="fa fa-post-footer fa-twitter"></i>
                                                            </a>
                                                            <a target="_blank" href="https://plus.google.com/share" title="Post this on Google Plus!" rel="noopener">
                                                                <i class="fa fa-post-footer fa-google-plus"></i>
                                                            </a>
                                                            <a href="mailto:" title="Send this article to a friend!">
                                                                <i class="fa fa-post-footer fa-envelope"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="vc_controls scroll-down" data-scroll="vc_cont1"><i class="fa fa-chevron-down"></i></div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-20 border-left">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="box-heading" style="padding-left: 10px;">
                                        <div class="at-sec-title">
                                            <h2 style="font-size: 15px;">Trending <span>Advices</span> </h2>
                                            <div class="at-heading-under-line" style="margin: auto;">
                                                <div class="at-heading-inside-line" ></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="border-top: 2px solid #f4f4f4; padding-right: 10px; padding-left: 10px;">&nbsp;</div>
                            <div class="vc_controls scroll-up" data-scroll="vc_cont2"><i class="fa fa-chevron-up"></i></div>
                            <div class="verticleCarouselContainer" id="vcc2">
                                <div class="adviceList" id="vc_cont2">
                                    <?php foreach ($tadvices as $tadvice){ ?>
                                        <div class="home-featured-image vc_item vcx_item">
                                            <a href="blog/<?php echo path($tadvice->title,$tadvice->id); ?>">
                                                <img src="<?php echo url('/assets/blog/'.$tadvice->images); ?>" class="alignleft" sizes="(max-width: 600px) 100vw, 600px">
                                            </a>
                                            <h5 class="entry-title" itemprop="headline" style="padding: 5px; border: 4px solid #f4f4f4; ">
                                                <a class="entry-title-link" rel="bookmark"><?=$tadvice->title; ?></a>
                                            </h5>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="vc_controls scroll-down" data-scroll="vc_cont2"><i class="fa fa-chevron-down"></i></div>

                        </div>

                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
    <script>
        function changeHub(){
            $('#myModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        };
    </script>
<?php }else { ?>

    <script>
        $(document).ready(function(){
            $('#myModal').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
        });
    </script>
<?php } ?>




<button class="btn btn-primary hidden" id="open-service-modal1" data-popup-open="popup-login">Get Started !</button>
<div class="popup" id="popup-login" data-popup="popup-login">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-6 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('login-model'); ?>" method="post" class="form-horizontal single">
                <?=csrf_field() ?>
                <fieldset>
                    <div class="form-group">
                        <label class="control-label"><?=translate('E-mail') ?></label>
                        <input name="email" type="email" value="<?=isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control"  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?=translate('Password') ?></label>
                        <input name="password" type="password" class="form-control"  />
                    </div>
                    <input name="login" type="submit" value="<?=translate('Login') ?>" class="btn btn-primary" />
                </fieldset>
            </form>
            <div class="text-center"><a class="smooth" href="<?=url('reset-password')?>">Forgot your password ?</a></div>
        </div>
        <a class="popup-close" data-popup-close="popup-login" href="#">x</a>
    </div>
</div>
<script>
    function submitHub(){
        if($('input[name=hub]:checked').val()) {
            $('form#hubForm').submit();
        }else{
            alert('Please select rate type to proceed!')
        }
    }
    $(document).ready(function(){
        var owl0 = $(".box #myCarousel0");

        $("#myCarousel0_next").on("click", function() {
            event.preventDefault();
            owl0.trigger('owl.next');
            return false;
        })
        $("#myCarousel0_prev").on("click", function() {
            event.preventDefault();
            owl0.trigger('owl.prev');
            return false;
        });

        owl0.owlCarousel({
            slideSpeed : 400,
            itemsCustom : [
                [0, 1],
                [360, 2],
                [700, 3],
                [1255, 3]
            ]
        });

        /*var handlesSlider = document.getElementById('price');
    noUiSlider.create(handlesSlider, {
        start: [<=$price['min']?>,<=$price['max']?>],
        step: 1,
        connect: false,
        range: {'min':<=$price['min']?>,'max':<=$price['max']?>},
    });
    var BudgetElement = [document.getElementById('min'),document.getElementById('max')];
    handlesSlider.noUiSlider.on('update', function(values, handle) {
        BudgetElement[0].textContent = values[0];
        BudgetElement[1].textContent = values[1];
        $("#min").val(values[0]);
        $(".pull-left.price").html(values[0]);
        $("#max").val(values[1]);
        $(".pull-right.price").html(values[1]);
    });*/

    });
    function initVerticalCarousel(obj){

        var vc = $(obj);
        var heights = $(obj +" .vcx_item").map(function ()
        {
            return $(this).height();
        }).get();
        var maxHeight = Math.max.apply(null, heights) + 20;
        window.matchMedia('screen and (max-width: 991px)')
        {
            vc.attr('style', 'height:'+maxHeight+'px;');
        }

    }
    $(window).resize(function(){
        initVerticalCarousel('#vcc1');
        initVerticalCarousel('#vcc2');
    });
    $(document).ready(function(){
        var owl1 = $(".box #myCarousel1");

        $("#myCarousel1_next").on("click", function() {
            event.preventDefault();
            owl.trigger('owl.next');
            return false;
        })
        $("#myCarousel1_prev").on("click", function() {
            event.preventDefault();
            owl.trigger('owl.prev');
            return false;
        });

        owl1.owlCarousel({
            slideSpeed : 400,
            itemsCustom : [
                [0, 1],
                [480, 1],
                [700, 1],
                [1255, 1]
            ]
        });
        initVerticalCarousel('#vcc1');
        initVerticalCarousel('#vcc2');

        /*var handlesSlider = document.getElementById('price');
    noUiSlider.create(handlesSlider, {
        start: [<=$price['min']?>,<=$price['max']?>],
        step: 1,
        connect: false,
        range: {'min':<=$price['min']?>,'max':<=$price['max']?>},
    });
    var BudgetElement = [document.getElementById('min'),document.getElementById('max')];
    handlesSlider.noUiSlider.on('update', function(values, handle) {
        BudgetElement[0].textContent = values[0];
        BudgetElement[1].textContent = values[1];
        $("#min").val(values[0]);
        $(".pull-left.price").html(values[0]);
        $("#max").val(values[1]);
        $(".pull-right.price").html(values[1]);
    });*/
    });
</script>
<script>

    $(function() {
//----- OPEN
        $('[data-popup-open]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            e.preventDefault();
        });
//----- CLOSE
        $('[data-popup-close]').on('click', function(e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            e.preventDefault();
        });
    });
    function getModel(){
        //alert('hello');
        $('#open-service-modal1').click();
    }

    function addProject(val) {
        var ele =$(this);
        var proid = $('#ghanta').data('id');
        var project_id = val;
        $.ajax({
            type: "POST",
            url: "add-product-project",
            data:'_token=<?=csrf_token();?>&product_id='+proid+'&project_id='+project_id,
            success: function(data){

                $('#pclose').click();
                ele.unbind();
            }
        });
    }

    $('.likebutton').click( function () {
        var ele =$(this);
        var proid = $(this).prevAll('input').val();
        $.ajax({
            type: "POST",
            url: "product-wishlist",
            data:'_token=<?=csrf_token();?>&product_id='+proid,
            success: function(data){
                //alert('hello');
                $('#open-project-modal1').click();
                $('#projectName').html(data);
                ele.html('<span class="tickgreen">✔</span> Added to Project');
                ele.unbind();
            }
        });
    });

    /*$('.likebutton').click( function () {
        var proid = $(this).prevAll('input').val();
        var ele =$(this);
        $.ajax({
            type: "POST",
            url: "product-wishlist",
            data:'_token=<=csrf_token();?>&product_id='+proid,
            success: function(data){
                ele.html('<span class="tickgreen">✔</span> Added to Collections');
                ele.unbind();
            }
        });
    });*/

    /*$('.likebutton1').click( function () {
        var proid1 = $(this).prevAll('input').val();
        var ele1 =$(this);
        $.ajax({
            type: "POST",
            url: "product-wishlist",
            data:'_token=<=csrf_token();?>&product_id='+proid1,
            success: function(data){
                ele1.html('<span class="tickgreen">✔</span> Added to Collections');
                ele1.unbind();
            }
        });
    });*/

    function addProject(val) {
        var ele =$(this);
        var proid = $('#ghanta').data('id');
        var project_id = val;
        $.ajax({
            type: "POST",
            url: "add-product-project",
            data:'_token=<?=csrf_token();?>&product_id='+proid+'&project_id='+project_id,
            success: function(data){

                $('#pclose').click();
                ele.unbind();
            }
        });
    }

    $('.likebutton1').click( function () {
        var ele =$(this);
        var proid = $(this).prevAll('input').val();
        $.ajax({
            type: "POST",
            url: "product-wishlist",
            data:'_token=<?=csrf_token();?>&product_id='+proid,
            success: function(data){
                //alert('hello');
                $('#open-project-modal1').click();
                $('#projectName').html(data);
                ele.html('<span class="tickgreen">✔</span> Added to Project');
                ele.unbind();
            }
        });
    });


    function getcreate(){
        event.preventDefault();
        $('#project-modal2').click();
    }

    $(document).ready(function(){
        $(document).on('click','.show_more',function(){
            var brands = $('input[name=category]').val();
            var ID = $(this).attr('id');
            $('.show_more').hide();
            $('.loding').show();
            $.ajax({
                type:'POST',
                url:'get-faq',
                data:'brands='+brands+'&_token=<?=csrf_token();?>&id='+ID,
                success:function(html){
                    $('#show_more_main'+ID).remove();
                    $('#fload').append(html);
                }
            });
        });
    });

    $(document).ready(function(){
        $(document).on('click','.brand_more',function(){
            var id = $(this).attr('id');
            $('.brand_more').hide();
            $('.loding_brand').show();
            $.ajax({
                type:'POST',
                url:'get-brand-load',
                data:'_token=<?=csrf_token();?>&id='+id,
                success:function(html){
                    $('#brand_more_main'+id).remove();
                    $('#bload').append(html);
                }
            });
        });
        $(document).on('click', '.filter-button', function(){
            $('aside.sidebar-shop').addClass('open');
        });
        $(document).on('click', '.filter-div-close', function(){
            $('aside.sidebar-shop').removeClass('open');
        });
        // Function to update counters on all elements with class counter
        var doUpdate = function() {
            $('.countdown-timer').each(function() {
                var count = parseInt($(this).html());
                var pid = $(this).data('key');
                if (count > 0) {
                    $(this).html(count - 1);
                }else if(count == 0){
                    <?php if(customer('user_type') == ''){ ?>
                    $('#'+pid+' .price').html('Login to view price');
                    <?php } else{ ?>
                    $('#'+pid+' .price').html('Waiting for update');
                    <?php } ?>
                    $('#'+pid+' .tooltipx').remove();
                    }
                });
            };

        // Schedule the update to happen once every second
        setInterval(doUpdate, 1000);
    });


</script>
<script>
    var handlesSlider = document.getElementById('price');
    noUiSlider.create(handlesSlider, {
        start: [<?=$price['set_min']?>,<?=$price['set_max']?>],
        step: 1,
        connect: false,
        range: {'min':0,'max':<?=$price['max']?>},
    });
    var BudgetElement = [document.getElementById('min'),document.getElementById('max')];
    handlesSlider.noUiSlider.on('update', function(values, handle) {
        BudgetElement[0].textContent = values[0];
        BudgetElement[1].textContent = values[1];
        $("#min").val(values[0]);
        $(".pull-left.price").html(values[0]);
        $("#max").val(values[1]);
        $(".pull-right.price").html(values[1]);
    });



</script>
<?php echo $footer?>
<button class="btn btn-primary hidden" id="open-project-modal1" data-popup-open="popup-project">Get Started !</button>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <?=csrf_field() ?>
            <label class="control-label">Select Your Project</label>
            <button type="button" name="create" class="btn btn-primary" onClick="getcreate()" style="float: right;"><i class="fa fa-plus"></i> Create a new project </button>
            <div id="projectName">

            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>
<a class="btn btn-primary hidden" id="project-modal2" data-popup-open="popup-project2" href="#">Get Started !</a>
<div class="popup" id="popup-project2" data-popup="popup-project2">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project2" href="#">x</a>
    </div>
</div>
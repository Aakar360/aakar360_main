<?php echo $header?>

<div class="profile-root bg-white ng-scope">
    <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
        <div class="clearfix wrapper-1140 two-bgs">
            <?php if(customer('header_image') != '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') != '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-6" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image'); ?>" class="cover">
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
            <img itemprop="image" class="profile-icon" src="assets/user_image/<?php if(customer('image') != ''){ echo Customer('image'); } else { echo 'user.png'; } ?>">
            <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                <a itemprop="url" class="url">
                    <?php echo Customer('name'); ?>
                </a>
                <p style="text-align: center;"><a href="professional_account" class="btn">My Profile</a></p>
            </h1>
            <div class="sans-light font-15 line-height-md hidden-xs">
                <p style="text-align: justify; padding: 30px;"><?php echo customer('about_professional'); ?></p>
            </div>
            <div class="sans-light font-15 line-height-md hidden-xs" style="padding-bottom: 0px; position: absolute; width: 100%; bottom: 0;">
                <p style="text-align: center;">
                    <a href="professional_product" class="btn col-lg-2 btn-primary">Products</a>
                    <a href="professional_services" class="btn col-lg-2">Services</a>
                    <a href="professional_design" class="btn col-lg-2">Design</a>
                    <a href="professional_advices" class="btn col-lg-2">Advices</a>
                    <a href="my-projects"  class="btn col-lg-2">My Projects</a>
                </p>
            </div>
        </div>

        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left">
                    <section class="at-property-sec" style="padding-top: 0px;">
                        <div class="container" style="margin-left: -15px; width: 1140px;">
                            <ul class="nav nav-pills" style="background-color: white;">
                                <li class="active"><a data-toggle="tab"  href="#product">Product Orders</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="product" class="tab-pane fade in active">
                                    <br>
                                    <?php foreach($orders as $pro) {
                                        $products_data = $pro->products;
                                        $products = json_decode($products_data, true);
                                        if (count($products) > 0) {
                                            $ids = "";
                                            foreach ($products as $data) {
                                                $ids = $data['id'];

                                                $ids = rtrim($ids, ',');
                                                $order_products = DB::select("SELECT * FROM products WHERE id IN (".$ids.") ORDER  BY id DESC ")[0];
                                                echo '<div class="col-md-3 col-sm-3">
                                                <div class="at-property-item at-col-default-mar" id="'.$order_products->id . '">
                                                    <div class="at-property-img">
                                                    <a href="' . url('invoice/'.$pro->id).'" data-title="'.translate($order_products->title).'">
                                                        <img src="'.url('/assets/products/'.image_order($order_products->images)).'" style="width: 100%; height: 150px;" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="at-property-location" style="min-height: 15px;">
                                                        <p>
                                                            <a href="'.url('invoice/'.$pro->id).'" data-title="'.translate($order_products->title).'">'.translate($order_products->title).'</a>
                                                            <br><span class="price">'.c($order_products->price).'</span>
                                                            <div class="rating">';


                                                $rates = getRatings($order_products->id);
                                                $tr = $rates['rating'];
                                                $i = 0;
                                                while ($i < 5) {
                                                    $i++;
                                                    echo '<i class="star'.(($i <= $rates["rating"]) ? "-selected" : "").'"></i>';
                                                    $tr--;
                                                }
                                                echo '('.$rates["total_ratings"].')
                                                            </div>
                                                        </p>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div>';
                                            }
                                        }
                                    }
                                    ?>
                                </div>



                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <?php echo $footer?>

    <script>
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });
        function getprofile(){
            event.preventDefault();
            $('#personal-modal').click();
        }

        function getcreate(){
            event.preventDefault();
            $('#project-modal').click();
        }

        function showProject(val) {
            document.getElementById("myForm").submit();
        }
    </script>

    <button class="btn btn-primary hidden" id="open-pro-modal1" data-popup-open="popup-pro">Get Started !</button>
    <div class="popup" id="popup-pro" data-popup="popup-pro">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-lg-12 account" style="border: none; box-shadow: none;">
                <?=csrf_field() ?>
                <div class="grid" id="projectName">

                </div>
            </div>
            <a class="popup-close" id="pclose" data-popup-close="popup-pro" href="#">x</a>
        </div>
    </div>

    <a class="btn btn-primary hidden" id="project-modal" data-popup-open="popup-project" href="#">Get Started !</a>
    <div class="popup" id="popup-project" data-popup="popup-project">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                        <tbody>
                        <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                        <tr>
                            <td>Project Title</td>
                            <td>:</td>
                            <td><input type="text" name="title" required class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Project Image</td>
                            <td>:</td>
                            <td><input type="file" name="image" required class="form-control"></td>
                        </tr>
                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-project" href="#">x</a>
        </div>
    </div>

    <a class="btn btn-primary hidden" id="personal-modal" data-popup-open="popup-personal" href="#">Get Started !</a>
    <div class="popup" id="popup-personal" data-popup="popup-personal">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('account'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                        <tbody>
                        <tr>
                            <td>Name (*)</td>
                            <td>:</td>
                            <td><input type="text" name="name" required class="form-control" value="<?php echo Customer('name'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Mobile (*)</td>
                            <td>:</td>
                            <td><input type="text" name="mobile" required class="form-control" value="<?php echo Customer('mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Alternate Mobile</td>
                            <td>:</td>
                            <td><input type="text" name="alternate_mobile" class="form-control" value="<?php echo Customer('alternate_mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Email (*)</td>
                            <td>:</td>
                            <td><input type="text" name="email" required class="form-control" value="<?php echo Customer('email'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Profile Image</td>
                            <td>:</td>
                            <td><input type="file" name="image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 1</td>
                            <td>:</td>
                            <td><input type="file" name="header_image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 2</td>
                            <td>:</td>
                            <td><input type="file" name="header_image_1" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td>Company (*)</td>
                            <td>:</td>
                            <td><input type="text" name="company" class="form-control" required value="<?php echo Customer('company'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 1 (*)</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_1" required class="form-control" value="<?php echo Customer('address_line_1'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 2</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_2" class="form-control" value="<?php echo Customer('address_line_2'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Country (*)</td>
                            <td>:</td>
                            <td>
                                <select name="country" id="country_shipping" required class="form-control" onChange="getStates(this.value, 'state_shipping')">
                                    <option value="<?php Customer('country'); ?>"><?php echo getCountryName(Customer('country')); ?></option>
                                    <option value="">Select country</option>
                                    <?php foreach($countries as $country){
                                        echo '<option value="'.$country->id.'">'.$country->name.'</option>';
                                    }?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>State (*)</td>
                            <td>:</td>
                            <td>
                                <select name="state" class="form-control" required id="state_shipping" onChange="getCities(this.value, 'city_shipping')">
                                    <option value="<?php Customer('state'); ?>"><?php echo getStateName(Customer('state')); ?></option>
                                    <option value="">Please select region, state or province</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>City (*)</td>
                            <td>:</td>
                            <td>
                                <select name="city" class="form-control" required id="city_shipping">
                                    <option value="<?php Customer('city'); ?>"><?php echo getCityName(Customer('city')); ?></option>
                                    <option value="">Please select city or locality</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Postcode (*)</td>
                            <td>:</td>
                            <td><input type="text" name="postcode" required class="form-control" value="<?php echo Customer('postcode'); ?>"></td>
                        </tr>
                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-personal" href="#">x</a>
        </div>
    </div>
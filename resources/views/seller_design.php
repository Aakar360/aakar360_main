<?php echo $header?>

<div class="profile-root bg-white ng-scope">
    <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
        <div class="clearfix wrapper-1140 two-bgs">
            <?php if(customer('header_image') != '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') != '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-6" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image'); ?>" class="cover">
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
            <img itemprop="image" class="profile-icon" src="assets/user_image/<?php if(customer('image') != ''){ echo Customer('image'); } else { echo 'user.png'; } ?>">
            <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                <a itemprop="url" class="url">
                    <?php echo Customer('name'); ?>
                </a>
                <p style="text-align: center;"><a href="#" onClick="getprofile()" class="btn">My Profile</a></p>
            </h1>
            <div class="sans-light font-15 line-height-md hidden-xs">
                <p style="text-align: justify; padding: 30px;">Vanessa&nbsp;has a Bachelor’s Degree in Architecture and a Minor in Interior Design from the University of Oklahoma. Her architecture education has given her a strong passion for incorporating clean lines, unexpected color palettes, and simple but meaningful details into the spaces she designs.</p>
            </div>
            <div class="sans-light font-15 line-height-md hidden-xs" style="padding-bottom: 0px; position: absolute; width: 100%; bottom: 0;">
                <p style="text-align: center;">
                    <a href="seller_product" class="btn col-lg-2">Products</a>
                    <a href="seller_services" class="btn col-lg-2">Services</a>
                    <a href="seller_design" class="btn btn-primary col-lg-2">Design</a>
                    <a href="seller_advices" class="btn col-lg-2">Advices</a>
                    <a href="my-projects"  class="btn col-lg-2">My Projects</a>
                </p>
            </div>
        </div>

        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left">
                    <section class="at-property-sec" style="padding-top: 0px;">
                        <div class="container" style="margin-left: -15px; width: 1140px;">
                            <ul class="nav nav-pills" style="background-color: white;">
                                <li class="active"><a href="#plan" data-toggle="tab" >Plan Orders</a></li>
                                <li class=""><a href="#plan-wish" data-toggle="tab" >Plan Collections</a></li>
                                <li class=""><a data-toggle="tab"  href="#image-ins">Image Collections</a></li>
                            </ul>

                            <div class="tab-content">

                                <div id="plan" class="tab-pane fade in active">
                                    <br>
                                    <?php foreach($orders as $layout){
                                        $lay = DB::select('SELECT * FROM layout_plans Where id = '.$layout->id)[0];?>
                                        <div class="grid-item">
                                            <div class="tiles">
                                                <a href="<?=url('planinvoice/'.$layout->id)?>">
                                                    <img src="<?php echo url('/assets/layout_plans/'.image_order($lay->images)); ?>"/>
                                                    <div class="icon-holder">
                                                        <?php $amm = json_decode($lay->interior_ammenities);
                                                        $i = 1;
                                                        //dd($amm);
                                                        foreach($amm as $key=>$value){
                                                            $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                                                            //dd($a);
                                                            ?>

                                                            <?php if($i <= 3){
                                                                if($i%3 == 0){ ?>
                                                                    <div class="icons mrgntop borderRight0"><b><?=$a->name.' : '.$value; ?></b></div>
                                                                <?php }else{ ?>
                                                                    <div class="icons mrgntop"><b><?=$a->name.' : '.$value; ?></b></div>
                                                                <?php }
                                                            }else{
                                                                if($i%3 == 0){ ?>
                                                                    <div class="icons mrgnbot borderRight0"><b><?=$a->name .' : '.$value; ?></b></div>
                                                                <?php }else{ ?>
                                                                    <div class="icons mrgnbot"><b><?=$a->name .' : '.$value; ?></b></div>
                                                                <?php } } ?>
                                                            <?php $i++; } ?>
                                                    </div>
                                                </a>

                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>



                                <div id="plan-wish" class="tab-pane fade">
                                    <?php foreach($planwishlist as $plwish){
                                        $layo = DB::select('SELECT * FROM layout_plans Where id = '.$plwish->layout_id)[0];?>
                                        <div class="grid-item">
                                            <div class="tiles">
                                                <a href="<?php echo url('/layout_plan/'.$layo->slug); ?>">
                                                    <img src="<?php echo url('/assets/layout_plans/'.image_order($layo->images)); ?>"/>
                                                    <div class="icon-holder">
                                                        <?php $amm = json_decode($lay->interior_ammenities);
                                                        $i = 1;
                                                        //dd($amm);
                                                        foreach($amm as $key=>$value){
                                                            $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                                                            //dd($a);
                                                            ?>

                                                            <?php if($i <= 3){
                                                                if($i%3 == 0){ ?>
                                                                    <div class="icons mrgntop borderRight0"><b><?=$a->name.' : '.$value; ?></b></div>
                                                                <?php }else{ ?>
                                                                    <div class="icons mrgntop"><b><?=$a->name.' : '.$value; ?></b></div>
                                                                <?php }
                                                            }else{
                                                                if($i%3 == 0){ ?>
                                                                    <div class="icons mrgnbot borderRight0"><b><?=$a->name .' : '.$value; ?></b></div>
                                                                <?php }else{ ?>
                                                                    <div class="icons mrgnbot"><b><?=$a->name .' : '.$value; ?></b></div>
                                                                <?php } } ?>
                                                            <?php $i++; } ?>
                                                    </div>
                                                </a>

                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>



                                <div id="image-ins" class="tab-pane fade">
                                    <?php foreach($imgins as $ii){
                                        $igi = DB::select('SELECT * FROM photo_gallery Where id = '.$ii->image_id)[0];?>
                                        <div class="grid-item">
                                            <div class="tiles">
                                                <a href="photo-gallery/<?=$igi->slug; ?>">
                                                    <img src="assets/images/gallery/<?=$igi->image; ?>"/>
                                                    <div class="icon-holder">
                                                        <div class="icons1 mrgntop"><?=substr(translate($igi->title), 0, 20); if(strlen($igi->title)>20) echo '...';?></div>
                                                        <div class="icons2 mrgntop borderRight0"><?=getPlanPhotsCount($igi->id); ?> Photos</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <?php echo $footer?>

    <script>
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });
        function getprofile(){
            event.preventDefault();
            $('#personal-modal').click();
        }

        function getcreate(){
            event.preventDefault();
            $('#project-modal').click();
        }

        function showProject(val) {
            document.getElementById("myForm").submit();
        }
    </script>

    <a class="btn btn-primary hidden" id="project-modal" data-popup-open="popup-project" href="#">Get Started !</a>
    <div class="popup" id="popup-project" data-popup="popup-project">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                        <tbody>
                        <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                        <tr>
                            <td>Project Title</td>
                            <td>:</td>
                            <td><input type="text" name="title" required class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Project Image</td>
                            <td>:</td>
                            <td><input type="image" name="image" required class="form-control"></td>
                        </tr>
                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-project" href="#">x</a>
        </div>
    </div>

    <a class="btn btn-primary hidden" id="personal-modal" data-popup-open="popup-personal" href="#">Get Started !</a>
    <div class="popup" id="popup-personal" data-popup="popup-personal">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('account'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                        <tbody>
                        <tr>
                            <td>Name (*)</td>
                            <td>:</td>
                            <td><input type="text" required name="name" class="form-control" value="<?php echo Customer('name'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Mobile (*)</td>
                            <td>:</td>
                            <td><input type="text" required name="mobile" class="form-control" value="<?php echo Customer('mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Alternate Mobile</td>
                            <td>:</td>
                            <td><input type="text" name="alternate_mobile" class="form-control" value="<?php echo Customer('alternate_mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Email (*)</td>
                            <td>:</td>
                            <td><input type="text" required name="email" class="form-control" value="<?php echo Customer('email'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Profile Image</td>
                            <td>:</td>
                            <td><input type="file" name="image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 1</td>
                            <td>:</td>
                            <td><input type="file" name="header_image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 2</td>
                            <td>:</td>
                            <td><input type="file" name="header_image_1" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td>Company (*)</td>
                            <td>:</td>
                            <td><input type="text" name="company" required class="form-control" value="<?php echo Customer('company'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 1 (*)</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_1" required class="form-control" value="<?php echo Customer('address_line_1'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 2</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_2" class="form-control" value="<?php echo Customer('address_line_2'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Country (*)</td>
                            <td>:</td>
                            <td>
                                <select name="country" id="country_shipping" required class="form-control" onChange="getStates(this.value, 'state_shipping')">
                                    <option value="<?php Customer('country'); ?>"><?php echo getCountryName(Customer('country')); ?></option>
                                    <option value="">Select country</option>
                                    <?php foreach($countries as $country){
                                        echo '<option value="'.$country->id.'">'.$country->name.'</option>';
                                    }?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>State (*)</td>
                            <td>:</td>
                            <td>
                                <select name="state" required class="form-control" id="state_shipping" onChange="getCities(this.value, 'city_shipping')">
                                    <option value="<?php Customer('state'); ?>"><?php echo getStateName(Customer('state')); ?></option>
                                    <option value="">Please select region, state or province</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>City (*)</td>
                            <td>:</td>
                            <td>
                                <select name="city" required class="form-control" id="city_shipping">
                                    <option value="<?php Customer('city'); ?>"><?php echo getCityName(Customer('city')); ?></option>
                                    <option value="">Please select city or locality</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Postcode (*)</td>
                            <td>:</td>
                            <td><input type="text" name="postcode" required class="form-control" value="<?php echo Customer('postcode'); ?>"></td>
                        </tr>
                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-personal" href="#">x</a>
        </div>
    </div>
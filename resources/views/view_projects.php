<?php echo $header?>


    <section class="pdtopbtm-50">
        <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-offset-1 col-lg-10">
                <?php if(!empty($pro) || !empty($lay)) {
                echo '<div class="row">
                    <div class="col-md-12">
                        <div class="at-sec-title">
                            <h2 style="text-transform: none;">Design & Layout Plans</h2>
                            <div class="at-heading-under-line">
                                <div class="at-heading-inside-line"></div>
                            </div>
                        </div>
                    </div>
                </div>'; ?>
                <div class="grid">

                    <?php

                        foreach ($pro as $p) {
                            $imid = $p->image_id;
                            $rel = DB::select("SELECT * FROM photo_gallery WHERE id = '" . $imid . "'")[0];
                            echo '<div class="grid-item">
                            <div class="tiles">
                                <a href="' . url('/photo-gallery/' . $rel->slug) . '">
                                    <img src="' . url('/assets/images/gallery/' . $rel->image) . '"/>
                                    <div class="icon-holder">
                                        <div class="icons1 mrgntop">';
                            echo substr(translate($rel->title), 0, 20);
                            if (strlen($rel->title) > 20) echo '...';
                            echo '</div>
                                        <div class="icons2 mrgntop borderRight0">' . getPlanPhotsCount($rel->id) . ' Photos</div>
                                    </div>
                                </a>
                                <a class="pro-del" href="'.url('del-pic'.'?id='.$p->id).'">x</a>
                            </div>
                        </div>';
                        }
                    ?>
                    <?php foreach($lay as $plwish){
                        $layo = DB::select('SELECT * FROM layout_plans Where id = '.$plwish->layout_id)[0];?>
                        <div class="grid-item">
                            <div class="tiles">
                                <a href="<?php echo url('/layout_plan/'.$layo->slug); ?>">
                                    <img src="<?php echo url('/assets/layout_plans/'.image_order($layo->images)); ?>"/>
                                    <div class="icon-holder">
                                        <?php $amm = json_decode($layo->interior_ammenities);
                                        $i = 1;
                                        //dd($amm);
                                        foreach($amm as $key=>$value){
                                            $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                                            //dd($a);
                                            ?>

                                            <?php if($i <= 3){
                                                if($i%3 == 0){ ?>
                                                    <div class="icons mrgntop borderRight0"><b><?=$a->name.' : '.$value; ?></b></div>
                                                <?php }else{ ?>
                                                    <div class="icons mrgntop"><b><?=$a->name.' : '.$value; ?></b></div>
                                                <?php }
                                            }else{
                                                if($i%3 == 0){ ?>
                                                    <div class="icons mrgnbot borderRight0"><b><?=$a->name .' : '.$value; ?></b></div>
                                                <?php }else{ ?>
                                                    <div class="icons mrgnbot"><b><?=$a->name .' : '.$value; ?></b></div>
                                                <?php } } ?>
                                            <?php $i++; } ?>
                                    </div>
                                </a>
                                <a class="pro-del" href="<?=url('del-lay'.'?id='.$plwish->id); ?>">x</a>
                            </div>
                        </div>
                    <?php } ?>

                </div>
<?php } ?>


                <div class="row">

                    <?php
                    if(!empty($proser)) {
                        echo '
                            <div class="col-md-12">
                                <div class="at-sec-title">
                                    <h2 style="text-transform: none;">Services</h2>
                                    <div class="at-heading-under-line">
                                        <div class="at-heading-inside-line"></div>
                                    </div>
                                </div>
                            </div>';
                        foreach ($proser as $serwish) {
                            if ($serwish->service_id == '') {
                                $ser = DB::select('SELECT * FROM services_category Where id = ' . $serwish->sub_service_id)[0];

                                echo '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                    <a href="sub_service/' . path($ser->name, $ser->id) . '" data-title="' . translate($ser->name) . '">
                                    <div class="at-property-item at-col-default-mar" id="' . $ser->id . '">
                                        <div class="at-property-img">
                                            <img src="' . url('/assets/services/' . image_order($ser->image)) . '" style="width: 360px; height: 200px;" alt="">
                                        </div>
                                        <div class="at-property-location" style="min-height: 150px;">
                                            <h4><a href="sub_service/' . path($ser->name, $ser->id) . '" data-title="' . translate($ser->name) . '">' . translate($ser->name) . '</a></h4>
                                            <p>' . $ser->description . '</p>
                                        </div>
                                    </div></a>
                                    <a class="pro-del" href="'.url('del-ser'.'?id='.$serwish->id).'">x</a>
                                </div>';
                            } else {
                                $sera = DB::select('SELECT * FROM services Where id = ' . $serwish->service_id)[0];
                                echo '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <a onClick="getQuestions(' . $sera->id . ')" href="#" data-title="' . translate($sera->title) . '">
                                        <div class="at-property-item at-col-default-mar" id="' . $sera->id . '">
                                            <div class="at-property-img">
                                                <img src="' . url('/assets/services/' . image_order($sera->images)) . '" style="width: 360px; height: 200px;" alt="">
                                            </div>
                                            
                                            <div class="at-property-location" style="min-height: 150px;">
                                                <h5><a href="#" data-title="' . translate($sera->title) . '">' . translate($sera->title) . '</a></h5>
                                                <b>Service Starts at ' . c($sera->price) . '</b>
                                                <p>' . substr($sera->description, 0, 50) . '...</p>';

                                echo '</div>
                                        </div>
                                    </a>
                                    <a class="pro-del" href="'.url('del-ser'.'?id='.$serwish->id).'">x</a>
                                </div>';
                            }
                        }
                    }
                    ?>
                </div>
                <div class="row">
                    <?php if(!empty($prod)){
                        echo '<div class="col-md-12">
                                <div class="at-sec-title">
                                    <h2 style="text-transform: none;">Products</h2>
                                    <div class="at-heading-under-line">
                                        <div class="at-heading-inside-line"></div>
                                    </div>
                                </div>
                            </div>';
                        foreach($prod as $prowish){
                            $pro = DB::select('SELECT * FROM products Where id = '.$prowish->product_id)[0];
                            echo '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                            <div class="at-property-item at-col-default-mar" id="'.$pro->id.'">
                                                <div class="at-property-img">
                                                <a href="product/'.path($pro->title,$pro->id).'" data-title="<'.translate($pro->title).'">
                                                    <img src="'.url('/assets/products/'.image_order($pro->images)).'" style="width: 100%; height: 150px;" alt="">
                                                    </a>
                                                </div>
                                                <div class="at-property-location" style="min-height: 15px;">
                                                    <p>
                                                        <a href="product/'.path($pro->title,$pro->id).'" data-title="'.translate($pro->title).'">'.translate($pro->title).'</a>
                                                        <br><span class="price">'.c($pro->price).'</span>
                                                        <div class="rating">';
                            $rates = getRatings($pro->id);
                            $tr = $rates['rating']; $i = 0; while($i<5){ $i++;
                                echo '<i class="star'.(($i<=$rates["rating"]) ? "-selected" : "").'"></i>';
                                $tr--; }
                            echo '('.$rates["total_ratings"].')
                                                        </div>
                                                        <div class="cart-btn-custom">
                                                                    <button class="bg" data-redirect="product/'.path($pro->title,$pro->id).'" data-title="'.translate($pro->title).'"><i class="icon-basket"></i> Add to Cart</button>
                                                                </div>
                                                    </p>
                                                    
                                                </div>
                                               
                                            </div>
                                             <a class="pro-del" href="'.url('del-pro'.'?id='.$prowish->id).'">x</a>
                                        </div>';
                        }
                    }  ?>

                </div>
            </div>
        </div>
        </div>

    </section>


<?php echo $footer?>
<script>
    jQuery(window).on('load', function(){
        var $ = jQuery;
        var $container = $('.grid');
        $container.masonry({
            columnWidth:10,
            gutterWidth: 15,
            itemSelector: '.grid-item'
        });
    });
</script>

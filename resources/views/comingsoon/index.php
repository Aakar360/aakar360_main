<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <!-- Title of Website -->
  <title>Aakar360</title>
  <meta name="description" content="Aakar360"/>
  <meta name="keywords" content="Aakar360"/>
  <meta name="author" content="Aakar360">
  <!-- Favicon -->
  <link rel="icon" href="favicon.png" type="image/png">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?=$data['tp'] ?>/comingsoon/css/plugins/bootstrap.min.css">
  <!-- Font Icons -->
  <link rel="stylesheet" href="<?=$data['tp'] ?>/comingsoon/css/icons/font-awesome.css">
  <link rel="stylesheet" href="<?=$data['tp'] ?>/comingsoon/css/icons/linea.css">
  <!-- Google Fonts -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700">
  <!-- Plugins CSS -->
  <link rel="stylesheet" href="<?=$data['tp'] ?>/comingsoon/css/plugins/loaders.min.css">
  <link rel="stylesheet" href="<?=$data['tp'] ?>/comingsoon/css/plugins/photoswipe.css">
  <link rel="stylesheet" href="<?=$data['tp'] ?>/comingsoon/css/icons/photoswipe/icons.css">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?=$data['tp'] ?>/comingsoon/css/style.css">
  <!-- Responsive CSS -->
  <link href="<?=$data['tp'] ?>/comingsoon/css/responsive.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body data-spy="scroll" data-target=".scrollspy" class="bg-dark">
<div id="page">
    <!-- Particle Waves BG -->
    <div id="particle-waves" class="section-overlay" data-bg-color="#1f64c5" data-wave-color="#28bbff"></div>
    <!-- /End Particle Waves BG -->

    <!-- Overlay BG -->
    <div class="section-overlay bg-black overlay-opacity"></div>
    <!-- /End Overlay BG -->
  <div class="container-fluid">
    <div class="row">
      <div id="info" class="col-md-12 text-white text-center page-info col-transform">
        <div class="vert-middle">
          <div class="reveal scale-out">
            <div class="p-t-b-15">
              <!-- Headline & Description -->
              <h2>We're coming soon</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /#page -->
<div id="photoswipe"></div>

<!-- Scripts -->
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/jquery1.11.2.min.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/bootstrap.min.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/scrollreveal.min.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/contact-form.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/newsletter-form.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/jquery.ajaxchimp.min.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/photoswipe/photoswipe.min.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/photoswipe/photoswipe-ui-default.min.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/jquery.countdown.min.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/three.min.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/Projector.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/CanvasRenderer.js"></script>
<script src="<?=$data['tp'] ?>/comingsoon/js/plugins/prefixfree.min.js"></script>

<!-- Custom Script -->
<script src="<?=$data['tp'] ?>/comingsoon/js/custom.js"></script>
</body>
</html>

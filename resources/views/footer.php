<section class="at-product-category" id="footer2" style="margin: 0; padding: 50px 0;border:none;background: #292929;">
	<div class="container">
		<div class="row">
            <?php if($cust_type == 'institutional'){?>
            <div class="col-md-12">
                <div class="col-md-6">
                    <h4>HELP</h4>
                    <a href="./cancellation">Cancellation & Returns</a>
                    <a href="./terms-conditions">Terms and Conditions</a>
                    <a href="#">Privacy Policy</a>
                </div>
                <div class="col-md-6">
                    <h4>Aakar360</h4>
                    <a href="<?=url('')?>">Home</a>
                    <a href="./about-us">About Us</a>
                    <a href="./contact-us">Contact Us</a>
                </div>
            </div>
            <?php } else{?>
            <div class="col-md-12">
                <div class="col-md-4">
                    <h4>HELP</h4>
                    <a href="./shipping">Shipping</a>
                    <a href="./cancellation">Cancellation & Returns</a>
                    <a href="./faq">FAQ</a>
                    <a href="./report-infringement">Report Infringement</a>
                    <a href="./terms-of-use">Terms of Use</a>
                    <a href="./terms-conditions">Terms and Conditions</a>
                </div>
                <div class="col-md-4">
                    <h4>Aakar360</h4>
                    <a href="./contact-us">Contact Us</a>
                    <a href="./about-us">About Us</a>
                    <a href="./careers">Careers</a>
                    <a href="./stories">Aakar360 Stories</a>
                    <a href="./press">Press</a>
                    <a href="./sell">Sell on Aakar360</a>
                    <a href="register/dealer">Become a Dealer</a>
                </div>
                <div class="col-md-4">
                    <h4>MISC</h4>
                    <a href="./online-shopping">Online Shopping</a>
                    <a href="./affiliate-program">Affiliate Program</a>
                    <a href="./gift-card">Gift Card</a>
                    <a href="./subscription">Specedoor First Subscription</a>
                    <a href="./sitemap">Sitemap</a>
                </div>
            </div>
            <?php }?>
		</div>
	</div>
</section>

<section class="at-product-category" id="footer3" style="margin: 0; padding: 50px 0;border:none;background: #191919;">
    <?php if($cust_type == 'institutional'){?>
	<div class="container" style="display:none;">
        <?php } else{?>
        <div class="container">
        <?php }?>
		<div class="row">
			<?php
			$pcats = false;														
			$pcats = $data['p_cat'];
			if($pcats){
				foreach($pcats as $pcat){
			?>
			<div class="col-md-12">
				<span class="text-uppercase"><?php echo $pcat->name; ?> : </span>
				<?php 
					$pscats = false;
					$pscats = getProductCategories($pcat->id);
					if($pscats){
						foreach($pscats as $pscat){ 
				?>
				<a href="./products?catlink=<?=$pscat->id;?>"><?=$pscat->name;?></a> | 
					<?php } } ?>
			</div>
			<?php } } ?>
			
			<?php
			$scats = false;														
			$scats = $data['s_cat'];
			if($scats){
				foreach($scats as $scat){
			?>
			<div class="col-md-12">
				<span class="text-uppercase"><?php echo $scat->name; ?> : </span>
				<?php 
					$sscats = false;
					$sscats = getProductCategories($scat->id);
					if($sscats){
						foreach($sscats as $sscat){ 
				?>
				<a href="./sub_service/<?php echo path($sscat->name, $sscat->id); ?>"><?=$sscat->name;?></a> | 
					<?php } } ?>
			</div>
			<?php } } ?>
			
			
		</div>
	</div>
</section>
<!--footer class="at-main-footer at-over-layer-black">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="at-footer-about-col at-col-default-mar">
                    <div class="at-footer-logo">
                        <a href="<?=url('')?>"><img src="<?=$data['cfg']->logo ?>"></a>
                    </div>
                    <hr>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                    <div class="at-social text-left">
                        <?php foreach($social as $platform => $account) {?>
                            <a href="<?=$account ?>"><i class="icon-social-<?=$platform ?>"></i></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-12">
                <div class="at-footer-link-col at-col-default-mar">
                    <h4>Quick links</h4>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                    <ul>
                        <?php foreach ($links as $link) {?>
                            <li><a href="<?=$link->link ?>" ><?=translate($link->title) ?></a></li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="at-footer-Tag-col at-col-default-mar">
                    <h4>Tags</h4>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                    <div class="at-tag-group clearfix">

                        <a class="hvr-bounce-to-right at-bg-hvr" href="#">WEB DESIGN</a>
                        <a class="hvr-bounce-to-right" href="#">HTML</a>
                        <a class="hvr-bounce-to-right" href="#">RESPONSIVE</a>
                        <a class="hvr-bounce-to-right" href="#">PHP</a>
                        <a class="hvr-bounce-to-right" href="#">JQUERY</a>
                        <a class="hvr-bounce-to-right" href="#">JAVASCRIPT</a>
                        <a class="hvr-bounce-to-right" href="#">ANIMATION</a>
                        <a class="hvr-bounce-to-right" href="#">CSS</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="at-footer-gallery-col at-col-default-mar">
                    <h4>Awesome Gallery</h4>
                    <div class="at-heading-under-line">
                        <div class="at-heading-inside-line"></div>
                    </div>
                    <div class="at-gallery clearfix">
                        <!--portfolio single img end-->
                        <!--ul>
                            <li>
                                <a class="thumbnail gallery" href="assets/images/gallery/1.jpg"><img src="assets/images/gallery/1.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="thumbnail gallery" href="assets/images/gallery/2.jpg"><img src="assets/images/gallery/2.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="thumbnail gallery" href="assets/images/gallery/3.jpg"><img src="assets/images/gallery/3.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="thumbnail gallery" href="assets/images/gallery/4.jpg"><img src="assets/images/gallery/4.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="thumbnail gallery" href="assets/images/gallery/5.jpg"><img src="assets/images/gallery/5.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="thumbnail gallery" href="assets/images/gallery/6.jpg"><img src="assets/images/gallery/6.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="thumbnail gallery" href="assets/images/gallery/7.jpg"><img src="assets/images/gallery/7.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="thumbnail gallery" href="assets/images/gallery/8.jpg"><img src="assets/images/gallery/8.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="thumbnail gallery" href="assets/images/gallery/9.jpg"><img src="assets/images/gallery/9.jpg" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer-->
<!-- footer end -->

<!-- Copyright start from here -->
<section class="at-copyright">
    
        <div class="row">
            <p>Copyright ©2018 All Rights Reserved || Made with <i class="fa fa-heart" style="color: red;"></i> By <a href="https://www.adroweb.com" target="_blank">Adroweb</a> </p>
        </div>
    
</section>


        <!-- Bootstrap Core JavaScript -->
        <script src="assets/js/bootstrap.min.js"></script>

        <!-- all plugins and JavaScript -->
        <script type="text/javascript" src="assets/js/css3-animate-it.js"></script>

        <script type="text/javascript" src="assets/js/bootstrap-dropdownhover.min.js"></script>

        <script type="text/javascript" src="assets/js/featherlight.min.js"></script>

        <script type="text/javascript" src="assets/js/featherlight.gallery.min.js"></script>

        <script type="text/javascript" src="assets/js/jquery.flexslider.js"></script>

        <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>

        <script type="text/javascript" src="assets/js/jarallax.js"></script>

        <script type="text/javascript" src="assets/js/jquery-ui.js"></script>

        <script type="text/javascript" src="assets/js/jquery-scrolltofixed-min.js"></script>

        <script type="text/javascript" src="assets/js/morphext.min.js"></script>

        <script type="text/javascript" src="assets/js/dyscrollup.js"></script>

<script>
    $(document).ready(function() {
        $('#datatable-editable').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
    } );

    $(document).ready(function() {
        $('#datatable-editable1').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
    } );
</script>
        <!-- Main Custom JS -->
       
		<script src="themes/default/assets/main.js"></script>

		<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
		<!-- the jScrollPane script -->
		<script type="text/javascript" src="assets/js/jquery.mousewheel.js"></script>

		<script type="text/javascript" src="assets/js/jquery.contentcarousel.js"></script>

		<script src="assets/js/slick.js"></script>

 <script type="text/javascript" src="assets/js/main.js"></script>

 <script src="assets/js/select2.js"></script>

 <script src="assets/js/cbpFWTabs.js"></script>
 <script src="assets/js/aos.js"></script>

 <script src="themes/default/assets/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.bootstrap-dropdown-hover.js"></script>

<script src="assets/js/carouFredSel-6.2.1.js" type="text/javascript"></script>
        <script src="assets/js/getwidthbrowser.js" type="text/javascript"></script>
        <script src="assets/js/cs.bossthemes.js" type="text/javascript"></script>


<script>
    //$('[data-toggle="dropdown"]').bootstrapDropdownHover();
    $.fn.bootstrapDropdownHover();
    //$('#dropdownMenu1').bootstrapDropdownHover();
    //$('.navbar [data-toggle="dropdown"]').bootstrapDropdownHover();
</script>

<script>
    function submitRfr(){
        $('.error').hide();
        var variants = 0;
        if($('.variantsx')){
            if($('.variantsx').val()){
                variants = 1;
            }
        }else{
            variants = -1;
        }
        if(variants == 0){
            $('.variantsx').parent().find('.error').html('Variant is mandatory').show();
        }
        else if($('textarea[name=msg]').val() == ''){
            $('textarea[name=msg]').parent().find('.error').html('Enquiry/Message is mandatory.').show();
        }
        else {
            $('form#rfrForm').submit();
        }
    }
    $( function() {
        $( "#datepicker" ).datepicker();
    } );
    $( function() {
        $( "#datepicker1" ).datepicker();
    } );
	$(document).ready(function(){
		/*AOS.init({
		  duration: 600,
		  offset: 200
		});*/
  // Add smooth scrolling to all links
        $('.select2').select2({
            width: '100%'
        });
	$("a.animation").on('click', function(event) {
		// Make sure this.hash has a value before overriding default behavior
		if (this.hash !== "") {
		  // Prevent default anchor click behavior
		  event.preventDefault();

		  // Store hash
		  var hash = this.hash;

		  // Using jQuery's animate() method to add smooth page scroll
		  // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		  $('html, body').animate({
			scrollTop: $(hash).offset().top - 80
		  }, 800, function(){
			  setTimeout(function(){
				$('.anim1').addClass('fadeIn');
				setTimeout(function(){ $('.anim1').removeClass('fadeIn'); }, 3000);
			  }, 500);
		  });
		} // End if
		
	  });
		
	});
    function getRegModal(){
        event.preventDefault();
        $('#open-register').click();
    }
    $(document).on('click', '.request-rate', function(){
        $('#rfrModal').modal('show');
    });
</script>

<button class="btn btn-primary hidden" id="open-register" data-popup-open="popup-register">Get Started !</button>
<div class="popup" id="popup-register" data-popup="popup-register">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <div class="row" style="margin-top: 40px;">
                <a href="register/individual">
                    <div class="col-sm-3 anim1 animated" data-duration="2000">
                        <div class="icon-box boxed-style align-center animated" data-animation="zoomOut" data-delay="600" style="width: 230px; height: 250px; box-shadow: 0 0 20px #a5a5a5; background-color: white;">
                            <div class="animated-icon colored" style="width: 100px;">
                                <img src="<?=url('assets/images/individual.png'); ?>" style="width: 70px;height: auto;"/>
                            </div>
                            <div class="ib-content">
                                <h5>Individual Buyer</h5>
                                <p>Starting your own Project?</p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="register/institutional">
                    <div class="col-sm-3 anim1 animated" data-duration="2000">
                        <div class="icon-box boxed-style align-center animated" data-animation="zoomOut" data-delay="300" style="width: 230px; height: 250px; box-shadow: 0 0 20px #a5a5a5; background-color: white;">
                            <div class="animated-icon colored" style="width: 100px;">
                                <img src="<?=url('assets/images/institutional.png'); ?>" style="width: 70px;height: auto;"/>
                            </div>
                            <div class="ib-content">
                                <h5 style="width: 200px; margin-left: -20px;">Institutional Buyer</h5>
                                <p>Require Material in Bulk Quantity?</p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="register/professional">
                    <div class="col-sm-3 anim1 animated" data-duration="2000">
                        <div class="icon-box boxed-style align-center animated" data-animation="zoomOut"  style="width: 230px; height: 250px; box-shadow: 0 0 20px #a5a5a5; background-color: white;">
                            <div class="animated-icon colored" style="width: 100px;">
                                <img src="<?=url('assets/images/professional.png'); ?>" style="width: 70px;height: auto;"/>
                            </div>
                            <div class="ib-content">
                                <h5>Professional</h5>
                                <p>Want Technology Driven Solution for your Client?</p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="register/seller">
                    <div class="col-sm-3 anim1 animated" data-duration="2000">
                        <div class="icon-box boxed-style align-center animated" data-animation="zoomOut" data-delay="600" style="width: 230px; height: 250px; box-shadow: 0 0 20px #a5a5a5; background-color: white;">
                            <div class="animated-icon colored" style="width: 100px;">
                                <img src="<?=url('assets/images/supplyer.png'); ?>" style="width: 70px;height: auto;"/>
                            </div>
                            <div class="ib-content">
                                <h5 style="width: 200px; margin-left: -20px;">Manufacturer / Supplier</h5>
                                <p>Looking for New Way to Reach?</p>
                            </div>
                        </div>
                    </div>
                </a>

            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-register" href="#">x</a>
    </div>
</div>

<div id="rateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="<?=url('i/change-hub'); ?>" method="post" id="hubForm">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                <h4 class="modal-title">Select Rate Type</h4>
            </div>
            <div class="modal-body">
                    <?=csrf_field()?>
                    <div class="row">
                        <?php foreach($hubs as $h){ ?>
                            <div class=" col-md-3">
                                <label><input type="radio" value="<?=$h->id?>" name="hub" required/> <?=$h->title?></label>
                            </div>
                        <?php } ?>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default" >Done</button>
            </div>
            </form>
        </div>

    </div>
</div>
	</body>
</html>

<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Units</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="units" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
            <form action="" method="post" style="max-width: 100%">
				'.csrf_field().'
				
					<div class="row">
						<div class="input-field col s12 m4">
							Name
							<input name="name" type="text" class="form-control" required />
						</div>
						<div class="input-field col s12 m4">
							Symbol
							<input name="symbol" type="text" class="form-control" required />
						</div>
						
						
						<div class="input-field col s12 m4">
							Conversion
							<input name="unit" type="text" class="form-control" />
						</div></div>
						<div class="row">
						
						<label style="font-size: 16px; margin-right: 10px;" class="control-label">Is Default?</label>
							<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes
  <input type="radio" id="yes" value="1" name="default">
  <span class="checkmark"></span>
</label> <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No
  <input type="radio" checked id="no" value="0" name="default">
  <span class="checkmark"></span></label>
						
						</div>
						<div class="row">
						<input name="add" type="submit" value="Add Unit" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Unit</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="units" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="" method="post"style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						<div class="input-field col s12 m4">
							<label class="control-label">Name</label>
							<input name="name" type="text" class="form-control" required value="'.$data['unit']->name.'"/>
						</div>
						<div class="input-field col s12 m4">
							<label class="control-label">Symbol</label>
							<input name="symbol" type="text" class="form-control" required value="'.$data['unit']->symbol.'"/>
						</div>
						<div class="input-field col s12 m4">
							<label class="control-label">Conversion</label>
							<input name="unit" type="text" class="form-control" value="'.$data['unit']->unit.'"/>
						</div>
						
						</div>
						<div class="row">
						
						<label style="font-size: 16px; margin-right: 10px;" class="control-label">Is Default?</label>';
    $val = $data['unit']->def;
    if($val == '1'){
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes
  <input type="radio" id="yes" checked value="1" name="default">
  <span class="checkmark"></span>
</label> <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No
  <input type="radio" checked id="no" value="0" name="default">
  <span class="checkmark"></span>
</label></div>';
    }else {
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes
  <input type="radio" id="yes"  value="1" name="default" checked>
  <span class="checkmark"></span>
</label> <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No
  <input type="radio"  id="no" value="0" name="default" checked>
  <span class="checkmark"></span>
</label></div>';
    }
    echo '
						
				<input name="edit" type="submit" value="Update Unit" style="padding:3px 25px;" class="btn btn-primary" />
					
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Units<a href="units?add" style="padding:3px 15px; font-size: 15px" class="add">Add Units</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your Weight Units </h5>
                <div class="row" style="position:relative;">
                    <div class="col s12 table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr role="row">
                                <th>Sr. No.</th>
                                <th>Name</th>
                                <th>Symbol</th>
                                <th>Conversion</th>
                                <th>Default</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['units'] as $unit){
                                echo'<tr>
            <td>'.$sr.'</td>
			<td>'.$unit->name.'</td>
			<td>'.$unit->symbol.'</td>
			<td>'.$unit->unit.'</td>
			<td>';
                                if($unit->def){
                                    echo '<label class="label label-success">Yes</label>';
                                }else{
                                    echo '<label class="label label-warning">No</label>';
                                }
                                echo '</td>
			<td>
                <a href="units?delete='.$unit->id.'"><i class="icon-trash"></i></a>
				<a href="units?edit='.$unit->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          ';
                                $sr++; }
                            }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<?php
echo $data['footer'];
?>

<style>
    /* The container */
    .radio_container {
        display:  contents;
        position: relative;
        padding-left: 20px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: .8rem;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .radio_container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .radio_container:hover input ~ .checkmark {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .radio_container input:checked ~ .checkmark {
        background-color: #2196F3;
        height: 16px;
        width:16px;
        top:5px;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
        background-color: #eee;
    }

    /* Show the indicator (dot/circle) when checked */
    .radio_container input:checked ~ .checkmark:after {
        display: block;
        background-color: #eee;
    }

    /* Style the indicator (dot/circle) */
    .radio_container .checkmark:after {
        top: 19px;
        left: 9px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        position: fixed;

        background-color: #eee;
    }

    [type='radio']:not(:checked) + span, [type='radio']:checked + span {

        padding-left: 0px;
        hieght:20px;
    }
</style>

<script src="<?=$data['tp']; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$data['tp']; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$data['tp']; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$data['tp']; ?>/admin/js/select2.min.js"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$data['tp']; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->


<!-- Default inline 1-->


<!-- Default inline 2-->
<!--<div class="custom-control custom-radio custom-control-inline">-->
<!--    <input type="radio" class="custom-control-input" id="defaultInline2" name="inlineDefaultRadiosExample">-->
<!--    <label class="custom-control-label" for="defaultInline2">2</label>-->
<!--</div>-->
<!---->
<!--<!-- Default inline 3-->
<!--<div class="custom-control custom-radio custom-control-inline">-->
<!--    <input type="radio" class="custom-control-input" id="defaultInline3" name="inlineDefaultRadiosExample">-->
<!--    <label class="custom-control-label" for="defaultInline3">3</label>-->
<!--</div>-->
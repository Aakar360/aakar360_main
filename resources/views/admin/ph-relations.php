<?php
echo $header;
if(isset($_GET['add'])) {	
	echo $notices.'<form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="ph-relations"><i class="icon-arrow-left"></i></a>Add new PH Relation</h5>
				<fieldset>
				    <div class="row">
					  <div class="form-group col-md-12" style="margin: 0 0 10px 0">
						<label class="control-label">Hubs</label>
						<select name="hubs[]" multiple class="form-control select2" required data-placeholder="Select Hubs" id="hubs">';
                            foreach ($hubs as $hub){
								echo '<option value="'.$hub->id.'">'.$hub->title.'</option>';
							}
							echo '</select>
					  </div>
                  </div>
                  <div class="row phitems">
                    <div class="phitem">
                          <div class="form-group col-md-5 proDiv" style="margin: 0 0 10px 0">
                                <label class="control-label">Products</label>
                                <select class="form-control select2 products" id="products">';
                                echo '<option value="">Select Product</option>';
                                foreach ($products as $product){
                                    echo '<option value="'.$product->id.'">'.$product->title.'</option>';
                                }
                                echo '</select>
                          </div>
                          <div class="form-group col-md-3" style="margin: 0 0 10px 0">
                                <label class="control-label">Price</label>
                                <input type="text" value="0" class="form-control price"/>
                          </div>
                          <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                <label class="control-label">Loading</label>
                                <input type="text" value="0" class="form-control lprice"/>
                          </div>
                          <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                <button type="button" class="btn btn-primary addNew" value="1">+</button>
                          </div>
					  </div>
                  </div>
					  <input name="add" type="submit" value="Add PH" class="btn btn-primary" />
				</fieldset>
			</form>';
} elseif(isset($_GET['edit'])) {
	echo $notices.'<form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="ph-relations"><i class="icon-arrow-left"></i></a>Edit Hub</h5>
				<fieldset>
				    <div class="row">
					  <div class="form-group col-md-12" style="margin: 0 0 10px 0">
						<label class="control-label">Hubs</label>
						<select name="hubs[]" multiple class="form-control select2" required data-placeholder="Select Hubs" id="hubs">';
							foreach ($hubs as $hub){
                                $shs = explode(',', $ph->hubs);
								echo '<option '.(in_array($hub->id, $shs) ? 'selected' : '').' value="'.$hub->id.'">'.$hub->title.'</option>';
							}
							echo '</select>
					  </div>
                  </div>
                  <div class="row phitems">
                      <div class="phitem">
                          <div class="form-group col-md-5 proDiv" style="margin: 0 0 10px 0">
                                <label class="control-label">Products</label>
                                <select class="form-control select2 products" id="products">';
                                echo '<option value="">Select Product</option>';
                                foreach ($products as $product){
                                    echo '<option value="'.$product->id.'">'.$product->title.'</option>';
                                }
                                echo '</select>
                          </div>
                          <div class="form-group col-md-3" style="margin: 0 0 10px 0">
                                <label class="control-label">Price</label>
                                <input type="text" value="0" class="form-control price"/>
                          </div>
                          <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                <label class="control-label">Loading</label>
                                <input type="text" value="0" class="form-control lprice"/>
                          </div>
                          <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                <button type="button" class="btn btn-primary addNew" value="1">+</button>
                          </div>
					  </div>';
                    $pros = json_decode($ph->products);
                    $i = 1;
                    foreach($pros as $pro) {
                        $pr = \App\Products::find($pro->product);
                        echo '<div class="phitem color2">
                          <div class="form-group col-md-5 proDiv" style="margin: 0 0 10px 0">
                          <input type="hidden" value="'.$pr->id.'" name="products[]"><label class="control-label">Product</label><input type="text" readonly="" value="'.$pr->title.'" class="form-control"></div>
                          <div class="form-group col-md-3" style="margin: 0 0 10px 0">
                                <label class="control-label">Price</label>
                                <input type="text" value="'.$pro->price.'" class="form-control price" name="price[]">
                          </div>
                          <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                <label class="control-label">Loading</label>
                                <input type="text" value="'.$pro->loading.'" class="form-control lprice" name="lprice[]">
                          </div>
                          <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                <button type="button" class="btn btn-danger removeItem" value="'.$i.'">-</button>
                          </div>
					  </div>';
                        $i++;
                    }
              echo '</div>
                  <input name="edit" type="submit" value="Edit Hub" class="btn btn-primary" />
				</fieldset>
			</form>';
} else {
?>
<div class="head">
<h3>Products-Hubs Relations<a href="ph-relations?add" class="add">Add PH</a></h3>
<p>Manage your products and manufacturing hubs relations</p>
</div>
<div class="row">
    <div class="col-lg-12">
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Hubs</th>
                <th>Products</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
<?php
    $sr = 1;
	echo $notices;
	foreach ($phs as $p){
	    $hs = array_pluck(\App\ManufacturingHubs::whereIn('id', explode(',', $p->hubs))->get(), 'title');
	    $pros = json_decode($p->products);
	    $pss = array_pluck($pros, 'product');
	    $ps = array_pluck(\App\Products::whereIn('id', $pss)->get(), 'title');
		echo'<tr>
            <td>'.$sr.'</td>
			<td>'.implode(', ', $hs).'</td>
            <td>'.implode(', ', $ps).'</td>
            <td>'.date_format(date_create($p->created_at), "d M, Y").'</td>
			<td>
                <a href="ph-relations?delete='.$p->id.'"><i class="icon-trash"></i></a>
				<a href="ph-relations?edit='.$p->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';
	$sr++; }
}?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
<?php
echo $footer;
?>
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            allowClear: true
        });
    });
    $(document).on('click', '.addNew', function(){
        var ele = $(this);
        var newItem = $('.phitem:first').clone();
        var pid = $('.phitem:first').find('.products').val();
        if(pid != '') {
            var pval = $('.phitem:first .proDiv').find('.products option:selected').text();
            //alert(pval);
            var proField = '<input type="hidden" value="' + pid + '" name="products[]"/>';
            var proFieldLabel = '<label class="control-label">Product</label>';
            var proFieldView = '<input type="text" readonly value="' + pval + '" class="form-control"/>';
            $('.phitems').append(newItem);
            $('.phitem:last').find('.addNew').val($('.phitem').length).removeClass('btn-primary').removeClass('addNew').addClass('btn-danger').addClass('removeItem').html('-');
            $('.phitem:last').find('.proDiv').html(proField + proFieldLabel + proFieldView);
            $('.phitem:last').find('.price').attr('name', 'price[]');
            $('.phitem:last').find('.lprice').attr('name', 'lprice[]');
            $('.phitem:last').addClass('color2');
        }else{
            alert('You must select a Product to Add.');
        }
    });
    $(document).on('click', '.removeItem', function(){
        $(this).parent().parent().remove();
    });
</script>

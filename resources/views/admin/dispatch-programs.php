<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$tp; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/custom/custom.css">
    <link href="<?=$tp; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>
<!-- END: Head-->

<?php echo $header;
    ?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5 class="card-title" style="font-size: 20px;">Manage Dispatch Program</h5>

                <div class="divider"></div>
                <div class="row" style="position:relative;">
        </div>
        <?php if($notices !== ''){ ?>
        <?=$notices?>
        <?php } ?>

        <div class="table-responsive">
            <table class="table table-striped table-bordered" id="datatable-editable">
                <thead>
                <tr class="bg-blue">
                    <th>ID</th>
                    <th>User Details</th>
                    <th>Item Details</th>
                    <th>Schedule Date</th>
                    <th>Status</th>
                    <th>Created Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($dispatches as $dispatch) {
                    $status = $dispatch->status;
                    $customer = DB::table('customers')->where('id', $dispatch->user_id)->first();
                    echo '<tr>
            <td>' . $dispatch->id . '</td>
            <td>Name : ' . $customer->name . '<br>Company : '.$customer->company.'<br>Email : '.$customer->email.'<br>Mobile : '.$customer->mobile.'</td>';
                    $items = json_decode($dispatch->items);
                    $party = json_decode($dispatch->party);
                    $variants = json_decode($dispatch->variants);
                    $quantity = json_decode($dispatch->quantity);
                    $unit = json_decode($dispatch->unit);
                    $html = '<ul id="myUL">';
                    $i = 0;
                    foreach ($party as $p){
                        $html .= '<li><span class="caretx">'.getPartyName($p[0]).'</span>';
                        $html .= '<ul class="nested">';
                        foreach ($items[$i] as $item){
                            $html .= '<li><span class="caretx">'.getProductName($item).'</span>';
                            $html .= '<ul class="nested">';
                            $j = 0;
                            foreach($variants[$i] as $var){
                                $html .= '<li>* '.getVariantName($var).' - '.$quantity[$i][$j].' '.getUnitSymbol($unit[$i][$j]).'</li>';
                                $j++;
                            }
                            $html .= '</ul>';
                            $html .= '</li>';
                        }
                        $html .= '</ul>';
                        $html .= '</li>';
                        $i++;
                    }

            echo '<td>' . $html . '</td>
            <td>' . date('d, M Y', strtotime($dispatch->schedule)) . '</td>
            <td>' . ($status == 0 ? '<span class="badge alert-warning">Pending</span>' : ($status == 1 ? '<span class="badge alert-primary">Approved</span>' : ($status == 2 ? '<span class="badge alert-info">Loading</span>' : ($status == 3 ? '<span class="badge alert-warning">In Transit</span>' : '<span class="badge alert-success">Delivered</span>')))) .'</td>
            <td>' . date('d, M Y h:i:s a', strtotime($dispatch->created_at)) . '</td>
            <td>';
                    if ($status == 1) {
                        echo '<a href="dispatch-programs?id=' . $dispatch->id . '&status=2" class="btn btn-xs btn-info">Set Loading</a>';
                    } else if($status == 2) {
                        echo '<a href="dispatch-programs?id=' . $dispatch->id . '&status=3" class="btn btn-xs btn-warning">Set In Transit</a>';
                    }else if($status == 3){
                        echo '<a href="dispatch-programs?id=' . $dispatch->id . '&status=4" class="btn btn-xs btn-success">Set Delivered</a>';
                    }else if($status == 4) {
                        echo 'No Action';
                    }else{
                        echo '<a href="dispatch-programs?id=' . $dispatch->id . '&status=1" class="btn btn-xs btn-primary">Approve</a>';
                    }
                    echo '</td>
          </tr>';
                } ?>
                </tbody>
            </table>
        </div>
            </div>
        </div>
    </div>
</div>
</div>
        <?php
echo $footer;
?>

<script src="<?=$tp; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/admin/js/select2.min.js"></script>
<script src="<?=$tp; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->

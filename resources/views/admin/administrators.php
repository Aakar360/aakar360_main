<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Add new Admin</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="administrators" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="form-group col-md-6">
							<label class="control-label">Name</label>
							<input name="name" type="text"  class="form-control" required/>
						  </div>
						  <div class="form-group col-md-6">
							<label class="control-label">Email</label>
							<input name="email" type="text" class="form-control"  required/>
						  </div>
						  <div class="form-group col-md-6">
							<label class="control-label">Password</label>
							<input name="pass" type="password" class="form-control"  required/>
						  </div>
						  </div>
						  <input name="add" type="submit" value="Add Admin" style="padding:3px 25px;" class="btn btn-primary" />
					
				</form>
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Edit admin</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="administrators" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="form-group col-md-6">
							<label class="control-label">Name</label>
							<input name="name" type="text"  value="'.$data['admin']->u_name.'" class="form-control" required/>
						  </div>
						 <div class="form-group col-md-6">
							<label class="control-label">Email</label>
							<input name="email" type="text"  value="'.$data['admin']->u_email.'" class="form-control" required/>
						  </div>
						  <div class="form-group col-md-6">
							<label class="control-label">Password</label>
							<input name="pass" type="password"  class="form-control" required/>
						  </div>
						  </div>
						  <input name="edit" type="submit" value="Edit admin" style="padding:3px 25px;" class="btn btn-primary" />
					
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 650px;">
                    <h3>Administrators<a href="administrators?add" class="add">Add admin</a></h3>
                    <p>Manage administration accounts</p>

    <?php
    echo $data['notices'];
    foreach ($data['admins'] as $admin){
        echo'
			<div class="mini bloc">
			<h5>'.$admin->u_name.'
				<div class="tools">
					<a href="administrators?delete='.$admin->u_id.'"><i class="icon-trash"></i></a>
					<a href="administrators?edit='.$admin->u_id.'"><i class="icon-pencil"></i></a>
				</div>
			</h5>
			<p>'.$admin->u_email.'</p>
			</div>';
    }
}
echo $data['footer'];
?>
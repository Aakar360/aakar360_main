<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title><?=$data['title']; ?></title>
    <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/vendors/animate-css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/vendors/chartist-js/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/vendors/chartist-js/chartist-plugin-tooltip.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/css/pages/dashboard-modern.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/css/pages/intro.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/css/custom/custom.css">
    <link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?=$data['tp']; ?>/css/select2.min.css" rel="stylesheet" />
</head>
<?=$data['header'];?>
<?=$data['notices'];?>
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <div class="card">
                    <div class="card-content">
                        <h4 class="card-title" style="color: #0d1baa; margin-left: 5px">Push Notification(Retail)</h4>
                        <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
                            <input type="hidden" name="_token" value="<?php echo  csrf_token(); ?>">
                            <div class="col s12 m11"><i class="material-icons prefix" style=" margin-left: -13px; margin-top: 26px;">group</i>
                                <div class="col s12 m12" style="margin-top: -51px;">
                                    Select Customers<label><input id="customer_check_all" name="customer_check_all_name"  type="checkbox">
                                        <span class="customer_all" style="text-decoration: none;margin-left: 15px;font-size: 12px;padding-left: 20px;">Select All</span>
                                    </label>
                                    <select class="customer_list browser-default" name="cust[]" id="contid" multiple style="width: 100%" required>
                                        <?php foreach ($data['customer'] as $customer){
                                            ?>
                                            <option value="<?=$customer->id?>"><?=$customer->name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m1 input-field col s1 btn-margin-sms">
                                Total : <span id="tot_cust"></span>
                            </div>
                            <div class="input-field col s12 m11">
                                <i class="material-icons prefix" style="margin-top: 5px;margin-left: -17px;">contact_phone</i>
                                <div class="col s12 m12">
                                    <input type="text" name="title" class="title" placeholder="Enter Title">
                                </div>
                            </div>

                            <div class="input-field col s12 m11">
                                <i class="material-icons prefix" style="margin-left: -16px; margin-top: 22px;">mail</i>
                                <div class="col s12 m12">
                                    <label for="ccomment" style="text-decoration: none;color: #9e9e9e;font-size: 12px;">Your Notification</label>
                                    <textarea id="msg_input" name="msg" style="font-size: small" class="materialize-textarea " aria-invalid="msgid"></textarea>
                                </div>
                            </div>
                            <div class="input-field col s12 m1 ">
                                Text : [<span id="tot_msg_show"></span>]  msg : [<span id="tot_msg_count">1</span>]
                            </div>

                            <div class="input-field btn-margin-sms col s12 m12" style="margin-top: 12px;margin-bottom: 160px">
                                <center> <input name="note_send" type="submit" value="send" style="padding: 3px 25px;" class="btn btn-primary" /></center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="<?=$data['tp']; ?>/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$data['tp']; ?>/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$data['tp']; ?>/js/plugins.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<!--<script src="<?/*=$data['tp']; */?>/js/scripts/form-elements.js"></script>-->
<!-- END PAGE LEVEL JS-->
<script src="<?=$data['tp']; ?>/js/select2.min.js"></script>
</body>
</html>
<script>
    $(document).ready(function(){
        $('.state_list').select2();
        $('.district_list').select2();
        $('.pgroup_list').select2();
        $('.locality_list').select2();
        $('.customer_list').select2();
        $('.cust_category').select2();
        $('.acc_manager').select2();
        $('.ctype').select2();
        $('.class').select2();
        $('.visited').select2();

        var count = $("#contid :selected").length;
        $('#tot_cust').html(count);

        $('#msg_input').keyup(function() {
            var tot_msg_1 = this.value.length;
            var tot_msg = (parseInt(tot_msg_1)-1)/140;
            $('#tot_msg_show').html(parseInt(tot_msg_1));
            $('#tot_msg_count').html(parseInt(tot_msg)+1);
        });
        $('#contid').change(function () {
            var count = $("#contid :selected").length;
            $('#tot_cust').html(count);
        });

        $('#state_check_all').click(function () {
            var isChecked = $("#state_check_all").is(":checked");
            if (isChecked) {
                $(".state_list option").attr("selected", true);

            } else {
                $(".state_list option").attr("selected", false);
            }
        });
        $('#district_check_all').click(function () {
            var isChecked = $("#district_check_all").is(":checked");
            if (isChecked) {
                $(".district_list option").attr("selected", true);

            } else {
                $(".district_list option").attr('selected', false);
            }
        });
        $('#customer_check_all').click(function () {

            var isChecked = $("#customer_check_all").is(":checked");
            if (isChecked) {
                $(".customer_list option").attr("selected", true);
                var count = $("#contid :selected").length;
                $('#tot_cust').html(count);

            } else {
                $(".customer_list option").attr('selected', false);
                var count = $("#contid :selected").length;
                $('#tot_cust').html(count);
            }
        });

        $('button[name=msg_send]').click(function () {
            $('button[name=msg_send]').html('Wait...');
            var mob = $("#contid").val();
            var cname = encodeURIComponent($("#msg_input").val());
            var mob2 = $(".new_no").val();
            if(mob==''){
                mob = mob2;
            }
            else{
                if(mob2==''){
                    mob = mob;
                }else{
                    mob = mob +','+ mob2;
                }
            }
            mob = mob.toString().replace(/\s/g,'');
            //alert(mob);
            var url = "https://merasandesh.com/api/sendsms";
            $.ajax({
                type: "POST",
                url: url,
                data:{username:'smshop', password: 'Smshop@123', senderid: 'SUPORT', message: cname, numbers:mob, unicode:0},
                crossOrigin: true,
                beforeSend: function(){
                    $('button[name=msg_send]').html('Wait...');
                    $.ajax({
                        type: "POST",
                        url: "msg_save",
                        data:{message:cname, numbers:mob, _token:'<?=csrf_token();?>'},
                        success: function(data){

                        }
                    });
                },
                success: function(data){

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                },
                complete: function(data){
                    $('button[name=msg_send]').html('Send');
                    alert('SMS sent succesfully !');
                    location.reload();
                }
            });

        });

        $('.new_no').blur(function () {
            var a = $(this).val();
            var b = $(this).val().length;
            var c = a.indexOf(",");
            if(a==''){
                $('.send_msg').prop("disabled", false);
            }
            else{
                if(c==-1){
                    if(b==10){
                        $('.send_msg').prop("disabled", false);
                    }
                    else{
                        alert("Please Enter 10 digit no");
                        $('.send_msg').prop("disabled", true);
                    }
                }else{
                    var str = a;
                    var str_array = str.split(',');
                    for(var i = 0; i < str_array.length; i++) {
                        str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                        var tot = str_array[i].length;
                        if(tot!=10){
                            alert("Please Enter 10 digit number before and after comma");
                            $('.send_msg').prop("disabled", true);
                        }
                        else{
                            $('.send_msg').prop("disabled", false);
                        }
                    }
                }
            }

        });

        $("input[name=new_no]").keypress(function (e) {
            if (/\d+|,/i.test(e.key) ){
                console.log("character accepted: " + e.key)
            } else {
                console.log("illegal character detected: "+ e.key)
                return false;
            }
        });
    });
</script>
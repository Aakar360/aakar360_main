<?php
echo $header;
if(isset($_GET['add'])) {
    echo $notices.'<form action="" method="post" enctype="multipart/form-data" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="photos"><i class="icon-arrow-left"></i></a>Add new design</h5>
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Title</label>
						<input name="title" type="text"  class="form-control" />
					  </div>
					                       
					  <div class="form-group">
						<label class="control-label">Description</label>
						<textarea name="description" class="form-control" ></textarea>
					  </div>
					  <div class="form-group">
						<label class="control-label">Services images</label>
					    <input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"/>
					  </div>
					  <div class="form-group">
						<label class="control-label">Price</label>
						<input name="price" type="text" class="form-control"  />
					  </div>

					  <div class="form-group">
							<label class="control-label">Category</label>
							<select name="category" class="form-control select2">
							<option value="0"></option>';
                            foreach ($parents as $parent){
                                echo '<option value="'.$parent->id.'">'.$parent->name.'</option>';
                            }
                            echo '</select>
					  </div>
					  
					  <input name="add" type="submit" value="Add services" class="btn btn-primary" />
				</fieldset>
			</form>';
} else if(isset($_GET['edit'])) {
    echo $notices.'<form action="" method="post" class="form-horizontal single" enctype="multipart/form-data">
			'.csrf_field().'
			<h5><a href="photos"><i class="icon-arrow-left"></i></a>Edit design</h5>
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Title</label>
						<input name="title" type="text"  value="'.$design->title.'" class="form-control" />
					  </div>
					  <div class="form-group">
						<label class="control-label">Price</label>
						<input name="price" type="text" value="'.$design->price.'" class="form-control"  />
					  </div>
					  
                    
					  <div class="form-group">
						<label class="control-label">Description</label>
						<textarea name="description" class="form-control">'.$design->description.'</textarea>
					  </div>';
                        if (!empty($design->images)){
                            echo '<p>Uploading new images will overwrtite current images .</p>';
                            $images = explode(',',$design->images);
                            foreach($images as $image){
                                echo '<img class="col-md-2" src="'.url('/assets/design/'.$image).'" />';
                            }
                            echo '<div class="clearfix"></div>';
                        }
                        echo '<div class="form-group">
							<label class="control-label">Design images</label>
							<input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"/>
						  </div>
					  <div class="form-group">
							<label class="control-label">Category</label>
							<select name="category" class="form-control select2">
							<option value="0"></option>';
                            foreach ($parents as $parent){
                                echo '<option value="'.$parent->id.'" '.($parent->id == $design->category ? 'selected' : '').'>'.$parent->name.'</option>';
                            }
                            echo '</select>
					  </div>
					  
					  <input name="edit" type="submit" value="Edit design" class="btn btn-primary" />
				</fieldset>
			</form>';
} else {
?>
<div class="head">
    <h3>Photo Gallery<a href="photos?add" class="add">Add New Album</a></h3>
    <p>Manage your photos</p>
</div>
<div class="table-responsive">
    <table class="table table-striped table-bordered" id="datatable-editable">
        <thead>
        <tr class="bg-blue">
            <th>Sr. No.</th>
            <th>Album Banner</th>
            <th>Album Title</th>
            <th>Album Category</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
<?php
$sr = 1;
echo $notices;
foreach ($design as $des){
    echo'<tr>
            <td>'.$sr.'</td>
            <td><img src="../assets/design/'.image_order($des->images).'" style="height: 100px; width: 100px;"></td>
            <td>'.$des->title.'</td>';
    foreach ($parents as $parent) {
        echo '<td>' . $parent->name . '</td>';
    }
           ECHO '<td><a href="services?delete='.$des->id.'"><i class="icon-trash"></i></a>
				<a href="services?edit='.$des->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';
          

		echo'</div>
	</div>';
$sr++;}
}
?>
        </tbody>
    </table>
</div>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $footer;
?>




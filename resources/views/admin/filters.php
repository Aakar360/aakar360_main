<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <form action="" method="post" class="form-horizontal single"  enctype="multipart/form-data">
			'.csrf_field().'
			<h5><a href="filters"><i class="icon-arrow-left"></i></a>Add Filter Group</h5>
				<div class="row">
						<div class="form-group">
							<label class="control-label">Name</label>
							<input type="text" name="name" class="form-control"/>
						</div>
						
					  <input name="add" type="submit" value="Submit" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <form action="" method="post" class="form-horizontal single" enctype="multipart/form-data">
			'.csrf_field().'
			<h5><a href="filters"><i class="icon-arrow-left"></i></a>Edit Filter Group</h5>
				<div class="row">
					  
					  <div class="form-group">
							<label class="control-label">Name</label>
							<input type="text" name="name" class="form-control" value="'.$data['filter']->name.'"/> 
					  </div>
					  
					  <input name="edit" type="submit" value="Update" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
}elseif(isset($_GET['add_option'])) {
    $fil = DB::select("SELECT * FROM filter WHERE id = ".$_GET['add_option'])[0];
    echo $data['notices'].'<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <form action="" method="post" class="form-horizontal single"  enctype="multipart/form-data">
			'.csrf_field().'
			<h5><a href="filters"><i class="icon-arrow-left"></i></a>Add Filter Option For '.$fil->name.'</h5>
				<div class="row">
				        <input type="hidden" name="filter_id" value="'.$_GET['add_option'].'">
						<div class="field_wrapper">
                            <div style="padding-bottom: 10px;">
                                <input type="text" name="name[]" value="" class="form-control" style="float: left; width: 450px;"/>
                                <a href="javascript:void(0);" class="add_button" title="Add field" style="float: right;"><i class="icon-plus" style="font-size: 20px;"></i> </a>
                            </div>
                            <div  style="padding-bottom: 10px;">&nbsp;</div>
                        </div>
						
					  <input name="add_option" type="submit" value="Submit" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit_option'])) {
    echo $data['notices'].'<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <form action="" method="post" class="form-horizontal single" enctype="multipart/form-data">
			'.csrf_field().'
			<h5><a href="filters"><i class="icon-arrow-left"></i></a>Edit Filter Option </h5>
				<div class="row">
					  
					  <div class="form-group">
							<label class="control-label">Name</label>
							<input type="text" name="name" class="form-control" value="'.$data['filter_opt']->name.'"/> 
					  </div>
					  
					  <input name="edit_option" type="submit" value="Update" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['view_option'])) {
    $fil = DB::select("SELECT * FROM filter WHERE id = ".$_GET['view_option'])[0];
    echo '<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
<h3>Filter Options For '.$fil->name.'<a href="filters?add_option='.$_GET['view_option'].'" class="add">Add Filter Options For '.$fil->name.'</a></h3>
<p>Manage Filters Options For '.$fil->name.'</p>


    <div class="col-lg-12">
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>';
    $sr = 1;
    echo $data['notices'];
    foreach ($data['filter_opts'] as $f){
        echo'<tr>
            <td>'.$sr.'</td>
			<td>'.$f->name.'</td>
			<td>
                <a href="filters?delete_option='.$f->id.'" title="Delete"><i class="icon-trash"></i></a>
				<a href="filters?edit_option='.$f->id.'" title="Edit"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          ';
        $sr++; }
    echo '</tbody>
    </table>
    </div>
    </div>
    </div>
     </div>
    </div>
    </div>';
} else {
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Filters<a href="filters?add" style="padding:3px 15px; font-size: 15px;" class="add">Add Filters</a></h5>
                <p>Manage Filters</p>
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['filters'] as $feature){
                                echo'<tr>
            <td>'.$sr.'</td>
			<td>'.$feature->name.'</td>
			<td>
                <a href="filters?delete='.$feature->id.'" title="Delete"><i class="icon-trash"></i></a>
				<a href="filters?edit='.$feature->id.'" title="Edit"><i class="icon-pencil"></i></a>
				<a href="filters?add_option='.$feature->id.'" title="Add Options"><i class="icon-plus"></i></a>
				<a href="filters?view_option='.$feature->id.'" title="View Options"><i class="icon-eye"></i></a>
            </td>
          </tr>
          ';
                                $sr++; }
                            }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $data['footer'];
?>
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            placeholder: "Please Select...",
            allowClear: true
        });
    });

    $(document).ready(function(){
        var maxField = 20; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '<div><input type="text" name="name[]" value="" class="form-control" style="float: left; width: 450px; border: 1px solid #000000;"/><a href="javascript:void(0);" class="remove_button" style="float: right"><i class="icon-close" style="font-size: 20px" </div>'; //New input field html
        var x = 1; //Initial field counter is 1

        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
</script>
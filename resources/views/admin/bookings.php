<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$tp; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/custom/custom.css">
    <link href="<?=$tp; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>



<?php echo $header;
if(isset($_GET['view'])){?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>View Bookings</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="bookings" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <div class="col s12 m6"><?=getProName($booking->product_id); ?></div>
                    <table id="showGroupData" class="table table-striped table-bordered" style="background-color: white;">
                        <?php foreach((array)json_decode($booking->variants) as $vr){?>
                            <tr style=" border: 1px solid #000000;">
                                <td>
                                    <div class="input-field col s12 m6">
                                        Variant Title
                                    </div>
                                    <label class="control-label">
                                        <?php if($booking->direct_booking == '1'){
                                            if($booking->without_variants == '1'){
                                                echo $vr->title;
                                            }else{
                                                echo getVariant($vr->id);
                                            }
                                        }else{
                                            echo getProName($vr->id);
                                        }
                                        ?></label>
                                </td>
                                <td>
                                    <div class="input-field col s12 m6">
                                        Quantity
                                    </div>
                                    <label class="control-label"><?=$vr->quantity.' - '.$vr->unit; ?></label>
                                </td>
                                <td>
                                    <div class="input-field col s12 m6">
                                        Price Detail (/<?=$vr->unit; ?>)
                                    </div>
                                    <label class="control-label">
                                        <?php
                                        if($booking->without_variants == 1 && $booking->direct_booking == 1){
                                            foreach($vr->price_detail as $pv){
                                                $basic = (double)(isset($pv->basic) ? $pv->basic : '0');

                                                echo $basic.' - Basic Amount ';
                                            }
                                        }elseif($booking->method == 'bybasic'){
                                            foreach($vr->price_detail as $pv){
                                                if($pv !== null){
                                                    $basic = (round((double)(isset($pv->basic)) ? $pv->basic : '0'+(double)$amount));
                                                    $sizeDifference = 0;
                                                    if(isset($pv->size_difference)){
                                                        $sizeDifference = $pv->size_difference;
                                                    }
                                                    $loading = (!empty($pv->loading)) ? $pv->loading : '0';
                                                    $pr = $basic+$sizeDifference+$loading;
                                                    $product = \App\Products::find($booking->product_id);
                                                    $t = ($product->tax == '' ? 18.00 : floatval(number_format($product->tax, 2, '.', '')));
                                                    $tax = ($pr * $t) / 100;
                                                    $total = $pr + $tax;
                                                    echo  (double)(isset($vr->price) ? $vr->price : '0').' - Basic Amount ';
                                                }
                                            }
                                        }else{
                                            $days = '';
                                            $amount = 0;
                                            if($booking->credit_charge_id !== null){
                                                $credit = DB::table('credit_charges')->where('id', $booking->credit_charge_id)->first();
                                                if($credit !== null){
                                                    $days = $credit->days;
                                                    $amount = $credit->amount;
                                                }
                                            }
                                            foreach($vr->price_detail as $pv){
                                                $crd = '';
                                                $cra = '';
                                                if($days !== ''){
                                                    $crd = $days.' - Credit Charge Days <br>';
                                                }
                                                if($days !== ''){
                                                    $cra = $amount.' - ('.$days.') Credit Charges <br>';
                                                }
                                                $basic = (double)(isset($pv->basic) ? $pv->basic : '0');
                                                $sizeDifference = (isset($pv->size_difference) ? $pv->size_difference : '0');
                                                $loading = (isset($pv->loading) ? $pv->loading : '0');

                                                $pr = $basic+(double)$amount+$sizeDifference+$loading;

                                                $product = \App\Products::find($booking->product_id);
                                                $t = ($product->tax == '' ? 18.00 : floatval(number_format($product->tax, 2, '.', '')));
                                                $tax = ($pr * $t) / 100;
                                                $total = $pr + $tax;

                                                echo $basic.' - Basic Amount <br>'.
                                                    $cra.
                                                    $sizeDifference.' - Size Diffrence <br>'.
                                                    $loading.' - Loading <br>'.
                                                    $tax.' - Tax <br>'.

                                                    $total.' - Total Amount';
                                            }
                                        }?>
                                    </label>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($_GET['edit'])){?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Update Status</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="bookings" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="bookings?updateStatus=<?php echo $_GET['edit']; ?>" method="post" style="margin-top: 90px; max-width: 100%;">
                        <?php echo csrf_field(); ?>
                        <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                            <input type="hidden" name="id" value="<?=$_GET['edit']; ?>">
                            <select name="status" class="form-control">
                                <option value="">Select Status</option>
                                <option value="0" <?php if($booking->status == 0) { echo "selected"; } ?>>Unapproved</option>
                                <option value="1" <?php if($booking->status == 1) { echo "selected"; } ?>>Approved</option>
                                <option value="2" <?php if($booking->status == 2) { echo "selected"; } ?>>Failed</option>
                                <option value="3" <?php if($booking->status == 3) { echo "selected"; } ?>>Completed</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <input name="updateStatus" type="submit" value="Update Status" style="padding: 3px 25px;" class="btn btn-primary" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php }else{
?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5 class="card-title" style="font-size: 20px;">Manage Bookings</h5>

                <div class="divider"></div>
                <div class="row" style="position:relative;">
                    <?php if($notices !== ''){ ?>
                        <?=$notices?>
                    <?php } ?>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>ID</th>
                                <th>User Details</th>
                                <th>Variants - Quantity - Total Price</th>
                                <th>Products</th>
                                <th>Payment Option</th>
                                <th>Status</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($bookings as $quotation) {
                                $bids = explode(',', $quotation->brand_id);
                                $proName = DB::table('products')->where('id', $quotation->product_id)->first();
                                $brand_names = array_pluck(DB::table('products')->whereIn('id', $bids)->get(), 'title');
                                $name = implode(', ', $brand_names);
                                $status = $quotation->status;
                                $cust_name ='';
                                $cust_email ='';
                                $customer = DB::table('customers')->where('id', $quotation->user_id)->first();
                                if(isset($customer->name)){
                                    $cust_name = $customer->name;
                                }
                                else{
                                    $cust_name = '';
                                }
                                if(isset($customer->email)){
                                    $cust_email = $customer->email;
                                }
                                else{
                                    $cust_email = '';
                                }
                                echo '<tr>
							<td>' . $i . '</td>
							<td>' . getCustomerData($quotation->user_id) . '</td>';
                                $variant = (array)json_decode($quotation->variants);
                                $html = '<ul id="myUL">';
                                foreach ($variant as  $key=>$var) {

                                    if($quotation->direct_booking == '1'){
                                        if($quotation->without_variants == '1'){

                                            $html .= '<li>* ' . getProName($var->title) . ' - '.$var->quantity;
                                        }else{
                                            $html .= '<li>* ' . getVariant($var->id) . ' - '.$var->quantity;
                                        }
                                    }else{
                                        $html .= '<li>* ' . getVariant($var->id) . ' - '.$var->quantity . ' - '.$var->unit;
                                    }
                                    if($quotation->without_variants == '1'){
                                        $html .=  ' - '.$var->price;
                                    }elseif($quotation->method == 'bybasic'){
                                        $html .=  ' - '.$var->price;
                                    }else{
                                        if(isset($var->price_detail)){
                                            $days = '';
                                            $amount = 0;
                                            if($quotation->credit_charge_id !== null){
                                                $credit = DB::table('credit_charges')->where('id', $quotation->credit_charge_id)->first();
                                                if($credit !== null){
                                                    $days = $credit->days;
                                                    $amount = $credit->amount;
                                                }
                                            }
                                            foreach($var->price_detail as $pv){

                                                if($pv !== null){
                                                    $basic = (round((double)(isset($pv->basic)) ? $pv->basic : '0'+(double)$amount));
                                                    $sizeDifference = 0;
                                                    if(isset($pv->size_difference)){
                                                        $sizeDifference = $pv->size_difference;
                                                    }
                                                    $loading = (!empty($pv->loading)) ? $pv->loading : '0';
                                                    $pr = $basic+$sizeDifference+$loading;
                                                    $product = \App\Products::find($quotation->product_id);
                                                    $t = ($product->tax == '' ? 18.00 : floatval(number_format($product->tax, 2, '.', '')));
                                                    $tax = ($pr * $t) / 100;
                                                    $total = $pr + $tax;
                                                    $html .=  ' - '.$total;
                                                }
                                            }
                                        }
                                    }
                                    $html .= '</li>';
                                }
                                echo '<td>'.$html.'</td>
							<td>'.$proName->title.'</td>
							<td>'.$quotation->payment_option.'</td>
							<td>'.($status == 0 ? '<span style="color: #111;" class="badge alert-warning">Unapproved</span>' : ($status == 2 ? '<span style="color: #111;" class="badge alert-danger">Failed</span>' : ($status == 3 ? '<span style="color: #111;" class="badge alert-danger">Completed</span>' : '<span style="color: #111;" class="badge alert-success">Approved</span>'))) . '</td>
							<td>' . date('d, M Y h:i:s a', strtotime($quotation->created_at)) . '</td>
							<td>';
                                echo '<a href="bookings?view='.$quotation->id.'" class="btn btn-xs btn-primary" id="quote">view</a>
								<input type="hidden" id="quotation" class="form-control" value="'.$quotation->id.'" name="id">';
                                echo '<a href="bookings?edit='.$quotation->id.'" class="btn btn-xs btn-danger">Status</a>';
                                echo '</td>
							</tr>';
                                $i++;}
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            }
            echo $footer;
            ?>
            <script src="<?=$tp; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
            <!-- BEGIN VENDOR JS-->
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
            <!-- BEGIN PAGE VENDOR JS-->
            <script src="<?=$tp; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
            <!-- END PAGE VENDOR JS-->
            <!-- BEGIN THEME  JS-->
            <script src="<?=$tp; ?>/admin/js/plugins.js" type="text/javascript"></script>
            <script src="<?=$tp; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
            <script src="<?=$tp; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
            <!-- END THEME  JS-->
            <!-- BEGIN PAGE LEVEL JS-->

            <!-- END PAGE LEVEL JS-->
            <script src="<?=$tp; ?>/admin/js/select2.min.js"></script>
            <script src="<?=$tp; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
            <!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->

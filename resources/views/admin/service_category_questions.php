<?php
echo $header;
if(isset($_GET['add'])) {
    echo $notices.'<form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="service_category_questions"><i class="icon-arrow-left"></i></a>Add new Question</h5>
				<fieldset>
					  <div class="form-group">
					        <div class="col-md-12">
                                <label class="control-label">Question</label>
                                <select name="question_id" class="form-control">
                                    <option value="" >Please Select Question</option>';
                                    foreach($questions as $question){
                                        echo '<option value="'.$question->id.'">'.$question->question.'</option>';
                                    }
                                echo '</select>
                            </div>
							<div class="col-md-12">
							    <label class="control-label">Service</label>
								<select name="service_id" class="form-control">
                                    <option value="" >Please Select Service</option>';
                                    foreach($services as $service){
                                        echo '<option value="'.$service->id.'">'.$service->name.'</option>';
                                    }
							    echo '</select>
							</div>
							<div class="col-md-12">
							    <label class="control-label">Priority</label>
								<input type="text" name="priority" class="form-control">
							</div>
							
					  </div>
					  
					  
					  <input name="add" type="submit" value="Submit" class="btn btn-primary" />
				</fieldset>
			</form>';
} elseif(isset($_GET['edit'])) {


    echo $notices.'<form action="" method="post" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="service_category_questions"><i class="icon-arrow-left"></i></a>Edit Question</h5>
				<fieldset>
					  
					  <div class="form-group" id="que-type">
							<label class="control-label">Question</label>
							<select name="question_id" class="form-control">
                                    <option value="" >Please Select Question</option>';
                                    foreach($questions as $question){
                                        $selected = '';
                                        if($ques->question_id == $question->id){
                                            $selected = 'selected';
                                        }
                                        echo '<option value="'.$question->id.'" '.$selected.'>'.$question->question.'</option>';
                                    }
                                echo '</select>
					  ';

					  echo '</div>
                        <div class="form-group" id="que-type">
							<label class="control-label">Service</label>
							<select name="service_id" class="form-control">
                                    <option value="" >Please Select Services Category</option>';
                                    foreach($services as $service){
                                        $selected = '';
                                        if($ques->service_id == $service->id){
                                            $selected = 'selected';
                                        }
                                        echo '<option value="'.$service->id.'" '.$selected.'>'.$service->name.'</option>';
                                    }
                                echo '</select>
					  ';

					  echo '</div>
					  <div class="form-group">
						<label class="control-label">Priority</label>
						<input name="priority" type="text" value="'.$ques->priority.'" class="form-control"  />
					  </div>
					  
					  <input name="edit" type="submit" value="Update" class="btn btn-primary" />
				</fieldset>
			</form>';
} else {
    ?>
    <div class="head">
        <h3>Questions<a href="service_category_questions?add" class="add">Add Questions</a></h3>
        <p>Manage your Questions</p>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Question</th>
                <th>Service Name</th>
                <th>Priority</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sr = 1;
            echo $notices;
            foreach ($que as $que){
                echo '<tr>
                <td>'.$sr.'</td>
                <td>'; ?> <?php echo getQuestion($que->question_id); ?><?php echo'</td>
                <td>'; ?><?php echo getServiceCategoryName($que->service_id); echo '</td>
                <td>'.$que->priority.'</td>
                <td><a href="service_category_questions?delete='.$que->id.'"><i class="icon-trash"></i></a>
                    <a href="service_category_questions?edit='.$que->id.'"><i class="icon-pencil"></i></a>
                </td>
              </tr>';
                $sr++;}
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php

echo $footer;
?>
<script>
function removeThis($id){
	event.preventDefault();
	$('[data='+$id+']').remove();
}
$('#add-option').on('click', function(){
	var id = $('#que-type > div').length;
	var html = '<div class="col-md-12 options" data="'+id+'"><label class="control-label col-md-12" data="'+id+'">Option '+(id+1)+'</label><input name="option[]" type="text" data="'+id+'" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" /><a onClick="removeThis('+id+');" class="pul-right" style="line-height: 3;" data="'+id+'"><i class="icon-close" style="font-size: 20px;"></i></a></div>';
	$('#add-option').before(html);
});
function getData(data){
	if(data == 'text' || data == 'textarea'){
		$('.options').hide();
	}else{
		$('.options').show();
	}	
}


</script>
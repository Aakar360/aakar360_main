<?php
echo $data['header'];
if(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Edit template</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="templates" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" class="form-horizontal single">
				'.csrf_field().'
				
					<div class="row">
						  <div class="form-group">
							<label class="control-label">template title</label>
							<input name="title" value="'.$data['template']->title.'" type="text"  class="form-control" required/>
						  </div>
						  <div class="form-group">
							<label class="control-label">template content</label>
							<textarea class="form-control" name="template" id="template" rows="10" cols="80" required>'.$data['template']->template.'</textarea>
						  </div>
						  <input name="edit" type="submit" value="Edit template" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 650px;">
                    <h3>Templates</h3>
                    <p>Manage your website email templates</p>

    <?php

    foreach ($data['templates'] as $template){
        echo'<div class="bloc">
			<h5>'.$template->title.'<b> ('.$template->code.') </b><div class="tools"><a href="templates?edit='.$template->id.'"><i class="icon-pencil"></i></a></div></h5>
			</div>';
    }
}
echo $data['footer'];
?>
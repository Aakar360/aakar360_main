<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'/>
		<title><?=$title?></title>
		<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<meta content='<?=$cfg->desc?>' name='description'/>
		<meta content='<?=$cfg->key?>' name='keywords'/>
		<!--ASSETS-->
		<base href="<?=url('admin')?>/" />
		<!--    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">-->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400" rel="stylesheet">
		<link rel="apple-touch-icon" href="<?=$tp; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/admin/images/favicon/favicon-32x32.png">
		<link rel="stylesheet" href="<?=$tp?>/admin/select2.css">
		<link rel="stylesheet" href="<?=$tp?>/admin/style.css">
		<link rel="stylesheet" href="<?=$tp?>/assets/dataTables.bootstrap4.css">
		<link rel="stylesheet" href="<?=$tp?>/assets/jquery.dataTables.min.css">
		<link rel="stylesheet" href="<?=$tp?>/assets/buttons.dataTables.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/vendors.min.css">

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/assets/bootstrap.min.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">

    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/custom/custom.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/bootstrap-datetimepicker.min.css">


    <link href="<?=$tp?>/admin/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?=$tp?>/assets/bootstrap-switch.min.css">
        <script src="<?=$tp?>/assets/jquery.min.js"></script>
        <script src="<?=$tp?>/assets/bootstrap-switch.min.js"></script>
		<script src="<?=$tp?>/assets/bootstrap.min.js"></script>
		<script src="<?=$tp?>/assets/Chart.min.js"></script>
        <script src="<?=$tp?>/assets/jquery.dataTables.js"></script>
        <script src="<?=$tp?>/assets/dataTables.bootstrap4.js"></script>
        <script src="<?=$tp?>/assets/todolist.js"></script>
        <script src="<?=$tp?>/assets/todolist.js"></script>


	</head>
	<style>
    a{ text-decoration: none;  }
    a:hover{text-decoration: none}
    p {  margin: 0 0 0px;  }

    .sidenav {
        top: 64px;


    }
    .active {
        display: contents;
    }

    .icon-eye:before {
        content: "\e087";
        margin-right: 5px;
    }
    .icon-pencil:before {
        content: "\e05f";
        margin-right: 5px;
    }
    .icon-trash:before {
        content: "\e054";
        margin-right: 5px;
    }

    .btn{
        margin-right: 5px;
    }
    .icon-plus:before {
        content: "\e095";
        margin-right: 5px;
    }


    input[type="radio"] {
        display:inline;
    }
    /* The container */
    .radio_container {
        display:  contents;
        position: relative;
        padding-left: 20px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: .8rem;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .radio_container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .radio_container:hover input ~ .checkmark {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .radio_container input:checked ~ .checkmark {
        background-color: #2196F3;
        height: 16px;
        width:16px;
        top:-4px;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
        background-color: #eee;
    }

    /* Show the indicator (dot/circle) when checked */
    .radio_container input:checked ~ .checkmark:after {
        display: block;
        background-color: #eee;
    }

    /* Style the indicator (dot/circle) */
    .radio_container .checkmark:after {
        top: 19px;
        left: 9px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        position: fixed;

        background-color: #eee;
    }

    [type='radio']:not(:checked) + span, [type='radio']:checked + span {

        padding-left: 0px;
        hieght:20px;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__rendered {
        box-sizing: border-box;
        list-style: none;
        margin: 0;
        padding: 0 5px;
        width: 100%;
        max-height: 300px;
        overflow-y: auto;
    }

</style>
<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 2-columns" data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">
			<!-- BEGIN: Header-->
<header class="page-topbar" id="header">
    <!-- <div class="navbar navbar-fixed">
                <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">
                    <divclass="nav-wrapper">
                        <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>
                            <input class="header-search-input z-depth-2" type="text" name="Search" placeholder=" Type here ..">
                 </div>
                <ul class="navbar-list right">

                    <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-lighttoggle -fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
                    <li class="hide-on-large-only"><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i class="material-icons">search</i></a></li>
                    <li><a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);" data-target="notifications-dropdown"><i class="material-icons">notifications_none<small class="notification-badge">5</small></i></a></li>
                    <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="<?=$tp; ?>/images/avatar/avatar-7.png" alt="avatar"><i></i></span></a></li>
                                <li><a class="waves-effect waves-block waves-light sidenav-trigger" href="#" data-target="slide-out-right"><i class="material-icons">format_indent_increase</i></a></li>
                </ul>
                <!-- translation-button-->

                <!-- notifications-dropdown-->
                <!--<ul class="dropdown-content" id="notifications-dropdown">
                <li>
                    <h6>NOTIFICATIONS<span class="new badge">5</span></h6>
                                </li>
                <li class="divider"></li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-iconsicon-bg-circle cyan small">add_shopping_cart</span>A new order has been placed!</a>
                                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
                </li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-iconsicon-bg-circle red small">stars</span>Completed the task</a>
                                <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
                </li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-iconsicon-bg-circle teal small">settings</span>Settings updated</a>
                            <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
                            </li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle deep-orange small">today</span> Director meeting started</a>
                    <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
                </li>
                <li><a class="grey-text text-darken-2" href="#!"><span class="material-icons icon-bg-circle amber small">trending_up</span> Generate monthly report</a>
                    <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
                </li>
            </ul>-->
                <!-- profile-dropdown-->
                <!--<ul class="dropdown-content" id="profile-dropdown">
                <li><a class="grey-text text-darken-1" href="user-profile-page.html"><i class="material-icons">person_outline</i> Profile</a></li>
                <li><a class="grey-text text-darken-1" href="app-chat.html"><i class="material-icons">chat_bubble_outline</i> Chat</a></li>
                <li><a class="grey-text text-darken-1" href="page-faq.html"><i class="material-icons">help_outline</i> Help</a></li>
                <li class="divider"></li>
                <li><a class="grey-text text-darken-1" href="user-lock-screen.html"><i class="material-icons">lock_outline</i> Lock</a></li>
                <li><a class="grey-text text-darken-1" href="user-login.html"><i class="material-icons">keyboard_tab</i> Logout</a></li>
            </ul>
            </div>
        </nav>
    </div>-->
</header>
<!-- END: Header-->

<!-- BEGIN: SideNav-->
<!--<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">-->
<aside class="sidenav-main nav-expanded nav-collapsed nav-collapsible sidenav-light sidenav-active-square" >
    <div class="brand-sidebar">
        <h1 class="logo-wrapper">
            <a class="brand-logo darken-1" href="<?=url('admin/index');?>"><img src="<?=$tp; ?>/admin/images/logo/aakar360.png" alt="aakar360 logo"/>
                <span class="logo-text hide-on-med-and-down"></span></a>
                        <!--<a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a>-->
        </h1>
                        </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
                            <li class="bold">
                                <a class="waves-effect waves-cyan " href="<?=url('admin/logout');?>">
                                    <i class="material-icons md-24">power_settings_new</i><span class="menu-title" data-i18n="">Logout</span>
            </a>
            <p class="navigation-header-sub-text">Logout</p>
        </li>
        <li class="active bold">
            <!--Dashboard-->
                                            <a class=" waves-effect waves-cyan "href="<?=url('admin');?>"><i class="material-icons md-24">home</i> <span class="menu-title" data-i18n="">Dashboard</a>
                                        <p class="navigation-header-sub-text">Dashboard</p>

                                </li>


                                        <li class="bold">
                                            <a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">account_balance_wallet</i>
                                                <span class="menu-title" data-i18n="">Seller </span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Seller</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="seller" <?=($area == "seller" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Seller</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="pendingproduct" <?=($area == "pendingproduct" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Pending Products</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="approvalproduct" <?=($area == "approvalproduct" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Approval Products</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="rejectproduct" <?=($area == "rejectroduct" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Reject Products</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="deleteproduct" <?=($area == "deleteproduct" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Deleted Products</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="sellers-payment" <?=($area == "sellers-payment" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Sellers Payment</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="seller_po" <?=($area == "seller_po" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Sellers Po</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="cr-note" <?=($area == "cr-note" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Cr Note</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="dr-note"  <?=($area == "dr-note" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Dr Note</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">group_add</i>
                                        <span class="menu-title" data-i18n="">
                                                Institutional</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Institutional</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="institutional" <?=($area == "institutional" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Users</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="bookings" <?=($area == "bookings" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Booking</span></a>
                                                    </li>
                                                <li>
													<a class="collapsible-body"href="dispatch-programs" <?=($area == "dispatch" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Dispatch Programs</span></a>
                                                    <a class="collapsible-body"href="request-quotation" <?=($area == "dispatch" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Request for quotation</span></a>
                                                    <a class="collapsible-body"href="request-for-call" <?=($area == "request-for-call" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Request for call</span></a>
													<a class="collapsible-body"href="quick-quote" <?=($area == "quick-quote" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Quick Quote</span></a>
													<a class="collapsible-body"href="mentor-program" <?=($area == "mentor-program" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Mentor Program</span></a>
													<a class="collapsible-body"href="credit-charges" <?=($area == "credit-charges" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Credit Charges</span></a>
													<a class="collapsible-body"href="counter-offer" <?=($area == "counter-offer" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Counter Offer</span></a>
												</li>

                                            </ul>
                                        </div>
                                    </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">storefront</i>
                                        <span class="menu-title" data-i18n="">Fronted</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Fronted</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="banners" <?=($area == "banners" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Manage Banners</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="home-page-design" <?=($area == "home-page-design" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Manage Home Page Design</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="mega-menu-image" <?=($area == "mega-menu-image" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Manage Mega Menu </span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="default_image" <?=($area == "default_image" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span> Default Image</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="link" <?=($area == "llink" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Manage link</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="pages" <?=($area == "pages" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Pages</span></a>
                                                    </li>


                                        </ul>
                                    </div>
                                </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">archive</i>
                                        <span class="menu-title" data-i18n="">
                                                Products</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Products</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="manufacturing-hubs" <?=($area == "mhubs" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i> <span>Manufacturing Hubs</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="products" <?=($area == "products" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i> <span>Products</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="product-type" <?=($area == "product-type" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i> <span>Product Grade</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="units" <?=($area == "units" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Weight Units</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="categories" <?=($area == "categories" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Categories</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="size" <?=($area == "size" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Variant Sizes</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="sizes_group" <?=($area == "sizes_group" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Variant Sizes Group</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="filters" <?=($area == "filters" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Filters</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="best-for-cat" <?=($area == "best-for-cat" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Best For Categories</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="brands" <?=($area == "brands" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Brands</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="featured" <?=($area == "featured" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Suggested Products</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body" href="customers" <?=($area == "customers" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Customers</span></a>
                                                    </li>
                                                <li>
                                                    <a class="collapsible-body" href="dealers" <?=($area == "dealers" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Dealers</span></a>
                                                </li>
                                                <li>
                                                        <a class="collapsible-body" href="coupons" <?=($area == "coupons" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Coupons</span></a>
                                                    </li>
                                                <li>
                                                        <?php $rew = DB::select("SELECT COUNT(*) as count FROM reviews WHERE active = 0")[0]->count;?>
                                                        <a class="collapsible-body" href="reviews" <?=($area == "reviews" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Reviews </span> </a>
                                                    </li>
                                                <li>
                                                        <?php $ppenfaq = DB::select("SELECT COUNT(*) as count FROM product_faq WHERE status = 'Pending'")[0]->count;?>
                                                        <a class="collapsible-body" href="product_faq" <?=($area == "product_faq" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Product FAQ</span> </a>
                                                    </li>
                                                </ul>
                                        </div>
                                    </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">comment</i>
                                        <span class="menu-title" data-i18n="">
                                                Advices</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Advices</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="blog" <?=($area == "blog" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Advices</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="best-for-advices" <?=($area == "best-for-advices" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Best For Advices</span></a>
                                                    </li>
                                                <li>
                                                        <?php $apenfaq = DB::select("SELECT COUNT(*) as count FROM advices_faq WHERE status = 'Pending'")[0]->count;?>
                                                        <a class="collapsible-body"href="advices_faq" <?=($area == "advices_faq" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Advices FAQ</span> <span class="label label-info"><?=$apenfaq; ?></span></a>
                                                    </li>
                                                <li>
                                                        <?php $arew = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 0")[0]->count;?>
                                                        <a class="collapsible-body"href="advices_reviews" <?=($area == "advices_reviews" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Advices Reviews</span> <span class="label label-info"><?=$arew; ?></span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="advices_category" <?=($area == "advices_category" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Advices Categories</span></a>
                                                    </li>


                                        </ul>
                                    </div>
                                </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">build</i>
                                        <span class="menu-title" data-i18n="">
                                                Services</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Services</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="services_categories" <?=($area == "services_categories" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Categories</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="services" <?=($area == "services" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Services</span></a>
                                                    </li>
                                                <li>
                                                        <?php $serreq = DB::select("SELECT COUNT(*) as count FROM services_answer WHERE status = 'Pending'")[0]->count;?>
                                                        <a class="collapsible-body"href="services_answer" <?=($area == "services_answer" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Services Request(s)</span> <span class="label label-info"><?=$serreq; ?></span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="s-how-it-works" <?=($area == "s-how-it-works" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Questions Master</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="advices_category" <?=($area == "advices_category" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>How It Works</span></a>
                                                    </li>
                                                <li>
                                                        <?php $penfaq = DB::select("SELECT COUNT(*) as count FROM faq WHERE status = 'Pending'")[0]->count;?>
                                                        <a class="collapsible-body"href="faq" <?=($area == "faq" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Services FAQ</span> <span class="label label-info"><?=$penfaq; ?></span></a>
                                                    </li>

                                            </ul>
                                        </div>
                                    </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">view_quilt</i>
                                        <span class="menu-title" data-i18n="">
                                                Design</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Design</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="design_category" <?=($area == "design_category" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Categories</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="design-cat-banner" <?=($area == "design-cat-banner" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Design Category Banner</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="best-for-layout" <?=($area == "best-for-layout" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Best For Layout</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="layout-plans" <?=($area == "layout_plans" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Layout Plans</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="layout_addon" <?=($area == "layout_addon" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Layout Addon</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="photos" <?=($area == "photos" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Photos</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="best-for-photo" <?=($area == "best-for-photo" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Best For Photo</span></a>
                                                    </li>
                                                <li>
                                                        <?php $prew = DB::select("SELECT COUNT(*) as count FROM photo_reviews WHERE active = 0")[0]->count;?>
                                                        <a class="collapsible-body"href="photo-reviews" <?=($area == "photo-reviews" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Photo Reviews</span> <span class="label label-info"><?=$prew; ?></span></a>
                                                    </li>
                                                <li>
                                                        <?php $lrew = DB::select("SELECT COUNT(*) as count FROM layout_reviews WHERE active = 0")[0]->count;?>
                                                        <a class="collapsible-body"href="layout_reviews" <?=($area == "layout_reviews" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Layout Reviews</span> <span class="label label-info"><?=$lrew; ?></span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="amminities" <?=($area == "amminities" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Amminities</span></a>
                                                    </li>
                                                <li>
                                                        <?php $penlfaq = DB::select("SELECT COUNT(*) as count FROM layout_faq WHERE status = 'Pending'")[0]->count;?>
                                                        <a class="collapsible-body"href="layout_faq" <?=($area == "layout_faq" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Layout Faq's</span> <span class="label label-info"><?=$penlfaq; ?></span></a>
                                                    </li>
                                                <li>
                                                        <?php $modify = DB::select("SELECT COUNT(*) as count FROM modify_plan WHERE status = 'Pending'")[0]->count;?>
                                                        <a class="collapsible-body"href="modify_plan"  <?=($area == "modify_plan" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Modify Plan Request(s)</span> <span class="label label-info"><?=$serreq; ?></span></a></li>
                                                    <li>

                                                        <a class="collapsible-body"href="image_likes" <?=($area == "image_likes" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Photo Likes</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="image_inspirations" <?=($area == "image_inspirations" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Photo Inspirations</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="plan_orders" <?=($area == "plan_orders" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Plan Orders</span></span></a>
                                                    </li>

                                            </ul>
                                        </div>
                                    </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">shop</i>
                                        <span class="menu-title" data-i18n="">
                                                Retail</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Sales</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion"><tr>
                                                    <td>
                                                        <?php $ppenfaq = \App\SpecialRequirements::where('status', 0)->count();?>
                                                        <a href="product_sr" <?=($area == "product_sr" ? "class=active" : "")?>><span>Special Requests</span> <span class="label label-info"><?=$ppenfaq; ?></span></a>
                                                    </td>
                                                </tr>
                                                <li>
                                                        <a class="collapsible-body"href="orders" <?=($area == "orders" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Orders</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="stats" <?=($area == "stats" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Statistics</span></a>
                                                    </li>


                                        </ul>
                                    </div>
                                </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">business</i>
                                        <span class="menu-title" data-i18n="">
                                                Marketing</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Marketing</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="tracking" <?=($area == "tracking" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Tracking</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="newsletter" <?=($area == "newsletter" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Newsletter</span></a>
                                                    </li>


                                        </ul>
                                    </div>
                                </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">accessibility_new</i>
                                        <span class="menu-title" data-i18n="">
                                                Visitors</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Visitors</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="referrers" <?=($area == "referrers" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Refferes</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="os" <?=($area == "os" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Oprarting Systems</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="browsers" <?=($area == "browsers" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Browsers</span></a>
                                                    </li>


                                        </ul>
                                    </div>
                                </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">note</i>
                                        <span class="menu-title" data-i18n="">
                                                Settings</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Settings</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body" href="postcodes" <?=($area == "postcodes" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Postcodes</span></a>
                                                    </li>
                                                <li>
                                                    <a class="collapsible-body" href="cities" <?=($area == "cities" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Cities</span></a>
                                                </li>
                                                <li>
                                                        <a class="collapsible-body"href="shipping" <?=($area == "shipping" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Shipping cost</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="flat" <?=($area == "flat" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Shipping Flat Rate</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="payment" <?=($area == "payment" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Payment</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="currency" <?=($area == "currency" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Currency</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="units" <?=($area == "units" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Weight Units</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="settings" <?=($area == "settings" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Settings</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="theme" <?=($area == "theme" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Theme settings</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="lang" <?=($area == "lang" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Language</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="tokens" <?=($area == "tokens" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>API Tokens</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="export" <?=($area == "export" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Export</span></a>
                                                    </li>
                                                </ul>
                                        </div>
                                    </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">flag</i>
                                        <span class="menu-title" data-i18n="">
                                                Customization</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Customization</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="registerimage" <?=($area == "registerimage" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Register Image</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="slider" <?=($area == "slider" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Slider</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="editor" <?=($area == "editor" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Theme editor</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="templates" <?=($area == "templates" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Templates</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="builder" <?=($area == "builder" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Page builder</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="menu" <?=($area == "menu" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Main menu</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="bottom" <?=($area == "bottom" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Footer menu</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="registerimage" <?=($area == "registerimage" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Extrafields</span></a>
                                                    </li>
                                                </ul>
                                        </div>
                                    </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">stay_primary_portrait</i>
                                        <span class="menu-title" data-i18n="">
                                                Calculator</span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Calculator</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="tmtcalculator" <?=($area == "tmt" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>TMT Bars</span></a>
                                                    </li>


                                            </ul>
                                        </div>
                                    </li>
                                <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">person</i>
                                        <span class="menu-title" data-i18n="">
                                                Administration </span><i class="icon-plus" style="float: right;"></i>
                                            </a>
                                        <p class="navigation-header-sub-text">Administration</p>
                                    <div id="collapsible-body" class="collapsible-body">
                                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                                <li>
                                                        <a class="collapsible-body"href="support" <?=($area == "support" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Support</span></a>
                                                    </li>
                                                <li>
                                                        <a class="collapsible-body"href="administrators" <?=($area == "administrators" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Administrators</span></a>
                                                    </li>
                                                <li>

                                                        <a class="collapsible-body"href="profile" <?=($area == "profile" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Profile</span></a>
                                                    </li>


                                            </ul>
                                        </div>
                                    </li>
        <li class="bold"><a class="collapsible-header waves-effect waves-cyan" href="javascript:void(0);"><i class="material-icons md-24">person</i>
                <span class="menu-title" data-i18n="">
                                                Notification </span><i class="icon-plus" style="float: right;"></i>
            </a>
            <p class="navigation-header-sub-text">Notification</p>
            <div id="collapsible-body" class="collapsible-body">
                <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                    <li>
                        <a class="collapsible-body"href="notification" <?=($area == "notification" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Institutional Notification</span></a>
                    </li>

                    <li>
                        <a class="collapsible-body"href="retail_notification" <?=($area == "notification" ? "class=active" : "")?>data-i18n=""><i class="material-icons md-18">done</i><span>Retail Notification</span></a>
                    </li>

                </ul>
            </div>
        </li>


                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            <div class="navigation-background">
                        </div>
                    <a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
			</aside>
		<!-- END: SideNav-->
			<div id="main"class="main-full">
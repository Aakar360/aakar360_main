<?php echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 650px;">
    <h5>Edit Language</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="lang" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" class="form-horizontal single"  enctype="multipart/form-data">
			'.csrf_field().'
			<h5><a href="banners"><i class="icon-arrow-left"></i></a>Add new Banner</h5>
				<div class="row">
					  <div class="form-group">
						<label class="control-label">Title</label>
						<input name="title" type="text"  class="form-control"/>
					  </div>
					  <div class="form-group">
						<label class="control-label">Short Description</label>
						<input name="short_description" type="text" class="form-control"/>
					  </div>
					  <div class="form-group">
						<label class="control-label">Banner image</label>
						<input name="image" type="file" class="form-control" required="required" />
					  </div>
					  <div class="form-group">
						<label class="control-label">Priority</label>
						<input name="priority" type="text" class="form-control"  required="required"/>
					  </div>
					  
					  <input name="add" type="submit" value="Add banner" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Change Image</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="registerimage" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" style="max-width: 100%;" enctype="multipart/form-data">
			'.csrf_field().'
			
				<div class="row">
					 
					  <div class="form-group col-md-6">
						<label class="control-label">User Type</label>
						<input type="text" class="form-control" name="user_type" value="'.$data['register']->user_type.'" readonly/>
					  </div>
					  <div class="form-group col-md-6">
						<label class="control-label">Image</label>
						<input type="file" class="form-control" name="image"/>
					  </div>
					  </div>
                        <input name="edit" type="submit" value="Edit Image" style="padding:3px 25px;" class="btn btn-primary" />
				
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 650px;">
    <h5>Register Images</h5>
    <h6>Manage your Register Image</h6>


    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>User Type</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sr = 1;
            echo $data['notices'];
            foreach ($data['register'] as $reg) {
                $img = $reg->image;
                echo '<tr>
            <td>' . $sr . '</td>
            <td>' . $reg->user_type . '</td>';
                if($img!=''){
                    echo '<td><img src="../assets/register/'.$reg->image.'" style="height: auto; width: 100px;"></td>';
                }
                else{
                    echo '<td>No Image</td>';
                }

                echo '<td>               
                <a href="registerimage?edit=' . $reg->id . '"><i class="icon-pencil"></i></a>
                <a href="registerimage?delete=' . $reg->id . '"><i class="icon-trash"></i></a>';
                echo '</td>
          </tr>';
                $sr++;
            } ?>
            </tbody>
        </table>
    </div>
    <?php
}
echo $data['footer'];
?>
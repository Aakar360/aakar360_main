<?php
echo $data['header'];
if (isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 650px;">
    <h5>Add new Language</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="lang" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" class="form-horizontal single">
				'.csrf_field().'
				
					<div class="row">
						  <div class="form-group">
							<label class="control-label">Language</label>
							<input name="name" type="text"  class="form-control" required/>
						  </div>
						  <div class="form-group">
							<label class="control-label">Language code</label>
							<input name="code" type="text" class="form-control" required />
						  </div>
						  <input name="add" type="submit" value="Add Language" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 650px;">
    <h5>Edit Language</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="lang" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" class="form-horizontal single">
				'.csrf_field().'
				
					<div class="row">
						  <div class="form-group">
							<label class="control-label">Language</label>
							<input name="name" type="text"  value="'.$data['lang']->name.'" class="form-control" required/>
						  </div>
						  <div class="form-group">
							<label class="control-label">Language code</label>
							<input name="code" type="text" value="'.$data['lang']->code.'" class="form-control"  required/>
						  </div>
						  <input name="edit" type="submit" value="Edit Language" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
    ?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h3>Languages<a href="lang?add" style="padding:3px 15px;" class="add">Add language</a></h3>
    <p>Manage your site languages and translations .</p>

    <h4>Available languages :</h4>
    <?php
    echo $data['notices'];
    foreach ($data['langs'] as $lang){
        echo '<div class="bloc">
				<h5>
					<a href="lang?lang='.$lang->code.'">'.$lang->name.'</a>
					<div class="tools">
						<a href="lang?delete_language='.$lang->id.'"><i class="icon-trash"></i></a>
						<a href="lang?edit='.$lang->id.'"><i class="icon-pencil"></i></a>
					</div>
				</h5>
			</div>';
    }
    echo '<h4>Translations ('.$data['l'].') :</h4>';
    foreach ($data['translations'] as $translation){
        echo'<div class="bloc" style="padding: 0px;">
				<div class="col-md-6" style="padding: 22px 25px;"><b>'.htmlspecialchars($translation->word).'</b><div class="tools"><a href="lang?lang='.$l.'&delete='.$translation->id.'"><i class="icon-trash"></i></a></div></div>
				<div class="col-md-6 tt t"><input value="'.htmlspecialchars($translation->translation).'"/><i id="'.$translation->id.'" class="icon-check translate"></i></div>
				<div class="clearfix"></div>
			</div>';
    }
}
?>
    </div>
    <script>
        $('.translate').click(function(){
            var data= $(this).attr('id');
            var translation = $(this).closest("div").find("input").val();
            $.post('lang?save='+data,{translation: translation,_token: '<?=csrf_token()?>'},function(d){
                alert('Saved');
            });
        });
    </script>
<?php
echo $data['footer'];
?>
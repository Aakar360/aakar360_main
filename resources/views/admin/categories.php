<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/custom/custom.css">
    <link href="<?=$data['tp']; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>
<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <h5>Add Category</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="categories" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Category name
						<input name="name" type="text"  class="form-control" required="required" />
					  </div>
					  <div class="input-field col s12 m6">
						Category path
						<input name="path" type="text" class="form-control"  required="required"/>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Category image
						<input name="image" type="file" class="form-control" required="required" />
					  </div>
					  <div class="input-field col s12 m6">
						Category Banner
						<input name="banner" type="file" class="form-control"  />
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
							Parent category
							<select name="parent" class="form-control">
							<option value="0"></option>';
    foreach ($data['parents'] as $parent){
        echo '<option value="'.$parent->id.'">'.$parent->name.'</option>';
        $childs = DB::select("SELECT * FROM category WHERE parent = ".$parent->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
					  </div>
					  <div class="input-field col s12 m6">
							Top Brands
							<select name="brands[]" multiple class="form-control select2">';
    foreach ($data['brands'] as $brand){
        echo '<option value="'.$brand->id.'">'.$brand->name.'</option>';
    }
    echo '</select>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							Filter
							<select name="filter[]" multiple class="form-control select2">';
    foreach ($data['filters'] as $filter){
        echo '<option value="'.$filter->id.'">'.$filter->name.'</option>';
    }
    echo '</select>
						  </div>
						  <div class="input-field col s12 m6">
							Weight Unit
							<select name="weight_unit[]" multiple class="form-control select2">';
    foreach ($data['units'] as $unit){
        echo '<option value="'.$unit->id.'">'.$unit->symbol.'</option>';
    }
    echo '</select>
						  </div>
					<div class="input-field col s12 m6">
						Brand Banner
						<input name="brand_banner" type="file" class="form-control"  />
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
							Popular<br>
							<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="popular" type="radio" id="yes" value="1" /><span class="checkmark"></span></label> <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="popular" type="radio" id="no" value="0" /><span class="checkmark"></span></label>
					  </div>
					  <div class="input-field col s12 m6">
							Popular Priority<br>
							<input name="popular_priority" type="number" value="0"/>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Popular in Brands<br>
						<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="popular_in_brands" type="radio" id="yesp" value="1" /><span class="checkmark"></span></label> &nbsp; <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="popular_in_brands" type="radio" id="nop" value="0" /><span class="checkmark"></span></label>
					  </div>
					  <div class="input-field col s12 m6">
							Popular in Brands Priority<br>
							<input name="popular_brands_priority" type="number" value="0"/>
					  </div>
					  </div>
					  <div class="row">
                        <div class="input-field col s12 m6">
							Consumption Ratio (TON)<br>
							<input name="consumption_ratio" type="text"/>
						  </div>
						  </div>
                      <div class="row">
						  <div class="row">
                              <div class="input-field col s12 m6">
                                Show Category in business<br>
                                <label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="is_view" type="radio" id="yesp" value="1" /><span class="checkmark"></span></label> &nbsp; <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="is_view" type="radio" id="nop" value="0" /><span class="checkmark"></span></label>
                              </div>
					  </div>
					  </div>
					  <input name="add" type="submit" value="Add category" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Category</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="categories" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width:  100%">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Category name
						<input name="name" type="text"  value="'.$data['category']->name.'" class="form-control" required="required"/>
					  </div>
					  <div class="input-field col s12 m6">
						Category path
						<input name="path" type="text" value="'.$data['category']->path.'" class="form-control"  required="required"/>
					  </div>
					  </div><div class="row"><div class="input-field col s12 m6">';

    if (!empty($data['category']->image)){

        echo '
					       <p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/products/'.$data['category']->image).'" required="required"/>';

        echo '<div class="clearfix"></div></div>';
    }

    if (!empty($data['category']->download)){
        echo '
					       <p>Uploading new file will overwrite current file .</p></div>';
    }
    echo '
                        
					  <div class="input-field col s12 m6">
						Image
						<input type="file" class="form-control" name="image"/>
					  </div></div><div class="row"><div class="input-field col s12 m6">';
    if (!empty($data['category']->banner)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/products/'.$data['category']->banner).'" />';

        echo '<div class="clearfix"></div></div>';
    }
    echo '
					  <div class="input-field col s12 m6">
						Category Banner
						<input name="banner" type="file" class="form-control"  />
					  </div>
					  </div>
					   <div class="row">
					  <div class="input-field col s12 m6">
							Parent category
							<select name="parent" class="form-control">
							<option value="0"></option>';
    foreach ($data['parents'] as $parent){
        echo '<option value="'.$parent->id.'" '.($parent->id == $data['category']->parent ? 'selected' : '').'>'.$parent->name.'</option>';
        $childs = DB::select("SELECT * FROM category WHERE parent = ".$parent->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'" '.($child->id == $data['category']->parent ? 'selected' : '').'>- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'" '.($subchild->id == $data['category']->parent ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
					  </div>
					  <div class="input-field col s12 m6">
							Filter
							<select name="filter[]" multiple class="form-control select2">';
    foreach ($data['filters'] as $flat){
        $cbrands = array();
        if($data['category']->filter){
            $cbrands = explode(',', $data['category']->brands);
        }
        echo '<option value="'.$flat->id.'" '.(in_array($flat->id, $cbrands) ? 'selected' : '').'>'.$flat->name.'</option>';

    }
    echo '</select>
						  </div>
						  <div class="input-field col s12 m6">
							Weight Unit
							<select name="weight_unit[]" multiple class="form-control select2">';
    foreach ($data['units'] as $unit){
        $cunits = array();
        if($data['category']->weight_unit){
            $cunits = explode(',', $data['category']->weight_unit);
        }
        echo '<option value="'.$unit->id.'" '.(in_array($unit->id, $cunits) ? 'selected' : '').'>'.$unit->symbol.'</option>';

    }
    echo '</select>
						  </div>
						  </div>
					<div class="row">
					  <div class="input-field col s12 m6">
							Top Brands
							<select name="brands[]" multiple class="form-control select2">';
    foreach ($data['brands'] as $brand){
        $cbrands = array();
        if($data['category']->brands){
            $cbrands = explode(',', $data['category']->brands);
        }
        echo '<option value="'.$brand->id.'" '.(in_array($brand->id, $cbrands) ? 'selected' : '').'>'.$brand->name.'</option>';

    }
    echo '</select>
						  </div><div class="input-field col s12 m6">
							Consumption Ratio (TON)<br>
							<input name="consumption_ratio" type="text" value="'.$data['category']->consumption_ratio.'"/>
						  </div></div><div class="row"><div class="input-field col s12 m6">';
    if (!empty($data['category']->brand_banner)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/products/'.$data['category']->brand_banner).'" />';

        echo '<div class="clearfix"></div></div>';
    }
    echo '
					  <div class="input-field col s12 m6">
						Brand Banner
						<input name="brand_banner" type="file" class="form-control"  />
					  </div>
					  </div>
					  <div class="row">
						<div class="input-field col s12 m6">  
						Popular<br>';
    $val = $data['category']->popular;
    if($val == '1'){
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="popular" type="radio" id="yes" checked="checked" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="popular" type="radio" id="no" value="0" /><span class="checkmark"></span></label></div>';
    }else {
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="popular" type="radio" id="yes" value="1" /><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container"><input name="popular" type="radio" checked="checked"  id="no" value="0" />No<span class="checkmark"></span></label></div>';
    }
    echo '
						  <div class="input-field col s12 m6">
							Popular Priority<br>
							<input name="popular_priority" type="number" value="'.$data['category']->popular_priority.'"/>
						  </div>
						  </div>
						  <div class="row" style="margin-bottom: 10px;">
						  <div class="input-field col s12 m6">
						  Popular in Brands<br>';
    $val = $data['category']->popular_in_brands;
    if($val == '1'){
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="popular_in_brands" type="radio" id="yesp" checked="checked" value="1" /><span class="checkmark"></span></label> <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="popular_in_brands" type="radio" id="nop" value="0" /><span class="checkmark"></span></label></div>';
    }else {
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="popular_in_brands" type="radio" id="yesp" value="1" /><span class="checkmark"></span></label> <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="popular_in_brands" type="radio" checked="checked"  id="nop" value="0" /><span class="checkmark"></span></label></div>';
    }
    echo '
						  <div class="input-field col s12 m6">
							Popular in Brands Priority<br>
							<input name="popular_brands_priority" type="number" value="'.$data['category']->popular_brands_priority.'"/>
						  </div>
						  </div>
						  
						  <div class="row" style="margin-bottom: 10px;">
						  <div class="input-field col s12 m6">
						  Show Category in business<br>';
    $val = $data['category']->popular_in_brands;
    if($val == '0'){
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="is_view" type="radio" id="yesp" checked="checked" value="0" /><span class="checkmark"></span></label> <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="is_view" type="radio" id="nop" value="1" /><span class="checkmark"></span></label></div>';
    }else {
        echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input name="is_view" type="radio" id="yesp" value="0" /><span class="checkmark"></span></label> <label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input name="is_view" type="radio" checked="checked"  id="nop" value="1" /><span class="checkmark"></span></label></div>';
    }
    echo '
						  
						  </div>
						  
					  <input name="edit" type="submit" value="Edit category" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Category<a href="categories?add" style="padding:3px 15px; font-size: 15px" class="add">Add Categories</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your Categories </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Category Image</th>
                            <th>Category Name</th>
                            <th>Category Banner</th>
                            <th>Parent Category Name</th>
                            <th>Popular</th>
                            <th>Priority</th>
                            <th>Popular in Brands</th>
                            <th>Priority</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['categories'] as $category){
                            echo'<tr>
            <td>'.$sr.'</td>
			<td><img src="../assets/products/'.image_order($category->image).'" style="height: auto; width: 100px;"></td>
            <td><a href="../'.$category->path.'">'.$category->name.'</a></td>
			<td><img src="../assets/products/'.image_order($category->banner).'" style="height: auto; width: 150px;"></td>
			';
                            if($category->parent != 0){
                                $parent = DB::select("SELECT * FROM category WHERE id = ".$category->parent)[0];
                                echo'<td>'.$parent->name.'</td>';
                            } else {
                                echo'<td>No Parent</td>';
                            }
                            if($category->popular == '0'){
                                $pop = 'No';
                            }else{
                                $pop = 'Yes';
                            }
                            echo'<td>'.$pop.'</a></td>';
                            echo'<td>'.$category->popular_priority.'</a></td>';
                            if($category->popular_in_brands == '0'){
                                $pop = 'No';
                            }else{
                                $pop = 'Yes';
                            }
                            echo '<td>'.$pop.'</a></td>';
                            echo '<td>'.$category->popular_brands_priority.'</a></td>
			<td>
                <a href="categories?delete='.$category->id.'"><i class="icon-trash"></i></a>
				<a href="categories?edit='.$category->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>
          ';
                            $sr++; }
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
echo $data['footer'];
?>
<script src="<?=$data['tp']; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$data['tp']; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$data['tp']; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$data['tp']; ?>/admin/js/select2.min.js"></script>
<script src="<?=$data['tp']; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$data['tp']; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
<script>
    $( document ).ready(function() {
        $('.select2').select2({
            placeholder: "Select top brands",
            allowClear: true
        });
    });
</script>
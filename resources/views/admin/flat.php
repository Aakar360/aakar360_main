<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <form action="" method="post" class="form-horizontal single">
				'.csrf_field().'
				<h5><a href="flat"><i class="icon-arrow-left"></i></a>Add new cost</h5>
					<div class="row">
						<div class="form-group">
							<label class="control-label">Shipping Name</label>
							<input name="name" type="text" class="form-control" required />
						</div>
						<div class="form-group">
							<label class="control-label">Postcodes (Comma separated values for eg. 110001,110002,110003)</label>
							<textarea name="postcodes" rows="5"></textarea>
						</div>
						  <div class="form-group">
							<label class="control-label">Shipping cost</label>
							<input name="cost" type="text" class="form-control" required />
						  </div>
						  <div class="form-group">
							<label class="control-label">Country</label>
							<select id="country" name="country" class="form-control">';
    foreach ($data['countries'] as $country){
        echo '<option value="'.$country->iso.'" data-phone="'.$country->phonecode.'">'.$country->nicename.'</option>';
    }
    echo '</select>
						  </div>
						  <input name="add" type="submit" value="Add cost" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <form action="" method="post" style="max-width: 100%">
				'.csrf_field().'
				<h5><a href="flat"><i class="icon-arrow-left"></i></a>Edit cost</h5>
					<div class="row">
						<div class="form-group col-md-6">
							<label class="control-label">Shipping Name</label>
							<input name="name" type="text" class="form-control" required value="'.$data['cost']->name.'"/>
						</div>
						<div class="form-group col-md-6">
							<label class="control-label">Postcodes (Comma separated values for eg. 110001,110002,110003)</label>
							<textarea name="postcodes" rows="5">'.$data['cost']->postcodes.'</textarea>
						</div>
						  <div class="form-group col-md-6">
							<label class="control-label">Shipping cost</label>
							<input name="cost" value="'.$data['cost']->cost.'" type="text" class="form-control" required />
						  </div>
						  <div class="form-group col-md-6">
							<label class="control-label">Country</label>
							<select id="country" name="country" class="form-control">';
    foreach ($data['countries'] as $country){
        echo '<option value="'.$country->iso.'" '.($country->iso == $cost->country ? 'selected' : '').'>'.$country->nicename.'</option>';
    }
    echo '</select>
						  </div>
						  </div>
						  <input name="edit" type="submit" value="Edit cost" style="padding:3px 25px;" class="btn btn-primary" />
					
				</form>
				</div>
				</div>
				</div>
				</div>';
} else {
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
                <h3>Shipping cost<a href="shipping?add" style="padding:3px 15px;" class="add">Add cost</a></h3>
                <p>Set your shipping costs for specific countries</p>

                <?php
                echo $data['notices'];
                foreach ($data['costs'] as $cost){
                    echo '<div class="mini bloc">
			<h5>'.$cost->name.' ('.c($cost->cost).')
				<div class="tools">
					<a href="flat?edit='.$cost->id.'"><i class="icon-pencil"></i></a>
				</div>
			</h5>
			</div>
			';
                }
                }
                echo $data['footer'];
                ?>
                <script>
                    $(document).ready(function(){
                        $('.select2').select2();
                    });
                </script>
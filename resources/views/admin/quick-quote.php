<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$tp; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/custom/custom.css">
    <link href="<?=$tp; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet"
          href="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>-->
<!--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->



    <!-- END: Custom CSS-->
</head>

<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>
<!-- END: Head-->

<?php echo $header; ?>
<div class="row" style="margin-top: 10px;">
    <div class="col s12">
        <div class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5 class="card-title" style="font-size: 20px;">Manage Quick Quote</h5>

                <div class="divider"></div>
                <div class="row" style="position:relative;">
                </div>
                <?php if ($notices !== '') { ?>
                    <?= $notices ?>
                <?php }

                ?>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Quote</th>
                            <th>File</th>
                            <th>Status</th>
                            <th>Created Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($quotations as $key=>$quotation) {
							$status = $quotation->status;
                            echo '<tr>
                                <td>'; echo $key+1; echo '</td>
                                <td>'.$quotation->name.'</td>
                                <td>'.$quotation->email.'</td>
                                <td>'.$quotation->contact.'</td>
                                <td>'.$quotation->quote.'</td>
                                <td><a href="'.url('/assets/crm/images/user/'.$quotation->image).'" download>Download</a></td>
                                <td>'.($quotation->status == 0 ? '<span style="color:yellow;">Pending</span>' : '<span class="alert-success">Approved</span>').'</td>
                                <td>' . date('d, M Y h:i:s a', strtotime($quotation->time)) . '</td>
								<td>';
                                if ($status == 1) {
									echo '<a href="quick-quote?id='.$quotation->id.'&status=0" class="btn btn-xs btn-primary" id="quote">Approved</a><input type="hidden" id="quotation" class="form-control" value="'.$quotation->id.'">';
								}  else {
									echo '<a href="quick-quote?id='.$quotation->id.'&status=1" class="btn btn-xs btn-primary" id="quote">Approve</a><input type="hidden" id="quotation" class="form-control" value="'.$quotation->id.'">';
								}
								echo '</td>
                            </tr>';
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
        <?php
echo $footer;
?>

<script src="<?=$tp; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/admin/js/select2.min.js"></script>
<script src="<?=$tp; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<script src="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->


<script>
    $('#datetimepicker1').datetimepicker({
        defaultDate: new Date(),
        format: 'YYYY-MM-DD H:mm:ss',
        sideBySide: true
    });
</script>

        <script>

            $( document ).ready(function() {
                $('#quote').on('click', function(){
                    var id = $('#quotation').val();

        $.ajax({
        url: 'get_variant_group',
        type: 'post',
        data: 'id='+id+'&_token=<?=csrf_token(); ?>',
        success: function(data){
        $('#showGroupData').html('');
        $('#showGroupData').html(data);
        var no_of_rows = $('#showGroupData').find('tr').length;
        var no_of_rows1 = no_of_rows - 1;
        alert("Total:" + no_of_rows1);

        }
        });
                });

            });

        </script>
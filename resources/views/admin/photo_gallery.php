<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Photo</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="photo_gallery" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Title
						<input name="title" type="text"  class="form-control" />
					  </div>
						<div class="input-field col s12 m6">
							Banner images
							<input type="file" class="form-control" name="banner_image" accept="image/*"  required/>
						</div>
						</div> 
						<div class="row">               
					  <div class="input-field col s12 m6">
						Description
						<textarea name="description" class="form-control" ></textarea>
					  </div>
					  <div class="input-field col s12 m6">
							Gallery Category
							<select name="category" class="form-control select2">';
    foreach ($data['categories'] as $category){
        echo '<option value="'.$category->id.'">'.$category->name.'</option>';
        $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$category->id." ORDER BY id DESC");
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id." ORDER BY id DESC");
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
					  </div>
					   </div>
					   <div class="row"> 
					  <div class="input-field col s12 m6">
                        Is Featured
                        <label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input type="radio" name="featured" value="1"><span class="checkmark"></span>
</label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No &nbsp;&n<input type="radio" name="featured" value="0" checked><span class="checkmark"></span></label> 
                      </div>
                      </div>
					  <input name="add" type="submit" value="Add Photos" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else if(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Photo</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="photo_gallery" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Title
						<input name="title" type="text"  value="'.$data['photo']->title.'" class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
					  <p>Uploading new images will overwrtite current images .</p>
					  <img class="col-md-2" src="'.url('/assets/images/gallery/'.$data['photo']->image).'" />
					  </div>
					  </div>
					  <div class="row">
						<div class="input-field col s12 m6">
							Banner images
							<input type="file" class="form-control" name="banner_image" accept="image/*"  required/>
						</div>
					  <div class="input-field col s12 m6">
						Description
						<textarea name="description" class="form-control">'.$data['photo']->description.'</textarea>
					  </div></div>
					  <div class="row">
						<div class="input-field col s12 m6">
							Gallery category
							<select name="category" class="form-control">';
    foreach ($categories as $category){
        echo '<option value="'.$category->id.'" '.($category->id == $data['photo']->category ? 'selected' : '').'>'.$category->name.'</option>';
        $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$category->id." ORDER BY id DESC");
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'" '.($child->id == $data['photo']->category ? 'selected' : '').'>- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id." ORDER BY id DESC");
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'" '.($subchild->id == $data['photo']->category ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
						</div>
						<div class="input-field col s12 m6">
							Is Featured';
    $rval = $data['photo']->featured;
    $yes = '';
    $no = '';
    if($rval == '1'){
        $yes = 'checked';
    }else{
        $no = 'checked';
    }
    echo '<label style="margin-right: 5px; font-size: 12px;" class="radio_container">Yes<input type="radio" name="featured" value="1" '.$yes.'><span class="checkmark"></span></label><label style="margin-right: 5px; font-size: 12px;" class="radio_container">No<input type="radio" name="featured" value="0" '.$no.'><span class="checkmark"></span></label> 
						  </div></div>
					  
					  <input name="edit" type="submit" value="Edit Photo" style="padding: 3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Photo Gallery<a href="photos?add" style="padding:3px 15px;" class="add">Add photos</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your photos</h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Album Banner</th>
                            <th>Album Title</th>
                            <th>Album Category</th>
                            <th>Total Photos</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['photo'] as $des){
                            echo'<tr>
            <td>'.$sr.'</td>
            <td><img style="height: 50px;width:auto;" src="'.url('/assets/images/gallery/'.$des->image).'" /></td>
			<td>'.$des->title.'</td>';
                            echo '<td>' . getPlanCategory($des->category) . '</td>';
                            echo '<td>' . getPlanPhotsCount($des->id) . '</td>';

                            ECHO '<td><a href="photos?delete='.$des->id.'"><i class="icon-trash"></i></a>
						<a href="photos?edit='.$des->id.'"><i class="icon-pencil"></i></a>
						<a href="update-photos?gallery='.$des->id.'" title="Add Photos" data-toggle="title"><i class="icon-plus"></i></a>
					</td>
				  </tr>';


                            echo'</div>
			</div>';
                            $sr++;}
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $data['footer'];
?>




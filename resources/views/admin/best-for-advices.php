<?php
echo $header;
if(isset($_GET['add'])) {
    echo $notices.'<form action="" method="post" enctype="multipart/form-data" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="best-for-advices"><i class="icon-arrow-left"></i></a>Add new Best For Advices</h5>
				<fieldset>
				        <div class="form-group">
						<label class="control-label">Title</label>
						<input name="title" type="text"  class="form-control" />
					  </div>
					  <div class="form-group">
						<label class="control-label">Link</label>
						<input name="link" type="text"  class="form-control" />
					  </div>
					  
					  
					  <div class="form-group">
						<label class="control-label">Content</label>
						<textarea name="content" class="form-control" rows="10" cols="80" required></textarea>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Image</label>
						<input name="image" type="file"  class="form-control" />
					  </div>
					  
					  <input name="add" type="submit" value="Submit" class="btn btn-primary" />
				</fieldset>
			</form>';
} elseif(isset($_GET['edit'])) {

    echo $notices.'<form action="" method="post" enctype="multipart/form-data" class="form-horizontal single">
			'.csrf_field().'
			<h5><a href="best-for-advices"><i class="icon-arrow-left"></i></a>Edit Best For Advices</h5>
				<fieldset>
				    <div class="form-group">
						<label class="control-label">Title</label>
						<input name="title" type="text" value="'.$bestforcat->title.'" class="form-control" />
					  </div>
					  <div class="form-group">
						<label class="control-label">Link</label>
						<input name="link" type="text"  value="'.$bestforcat->link.'" class="form-control" />
					  </div>
					  
					  
					  <div class="form-group">
						<label class="control-label">Content</label>
						<textarea name="content" class="form-control" rows="10" cols="80" required>'.$bestforcat->content.'</textarea>
					  </div>';
					  if (!empty($bestforcat->image)){

							  echo '<p>Uploading new images will overwrtite current images .</p>';

									echo '<img class="col-md-2" src="'.url('/assets/products/'.$bestforcat->image).'" />';

							  echo '<div class="clearfix"></div>';
						  }
						  echo '
					  
					  <div class="form-group">
						<label class="control-label">Image</label>
						<input name="image" type="file"  class="form-control" />
					  </div>
					  
					  <input name="edit" type="submit" value="Update" class="btn btn-primary" />
				</fieldset>
			</form>';
} else {
    ?>
    <div class="head">
        <h3>Best For Advices<a href="best-for-advices?add" class="add">Add Best</a></h3>
        <p>Manage your best for advices</p>
    </div>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered" id="datatable-editable">
                <thead>
                <tr class="bg-blue">
                    <th>Sr. No.</th>
                    <th>Link</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sr = 1;
                echo $notices;
                foreach ($bestforcats as $category){
                    echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'.$category->link.'</td>
                        <td>'.$category->title.'</td>
                        <td>'.$category->content.'</td>
                        <td><img src="../assets/products/'.image_order($category->image).'" style="height: auto; width: 100px;"></td>
                        
            
                        <td>
                            <a href="best-for-advices?delete='.$category->id.'"><i class="icon-trash"></i></a>
                            <a href="best-for-advices?edit='.$category->id.'"><i class="icon-pencil"></i></a>
                        </td>
                      </tr>
                      ';
                    $sr++;
                }?>
                </tbody>
            </table>
        </div>
    </div>
</div>
    <?php
}
echo $footer;
?>
<script>
function removeThis($id){
	event.preventDefault();
	$('[data='+$id+']').remove();
}
$('#add-option').on('click', function(){
	var id = $('#que-type > div').length;
	var html = '<div class="col-md-12 options" data="'+id+'"><label class="control-label col-md-12" data="'+id+'">Option '+(id+1)+'</label><input name="option[]" type="text" data="'+id+'" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" /><a onClick="removeThis('+id+');" class="pul-right" style="line-height: 3;" data="'+id+'"><i class="icon-close" style="font-size: 20px;"></i></a></div>';
	$('#add-option').before(html);
});
function getData(data){
	if(data == 'text' || data == 'textarea'){
		$('.options').hide();
	}else{
		$('.options').show();
	}	
}


function addit(val){
    var da = val;
    $.ajax({
        url: 'catData',
        type: 'post',
        data: 'page='+da+'&_token=<?=csrf_token(); ?>',
        success: function(data){
            $('#catdata').html(data);
        }
    });
}

</script>
<script>
    $( document ).ready(function() {
        CKEDITOR.replace( 'editor' );
        CKEDITOR.replace( 'editor1' );
    });
</script>
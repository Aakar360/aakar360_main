<?php
echo $data['header'];
if(isset($_GET['edit'])){
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit plan status</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="plan_orders" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" class="form-horizontal single" enctype="multipart/form-data">
			'.csrf_field().'
			
				<div class="row">
					  <div class="form-group">
						<label class="control-label">Status</label>
						<select name="status" class="form-control">';
    if($data['plan']->status == 'Processing') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" selected>Processing</option>
                                <option value="Viewed">Viewed</option>
                                <option value="Hold">Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['plan']->status == 'Viewed') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" selected>Viewed</option>
                                <option value="Hold">Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['plan']->status == 'Hold') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" selected>Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['plan']->status == 'Pending') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" >Hold</option>
                                <option value="Pending" selected>Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    } else if($data['plan']->status == 'Completed') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" >Hold</option>
                                <option value="Pending" >Pending</option>
                                <option value="Completed" selected>Completed</option>
                                <option value="Cancelled">Cancelled</option>';
    }
    else if($data['plan']->status == 'Cancelled') {
        echo '<option value="">Please Select Status</option>
                                <option value="Processing" >Processing</option>
                                <option value="Viewed" >Viewed</option>
                                <option value="Hold" >Hold</option>
                                <option value="Pending" >Pending</option>
                                <option value="Completed" >Completed</option>
                                <option value="Cancelled" selected>Cancelled</option>';
    }
    echo '</select>
					  </div>
					  <input name="edit" type="submit" value="Edit order status" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
    ?>


<?php } elseif(isset($_GET['details'])){ ?>
    <br/>
    <div class="col-md-8">
        <div class="list">
            <div class="title">
                <a href="plan_orders"><i class="icon-arrow-left"></i> List</a>
            </div>
            <div class="title">
                <i class="icon-user"></i> Customer details
            </div>
            <div class="item"><h6><b>Order ID</b> : #<?=$data['pland']->id?></h6></div>
            <div class="item"><h6><b>Order time</b> : <?=$data['pland']->created; ?></h6></div>
            <div class="item order">
                <h6><b>Shipping Address</b> : </h6>
                <p>
                    <?php $datad = json_decode($data['pland']->address);

                    echo '<b>Name : </b> '.$datad->shipping_fname.'<br>';
                    echo '<b>Company : </b> '.$datad->shipping_company.'<br>';
                    echo '<b>Address Line 1 : </b> '.$datad->shipping_address_line_1.'<br>';
                    echo '<b>Address Line 2 : </b> '.$datad->shipping_address_line_2.'<br>';
                    echo '<b>Country : </b> '.getCountryName($datad->shipping_country).'<br>';
                    echo '<b>State : </b> '.getStateName($datad->shipping_state).'<br>';
                    echo '<b>City : </b> '.getCityName($datad->shipping_city).'<br>';
                    echo '<b>Pincode : </b> '.$datad->shipping_postcode.'<br>';
                    echo '<b>Contact No. : </b> '.$datad->shipping_mobile.'<br>';
                    echo '<b>Alternate Contact No. : </b> '.$datad->shipping_alternate_contact.'<br>';?>
                </p>
            </div>
            <br/>
        </div>
        <div class="list">
            <div class="title">
                <i class="icon-basket"></i> Plan Details
            </div>
            <?php
            $layout_id = $data['pland']->plan_id;
            $addons = $data['pland']->addon_id;
            $package = $data['pland']->package_id;
            $totalprice = $data['pland']->package_price;?>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Plan Name</th>
                        <th>Package Name</th>
                        <th>Addons Detail</th>
                        <th class="text-right">Package Price</th>
                        <th class="text-right">Addons Price</th>
                        <th class="text-right">Total Price</th>
                        <th class="text-right">Tax(%)</th>
                        <th class="text-right">Tax Amount</th>
                        <th class="text-right">Total Amount</th>
                    </tr>
                    </thead>
                    <?php $serv = DB::table('layout_plans')->where('id',$layout_id)->first();
                    $pack = '';
                    $pprice = '';
                    if($serv !== null){
                        if($package == '1'){
                            $pack = $serv->p1_name;
                            $pprice = $serv->p1_price;
                        }elseif($package == '2'){
                            $pack = $serv->p2_name;
                            $pprice = $serv->p2_price;
                        }elseif($package == '3'){
                            $pack = $serv->p3_name;
                            $pprice = $serv->p3_price;
                        }
                    }
                    $aprice = $totalprice - $pprice;
                    $tax = 0;
                    $taxamt= 0;
                    $totaladonprice = 0;
                    $taxprice = 0; ?>
                    <tr>
                        <td><?php if($serv !== null){ echo $serv->title; } ?></td>
                        <td><?php echo $pack; ?></td>
                        <td>
                            <?php $ad = explode(',', $addons);
                            foreach ($ad as $a){
                                $as = DB::table('layout_addon')->where('id',$a)->first();
                                if($as !== null){
                                    echo $as->name.' - '.c($as->price).'<br>';
                                    $totaladonprice += $as->price;
                                }
                            } ?>
                        </td>
                        <td class="text-right"><?php echo c($pprice); ?></td>
                        <td class="text-right"><?php echo c($totaladonprice);
                            if($serv !== null){
                                if($serv->tax !== null) {
                                    $tax = $serv->tax;
                                    $taxa = 1 + ($tax / 100);
                                    $taxprice = ($pprice + $totaladonprice) - ($pprice + $totaladonprice)/$taxa;

                                } else { $tprice = $totalprice; } }?>
                        </td>
                        <td class="text-right"><?php echo c(($pprice + $totaladonprice)); ?></td>
                        <td class="text-right"><?php echo $tax; ?></td>
                        <td class="text-right"><?php echo c(number_format($taxprice, 2, '.', '')); ?></td>
                        <td class="text-right"><?php echo c(($pprice + $totaladonprice)); ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="list">
            <div class="title">
                <i class="icon-credit-card"></i> Payment details
            </div>
            <?php
            $payment_data = json_decode($data['pland']->payment_method,true); ?>
            <div class="alert alert-warning" style="margin-bottom: 0;">Order unpaid</div>
            <?php if ($payment_data['method'] == 'paypal') { ?>
                <div class="item order"><h6><b>Payment method : </b>PayPal</h6></div>
                <div class="item order"><h6><b>Payment status : </b><?php echo $payment_data['payment_status']; ?></h6></div>
                <div class="item order"><h6><b>Transcation ID : </b><?php echo $payment_data['txn_id']; ?></h6></div>
                <div class="item order"><h6><b>Receiver e-mail : </b><?php echo $payment_data['receiver_email']; ?></h6></div>
                <div class="item order"><h6><b>Payer e-mail : </b><?php echo $payment_data['payer_email']; ?></h6></div>
                <div class="item order"><h6><b>Payment amount : </b><?php echo $payment_data['payment_amount']; ?></h6></div>
                <div class="item order"><h6><b>Payment currency : </b><?php echo $payment_data['payment_currency']; ?></h6></div>
            <?php } elseif ($payment_data['method'] == 'stripe') { ?>
                <div class="item order"><h6><b>Payment method : </b>Stripe</h6></div>
                <div class="item order"><h6><b>Charge ID : </b><?php echo $payment_data['charge_id']; ?></h6></div>
                <div class="item order"><h6><b>Balance transaction : </b><?php echo $payment_data['balance_transaction']; ?></h6></div>
                <div class="item order"><h6><b>Card ID : </b><?php echo $payment_data['card']; ?></h6></div>
                <div class="item order"><h6><b>Card brand : </b><?php echo $payment_data['card_brand']; ?></h6></div>
                <div class="item order"><h6><b>Last 4 : </b><?php echo $payment_data['last4']; ?></h6></div>
                <div class="item order"><h6><b>Expiry month : </b><?php echo $payment_data['exp_month']; ?></h6></div>
                <div class="item order"><h6><b>Expiry year : </b><?php echo $payment_data['exp_year']; ?></h6></div>
                <div class="item order"><h6><b>Fingerprint : </b><?php echo $payment_data['fingerprint']; ?></h6></div>';
            <?php } elseif ($payment_data['method'] == 'cash') { ?>
                <div class="item order"><h6><b>Payment method : </b>Cash on delivery</h6></div>
            <?php } elseif ($payment_data['method'] == 'bank') { ?>
                <div class="item order"><h6><b>Payment method : </b>Bank Transfer</h6></div>
            <?php }?>
        </div>
        <div class="list">
            <div class="title">
                <i class="icon-badge"></i>Order status
            </div>
            <div class="item text-center"><br/><h6><?=$data['pland']->status;?></h6><br/></div>
        </div>
    </div>
<?php } else { ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Plan Order(s)</h5>
                    <h5>Manage your plan order(s)</h5>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>User Name</th>
                                <th>Plan Name</th>
                                <th>Package Name</th>
                                <th>Package Price</th>
                                <th>Addon</th>
                                <th>Payment Method</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['plans'] as $p){
                                echo'<tr>
                        <td>'.$sr.'</td>';
                                $u = $p->user_id;
                                $user = DB::table('customers')->where('id',$u)->first();
                                if($user !== null){
                                    echo '<td>'.$user->name.'</td>';
                                }else{ echo '<td>NA</td>'; }

                                $pl = DB::table('layout_plans')->where('id',$p->plan_id)->first();
                                if($pl !== null){
                                    echo '<td>'.$pl->title.'</td>';
                                }else{ echo '<td>NA</td>'; }

                                $package_name = '';
                                $package_price = '';
                                if($p->package_id == '1'){
                                    $package_name = $pl->p1_name;
                                    $package_price = $pl->p1_price;
                                } elseif($p->package_id == '2'){
                                    $package_name = $pl->p2_name;
                                    $package_price = $pl->p2_price;
                                } elseif($p->package_id == '3'){
                                    $package_name = $pl->p3_name;
                                    $package_price = $pl->p3_price;
                                }
                                echo '<td>'.$package_name.'</td>
                        <td>'.$package_price.'</td>

                        <td>';
                                $ad = explode(',', $p->addon_id);
                                foreach ($ad as $a){
                                    $as = DB::table('layout_addon')->where('id',$a)->first();
                                    if($as !== null){
                                        echo $as->name.' - '.c($as->price).'<br>';
                                    }
                                }
                                echo '</td><td>';
                                $pay = json_decode($p->payment_method);
                                foreach ($pay as $key=>$value) {
                                    echo $key.' - '.$value.'<br>';
                                }
                                echo '</td>
                        <td>'.$p->status.'</td>
                        <td>'.$p->created.'</td>
                        <td><a href="plan_orders?edit='.$p->id.'"><i class="icon-question"></i></a>
                        <a href="plan_orders?details='.$p->id.'"><i class="icon-eye"></i></a>
                        </td></tr>';

                                $sr++;}
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php } ?>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $data['footer'];
?>




<?php 
	echo $header;
	echo $notices;
?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <form action="" method="post" style="max-width: 100%;">
	<?=csrf_field()?>
	<h5>Profile</h5>
        <div class="row">
		<div class="form-group col-md-6">
			<label class="control-label">Your name</label>
			<input name="name" type="text"  value="<?=$user->u_name;?>" class="form-control" />
		</div>
            <div class="form-group col-md-6">
			<label class="control-label">Your Email</label>
			<input name="email" type="text"  value="<?=$user->u_email;?>" class="form-control" />
		</div>
            <div class="form-group col-md-6">
			<label class="control-label">Password</label>
			<input name="pass" type="password"  class="form-control" />
		</div>
        </div>
		<input name="update" type="submit" value="Update" style="padding:3px 25px;" class="btn btn-primary" />

</form>
    </div>
    </div>
    </div>
    </div>
<?php echo $footer;?>
<?php
echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Add new menu</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="bottom" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="form-group col-md-6">
							<label class="control-label">menu item link</label>
							<input name="link" type="text"  class="form-control" required/>
						  </div>
						  <div class="form-group col-md-6">
							<label class="control-label">menu item title</label>
							<input name="title" type="text" class="form-control" required/>
						  </div>
						  </div>
						  <input name="add" type="submit" value="Add menu item" style="padding:3px 25px;" class="btn btn-primary" />
					
				</form>
				</div>
				</div>
				</div>
				</div>';
}
elseif(isset($_GET['edit']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Edit menu item</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="bottom" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
    <form action="" method="post" style="max-width: 100%;">
				'.csrf_field().'
				
					<div class="row">
						  <div class="form-group col-md-6">
							<label class="control-label">menu item title</label>
							<input name="title" type="text"  value="'.$data['item']->title.'" class="form-control" required/>
						  </div>
						  <div class="form-group col-md-6">
							<label class="control-label">menu item link</label>
							<input name="link" type="text" value="'.$data['item']->link.'" class="form-control" required/>
						  </div>
						  </div>
						  <input name="edit" type="submit" value="Edit menu item" style="padding:3px 25px;" class="btn btn-primary" />
					
				</form>
				</div>
				</div>
				</div>
				</div>';
}else{
    ?>
    <div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h3>Menu<a href="bottom?add" style="padding:3px 15px;" class="add">Add menu item</a></h3>
    <p>Manage footer menu links</p>

    <?php
    echo $data['notices']."<ul>";
    foreach ($data['items'] as $item){
        echo '<li id="'.$item->id.'" class="m">'.$item->title.'
			<div class="tools">
			<a href="bottom?delete='.$item->id.'"><i class="icon-trash"></i></a>
			<a href="bottom?edit='.$item->id.'"><i class="icon-pencil"></i></a>
			</div>
			<p>'.$item->link.'</p></li>';
    }
    echo "</ul><button class='btn save'>Save</button>";
}
?>
    </div>
    <style>
        .save {

            color: white !important;
            border-radius: 30px !important;
            margin: auto;
            padding: 1px 30px !important;
            display: block !important;
        }
    </style>
    <script src="<?=$data['tp'];?>/assets/jquery-ui.min.js"></script>
    <script>
        $(function(){
            function serializeList(container)
            {
                var str = ''
                var n = 0
                var els = container.find('li.m')
                for (var i = 0; i < els.length; ++i) {
                    var el = els[i]
                    var p = el.id
                    if (p != -1) {
                        if (str != '') str = str + '&'
                        str = str + 'item[]=' + p
                        ++n
                    }
                }
                return str
            }
            $('ul').sortable({connectWith:"ul"});
            $('.save').click(function(){
                var data= serializeList($('ul'));
                $.post('bottom?save',{"data":data,_token: '<?=csrf_token()?>'},function(d){
                    alert('Saved');
                });
            });
        });
    </script>
<?php echo $data['footer'];?>
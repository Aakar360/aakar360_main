<?php echo $data['header'];
if(isset($_GET['add']))
{
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
    <h5>Add New Conversions</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="tmtcalculator" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
    <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
				'.csrf_field().'
				
					<div class="row">
						  
						  <div class="input-field col s12 m6">
							Brand name
							<select name="brand_id" class="form-control" required id="brand_name">';
    foreach($data['brand'] as $bnd){
        echo '<option  value="'.$bnd->id.'">'.translate($bnd->name).'</option>';
    }
    echo '</select>
						  </div>
						  <input name="brand_name" type="hidden"  class="form-control" id="bid"/>
						  <div class="input-field col s12 m6">
							<label class="control-label">Variants</label>
							<input name="variant_name" type="text"  class="form-control" required/>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">KG</label>
							<input name="kg" type="text"  class="form-control" required/>
						  </div>
						  <div class="input-field col s12 m6">
							<label class="control-label">Rod</label>
							<input name="rod" type="text"  class="form-control" required/>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							<label class="control-label">Bundle</label>
							<input name="bundle" type="text"  class="form-control" required/>
						  </div>
						  </div>
						  <input name="add" type="submit" value="Add Conversion" style="padding:3px 25px;" class="btn btn-primary" />
					</div>
				</form>
				</div>
				</div>
				</div>
				</div>';
}  elseif(isset($_GET['edit'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
    <div class="card-content" style="min-height: 550px;">
     <h5>Edit Conversions</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="tmtcalculator" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
    <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
			        <div class="row">
						  <div class="input-field col s12 m6">
							Brand name
							<input name="brand_name" type="text" value="'.$data['tmtconversion']->brand_name.'"  class="form-control" readonly required/>
						  </div>
						  <div class="input-field col s12 m6">
							Variant name
							<input name="variant_name" type="text" value="'.$data['tmtconversion']->variant_name.'"  class="form-control" required/>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							KG
							<input name="kg" type="text"  class="form-control" value="'.$data['tmtconversion']->kg.'" required/>
						  </div>
						  <div class="input-field col s12 m6">
							Rod
							<input name="rod" type="text"  class="form-control" value="'.$data['tmtconversion']->rod.'" required/>
						  </div>
						  </div>
						  <div class="row">
						  <div class="input-field col s12 m6">
							Bundle
							<input name="bundle" type="text"  class="form-control" value="'.$data['tmtconversion']->bundle.'" required/>
						  </div>						  
						 </div>
                        <input name="edit" type="submit" value="Edit Conversion" style="padding: 3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>TMT Bar Calculator<a href="tmtcalculator?add" style="padding:3px 15px;" class="add">Add Conversion</a></h5>

                <h6>Manage TMT bar Details</h6>


                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatable-editable">
                        <thead>
                        <tr class="bg-blue">
                            <th>Sr. No.</th>
                            <th>Brand Name</th>
                            <th>Variant Name</th>
                            <th>KG</th>
                            <th>Rod</th>
                            <th>Bundle</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sr = 1;
                        echo $data['notices'];
                        foreach ($data['tmtbars'] as $tmt) {

                            echo '<tr>
            <td>' . $sr . '</td>
            <td>' . $tmt->brand_name . '</td>           
            <td>' . $tmt->variant_name . '</td>           
            <td>' . $tmt->kg . '</td>           
            <td>' . $tmt->rod . '</td>           
            <td>' . $tmt->bundle . '</td>           
            <td>               
                <a href="tmtcalculator?edit=' . $tmt->id . '"><i class="icon-pencil"></i></a>
                <a href="tmtcalculator?delete=' . $tmt->id . '"><i class="icon-trash"></i></a>';
                            echo '</td>
          </tr>';
                            $sr++;
                        } ?>
                        </tbody>
                    </table>
                </div>

                <?php
                }
                echo $data['footer'];
                ?>
                <script>
                    $(document).ready(function() {
                        $('#brand_name').on('change', function(){
                            var a = $(this).find('option:selected').text();
                            $('#bid').val(a);

                        });
                    });
                </script>

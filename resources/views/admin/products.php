<!DOCTYPE html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="keywords" content="">
        <meta name="author" content="ThemeSelect">
        <link rel="apple-touch-icon" href="<?=$data['tp']; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
        <link rel="shortcut icon" type="image/x-icon" href="<?=$data['tp']; ?>/admin/images/favicon/favicon-32x32.png">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
        <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
        <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/pages/data-tables.css">
        <link rel="stylesheet" type="text/css" href="<?=$data['tp']; ?>/admin/css/custom/custom.css">
        <link href="<?=$data['tp']; ?>/admin/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://rawgit.com/Eonasdan/bootstrap-datetimepicker/master/build/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<?php
echo $data['header'];
if(isset($_GET['add']))
{
echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 650px;">
                    <h5>Add Product</h5>
                    <div class="col s12 m3 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:10px; 5px; width: 110px;" href="products" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    '.csrf_field().'
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Product name
                            <input name="title" type="text"  class="form-control" required/>
                        </div>
                        <div class="input-field col s12 m6">
                            Product category
                            <select name="category" class="form-control">';
                                foreach ($data['categories'] as $category){
                                    echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                    $childs = DB::table("category")->where('parent', '=', $category->id)->orderBy('id','DESC')->get();
                                    foreach ($childs as $child){
                                        echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
                                        $subchilds = DB::table("category")->where('parent', '=', $child->id)->orderBy('id','DESC')->get();
                                        foreach ($subchilds as $subchild){
                                            echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                        }
                                    }
                                }
                            echo '</select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Bought Together Category(s)
                            <select name="fb_cat[]" multiple class="form-control select2">';
                                foreach ($data['categories'] as $category){
                                    echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                    $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
                                    foreach ($childs as $child){
                                        echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
                                        $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id." ORDER BY id DESC");
                                        foreach ($subchilds as $subchild){
                                            echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                        }
                                    }
                                }
                            echo '</select>
                        </div>
                        <div class="input-field col s12 m6">
                            Brand
                            <select name="brand_id" class="form-control">
                                <option value="">Please Select Brand</option> ';
                                foreach ($data['brands'] as $brand){
                                    echo '<option value="'.$brand->id.'">'.$brand->name.'</option>';
                                }
                            echo '</select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Product description
                            <textarea name="text" type="text" id="" class="form-control editor1"></textarea>
                        </div>
                        <div class="input-field col s12 m6">
                            Product specification
                            <textarea name="specification" id="" type="text" class="form-control editor"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            HSN Code
                            <input name="hsn" type="text" class="form-control" />
                        </div>
                        <div class="input-field col s12 m6">
                            Purchase Price 
                            <input name="purchase_price" type="text" class="form-control" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m3">
                            Is Price Expire?
                            <select name="is_variable" class="form-control">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            Validity
                            <div class="input-group date" id="datetimepicker1">
                                <input type="text" name="validity" value=" " class="form-control validity">
                                <span class="input-group-addon" style="position: relative;">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="input-field col s12 m3">
                            Substract Stock?
                            <select name="substract_stock" class="form-control">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Tax (%)
                            <input name="tax" type="number" class="form-control" required />
                        </div>
                        <div class="input-field col s12 m6">
                            Available quantity
                            <input name="quantity" type="text" class="form-control" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Weight
                            <input name="weight" type="text" class="form-control" required/>
                        </div>
                        <div class="input-field col s12 m6">
                            Weight Unit
                            <select name="weight_unit" class="form-control">
                                <option>Select Weight Unit</option>';
                                foreach ($data['units'] as $unit){
                                    echo '<option value="'.$unit->id.'">'.$unit->symbol.'</option>';
                                }
                            echo '</select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Product images
                            <input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"  required/>
                        </div>
                        <div class="input-field col s12 m6">
                            Download ( digital product )
                            <input type="file" class="form-control" name="download"/>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="input-field col s12">
                            Customer options
                            <input type="hidden" id="option_count"/>
                            <div id="add_option" class="button pull-right">Add field</div>
                        </div>
                        <div class="col-lg-5" id="options"></div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: -13px;">
            <div class="col s12">
                <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                    <div class="card-content" style="min-height: 420px;">
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <h4>Institutional Price</h4>
                                <div class="col phitems">
                                    <div class="phitem">
                                        <div class="form-group col-md-2 proDiv" style="margin: 0 0 10px 0">
                                            Hubs
                                            <select class="form-control select2 hubs" id="hubs">';
                                                echo '<option value="">Select Hub</option>';
                                                foreach ($data['hubs'] as $hub){
                                                    echo '<option value="'.$hub->id.'">'.$hub->title.'</option>';
                                                }
                                            echo '</select>
                                        </div>
                                        <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                            Purchase Price
                                            <input type="text" value="0" class="form-control pprice"/>
                                        </div>
                                        <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                            Price
                                            <input type="text" value="0" class="form-control price"/>
                                        </div>
                                        <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                            Quantity
                                            <input type="text" value="0" class="form-control ins_quantity" name="ins_quantity"/>
                                        </div>
                                        <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                            Validity
                                            <div class="input-group date" id="datetimepicker1">
                                                <input type="text" value=" " class="form-control ins_validity" name="ins_validity">
                                                <span class="input-group-addon" style="position: relative;">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                            Loading
                                            <input type="text" value="0" class="form-control lprice"/>
                                        </div>
                                        <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                            <button type="button" class="btn btn-primary addNew" value="1">+</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m4">
                                        Accept Special Requirements
                                        <select name="asr" class="form-control">
                                            <option value="0" >No</option>
                                            <option value="1" >Yes</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s12 m4">
                                        Show Price
                                        <select name="sp" class="form-control">
                                            <option value="0" >No</option>
                                            <option value="1" >Yes</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s12 m4">
                                        Minimum Order Quantity (Inst.)
                                        <input name="min_ins" type="text" class="form-control" required value="0"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        Show Description
                                        <select name="description_view" class="form-control">
                                            <option value="0" >No</option>
                                            <option value="1" >Yes</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        Show Specification
                                        <select name="specification_view" class="form-control">
                                            <option value="0" >No</option>
                                            <option value="1" >Yes</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        Show Rating
                                        <select name="rating_view" class="form-control">
                                            <option value="0" >No</option>
                                            <option value="1" >Yes</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        Show Faq
                                        <select name="faq_view" class="form-control">
                                            <option value="0" >No</option>
                                            <option value="1" >Yes</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s12 m3">
                                        Show Weight And Length
                                        <select name="show_weight_length" class="form-control">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: -13px;">
                <div class="col s12">
                    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                        <div class="card-content">
                            <h4>Retail Price</h4>
                            <div class="row">
                                <h6>City Wise Price</h6>
                                <div class="row">
                                    <div class="col cwitems">
                                        <div class="cwitem">
                                            <div class="form-group col-md-2 cwproDiv" style="margin: 0 0 10px 0">
                                                Cities
                                                <select class="form-control select2 cities" id="cities" name="cities[]">';
                                                    echo '<option value="">Select City</option>';
                                                    foreach ($data['cities'] as $hub){
                                                        echo '<option value="'.$hub->name.'">'.$hub->name.'</option>';
                                                    }
                                                echo '</select>
                                            </div>
                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                Purchase Price
                                                <input type="text" value="0" class="form-control cwpprice" name="cwpprice[]"/>
                                            </div>
                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                Price
                                                <input type="text" value="0" class="form-control cwprice" name="cwprice[]"/>
                                            </div>
                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                Quantity
                                                <input type="text" value="0" class="form-control cwins_quantity" name="cwins_quantity[]"/>
                                            </div>
                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                Validity
                                                <div class="input-group date" id="datetimepicker1">
                                                    <input type="text" value="" class="form-control cwins_validity" name="cwins_validity[]">
                                                    <span class="input-group-addon" style="position: relative;">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                Loading
                                                <input type="text" value="0" class="form-control cwlprice" name="cwlprice[]"/>
                                            </div>
                                            <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                <button type="button" class="btn btn-primary cwaddNew" id="moreButton" value="1">+</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div><br><br>
                                <div id="insertmoreBefore"></div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="input-field col s12 m6">
                                        Market Price (Retail)
                                        <input name="price" type="text" class="form-control" required />
                                    </div>
                                    <div class="input-field col s12 m6">
                                        Sale Price (Retail)
                                        <input name="sale" type="text" class="form-control" required />
                                    </div>
                                    <div class="input-field col s12 m3">
                                        Minimum Order Quantity (Retail)
                                        <input name="min" type="text" class="form-control" required value="0"/>
                                    </div>
                                    <div class="input-field col s12 m6">
                                        Maximum Order Quantity (Retail)
                                        <input name="max" type="text" class="form-control" required value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: -13px;">
                <div class="col s12">
                    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s12">
                                    <input name="add" type="submit" value="Add product" style="padding: 3px 25px;" class="btn btn-primary" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
</div>';
} elseif(isset($_GET['edit'])){
echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 650px;">
                    <h5>Update Product</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:10px; 5px; width: 110px;" href="products" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    '.csrf_field().'
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Product name
                            <input name="title" type="text" value="'.$data['product']->title.'" class="form-control"  required/>
                        </div>
                        <div class="input-field col s12 m6">
                            Product category
                            <select name="category" class="form-control">';
                                foreach ($data['categories'] as $category){
                                    echo '<option value="'.$category->id.'" '.($category->id == $data['product']->category ? 'selected' : '').'>'.$category->name.'</option>';
                                    $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
                                    foreach ($childs as $child){
                                        echo '<option value="'.$child->id.'" '.($child->id == $data['product']->category ? 'selected' : '').'>- '.$child->name.'</option>';
                                        $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id." ORDER BY id DESC");
                                        foreach ($subchilds as $subchild){
                                            echo '<option value="'.$subchild->id.'" '.($subchild->id == $data['product']->category ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                        }
                                    }
                                }
                            echo '</select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Bought Together Category(s)
                            <select name="fb_cat[]" multiple class="form-control select2">';
                                foreach ($data['categories'] as $category){
                                    $fb_cats = array();
                                    if($data['product']->fb_cat){
                                        $fb_cats = explode(',', $data['product']->fb_cat);
                                    }
                                    echo '<option value="'.$category->id.'" '.(in_array($category->id, $fb_cats) ? 'selected' : '').'>'.$category->name.'</option>';
                                    $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
                                    foreach ($childs as $child){
                                        echo '<option value="'.$child->id.'" '.(in_array($child->id, $fb_cats)? 'selected' : '').'>- '.$child->name.'</option>';
                                        $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id." ORDER BY id DESC");
                                        foreach ($subchilds as $subchild){
                                            echo '<option value="'.$subchild->id.'" '.($subchild->id == $data['product']->category ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                        }
                                    }
                                }
                            echo '</select>
                        </div>
                        <div class="input-field col s12 m6">
                            Brand
                            <select name="brand_id" class="form-control">';
                                foreach ($data['brands'] as $brand){
                                    echo '<option value="'.$brand->id.'" '; echo ($brand->id == $data['product']->brand_id) ? 'selected' : '';
                                    echo '>'.$brand->name.'</option>';
                                }
                            echo '</select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Product description
                            <textarea name="text" type="text" id="" class="form-control editor1">'.$data['product']->text.'</textarea>
                        </div>
                        <div class="input-field col s12 m6">
                            Product specification
                            <textarea name="specification" id="" type="text" class="form-control editor">'.$data['product']->specification.'</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            HSN Code
                            <input name="hsn" type="text" class="form-control" value="'.$data['product']->hsn.'" />
                        </div>
                        <div class="input-field col s12 m6">
                            Purchase Price 
                            <input name="purchase_price" type="text" class="form-control" value="'.$data['product']->purchase_price.'" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m3">
                            Is Price Expire?
                            <select name="is_variable" class="form-control">
                                <option '.($data['product']->is_variable == 0 ? "selected" : "").' value="0">No</option>
                                <option '.($data['product']->is_variable == 1 ? "selected" : "").' value="1">Yes</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            Validity
                            <div class="input-group date" id="datetimepicker1">
                                <input type="text" name="validity" value="'.$data['product']->validity.'" class="form-control validity">
                                <span class="input-group-addon" style="position: relative;">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="input-field col s12 m3">
                            Substract Stock?
                            <select name="substract_stock" class="form-control">
                                <option '.($data['product']->sub_stock == 0 ? "selected" : "").' value="0">No</option>
                                <option '.($data['product']->sub_stock == 1 ? "selected" : "").' value="1">Yes</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Tax (%)
                            <input name="tax" type="number" class="form-control" required value="'.$data['product']->tax.'" />
                        </div>
                        <div class="input-field col s12 m6">
                            Available quantity
                            <input name="quantity" type="text" class="form-control" value="'.$data['product']->quantity.'" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Weight
                            <input name="weight" type="text" class="form-control" value="'.$data['product']->weight.'" required/>
                        </div>
                        <div class="input-field col s12 m6">
                            Weight Unit
                            <select name="weight_unit" class="form-control">
                                <option>Select Weight Unit</option>';
                                foreach ($data['units'] as $unit){
                                    echo '<option value="'.$unit->id.'" '.(($unit->id == $data['product']->weight_unit) ? 'selected' : '').'>'.$unit->symbol.'</option>';
                                }
                            echo '</select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">';
                            if (!empty($data['product']->images)){
                                echo '<p>Uploading new images will overwrtite current images .</p>';
                                $images = explode(',',$data['product']->images);
                                foreach($images as $image){
                                    echo '<img class="col-md-2" src="'.url('/assets/products/'.$image).'" />';
                                }
                                echo '<div class="clearfix"></div>';
                            }
                            echo 'Product images
                            <input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"/>
                        </div>
                        <div class="input-field col s12 m6">';
                            if (!empty($data['product']->download)){
                                echo '<p>Uploading new file will overwrite current file .</p>';
                            }
                            echo 'Download
                            <input type="file" class="form-control" name="download"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            Customer options
                            <input type="hidden" id="option_count" value="'.count(json_decode($data['product']->options,true)).'"></input>
                            <div id="options">';
                                $options = json_decode($data['product']->options,true);
                                if(!empty($options)){
                                    foreach($options as $i=>$row){
                                        echo '<div class="input-field col s12 m6" data-no="'.$row['no'].'">
                                            <div class="col-sm-6">
                                                <input name="option_title[]" class="form-control" placeholder="Title" type="text" value="'.$row['title'].'">
                                            </div>
                                            <div class="col-sm-5">
                                                <select class="form-control option_type" name="option_type[]">
                                                    <option value="text"'; if($row['type'] == 'text'){ echo 'selected'; } echo '>Text input</option>
                                                    <option value="select"'; if($row['type'] == 'select'){ echo 'selected'; } echo '>Dropdown - single select</option>
                                                    <option value="multi_select"'; if($row['type'] == 'multi_select'){ echo 'selected'; } echo '>Checkbox - multi select</option>
                                                    <option value="radio"'; if($row['type'] == 'radio'){ echo 'selected'; } echo '>Radio</option>
                                                </select>
                                                <div class="options">';
                                                    if($row['type'] == 'text'){
                                                        echo '<input type="hidden" name="option_set'.$row['no'].'[]" value="none" >';
                                                    } else {
                                                        foreach ($row['option'] as $key => $row1) {
                                                            echo '<div style="margin: 10px -15px;">
                                                                <div class="col-sm-10">
                                                                    <input value="'.$row1.'" type="text" name="option_set'.$row['no'].'[]" class="form-control required"  placeholder="Option">
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <span class="remove-option mini-button"><i class="icon-close"></i></span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>';
                                                        }
                                                        echo '<div class="pull-right button add_option">Add option</div>';
                                                    }
                                                echo '</div>
                                            </div>
                                            <input name="option_no[]" value="'.$row['no'].'" type="hidden">
                                            <div class="col-sm-1"> <span class="remove mini-button"><i class="icon-close"></i></span> </div>
                                        </div>';
                                    }
                                }
                            echo '</div>
                            <div id="add_option" class="button pull-right">Add field</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: -13px;">
                <div class="col s12">
                    <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                        <div class="card-content" style="min-height: auto;">
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <h4>Institutional Price</h4>
                                    <div class="col phitems">
                                        <div class="phitem">
                                            <div class="form-group col-md-2 proDiv" style="margin: 0 0 10px 0">
                                                Hubs
                                                <select class="form-control select2 hubs" id="hubs">
                                                    <option value="">Select Hub</option>';
                                                    foreach ($data['hubs'] as $hub){
                                                        echo '<option value="'.$hub->id.'">'.$hub->title.'</option>';
                                                    }
                                                echo '</select>
                                            </div>
                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                Purchase Price
                                                <input type="text" value="0" class="form-control pprice"/>
                                            </div>
                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                Price
                                                <input type="text" value="0" class="form-control price"/>
                                            </div>
                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                Quantity
                                                <input type="text" value="0" class="form-control ins_quantity" name="ins_quantity"/>
                                            </div>
                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                Validity
                                                <div class="input-group date" id="datetimepicker1">
                                                    <input type="text" value=" " class="form-control ins_validity" name="ins_validity">
                                                    <span class="input-group-addon" style="position: relative;">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                Loading
                                                <input type="text" value="0" class="form-control lprice"/>
                                            </div>
                                            <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                <button type="button" class="btn btn-primary addNew" value="1">+</button>
                                            </div>
                                        </div>';
                                        if($data['ph'] !== null) {
                                            $mhubs = json_decode($data['ph']->hubs);
                                            $i = 1;
                                            foreach ($mhubs as $mh) {
                                                $m = \App\ManufacturingHubs::find($mh->hub);
                                                if(isset($mh->quantity)){
                                                    $quant = $mh->quantity;
                                                }else{
                                                    $quant = '';
                                                }
                                                if(isset($mh->validity)){
                                                    $val = $mh->validity;
                                                }else{
                                                    $val = '';
                                                }
                                                echo '<div class="phitem color2">
                                                    <div class="form-group col-md-2 proDiv" style="margin: 0 0 10px 0">
                                                        <input type="hidden" value="' . $m->id . '" name="hubs[]"><label class="control-label">Hub</label><input type="text" readonly="" value="' . $m->title . '" class="form-control"></div>
                                                        <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                            <label class="control-label">Purchase Price</label>
                                                            <input type="text" value="' . (isset($mh->pprice) ? $mh->pprice : $mh->price) . '" class="form-control pprice" name="pprice[]">
                                                        </div>
                                                        <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                            <label class="control-label">Price</label>
                                                            <input type="text" value="' . $mh->price . '" class="form-control price" name="iprice[]">
                                                        </div>
                                                        <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                            <label class="control-label">Quantity</label>
                                                            <input type="text" value="' . $quant . '" class="form-control ins_quantity" name="ins_quantity[]">
                                                        </div>
                                                        <div class="form-group col-md-2 valid" style="margin: 0 0 10px 0">
                                                            Validity
                                                            <div class="input-group date" id="picker">
                                                                <input type="text" value="' . $val . ' "class="form-control ins_validity" name="ins_validity[]">
                                                                <span class="input-group-addon" style="position: relative;">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                            <label class="control-label">Loading</label>
                                                            <input type="text" value="' . $mh->loading . '" class="form-control lprice" name="lprice[]">
                                                        </div>
                                                        <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                            <button type="button" class="btn btn-danger removeItem" value="' . $i . '">-</button>
                                                        </div>
                                                    </div>';
                                                $i++;}
                                            }
                                            echo '</div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12 m4">
                                                Accept Special Requirements
                                                <select name="asr" class="form-control">
                                                    <option value="0" '.(($data['product']->asr == 0)? "selected" : "").'>No</option>
                                                    <option value="1" '.(($data['product']->asr == 1)? "selected" : "").'>Yes</option>
                                                </select>
                                            </div>
                                            <div class="input-field col s12 m4">
                                                Show Price
                                                <select name="sp" class="form-control">
                                                    <option value="0" '.(($data['product']->product_showprice == 0)? "selected" : "").'>No</option>
                                                    <option value="1" '.(($data['product']->product_showprice == 1)? "selected" : "").'>Yes</option>
                                                </select>
                                            </div>
                                            <div class="input-field col s12 m4">
                                                Minimum Order Quantity (Inst.)
                                                <input name="min_ins" type="text" class="form-control" required value="0"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12 m6">
                                                Show Description
                                                <select name="description_view" class="form-control">
                                                    <option value="0" '.(($data['product']->description_view == 0)? "selected" : "").'>No</option>
                                                    <option value="1" '.(($data['product']->description_view == 1)? "selected" : "").'>Yes</option>
                                                </select>
                                            </div>
                                            <div class="input-field col s12 m6">
                                                Show Specification
                                                <select name="specification_view" class="form-control">
                                                    <option value="0" '.(($data['product']->specification_view == 0)? "selected" : "").'>No</option>
                                                    <option value="1" '.(($data['product']->specification_view == 1)? "selected" : "").'>Yes</option>
                                                </select>
                                            </div>
                                            <div class="input-field col s12 m6">
                                                Show Rating
                                                <select name="rating_view" class="form-control">
                                                    <option value="0" '.(($data['product']->rating_view == 0)? "selected" : "").'>No</option>
                                                    <option value="1" '.(($data['product']->rating_view == 1)? "selected" : "").'>Yes</option>
                                                </select>
                                            </div>  
                                            <div class="input-field col s12 m6">
                                                Show Faq
                                                <select name="faq_view" class="form-control">
                                                    <option value="0" '.(($data['product']->faq_view == 0)? "selected" : "").'>No</option>
                                                    <option value="1" '.(($data['product']->faq_view == 1)? "selected" : "").'>Yes</option>
                                                </select>
                                            </div>
                                            <div class="input-field col s12 m6">
                                                Show Weight Length
                                                <select name="show_weight_length" class="form-control">
                                                    <option value="0" '.(($data['product']->show_weight_length == 0)? "selected" : "").'>No</option>
                                                    <option value="1" '.(($data['product']->show_weight_length == 1)? "selected" : "").'>Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: -13px;">
                        <div class="col s12">
                            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                                <div class="card-content">
                                    <h4>Retail Price</h4>
                                    <div class="row">
                                        <h6>City Wise Price</h6>
                                        <div class="col cwitems">
                                            <div class="cwitem">
                                                <div class="form-group col-md-2 cwproDiv" style="margin: 0 0 10px 0">
                                                    Cities
                                                    <select class="form-control select2 cities" id="cities" name="cities[]">
                                                        <option value="">Select City</option>';
                                                        foreach ($data['cities'] as $hub){
                                                            echo '<option value="'.$hub->name.'">'.$hub->name.'</option>';
                                                        }
                                                    echo '</select>
                                                </div>
                                                <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                    Purchase Price
                                                    <input type="text" value="0" class="form-control cwpprice" name="cwpprice[]"/>
                                                </div>
                                                <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                    Price
                                                    <input type="text" value="0" class="form-control cwprice" name="cwprice[]"/>
                                                </div>
                                                <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                    Quantity
                                                    <input type="text" value="0" class="form-control cwins_quantity" name="cwins_quantity"/>
                                                </div>
                                                <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                    Validity
                                                    <div class="input-group date" id="datetimepicker1">
                                                        <input type="text" value=" " class="form-control cwins_validity" name="cwins_validity">
                                                        <span class="input-group-addon" style="position: relative;">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                    Loading
                                                    <input type="text" value="0" class="form-control lprice" name="cwlprice[]"/>
                                                </div>
                                                <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                    <button type="button" class="btn btn-primary cwaddNew" value="1">+</button>
                                                </div>
                                            </div>';
                                            if($data['cityWise'] !== null) {
                                                echo '<div class="cwitem color2">';
                                                $i = 1;
                                                foreach ($data['cityWise'] as $cw) {
                                                    $prices = json_decode($cw->prices);
                                                    foreach ($prices as $cwp) {
                                                        echo '<div class="form-group col-md-2 cwproDiv" style="margin: 0 0 10px 0">
                                                            <label class="control-label">City</label><input type="text" name="cities[]" readonly="" value="' . $cw->city . '" class="form-control"></div>
                                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                                <label class="control-label">Purchase Price</label>
                                                                <input type="text" value="' . (isset($cwp->pprice) ? $cwp->pprice : $cwp->price) . '" class="form-control cwpprice" name="cwpprice[]">
                                                            </div>
                                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                                <label class="control-label">Price</label>
                                                                <input type="text" value="' . $cwp->price . '" class="form-control cwprice" name="cwprice[]">
                                                            </div>
                                                            <div class="form-group col-md-2" style="margin: 0 0 10px 0">
                                                                <label class="control-label">Quantity</label>
                                                                <input type="text" value="' . $cwp->quantity . '" class="form-control cwins_quantity" name="cwins_quantity[]">
                                                            </div>
                                                            <div class="form-group col-md-2 valid" style="margin: 0 0 10px 0">
                                                                Validity
                                                                <div class="input-group date" id="picker">
                                                                    <input type="text" value="' . $cwp->validity . '"class="form-control cwins_validity" name="cwins_validity[]">
                                                                    <span class="input-group-addon" style="position: relative;">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                                <label class="control-label">Loading</label>
                                                                <input type="text" value="' . $cwp->loading . '" class="form-control lprice" name="cwlprice[]">
                                                            </div>
                                                            <div class="form-group col-md-1" style="margin: 0 0 10px 0">
                                                                <button type="button" class="btn btn-danger removeItem" value="' . $i . '">-</button>
                                                            </div>
                                                        </div>';
                                                    $i++; }
                                                }
                                            }
                                        echo '</div>
                                    </div>
                                    <div class="row">
                                        <div class="row">
                                            <div class="input-field col s12 m3">
                                                Market Price (Retail)
                                                <input name="price" type="text" value="'.$data['product']->price.'" class="form-control"  required/>
                                            </div>
                                            <div class="input-field col s12 m3">
                                                Sale Price (Retail)
                                                <input name="sale" type="text" value="'.$data['product']->sale.'" class="form-control"  required/>
                                            </div>
                                            <div class="input-field col s12 m6">
                                                Minimum Order Quantity (Retail)
                                                <input name="min" type="text" class="form-control" required value="'.$data['product']->min_qty.'"/>
                                            </div>
                                            <div class="input-field col s12 m6">
                                                Maximum Order Quantity (Retail)
                                                <input name="max" type="text" class="form-control" required value="'.$data['product']->max_qty.'"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: -13px;">
                        <div class="col s12">
                            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                                <div class="card-content">
                                    <div class="row">
                                        <div class="col s12">
                                            <input name="edit" type="submit" value="Update product" style="padding: 3px 25px;" class="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>';
}elseif(isset($_GET['add_variants'])){
    echo '<div class="row" style="margin-top: 13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 650px;">
                    <form action="products?add_variants='.$_GET['add_variants'].'" method="post" enctype="multipart/form-data" style="max-width: 100%;">
                        '.csrf_field().'
                        <h5><a href="products"><i class="icon-arrow-left"></i></a>Variants for <b>'.$data['product']->title.'</b></h5>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">Variant Group</label>
                                <select name="group" class="form-control" id="getgroup">
                                    <option value="">Please Select Group Name</option> ';
                                    foreach ($data['groups'] as $group){
                                        echo '<option value="'.$group->id.'">'.$group->name.'</option>';
                                    }
                                echo '</select>
                            </div>
                            <table id="showGroupData" style="background-color: white;">
                                <tr style=" border: 1px solid #000000;">
                                    <td style="padding: 10px;">
                                        <label class="control-label">Variant Title</label>
                                        <select name="variant_title" class="form-control select2" style="background-color: white" id="variant">';
                                            foreach ($data['sizes'] as $size){
                                                echo '<option value="'.$size->id.'">'.htmlspecialchars($size->name).'</option>';
                                            }
                                        echo '</select>
                                    </td>
                                    <td style="padding: 10px; width: 225px;">
                                        <label class="control-label">Length</label>
                                        <input type="hidden" name="length" id="weight_value_id">
                                        <span id="length_value"></span>
                                        <span id="weight_value"></span>
                                    </td>
                                    <td style="padding: 10px;">
                                        <label class="control-label">Size Difference</label>
                                        <select name="price" class="form-control select2" style="background-color: white" id="price_value">
                                            <option value="">Please select</option>
                                        </select>
                                    </td>
                                    <td style="padding: 10px;">
                                        Display<br>
                                        <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                            <input name="display" type="radio" id="dyes" value="1" />
                                            Yes
                                            <input name="display" type="radio" id="dyes" value="1" />
                                            <span class="checkmark"></span>
                                        </label> <br> 
                                        <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                            <input name="display" type="radio" id="dno" value="0" checked />
                                            NO
                                            <input name="display" type="radio" id="dno" value="0" checked/>
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                    <td style="padding: 10px;">
                                        Manufacture<br>
                                        <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                            <input name="manufacture" type="radio" id="myes" value="1" />
                                            Yes
                                            <input name="manufacture" type="radio" id="myes" value="1" />
                                            <span class="checkmark"></span>
                                        </label> <br> 
                                        <label style="margin-right: 5px; font-size: 12px;" class="radio_container">
                                            <input name="manufacture" type="radio" id="mno" value="0" checked />
                                            NO
                                            <input name="manufacture" type="radio" id="mno" value="0" checked/>
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" style="text-align: right">
                                        <input name="add_variant" type="submit" value="Add Variant" style="float: right; padding:3px 25px;" class="btn btn-primary btn-sm"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <div class="row">
                            <div class="col-lg-12">
                                <button style="margin: 5px;" type="button" class="btn btn-danger btn-xs delete-all" data-url="">Delete</button>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered" id="example">
                            <thead>
                                <tr class="bg-blue">
                                    <th>
                                        <input type="checkbox" id="check_all" style="opacity: 1; top: -15px; position: relative;">
                                    </th>
                                    <th>Sr. No.</th>
                                    <th>Variant Title</th>
                                    <th>Length</th>
                                    <th>Price</th>
                                    <th>Weight</th>
                                    <th>Display</th>
                                    <th>Manufacture</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>';
                                $sr = 1;
                                echo $data['notices'];
                                foreach ($data['variants'] as $var){
                                    $dis = 'No';
                                    $man = 'No';
                                    $var_id = $var->id;
                                    if($var->display){
                                        $dis = 'Yes';
                                    }
                                    if($var->manufacture){
                                        $man = 'Yes';
                                    }
                                    $price = DB::table('multiple_size')->where('id',$var->price)->first();
                                    $title = DB::table('size')->where('id',$var->variant_title)->first();
                                    echo'<tr>
                                        <td>
                                            <input type="checkbox" class="checkbox" data-id="'.$var->id.'" style="opacity: 1;">
                                        </td>
                                        <td>'.$sr.'</td>
                                        <td>'; if($title !== null){ echo $title->name; } echo '</td>
                                        <td>';
                                            $length = DB::table('size_weight')->where('size_id',$var->variant_title)->first();
                                            if($length !== null){
                                                $lunit = DB::table('units')->where('id',$length->lunit)->first();
                                                if($lunit !== null){
                                                    echo $length->length.' - '.$lunit->name.'</br>';
                                                }
                                            }
                                        echo '</td>
                                        <td>'; if($price !== null){ echo c($price->price, 2, true); } echo'</td>
                                        <td>';
                                            $length = DB::table('size_weight')->where('size_id',$var->variant_title)->first();
                                            if($length !== null){
                                                $wunit = DB::table('units')->where('id',$length->wunit)->first();
                                                if($wunit !== null){
                                                    echo $length->weight.' - '.$wunit->name.'</br>';
                                                }
                                            }
                                        echo '</td>';
                                        echo '<td>'.$dis.'</td>
                                        <td>'.$man.'</td>
                                        <td>
                                            <a href="products?add_variants='.$_GET['add_variants'].'&delete='.$var->id.'"><i class="icon-trash"></i></a>
                                            <a onClick="editVariant(\''.$var->id.'\')"><i class="icon-pencil"></i></a>
                                        </td>
                                    </tr>';
                                $sr++;}
                            echo '</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>';
}elseif(isset($_GET['add_discount'])){
    echo '<div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Discount for <b>'.$data['product']->title.'</b></h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="products" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
                    <form action="products?add_discount='.$_GET['add_discount'].'" method="post" class="form-horizontal" enctype="multipart/form-data" style="max-width: 100%;">
                        '.csrf_field().'
                        <div class="row">
                            <div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
                                <label class="control-label">User Type</label>
                                <select id="user_type" name="user_type" class="form-control" required="required">
                                    <option value="">Select User Type </option>
                                    <option value="individual"> Individual Buyer </option>
                                    <option value="institutional"> Institutional Buyer</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2" style="padding-left: 30px;padding-right:30px;">
                                <label class="control-label">Quantity</label>
                                <input name="quantity" type="text" class="form-control" required="required"/>
                            </div>
                            <div class="form-group col-md-2" style="padding-left: 30px;padding-right:30px;">        
                                <label class="control-label">Discount</label>
                                <input name="discount" type="text" class="form-control" required="required"/>
                            </div>
                            <div class="form-group col-md-3" style="padding-left: 30px;padding-right:30px;">
                                <label class="control-label">Discount Type</label>
                                <select name="discount_type" class="form-control" required="required">
                                    <option value="">Select Discount Type </option>
                                    <option value="%"> % </option>
                                    <option value="Flat"> Flat </option>
                                </select>
                            </div>
                            <input name="add_discount" type="submit" value="Add Discount" style="padding:3px 25px;" class="btn btn-primary col-md-2" />
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable2">
                            <thead>
                                <tr class="bg-blue">
                                    <th>Sr. No.</th>
                                    <th>Type</th>
                                    <th>Quantity</th>
                                    <th>Discount</th>
                                    <th>Discount Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>';
                                $sr = 1;
                                echo $data['notices'];
                                foreach ($data['discounts_individual'] as $var){
                                    echo'<tr>
                                        <td>'.$sr.'</td>
                                        <td>'.$var->user_type.'</td>
                                        <td>'.$var->quantity.'</td>
                                        <td>'.$var->discount.'</td>
                                        <td>'.$var->discount_type.'</td>
                                        <td>
                                            <a href="products?add_discounts='.$_GET['add_discount'].'&delete='.$var->id.'"><i class="icon-trash"></i></a>
                                            <a onClick="editDiscount(\''.$var->id.'\')"><i class="icon-pencil"></i></a>
                                        </td>
                                    </tr>';
                                $sr++;}
                            echo '</tbody>
                        </table>
                        <table class="table table-striped table-bordered" id="datatable-editable4">
                            <thead>
                                <tr class="bg-blue">
                                    <th>Sr. No.</th>
                                    <th>Type</th>
                                    <th>Quantity</th>
                                    <th>Discount</th>
                                    <th>Discount Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>';
                            $sr = 1;
                            if(isset($notices)){
                                echo $notices;
                            }
                            foreach ($data['discounts_institutional'] as $var){
                                echo '<tr>
                                    <td>'.$sr.'</td>
                                    <td>'.$var->user_type.'</td>
                                    <td>'.$var->quantity.'</td>
                                    <td>'.$var->discount.'</td>
                                    <td>'.$var->discount_type.'</td>
                                    <td>
                                        <a href="products?add_discounts='.$_GET['add_discount'].'&delete='.$var->id.'"><i class="icon-trash"></i></a>
                                        <a href="javascript:void(0);" onClick="editDiscount('.$var->id.')"><i class="icon-pencil"></i></a>
                                    </td>
                                </tr>';
                            $sr++;}
                            echo '</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>';
}else {
echo '<div class="row" style="margin-top: 13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Products<a href="products?add" style="padding:3px 15px; font-size: 15px;" class="add">Add Products</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your Products</h5>
                <div class="divider"></div>
                <div class="row" style="position:relative;">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable3">
                            <thead>
                                <tr class="bg-blue">
                                    <th>Sr. No.</th>
                                    <th>Product Image</th>
                                    <th>Product Name</th>
                                    <th>Market Price</th>
                                    <th>Sale Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>';
                                $sr = 1;
                                echo $data['notices'];
                                foreach ($data['products'] as $product){
                                    echo '<tr>
                                        <td>'.$sr.'</td>
                                        <td><img src="../assets/products/'.image_order($product->images).'" style="height: 100px; width: 100px;"></td>
                                        <td>'.$product->title.'</td>
                                        <td>'.c($product->price).'</td>
                                        <td>'.c($product->sale).'</td>
                                        <td>
                                            <a href="products?delete='.$product->id.'"><i class="icon-trash"></i></a>
                                            <a href="products?edit='.$product->id.'"><i class="icon-pencil"></i></a>
                                            <a data-toggle="tooltip" title="Add Variants" href="products?add_variants='.$product->id.'"><i class="icon-plus"></i></a>
                                            <a data-toggle="tooltip" title="Bulk Discount" href="products?add_discount='.$product->id.'"><i class="icon-present"></i></a>
                                        </td>
                                    </tr>';
                                $sr++;}
                            echo '</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
}
?>

    <style>
        .button {
            background: gainsboro;
            padding: 5px 20px;
            cursor: pointer;
            border-radius: 30px;
        }
        .mini-button {
            cursor: pointer;
            border-radius: 30px;
            background: transparent;
            padding: 5px 0px;
            display: block;
        }
        .fade:not(.show) {
            opacity: 1;
            background-color: transparent;
        }
        .modal {
            width:100%;
            max-height: 90%;
        }
    </style>
    <script src="<?=$data['tp']; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
    <script src="<?=$data['tp']; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
    <script src="<?=$data['tp']; ?>/admin/js/plugins.js" type="text/javascript"></script>
    <script src="<?=$data['tp']; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
    <script src="<?=$data['tp']; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
    <script src="<?=$data['tp']; ?>/admin/js/select2.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="<?=$data['tp']; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script src="<?=$data['tp']; ?>/admin/js/bootstrap-datetimepicker.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#check_all').on('click', function(e) {
                if($(this).is(':checked',true)){
                    $(".checkbox").prop('checked', true);
                } else {
                    $(".checkbox").prop('checked',false);
                }
            });
            $('.checkbox').on('click',function(){
                if($('.checkbox:checked').length == $('.checkbox').length){
                    $('#check_all').prop('checked',true);
                }else{
                    $('#check_all').prop('checked',false);
                }
            });
            $('.delete-all').on('click', function(e) {
                var idsArr = [];
                $(".checkbox:checked").each(function() {
                    idsArr.push($(this).attr('data-id'));
                });
                if(idsArr.length <=0){
                    alert("Please select atleast one record to delete.");
                }  else {
                    if(confirm("Are you sure, you want to delete the selected variants?")){
                        var strIds = idsArr.join(",");
                        var token = "{{ csrf_token() }}";
                        $.ajax({
                            url: 'variantMultipleDelete',
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: 'ids='+strIds+'&_token=<?=csrf_token(); ?>',
                            success: function (data) {
                                if (data['status']==true) {
                                    $(".checkbox:checked").each(function() {
                                        $(this).parents("tr").remove();
                                    });
                                    alert(data['message']);
                                    window.location.reload();
                                } else {
                                    alert('Whoops Something went wrong!!');
                                }
                            },
                            error: function (data) {
                                alert(data.responseText);
                            }
                        });
                    }
                }
            });
        });

        $( document ).ready(function() {
            $('.select2').select2();
        });

        $('.date').datetimepicker({
            defaultDate: new Date(),
            format: 'YYYY-MM-DD H:mm:ss',
            sideBySide: true
        });

        function option_count(type){
            var count = $('#option_count').val();
            if(type == 'add'){
                count++;
            }
            if(type == 'reduce'){
                count--;
            }
            $('#option_count').val(count);
        }

        $("#add_option").click(function(){
            option_count('add');
            var co = $('#option_count').val();
            $("#options").append(''
                +'<div class="form-group" data-no="'+co+'">'
                +'    <div class="input-field col s12 m6">'
                +'        <input type="text" name="option_title[]" class="form-control"  placeholder="Title">'
                +'    </div>'
                +'    <div class="input-field col s12 m5">'
                +'        <select class="form-control option_type" name="option_type[]" >'
                +'            <option value="text">Text input</option>'
                +'            <option value="select">Dropdown - single select</option>'
                +'            <option value="multi_select">Checkbox - multi select</option>'
                +'            <option value="radio">Radio</option>'
                +'        </select>'
                +'        <div class="options">'
                +'          <input type="hidden" name="option_set'+co+'[]" value="none" >'
                +'        </div>'
                +'    </div>'
                +'    <input type="hidden" name="option_no[]" value="'+co+'" >'
                +'    <div class="input-field col s12 m1">'
                +'        <span class="remove mini-button"><i class="icon-close"></i></span>'
                +'    </div>'
                +'</div>'
            );
        });

        $("#options").on('change','.option_type',function(){
            var co = $(this).closest('.form-group').data('no');
            if($(this).val() !== 'text'){
                $(this).closest('div').find(".options").html('<div class="pull-right button add_option">Add option</div>');
            } else {
                $(this).closest('div').find(".options").html('<input type="hidden" name="option_set'+co+'[]" value="none" ><input type="hidden" name="option_set'+co+'[]" value="none" >');
            }
        });

        // abhijeet code start
        $('#datatable-editable2').hide();
        $('#datatable-editable4').hide();
        $(document).on('change','#user_type',function(){
            var type = $(this).val();

            if(type=='individual'){
                $('#datatable-editable2').show();
                $('#datatable-editable4').hide();
            }else if(type=='institutional'){
                $('#datatable-editable2').hide();
                $('#datatable-editable4').show();
            }
        });

        // abhijeet code end

        $("#options").on('click','.add_option',function(){
            var co = $(this).closest('.form-group').data('no');
            $(this).closest('.options').prepend(''
                +'    <div style="margin: 10px -15px;">'
                +'        <div class="col-sm-5">'
                +'          <input type="text" name="option_set'+co+'[]" class="form-control required"  placeholder="Option">'
                +'        </div>'
                +'        <div class="col-sm-5">'
                +'          <input type="text" name="option_set'+co+'[]" class="form-control required"  placeholder="Price">'
                +'        </div>'
                +'        <div class="col-sm-1">'
                +'          <span class="remove-option mini-button"><i class="icon-close"></i></span>'
                +'        </div>'
                +'        <div class="clearfix"></div>'
                +'    </div>'
            );
        });

        $('body').on('click', '.remove', function(){
            $(this).parent().parent().remove();
        });

        $('body').on('click', '.remove-option', function(){
            var co = $(this).closest('.form-group').data('no');
            $(this).parent().parent().remove();
            if($(this).parent().parent().parent().html() == ''){
                $(this).parent().parent().parent().html(''
                    +'   <input type="hidden" name="option_set'+co+'[]" value="none" >'
                );
            }
        });
        function editVariant(vid){
            var ret_id = "<?=isset($_GET['add_variants']) ? $_GET['add_variants'] : 0;?>";
            $.ajax({
                url: 'get-variant',
                type: 'post',
                data: 'id='+vid+'&_token=<?=csrf_token(); ?>&ret_id='+ret_id,
                success: function(datax){
                    $("#edit-data").html(datax);
                    $('#edit-button').trigger('click');
                    $(document).ready(function() {
                        var max_fields      = 10;
                        var wrapper   		= $(".input_fields_wrap1");
                        var add_button      = $(".add_field_button1");
                        var x = 1;
                        $(add_button).click(function(e){
                            e.preventDefault();
                            if(x < max_fields){
                                x++;
                                $(wrapper).append('<div><input type="text" name="length[]"/><a href="#" class="remove_field1">Remove</a></div>'); //add input box
                            }
                        });
                        $(wrapper).on("click",".remove_field1", function(e){ //user click on remove text
                            e.preventDefault(); $(this).parent('div').remove(); x--;
                        })
                    });
                }
            });
        }

        function editDiscount(vid){
            $.ajax({
                url: 'get-discount',
                type: 'post',
                data: 'id='+vid+'&_token=<?=csrf_token(); ?>',
                success: function(datax){
                    var data = JSON.parse(datax);
                    $('#modal-form input[name=dis_id]').val(data.id);
                    $('#modal-form input[name=quantity]').val(data.quantity);
                    $('#modal-form input[name=discount]').val(data.discount);
                    $('#modal-form input[name=discount_type]').val(data.discount_type);
                    if(data.discount_type == '%'){
                        $('#modal-form .discount_type').html('<label class="control-label">Discount Type</label>'
                            +'<select name="discount_type" class="form-control" required="required">'
                            +'	<option value=""> Please Select Discount Type </option>'
                            +'	<option value="%" selected="selected"> % </option>'
                            +'	<option value="Flat"> Flat </option>'
                            +'</select>');
                    }else{
                        $('#modal-form .discount_type').html('<label class="control-label">Discount Type</label>'
                            +'<select name="discount_type" class="form-control" required="required">'
                            +'	<option value=""> Please Select Discount Type </option>'
                            +'	<option value="%"> % </option>'
                            +'	<option value="Flat" selected="selected"> Flat </option>'
                            +'</select>');
                    }
                    $('#edit-dis-button').trigger('click');
                }
            });
        }
    </script>
    <?php
        echo $data['footer'];
    ?>
    <?php if(isset($_GET['add_variants'])){ ?>
        <button type="hidden" data-toggle="modal" data-target="#edit-variant" id="edit-button"></button>
        <div class="modal fade in" id="edit-variant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="margin-top: 165px;">
                <div style="width:100%; height:100%;" class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Variant</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -35px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="edit-data"></div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#variant').on('change', function(){
                    var id = $(this).val();
                    $.ajax({
                        type: 'post',
                        url: 'get-variant-data',
                        data: {_token: '<?=csrf_token()?>', id: id},
                        success: function(data){
                            $('input[name=length1]').val(data.length1);
                            $('input[name=length2]').val(data.length2);
                            $('input[name=length3]').val(data.length3);
                            $('input[name=length4]').val(data.length4);
                            $('#price_value').html(data.price);
                            $('#length_value').html(data.length_value);
                            $('#weight_value').html(data.weight_value);
                            $('#weight_value_id').val(data.weight_value_id);
                            $("#price_value").select2('refresh');
                            $('input[name=weight]').val(data.weight);
                            $('input[name=light]').val(data.light);
                            $('input[name=super_light]').val(data.super_light);
                            $('select[name=lunit] option[value='+data.lunit+']').prop('selected', true);
                            $('select[name=wunit] option[value='+data.wunit+']').prop('selected', true);
                            if(data.w_available == 'available') {
                                $('input[name=w_available]').prop('checked', true);
                            }
                            if(data.l_available == 'available') {
                                $('input[name=l_available]').prop('checked', true);
                            }
                            if(data.s_available == 'available') {
                                $('input[name=s_available]').prop('checked', true);
                            }
                        },
                        error: function(){
                            alert('Oops! Something went wrong. Try again.');
                        }
                    })
                });
            });
        </script>
    <?php } ?>
    <?php if(isset($_GET['add_discount'])){ ?>
        <button type="hidden" data-toggle="modal" data-target="#edit-discount" id="edit-dis-button"></button>
        <div class="modal fade" id="edit-discount" tabindex="-1" role="dialog" aria-labelledby="editDiscount" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Discount</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -35px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="products?add_discount=<?=$_GET['add_discount'];?>" method="post" class="form-horizontal" id="modal-form" enctype="multipart/form-data" style="max-width: 100%;padding: 0;">
                            <?php echo csrf_field(); ?>
                            <input type="hidden" name="dis_id" value=""/>
                            <input type="hidden" name="dis_id" value=""/>
                            <fieldset>
                                <div class="form-group col-md-12" style="padding-left: 30px;padding-right: 30px;">
                                    <label class="control-label">Quantity</label>
                                    <input name="quantity" type="text" class="form-control" required="required"/>
                                </div>
                                <div class="form-group col-md-12" style="padding-left: 30px;padding-right: 30px;">
                                    <label class="control-label">Discount</label>
                                    <input name="discount" type="text" class="form-control" required="required"/>
                                </div>
                                <div class="form-group col-md-12 discount_type" style="padding-left: 30px;padding-right: 30px;" >
                                    <label class="control-label">Discount Type</label>
                                    <select name="discount_type" class="form-control" required="required">
                                        <option value=""> Please Select Discount Type </option>
                                        <option value="%"> % </option>
                                        <option value="Flat"> Flat </option>
                                    </select>
                                </div>
                                <center>
                                    <input name="edit_discount" type="submit" value="Edit Discount" class="btn btn-primary"/>
                                <center>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <script>
        $( document ).ready(function() {
            var editor_config = {
                path_absolute : "<?php echo URL::to('/admin') ?>/",
                selector: "textarea.editor",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table  directionality",
                    "emoticons template paste textpattern"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                templates: [
                    {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
                    {title: 'Some title 2', description: 'Some desc 2', url: 'http://162.144.132.86/~adroweb/'}
                ],
                relative_urls: false,
            };
            tinymce.init(editor_config);
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('.select2').select2({
                placeholder: "Select sizes",
                allowClear: true
            });
            var editor_config1 = {
                path_absolute : "<?php echo URL::to('/admin') ?>/",
                selector: "textarea.editor1",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table directionality",
                    "emoticons template paste textpattern"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                templates: [
                    {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
                    {title: 'Some title 2', description: 'Some desc 2', url: 'http://162.144.132.86/~adroweb/'}
                ],
                relative_urls: false,
            };
            tinymce.init(editor_config1);
        });

        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><input type="text" name="length[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
                }
            });
            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            });
            $('#input_starttime').datetimepicker({format: 'LT'});
        });

        $('#getgroup').change(function (){
            var id = $(this).val();
            $.ajax({
                url: 'get_sizes_group',
                type: 'post',
                data: 'id='+id+'&_token=<?=csrf_token(); ?>',
                success: function(data){
                    $('#showGroupData').html('');
                    $('#showGroupData').html(data);
                    var no_of_rows = $('#showGroupData').find('tr').length;
                    var no_of_rows1 = no_of_rows - 1;
                    alert("Total:" + no_of_rows1);
                }
            });
        });
        $(document).on('click', '.addNew', function(){
            var ele = $(this);
            var newItem = $('.phitem:first').clone();
            var pid = $('.phitem:first').find('.hubs').val();
            if(pid != '') {
                var pval = $('.phitem:first .proDiv').find('.hubs option:selected').text();
                var proField = '<input type="hidden" value="' + pid + '" name="hubs[]"/>';
                var proFieldLabel = '<label class="control-label">Hub</label>';
                var proFieldView = '<input type="text" readonly value="' + pval + '" class="form-control"/>';
                $('.phitems').append(newItem);
                $('.phitem:last').find('.addNew').val($('.phitem').length).removeClass('btn-primary').removeClass('addNew').addClass('btn-danger').addClass('removeItem').html('-');
                $('.phitem:last').find('.proDiv').html(proField + proFieldLabel + proFieldView);
                $('.phitem:last').find('.pprice').attr('name', 'pprice[]');
                $('.phitem:last').find('.price').attr('name', 'iprice[]');
                $('.phitem:last').find('.ins_quantity').attr('name', 'ins_quantity[]');
                $('.phitem:last').find('.ins_validity').attr('name', 'ins_validity[]');
                $('.phitem:last').find('.lprice').attr('name', 'lprice[]');
                $('.phitem:last').addClass('color2');
            }else{
                alert('You must select a Hub to Add.');
            }
        });

        $(document).on('click', '.removeItem', function(){
            $(this).parent().parent().remove();
        });

        $(document).ready( function () {
            $('#myTable').DataTable();
        } );

        function updateDataTableSelectAllCtrl(table){
            var $table             = table.table().node();
            var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
            var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
            var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);
            if($chkbox_checked.length === 0){
                chkbox_select_all.checked = false;
                if('indeterminate' in chkbox_select_all){
                    chkbox_select_all.indeterminate = false;
                }
            } else if ($chkbox_checked.length === $chkbox_all.length){
                chkbox_select_all.checked = true;
                if('indeterminate' in chkbox_select_all){
                    chkbox_select_all.indeterminate = false;
                }
            } else {
                chkbox_select_all.checked = true;
                if('indeterminate' in chkbox_select_all){
                    chkbox_select_all.indeterminate = true;
                }
            }
        }

        $(document).ready(function (){
            var rows_selected = [];
            var table = $('#example').DataTable({
                'order': [[1, 'asc']],
                'rowCallback': function(row, data, dataIndex){
                    var rowId = data[0];
                    if($.inArray(rowId, rows_selected) !== -1){
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
                    }
                }
            });

            // Handle click on checkbox
            $('#example tbody').on('click', 'input[type="checkbox"]', function(e){
                var $row = $(this).closest('tr');
                var data = table.row($row).data();
                var rowId = data[0];
                var index = $.inArray(rowId, rows_selected);
                if(this.checked && index === -1){
                    rows_selected.push(rowId);
                } else if (!this.checked && index !== -1){
                    rows_selected.splice(index, 1);
                }
                if(this.checked){
                    $row.addClass('selected');
                } else {
                    $row.removeClass('selected');
                }
                updateDataTableSelectAllCtrl(table);
                e.stopPropagation();
            });
            $('#example').on('click', 'tbody td, thead th:first-child', function(e){
                $(this).parent().find('input[type="checkbox"]').trigger('click');
            });
            $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
                if(this.checked){
                    $('#example tbody input[type="checkbox"]:not(:checked)').trigger('click');
                } else {
                    $('#example tbody input[type="checkbox"]:checked').trigger('click');
                }
                e.stopPropagation();
            });
            table.on('draw', function(){
                updateDataTableSelectAllCtrl(table);
            });

            $('#frm-example').on('submit', function(e){
                var form = this;
                $.each(rows_selected, function(index, rowId){
                    $(form).append(
                        $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'id[]')
                        .val(rowId)
                    );
                });
            });
        });

//        var $insertmoreBefore = $('#insertmoreBefore');
//        var $i = 0;
//        // Add More Inputs
//        $('#moreButton').click(function(){
//            $i = $i+1;
//            var indexs = $i+1;
//            //append Options
//            $('<div class="row"><div class="col cwitems">'
//                +'<div class="cwitem" id="addMoreBox'+indexs+'">'
//                +   '<div class="form-group col-md-2 cwproDiv" style="margin: 0 0 10px 0">'
//                +       'Cities'
//                +       '<select class="form-control select2 cities" id="cities" name="cities[]">'
//                +           '<option value="">Select City</option>'
//                +           '<?php //foreach ($data['cities'] as $hub){ ?>//'
//                +               '<option value="<?//=$hub->name; ?>//"><?//=$hub->name; ?>//</option>'
//                +           '<?php //} ?>//'
//                +       '</select>'
//                +   '</div>'
//                +   '<div class="form-group col-md-2" style="margin: 0 0 10px 0">'
//                +       'Purchase Price'
//                +       '<input type="text" value="0" class="form-control cwpprice" name="cwpprice[]"/>'
//                +   '</div>'
//                +   '<div class="form-group col-md-2" style="margin: 0 0 10px 0">'
//                +       'Price'
//                +       '<input type="text" value="0" class="form-control cwprice" name="cwprice[]"/>'
//                +   '</div>'
//                +   '<div class="form-group col-md-2" style="margin: 0 0 10px 0">'
//                +       'Quantity'
//                +       '<input type="text" value="0" class="form-control cwins_quantity" name="cwins_quantity[]"/>'
//                +   '</div>'
//                +   '<div class="form-group col-md-2" style="margin: 0 0 10px 0">'
//                +       'Validity'
//                +       '<div class="input-group date" id="datetimepicker1">'
//                +           '<input type="text" value="" class="form-control cwins_validity" name="cwins_validity[]">'
//                +           '<span class="input-group-addon" style="position: relative;">'
//                +               '<span class="glyphicon glyphicon-calendar"></span>'
//                +           '</span>'
//                +       '</div>'
//                +   '</div>'
//                +   '<div class="form-group col-md-1" style="margin: 0 0 10px 0">'
//                +       'Loading'
//                +       '<input type="text" value="0" class="form-control cwlprice" name="cwlprice"/>'
//                +   '</div>'
//                +   '<div class="form-group col-md-1" style="margin: 0 0 10px 0">'
//                +       '<button type="button" onclick="removeBox('+indexs+')" class="btn btn-sm btn-danger">-<i class="fa fa-times"></i></button>'
//                +   '</div>'
//                +   '</div></div></div>').insertBefore($insertmoreBefore);
//            $('#cities').select2('refresh');
//        });
//        // Remove fields
//        function removemoreBox(index){
//            $('#addMore'+index).remove();
//        }

        $(document).on('click', '.cwaddNew', function(){
            var ele = $(this);
            var newItem = $('.cwitem:first').clone();
            var pid = $('.cwitem:first').find('.cities').val();
            if(pid != '') {
                var pval = $('.cwitem:first .cwproDiv').find('.cities option:selected').text();
                var proField = '<input type="hidden" value="' + pid + '" name="cities[]"/>';
                var proFieldLabel = '<label class="control-label">City</label>';
                var proFieldView = '<input type="text" readonly value="' + pval + '" class="form-control"/>';
                $('.cwitems').append(newItem);
                $('.cwitem:last').find('.cwaddNew').val($('.cwitem').length).removeClass('btn-primary').removeClass('cwaddNew').addClass('btn-danger').addClass('cwremoveItem').html('-');
                $('.cwitem:last').find('.cwproDiv').html(proField + proFieldLabel + proFieldView);
                $('.cwitem:last').find('.cwpprice').attr('name', 'cwpprice[]');
                $('.cwitem:last').find('.cwprice').attr('name', 'cwprice[]');
                $('.cwitem:last').find('.cwins_quantity').attr('name', 'cwins_quantity[]');
                $('.cwitem:last').find('.ins_validity').attr('name', 'cwins_validity[]');
                $('.cwitem:last').find('.cwlprice').attr('name', 'cwlprice[]');
                $('.cwitem:last').addClass('color2');
            }else{
                alert('You must select a city to Add.');
            }
        });
        $(document).on('click', '.cwremoveItem', function(){
            $(this).parent().parent().remove();
        });
</script>
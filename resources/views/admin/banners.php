<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    <!--    <title>-->
    <!--        --><?//=$title; ?>
    <!--    </title>-->
    <link rel="apple-touch-icon" href="<?=$tp; ?>/admin/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=$tp; ?>/admin/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/flag-icon/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/vendors/data-tables/css/select.dataTables.min.css">

    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/themes/vertical-modern-menu-template/style.css">
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/pages/data-tables.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=$tp; ?>/admin/css/custom/custom.css">
    <link href="<?=$tp; ?>/admin/css/select2.min.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>
<style type="text/css">

    .modal-window {
        position: fixed;
        background-color: rgba(200, 200, 200, 0.75);
        top: -70px;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999;
        opacity: 0;
        pointer-events: none;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        transition: all 0.3s;
    }

    .modal-window:target {
        opacity: 1;
        pointer-events: auto;
    }

    .modal-window>div {
        width: 800px;
        position: relative;
        margin: 10% auto;
        padding: 2rem;
        background: #fff;
        color: #444;
    }

    .modal-window header {
        font-weight: bold;
    }

    .modal-close {
        color: #aaa;
        line-height: 50px;
        font-size: 80%;
        position: absolute;
        right: 0;
        text-align: center;
        top: 0;
        width: 70px;
        text-decoration: none;
    }

    .modal-close:hover {
        color: #000;
    }

    .modal-window h1 {
        font-size: 150%;
        margin: 0 0 15px;
    }
</style>

<?php
echo $header;
if(isset($_GET['add'])) {	
	echo $notices.'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
            <h5>Add Banners</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="banners" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Title
						<input name="title" type="text"  class="form-control"/>
					  </div>
					  <div class="input-field col s12 m6">
						Short Description
						<input name="short_description" type="text" class="form-group"/>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
						Banner image
						<input name="image" type="file" class="form-control" required="required" />
					  </div>
					  <div class="input-field col s12 m6">
						Priority
						<input name="priority" type="text" class="form-control"  required="required"/>
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m6">
                        Parent category
                        <select name="section" class="form-control">
							<option value="home">Common Home</option>
							<option value="institutional_home">Institutional Home</option>
					    </select>
					  </div>
					  <div class="row">
					  <input name="add" type="submit" value="Add banner" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {
	echo $notices.'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 650px;">
              <h5>Edit Banners</h5>
                    <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                        <a class="btn myblue waves-light " style="padding:0 5px;" href="banners" >
                            <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                        </a>
                    </div>
         <form action="" method="post"  enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Banner title
						<input name="title" type="text"  value="'.$banner->title.'" class="form-control"/>
					  </div>
					  <div class="input-field col s12 m6">
						Short Description
						<input name="short_description" type="text" value="'.$banner->short_description.'" class="form-control"/>
					  </div>
					  </div><div class="row">';
					  if (!empty($banner->image)){

							  echo '<p>Uploading new images will overwrtite current images .</p>';

									echo '<img class="col-md-2" src="'.url('/assets/banners/'.$banner->image).'" required="required"/>';

							  echo '<div class="clearfix"></div>';
						  }

						  if (!empty($banner->download)){
							  echo '<p>Uploading new file will overwrite current file .</p>';
						  }
						  echo '
                        
					  <div class="input-field col s12 m6">
						Image
						<input type="file" class="form-control" name="image"/>
					  </div>
					  </div>
					  <div class="row">
					 <div class="input-field col s12 m6">
						Priority
						<input name="priority" type="text" value="'.$banner->priority.'" class="form-control"  />
					  </div>
					  
					  
					  <div class="input-field col s12 m6">
                        Parent category
                        <select name="section" class="form-control">
							<option '.(($banner->section == "home")? "selected" : "").' value="home">Common Home</option>
							<option '.(($banner->section == "institutional_home")? "selected" : "").' value="institutional_home">Institutional Home</option>
					    </select>
					  </div>
					  </div>
					   <div class="row">
					  <input name="edit" type="submit" value="Edit banner" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
?>
<div class="row" style="margin-top: 5px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h3>Banners<a href="banners?add" style="padding:3px 15px; font-size: 15px;"  class="add">Add Banners</a></h3>
                <h5 class="card-title" style="font-size: 20px;">Manage Banners</h5>

                <div class="divider"></div>
                <div class="row" style="position:relative;">
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatable-editable">
            <thead>
            <tr class="bg-blue">
                <th>Sr. No.</th>
                <th>Image</th>
                <th>Title</th>
                <th>Short Description</th>
                <th>Priority</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
<?php
    $sr = 1;
	echo $notices;
	foreach ($banners as $category){
		echo'<tr>
            <td>'.$sr.'</td>
			<td><img src="../assets/banners/'.$category->image.'" style="height: auto; width: 100px;"></td>
            <td>'.$category->title.'</td>
			<td>'.$category->short_description.'</td>
			<td>'.$category->priority.'</td>
			<td>
                <a href="banners?delete='.$category->id.'"><i class="icon-trash"></i></a>
				<a href="banners?edit='.$category->id.'"><i class="icon-pencil"></i></a>
            </td>
          </tr>';
	$sr++; }
} ?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
        </div>
    </div>
</div>
<?php
echo $footer;
?>
<script src="<?=$tp; ?>/admin/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/cr-1.5.2/fh-3.1.6/datatables.min.js"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?=$tp; ?>/admin/vendors/noUiSlider/nouislider.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?=$tp; ?>/admin/js/plugins.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?=$tp; ?>/admin/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->

<!-- END PAGE LEVEL JS-->
<script src="<?=$tp; ?>/admin/js/select2.min.js"></script>
<script src="<?=$tp; ?>/admin/js/scripts/ui-alerts.js" type="text/javascript"></script>
<!-- <script src="<?php //$tp; ?>/js/scripts/data-tables.js" type="text/javascript"></script> -->
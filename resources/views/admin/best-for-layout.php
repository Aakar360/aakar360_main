<?php
echo $header;
if(isset($_GET['add'])) {
    echo $notices.'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Best Layout</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="best-for-layout" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
				        <div class="input-field col s12 m4">
						Title
						<input name="title" type="text"  class="form-control" />
					  </div>
					  <div class="input-field col s12 m4">
						Link
						<input name="link" type="text"  class="form-control" />
					  </div>
					  
					  <div class="input-field col s12 m4">
						Image
						<input name="image" type="file"  class="form-control" />
					  </div>
					  </div>
					  <div class="row">
					  <div class="input-field col s12 m12">
						Content
						<textarea name="content" class="form-control" rows="10" cols="80" required></textarea>
					  </div>
					  
					  
					  </div>
					  <input name="add" type="submit" value="Submit" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {

    echo $notices.'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit Best Layout</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="best-for-layout" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%;">
			'.csrf_field().'
			
				<div class="row">
				    <div class="input-field col s12 m6">
						Title
						<input name="title" type="text" value="'.$bestforcat->title.'" class="form-control" />
					  </div>
					  <div class="input-field col s12 m6">
						Link
						<input name="link" type="text"  value="'.$bestforcat->link.'" class="form-control" />
					  </div>
					  </div>
					  <div class="row" style="margin-bottom: 10px;">';
					  if (!empty($bestforcat->image)){

							  echo '<p>Uploading new images will overwrtite current images .</p>';

									echo '<img class="col-md-2" src="'.url('/assets/products/'.$bestforcat->image).'" />';

							  echo '<div class="clearfix"></div>';
						  }
						  echo '
					  
					  <div class="input-field col s12 m6">
						Image
						<input name="image" type="file"  class="form-control" />
					  </div>
					  </div>
					  <div class="row" style="margin-bottom: 10px;">
					  <div class="input-field col s12 m12">
						Content
						<textarea name="content" class="form-control" rows="10" cols="80" required>'.$bestforcat->content.'</textarea>
					  </div></div>
					  <input name="edit" type="submit" value="Update" style="padding:3px 25px;" class="btn btn-primary" />
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
    ?>
<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
                <h5>Best For layout <a href="best-for-layout?add" style="padding:3px 15px;" class="add">Add Layout</a></h5>
                <h5 class="card-title" style="color: #0d1baa;">Manage Your layout</h5>
        <div class="table-responsive">
            <table class="table table-striped table-bordered" id="datatable-editable">
                <thead>
                <tr class="bg-blue">
                    <th>Sr. No.</th>
                    <th>Link</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sr = 1;
                echo $notices;
                foreach ($bestforcats as $category){
                    echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'.$category->link.'</td>
                        <td>'.$category->title.'</td>
                        <td>'.$category->content.'</td>
                        <td><img src="../assets/products/'.image_order($category->image).'" style="height: auto; width: 100px;"></td>
                        
            
                        <td>
                            <a href="best-for-layout?delete='.$category->id.'"><i class="icon-trash"></i></a>
                            <a href="best-for-layout?edit='.$category->id.'"><i class="icon-pencil"></i></a>
                        </td>
                      </tr>
                      ';
                    $sr++;
                }?>
                </tbody>
            </table>
        </div>
    </div>
</div>
    <?php
}
echo $footer;
?>
<script>
function removeThis($id){
	event.preventDefault();
	$('[data='+$id+']').remove();
}
$('#add-option').on('click', function(){
	var id = $('#que-type > div').length;
	var html = '<div class="col-md-12 options" data="'+id+'"><label class="control-label col-md-12" data="'+id+'">Option '+(id+1)+'</label><input name="option[]" type="text" data="'+id+'" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" /><a onClick="removeThis('+id+');" class="pul-right" style="line-height: 3;" data="'+id+'"><i class="icon-close" style="font-size: 20px;"></i></a></div>';
	$('#add-option').before(html);
});
function getData(data){
	if(data == 'text' || data == 'textarea'){
		$('.options').hide();
	}else{
		$('.options').show();
	}	
}


function addit(val){
    var da = val;
    $.ajax({
        url: 'catData',
        type: 'post',
        data: 'page='+da+'&_token=<?=csrf_token(); ?>',
        success: function(data){
            $('#catdata').html(data);
        }
    });
}

</script>
<script>
    $( document ).ready(function() {
        CKEDITOR.replace( 'editor' );
        CKEDITOR.replace( 'editor1' );
    });
</script>
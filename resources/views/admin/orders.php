<?php
echo $data['header'];
if (isset($_GET['details'])){
    echo $data['notices'];
    echo '<br/>
    <div class="col-md-8">
		<div class="list">
            <div class="title">
                <i class="icon-user"></i>Customer details
            </div>
            <div class="item order"><h6><b>Order ID</b> : #'.$data['order']->id.'</h6></div>
            <div class="item order"><h6><b>Order time</b> : '.date('l jS \of F Y H:i:s',$data['order']->time).'</h6></div>
		';
        if(isset($data['fields'])) {
            foreach ($data['fields'] as $field) {
                $code = $field->code;
                if ($code == 'country') {
                    $data['order']->$code = country($data['order']->$code);
                }
                if ($code != 'address') {
                    echo '<div class="item order"><h6><b>' . $field->name . '</b> : ' . $data['order']->$code . '</h6></div>';
                } else {
                    $datas = json_decode($data['order']->$code);
                    if($datas) {
                        echo '<div class="item order"><h6><b>Shipping Address</b> : </h6>
                            <p>';
                                echo '<b>Name : </b>'; if(isset($datas->shipping_fname)) echo $datas->shipping_fname . '<br>';
                                echo '<b>Company : </b>'; if(isset($datas->shipping_company)) echo $datas->shipping_company . '<br>';
                                echo '<b>Address Line 1 : </b>'; if(isset($datas->shipping_address_line_1)) echo $datas->shipping_address_line_1 . '<br>';
                                echo '<b>Address Line 2 : </b>'; if(isset($datas->shipping_address_line_2)) echo $datas->shipping_address_line_2 . '<br>';
                                echo '<b>Country : </b>'; if(isset($datas->shipping_country)) echo getCountryName($datas->shipping_country) . '<br>';
                                echo '<b>State : </b>'; if(isset($datas->shipping_state)) echo getStateName($datas->shipping_state) . '<br>';
                                echo '<b>City : </b>'; if(isset($datas->shipping_city)) echo getCityName($datas->shipping_city) . '<br>';
                                echo '<b>Pincode : </b>'; if(isset($datas->shipping_postcode)) echo $datas->shipping_postcode . '<br>';
                                echo '<b>Contact No. : </b>'; if(isset($datas->shipping_mobile)) echo $datas->shipping_mobile . '<br>';
                                echo '<b>Alternate Contact No. : </b>'; if(isset($datas->shipping_alternate_contact)) echo $datas->shipping_alternate_contact . '<br>';
                            echo '</p>
                        </div>';
                    }
                }
            }
        }
        echo '</div>
		<div class="list">
            <div class="title">
                <i class="icon-basket"></i>Products
            </div>';
            $products_data = $data['order']->products;
            $products = json_decode($products_data, true);
            if(count($products)>0){
                $ids = "";
                foreach($products as $datad){
                    $ids = $ids . $datad['id'] . ",";
                    $q[$datad['id']] = $datad['quantity'];
                    $options_list[$datad['id']] = $datad['options'];
                    $variants[$datad['id']] = $datad['variants'];
                }

                $ids = explode(',',$ids);
                $order_products = \App\Products::whereIn('id',$ids)->orderBy('id','DESC')->get();
                foreach ($order_products as $product){
        //            dd($variants[$product->id]);
                    $options = json_decode($options_list[$product->id],true);
                    $option_array = array();
                    if($options) {
                        foreach ($options as $option) {
                            $option_array[] = '<i>' . $option['title'] . '</i> : ' . $option['value'];
                        }
                    }
                    echo '<div class="item">
                        <h6>'.$product->title.' x '.$q[$product->id].'
                        <b class="pull-right">
                        <strike>'.c($product->price).'</strike> 
                        <span class="price">'.c($product->sale).'</span> 
                        </b>
                        <br/>'.implode('<br/>',$option_array).'<br/>'.$variants[$product->id]. '</div>';
                }
                if (!empty($data['order']->coupon)) {
                    // Check if coupon is valid
                    if (DB::select("SELECT COUNT(code) as count FROM coupons WHERE code = '".$data['order']->coupon."'")[0]->count > 0){
                        $coupon_data = DB::select("SELECT * FROM coupons WHERE code = '".$data['order']->coupon."'")[0];
                        echo '<div class="item order text-right"> Coupon : <b>'.$coupon_data->discount.$coupon_data->type.'</b></div>';
                    }
                }
                if ($data['order']->shipping != 0) {
                    // Check if coupon is valid
                    echo '<div class="item order text-right"> Shipping : <b>'.c($data['order']->shipping).'</b></div>';
                }
                echo '<div class="item order text-right">Total : <b>'.c($data['order']->summ).'</b></div>';
            }
        echo '</div>
		<div class="text-center"><a href="orders?delete='.$data['order']->id.'" class="btn btn-danger">Delete order</a></div>
    </div>
		<div class="col-md-4">
			<div class="list">
                <div class="title">
                    <i class="icon-credit-card"></i>Payment details
                </div>';
                $payment_data = json_decode($data['order']->payment,true);
                if(!empty($payment_data)){
                    echo '<div class="alert alert-warning" style="margin-bottom: 0;">Order unpaid</div>';
                    if ($payment_data['method'] == 'paypal') {
                        echo '<div class="item order"><h6><b>Payment method : </b>PayPal</h6></div>
                                    <div class="item order"><h6><b>Payment status : </b>'.$payment_data['payment_status'].'</h6></div>
                                    <div class="item order"><h6><b>Transcation ID : </b>'.$payment_data['txn_id'].'</h6></div>
                                    <div class="item order"><h6><b>Receiver e-mail : </b>'.$payment_data['receiver_email'].'</h6></div>
                                    <div class="item order"><h6><b>Payer e-mail : </b>'.$payment_data['payer_email'].'</h6></div>
                                    <div class="item order"><h6><b>Payment amount : </b>'.$payment_data['payment_amount'].'</h6></div>
                                    <div class="item order"><h6><b>Payment currency : </b>'.$payment_data['payment_currency'].'</h6></div>';
                    } elseif ($payment_data['method'] == 'stripe') {
                        echo '<div class="item order"><h6><b>Payment method : </b>Stripe</h6></div>
                                    <div class="item order"><h6><b>Charge ID : </b>'.$payment_data['charge_id'].'</h6></div>
                                    <div class="item order"><h6><b>Balance transaction : </b>'.$payment_data['balance_transaction'].'</h6></div>
                                    <div class="item order"><h6><b>Card ID : </b>'.$payment_data['card'].'</h6></div>
                                    <div class="item order"><h6><b>Card brand : </b>'.$payment_data['card_brand'].'</h6></div>
                                    <div class="item order"><h6><b>Last 4 : </b>'.$payment_data['last4'].'</h6></div>
                                    <div class="item order"><h6><b>Expiry month : </b>'.$payment_data['exp_month'].'</h6></div>
                                    <div class="item order"><h6><b>Expiry year : </b>'.$payment_data['exp_year'].'</h6></div>
                                    <div class="item order"><h6><b>Fingerprint : </b>'.$payment_data['fingerprint'].'</h6></div>';
                    } elseif ($payment_data['method'] == 'cash') {
                        echo '<div class="item order"><h6><b>Payment method : </b>Cash on delivery</h6></div>';
                    } elseif ($payment_data['method'] == 'bank') {
                        echo '<div class="item order"><h6><b>Payment method : </b>Bank Transfer</h6></div>';
                    }
                }
            echo '</div>
			<div class="list">
                <div class="title">
                    <i class="icon-badge"></i>Order status
                </div>
			<div class="item order text-center"><br/><h6>'.status($data['order']->stat).'</h6><br/></div>
			<div class="item order">
				<form action="orders?details='.$data['order']->id.'" style="padding: 0px 15px;" method="post" class="form-horizontal">
				'.csrf_field().'
				<div class="form-group">
					<label class="control-label">Change to</label>
					<select name="stat" class="form-control">
						<option value="1">Pending</option>
						<option value="2">Shipped</option>
						<option value="3">Delivered</option>
						<option value="4">Cancelled</option>
					</select>
				</div>
				<input style="margin-top: 10px;" name="save" value="Save" class="btn btn-primary" type="submit">
				</form>
			</div>
        </div>
    </div>';
} else {
    echo '<div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 650px;">
                    <h3>Orders</h3>
		            <p>View and manage your website orders</p>
		        <div>';
                echo $data['notices'];
                foreach ($data['orders'] as $order){
                    echo'<div class="bloc">
                        <h5>'.$order->name.'<div class="tools">
                        <a href="orders?details='.$order->id.'"><i class="icon-eye"></i></a>
                        <a href="orders?delete='.$order->id.'"><i class="icon-trash"></i></a>
                        </div></h5>
                        <p>';
                    if(isset($data['fields'])) {
                        foreach ($data['fields'] as $field) {
                            $code = $field->code;
                            if ($field->code == 'country') {
                                $order->$code = country($order->$code);
                            }
                            if ($code != 'address') {
                                echo '<div class="item order"><h6><b>' . $field->name . '</b> : ' . $order->$code . '</h6></div>';
                            } else {
                                $datas = json_decode($order->$code);
                                if ($datas !== null) {
                                    echo '<div class="item order"><h6><b>Shipping Address</b> : </h6>
                                        <p>';
                                            echo '<b>Name : </b> '; if(isset($datas->shipping_fname)) echo $datas->shipping_fname . '<br>';
                                            echo '<b>Company : </b> '; if(isset($datas->shipping_company)) echo $datas->shipping_company . '<br>';
                                            echo '<b>Address Line 1 : </b>'; if(isset($datas->shipping_address_line_1)) echo $datas->shipping_address_line_1 . '<br>';
                                            echo '<b>Address Line 2 : </b> '; if(isset($datas->shipping_address_line_2)) echo $datas->shipping_address_line_2 . '<br>';
                                            echo '<b>Country : </b> '; if(isset($datas->shipping_country)) echo getCountryName($datas->shipping_country) . '<br>';
                                            echo '<b>State : </b> '; if(isset($datas->shipping_state)) echo getStateName($datas->shipping_state) . '<br>';
                                            echo '<b>City : </b> '; if(isset($datas->shipping_city)) echo getCityName($datas->shipping_city) . '<br>';
                                            echo '<b>Pincode : </b> '; if(isset($datas->shipping_postcode)) echo $datas->shipping_postcode . '<br>';
                                            echo '<b>Contact No. : </b> '; if(isset($datas->shipping_mobile)) echo $datas->shipping_mobile . '<br>';
                                            echo '<b>Alternate Contact No. : </b> '; if(isset($datas->shipping_alternate_contact)) echo $datas->shipping_alternate_contact . '<br>';
                                        echo '</p>
                                    </div>';
                                }
                            }
                            echo '<br/>';
                        }
                    }
                    echo '</p>
                    <div class="op">';
                    $products = json_decode($order->products, true);
                    if(count($products)>0){
                        $ids = "";
                        foreach($products as $datat){
                            $ids .= $datat['id'] . ",";
                            $q[$datat['id']] = $datat['quantity'];
                        }
                        $ids = rtrim($ids, ',');
                        $products = DB::select("SELECT * FROM products WHERE id IN (".$ids.")  ORDER BY id DESC ");
                        $total_price=0;
                        foreach ($products as $product){
                            echo '<div id='.$product->id.' ><p>'.$product->title.'</p><b>'.$q[$product->id].' Unit</b><div style="clear:both"></div></div>';
                        }
                        echo '<b>Total Amount : '.c($order->summ).'</b>';
                    }
                    echo'<div style="clear:both;"></div>
                    </div>
                    </div>';
                }
            echo'</div>
        </div>
        </div>
        </div>
        </div>
    </div>';
}
$data['foot'];
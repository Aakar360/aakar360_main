<?php
echo $data['header'];
if(isset($_GET['edit'])){
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Edit faq status</h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="faq" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
                <form action="" method="post" class="form-horizontal single" enctype="multipart/form-data">
			'.csrf_field().'
			
				<fieldset>
					  <div class="form-group">
						<label class="control-label">Answer</label>
						<textarea class="form-control" name="answer" rows="5" placeholder="Write answer here" required="required">'.$data['faq']->answer.'</textarea>
					  </div>
					  <div class="form-group">
						<label class="control-label">Status</label>
						<select name="status" class="form-control">';
    if($data['faq']->status == 'Pending') {
        echo '<option value="">Please Select Status</option>
                                <option value="Pending" selected>Pending</option>
                                <option value="Approved">Approved</option>';
    } else if($data['faq']->status == 'Approved') {
        echo '<option value="">Please Select Status</option>
                                <option value="Pending" >Pending</option>
                                <option value="Approved" selected>Approved</option>';
    }
    echo '</select>
					  </div>
					  <input name="edit" type="submit" value="Edit faq" style="padding:3px 25px;" class="btn btn-primary" />
				</fieldset>
			</form>
			</div>
			</div>
			</div>
			</div>';
    ?>


<?php } else { ?>
    <div class="row" style="margin-top: -13px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h3>Services FAQ(s)</h3>
                    <h5 class="card-title" style="color: #0d1baa;">Manage faq(s)</h5>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Service Name</th>
                                <th>User Name</th>
                                <th>Question</th>
                                <th>Answer</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['faqs'] as $faq){
                                echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'.getServiceCategoryName($faq->service_id).'</td>';
                                $u = $faq->user_id;
                                $user = DB::select("SELECT * FROM customers WHERE id = '$u'")[0];
                                echo '<td>'.$user->name.'</td>
                        
                        <td>'.$faq->question.'</td>
                        <td>'.$faq->answer.'</td>
                        <td>'.$faq->status.'</td>
                        <td><a href="faq?edit='.$faq->id.'"><i class="icon-question"></i></a>
                        </td></tr>';

                                $sr++;}
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<style>
    .button {
        background: gainsboro;
        padding: 5px 20px;
        cursor: pointer;
        border-radius: 30px;
    }
    .mini-button {
        cursor: pointer;
        border-radius: 30px;
        background: transparent;
        padding: 5px 0px;
        display: block;
    }
</style>

<?php
echo $data['footer'];
?>




<?php
echo $data['header'];
if(isset($_GET['add'])) {
    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
            <h5>Add Best Category </h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="best-for-cat" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Link
						<input name="link" type="text"  class="form-control" />
					  </div>
					  
					  <div class="input-field col s12 m6">
                        Category
                        <select name="category" class="form-control">';
    foreach ($data['categories'] as $brand){
        echo '<option value="'.$brand->id.'">'.$brand->name.'</option>';
        $childs = DB::select("SELECT * FROM category WHERE parent = ".$brand->id);
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id);
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
                      </div>
                      </div>
                      <div class="row">
                       <div class="input-field col s12 m6">
						Image
						<input name="image" type="file"  class="form-control" />
					  </div>
					  <div class="input-field col s12 m12">
						Content
						<textarea name="content" class="form-control" id="editor" rows="10" cols="80" required></textarea>
					  </div>
					  
					  
					  </div>
					  <div class="row">
					  <input name="add" type="submit" value="Submit" class="btn btn-primary" />
				</div>
				</div>
			</form>
			</div>
			</div>
			</div>
			</div>';
} elseif(isset($_GET['edit'])) {

    echo $data['notices'].'<div class="row" style="margin-top: -13px;">
    <div class="col s12">
        <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
            <div class="card-content" style="min-height: 550px;">
             <h5>Edit Best Category </h5>
                <div class="col s12 m12 " style="text-align:center;margin-top: 20px;" >
                    <a class="btn myblue waves-light " style="padding:0 5px;" href="best-for-cat" >
                        <i class="material-icons left" style="margin-right: 5px">arrow_back</i>Back
                    </a>
                </div>
            <form action="" method="post" enctype="multipart/form-data" style="max-width: 100%">
			'.csrf_field().'
			
				<div class="row">
					  <div class="input-field col s12 m6">
						Link
						<input name="link" type="text"  value="'.$data['bestforcat']->link.'" class="form-control" />
					  </div>
					  
					  <div class="input-field col s12 m6">
							Product category
							<select name="category" class="form-control">';
    foreach ($data['categories'] as $category){
        echo '<option value="'.$category->id.'" '.($category->id == $data['bestforcat']->category ? 'selected' : '').'>'.$category->name.'</option>';
        $childs = DB::select("SELECT * FROM category WHERE parent = ".$category->id." ORDER BY id DESC");
        foreach ($childs as $child){
            echo '<option value="'.$child->id.'" '.($child->id == $data['bestforcat']->category ? 'selected' : '').'>- '.$child->name.'</option>';
            $subchilds = DB::select("SELECT * FROM category WHERE parent = ".$child->id." ORDER BY id DESC");
            foreach ($subchilds as $subchild){
                echo '<option value="'.$subchild->id.'" '.($subchild->id == $data['bestforcat']->category ? 'selected' : '').'><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
            }
        }
    }
    echo '</select>
						  </div>
						  </div>
						  <div class="row">
					  <div class="input-field col s12 m12">
						Content
						<textarea name="content" class="form-control" id="editor" rows="10" cols="80" required>'.$data['bestforcat']->content.'</textarea>
					  </div></div><div class="row"><div class="input-field col s12 m6">';
    if (!empty($data['bestforcat']->image)){

        echo '<p>Uploading new images will overwrtite current images .</p>';

        echo '<img class="col-md-2" src="'.url('/assets/products/'.$data['bestforcat']->image).'" />';

        echo '<div class="clearfix"></div>';
    }
    echo '</div>
					  
					  <div class="input-field col s12 m6">
						Image
						<input name="image" type="file"  class="form-control" />
					  </div>
					  </div>
					  
					  <input name="edit" type="submit" value="Update" class="btn btn-primary" />
				
				
			</form>
			</div>
			</div>
			</div>
			</div>';
} else {
    ?>
    <div class="row" style="margin-top: 10px;">
        <div class="col s12">
            <div  class="card card-tabs" style="box-shadow: 2px 6px 6px #888888;">
                <div class="card-content" style="min-height: 550px;">
                    <h5>Best ForCategory<a href="best-for-cat?add" style="padding: 3px 15px; font-size: 15px;" class="add">Add Best</a></h5>
                    <h5 class="card-title" style="color: #0d1baa;">Manage Your Categories </h5>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="datatable-editable">
                            <thead>
                            <tr class="bg-blue">
                                <th>Sr. No.</th>
                                <th>Link</th>
                                <th>Category</th>
                                <th>Content</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sr = 1;
                            echo $data['notices'];
                            foreach ($data['bestforcats'] as $category){
                                echo'<tr>
                        <td>'.$sr.'</td>
                        <td>'.$category->link.'</td>
                        <td>'.getCategoryName($category->category).'</td>
                        <td>'.$category->content.'</td>
                        <td><img src="../assets/products/'.image_order($category->image).'" style="height: auto; width: 100px;"></td>
                        
            
                        <td>
                            <a href="best-for-cat?delete='.$category->id.'"><i class="icon-trash"></i></a>
                            <a href="best-for-cat?edit='.$category->id.'"><i class="icon-pencil"></i></a>
                        </td>
                      </tr>
                      ';
                                $sr++;
                            }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php
}
echo $data['footer'];
?>
<script>
    function removeThis($id){
        event.preventDefault();
        $('[data='+$id+']').remove();
    }
    $('#add-option').on('click', function(){
        var id = $('#que-type > div').length;
        var html = '<div class="col-md-12 options" data="'+id+'"><label class="control-label col-md-12" data="'+id+'">Option '+(id+1)+'</label><input name="option[]" type="text" data="'+id+'" class="form-control col-md-11" style="width: 90%;margin-right: 5px;" /><a onClick="removeThis('+id+');" class="pul-right" style="line-height: 3;" data="'+id+'"><i class="icon-close" style="font-size: 20px;"></i></a></div>';
        $('#add-option').before(html);
    });
    function getData(data){
        if(data == 'text' || data == 'textarea'){
            $('.options').hide();
        }else{
            $('.options').show();
        }
    }


    function addit(val){
        var da = val;
        $.ajax({
            url: 'catData',
            type: 'post',
            data: 'page='+da+'&_token=<?=csrf_token(); ?>',
            success: function(data){
                $('#catdata').html(data);
            }
        });
    }

</script>
<script>
    $( document ).ready(function() {
        CKEDITOR.replace( 'editor' );
        CKEDITOR.replace( 'editor1' );
    });
</script>
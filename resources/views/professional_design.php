<?php echo $header?>

<div class="profile-root bg-white ng-scope">
    <div class="clearfix buffer-top text-center wrapper-1400 bg-color">
        <div class="clearfix wrapper-1140 two-bgs">
            <?php if(customer('header_image') != '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') == '' && customer('header_image_1') == ''){ ?>
                <div class="col-sm-12">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/banners/<?=$dimg->image; ?>" class="cover">
                    </div>
                </div>
            <?php } elseif(customer('header_image') != '' && customer('header_image_1') != ''){ ?>
                <div class="col-sm-6" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image'); ?>" class="cover">
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs" style=" padding-left: 2px; padding-right: 2px;">
                    <div class="profile-background vcard wrapper-1140">
                        <img src="assets/user_image/<?php echo Customer('header_image_1'); ?>" class="cover">
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="profile-overlay clearfix wrapper-980 bgs-white ng-isolate-scope" style="width: 980px;">
            <img itemprop="image" class="profile-icon" src="assets/user_image/<?php if(customer('image') != ''){ echo Customer('image'); } else { echo 'user.png'; } ?>">
            <h1 itemprop="name" class="name hand-writing font-44 font-36-xs">
                <a itemprop="url" class="url">
                    <?php echo Customer('name'); ?>
                </a>
                <p style="text-align: center;"><a href="professional_account" class="btn">My Profile</a></p>
            </h1>
            <div class="sans-light font-15 line-height-md hidden-xs">
                <p style="text-align: justify; padding: 30px;"><?php echo customer('about_professional'); ?></p>
            </div>
            <div class="sans-light font-15 line-height-md hidden-xs" style="padding-bottom: 0px; position: absolute; width: 100%; bottom: 0;">
                <p style="text-align: center;">
                    <a href="professional_product" class="btn col-lg-2">Products</a>
                    <a href="professional_services" class="btn col-lg-2">Services</a>
                    <a href="professional_design" class="btn col-lg-2 btn-primary">Design</a>
                    <a href="professional_advices" class="btn col-lg-2">Advices</a>
                    <a href="my-projects"  class="btn col-lg-2">My Projects</a>
                </p>
            </div>
        </div>

        <div class="clearfix wrapper-980 bg-white m-t-xs m-b-md">
            <div class="about-designer clearfix">
                <div class="text-left">
                    <section class="at-property-sec" style="padding-top: 0px;">
                        <div class="container" style="margin-left: -15px; width: 1140px;">
                            <ul class="nav nav-pills" style="background-color: white;">
                                <li class="active"><a href="#plan" data-toggle="tab" >Plan Orders</a></li>
                                <li class=""><a href="#plan-wish" data-toggle="tab" >Plan Collections</a></li>
                                <li class=""><a data-toggle="tab"  href="#image-ins">Image Collections</a></li>
                                <li><a href="#my-layout" data-toggle="tab" >My Layout Plans</a></li>
                                <li><a href="#add-layout" data-toggle="tab" >Add New Layout Plan</a></li>

                            </ul>

                            <div class="tab-content">



                                <div id="plan" class="tab-pane fade in active">
                                    <br>
                                    <?php foreach($orders as $layout){
                                        $lay = DB::select('SELECT * FROM layout_plans Where id = '.$layout->id)[0];?>
                                        <div class="grid-item">
                                            <div class="tiles">
                                                <a href="<?=url('planinvoice/'.$layout->id)?>">
                                                    <img src="<?php echo url('/assets/layout_plans/'.image_order($lay->images)); ?>"/>
                                                    <div class="icon-holder">
                                                        <?php $amm = json_decode($lay->interior_ammenities);
                                                        $i = 1;
                                                        //dd($amm);
                                                        foreach($amm as $key=>$value){
                                                            $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                                                            //dd($a);
                                                            ?>

                                                            <?php if($i <= 3){
                                                                if($i%3 == 0){ ?>
                                                                    <div class="icons mrgntop borderRight0"><b><?=$a->name.' : '.$value; ?></b></div>
                                                                <?php }else{ ?>
                                                                    <div class="icons mrgntop"><b><?=$a->name.' : '.$value; ?></b></div>
                                                                <?php }
                                                            }else{
                                                                if($i%3 == 0){ ?>
                                                                    <div class="icons mrgnbot borderRight0"><b><?=$a->name .' : '.$value; ?></b></div>
                                                                <?php }else{ ?>
                                                                    <div class="icons mrgnbot"><b><?=$a->name .' : '.$value; ?></b></div>
                                                                <?php } } ?>
                                                            <?php $i++; } ?>
                                                    </div>
                                                </a>

                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>



                                <div id="plan-wish" class="tab-pane fade">
                                    <?php foreach($planwishlist as $plwish){
                                        $layo = DB::select('SELECT * FROM layout_plans Where id = '.$plwish->layout_id)[0];?>
                                        <div class="grid-item">
                                            <div class="tiles">
                                                <a href="<?php echo url('/layout_plan/'.$layo->slug); ?>">
                                                    <img src="<?php echo url('/assets/layout_plans/'.image_order($layo->images)); ?>"/>
                                                    <div class="icon-holder">
                                                        <?php $amm = json_decode($lay->interior_ammenities);
                                                        $i = 1;
                                                        //dd($amm);
                                                        foreach($amm as $key=>$value){
                                                            $a = DB::select("SELECT * FROM amminities WHERE id = ".$key)[0];
                                                            //dd($a);
                                                            ?>

                                                            <?php if($i <= 3){
                                                                if($i%3 == 0){ ?>
                                                                    <div class="icons mrgntop borderRight0"><b><?=$a->name.' : '.$value; ?></b></div>
                                                                <?php }else{ ?>
                                                                    <div class="icons mrgntop"><b><?=$a->name.' : '.$value; ?></b></div>
                                                                <?php }
                                                            }else{
                                                                if($i%3 == 0){ ?>
                                                                    <div class="icons mrgnbot borderRight0"><b><?=$a->name .' : '.$value; ?></b></div>
                                                                <?php }else{ ?>
                                                                    <div class="icons mrgnbot"><b><?=$a->name .' : '.$value; ?></b></div>
                                                                <?php } } ?>
                                                            <?php $i++; } ?>
                                                    </div>
                                                </a>

                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>





                                <div id="image-ins" class="tab-pane fade">
                                    <?php foreach($imgins as $ii){
                                        $igi = DB::select('SELECT * FROM photo_gallery Where id = '.$ii->image_id)[0];?>
                                        <div class="grid-item">
                                            <div class="tiles">
                                                <a href="photo-gallery/<?=$igi->slug; ?>">
                                                    <img src="assets/images/gallery/<?=$igi->image; ?>"/>
                                                    <div class="icon-holder">
                                                        <div class="icons1 mrgntop"><?=substr(translate($igi->title), 0, 20); if(strlen($igi->title)>20) echo '...';?></div>
                                                        <div class="icons2 mrgntop borderRight0"><?=getPlanPhotsCount($igi->id); ?> Photos</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>

                                <div id="my-layout" class="tab-pane fade" style="background-color: white;">
                                    <br>
                                    <?php
                                        echo '<div class="head">
                                            <h3>Layout Plans</h3>
                                            <p>Manage your layout plans </p>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="datatable-editable">
                                                <thead>
                                                    <tr class="bg-blue">
                                                        <th>Sr. No.</th>
                                                        <th>Plan Banner</th>
                                                        <th>Plan Name</th>
                                                        <th>Plan Number</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';
                                                $sr = 1;
                                                foreach ($plans as $plan){
                                                echo '<tr>
                                                    <td>'.$sr.'</td>
                                                    <td><img src="../assets/layout_plans/'.image_order($plan->images).'" style="height: 100px; width: 100px;"></td>
                                                    <td>'.$plan->title.'</td>
                                                    <td>'.$plan->plan_code.'</td>
                                                    <td><a href="layout-plans?delete='.$plan->id.'"><i class="icon-trash"></i></a>
                                                        <a onclick="professionaledit()" href="#" id="editid" data-id="'.$plan->id.'"><i class="icon-pencil"></i></a>
                                                        <a href="layout-plans/layout_plan_addon/'.$plan->slug.'"><i class="icon-plus"></i></a>
                                                    </td>
                                                  </tr>
                                                '; $sr++;
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                <div id="add-layout" class="tab-pane fade" style="background-color: white;">
                                    <br>
                                    <?php
                                    echo '<form action="'.url('professional_design_add').'" method="post" enctype="multipart/form-data" class="form-horizontal single">
                                        '.csrf_field().'
                                        <h5>Add New Layout Plan</h5>
                                        <table class="responsive-table bordered" style="width: 80%; margin: auto;">
                                            <tr>
                                                <td>Layout Plan name</td>
                                                <td><input name="title" type="text"  class="form-control" required/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Layout Plan code</td>
                                                <td><input name="plan_code" type="text"  class="form-control" required/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan Category</td>
                                                <td>
                                                    <select name="category" class="form-control select2">';
                                                        foreach ($categories as $category){
                                                        echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                                        $childs = DB::select("SELECT * FROM design_category WHERE parent = ".$category->id." ORDER BY id DESC");
                                                        foreach ($childs as $child){
                                                        echo '<option value="'.$child->id.'">- '.$child->name.'</option>';
                                                        $subchilds = DB::select("SELECT * FROM design_category WHERE parent = ".$child->id." ORDER BY id DESC");
                                                        foreach ($subchilds as $subchild){
                                                        echo '<option value="'.$subchild->id.'"><i style="padding-left: 10px;">--> '.$subchild->name.'</i></option>';
                                                        }
                                                        }
                                                        }
                                                        echo '</select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan description</td>
                                                <td><textarea name="description" type="text" id="editor1" class="form-control" required></textarea></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan specification</td>
                                                <td><textarea name="specification" id="editor" type="text" class="form-control" required></textarea></td>
                                            </tr
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan price</td>
                                                <td><input name="price" type="text" class="form-control" required /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Tax (%)</td>
                                                <td><input name="tax" type="text" class="form-control" required /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><label class="control-label">Drawing Views</label></td>
                                                <td><input name="drawing_views[]" type="text" class="form-control col-lg-6 pull-left" style="width: 50%;" required/>
                                                <input type="file" class="form-control col-lg-6 pull-right" name="file" accept="pdf/*"  style="width: 50%;" required/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><label class="control-label">Drawing Views</label></td>
                                                <td><input name="drawing_views[]" type="text" class="form-control col-lg-6 pull-left" style="width: 50%;" required/>
                                                <input type="file" class="form-control col-lg-6 pull-right" name="file1" accept="pdf/*"  style="width: 50%;" required/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan images</td>
                                                <td><input type="file" class="form-control" name="images[]" multiple="multiple" accept="image/*"  required/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Layout Amminities</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select name="ammenities[]" class="form-control"  style="border-radius: 0px; border: 1px solid;">
                                                        <option value="">Please Select Amminity</option>';
                                                        foreach($amminities as $ammy){
                                                        echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                                        }
                                                        echo '</select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="interior_ammenities[]"  style="border-radius: 0px; border: 1px solid;"/>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                                        <option value="">Please Select Amminity</option>';
                                                        foreach($amminities as $ammy){
                                                        echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                                        }
                                                        echo '</select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="interior_ammenities[]"   style="border-radius: 0px; border: 1px solid;"/>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                                        <option value="">Please Select Amminity</option>';
                                                        foreach($amminities as $ammy){
                                                        echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                                        }
                                                        echo '</select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="interior_ammenities[]"   style="border-radius: 0px; border: 1px solid;"/>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                                        <option value="">Please Select Amminity</option>';
                                                        foreach($amminities as $ammy){
                                                        echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                                        }
                                                        echo '</select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="interior_ammenities[]"   style="border-radius: 0px; border: 1px solid;"/>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select name="ammenities[]" class="form-control"   style="border-radius: 0px; border: 1px solid;">
                                                        <option value="">Please Select Amminity</option>';
                                                        foreach($amminities as $ammy){
                                                        echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                                        }
                                                        echo '</select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="interior_ammenities[]"   style="border-radius: 0px; border: 1px solid;"/>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select name="ammenities[]" class="form-control"  style="border-radius: 0px; border: 1px solid;">
                                                        <option value="">Please Select Amminity</option>';
                                                        foreach($amminities as $ammy){
                                                        echo '<option value="'.$ammy->id.'">'.$ammy->name.'</option>';
                                                        }
                                                        echo '</select>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control" name="interior_ammenities[]" style="border-radius: 0px; border: 1px solid;"/>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 1 Name</td>
                                                <td><input name="p1_name" type="text" class="form-control" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 1 Price</td>
                                                <td><input name="p1_price" type="text" class="form-control" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 1 Duration</td>
                                                <td><input name="p1_duration" type="text" class="form-control" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 1 description</td>
                                                <td><textarea name="p1_description" type="text" id="editorp1" class="form-control"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td>Plan 2 Name</td>
                                                <td><input name="p2_name" type="text" class="form-control" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 2 Price</td>
                                                <td><input name="p2_price" type="text" class="form-control" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 2 Duration</td>
                                                <td><input name="p2_duration" type="text" class="form-control" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 2 description</td>
                                                <td><textarea name="p2_description" type="text" id="editorp2" class="form-control"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 3 Name</td>
                                                <td><input name="p3_name" type="text" class="form-control" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 3 Price</td>
                                                <td><input name="p3_price" type="text" class="form-control" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 3 Duration</td>
                                                <td><input name="p3_duration" type="text" class="form-control" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Plan 3 description</td>
                                                <td><textarea name="p3_description" type="text" id="editorp3" class="form-control"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><input name="add" type="submit" value="Add plan" class="btn btn-primary" /></td>
                                            </tr>
                                        </table>
                                    </form>';
                                    ?>
                                </div>


                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <?php echo $footer?>

    <script>
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });

        function professionaledit(){
            event.preventDefault();
            var edit = $('#editid').attr('data-id');
            $.ajax({
                type: "POST",
                url: "get_professional_edit_detail",
                data:'_token=<?=csrf_token();?>&id='+edit,
                success: function(data){
                    //$("#state-list").html('');
                    $('#service-edit').click();
                    $("#edit-detail").html(data);
                }
            });

        }
        $( document ).ready(function() {
            CKEDITOR.replace( 'editor' );
            CKEDITOR.replace( 'editor1' );
            CKEDITOR.replace( 'editorp1' );
            CKEDITOR.replace( 'editorp2' );
            CKEDITOR.replace( 'editorp3' );
        });

        function getcreate(){
            event.preventDefault();
            $('#project-modal').click();
        }

        function showProject(val) {
            document.getElementById("myForm").submit();
        }
    </script>

<button class="btn btn-primary hidden" id="open-pro-modal1" data-popup-open="popup-pro">Get Started !</button>
<div class="popup" id="popup-pro" data-popup="popup-pro">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-lg-12 account" style="border: none; box-shadow: none;">
            <?=csrf_field() ?>
            <div class="grid" id="projectName">

            </div>
        </div>
        <a class="popup-close" id="pclose" data-popup-close="popup-pro" href="#">x</a>
    </div>
</div>

<a class="btn btn-primary hidden" id="project-modal" data-popup-open="popup-project" href="#">Get Started !</a>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="file" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>

<a class="btn btn-primary hidden" id="service-edit" data-popup-open="service-edit" href="#">Get Started !</a>
<div class="popup" id="service-edit" data-popup="service-edit">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;" id="edit-detail">

        </div>
        <a class="popup-close" data-popup-close="service-edit" href="#">x</a>
    </div>
</div>

<a class="btn btn-primary hidden" id="project-modal" data-popup-open="popup-project" href="#">Get Started !</a>
<div class="popup" id="popup-project" data-popup="popup-project">
    <div class="popup-inner" style="padding-top: 120px;">
        <div class="col-md-10 account" style="border: none; box-shadow: none;">
            <?php
            if (session('error') != ''){
                echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
            }
            ?>
            <form action="<?php echo url('create-project'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                    <tbody>
                    <input type="hidden" name="user_id" value="<?=customer('id'); ?>">
                    <tr>
                        <td>Project Title</td>
                        <td>:</td>
                        <td><input type="text" name="title" required class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Project Image</td>
                        <td>:</td>
                        <td><input type="image" name="image" required class="form-control"></td>
                    </tr>
                    <?php echo csrf_field(); ?>
                    <tr>
                        <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <a class="popup-close" data-popup-close="popup-project" href="#">x</a>
    </div>
</div>



    <a class="btn btn-primary hidden" id="personal-modal" data-popup-open="popup-personal" href="#">Get Started !</a>
    <div class="popup" id="popup-personal" data-popup="popup-personal">
        <div class="popup-inner" style="padding-top: 120px;">
            <div class="col-md-10 account" style="border: none; box-shadow: none;">
                <?php
                if (session('error') != ''){
                    echo '<script type="text/javascript">alert("'.translate(session('error')).'");</script>';
                }
                ?>
                <form action="<?php echo url('account'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal single">
                    <table class="responsive-table bordered" style="width: 50%; margin: auto;">
                        <tbody>
                        <tr>
                            <td>Name (*)</td>
                            <td>:</td>
                            <td><input type="text" name="name" required class="form-control" value="<?php echo Customer('name'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Mobile (*)</td>
                            <td>:</td>
                            <td><input type="text" name="mobile" required class="form-control" value="<?php echo Customer('mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Alternate Mobile</td>
                            <td>:</td>
                            <td><input type="text" name="alternate_mobile" class="form-control" value="<?php echo Customer('alternate_mobile'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Email (*)</td>
                            <td>:</td>
                            <td><input type="text" name="email" required class="form-control" value="<?php echo Customer('email'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Profile Image</td>
                            <td>:</td>
                            <td><input type="file" name="image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 1</td>
                            <td>:</td>
                            <td><input type="file" name="header_image" class="form-control" ></td>
                        </tr>

                        <tr>
                            <td>Banner Image 2</td>
                            <td>:</td>
                            <td><input type="file" name="header_image_1" class="form-control" ></td>
                        </tr>
                        <tr>
                            <td>Company (*)</td>
                            <td>:</td>
                            <td><input type="text" name="company" required class="form-control" value="<?php echo Customer('company'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 1 (*)</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_1" required class="form-control" value="<?php echo Customer('address_line_1'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Address Line 2</td>
                            <td>:</td>
                            <td><input type="text" name="address_line_2" class="form-control" value="<?php echo Customer('address_line_2'); ?>"></td>
                        </tr>
                        <tr>
                            <td>Country (*)</td>
                            <td>:</td>
                            <td>
                                <select name="country" id="country_shipping" required class="form-control" onChange="getStates(this.value, 'state_shipping')">
                                    <option value="<?php Customer('country'); ?>"><?php echo getCountryName(Customer('country')); ?></option>
                                    <option value="">Select country</option>
                                    <?php foreach($countries as $country){
                                        echo '<option value="'.$country->id.'">'.$country->name.'</option>';
                                    }?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>State (*)</td>
                            <td>:</td>
                            <td>
                                <select name="state" class="form-control" required id="state_shipping" onChange="getCities(this.value, 'city_shipping')">
                                    <option value="<?php Customer('state'); ?>"><?php echo getStateName(Customer('state')); ?></option>
                                    <option value="">Please select region, state or province</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>City (*)</td>
                            <td>:</td>
                            <td>
                                <select name="city" class="form-control" required id="city_shipping">
                                    <option value="<?php Customer('city'); ?>"><?php echo getCityName(Customer('city')); ?></option>
                                    <option value="">Please select city or locality</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Postcode (*)</td>
                            <td>:</td>
                            <td><input type="text" name="postcode" class="form-control" required value="<?php echo Customer('postcode'); ?>"></td>
                        </tr>
                        <?php echo csrf_field(); ?>
                        <tr>
                            <td colspan="3"><input type="submit" name="submit" value="Update" class="btn btn-primary btn-lg"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <a class="popup-close" data-popup-close="popup-personal" href="#">x</a>
        </div>
    </div>
<?php echo $header?>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3719.519146883229!2d81.6623744147666!3d21.21125268589984!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a28dd82a50567f7%3A0xfd0d78d8d6176b27!2sAdroweb+IT+Services+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1553672035845" width="100%" height="400" frameborder="0" style="border:0; margin-bottom: 0;" allowfullscreen></iframe>
    <section class="at-contact-sec" style="background: white;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="at-contact-form at-col-default-mar">
                        <div id="form-messages"></div>
                        <form method="post" action="<?php url("contact-email"); ?>">
                            <?=csrf_field()?>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Your Name" required>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" placeholder="Subject" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-default hvr-bounce-to-right" name="submit" type="submit">Sent Message</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="at-info-box at-col-default-mar">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <p>mailaddress@gmail.com</p>
                    </div>
                    <div class="at-info-box at-col-default-mar">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <p>+0123 (123) 6719900</p>
                    </div>
                    <div class="at-info-box at-col-default-mar">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <p>99, Big Building, Glasgow, United Kingdom.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  Contact end-->

    <script type="text/javascript" src="assets/js/google-map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdEPAHqgxFK5pioDAB3rsvKchAtXxRGO4&amp;callback=myMap"></script>
<?php echo $footer?>
<?php echo $header?>

<div class="b-hero1">
    <figure  style="background-image:url(&quot;<?php echo url('/assets/blog/'.$dimg->banner_image); ?>&quot;)" class="b-post__hero-image">
        <div data-w-id="403de785-9462-5110-ab3f-b13328bf2c04" class="b-post__hero-inner">
            <div class="b-container cc-read cc-hero">
                <div class="b-read__wrap cc-hero">
                    <h1 class="b-post__title"> <?php if(!empty($dimg)){ ?><?=translate($dimg->name); }?></h1>
                    <p class="b-post__subhead">Take a look at these 19 websites made in Webflow and handpicked from our internal Slack.</p>
                </div>
            </div>
        </div>
    </figure>
</div>
<div class="row" style="text-align: center;">
    <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8" style="margin-top: -105px; z-index: 1">
        <div class="header-bottom">
            <div class="header-navigation">
                <ul class="navigation navigation--horizontal">
                    <li class="navigation-item navigation-design">
                        <a href="javascript:void(0)" title="Saved"><span>Saved</span><i class="fa fa-save"></i></a>
                    </li>
                    <li class="navigation-item navigation-architecture">
                        <?php $totvisit = 0;
                            foreach($btotal as $btv){
                                $totvisit = $totvisit + $btv->visits;
                            }
                        ?>
                        <a href="javascript:void(0)" title="Total Visits"><span><?=$totvisit; ?> Total Visits</span><i class="fa fa-eye"> <?=$totvisit; ?></i> </a>
                    </li>
                    <li class="navigation-item navigation-art">
                        <?php
                            $totrat = 0;
                            foreach ($btotal as $btr){
                                $total_revi = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$btr->id)[0]->count;
                                $totrat = $totrat + $total_revi;

                            }
                        ?>
                        <a href="javascript:void(0)" title="Liked"><span><?=$totrat; ?> Total Reviews</span><i class="fa fa-pencil-square-o"> <?=$totrat; ?></i></a>
                    </li>
                    <li class="navigation-item navigation-art">
                        <?php
                        $totli = 0;
                        foreach ($btotal as $btl){
                            $li = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$btl->id)[0]->count;
                            $totli = $totli + $li;

                        }
                        ?>

                        <a href="javascript:void(0)" title="Liked"><span><?=$totli; ?> Total Likes</span><i class="fa fa-thumbs-up"> <?=$totli; ?></i></a>
                    </li>
                </ul>
            </div>
        </div>


    </div>

</div>

<section class="pdtopbtm-50">
    <div class="container">
        <div class="row">
            <button class="btn btn-default filter-button"><i class="fa fa-filter"></i> Filter</button>
            <aside class="sidebar-shop col-md-3 order-md-first">
                <div class="filter-div-close"></div>
                <div class="sidebar-wrapper at-categories" style="padding: 10px;">
                    <div class="widget">
                        <h3 class="widget-title">
                            <a data-toggle="collapse" href="#widget-body-c" role="button" aria-expanded="true" aria-controls="widget-body-c">Categories</a>
                        </h3>
                        <div class="show collapse in" id="widget-body-c">
                            <div class="widget-body">
                                <ul class="cat-list">
                                    <?php foreach($cats as $cat){
                                        echo '<li><a href="advices/'.$cat->slug.'">'.translate($cat->name).'</a></li>';
                                        $childs = DB::select("SELECT * FROM advices_category WHERE parent = ".$cat->id." ORDER BY id DESC");
                                        foreach ($childs as $child){
                                            echo '<li><a href="advices/'.$child->id.'">- '.$child->name.'</a></li>';
                                        }
                                    }
                                    ?>
                                </ul>
                            </div><!-- End .widget-body -->
                        </div><!-- End .collapse -->
                    </div><!-- End .widget -->
                </div>
            </aside>

            <div class="col-md-9 col-lg-9">
                <?php foreach ($posts as $post){ ?>
                    <div class="profile-overlay clearfix wrapper-11140 ng-isolate-scope">
                        <div class="col-lg-12 advice-box" data-redirect="advice/<?=path($post->title,$post->id);?>">
                            <div class="col-lg-6 col-md-6 advice-banner">
                                <img class="advice-img" itemprop="image" src="<?php echo url('/assets/blog/'.$post->images); ?>">
                            </div>
                            <div class="col-lg-6 col-md-6 advice-content">
                                <div class="advice-inner-content">
                                    <h3><?=$post->title; ?>
                                        <p>
                                            <small class="pull-left"><i class="fa fa-bars"></i> <?php echo getadvicesCategoryName($post->category); ?></small>
                                            <small class="pull-right"><i class="fa fa-calendar"></i> <?=$post->date; ?></small>
                                        </p>
                                    </h3>

                                    <p><?=$post->short_des; ?></p>

                                </div>
                                <div class="advice-bottom-content">
                                    <?php
                                    $total_ratings = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->count;
                                    $total_reviews = DB::select("SELECT COUNT(*) as count FROM advices_reviews WHERE active = 1 AND review <> '' AND advice_id = ".$post->id)[0]->count;
                                    $like = DB::select("SELECT COUNT(*) as count FROM advices_like WHERE advice_id = ".$post->id)[0]->count;
                                    $rating = 0;
                                    if ($total_ratings > 0){
                                        $rating_summ = DB::select("SELECT SUM(rating) as sum FROM advices_reviews WHERE active = 1 AND advice_id = ".$post->id)[0]->sum;
                                        $rating = $rating_summ / $total_ratings;
                                    }
                                    $reviews = DB::select("SELECT * FROM advices_reviews WHERE advice_id = ".$post->id." AND active = 1 ORDER BY time DESC");
                                    $faqs = DB::select("SELECT * FROM advices_faq WHERE advice_id = ".$post->id." AND status = 'Approved' ORDER BY id DESC");
                                    $rating = DB::select("SELECT count(*) as total_user, SUM(rating) as total_rating FROM advices_reviews WHERE active = '1' AND advice_id = '".$post->id."'")[0];
                                    $total_rating =  $rating->total_rating;
                                    $total_user = $rating->total_user;
                                    if($total_rating==0){
                                        $avg_rating = 0;
                                    }
                                    else{
                                        $avg_rating = round($total_rating/$total_user,1);
                                    }
                                    ?>

                                    <p style="width: 25%; float: left; bottom: 10px;margin: 0;"> <i class="fa fa-eye"></i> <?=$post->visits; ?> Visits</p>
                                    <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-pencil-square-o"></i> <?=$total_reviews; ?> Reviews</p>
                                    <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-star"></i> <?=$avg_rating; ?> Ratings</p>
                                    <p style="width: 25%; float: left; bottom: 10px;margin: 0;"><i class="fa fa-thumbs-up"></i> <?=$like; ?> Likes</p>
                                </div>
                            </div>
                        </div>
                        <a data-title="<?php echo translate($post->title); ?>" href="blog/<?php echo path($post->title,$post->id); ?>">
                        </a>
                    </div>
                <?php } ?>
                <?php if(!empty($link)){?>
                    <div class="container">
                        <div class="blockquote blockquote--style-1">
                            <div class="row inner-div">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <img src="<?=url('/assets/products/'.image_order($link->image))?>" style="width:50%;height:auto;"/>
                                    </div>
                                    <div class="col-md-7" style="padding-top: 25px;">
                                        <h3><?=$link->content; ?></h3>
                                    </div>
                                    <div class="col-md-2" style="padding:15px 0 0 0">
                                        <a class="btn btn-primary" href="<?=$link->link; ?>" type="submit" style="padding: 10px auto !important;">
                                            GET STARTED
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

    <?php echo $footer?>

    <script>
        $(document).ready(function(){
            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if(activeTab){
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }

            $(document).on('click', '.filter-button', function(){
                $('aside.sidebar-shop').addClass('open');
            });
            $(document).on('click', '.filter-div-close', function(){
                $('aside.sidebar-shop').removeClass('open');
            });
        });
        $(function() {
//----- OPEN
            $('[data-popup-open]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                e.preventDefault();
            });
//----- CLOSE
            $('[data-popup-close]').on('click', function(e) {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                e.preventDefault();
            });
        });

    </script>
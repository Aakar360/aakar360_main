<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/14/2019
 * Time: 4:57 PM
 */
 echo $header?>
 <div class="content-wrapper">

  <div class="row">
   <div class="col-md-12 grid-margin">
    <div class="d-flex justify-content-between flex-wrap">
     <div class="d-flex align-items-end flex-wrap">
      <div class="mr-md-3 mr-xl-5">
       <h2>Dashboard</h2>
       <div class="d-flex">
        <i class="mdi mdi-home text-muted hover-cursor"></i>
        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Dashboard</p>
       </div>
      </div>

     </div>
    </div>
   </div>
  </div>
  <div class="row">
   <div class="col-md-12 stretch-card">
    <div class="card">
     <div class="card-body">
      <p class="card-title">Recent Purchases</p>
         <div class="col-md-6">
             <p class="card-title">Recent Quotation</p>
             <div class="table-responsive">
                 <table id="recent-purchases-listing" class="table">
                     <thead>
                     <tr>
                         <th>Name</th>
                         <th>Date</th>
                         <th>Status</th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php $sr = 1; ?>
                     <?php if(!empty($resent_quatation)) {foreach ($resent_quatation as $quatation){?>
                     <tr>

                         <td><?php echo variantTitle($quatation->product_id) ?></td>
                         <td><?php echo $quatation->created_at; ?></td>
                         <td><?php if($quatation->status == "1"){  ?>
                                 Approved
                             <?php }elseif($quatation->status == '0'){ ?>
                                 Pending
                             <?php }elseif($quatation->status == "2"){ ?>
                                 Rejected
                             <?php } ?></td>

                     </tr>
                     <?php }} $sr++;?>
                     </tbody>
                 </table>
             </div>
         </div>
         <div class="col-md-6">
             <p class="card-title">Recent Booking</p>
             <div class="table-responsive">
                 <table id="recent-purchases-listing" class="table">
                     <thead>
                     <tr>
                         <th>Name</th>
                         <th>Date</th>
                         <th>Status</th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php $sr = 1; ?>
                     <?php if(!empty($resent_booking)){ foreach ($resent_booking as $booking){ ?>

                     <tr>
                         <td><?php echo variantTitle($quatation->product_id) ?></td>
                         <td><?php echo $quatation->created_at; ?></td>
                         <td><?php if($quatation->status == "1"){  ?>
                                 Approved
                             <?php }elseif($quatation->status == '0'){ ?>
                                 Pending
                             <?php }elseif($quatation->status == "2"){ ?>
                                 Rejected
                             <?php } ?></td>
                     </tr>
                     <?php }} $sr++;?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
    </div>
   </div>
  </div>
 </div>
<?php echo $footer?>
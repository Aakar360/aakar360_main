<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/14/2019
 * Time: 4:57 PM
 */
 echo $header?>
 <div class="content-wrapper">

  <div class="row">
   <div class="col-md-12 grid-margin">
    <div class="d-flex justify-content-between flex-wrap">
     <div class="d-flex align-items-end flex-wrap">
      <div class="mr-md-3 mr-xl-5">
       <h2>My Bookings</h2>
       <!--p class="mb-md-0">Your analytics dashboard template.</p-->
       <div class="d-flex">
        <i class="mdi mdi-home text-muted hover-cursor"></i>
        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Products&nbsp;/&nbsp;</p>
           <p class="text-primary mb-0 hover-cursor"><a href="<?=url('my-bookings')?>">My Bookings</a></p>
       </div>
      </div>

     </div>
     <!--<div class="d-flex justify-content-between align-items-end flex-wrap">
      <button type="button" class="btn btn-light bg-white btn-icon mr-3 d-none d-md-block ">
       <i class="mdi mdi-download text-muted"></i>
      </button>
      <button type="button" class="btn btn-light bg-white btn-icon mr-3 mt-2 mt-xl-0">
       <i class="mdi mdi-clock-outline text-muted"></i>
      </button>
      <button type="button" class="btn btn-light bg-white btn-icon mr-3 mt-2 mt-xl-0">
       <i class="mdi mdi-plus text-muted"></i>
      </button>
      <button class="btn btn-primary mt-2 mt-xl-0">Generate report</button>
     </div>-->
    </div>
   </div>
  </div>
 <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
     <li class="nav-item">
         <a class="nav-link active" id="pending-tab" data-toggle="pill" href="#pending" role="tab" aria-controls="expired" aria-selected="false">Pending</a>
     </li>
     <li class="nav-item">
         <a class="nav-link" id="completed-tab" data-toggle="pill" href="#completed" role="tab" aria-controls="completed" aria-selected="false">Completed</a>
     </li>
     <li class="nav-item">
         <a class="nav-link" id="cancelled-tab" data-toggle="pill" href="#cancelled" role="tab" aria-controls="cancelled" aria-selected="false">Cancelled</a>
     </li>
 </ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
        <div class="row">
            <?php if(count($ret)){
                foreach ($ret as $booking){

                    $disabled = 'disabled';
                    $product = DB::table('products')->where('id', $booking['p_id'])->first();
                    if($product !== null){
                        $red = url('create-dispatch?bid='.$product->id);?>
                        <div class="col-md-2 grid-margin stretch-card products">
                            <div class="card" style="height: 75%">
                                <?php if($booking['status'] == 'Approved'){ $disabled = ''; ?>
                                    <div class="card-status alert-success">Approved</div>
                                <?php }elseif($booking['status'] == 'Unapproved'){ $red = false;?>
                                    <div class="card-status alert-warning">Pending</div>
                                <?php }elseif($booking['status'] == 'Failed'){ $red = false;?>
                                    <div class="card-status alert-danger">Cancelled</div>
                                <?php } ?>
                                <div class="card-image margin-10 bb-1">
                                    <img src="<?=url('assets/products/'.image_order($product->images))?>"/>
                                </div>
                                <div class="card-body">
                                    <p class="card-title"><?=$product->title;?></p>
                                </div>
                                <div class="card-footer">
                                    <p class="my-2"><i class="mdi mdi-timer"></i> <?=date("d, M Y", strtotime($booking['booking_date']))?></p>
<!--                                    <button type="button" class="btn btn-outline-primary btn-fw btn-sm" <?php //echo $disabled;?> data-redirect="<?php //echo $red?>">Dispatch</button>-->
                                </div>
                            </div>
                        </div>
                    <?php } } }else{?>
                <div class="col-md-12 text-center no-data">
                    <img src="<?=url('assets/institutional/images/no-data.png')?>"/>
                    <h2>You don't have any bookings. Let's start booking products for your next project!</h2>
                    <a href="<?=url('institutional-index')?>" class="btn btn-outline-primary btn-fw btn-lg my-3">Start Booking!</a>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed-tab">
        <div class="row">
            <?php if(count($appvret)){
                foreach ($appvret as $appvbooking){
                    $appvdisabled = 'disabled';
                    $appvproduct = \App\Products::where('id', $appvbooking['p_id'])->first();
                    if($appvproduct !== null){
                        $appvred = url('create-dispatch?bid='.$appvproduct->id);?>
                        <div class="col-md-2 grid-margin stretch-card products">
                            <div class="card" style="height: 75%">
                                <?php if($appvbooking['status'] == 'Approved'){ $appvdisabled = ''; ?>
                                    <div class="card-status alert-success">Approved</div>
                                <?php }elseif($appvbooking['status'] =='Unapproved'){ $appvred = false;?>
                                    <div class="card-status alert-warning">Pending</div>
                                <?php }elseif($appvbooking['status'] == 'Failed'){ $appvred = false;?>
                                    <div class="card-status alert-danger">Rejected</div>
                                <?php }elseif($appvbooking['status'] == 'Completed'){ $appvred = false;?>
                                    <div class="card-status alert-success">Completed</div>
                                <?php } ?>
                                <div class="card-image margin-10 bb-1">
                                    <img src="<?=url('assets/products/'.image_order($appvproduct->images))?>"/>
                                </div>
                                <div class="card-body">
                                    <p class="card-title"><?=$appvproduct->title;?></p>
                                </div>
                                <div class="card-footer">
                                    <p class="my-2"><i class="mdi mdi-timer"></i> <?=date("d, M Y", strtotime($appvbooking['booking_date']))?></p>
                                    <button type="button" class="btn btn-outline-primary btn-fw btn-sm" <?=$appvdisabled;?> data-redirect="<?=$appvred?>">View Details</button>
                                </div>
                            </div>
                        </div>
                    <?php } } }else{?>
                <div class="col-md-12 text-center no-data">
                    <img src="<?=url('assets/institutional/images/no-data.png')?>"/>
                    <h2>You don't have any bookings. Let's start booking products for your next project!</h2>
                    <a href="<?=url('institutional-index')?>" class="btn btn-outline-primary btn-fw btn-lg my-3">Start Booking!</a>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="tab-pane fade" id="cancelled" role="tabpanel" aria-labelledby="cancelled-tab">
        <div class="row">
            <?php if(count($canlret)){
                foreach ($canlret as $canlbooking){
                    $canldisabled = 'disabled';
                    $canlproduct = \App\Products::where('id', $canlbooking['p_id'])->first();
                    if($canlproduct !== null){
                        $canlred = url('create-dispatch?bid='.$canlproduct->id);?>
                        <div class="col-md-2 grid-margin stretch-card products">
                            <div class="card" style="height: 75%">
                                <?php if($canlbooking['status'] == 'Approved'){ $canldisabled = ''; ?>
                                    <div class="card-status alert-success">Approved</div>
                                <?php }elseif($canlbooking['status'] =='Unapproved'){ $canlred = false;?>
                                    <div class="card-status alert-warning">Pending</div>
                                <?php }elseif($canlbooking['status'] == 'Falled'){ $canlred = false;?>
                                    <div class="card-status alert-danger">Rejected</div>
                                <?php }elseif($canlbooking['status'] == 'Completed'){ $canlred = false;?>
                                    <div class="card-status alert-success">Completed</div>
                                <?php } ?>
                                <div class="card-image margin-10 bb-1">
                                    <img src="<?=url('assets/products/'.image_order($canlproduct->images))?>"/>
                                </div>
                                <div class="card-body">
                                    <p class="card-title"><?=$canlproduct->title;?></p>
                                </div>
                                <div class="card-footer">
                                    <p class="my-2"><i class="mdi mdi-timer"></i> <?=date("d, M Y", strtotime($canlbooking['booking_date']))?></p>
                                    <button type="button" class="btn btn-outline-primary btn-fw btn-sm" <?=$canldisabled;?> data-redirect="<?=$canlred?>">View Details</button>
                                </div>
                            </div>
                        </div>
                    <?php } } }else{?>
                <div class="col-md-12 text-center no-data">
                    <img src="<?=url('assets/institutional/images/no-data.png')?>"/>
                    <h2>You don't have any bookings. Let's start booking products for your next project!</h2>
                    <a href="<?=url('institutional-index')?>" class="btn btn-outline-primary btn-fw btn-lg my-3">Start Booking!</a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
 </div>
<?php echo $footer?>
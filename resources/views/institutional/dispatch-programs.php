<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/14/2019
 * Time: 4:57 PM
 */
 echo $header?>
 <div class="content-wrapper">

  <div class="row">
   <div class="col-md-12 grid-margin">
    <div class="d-flex justify-content-between flex-wrap">
     <div class="d-flex align-items-end flex-wrap">
      <div class="mr-md-3 mr-xl-5">
       <h2><?=translate($title)?></h2>
       <!--p class="mb-md-0">Your analytics dashboard template.</p-->
       <div class="d-flex">
        <i class="mdi mdi-home text-muted hover-cursor"></i>
        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Products&nbsp;/&nbsp;</p>
           <p class="text-primary mb-0 hover-cursor"><a href="<?=url('dispatch-programs')?>"><?=translate($title)?></a></p>
       </div>
      </div>

     </div>
        <?php if($dp){ ?>
     <div class="d-flex justify-content-between align-items-end flex-wrap">
         <a href="<?=url('create-dispatch')?>" class="btn btn-outline-primary btn-fw btn-lg">Create Dispatch Program</a>
     </div>
    <?php } ?>
    </div>
   </div>
  </div>
  <div class="row">
<?php if($dp == 0){ ?>
      <div class="col-md-12 text-center no-data">
          <img src="<?=url('assets/institutional/images/no-data.png')?>"/>
          <h2>You don't have any dispatch programs yet. Let's create!</h2>
          <a href="<?=url('create-dispatch')?>" class="btn btn-outline-primary btn-fw btn-lg my-3">Create Dispatch Program</a>
      </div>
<?php }else{ ?>
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Items Detail
                                </th>
                                <th>
                                    Schedule Date
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Created At
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
  </div>
  <!--<div class="row">
   <div class="col-md-7 grid-margin stretch-card">
    <div class="card">
     <div class="card-body">
      <p class="card-title">Cash deposits</p>
      <p class="mb-4">To start a blog, think of a topic about and first brainstorm party is ways to write details</p>
      <div id="cash-deposits-chart-legend" class="d-flex justify-content-center pt-3"></div>
      <canvas id="cash-deposits-chart"></canvas>
     </div>
    </div>
   </div>
   <div class="col-md-5 grid-margin stretch-card">
    <div class="card">
     <div class="card-body">
      <p class="card-title">Total sales</p>
      <h1>$ 28835</h1>
      <h4>Gross sales over the years</h4>
      <p class="text-muted">Today, many people rely on computers to do homework, work, and create or store useful information. Therefore, it is important </p>
      <div id="total-sales-chart-legend"></div>
     </div>
     <canvas id="total-sales-chart"></canvas>
    </div>
   </div>
  </div>-->

 </div>
<?php echo $footer?>
<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: "<?=url("get-dispatch-programs") ?>",
                type: 'GET'
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'items', name: 'items' },
                { data: 'schedule', name: 'schedule' },
                { data: 'status', name: 'status' },
                { data: 'created_at', name: 'created_at' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
    $(document).on('click', '.disp_delete', function(){
        var ele = $(this);
       if(confirm("Are ou sure to delete this Dispatch Program?")){
           $.ajax({
               url: '<?=url("delete-dispatch")?>',
               type: 'post',
               data: {_token: '<?=csrf_token()?>', id: ele.val(), _method: 'delete'},
               success: function(data){
                   //$('#table').DataTable().ajax.reload();
               },
               complete: function () {
                   $('#table').DataTable().ajax.reload();
               }
           });
       }
    });
</script>

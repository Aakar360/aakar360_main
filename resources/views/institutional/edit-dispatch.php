<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/14/2019
 * Time: 4:57 PM
 */
 echo $header?>
<div class="content-wrapper">

    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2><?=translate($title)?></h2>
                        <!--p class="mb-md-0">Your analytics dashboard template.</p-->
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Products&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor"><a href="<?=url('dispatch-programs')?>"><?=translate($title)?></a></p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-end flex-wrap">
                    <div class="form-group col-md-12">
                        <label>Scheduled Date</label>
                        <input name="datepicker" type="text" class="form-control form-control-sm border-default datepicker" placeholder="Schedule Date">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
<?php if($dp == 0){ ?>
        <div class="col-md-12 text-center no-data">
          <img src="<?=url('assets/institutional/images/no-data.png')?>"/>
          <h2>You don't have any Bookings yet. Let's book products!</h2>
          <a href="<?=url('institutional-index')?>" class="btn btn-outline-primary btn-fw btn-lg my-3">Start Booking!</a>
        </div>
<?php }else{ ?>
    <form method="post" action="update-dispatch" style="width: 100%;" id="dispatchForm">
        <input type="hidden" name="schedule" value="<?=(isset($program->schedule) ? date("Y-m-d", strtotime($program->schedule)) : date("Y-m-d"))?>"/>
        <input type="hidden" name="id" value="<?=(isset($_GET['id']) ? $_GET['id'] : '')?>"/>
        <?=csrf_field()?>
        <?php $sparties = json_decode($program->party); ?>
        <?php $sbooks = json_decode($program->items); ?>
        <?php $svariants = json_decode($program->variants); ?>
        <?php $sqtys = json_decode($program->quantity); ?>
        <?php $sunits = json_decode($program->unit);
            $i = 0;
        ?>
        <?php foreach($sparties as $p){ ?>

        <div class="middle-card" data-index="<?=$i?>">
            <div class="card">
                <div class="card-body" data-index="<?=$i?>">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Bookings</label>
                            <select name="booking[<?=$i?>][]" class="form-control form-control-sm border-primary select2 bookings" multiple data-placeholder="Select Booking" data-index="<?=$i?>">
                                <?php
                                foreach ($bookings as $booking){
                                    $selected = '';
                                    foreach($sbooks[$i] as $sbook){
                                        if($sbook == $booking->product_id){
                                            $selected = 'selected="selected"';
                                        }
                                    }
                                    echo '<option value="'.$booking->product_id.'" '.$selected.'>'.getProductName($booking->product_id).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Billing Party </label><a href="#" data-toggle="modal" data-target="#partyModal" style="float: right;font-size: 0.875rem;line-height: 1.4rem">Create New</a>
                            <select name="party[<?=$i?>][]" class="form-control form-control-sm border-secondary select2 parties" data-index="<?=$i?>">
                                <?php
                                    $bill = array();
                                    $bill['name'] = institutional('name');
                                    if(institutional('company') != ''){
                                        $bill['name'] = institutional('company');
                                    }
                                    $bill['value'] = 'customer-'.institutional('id');
                                ?>
                                <option>Select Billing Party</option>
                                <option value="<?=$bill['value']?>" <?=($p[0] == $bill['value'] ? 'selected="selected"' : '')?>><?=$bill['name']?></option>
                                <?php
                                    foreach($billing as $b){
                                        echo '<option value="party-'.$b->id.'" '.($p[0] == 'party-'.$b->id ? 'selected="selected"' : '').'>'.$b->company.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="prepender" data-index="<?=$i?>">
                        <div class="row cloner" data-index="<?=$i?>">
                            <div class="form-group col-md-6">
                                <label>Variants</label>
                                <select name="variants[<?=$i?>][]" class="form-control form-control-sm border-primary select2 variants" data-index="<?=$i?>" data-var-index="<?=count($svariants[$i])?>">
                                    <option value="">Select Variant</option>
                                    <?php
                                    foreach($sbooks[$i] as $sbook){
                                        $vars = getVariants($sbook);
                                        foreach($vars as $var){
                                            echo '<option value="'.$var['id'].'">'.$var['title'].'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Quantity</label>
                                <input name="quantity[<?=$i?>][]" type="text" class="form-control form-control-sm border-default quantity" placeholder="Qty" aria-label="Quantity" data-index="<?=$i?>" data-var-index="<?=count($svariants[$i])?>">
                            </div>
                            <div class="form-group col-md-2">
                                <label>Unit</label>
                                <select name="unit[<?=$i?>][]" class="form-control form-control-sm border-primary select2 unit" data-index="<?=$i?>" data-var-index="<?=count($svariants[$i])?>">
                                    <?php
                                    foreach ($units as $unit){
                                        echo '<option value="'.$unit->id.'">'.$unit->symbol.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label style="width: 100%;">&nbsp;</label>
                                <button type="button" class="btn btn-primary btn-sm add-more" data-index="<?=$i?>" data-var-index="0">Add</button>
                            </div>
                        </div>
                        <?php $j = 0; foreach($svariants[$i] as $svar){ ?>
                        <div class="row cloner" data-index="<?=$i?>" style="background: rgb(239, 239, 239);">
                            <div class="form-group col-md-6">
                                <label>Variants</label>
                                <select name="variants[<?=$i?>][]" class="form-control form-control-sm border-primary select2 variants" data-index="<?=$i?>" data-var-index="0">
                                    <option value="">Select Variant</option>
                                    <?php
                                    foreach($sbooks[$i] as $sbook){
                                        $vars = getVariants($sbook);
                                        foreach($vars as $var){
                                            $selected = '';
                                            if($svar == $var['id']){
                                                $selected = 'selected="selected"';
                                            }
                                            echo '<option value="'.$var['id'].'" '.$selected.'>'.$var['title'].'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Quantity</label>
                                <input name="quantity[<?=$i?>][]" type="text" class="form-control form-control-sm border-default quantity" placeholder="Qty" aria-label="Quantity" data-index="<?=$i?>" data-var-index="0" value="<?=$sqtys[$i][$j]?>">
                            </div>
                            <div class="form-group col-md-2">
                                <label>Unit</label>
                                <select name="unit[<?=$i?>][]" class="form-control form-control-sm border-primary select2 unit" data-index="<?=$i?>" data-var-index="<?=$j?>">
                                    <?php
                                    foreach ($units as $unit){
                                        $selected = '';
                                        if($unit->id == $sunits[$i][$j]){
                                            $selected = 'selected="selected"';
                                        }
                                        echo '<option value="'.$unit->id.'" '.$selected.'>'.$unit->symbol.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label style="width: 100%;">&nbsp;</label>
                                <button type="button" class="btn btn-danger btn-sm remove" data-index="<?=$i?>" data-var-index="<?=$j?>">Remove</button>
                            </div>
                        </div>
                    <?php $j++; } ?>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <?php if($i > 0){ ?>
                    <button type="button" class="btn btn-outline-danger btn-fw remove-dispatch" data-index="<?=$i?>">Remove</button>
                    <?php } ?>
                    <button type="button" class="btn btn-outline-primary btn-fw add-dispatch" data-index="<?=$i?>">Add Another Dispatch</button>
                </div>
            </div>
        </div>
        <?php $i++; } ?>
    </form>
    <div class="col-md-12 text-center">
        <a href="<?=url('dispatch-programs')?>" class="btn btn-secondary btn-fw">Cancel</a>
        <button type="button" class="btn btn-success btn-fw" id="submitForm">Submit</button>
    </div>
<?php } ?>
    </div>

</div>
<?php echo $footer?>
<div id="partyModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create New Billing Party</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form class="forms-sample" method="post" id="partyForm">
                    <div class="form-group">
                        <label for="exampleInputCompany">Company Name *</label>
                        <input type="text" name="company" class="form-control form-control-sm required" id="exampleInputCompany" placeholder="Company Name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail">Email *</label>
                        <input type="email" name="email" class="form-control form-control-sm required" id="exampleInputEmail" placeholder="Email Address" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputMobile">Contact No. *</label>
                        <input type="text" name="mobile" class="form-control form-control-sm required" id="exampleInputMobile" placeholder="Contact No." required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputAddress1">Address Line 1 *</label>
                        <input type="text" name="address1" class="form-control form-control-sm required" id="exampleInputAddress1" placeholder="Address Line 1" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputAddress2">Address Line 2</label>
                        <input type="text" name="address2" class="form-control form-control-sm" id="exampleInputAddress2" placeholder="Address Line 2">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputState">State *</label>
                        <select name="state" class="form-control form-control-sm required state" id="exampleInputState" required>
                            <option value="">Select State</option>
                            <?php
                                foreach($states as $key=>$value){
                                    echo '<option value="'.$key.'">'.$value.'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputCity">City *</label>
                        <select name="city" class="form-control form-control-sm required cities" id="exampleInputCity" required>
                            <option value="">Select City</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPostcode">Postcode *</label>
                        <input type="text" name="postcode" class="form-control form-control-sm required" id="exampleInputPostcode" placeholder="Postcode" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPan">PAN</label>
                        <input type="text" name="pan" class="form-control form-control-sm" id="exampleInputPan" placeholder="PAN">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputGst">GST *</label>
                        <input type="text" name="gst" class="form-control form-control-sm required" id="exampleInputGst" placeholder="GST" required>
                    </div>
                    <p class="alert alert-danger partyError" style="float: left;display:none;"></p>
                    <button type="button" class="btn btn-primary mr-2" style="float: right" id="saveParty">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="float: right; margin-right: 10px;">Cancel</button>
                    <?=csrf_field();?>
                </form>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $('.select2').select2();
        var setDate = new Date(<?=strtotime($program->schedule)?> * 1000);
        var curDate = new Date();
        if(setDate < curDate){
            setDate = curDate;
        }
        $('.datepicker').datepicker({
            format: 'dd, M yyyy',
            widgetPositioning: { horizontal: 'left', vertical: 'bottom'},
            showToday: true,
            autoclose: true,
            startDate: curDate
        }).datepicker("setDate", setDate).on('changeDate', function (ev) {
            $('input[name=schedule]').val(ev.date.getFullYear()+'-'+("0" + (parseInt(ev.date.getMonth())+1).toString()).slice(-2)+'-'+("0" + ev.date.getDate()).slice(-2));
        });
        $('#saveParty').on('click', function () {
            var next = true;
            $('#partyForm .required').each(function(){
                //alert($(this).val());
                if($(this).val() == ''){
                    $('.partyError').html('All fields with * mark is mandatory!');
                    $('.partyError').show();
                    next = false;
                }
            });
            if(next){
                var formData = $('form#partyForm').serialize();

                $.ajax({
                    type: 'post',
                    url: '<?=url("save-party")?>',
                    data: formData,
                    success: function(data){

                        $('.parties').val('');
                        $('.parties').append(data);
//                        alert(data);
                        $('#partyModal').modal('hide');
                        $('.partyError').html('');
                        $('.partyError').hide();
                    },
                    error: function(){
                        $('.partyError').html('Something went wrong. Try Again!');
                        $('.partyError').show();
                    }
                });

            }
        });
        $('.state').on('change', function(){
            var state = $(this).val();
            $.ajax({
                url: '<?=url('get-cities')?>',
                type: 'post',
                data: {_token: '<?=csrf_token()?>', id: state},
                success: function(data){
                    $('.cities').html(data);
                }
            })
        });
        $('#submitForm').on('click', function () {
            $('#dispatchForm').submit();
        });
    });

    $(document).on('click', '.add-more', function(){
        var ele = $(this);
        var varIndex = ele.data('var-index');
        if($('.variants[data-var-index='+varIndex+']').val() == '' || $('.quantity[data-var-index='+varIndex+']').val() == '' || $('.unit[data-var-index='+varIndex+']').val() == ''){
            alert("All fields are mandatory!");
        }else {
            var newVarIndex = parseInt(varIndex) + 1;
            var index = ele.data('index');
            var varOptions = '';
            $('.variants[data-var-index="' + varIndex + '"]:first option').each(function () {
                varOptions += '<option value="' + $(this).val() + '">' + $(this).html() + '</option>';
            });
            var unitOptions = '';
            $('.unit[data-var-index="' + varIndex + '"]:first option').each(function () {
                unitOptions += '<option value="' + $(this).val() + '">' + $(this).html() + '</option>';
            });
            var section = '<div class="row cloner" data-index="' + index + '">\n' +
                '                            <div class="form-group col-md-6">\n' +
                '                                <label>Variants</label>\n' +
                '                                <select name="variants[' + index + '][]" class="form-control form-control-sm border-primary select2 variants" data-index="' + index + '" data-var-index="' + newVarIndex + '">\n' + varOptions +
                '                                </select>\n' +
                '                            </div>\n' +
                '                            <div class="form-group col-md-2">\n' +
                '                                <label>Quantity</label>\n' +
                '                                <input name="quantity[' + index + '][]" type="text" class="form-control form-control-sm border-default quantity" placeholder="Qty" aria-label="Quantity" data-index="' + index + '" data-var-index="' + newVarIndex + '">\n' +
                '                            </div>\n' +
                '                            <div class="form-group col-md-2">\n' +
                '                                <label>Unit</label>\n' +
                '                                <select name="unit[' + index + '][]" class="form-control form-control-sm border-primary select2 unit" data-index="' + index + '" data-var-index="' + newVarIndex + '">\n' + unitOptions +
                '                                </select>\n' +
                '                            </div>\n' +
                '                            <div class="form-group col-md-2">\n' +
                '                                <label style="width: 100%;">&nbsp;</label>\n' +
                '                                <button type="button" class="btn btn-primary btn-sm add-more" data-index="' + index + '" data-var-index="' + newVarIndex + '">Add</button>\n' +
                '                            </div>\n' +
                '                        </div>';
            $('.prepender[data-index=' + index + ']').prepend(section);
            /*$('.cloner:first').clone()
                .find("input:text").val("").end()
                .find("select").val("").end()
                .prependTo('.prepender[data-index='+index+']');*/
            ele.removeClass('btn-primary').removeClass('add-more').addClass('btn-danger').addClass('remove').html('Remove');
            $('.cloner[data-index=' + index + ']:first').find('[data-var-index=' + varIndex + ']').attr('data-var-index', newVarIndex).end();
            $('.cloner[data-index=' + index + ']').css('background', '#efefef');
            $('.cloner[data-index=' + index + ']:first').css('background', 'transparent');
            setTimeout(function () {
                $('.select2[data-index=' + index + '][data-var-index=' + newVarIndex + ']').select2();
            }, 500);
        }
    });
    $(document).on('click', '.remove', function(){
        $(this).parent().parent().remove();
    });
    $(document).on('change', '.bookings', function () {
        var index = $(this).data('index');
        var books = $(this).val();
        if(books != ''){
            $.ajax({
                type: 'post',
                url: '<?=url("get-variants")?>',
                data: {_token:'<?=csrf_token()?>', books:books},
                success: function (data) {
                    $('.variants[data-index='+index+']').html(data);
                }
            })
        }else{
            $('.variants[data-index='+index+']').html('<option>Select Variants</option>');
        }
    });
    $(document).on('click', '.add-dispatch', function(){
        var ele = $(this);
        var index = ele.data('index');
        var newIndex = parseInt(index) + 1;

        var section = '<div class="middle-card" data-index="'+newIndex+'">' +
            '            <div class="card">' +
            '                <div class="card-body" data-index="'+newIndex+'">' +
            '                    <div class="row">' +
            '                        <div class="form-group col-md-6 bookingDiv" data-index="'+newIndex+'">' +
            '                            <label>Bookings</label>' +
    ' <select name="booking['+newIndex+'][]" class="form-control form-control-sm border-primary select2 bookings" multiple data-placeholder="Select Booking" data-index="'+newIndex+'">';
        var bOptions = '';
        $('.bookings:first option').each(function(){
            bOptions += '<option value="'+$(this).val()+'">'+$(this).html()+'</option>';
        })


    section = section + bOptions + '        </select></div>' +
    '                        <div class="form-group col-md-6">' +
    '                            <label>Billing Party </label><a href="#" data-toggle="modal" data-target="#partyModal" style="float: right;font-size: 0.875rem;line-height: 1.4rem">Create New</a>' +
    '                            <select name="party['+newIndex+'][]" class="form-control form-control-sm border-secondary select2 parties" data-index="'+newIndex+'">';
        var billOptions = '';
        $('.parties:first option').each(function(){
            billOptions += '<option value="'+$(this).val()+'">'+$(this).html()+'</option>';
        });
        var unitOptions = '';
        $('.unit:first option').each(function(){
            unitOptions += '<option value="'+$(this).val()+'">'+$(this).html()+'</option>';
        })


        section = section + billOptions + '        </select>' +
    '                        </div>' +
    '                    </div>' +
    '                    <div class="prepender" data-index="'+newIndex+'">' +
    '                        <div class="row cloner" data-index="'+newIndex+'">' +
    '                            <div class="form-group col-md-6">' +
    '                                <label>Variants</label>' +
    '                                <select name="variants['+newIndex+'][]" class="form-control form-control-sm border-primary select2 variants" data-index="'+newIndex+'">' +
    '                                    <option>Select Variant</option>' +
    '                                </select>' +
    '                            </div>' +
    '                            <div class="form-group col-md-2">' +
    '                                <label>Quantity</label>' +
    '                                <input name="quantity['+newIndex+'][]" type="text" class="form-control form-control-sm border-default quantity" placeholder="Qty" aria-label="Quantity" data-index="'+newIndex+'">' +
    '                            </div>' +
    '                            <div class="form-group col-md-2">' +
    '                                <label>Unit</label>' +
    '                                <select name="unit['+newIndex+'][]" class="form-control form-control-sm border-primary select2 unit" data-index="'+newIndex+'">' + unitOptions +
    '                                </select>' +
    '                            </div>' +
    '                            <div class="form-group col-md-2">' +
    '                                <label style="width: 100%;">&nbsp;</label>' +
    '                                <button type="button" class="btn btn-primary btn-sm add-more" data-index="'+newIndex+'" data-var-index="0">Add</button>' +
    '                            </div>' +
    '                        </div>' +
    '                    </div>' +
    '                </div>' +
    '                <div class="card-footer text-center">' +
    '        <button type="button" class="btn btn-outline-danger btn-fw remove-dispatch" data-index="'+newIndex+'">Remove</button>'+
    '                    <button type="button" class="btn btn-outline-primary btn-fw add-dispatch" data-index="'+newIndex+'">Add Another Dispatch</button>' +
    '                </div>' +
    '            </div>' +
    '        </div>';

        $('#dispatchForm').append(section);

        setTimeout(function(){
            $('.select2[data-index='+newIndex+']').select2();
        }, 500);

    });
    $(document).on('click', '.remove-dispatch', function(){
        var index = $(this).data('index');
        $('.middle-card[data-index='+index+']').remove();
    });
</script>

<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/14/2019
 * Time: 4:57 PM
 */
echo $header?>
    <div class="content-wrapper">

        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="d-flex justify-content-between flex-wrap">
                    <div class="d-flex align-items-end flex-wrap">
                        <div class="mr-md-3 mr-xl-5">
                            <h2>My Quotations</h2>
                            <!--p class="mb-md-0">Your analytics dashboard template.</p-->
                            <div class="d-flex">
                                <i class="mdi mdi-home text-muted hover-cursor"></i>
                                <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Products&nbsp;/&nbsp;</p>
                                <p class="text-primary mb-0 hover-cursor"><a href="<?=url('my-bookings')?>">My Quotations</a></p>
                            </div>
                        </div>


                    </div>
                    <!--<div class="d-flex justify-content-between align-items-end flex-wrap">
                     <button type="button" class="btn btn-light bg-white btn-icon mr-3 d-none d-md-block ">
                      <i class="mdi mdi-download text-muted"></i>
                     </button>
                     <button type="button" class="btn btn-light bg-white btn-icon mr-3 mt-2 mt-xl-0">
                      <i class="mdi mdi-clock-outline text-muted"></i>
                     </button>
                     <button type="button" class="btn btn-light bg-white btn-icon mr-3 mt-2 mt-xl-0">
                      <i class="mdi mdi-plus text-muted"></i>
                     </button>
                     <button class="btn btn-primary mt-2 mt-xl-0">Generate report</button>
                    </div>-->
                </div>
            </div>
        </div>
        <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist" style="width: 25%;">
            <li class="nav-item">
                <a class="nav-link active" id="live-tab" data-toggle="pill" href="#live" role="tab" aria-controls="expired" aria-selected="false">Live</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="expired-tab" data-toggle="pill" href="#expired" role="tab" aria-controls="expired" aria-selected="false">Expired</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="live" role="tabpanel" aria-labelledby="live-tab">
                <div class="row">
                    <?php if(count($bookings)){
                        foreach ($bookings as $booking){

                            $disabled = 'disabled';
                            $product = \App\Products::where('id',$booking['product_id'])->first();
                            if($product !== null){ ;?>
                                <div class="col-md-2 grid-margin stretch-card products">
                                    <div class="card" style="height: 75%">
                                        <?php if($booking['status'] == "Approved"){ $disabled = ''; ?>
                                            <div class="card-status alert-success">Approved</div>
                                        <?php }elseif($booking['status'] == 'Pending'){ ?>
                                            <div class="card-status alert-warning">Pending</div>
                                        <?php }elseif($booking['status'] == "Failed"){ ?>
                                            <div class="card-status alert-danger">Rejected</div>
                                        <?php } ?>
                                        <div class="card-image margin-10 bb-1">
                                            <img src="<?=url('assets/products/'.image_order($product->images))?>"/>
                                        </div>
                                        <div class="card-body">
                                            <p class="card-title"><?=$product->title;?></p>
                                            <?php if($booking['payment_option'] !== 'Select Payment Option') { ?>
                                                <div class="d-flex justify-content-between flex-wrap">
                                                    <div class="d-flex flex-column justify-content-around">
                                                        <small class="mb-1 text-muted">Payment Option</small>
                                                        <h5 class="mr-2 mb-0"><i class="mdi mdi-currency-inr"></i><?php echo $booking['payment_option']?></h5>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="card-footer">
                                            <p class="my-2"><i class="mdi mdi-timer"></i> <?=$booking['booking_date'];?></p>
                                            <a href="<?=url('i/my-quotations-detail?id='.$booking['id'].'&method='.$booking['method'].'&product_id='.$booking['product_id'].'&cat_id='.$booking['cat_id'])?>"><button type="button" class="btn btn-outline-primary btn-fw btn-sm" >View Detail</button></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } } }else{?>
                        <div class="col-md-12 text-center no-data">
                            <img src="<?=url('assets/institutional/images/no-data.png')?>"/>
                            <h2>You don't have any bookings. Let's start booking products for your next project!</h2>
                            <a href="<?=url('institutional-index')?>" class="btn btn-outline-primary btn-fw btn-lg my-3">Start Booking!</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="tab-pane fade" id="expired" role="tabpanel" aria-labelledby="expired-tab">
                <div class="row">
                    <?php if(count($exbookings)){
                        foreach ($exbookings as $exbooking){
                            $exdisabled = 'disabled';
                            $exproduct = \App\Products::where('id',$exbooking['product_id'])->first();
                            if($exproduct !== null){ ?>
                                <div class="col-md-2 grid-margin stretch-card products">
                                    <div class="card" style="height: 75%">
                                        <?php if($exbooking['status'] == 'Approved'){ $disabled = ''; ?>
                                            <div class="card-status alert-success">Approved</div>
                                        <?php }elseif($exbooking['status'] == 'Pending'){?>
                                            <div class="card-status alert-warning">Pending</div>
                                        <?php }elseif($exbooking['status'] == 'Expired'){ ?>
                                            <div class="card-status alert-danger">Expired</div>
                                        <?php } ?>
                                        <div class="card-image margin-10 bb-1">
                                            <img src="<?=url('assets/products/'.image_order($exproduct->images))?>"/>
                                        </div>
                                        <div class="card-body">
                                            <p class="card-title"><?=$exproduct->title;?></p>
                                            <?php if($booking['payment_option'] !== 'Select Payment Option') { ?>
                                                <div class="d-flex justify-content-between flex-wrap">
                                                    <div class="d-flex flex-column justify-content-around">
                                                        <small class="mb-1 text-muted">Payment Option</small>
                                                        <h5 class="mr-2 mb-0"><i class="mdi mdi-currency-inr"></i><?php echo $booking['payment_option']?></h5>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="card-footer">
                                            <p class="my-2"><i class="mdi mdi-timer"></i> <?=date("d, M Y", strtotime($exbooking['booking_date']))?></p><!--
                                     <button type="button" class="btn btn-outline-primary btn-fw btn-sm" <?/*=$exdisabled;*/?> data-redirect="<?/*=$red*/?>">Dispatch</button>-->
                                            <a href="<?=url('i/my-quotations-detail?id='.$exbooking['id'])?>"><button type="button" class="btn btn-outline-primary btn-fw btn-sm" >View Detail</button></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } } }else{?>
                        <div class="col-md-12 text-center no-data">
                            <img src="<?=url('assets/institutional/images/no-data.png')?>"/>
                            <h2>You don't have any Expired bookings. Let's start booking products for your next project!</h2>
                            <a href="<?=url('institutional-index')?>" class="btn btn-outline-primary btn-fw btn-lg my-3">Start Booking!</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!--<div class="row">
         <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
           <div class="card-body">
            <p class="card-title">Cash deposits</p>
            <p class="mb-4">To start a blog, think of a topic about and first brainstorm party is ways to write details</p>
            <div id="cash-deposits-chart-legend" class="d-flex justify-content-center pt-3"></div>
            <canvas id="cash-deposits-chart"></canvas>
           </div>
          </div>
         </div>
         <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
           <div class="card-body">
            <p class="card-title">Total sales</p>
            <h1>$ 28835</h1>
            <h4>Gross sales over the years</h4>
            <p class="text-muted">Today, many people rely on computers to do homework, work, and create or store useful information. Therefore, it is important </p>
            <div id="total-sales-chart-legend"></div>
           </div>
           <canvas id="total-sales-chart"></canvas>
          </div>
         </div>
        </div>-->

    </div>
<?php echo $footer?>
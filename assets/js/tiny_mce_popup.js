<?php

namespace App\Http\Controllers;

use App\Ads;
use App\Blog;
use App\Location;
use App\Rating;
use App\Views;
use Illuminate\Http\Request;
use App\Company;
use App\Category;
use App\HomePageAds;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class HomePageController extends Controller
{

    public function index()
    {
        if(request()->session()->get('user.city') != null) {
            $homepageadstop = HomePageAds::where('city', '=', request()->session()->get('user.city'))
            ->where('position', '=', 'top')->get();

            $homepageadsleft = HomePageAds::where('city', '=', request()->session()->get('user.city'))
                ->where('position', '=', 'left_half')->get();

            $homepageadsright = HomePageAds::where('city', '=', request()->session()->get('user.city'))
                ->where('position', '=', 'right_half')->get();

            $bottomcenter = HomePageAds::where('city', '=', request()->session()->get('user.city'))
                ->where('position', '=', 'bottom_center')->get();

            $blogs = Blog::all();

            return view('mainTable.index', compact('homepageadstop', 'homepageadsleft', 'homepageadsright', 'blogs', 'bottomcenter'));
        }
        else {
            $blogs = Blog::all();
            return view('mainTable.city_index', compact('blogs'));
        }
    }
    public function blogs(){
        $blogs = Blog::all();
        return view('mainTable.blogs', compact('blogs'));
    }
    public function blog($id){
        $blog = Blog::find($id);

        $left = Ads::where('city', '=', request()->session()->get('user.city'))
            ->where('section', '=', 'Left')
            ->where('page', '=', 'Blog')->first();

        $bottom = Ads::where('city', '=', request()->session()->get('user.city'))
            ->where('section', '=', 'Bottom')
            ->where('page', '=', 'Blog')->first();

        $right = Ads::where('city', '=', request()->session()->get('user.city'))
            ->where('section', '=', 'Right')
            ->where('page', '=', 'Blog')->first();

        return view('mainTable.blog', compact('blog', 'left', 'right', 'bottom'));
    }
    public function table(Request $request)
    {
        $companies = Company::filterByRequest($request)->paginate(9);
        if(request()->session()->get('user.city') != null)
            return view('mainTable.search', compact('companies'));
        else
            return redirect()->to('/');

    }

    public function category(Category $category, Location $location)
    {
        $search = '';
        $search = request()->get('search');
        $sortby = request()->get('sort');
        $sort = $sortby;
        if($search == '') {
            if ($location->id == null) {
                if($sortby != ''){
                    $order = explode('-', $sortby)[1];
                    $query = Company::join('category_company', 'companies.id', '=', 'category_company.company_id')
                        ->where('category_id','=', $category->id)
                        ->where('city_id', '=', request()->session()->get('user.city'))->orderBy('companies.total_rating', $order);
                }else{
                    $query = Company::join('category_company', 'companies.id', '=', 'category_company.company_id')
                        ->where('category_id','=', $category->id)
                        ->where('city_id', '=', request()->session()->get('user.city'))->orderBy('priority', 'ASC');
                }

            } else {
                if($sortby != '') {
                    $order = explode('-', $sortby)[1];
                    $query = Company::join('category_company', 'companies.id', '=', 'category_company.company_id')
                        ->where('category_id', '=', $category->id)
                        ->where('location_id', '=', $location->id)
                        ->where('city_id', '=', request()->session()->get('user.city'))->orderBy('companies.total_rating', $order);
                }else {
                    $query = Company::join('category_company', 'companies.id', '=', 'category_company.company_id')
                        ->where('category_id', '=', $category->id)
                        ->where('location_id', '=', $location->id)
                        ->where('city_id', '=', request()->session()->get('user.city'))->orderBy('priority', 'ASC');
                }
            }
        }else{
            if ($location->id == null) {
                if($sortby != '') {
                    $order = explode('-', $sortby)[1];
                    $query = Company::join('category_company', 'companies.id', '=', 'category_company.company_id')
                        ->where(function ($query) use ($search) {
                            $query->orWhere('name', 'LIKE', '%' . $search . '%');
                            $query->orWhere('courses_offered', 'LIKE', '%' . $search . '%');
                            $query->orWhere('short_des', 'LIKE', '%' . $search . '%');
                            $query->orWhere('address', 'LIKE', '%' . $search . '%');
                            $query->orWhere('description', 'LIKE', '%' . $search . '%');
                        })
                        ->where('category_id', '=', $category->id)
                        ->where('city_id', '=', request()->session()->get('user.city'))->orderBy('companies.total_rating', $order);
                }else {
                    $query = Company::join('category_company', 'companies.id', '=', 'category_company.company_id')
                        ->where(function ($query) use ($search) {
                            $query->orWhere('name', 'LIKE', '%' . $search . '%');
                            $query->orWhere('courses_offered', 'LIKE', '%' . $search . '%');
                            $query->orWhere('short_des', 'LIKE', '%' . $search . '%');
                            $query->orWhere('address', 'LIKE', '%' . $search . '%');
                            $query->orWhere('description', 'LIKE', '%' . $search . '%');
                        })
                        ->where('category_id', '=', $category->id)
                        ->where('city_id', '=', request()->session()->get('user.city'))->orderBy('priority', 'ASC');
                }
            } else {
                if($sortby != '') {
                    $order = explode('-', $sortby)[1];
                    $query = Company::join('category_company', 'companies.id', '=', 'category_company.company_id')
                        ->where(function ($query) use ($search) {
                            $query->orWhere('name', 'LIKE', '%' . $search . '%');
                            $query->orWhere('courses_offered', 'LIKE', '%' . $search . '%');
                            $query->orWhere('short_des', 'LIKE', '%' . $search . '%');
                            $query->orWhere('address', 'LIKE', '%' . $search . '%');
                            $query->orWhere('description', 'LIKE', '%' . $search . '%');
                        })
                        ->where('category_id', $category->id)
                        ->where('location_id', $location->id)
                        ->where('city_id', '=', request()->session()->get('user.city'))->orderBy('companies.total_rating', $order);
                }else {
                    $query = Company::join('category_company', 'companies.id', '=', 'category_company.company_id')
                        ->where(function ($query) use ($search) {
                            $query->orWhere('name', 'LIKE', '%' . $search . '%');
                            $query->orWhere('courses_offered', 'LIKE', '%' . $search . '%');
                            $query->orWhere('short_des', 'LIKE', '%' . $search . '%');
                            $query->orWhere('address', 'LIKE', '%' . $search . '%');
                            $query->orWhere('description', 'LIKE', '%' . $search . '%');
                        })
                        ->where('category_id', $category->id)
                        ->where('location_id', $location->id)
                        ->where('city_id', '=', request()->session()->get('user.city'))->orderBy('priority', 'ASC');
                }
            }
        }
        $companies = $query->paginate(9);

        $locations = Location::where('city_id', '=', request()->session()->get('user.city'))->get();

        $cat = \App\Category::WHERE('id', $category->id)->first();
        $loc = \App\Location::WHERE('id', $location->id)->first();

        $ads = Ads::where('city', '=', request()->session()->get('user.city'))
            ->where('page', '=', 'Category')
            ->where('section', '=', 'Left')->get();

        if(request()->session()->get('user.city') != null)
            return view('mainTable.category', compact('companies', 'category', 'cat', 'loc', 'locations', 'ads', 'sort'));
        else
            return redirect()->to('/');

    }

    public function company(Company $company)
    {
        if(request()->session()->get('user.city') != null) {
            $ads = Ads::where('city', '=', request()->session()->get('user.city'))
                ->where('section', '=', 'Right')
                ->where('page', '=', 'Single Details')->get();


            $rating = \App\Rating::selectRaw("count(*) as total_user, SUM(rating) as total_rating")->where('academy_id', '=', $company->id)->first();
            $total_rating =  $rating->total_rating;
            $total_user = $rating->total_user;
            if($total_rating==0){
                $avg_rating = 0;
            }
            else{
                $avg_rating = round($total_rating/$total_user,1);
            }
            $comments = \App\Rating::where('academy_id', '=', $company->id )->orderBy('id','DESC')->get();
            // $total_no_of_rating = App\Rating::select( DB::raw('count(id) as total_rating,rating'))->where('product_id', '=', $quiz->id )->groupBy('rating')->get();



            $data['avg_rating'] = $avg_rating;
            $data['total_user'] = $total_user;
            $data['comments'] = $comments;

            return view('mainTable.company', compact('company', 'ads', 'views', 'comments', 'rating', 'data', 'avg_rating', 'total_user'));
        }
        else {
            return redirect()->to('/');
        }

    }

    public function create(Request $request)
    {

        $company = Rating::create($request->all());

        $rating = \App\Rating::selectRaw("count(*) as total_user, SUM(rating) as total_rating")->where('academy_id', '=', $company->academy_id)->first();
        $total_rating =  $rating->total_rating;
        $total_user = $rating->total_user;
        if($total_rating > 0){
            $avg_rating = round($total_rating/$total_user,1);
            $tr = Company::where('id', $company->academy_id)->first();
            $tr->total_rating = $avg_rating;
            $tr->save();
        }

        return back();
    }

    public function setCity(Request $request, $city){
        $request->session()->put('user.city', $city);
        return back();
    }

    public function getdata(Request $request){
        $cont = Company::where('id', '=', $request->academy_id)->first();
        $contact = '
            <li>Mobile :- <a href="tel:'.$cont->mobile.'">'.$cont->mobile.'</a></li>
            <li>Email :- <a href="mailto:'.$cont->email.'">'.$cont->email.'</a></li>
            <li>Website :- <a href="'.$cont->website.'">'.$cont->website.'</a></li>
        ';

        $finalRequest = new Request(array_merge($request->all(), ['user_id' => Auth::user()->id]));
        //dd($finalRequest->all());
        $views = Views::create($finalRequest->all());

        define('HASOFFERS_API_URL', 'http://bulksms.adroweb.com/api/mt/SendSMS');
        $mobile = $cont->mobile;
        // Specify method arguments
        $args = array(
            'user' => 'stuhub',
            'password' => '123456',
            'senderid' => 'STUHUB',
            'channel' => 'Trans',
            'DCS' => 0,
            'flashsms' => 0,
            'number' => '91' . $mobile,
            'text' => "New visitor viewed your academy profile.\nName- ".Auth::user()->name.",\nMobile- ".Auth::user()->mobile_no.",\nEmail- ".Auth::user()->email  ,
            'route' => '08'
        );
        // Initialize cURL
        $curlHandle = curl_init();

        // Configure cURL request
        curl_setopt($curlHandle, CURLOPT_URL, HASOFFERS_API_URL . '?' . http_build_query($args));

        // Make sure we can access the response when we execute the call
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);

        // Execute the API call
        $jsonEncodedApiResponse = curl_exec($curlHandle);
        //dd($jsonEncodedApiResponse);
        return $contact;
    }


   public function enquiry(Request $request)
    {
        $to = "info@adroweb.com";
        $subject = "Message From stuhub.org";
        $a = "Name ";
        $b = "Email ";
        $c = "Subject ";
        $d = "Message ";

        $name = $_POST['name']; // required
        $comments = $_POST['msg']; // required
        $email = $_POST['email'];
        $contact = $_POST['subject'];

        $message = $a.$name.$b.$email.$c.$contact.$d.$comments;
        $message="
            <html>
            <body>
              <tr><td><h5>$a&nbsp;:</h5></td> <td>$name</td></tr> 
              <tr><td><h5>$b&nbsp;:</h5></td> <td>$email</td></tr> 
              <tr><td><h5>$c&nbsp;:</h5></td> <td>$contact</td></tr> 
              <tr><td><h5>$d&nbsp;:</h5></td> <td>$comments</td></tr>
            </body>
            </html>
            ";

        $from = $_POST["email"];
        $headers = "From: puneets334@gmail.com\r\n";
        $headers .= "Content-type: text/html\r\n";

        $sendmail = mail($to,$subject,$message,$headers);

        // verify if mail is sent or not
        $to = $_POST['email'];
        $subject = "Thank You For Your Intrest";
        $message="
            <html>
            <body>
              <tr><td>We have recieved your message. We will get back to you soon.</td></tr>
              <tr><td>Regards</td></tr>
              <tr><td>www.stuhub.org</td></tr>
             </body>
            </html>";

        $from = "puneets334@gmail.com";
        $headers = "From: $from\r\n";
        $headers .= "Content-type: text/html\r\n";

        mail($to,$subject,$message,$headers);
        if ($sendmail) {
            return back();
        }
        else {
            return back();
        }
    }


   
    
    public function contact()
    {
        return view('mainTable.contact');
    }

    public function contactCreate(Request $request)
    {
        $to = "info@stuhub.org";
        $subject = "New Contact Form Entry";
        $a = "Name ";
        $b = "Email ";
        $c = "Subject ";
        $d = "Message ";

        $name = $_POST['name']; // required
        $comments = $_POST['msg']; // required
        $email = $_POST['email'];
        $contact = $_POST['subject'];

        $message = $a.$name.$b.$email.$c.$contact.$d.$comments;
        $message="
            <html>
            <body>
              <tr><td><h5>$a&nbsp;:</h5></td> <td>$name</td></tr> 
              <tr><td><h5>$b&nbsp;:</h5></td> <td>$email</td></tr> 
              <tr><td><h5>$c&nbsp;:</h5></td> <td>$contact</td></tr> 
              <tr><td><h5>$d&nbsp;:</h5></td> <td>$comments</td></tr>
            </body>
            </html>
            ";

        $from = "no-reply@stuhub.org";
        $headers = "From: no-reply@stuhub.org\r\n";
        $headers .= "Content-type: text/html\r\n";

        $sendmail = mail($to,$subject,$message,$headers);

        // verify if mail is sent or not
        $to = $_POST['email'];
        $subject = "Thank You For Your Interest";
        $message="
            <html>
            <body>
              <tr><td>We have recieved your message. We will get back to you soon.</td></tr>
              <tr><td>Regards</td></tr>
              <tr><td>www.stuhub.org</td></tr>
             </body>
            </html>";

        $from = "info@stuhub.org";
        $headers = "From: $from\r\n";
        $headers .= "Content-type: text/html\r\n";

        mail($to,$subject,$message,$headers);
        if ($sendmail) {
            $msg = 'Message Sent successfully';
            return view('mainTable.contact', compact('msg'));
        }
        else {
            $msg = 'Message Sending Failed';
            return view('mainTable.contact', compact('msg'));
        }
    }

}